<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


//MTO ---------------------------------------------------------------
$route['mto_list/(:any)'] 				= 'mto/mto_list/$1';
$route['mto_add'] 						= 'mto/mto_add';
$route['mto_detail/(:any)']				= 'mto/mto_detail/$1';
$route['mto_detail_preview'] 			= 'mto/mto_detail_preview';
$route['mto_submit/(:any)'] 			= 'mto/mto_submit/$1';
$route['mto_approval/(:any)'] 			= 'mto/mto_approval/$1';
$route['mto_detail_approval/(:any)'] 	= 'mto/mto_detail_approval/$1';
//-------------------------------------------------------------------

//MR ---------------------------------------------------------------
$route['mr_list'] 						= 'mr/mr_list';
$route['mr_detail/(:any)'] 				= 'mr/mr_detail/$1';
//-------------------------------------------------------------------

//PO ---------------------------------------------------------------
$route['po_list'] 						= 'po/po_list';
//-------------------------------------------------------------------

//MIS
$route['mis_list'] 						= 'mis/mis_list';
$route['mis_add'] 						= 'mis/mis_add';
$route['mis_detail/(:any)']				= 'mis/mis_detail/$1';
$route['mis_detail_preview'] 			= 'mis/mis_detail_preview';

//MRS
$route['mrs_list'] 						= 'mrs/mrs_list';
$route['mrs_add'] 						= 'mrs/mrs_add';
$route['mrs_detail/(:any)']				= 'mrs/mrs_detail/$1';
$route['mrs_detail_preview'] 			= 'mrs/mrs_detail_preview';

//OSD
$route['osd_list'] 						= 'osd/osd_list';
$route['osd_add'] 						= 'osd/osd_add';
$route['osd_detail/(:any)']				= 'osd/osd_detail/$1';
$route['osd_detail_preview'] 			= 'osd/osd_detail_preview';

//MTO CATEGORY
$route['mto_category_list'] 			= 'mto_category';

$route['catalog_category_list'] 		= 'catalog_category';

//MATERIAL CATALOG -----------------------------------------------
$route['material_catalog_add'] 				= 'material_catalog/add';
$route['material_catalog_detail/(:any)'] 	= 'material_catalog/detail/$1';
$route['material_catalog_list'] 			= 'material_catalog';
$route['import_material_catalog'] 			= 'material_catalog/import_material';
//-----------------------------------------------------------------

//RECEIVING ------------------------------------------------------
$route['receiving_list/(:any)'] 		= 'receiving/receiving_list/$1';
$route['receiving_add'] 				= 'receiving/receiving_add';
$route['receiving_detail/(:any)'] 		= 'receiving/receiving_detail/$1';

$route['receiving_detail_preview'] 		= 'receiving/receiving_detail_preview';
// $route['receiving_pending_list']		= 'receiving/receiving'
//----------------------------------------------------------------

//RECEIVING CS ---------------------------------------------------
$route['receiving_cs_add'] 				= 'receiving/receiving_cs_add';
$route['receiving_cs_list'] 			= 'receiving/receiving_cs_list';
$route['receiving_cs_detail/(:any)'] 	= 'receiving/receiving_cs_detail/$1';
//----------------------------------------------------------------

//RECEIVING CONSUMABLE ---------------------------------------------------
$route['receiving_consumable_add'] 				= 'receiving/r_cons_add';
$route['receiving_consumable_list'] 			= 'receiving/r_cons_list';
$route['receiving_consumable_detail/(:any)'] 	= 'receiving/receiving_cs_detail/$1';
//----------------------------------------------------------------

//VENDOR ---------------------------------------------------------
$route['vendor_list'] 					= 'vendor/vendor_list';
$route['vendor_new'] 					= 'vendor/vendor_add';
$route['vendor_import'] 				= 'vendor/vendor_import';
$route['vendor_edit/(:any)'] 			= 'vendor/vendor_edit/$1';
//----------------------------------------------------------------

//VENDOR VERIFICATION --------------------------------------------
$route['vendor_verification_list'] 			= 'vendor_verification/vendor_verification_list';
$route['vendor_verification_new'] 			= 'vendor_verification/vendor_verification_add';
$route['vendor_verification_import'] 		= 'vendor_verification/vendor_verification_import';
$route['vendor_verification_edit/(:any)'] 	= 'vendor_verification/vendor_verification_edit/$1';
//----------------------------------------------------------------