<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	 public function __construct()
    {
        parent::__construct();
        $this->load->helper('browser');
        //check_browser();
       
        $this->load->model("auth_mod");        
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	//Function go back alert ketika gagal login

	public function get_client_ip() {

         $ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';

    	return $ipaddress;

	}

	public function cookie_get(){

		$this->load->helper('cookie'); 
		return $cookie = explode(";",$this->input->cookie('portal_user'));
	}

	public function index()
	{
		$this->load->helper('cookie');

		$get_user = $this->cookie_get();
		$id 	  = $get_user[0];
		
		if($id == ''){
		
				$data['meta_title'] = 'Login Page';
				$data['slider'] 		= $this->auth_mod->get_slider();
				$this->load->view('login', $data);

		} else {
				redirect('home');
		}
	}
	

	public function checking() {
		
		$this->load->model('auth_mod'); //load model_user
			
		$data = array('email' => $this->input->post('email', TRUE),'password' => md5($this->input->post('password', TRUE)));
		
		$hasil = $this->auth_mod->cek_user($data);

		$cookies_expired_on_1_days = 86400;
		$total_limit_days_cookies = 30;

		$cookies_expired_default = $cookies_expired_on_1_days * $total_limit_days_cookies;

		if ($hasil->num_rows() == 1) {
							 
			$this->load->helper('cookie');

			foreach ($hasil->result() as $sess) {


				// SET COOKIES FOR PORTAL //

				$link_portal = base_url();

				$array_value = $sess->id_user.";".$sess->full_name.";".$sess->badge_no.";".$sess->username.";".$sess->department.";".$sess->email.";".$sess->sign_id.";".$sess->id_role.";".$sess->status_user.";".$link_portal.";".$sess->project_id.";".$sess->last_update.";".$this->input->post('nama_pt', TRUE);

				// 0. id_user
				// 1. full_name
				// 2. badge_no
				// 3. username
				// 4. department
				// 5. email
				// 6. sign_id
				// 7. id_role
				// 8. status_user
				// 9. link_portal
				// 10. project_id
				// 11. last_update
				// 12. nama_pt

				
				$id_user_cookies = array('name' => 'portal_user','value' => $array_value,'expire' => $cookies_expired_default);
           		$this->input->set_cookie($id_user_cookies);	

           		// SET COOKIES FOR PORTAL //

				// SET COOKIES WH //

				$array_value_warehouse = $sess->warehouse_permission;
												
           		$app_warehouse_permission = array('name' => 'portal_wh','value' => $array_value_warehouse,'expire' => $cookies_expired_default);
           		
           		$this->input->set_cookie($app_warehouse_permission);
				
				// SET COOKIES WH //

            }   

            foreach ($hasil->result() as $checking_data) {

						$status_user = $checking_data->status_user;

						if ($status_user == 1) {

									$data = array(
										'ip_address' => $this->get_client_ip(),
										'username' 	 => $checking_data->username,
										'full_name'  => $checking_data->full_name,
									);

        							$this->db->insert('portal_user_login_history', $data);
								switch ($status_user){
									
									case 1 :
									redirect('home');
									break;

									default :
									redirect('Auth');
									break;
								}

						} else {
							
							$this->session->set_flashdata('message',"<div class='alert-secondary'> <p>Your account has been locked.</p></div><br>");

							redirect('Auth');
						}

				}

			} else {
					
					$this->session->set_flashdata('message','<script type="text/javascript">swal("Sorry!", "Your Email or password Incorrect!", "warning");</script>');
					redirect('Auth');
			}
	}

	
	public function logout() {

				$get_user      = $this->cookie_get();
       			$username      =  $get_user[3];
       			$ipaddress_cur = $this->get_client_ip();
       			
				$data = array('status_login' => '0');
				
        		$this->db->where('username', $username);
        		$this->db->where('ip_address', $ipaddress_cur);
        		$this->db->update('portal_user_login_history', $data);
				
				$this->load->helper('cookie');
           		delete_cookie('portal_user');	
           		delete_cookie('portal_emr');
           		delete_cookie('portal_qcs');	
           		delete_cookie('portal_pcms');
           		delete_cookie('portal_wh');	

				redirect('Auth');
	}



	
}

