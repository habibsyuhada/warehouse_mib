<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Budget extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('budget_mod');
		$this->load->model('general_mod');

		$this->user_cookie 		 = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));

		$this->sidebar = "budget/sidebar";
		$this->m_sidebar = "user/sidebar";
	}

	public function index(){
		redirect('budget/budgetList');
	}

	public function budgetList(){
		$where['det.status_delete'] = 1;
		if($this->input->get('year')){
			$where['bud.year'] = $this->input->get('year');
		}
		if($this->input->get('dept')){
			$where['bud.dept'] = $this->input->get('dept');
		}
		if($this->input->get('category')){
			$where['det.id_category'] = $this->input->get('category');
		}
		if($this->input->get('assetable')){
			$where['det.assetable'] = $this->input->get('assetable');
		}
		$data['all_list']	   			= $this->budget_mod->all_list(null, $where);

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->budget_mod->budget_categoty_list();
		foreach ($datadb as $value) {
			$data['cat_list'][$value['id']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Budget List';
		$data['subview']					= 'budget/budget_list';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	function budgetNewProcess($data_post){
		if($this->permission_cookie[2] == 0){
			redirect('auth/logout');
		}
		$form_data = array(
			'year' 					=> $data_post['year'],
			'dept' 					=> $data_post['dept'],
			'nama_pt'		 		=> $this->user_cookie[12],
			'created_by' 		=> $this->user_cookie[0],
		);
		$id = $this->budget_mod->budget_new_process($form_data);
		return $id;
	}

	public function budgetDetailNew($filter = NULL){
		$filter = explode("-", $filter);

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->budget_mod->budget_categoty_list();
		foreach ($datadb as $value) {
			$data['cat_list'][$value['id']] = $value;
		}

		$data['filter']	   				= $filter;
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'New Budget';
		$data['subview']					= 'budget/budget_new';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function budgetDetailNewProcess(){
		if($this->permission_cookie[2] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$where['year'] = $data_post['year'];
		$where['dept'] = $data_post['dept'];
		$budget = $this->budget_mod->budget_list(null, $where);
		if(count($budget) == 0){
			$id_budget = $this->budgetNewProcess($data_post);
		}
		else{
			$budget = $budget[0];
			$id_budget = $budget['id'];
		}

		$form_data = array(
			'id_budget' 	=> $id_budget,
			'id_category' => $data_post['category'],
			'assetable' 	=> $data_post['assetable'],
			'description' => $data_post['description'],
			'budget' 			=> $data_post['budget'],
			'remarks' 		=> $data_post['remarks'],
			'created_by' 	=> $this->user_cookie[0],
		);
		$id = $this->budget_mod->budget_detail_new_process($form_data);

		$this->session->set_flashdata('success', 'Your data has been inserted!');
		redirect('budget/budgetList/'.$data_post['year'].'-'.$data_post['dept'].'-'.$data_post['category']);
	}

	public function budgetDetailImport(){
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Import Budget';
		$data['subview']					= 'budget/budget_import';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function budgetDetailImportPreview(){
		if($this->permission_cookie[5] == 0){
			redirect('auth/logout');
		}

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['name_of_department']] = $value;
		}

		$datadb = $this->budget_mod->budget_categoty_list();
		foreach ($datadb as $value) {
			$data['cat_list'][$value['account_no']] = $value;
		}

		$id_user = $this->user_cookie;

		$config['upload_path']          = 'file/budget/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 						= TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("budget/budgetDetailImport");
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/budget/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data['sheet']								= $sheet;
		$data['drawing_type']					= '1';

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Import Budget';
		$data['subview']					= 'budget/budget_import_preview';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function budgetDetailImportProcess(){
		$form_data 			= array();
		$data_post 			= $this->input->post();
		$created_by 		= $this->user_cookie[0];
		$created_date 	= date("Y-m-d H:i:s");
		$id_budget_arr	= array();

		foreach ($data_post['year'] as $key => $value) {
			if(isset($id_budget_arr[$data_post['year'][$key]][$data_post['dept'][$key]])){
				$id_budget = $id_budget_arr[$data_post['year'][$key]][$data_post['dept'][$key]];
			}
			else{
				$where['year'] = $data_post['year'][$key];
				$where['dept'] = $data_post['dept'][$key];
				$budget = $this->budget_mod->budget_list(null, $where);
				if(count($budget) == 0){
					$data_need = array(
						'year'			=> $data_post['year'][$key],
						'dept'			=> $data_post['dept'][$key],
					);
					$id_budget = $this->budgetNewProcess($data_need);
				}
				else{
					$budget = $budget[0];
					$id_budget = $budget['id'];
				}
				$id_budget_arr[$data_post['year'][$key]][$data_post['dept'][$key]] = $id_budget;
			}
			

			array_push($form_data, array(
				'id_budget'			=>$id_budget, // Insert data nis dari kolom A di excel
				'description'		=>$data_post['description'][$key], // Insert data nis dari kolom A di excel
				'budget'				=>$data_post['budget'][$key], // Insert data nis dari kolom A di excel
				'remarks'				=>$data_post['remarks'][$key], // Insert data nis dari kolom A di excel
				'id_category'		=>$data_post['category'][$key], // Insert data nis dari kolom A di excel
				'created_by'		=>$created_by, // Insert data nis dari kolom A di excel
				'created_date'	=>$created_date, // Insert data nis dari kolom A di excel
			));
		}
		$this->budget_mod->budget_detail_import_process($form_data);
		
		$this->session->set_flashdata('success', 'Your data has been imported!');
		redirect('budget/budgetDetailImport');
	}

	public function budgetDetailEdit($id){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
		$datadb	  					= $this->budget_mod->all_list($id);
		$data['budget']	  	= $datadb[0];

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->budget_mod->budget_categoty_list();
		foreach ($datadb as $value) {
			$data['cat_list'][$value['id']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Edit Budget Detail';
		$data['subview']					= 'budget/budget_edit';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function budgetDetailEditProcess(){
		if($this->permission_cookie[3] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$where['id'] = $data_post['id'];
		$form_data = array(
			'description' => $data_post['description'],
			'budget' 			=> $data_post['budget'],
			'remarks' 		=> $data_post['remarks']
		);
		$id = $this->budget_mod->budget_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been updated!');
		redirect('budget/budgetDetailEdit/'.strtr($this->encryption->encrypt($data_post['id']), '+=/', '.-~'));
	}

	public function budgetDetailDeleteProcess($id){
		if($this->permission_cookie[4] == 0){
			redirect('auth/logout');
		}
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		$datadb	  	= $this->budget_mod->all_list($id);
		$budget	  	= $datadb[0];

		$where['id'] = $id;
		$form_data = array(
			'status_delete' => 0,
		);
		$id = $this->budget_mod->budget_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been deleted!');
		redirect('budget/budgetList/'.$budget['year'].'-'.$budget['dept'].'-'.$budget['id_category']);
	}

	// Transfer
		public function transferList(){
		$data['transfer_list']	   			= $this->budget_mod->transfer_list();

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->budget_mod->budget_categoty_list();
		foreach ($datadb as $value) {
			$data['cat_list'][$value['id']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Transfer List';
		$data['subview']					= 'budget/transfer_list';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function transferDetailNew(){

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->budget_mod->budget_categoty_list();
		foreach ($datadb as $value) {
			$data['cat_list'][$value['id']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'New Transfer Budget';
		$data['subview']					= 'budget/transfer_new';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function transferDetailNewProcess(){
		if($this->permission_cookie[14] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		if(($data_post['from_category'] == $data_post['to_category']) && ($data_post['from_dept'] == $data_post['to_dept'])){
			$this->session->set_flashdata('error', 'Source and Destination budget should be different!');
			redirect('budget/transferDetailNew/');
			return false;
		}

		$form_data = array(
			'year' => $data_post['year'],
			'assetable' => $data_post['assetable'],
			'from_category' => $data_post['from_category'],
			'from_dept' => $data_post['from_dept'],
			'to_category' => $data_post['to_category'],
			'to_dept' => $data_post['to_dept'],
			'budget' => $data_post['budget'],
			'remarks' => $data_post['remarks'],
			'nama_pt' => $this->user_cookie[12],
			'created_by' => $this->user_cookie[0],
		);
		$id = $this->budget_mod->transfer_detail_new_process($form_data);

		$this->session->set_flashdata('success', 'Your data has been inserted!');
		redirect('budget/transferList/');
	}

	public function transferDetailEdit($id){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
		$datadb	  					= $this->budget_mod->transfer_list($id);
		$data['transfer']	  	= $datadb[0];

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->budget_mod->budget_categoty_list();
		foreach ($datadb as $value) {
			$data['cat_list'][$value['id']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Edit Tranfer Budget Detail';
		$data['subview']					= 'budget/transfer_edit';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function transferDetailEditProcess(){
		if($this->permission_cookie[15] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		if(($data_post['from_category'] == $data_post['to_category']) && ($data_post['from_dept'] == $data_post['to_dept'])){
			$this->session->set_flashdata('error', 'Source and Destination budget should be different!');
			redirect('budget/transferDetailEdit/'.strtr($this->encryption->encrypt($data_post['id']), '+=/', '.-~'));
			return false;
		}

		$where['id'] = $data_post['id'];
		$form_data = array(
			'year' => $data_post['year'],
			'assetable' => $data_post['assetable'],
			'from_category' => $data_post['from_category'],
			'from_dept' => $data_post['from_dept'],
			'to_category' => $data_post['to_category'],
			'to_dept' => $data_post['to_dept'],
			'budget' => $data_post['budget'],
			'remarks' => $data_post['remarks'],
		);
		$id = $this->budget_mod->transfer_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been updated!');
		redirect('budget/transferDetailEdit/'.strtr($this->encryption->encrypt($data_post['id']), '+=/', '.-~'));
	}

	public function transferDetailDeleteProcess($id){
		if($this->permission_cookie[16] == 0){
			redirect('auth/logout');
		}
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		$datadb	  	= $this->budget_mod->transfer_list($id);
		$transfer	  	= $datadb[0];

		$where['id'] = $id;
		$form_data = array(
			'status_delete' => 0,
		);
		$id = $this->budget_mod->transfer_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been deleted!');
		redirect('budget/transferList/');
	}

	public function budgetCategoryList(){
		$data['all_list']	   			= $this->budget_mod->budget_category_list();

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Account List';
		$data['subview']					= 'budget/budget_category_list';
		$data['sidebar']					= $this->m_sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function budgetCategoryNew(){
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'New Account';
		$data['subview']					= 'budget/budget_category_new';
		$data['sidebar']					= $this->m_sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function budgetCategoryNewProcess(){
		if($this->permission_cookie[8] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$form_data = array(
			'account_no' => $data_post['account_no'],
			'assetable' => $data_post['assetable'],
			'category_name' => $data_post['category_name'],
			'created_by' => date('Y-m-d H:i:s'),
		);
		$id = $this->budget_mod->budget_category_new_process($form_data);

		$this->session->set_flashdata('success', 'Your data has been inserted!');
		redirect('budget/budgetCategoryList/');
	}

	public function budgetCategoryEdit($id){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
		$datadb	  					= $this->budget_mod->budget_category_list($id);
		$data['category']	  	= $datadb[0];

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Edit Account Detail';
		$data['subview']					= 'budget/budget_category_edit';
		$data['sidebar']					= $this->m_sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function budgetCategoryEditProcess(){
		if($this->permission_cookie[9] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$where['id'] = $data_post['id'];
		$form_data = array(
			'account_no' => $data_post['account_no'],
			'category_name' => $data_post['category_name'],
			'status_delete' 		=> $data_post['status_delete'],
		);
		$id = $this->budget_mod->budget_category_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been updated!');
		redirect('budget/budgetCategoryEdit/'.strtr($this->encryption->encrypt($data_post['id']), '+=/', '.-~'));
	}

	public function budgetCategoryDeleteProcess($id){
		if($this->permission_cookie[10] == 0){
			redirect('auth/logout');
		}
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		$where['id'] = $id;
		$form_data = array(
			'status_delete' => 0,
		);
		$id = $this->budget_mod->budget_category_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been deleted!');
		redirect('budget/budgetCategoryList/');
	}

	public function load_account($assetable){
		$where['assetable'] = $assetable;
		$where['status_delete'] = 1;
		$data	= $this->budget_mod->budget_category_list(null, $where);

		echo json_encode($data);
	}
}