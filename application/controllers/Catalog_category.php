<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Catalog_category extends CI_Controller {
    public function __construct() {
            
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();
        $this->load->helper('cookies');
        helper_cookies();

        $this->load->model('home_mod'); 
        $this->load->model('general_mod');
        $this->load->model('engineering_mod');
        $this->load->model('mrir_mod');

        $this->load->model('catalog_category_mod');

        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar = "user/sidebar";
    }

    function index(){
        redirect('catalog_category_list');
    }

    function vendor_check($param){

        if($param == 'vendor_number'){
            $vendor_number = $this->input->post('vendor_number');

            //CHECK VENDOR NUMBER -----------------------------
            $where['vendor_no'] = $vendor_number;
            $datadb = $this->vendor_mod->getAll($where);
            unset($where);

            $hasil = 0;
            if(sizeof($datadb) != 0){
                $hasil = 1;
            }
            //-------------------------------------------------

            echo json_encode(array(
                'hasil' => $hasil
            ));
        }
    }

    function catalog_category_list(){
        $data['read_cookies']       = $this->user_cookie;
        $data['meta_title']         = 'Account Description List';
        $data['subview']            = 'catalog_category/catalog_category_list';
        $data['sidebar']            = $this->sidebar;
        $data['read_permission']    = $this->permission_cookie;

        if($this->user_cookie[10] == 0){
            $this->session->set_flashdata('message','<script type="text/javascript">swal.fire({title: "Sorry!",text: "Your ID is not registered for any project.\n\nPlease call Portal Administrator to get support for this issue!",type: "warning"}).then(function() { window.location = "'.$this->user_cookie[9].'"});</script>');
        }   

        $this->load->view('index', $data);
    }

    function catalog_category_add(){
        $data['read_cookies']     = $this->user_cookie;
        $data['meta_title']       = 'Add New Account Description';
        $data['subview']          = 'catalog_category/catalog_category_new';
        $data['sidebar']          = $this->sidebar;
        $data['read_permission']  = $this->permission_cookie;
        
        $this->load->view('index', $data);
    }

    function catalog_category_add_process(){
        $user_id        = $this->user_cookie[0];
        $name           = $this->input->post('name');

        $form_data = array(
            'category_name' => $name,
            'created_by'    => $user_id,
            'created_date'  => date('Y-m-d H:i:s'),
            'status_delete' => 1
        );

        $this->catalog_category_mod->catalog_category_add($form_data);
        $this->session->set_flashdata('success', 'Your data has been Created!');
        redirect('catalog_category/catalog_category_list');
    }

    function catalog_category_edit($id = null){

        $id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

        if(empty($id)){ redirect('catalog_category/catalog_category_list'); }

        $where['id'] = $id;
        $data['catalog_category_list'] = $this->catalog_category_mod->catalog_category_list($where);
        unset($where);

        $data['read_cookies']     = $this->user_cookie;
        $data['meta_title']       = 'Edit Account Description';
        $data['subview']          = 'catalog_category/catalog_category_detail';
        $data['sidebar']          = $this->sidebar;
        $data['read_permission']  = $this->permission_cookie;
        
        $this->load->view('index', $data);
    }

    function catalog_category_edit_process(){
        $id             = $this->input->post('id');
        $name           = $this->input->post('name');

        $form_data = array(
            'category_name'  => $name,
        );

        $where['id'] = $id;
        $this->catalog_category_mod->catalog_category_edit($form_data, $where);

        $this->session->set_flashdata('success', 'Your data has been Updated!');
        redirect('catalog_category/catalog_category_list');
    }

    function vendor_delete_process(){       
        $id = $this->input->post('id');

        $form_data = array(
            'status' => 0
        );

        $where['id_vendor'] = $id;
        $this->vendor_mod->vendor_edit($form_data, $where);
        unset($where);
    }

    function pic_autocomplete(){
        if (isset($_GET['term'])){
            $result = $this->vendor_mod->search_pic_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['badge_no'].' - '.$row['full_name'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "User Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    //RECEIVING DETAIL LIST DATATABLE ----------------------------------------------------------------------------------
    function catalog_category_list_json(){
        error_reporting(0);
 
        $list = $this->catalog_category_mod->get_datatables_catalog_category_list_dt();
        
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {           
            $no++;
            $row   = array();
            
            $links = base_url()."catalog_category/catalog_category_edit/".strtr($this->encryption->encrypt($list->id), '+=/', '.-~');

            $row[] = $list->category_name;
            $row[] = "<a href='".$links."' class='btn btn-warning text-white' title='Edit'><i class='fa fa-edit'></i> Edit </a>&nbsp; 
                      <button class='btn btn-danger text-white' title='Delete' onclick='delete_action(".$list->id.")'><i class='fas fa-trash'></i> Delete </button>&nbsp;
                     ";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->catalog_category_mod->count_all_catalog_category_list_dt(),
            "recordsFiltered" => $this->catalog_category_mod->count_filtered_catalog_category_list_dt(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
    //-----------------------------------------------------------------------------------------------------------
}