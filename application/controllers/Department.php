<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('department_mod');
		$this->load->model('general_mod');

		$this->user_cookie 		 = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));

		$this->sidebar = "user/sidebar";
	}

	public function index(){
		redirect('department/departmentList');
	}

	public function departmentList(){
		$data['all_list']	   			= $this->department_mod->all_list();

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Department List';
		$data['subview']					= 'department/department_list';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function departmentDetailNew($filter = NULL){
		$filter = explode("-", $filter);

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$data['filter']	   				= $filter;
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'New Department';
		$data['subview']					= 'department/department_new';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function departmentDetailNewProcess(){
		if($this->permission_cookie[96] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$form_data = array(
			'name_of_department' => $data_post['name_of_department'],
		);
		$id = $this->department_mod->department_detail_new_process($form_data);

		$this->session->set_flashdata('success', 'Your data has been inserted!');
		redirect('department/departmentList/');
	}

	public function departmentDetailEdit($id){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
		$datadb	  					= $this->department_mod->all_list($id);
		$data['department']	  	= $datadb[0];

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'New Department';
		$data['subview']					= 'department/department_edit';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function departmentDetailEditProcess(){
		if($this->permission_cookie[97] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$where['id_department'] = $data_post['id_department'];
		$form_data = array(
			'name_of_department' => $data_post['name_of_department'],
			'status' 		=> $data_post['status'],
		);
		$id = $this->department_mod->department_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been updated!');
		redirect('department/departmentDetailEdit/'.strtr($this->encryption->encrypt($data_post['id_department']), '+=/', '.-~'));
	}

	public function departmentDetailDeleteProcess($id_department){
		if($this->permission_cookie[98] == 0){
			redirect('auth/logout');
		}
		$id_department = $this->encryption->decrypt(strtr($id_department, '.-~', '+=/'));

		$datadb	  	= $this->department_mod->all_list($id_department);
		$department	  	= $datadb[0];

		$where['id_department'] = $id_department;
		$form_data = array(
			'status' => 0,
		);
		$id = $this->department_mod->department_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been deleted!');
		redirect('department/departmentList/');
	}
}