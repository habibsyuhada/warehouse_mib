<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Engineering extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('home_mod');
		$this->load->model('general_mod');
		$this->load->model('engineering_mod');
		$this->load->model('mrir_mod');

		$this->user_cookie = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_qcs'));
		$this->sidebar = "mto/sidebar";
	}

	public function index(){

		redirect('engineering/draw_list');

	}

	public function draw_new(){

		$data['module_list']  	  = $this->general_mod->eng_module_get_db();
		$data['discipline_list']  = $this->general_mod->eng_discipline_get_db();
		$data['material_class']   = $this->general_mod->material_class_get_db($this->user_cookie[10]);
		$data['read_cookies'] 	  = $this->user_cookie;
		$data['meta_title'] 	  = 'Add New Drawing';
		$data['subview']    	  = 'engineering/drawing/draw_new';
		$data['sidebar']    	  = $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);

	}

	public function draw_new_process(){
		$user_id 				= $this->user_cookie[0];
		$project_id 			= $this->user_cookie[10];
		$drawing_no 			= $this->input->post('drawing_no');
		$sheet_no 				= $this->input->post('sheet_no');
		$module 				= $this->input->post('module');
		$discipline 			= $this->input->post('discipline');
		$material_class 		= join("; ", $this->input->post('material_class'));
		$received_date 			= date('Y-m-d', strtotime($this->input->post('received_date')));
		$description 			= $this->input->post('description');
		$is_imported 			= 0;

		$form_data = array(
			'user_id' => $user_id,
			'project_id' => $project_id,
			'drawing_no' => $drawing_no,
		  	'sheet_no' => $sheet_no,
		  	'module' => $module,
		  	'discipline' => $discipline,
		  	'material_class' => $material_class,
		  	'received_date' => $received_date,
		  	'descriptions' => $description,
		  	'is_imported' => $is_imported,
		);
		$this->engineering_mod->draw_new_process_db($form_data);
		$this->session->set_flashdata('success', 'Your data has been Created!');

		redirect('engineering/draw_list');
	}

	public function draw_list(){

		
		$datadb = $this->general_mod->eng_module_get_db();
		foreach ($datadb as $value) {
			$data['module_list'][$value['mod_id']] = $value['mod_desc'];
		}
		$datadb  = $this->general_mod->eng_discipline_get_db();
		foreach ($datadb as $value) {
			$data['discipline_list'][$value['id']] = $value['discipline_name'];
		}
		$datadb  = $this->general_mod->material_class_get_db();
		foreach ($datadb as $value) {
			$data['material_class'][$value['id']] = $value['material_class'];
		}

	

		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'Drawing List';
		$data['subview']    		= 'mto/engineering/drawing/draw_list';
		$data['sidebar']    		= $this->sidebar;
		$data['read_permission']  	= $this->permission_cookie;

		if($this->user_cookie[10] == 0){

			$this->session->set_flashdata('message','<script type="text/javascript">swal.fire({title: "Sorry!",text: "Your ID is not registered for any project.\n\nPlease call Portal Administrator to get support for this issue!",type: "warning"}).then(function() { window.location = "'.$this->user_cookie[9].'"});</script>');
			
		}	

		$this->load->view('index', $data);
	}

	public function draw($drawing_no = null){
		$drawing_no = $this->encryption->decrypt(strtr($drawing_no, '.-~', '+=/'));

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		if(empty($drawing_no)){ redirect('engineering/draw_list');}

		$data['module_list']  		= $this->general_mod->eng_module_get_db();
		$data['discipline_list']  	= $this->general_mod->eng_discipline_get_db();

		$data['material_class']  	= $this->general_mod->material_class_get_db($this->user_cookie[10]);
		$data['material_grade'] 	= $this->general_mod->material_grade_get_db(null,$this->user_cookie[10]);

		$where['drawing_no'] 	 = $drawing_no;
		$data['draw_list']  	 = $this->engineering_mod->draw_get_db($where);

		unset($where);
		$where['drawing_no'] 	 = $drawing_no;
		$where['mark_delete'] 	 = 0;
		$data['peacemark_list']  = $this->engineering_mod->peacemark_get_db($where);

		unset($where);
		$where['drawing_no'] 	= $drawing_no;
		$where['mark_delete'] 	= 0;
		$data['joint_list']  	= $this->engineering_mod->joint_get_db($where);
		
		unset($where);
		$where['drawing_no'] 	= $drawing_no;
		$data['revision_list']  = $this->engineering_mod->revision_get_db($where);
		
		if($this->input->get('t')){
			$data['t'] 	= $this->input->get('t');
		}else{
			$data['t'] 	= '';
		}

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= 'Drawing '.$drawing_no;
		$data['weld_type']  		= $this->engineering_mod->getWeldtypeData();
		$data['class_list']  				= $this->engineering_mod->getClassData();
		//print_r($data['weld_type']);
		//return false;
		$data['joint_type']  		= $this->engineering_mod->getJointTypeData();
		$data['subview']    		= 'mto/engineering/drawing/draw_detail';
		$data['sidebar']    		= $this->sidebar;
		
		$this->load->view('index', $data);
	}

	public function draw_edit_process(){

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$id 				= $this->input->post('id');
		$drawing_no 		= $this->input->post('drawing_no');
		$sheet_no 			= $this->input->post('sheet_no');
		$module 			= $this->input->post('module');
		$discipline 		= $this->input->post('discipline');
		$material_class 	= join("; ", $this->input->post('material_class'));
		$received_date 		= date('Y-m-d', strtotime($this->input->post('received_date')));
		$description 		= $this->input->post('description');
		$is_imported 		= 0;

		$form_data = array(
		  'drawing_no' => $drawing_no,
		  'sheet_no' => $sheet_no,
		  'module' => $module,
		  'discipline' => $discipline,
		  'material_class' => $material_class,
		  'received_date' => $received_date,
		  'descriptions' => $description,
		);

		$where['id'] = $id;

		$this->engineering_mod->draw_edit_process_db($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been Updated!');
		
		$drawing_no = strtr($this->encryption->encrypt($drawing_no), '+=/', '.-~');
		redirect('engineering/draw/'.$drawing_no);
	}

	public function draw_delete_process($drawing_no){		

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$form_data = array(
			'status' => 0,
		);
		$where['drawing_no'] = $drawing_no;
		$this->engineering_mod->draw_delete_process_db($where);

		$this->session->set_flashdata('success', 'Your data has been Deleted!');
		redirect('engineering/draw_list/');
	}

	// Drawing Piace Mark 
	public function piecemark_new_process(){

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$drawing_no 		= $this->input->post('drawing_no');
		$piece_mark_no 		= $this->input->post('piece_mark_no');
		$material_grade 	= $this->input->post('material_grade');
		$diameter 			= $this->input->post('diameter');
		$sch 				= $this->input->post('sch');
		$length 			= $this->input->post('length');
		$width 				= $this->input->post('width');
		$weight 			= $this->input->post('weight');
		$test_pack_no 		= $this->input->post('test_pack_no');
		$mark_delete 		= 0;
		$remarks 			= $this->input->post('remarks');
		$project_id 		= 1;
		$can_number		 	= $this->input->post('can_number');

		$form_data = array(
			'drawing_no' => $drawing_no,
			'piece_mark_no' => $piece_mark_no,
			'material_grade' => $material_grade,
			'diameter' => $diameter,
			'sch' => $sch,
			'length' => $length,
			'width' => $width,
			'weight' => $weight,
			'test_pack_no' => $test_pack_no,
			'mark_delete' => $mark_delete,
			'remarks' => $remarks,
			'project_id' => $project_id,
			'can_number' => $can_number
		);
		$this->engineering_mod->piecemark_new_process_db($form_data);
		$this->session->set_flashdata('success', 'Your data has been Created!');

		$drawing_no = strtr($this->encryption->encrypt($drawing_no), '+=/', '.-~');
		redirect('engineering/draw/'.$drawing_no.'?t=pc');
	}

	public function piecemark_delete_process($id, $drawing_no){		
		$drawing_no = $this->encryption->decrypt(strtr($drawing_no, '.-~', '+=/'));

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$form_data = array(
			'mark_delete' => 1,
		);
		$where['id'] = $id;
		$this->engineering_mod->peacemark_edit_process_db($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been Deleted!');

		$drawing_no = strtr($this->encryption->encrypt($drawing_no), '+=/', '.-~');
		redirect('engineering/draw/'.$drawing_no.'?t=pc');
	}

	public function peacemark_edit_process(){		

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$col 			= $this->input->post('col');
		$id 			= $this->input->post('id');
		$value 		= $this->input->post('value');

		$form_data = array(
			$col => $value,
		);
		$where['id'] = $id;
		$this->engineering_mod->peacemark_edit_process_db($form_data, $where);
	}

	// Joint Drawing
	public function joint_new_process(){

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$drawing_no 			= $this->input->post('drawing_no');
		$weld_map 				= $this->input->post('weld_map');
		$joint_no 				= $this->input->post('joint_no');
		$weld_type 				= $this->input->post('weld_type');
		$diameter 				= $this->input->post('diameter');
		$sch 					= $this->input->post('sch');
		$length 				= $this->input->post('length');
		$joint_type 			= $this->input->post('joint_type');
		$test_pack_no 			= $this->input->post('test_pack_no');
		$mark_delete 			= 0;
		$remarks 				= $this->input->post('remarks');
		$project_id 			= 1;
		$wps_group 				= $this->input->post('wps_group');
		$spool_no 				= $this->input->post('spool_no');
		$class 					= $this->input->post('class');
		$can_number				= $this->input->post('can_number');
		$form_data = array(
			'drawing_no' => $drawing_no,
			'weld_map' => $weld_map,
			'joint_no' => $joint_no,
			'weld_type' => $weld_type,
			'diameter' => $diameter,
			  'sch' => $sch,
			  'length' => $length,
			  'joint_type' => $joint_type,
			  'test_pack_no' => $test_pack_no,
			  'mark_delete' => $mark_delete,
			  'remarks' => $remarks,
			  'project_id' => $project_id,
			  'wps_group' => $wps_group,
			  'spool_no' => $spool_no,
			  'class' => $class,
			  'can_number' => $can_number
		);
		$this->engineering_mod->joint_new_process_db($form_data);
		$this->session->set_flashdata('success', 'Your data has been Created!');

		$drawing_no = strtr($this->encryption->encrypt($drawing_no), '+=/', '.-~');
		redirect('engineering/draw/'.$drawing_no.'?t=j');
	}

	public function joint_delete_process($id, $drawing_no){		
		$drawing_no = $this->encryption->decrypt(strtr($drawing_no, '.-~', '+=/'));

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$form_data = array(
			'mark_delete' => 1,
		);
		$where['id'] = $id;
		$this->engineering_mod->joint_edit_process_db($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been Deleted!');

		$drawing_no = strtr($this->encryption->encrypt($drawing_no), '+=/', '.-~');
		redirect('engineering/draw/'.$drawing_no.'?t=j');
	}

	public function joint_edit_process(){	

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$col 			= $this->input->post('col');
		$id 			= $this->input->post('id');
		$value 		= $this->input->post('value');

		$form_data = array(
			$col => $value,
		);
		$where['id'] = $id;
		$this->engineering_mod->joint_edit_process_db($form_data, $where);
	}

	// Drawing Revision
	public function revision_new_process(){

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}
   		$drawing_no = $this->encryption->decrypt(strtr($this->input->post('drawing_no'), '.-~', '+=/'));
		$id_user					= $this->user_cookie;

		//$drawing_no 			= $this->input->post('drawing_no');
		$id 			= $this->input->post('id');
		$rev_no 					= $this->input->post('rev_no');
		$remarks 					= $this->input->post('remarks');
		$project_id 			= 1;
		$name_file 				= $id_user[0].'-'.$id.'-'.date('YmdHis');

		$config['upload_path']          = 'upload/rev';
		$config['file_name']            = $name_file;
		$config['allowed_types']        = 'pdf';
		$config['max_size']        			= '2000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());

			$drawing_no = strtr($this->encryption->encrypt($drawing_no), '+=/', '.-~');
			redirect('engineering/draw/'.$drawing_no.'?t=r');
			return false;
		}

		$form_data = array(
			'project_id' => $project_id,
			'drawing_no' => $drawing_no,
			'rev_no' => $rev_no,
			'attachment' => $this->upload->data('file_name'),
			'remarks' => $remarks,
		);

		$this->engineering_mod->revision_new_process_db($form_data);
		$this->session->set_flashdata('success', 'Your revision has been Inserted!');

		$drawing_no = strtr($this->encryption->encrypt($drawing_no), '+=/', '.-~');
		redirect('engineering/draw/'.$drawing_no.'?t=r');
	}

	// Import Drawing
	public function draw_import(){

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$data['read_cookies'] 	    = $this->user_cookie;
		$data['meta_title'] 	    = 'Import Drawing';
		$data['subview']    	    = 'engineering/drawing/draw_import';
		$data['sidebar']    	 	= $this->sidebar;
		$data['read_permission']    = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function draw_import_process(){
		$data = $this->general_mod->eng_module_get_db();
		foreach ($data as $value) {
			$module_list[$value['mod_desc']] = $value['mod_id'];
		}
		$data  = $this->general_mod->eng_discipline_get_db();
		foreach ($data as $value) {
			$discipline_list[$value['initial']] = $value['id'];
		}
		$data  = $this->general_mod->material_class_get_db();
		foreach ($data as $value) {
			$material_class_list[$value['material_initial']] = $value['id'];
		}

		$drawing_no 						= $this->input->post('drawing_no');
		$sheet_no 							= $this->input->post('sheet_no');
		$module 								= $this->input->post('module');
		$discipline 						= $this->input->post('discipline');
		$received_date 					= $this->input->post('received_date');
		$descriptions 					= $this->input->post('descriptions');
		$user_id 								= $this->user_cookie[0];
		$project_id 						= $this->user_cookie[10];

		if(count($drawing_no) > 0){
			$form_data = array();
			foreach ($drawing_no as $key => $value) {
				array_push($form_data, array(
					'drawing_no'=>$drawing_no[$key], // Insert data nis dari kolom A di excel
					'sheet_no'=>$sheet_no[$key], // Insert data nis dari kolom A di excel
					'module'=>(isset($module_list[$module[$key]]) ? $module_list[$module[$key]] : '-'), // Insert data nama dari kolom B di excel
					'discipline'=>(isset($discipline_list[$discipline[$key]]) ? $discipline_list[$discipline[$key]] : '-'), // Insert data jenis kelamin dari kolom C di excel
					
					'received_date'=>date('Y-m-d', strtotime($received_date[$key])), // Insert data alamat dari kolom D di excel
					'descriptions'=>$descriptions[$key], // Insert data alamat dari kolom D di excel
					'user_id'=>$user_id, // Insert data alamat dari kolom D di excel
					'project_id'=>$project_id, // Insert data alamat dari kolom D di excel
				));
			}
			$this->engineering_mod->draw_new_import_process_db($form_data);
			$this->session->set_flashdata('success', 'Your data has been imported!');
			redirect("engineering/draw_import");
		}
		else{
			$this->session->set_flashdata('error', 'No Data to import!');
			redirect("engineering/draw_import");
		}
	}

	public function peacemark_import_process(){
		$drawing_no 					= $this->input->post('drawing_no');
		$piece_mark_no 					= $this->input->post('piece_mark_no');
		$material_grade 				= $this->input->post('material_grade');
		$diameter 						= $this->input->post('diameter');
		$sch 							= $this->input->post('sch');
		$length 						= $this->input->post('length');
		$weight 						= $this->input->post('weight');
		$can_number						= $this->input->post('can_number');
		$test_pack_no 					= $this->input->post('test_pack_no');
		$remarks 						= $this->input->post('remarks');
		$project_id 					= 1;

		if(count($drawing_no) > 0){
			$form_data = array();
			foreach ($drawing_no as $key => $value) {
				array_push($form_data, array(
					'drawing_no'=>$drawing_no[$key], // Insert data nis dari kolom A di excel
					'piece_mark_no'=>$piece_mark_no[$key], // Insert data nis dari kolom A di excel
					'material_grade'=>$material_grade[$key], // Insert data nis dari kolom A di excel
					'diameter'=>$diameter[$key], // Insert data nis dari kolom A di excel
					'sch'=>$sch[$key], // Insert data nis dari kolom A di excel
					'length'=>$length[$key], // Insert data nis dari kolom A di excel
					'weight'=>$weight[$key], // Insert data nis dari kolom A di excel
					'can_number'=>$can_number[$key],
					'test_pack_no'=>$test_pack_no[$key], // Insert data nis dari kolom A di excel
					'remarks'=>$remarks[$key], // Insert data nis dari kolom A di excel
					'project_id'=>$project_id, // Insert data alamat dari kolom D di excel
				));
			}

			$this->engineering_mod->peacemark_new_import_process_db($form_data);
			$this->session->set_flashdata('success', 'Your data has been imported!');
			redirect("engineering/draw_import");
		}
		else{
			$this->session->set_flashdata('error', 'No Data to import!');
			redirect("engineering/draw_import");
		}

		
	}

	public function joint_import_process(){
		$drawing_no 						= $this->input->post('drawing_no');
		$weld_map 							= $this->input->post('weld_map');
		$joint_no 							= $this->input->post('joint_no');
		$weld_type 							= $this->input->post('weld_type');
		$diameter 							= $this->input->post('diameter');
		$sch 										= $this->input->post('sch');
		$length 								= $this->input->post('length');
		$joint_type 						= $this->input->post('joint_type');
		$test_pack_no 					= $this->input->post('test_pack_no');
		$wps_group 							= $this->input->post('wps_group');
		$spool_no 							= $this->input->post('spool_no');
		$remarks 								= $this->input->post('remarks');


		//print_r(count(array_unique($joint_no)));
		//print_r(count($drawing_no));

		if(count(array_unique($joint_no)) == count($drawing_no)){

			if(count($drawing_no) > 0){
				$form_data = array();
				foreach ($drawing_no as $key => $value) {
					array_push($form_data, array(
						'drawing_no'=>$drawing_no[$key], // Insert data nis dari kolom A di excel
						'weld_map'=>$weld_map[$key], // Insert data nis dari kolom A di excel
						'joint_no'=>$joint_no[$key], // Insert data nis dari kolom A di excel
						'weld_type'=>$weld_type[$key], // Insert data nis dari kolom A di excel
						'diameter'=>$diameter[$key], // Insert data nis dari kolom A di excel
						'sch'=>$sch[$key], // Insert data nis dari kolom A di excel
						'length'=>$length[$key], // Insert data nis dari kolom A di excel
						'joint_type'=>$joint_type[$key], // Insert data nis dari kolom A di excel
						'test_pack_no'=>$test_pack_no[$key], // Insert data nis dari kolom A di excel
						'wps_group'=>$wps_group[$key], // Insert data nis dari kolom A di excel
						'spool_no'=>$spool_no[$key], // Insert data nis dari kolom A di excel
						'remarks'=>$remarks[$key], // Insert data nis dari kolom A di excel
						'mark_delete'=>0, // Insert data nis dari kolom A di excel
					));
				}

		

			$this->engineering_mod->joint_new_import_process_db($form_data);
			$this->session->set_flashdata('success', 'Your data has been imported!');
			redirect("engineering/draw_import");

		} else{
			$this->session->set_flashdata('error', 'No Data to import!');
			redirect("engineering/draw_import");
		}

		} else {

			$this->session->set_flashdata('error', 'Duplicate Joint on your excel, Please check again!');
			redirect("engineering/draw_import");

		}

		

	}

	public function draw_preview(){
		$datadb = $this->general_mod->eng_module_get_db();
		foreach ($datadb as $value) {
			$data['module_list'][$value['mod_desc']] = $value['mod_id'];
		}
		$datadb  = $this->general_mod->eng_discipline_get_db();
		foreach ($datadb as $value) {
			$data['discipline_list'][$value['initial']] = $value['id'];
		}
		$datadb  = $this->general_mod->material_class_get_db();
		foreach ($datadb as $value) {
			$data['material_class'][$value['material_initial']] = $value['id'];
		}

		$id_user = $this->user_cookie;

		$config['upload_path']          = 'file/draw/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 						= TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("engineering/draw_import");
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/draw/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data_check = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			
			if($numrow > 1){
				$data_check[]=$row['A'];
			}
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		$draw_duplicate		= $this->engineering_mod->draw_get_db(null,$data_check);
		$d = array();
		foreach ($draw_duplicate as $value) {
			$d[] = $value['drawing_no'];
		}
		$data['draw_duplicate']		= $d;
		$data['sheet']						= $sheet;

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Draw Preview';
		$data['subview']    	= 'engineering/drawing/draw_preview';
		$data['sidebar']    	= $this->sidebar;
		$this->load->view('index', $data);
	}

	public function peacemark_preview(){
		$datadb  = $this->general_mod->material_grade_get_db();
		foreach ($datadb as $value) {
			$data['material_grade'][$value['id']] = $value['material_grade'];
		}

		$id_user = $this->user_cookie;

		$config['upload_path']          = 'file/draw/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 						= TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("engineering/draw_import");
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/draw/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data_check = array();
		
		$numrow = 1;
		$piecemark_duplicate = array();
		foreach($sheet as $row){
			
			if($numrow > 1){
				$data_check[]=$row['A'];
			}

			$where['piece_mark_no'] = $row['B'];
			$where['drawing_no'] 		= $row['A'];
			$where['mark_delete'] 	= 0;
			$datadb									= $this->engineering_mod->peacemark_get_db($where);
			if($datadb){
				$datadb								= $datadb[0];
				$piecemark_duplicate[$datadb['drawing_no']][]=	$datadb['piece_mark_no'];
			}

			unset($where);
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		$draw_duplicate		= $this->engineering_mod->draw_get_db(null,$data_check);
		$d = array();
		foreach ($draw_duplicate as $value) {
			$d[] = $value['drawing_no'];
		}
		$data['draw_duplicate']					= $d;
		$data['piecemark_duplicate']		= $piecemark_duplicate;
		$data['sheet']									= $sheet;

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Peacemark Preview';
		$data['subview']    	= 'engineering/drawing/peacemark_preview';
		$data['sidebar']    	= $this->sidebar;
		$this->load->view('index', $data);
	}

	public function joint_preview(){

		$id_user = $this->user_cookie;

		$config['upload_path']          = 'file/draw/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 						= TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("engineering/draw_import");
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/draw/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data_check = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			
			if($numrow > 1){
				$data_check[]=$row['A'];
			}
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		$numrow = 1;
		$joint_duplicate = array();
		foreach($sheet as $row){
			
			if($numrow > 1){
				$data_check[]=$row['A'];
			}

			$where['joint_no'] 			= $row['C'];
			$where['drawing_no'] 		= $row['A'];
			$where['mark_delete'] 	= 0;
			$datadb									= $this->engineering_mod->joint_get_db($where);
			if($datadb){
				$datadb								= $datadb[0];
				$joint_duplicate[$datadb['drawing_no']][] =	$datadb['joint_no'];
			}

			unset($where);
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		$draw_duplicate		= $this->engineering_mod->draw_get_db(null,$data_check);
		$d = array();
		foreach ($draw_duplicate as $value) {
			$d[] = $value['drawing_no'];
		}
		$data['draw_duplicate']			= $d;
		$data['joint_duplicate']		= $joint_duplicate;
		$data['sheet']							= $sheet;

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Joint Preview';
		$data['subview']    	= 'engineering/drawing/joint_preview';
		$data['sidebar']    	= $this->sidebar;
		$this->load->view('index', $data);
	}

	// Draw Report
	public function draw_report(){
		$data['draw_list']  = array();
		if($this->input->get('df')!==NULL && $this->input->get('dt')!==NULL){
			$data['df']  = $this->input->get('df');
			$data['dt']  = $this->input->get('dt');
			if($this->input->get('draw_excel')!==NULL){
				redirect("engineering/draw_excel/".$data['df']."/".$data['dt']);
			}
			if($this->input->get('peacemark_excel')!==NULL){
				redirect("engineering/peacemark_excel/".$data['df']."/".$data['dt']);
			}
			if($this->input->get('joint_excel')!==NULL){
				redirect("engineering/joint_excel/".$data['df']."/".$data['dt']);
			}

			$where['received_date >='] = date('Y-m-d', strtotime($data['df']));
			$where['received_date <='] = date('Y-m-d', strtotime($data['dt']));
			$data['draw_list']  = $this->engineering_mod->draw_get_db($where);
		}
		$datadb = $this->general_mod->eng_module_get_db();
		foreach ($datadb as $value) {
			$data['module_list'][$value['mod_id']] = $value['mod_desc'];
		}
		$datadb  = $this->general_mod->eng_discipline_get_db();
		foreach ($datadb as $value) {
			$data['discipline_list'][$value['id']] = $value['discipline_name'];
		}
		$datadb  = $this->general_mod->material_class_get_db();
		foreach ($datadb as $value) {
			$data['material_class'][$value['id']] = $value['material_class'];
		}

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= 'Drawing Report';
		$data['subview']    		= 'engineering/drawing/draw_report';
		$data['sidebar']    		= $this->sidebar;
		$this->load->view('index', $data);
	}

	public function draw_excel($df, $dt){
		$df = date('Y-m-d', strtotime($df));
		$dt = date('Y-m-d', strtotime($dt));

		$datadb = $this->general_mod->eng_module_get_db();
		foreach ($datadb as $value) {
			$data['module_list'][$value['mod_id']] = $value['mod_desc'];
		}
		$datadb  = $this->general_mod->eng_discipline_get_db();
		foreach ($datadb as $value) {
			$data['discipline_list'][$value['id']] = $value['discipline_name'];
		}
		$datadb  = $this->general_mod->material_class_get_db();
		foreach ($datadb as $value) {
			$data['material_class'][$value['id']] = $value['material_class'];
		}

		$where['received_date >='] = $df;
		$where['received_date <='] = $dt;
		$data['draw_list']  = $this->engineering_mod->draw_get_db($where);

		$this->load->view('engineering/drawing/draw_excel', $data);
	}

	public function peacemark_excel($df, $dt){
		$df = date('Y-m-d', strtotime($df));
		$dt = date('Y-m-d', strtotime($dt));

		$datadb = $this->general_mod->eng_module_get_db();
		foreach ($datadb as $value) {
			$data['module_list'][$value['mod_id']] = $value['mod_desc'];
		}
		$datadb  = $this->general_mod->eng_discipline_get_db();
		foreach ($datadb as $value) {
			$data['discipline_list'][$value['id']] = $value['discipline_name'];
		}
		$datadb  = $this->general_mod->material_class_get_db();
		foreach ($datadb as $value) {
			$data['material_class'][$value['id']] = $value['material_class'];
		}
		$datadb  = $this->general_mod->material_grade_get_db();
		foreach ($datadb as $value) {
			$data['material_grade'][$value['id']] = $value['material_grade'];
		}

		$where['a.received_date >='] = $df;
		$where['a.received_date <='] = $dt;
		$data['draw_list']  = $this->engineering_mod->draw_report_peacemark_db($where);

		$this->load->view('engineering/drawing/peacemark_excel', $data);
	}

	public function joint_excel($df, $dt){
		$df = date('Y-m-d', strtotime($df));
		$dt = date('Y-m-d', strtotime($dt));

		$datadb = $this->general_mod->eng_module_get_db();
		foreach ($datadb as $value) {
			$data['module_list'][$value['mod_id']] = $value['mod_desc'];
		}
		$datadb  = $this->general_mod->eng_discipline_get_db();
		foreach ($datadb as $value) {
			$data['discipline_list'][$value['id']] = $value['discipline_name'];
		}
		$datadb  = $this->general_mod->material_class_get_db();
		foreach ($datadb as $value) {
			$data['material_class'][$value['id']] = $value['material_class'];
		}

		$where['a.received_date >='] = $df;
		$where['a.received_date <='] = $dt;
		$data['draw_list']  = $this->engineering_mod->draw_report_joint_db($where);

		$this->load->view('engineering/drawing/joint_excel', $data);
	}

	// Other

	//Drawing List DataTables 


	function drawing_list_json()
    {
    	error_reporting(0);
 
        $list = $this->engineering_mod->get_datatables_drawing_list_dt($this->user_cookie[10]);
        
        //return false
        // dd($type);
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {
        	$data_module 		  = $this->general_mod->eng_module_get_db($list->module);
			$data_discipline  	  = $this->general_mod->eng_discipline_get_db($list->discipline);
			$data_material_class  = $this->general_mod->material_class_get_db(null,$list->material_class);

			//print_r($data_module);

			$links = base_url()."engineering/draw/".strtr($this->encryption->encrypt($list->drawing_no), '+=/', '.-~');
			
            $no++;
            $row   = array();
            
            $row[] = $list->drawing_no;
            $row[] = $list->sheet_no;
            $row[] = $data_module[0]['mod_desc'];
            $row[] = $data_discipline[0]['discipline_name'];
            $row[] = $data_material_class[0]['material_class'];
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-file-alt'></i></a>";
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->engineering_mod->count_all_drawing_list_dt($this->user_cookie[10]),
            "recordsFiltered" => $this->engineering_mod->count_filtered_drawing_list_dt($this->user_cookie[10]),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }


//Drawing List DataTables 

}