<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		
		parent::__construct();
		$this->load->helper('browser');

		//check_browser();
		$this->load->helper('cookies');
		helper_cookies();
		cek_login($this->input->cookie('portal_user'));

		$this->load->model('home_mod');
		$this->load->model('general_mod');
		// $this->load->model('Mrir_mod');

		$this->user_cookie = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
	}

	public function index(){

		redirect('home/home');
	}

	public function home(){
		$data['read_cookies'] 	  	= $this->user_cookie;
		$data['meta_title'] 	  	= 'Home';
		$data['subview']    	  	= 'home/home';
		$data['read_permission']  	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function sidebarCollapse(){
		$sidebarCollapse['name'] = 'sidebarCollapse';
		$sidebarCollapse['expire'] = 86400*30;
		if($this->input->cookie('sidebarCollapse') !== NULL){
			$sidebarCollapse['value'] = ($this->input->cookie('sidebarCollapse') + 1) % 2 + 0;
		}
		else{
			$sidebarCollapse['value'] = 1;
		}
		$this->input->set_cookie($sidebarCollapse);	
	}
}