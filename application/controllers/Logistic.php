<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Logistic extends CI_Controller {
	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('home_mod');
		$this->load->model('general_mod');
		$this->load->model('engineering_mod');
		$this->load->model('mrir_mod');

		//FOR MTO ===================================
		$this->load->model('mto_mod');
		$this->load->model('material_catalog_mod');
		$this->load->model('mto_category_mod');
		//===========================================

		//RECEIVING ---------------------------------
		$this->load->model('receiving_mod');
		$this->load->model('mr_mod');
		//-------------------------------------------

		$this->user_cookie = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_qcs'));
		$this->sidebar = "receiving/sidebar";
	}

	function index(){
		redirect('receiving_list');
	}

	function po_autocomplete(){
		if (isset($_GET['term'])){
            $result = $this->receiving_mod->search_po_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['po_number'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "PO Not Found";
                echo json_encode($arr_result);
            }
        }
	}

	function receiving_list(){
		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'Receiving List';
		$data['subview']    		= 'receiving/receiving_list';
		$data['sidebar']    		= $this->sidebar;
		$data['read_permission']  	= $this->permission_cookie;

		if($this->user_cookie[10] == 0){

			$this->session->set_flashdata('message','<script type="text/javascript">swal.fire({title: "Sorry!",text: "Your ID is not registered for any project.\n\nPlease call Portal Administrator to get support for this issue!",type: "warning"}).then(function() { window.location = "'.$this->user_cookie[9].'"});</script>');
			
		}	

		$this->load->view('index', $data);
	}

	function receiving_add(){
		//MTO CATEGORY -----------------------------------------------------------
		$where['status_delete'] = 0; 
		$data['mto_category_list']   = $this->mto_mod->mto_category_get_db($where);
		unset($where);
		//------------------------------------------------------------------------

		//PROJECT CODE -----------------------------------------------------------
		$data['project_code']   = $this->general_mod->data_project(null);
		//------------------------------------------------------------------------

		$data['read_cookies'] 	  = $this->user_cookie;
		$data['meta_title'] 	  = 'Add New Receiving';
		$data['subview']    	  = 'receiving/receiving_new';
		$data['sidebar']    	  = $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);

	}

	function receiving_add_process(){
		$user_id 				= $this->user_cookie[0];

		$supply_number 			= $this->input->post('supply_number');
		$shipping_number 		= $this->input->post('shipping_number');
		$date_psa 				= date('Y-m-d', strtotime($this->input->post('date_psa')));
		$date_fsa 				= date('Y-m-d', strtotime($this->input->post('date_fsa')));
		$vendor 				= $this->input->post('vendor');
		$cipl 					= $this->input->post('cipl');
		$srn 					= $this->input->post('srn');
		$shipping_line 			= $this->input->post('shipping_line');
		$vessel_or_flight 		= $this->input->post('vessel_or_flight');
		$pol 					= $this->input->post('pol');
		$pod 					= $this->input->post('pod');
		$package_total 			= $this->input->post('package_total');
		$weight 				= $this->input->post('weight');
		$measurements 			= $this->input->post('measurements');

		//GP ======================================================
		$gp_20 					= $this->input->post('gp_20');
		$gp_40 					= $this->input->post('gp_40');
		$gp_45 					= $this->input->post('gp_45');
		//=========================================================

		//OT ======================================================
		$ot_20 					= $this->input->post('ot_20');
		$ot_40 					= $this->input->post('ot_40');
		//=========================================================

		//FR ======================================================
		$fr_20 					= $this->input->post('fr_20');
		$fr_40 					= $this->input->post('fr_40');
		//=========================================================

		$other 					= $this->input->post('other');
		$remarks 				= $this->input->post('remarks');

		//MATERIAL PACKAGE -----------
		$material_package 		= $this->input->post('material_package');
		$str_material_package	= '';
		for($i=0; $i<sizeof($material_package); $i++){
			$str_material_package .= $material_package[$i].' ';
		}

		$str_material_package = str_replace(' ', ';', $str_material_package);
		//----------------------------

		// PO ------------------------
		$po 					= $this->input->post('po');
		$str_po 				= '';
		for($i=0; $i<sizeof($po); $i++){
			$str_po .= $po[$i].' ';
		}

		$str_po = str_replace(' ', ';', $str_po);
		//----------------------------

		$form_data = array(
			'supply_number' 	=> $supply_number,
			'shipping_number' 	=> $shipping_number,
			'date_psa' 			=> $date_psa,
			'date_fsa' 			=> $date_fsa,
			'vendor' 			=> $vendor,
			'cipl' 				=> $cipl,
			'srn' 				=> $srn,
			'shipping_line' 	=> $shipping_line,
			'vessel_or_flight' 	=> $vessel_or_flight,
			'pol' 				=> $pol,
			'pod' 				=> $pod,
			'package_total' 	=> $package_total,
			'weight' 			=> $weight,
			'measurements' 		=> $measurements,
			'po_list' 			=> $str_po,
			'gp_20' 			=> $gp_20,
			'gp_40' 			=> $gp_40,
			'gp_45' 			=> $gp_45,
			'ot_20' 			=> $ot_20,
			'ot_40' 			=> $ot_40,
			'fr_20' 			=> $fr_20,
			'fr_40' 			=> $fr_40,
			'other' 			=> $other,
			'remarks' 			=> $remarks,
			'status' 			=> 1
		);

		// ETD ------------------------------
		if($this->input->post('etd')){
			$form_data['etd'] = date('Y-m-d', strtotime($this->input->post('etd')));
		}
		//-----------------------------------

		// ETA ------------------------------
		if($this->input->post('eta')){
			$form_data['eta'] = date('Y-m-d', strtotime($this->input->post('eta')));
		}
		//-----------------------------------

		// ATD ------------------------------
		if($this->input->post('atd')){
			$form_data['atd'] = date('Y-m-d', strtotime($this->input->post('atd')));
		}
		//-----------------------------------

		// ATA ------------------------------
		if($this->input->post('ata')){
			$form_data['ata'] = date('Y-m-d', strtotime($this->input->post('ata')));
		}
		//-----------------------------------

		$this->receiving_mod->receiving_new_process_db($form_data);
		$last_id = $this->db->insert_id();

		//ATTACHMENT --------------------------------------------
		self::add_document_receiving_process($last_id);
		//-------------------------------------------------------

		$this->session->set_flashdata('success', 'Your data has been Created!');

		redirect('receiving_list');
	}

	function add_document_receiving_process($last_id){
		$user_id 				= $this->user_cookie[0];
		$no_document 			= $this->input->post('no_document');
		$document_name 			= $this->input->post('document_name');

		if($document_name[0]){
			$data_attachment = array();
			for($i = 0; $i < sizeof($no_document); $i++){
				$name_file 						= $document_name[$i];

				$config['upload_path']          = 'file/receiving';
				$config['file_name']            = $name_file;
				$config['allowed_types']        = 'pdf';
				$config['max_size']        		= '2000';

				$this->load->library('upload', $config);

				$this->upload->initialize($config);

				if ( ! $this->upload->do_upload('receiving_document_'.$no_document[$i])){
					$this->session->set_flashdata('error', $this->upload->display_errors());

					$where['receiving_id'] = $last_id;
					$this->receiving_mod->receiving_delete_process_db($where);
					unset($where);

					redirect('receiving_list');
					return false;
				}

				array_push($data_attachment, array(
					'receiving_id' 	=> $last_id,
					'file_name' 	=> $this->upload->data('file_name'),
					'remarks'		=> $document_name[$i],
					'upload_by' 	=> $user_id,
					'upload_date'	=> date('Y-m-d H:i:s')
				));
			}

			$this->receiving_mod->receiving_attachment_add($data_attachment);
		}

	}

	function update_receiving_process(){
		$master_receiving_id 	= $this->input->post('id_receiving_master');
		$shipping_number 		= $this->input->post('shipping_number');
		$date_psa 				= date('Y-m-d', strtotime($this->input->post('date_psa')));
		$date_fsa 				= date('Y-m-d', strtotime($this->input->post('date_fsa')));
		$vendor 				= $this->input->post('vendor');
		$material_package 		= $this->input->post('material_package');
		$cipl 					= $this->input->post('cipl');
		$srn 					= $this->input->post('srn');
		$shipping_line 			= $this->input->post('shipping_line');
		$vessel_or_flight 		= $this->input->post('vessel_or_flight');
		$pol 					= $this->input->post('pol');
		$pod 					= $this->input->post('pod');
		$package_total 			= $this->input->post('package_total');
		$weight 				= $this->input->post('weight');
		$measurements 			= $this->input->post('measurements');
		// $date_received 			= date('Y-m-d', strtotime($this->input->post('date_received')));

		$gp_20 					= $this->input->post('gp_20');
		$gp_40 					= $this->input->post('gp_40');
		$gp_45 					= $this->input->post('gp_45');
		$ot_20 					= $this->input->post('ot_20');
		$ot_40 					= $this->input->post('ot_40');
		$fr_20 					= $this->input->post('fr_20');
		$fr_40 					= $this->input->post('fr_40');
		$other 					= $this->input->post('other');

		$form_data = array(
			'shipping_number' 	=> $shipping_number,
			'date_psa' 			=> $date_psa,
			'date_fsa' 			=> $date_fsa,
			'vendor' 			=> $vendor,
			'material_package' 	=> $material_package,
			'cipl' 				=> $cipl,
			'srn' 				=> $srn,
			'shipping_line' 	=> $shipping_line,
			'vessel_or_flight' 	=> $vessel_or_flight,
			'pol' 				=> $pol,
			'pod' 				=> $pod,
			'package_total' 	=> $package_total,
			'weight' 			=> $weight,
			'measurements' 		=> $measurements,
			'gp_20' 			=> $gp_20,
			'gp_40' 			=> $gp_40,
			'gp_45' 			=> $gp_45,
			'ot_20' 			=> $ot_20,
			'ot_40' 			=> $ot_40,
			'fr_20' 			=> $fr_20,
			'fr_40' 			=> $fr_40,
			'other' 			=> $other
		);

		// ETD ------------------------------
		if($this->input->post('etd')){
			$form_data['etd'] = date('Y-m-d', strtotime($this->input->post('etd')));
		}
		//-----------------------------------

		// ETA ------------------------------
		if($this->input->post('eta')){
			$form_data['eta'] = date('Y-m-d', strtotime($this->input->post('eta')));
		}
		//-----------------------------------

		// ATD ------------------------------
		if($this->input->post('atd')){
			$form_data['atd'] = date('Y-m-d', strtotime($this->input->post('atd')));
		}
		//-----------------------------------

		// ATA ------------------------------
		if($this->input->post('ata')){
			$form_data['ata'] = date('Y-m-d', strtotime($this->input->post('ata')));
		}
		//-----------------------------------

		$where['receiving_id'] = $master_receiving_id;
		$this->receiving_mod->receiving_edit_process_db($form_data, $where);
		unset($where);

		$this->session->set_flashdata('success', 'Data successful updated!');
        redirect("receiving_detail/".strtr($this->encryption->encrypt($master_receiving_id), '+=/', '.-~'));

	}


	//RECEIVING DETAIL ------------------------------------------------------------------------------------------
	function receiving_detail($id = null){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ 
			redirect('receiving');
		}

		$data['master_receiving_id'] = $id;

		if($this->input->get('t')){
			$data['t'] 	= $this->input->get('t');
		}else{
			$data['t'] 	= '';
		}

		//RECEIVING MASTER -------------------------------------------
		$where['receiving_id'] = $id;
		$receiving_list = $this->receiving_mod->receiving_data($where);
		$receiving_list = $receiving_list[0];
		unset($where);
		$title = 'Receiving '.$receiving_list['supply_number'];
		$data['receiving_list'] = $receiving_list;
		//------------------------------------------------------------

		//MTO --------------------------------------------------------
		$where['status'] = 1;
		$data['mr_list'] = $this->mr_mod->mr_list($where);
		unset($where);
		//------------------------------------------------------------

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= $title;
		$data['subview']    		= 'receiving/receiving_detail';
		$data['sidebar']    		= $this->sidebar;
		
		$this->load->view('index', $data);
	}

	//RECEIVING DETAIL PREVIEW ---------------
	function receiving_detail_preview(){

		$master_receiving_id = $this->input->post('master_receiving_id');

		$id_user = $this->user_cookie;
		$date = date('Y-m-d H:i:s');

		$config['upload_path']          = 'file/receiving/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 			= TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("receiving_detail/".strtr($this->encryption->encrypt($master_receiving_id), '+=/', '.-~'));
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/receiving/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data_check = array();
		
		$numrow = 1;

		//DECLARE ARRAY --------------
		$arr_material_catalog = [];
		$arr_unique_no = [];
		$arr_area = [];
		//---------------------------

		foreach($sheet as $row){
			
			if($numrow > 1){
				$data_check[] = $row['A'];
			}

			//MATERIAL CATALOG ===========================================================
			$sheet[$numrow]['id_material_catalog'] = '';
			$sheet[$numrow]['material_catalog'] = '';

			$where['short_desc'] = $row['A'];
			$datadb				 = $this->material_catalog_mod->getAll($where);
			unset($where);
			if($datadb){
				$datadb			 = $datadb[0];

				$arr_material_catalog[] 			= $datadb['short_desc']; //UNTUK DICEK MATERIAL CATALOGNYA

				$sheet[$numrow]['id_material_catalog']	= $datadb['id'];
				$sheet[$numrow]['material_catalog'] = $datadb['material'];
			}
			// ===========================================================================

			//UNIQUE NUMBER ==============================================================
			$where['uniq_no'] 	= $row['B'];
			$where['status'] 	= 1;
			$datadb 			= $this->receiving_mod->receiving_detail_data($where);
			if($datadb){
				$datadb 		= $datadb[0];
				$arr_unique_no[] 	= $datadb['uniq_no'];
			}
			unset($where);
			//============================================================================

			//AREA =======================================================================
			$sheet[$numrow]['id_area'] = '';
			$sheet[$numrow]['name_area'] = '';

			$where['area_name'] = $row['K'];
			$datadb 			= $this->general_mod->data_area($where);
			if($datadb){
				$datadb 		= $datadb[0];
				$arr_area[] 	= $datadb['area_name'];

				$sheet[$numrow]['id_area'] = $datadb['id'];
				$sheet[$numrow]['name_area'] = $datadb['area_name'];
			}
			unset($where);
			//============================================================================

			//SPEC CATEGORY ==============================================================
			$sheet[$numrow]['id_spec_category'] = '';
			$sheet[$numrow]['name_spec_category'] = '';

			$where['spec_code'] 	= $row['O'];
			$datadb 				= $this->general_mod->data_spec_category($where);
			if($datadb){
				$datadb 			= $datadb[0];
				$arr_spec_category[]= $datadb['spec_code'];

				$sheet[$numrow]['id_spec_category'] = $datadb['id'];
				$sheet[$numrow]['name_spec_category'] = $datadb['spec_desc'];
			}
			unset($where);
			//============================================================================

			$numrow++; // Tambah 1 setiap kali looping
		}

		$data['master_receiving_id'] 		= $master_receiving_id;
		$data['sheet']						= $sheet;

		$data['material_catalog_list'] 		= $arr_material_catalog;
		$data['unique_no_list'] 			= $arr_unique_no;
		$data['area_list'] 					= $arr_area;
		$data['spec_category_list'] 		= $arr_spec_category;

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Receiving Detail Preview';
		$data['subview']    	= 'receiving/receiving_preview';
		$data['sidebar']    	= $this->sidebar;
		$this->load->view('index', $data);
	}
	//----------------------------------------

	//RECEIVING DETAIL IMPORT ----------------
	function receiving_detail_import_process(){

        $master_receiving_id      	= $this->input->post('master_receiving_id');

        $user_id 					= $this->user_cookie[0];

        $material_catalog         	= $this->input->post('material_catalog');
        $unique_number 				= $this->input->post('unique_number');
        $po_item 					= $this->input->post('po_item');
        $heat_no					= $this->input->post('heat_no');
        $mill_certificate			= $this->input->post('mill_certificate');
        $country_origin				= $this->input->post('country_origin');
        $brand						= $this->input->post('brand');
        $uom						= $this->input->post('uom');
        $color_code					= $this->input->post('color_code');
        $valuta						= $this->input->post('valuta');
        $area						= $this->input->post('area');
        $location					= $this->input->post('location');
        $type						= $this->input->post('type');
        $spec						= $this->input->post('spec');
        $spec_category				= $this->input->post('spec_category');
        $plate_or_tag_no			= $this->input->post('plate_or_tag_no');
        $supplier_name				= $this->input->post('supplier_name');
        $ceq						= $this->input->post('ceq');
        $date_manufacturing			= $this->input->post('date_manufacturing');
        $category					= $this->input->post('category');
        $remarks					= $this->input->post('remarks');
        $status_receiving			= $this->input->post('status_receiving');

        //RECEIVING QTY -----------------------------------------------------------
        $qty						= $this->input->post('qty');
        $length						= $this->input->post('length');
        $unit_weight_length_sheet	= $this->input->post('unit_weight_length_sheet');
        $unit_weight_mm_sqm			= $this->input->post('unit_weight_mm_sqm');
        $total_weight_length_sheet	= $this->input->post('total_weight_length_sheet');
        $total_weight_mm_sqm		= $this->input->post('total_weight_mm_sqm');
        $value_mm_sqm				= $this->input->post('value_mm_sqm');
        $total_value_mm_sqm			= $this->input->post('total_value_mm_sqm');
        $total_value_length_sheet	= $this->input->post('total_value_length_sheet');
        //-------------------------------------------------------------------------

        if(count($material_catalog) > 0){
            $form_data = array();
            $form_data_quantity = array();
            foreach ($material_catalog as $key => $value) {

            	$rec_uniq_id = uniqid();

                array_push($form_data, array(
                    'receiving_id' 			=> $master_receiving_id, // Insert data nis dari kolom A di excel
                    'catalog_id'			=> $material_catalog[$key], // Insert data nis dari kolom A di excel
                    'mrir'					=> '', // Insert data nis dari kolom A di excel
                    'uniq_no'				=> $unique_number[$key],
                    'po_item'				=> $po_item[$key], // Insert data nis dari kolom A di excel
                    'heat_no'				=> $heat_no[$key], // Insert data nis dari kolom A di excel
                    'mill_certificate' 		=> $mill_certificate[$key], // Insert data nis dari kolom A di excel
                    'country_origin' 		=> $country_origin[$key], // Insert data nis dari kolom A di excel
                    'brand' 				=> $brand[$key], // Insert data nis dari kolom A di excel
                    'uom' 					=> $uom[$key], // Insert data nis dari kolom A di excel
                    'color_code' 			=> $color_code[$key], // Insert data nis dari kolom A di excel
                    'valuta' 				=> $valuta[$key], // Insert data nis dari kolom A di excel
	                'area' 					=> $area[$key], // Insert data nis dari kolom A di excel
                    'location' 				=> $location[$key], // Insert data nis dari kolom A di excel
                    'type' 					=> $type[$key], // Insert data nis dari kolom A di excel
                    'spec' 					=> $spec[$key], // Insert data nis dari kolom A di excel
                    'spec_category' 		=> $spec_category[$key], // Insert data nis dari kolom A di excel
                    'plate_or_tag_no' 		=> $plate_or_tag_no[$key], // Insert data nis dari kolom A di excel
                    'supplier_name' 		=> $supplier_name[$key], // Insert data nis dari kolom A di excel
                    'ceq' 					=> $ceq[$key], // Insert data nis dari kolom A di excel
                    'date_manufacturing' 	=> date('Y-m-d', strtotime($date_manufacturing[$key])), // Insert data nis dari kolom A di excel
                    'category' 				=> $category[$key], // Insert data nis dari kolom A di excel
                    'remarks' 				=> $remarks[$key], // Insert data nis dari kolom A di excel
                    'status_receiving' 		=> $status_receiving[$key], // Insert data nis dari kolom A di excel
                    'created_by' 			=> $user_id,
                    'created_date' 			=> date('Y-m-d H:i:s'),
                    'status' 				=> 1,
                    'rec_uniq_id' 			=> $rec_uniq_id
                ));

                array_push($form_data_quantity, array(
                    'receiving_id' 				=> $master_receiving_id, // Insert data nis dari kolom A di excel
                    'catalog_id'				=> $material_catalog[$key], // Insert data nis dari kolom A di excel
                    'qty'						=> $qty[$key],
                    'unit_weight_length_sheet'	=> @$unit_weight_length_sheet[$key], // Insert data nis dari kolom A di excel
                    'total_weight_length_sheet'	=> @$total_weight_length_sheet[$key], // Insert data nis dari kolom A di excel
                    'unit_weight_mm_sqm' 		=> @$unit_weight_mm_sqm[$key], // Insert data nis dari kolom A di excel
                    'total_weight_mm_sqm' 		=> @$total_weight_mm_sqm[$key], // Insert data nis dari kolom A di excel
                    'value_mm_sqm' 				=> @$value_mm_sqm[$key], // Insert data nis dari kolom A di excel
                    'total_value_length_sheet' 	=> @$total_value_length_sheet[$key], // Insert data nis dari kolom A di excel
                    'total_value_mm_sqm' 		=> @$total_value_mm_sqm[$key], // Insert data nis dari kolom A di excel
                    'status' 					=> 1,
                    'rec_uniq_id' 				=> $rec_uniq_id
                ));
            }

            $this->receiving_mod->receiving_detail_import_process_db($form_data);
            $this->receiving_mod->receiving_qty_import_process_db($form_data_quantity);

            $this->session->set_flashdata('success', 'Your data has been imported!');
            redirect("receiving_detail/".strtr($this->encryption->encrypt($master_receiving_id), '+=/', '.-~'));
        }
        else{
            $this->session->set_flashdata('error', 'No Data to import!');
            redirect("receiving_detail/".strtr($this->encryption->encrypt($master_receiving_id), '+=/', '.-~'));
        }

        
    }
    //----------------------------------------

    //RECEIVING DETAIL ADD PROCESS -----------
	function receiving_detail_add_process(){

		$master_receiving_id	= $this->input->post('master_receiving_id');
		$catalog_id 			= $this->input->post('mto_catalog');
		$mrir 					= '';
		$uniq_no 				= $this->input->post('unique_no');
		$po_item 				= $this->input->post('po_item');
		$heat_no 				= $this->input->post('heat_no');
		$mill_certificate 		= $this->input->post('mill_certificate');
		$country_origin 		= $this->input->post('country_origin');
		$brand 					= $this->input->post('brand');
		$uom 					= $this->input->post('uom');
		$color_code 			= $this->input->post('color_code');
		$valuta 				= $this->input->post('valuta');
		$area 					= $this->input->post('area');
		$location 				= $this->input->post('location');
		$type 					= $this->input->post('type');
		$spec 					= $this->input->post('spec');
		$spec_category 			= $this->input->post('spec_category');
		$plate_or_tag_no 		= $this->input->post('plate_or_tag_no');
		$supplier_name 			= $this->input->post('supplier_name');
		$ceq 					= $this->input->post('ceq');
		$date_manufacturing 	= date('Y-m-d', strtotime($this->input->post('date_manufacturing')));
		$category 				= $this->input->post('category');
		$remarks 				= $this->input->post('remarks');
		$status_receiving 		= $this->input->post('status_receiving');
		$created_by 			= $this->user_cookie[0];
		$created_date 			= date('Y-m-d H:i:s');

		$form_data = array(
			'receiving_id' 		=> $master_receiving_id,
			'catalog_id' 		=> $catalog_id,
			'mrir' 				=> '',
			'uniq_no' 			=> $uniq_no,
			'po_item' 			=> $po_item,
			'heat_no' 			=> $heat_no,
			'mill_certificate'	=> $mill_certificate,
			'country_origin'	=> $country_origin,
			'brand' 			=> $brand,
			'uom' 				=> $uom,
			'color_code' 		=> $color_code,
			'valuta' 			=> $valuta,
			'area' 				=> $area,
			'location' 			=> $location,
			'type' 				=> $type,
			'spec' 				=> $spec,
			'spec_category' 	=> $spec_category,
			'plate_or_tag_no' 	=> $plate_or_tag_no,
			'supplier_name' 	=> $supplier_name,
			'ceq' 				=> $ceq,
			'date_manufacturing'=> $date_manufacturing,
			'category' 			=> $category,
			'remarks' 			=> $remarks,
			'status_receiving' 	=> $status_receiving,
			'created_by' 		=> $created_by,
			'created_date' 		=> $created_date,
			'status' 			=> 1
		);

		$this->receiving_mod->receiving_detail_new_process_db($form_data);
	}
	//----------------------------------------

	function receiving_detail_list(){
		//MTO DETAIL ------------------------------------------------
		$where['receiving_id'] = $this->input->post('master_receiving_id');
		$where['status'] = 1;
		$data = $this->receiving_mod->receiving_detail_data($where);
		unset($where);
		//-----------------------------------------------------------

		// echo $this->input->post('master_receiving_id');exit;
		// print_r($data[0]);exit;

		for($i=0;$i<sizeof($data);$i++){

			//MATERIAL CATALOG ----------------------------------------------------
			$where['id'] = $data[$i]['catalog_id'];
			$datadb = $this->material_catalog_mod->getAll($where);
			unset($where);

			$data[$i]['name_catalog'] = $datadb[0]['material'];
			//---------------------------------------------------------------------

			//AREA ----------------------------------------------------------------
			$where['id'] = $data[$i]['area'];
			$datadb = $this->general_mod->data_area($where);
			unset($where);

			$data[$i]['name_area'] = $datadb[0]['area_name'];
			//---------------------------------------------------------------------
		}

		echo json_encode($data);
	}

	function receiving_check_do_pl(){
		$receiving_id = $this->input->post('receiving_id');
		$do_pl = $this->input->post('do_pl');
		$hasil = 0;

		$where['do_pl'] = $do_pl;

		if($receiving_id != ''){
			$where['receiving_id != '] = $receiving_id;
		}

		$datadb = $this->receiving_mod->receiving_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo json_encode(array(
			'hasil' => $hasil
		));
	}

	function receiving_check_unique_no(){
		$unique_no = $this->input->post('unique_no');
		$hasil = 0;

		$where['uniq_no'] = $unique_no;
		$where['status'] = 1;
		$datadb = $this->receiving_mod->receiving_detail_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo $hasil;
	}	

	function receiving_detail_check_mto_catalog(){
		$receiving_id = $this->input->post('receiving_id');
		$mto_id = $this->input->post('mto_id');
		$hasil = 0;

		$where['catalog_id'] 	= $mto_id;
		$where['receiving_id'] 	= $receiving_id;
		$where['status'] 		= 1;
		$datadb = $this->receiving_mod->receiving_detail_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo $hasil;
	}

	function receiving_detail_edit_process(){		
		$col 		= $this->input->post('col');
		$id 		= $this->input->post('id');
		$value 		= $this->input->post('value');

		$form_data = array(
			$col => $value,
			'update_date' => date('Y-m-d H:i:s'),
			'update_by' => $this->user_cookie[0]
		);
		$where['rec_det_id'] = $id;
		$this->receiving_mod->receiving_detail_edit_process_db($form_data, $where);
	}

	function receiving_detail_delete_process(){		

		$id = $this->input->post('id');

		$form_data = array(
			'status' => 0,
			'delete_date' => date('Y-m-d H:i:s'),
			'delete_by' => $this->user_cookie[0]
		);

		$where['rec_det_id'] = $id;
		$this->receiving_mod->receiving_detail_edit_process_db($form_data, $where);
	}


	//RECEIVING QTY -----------------------------------------------------------------------------------------------
	function receiving_qty_check_mto_catalog(){
		$receiving_id = $this->input->post('receiving_id');
		$mto_id = $this->input->post('mto_id');
		$hasil = 0;

		$where['catalog_id'] 	= $mto_id;
		$where['receiving_id'] 	= $receiving_id;
		$where['status'] 		= 1;
		$datadb = $this->receiving_mod->receiving_qty_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo $hasil;
	}

	function receiving_qty_add_process(){

		$master_receiving_id		= $this->input->post('master_receiving_id');
		$catalog_id 				= $this->input->post('mto_catalog_qty');
		$qty 						= $this->input->post('qty');
		$unit_weight_mm_sqm 		= $this->input->post('unit_weight_mm_sqm');
		$unit_weight_length_sheet 	= $this->input->post('unit_weight_length_sheet');
		$total_weight_length_sheet 	= $this->input->post('total_weight_length_sheet');
		$total_weight_mm_sqm 		= $this->input->post('total_weight_mm_sqm');
		$value_mm_sqm 				= $this->input->post('value_mm_sqm');
		$total_value_length_sheet 	= $this->input->post('total_value_length_sheet');
		$total_value_mm_sqm 		= $this->input->post('total_value_mm_sqm');

		$created_by 				= $this->user_cookie[0];
		$created_date 				= date('Y-m-d H:i:s');

		$form_data = array(
			'receiving_id' 				=> $master_receiving_id,
			'catalog_id' 				=> $catalog_id,
			'qty' 						=> $qty,
			'unit_weight_length_sheet' 	=> $unit_weight_length_sheet,
			'total_weight_length_sheet' => $total_weight_length_sheet,
			'unit_weight_mm_sqm' 		=> $unit_weight_mm_sqm,
			'total_weight_mm_sqm'		=> $total_weight_mm_sqm,
			'value_mm_sqm'				=> $value_mm_sqm,
			'total_value_length_sheet' 	=> $total_value_length_sheet,
			'total_value_mm_sqm' 		=> $total_value_mm_sqm,
			'status' 					=> 1
		);

		$this->receiving_mod->receiving_qty_new_process_db($form_data);
	}

	function receiving_qty_edit_process(){		
		$col 		= $this->input->post('col');
		$id 		= $this->input->post('id');
		$value 		= $this->input->post('value');

		$form_data = array(
			$col => $value
		);
		$where['rec_val_id'] = $id;
		$this->receiving_mod->receiving_qty_edit_process_db($form_data, $where);
	}

	function receiving_qty_delete_process(){		

		$id = $this->input->post('id');

		$form_data = array(
			'status' => 0,
			// 'delete_date' => date('Y-m-d H:i:s'),
			// 'delete_by' => $this->user_cookie[0]
		);

		$where['rec_val_id'] = $id;
		$this->receiving_mod->receiving_qty_edit_process_db($form_data, $where);
	}
	//-------------------------------------------------------------------------------------------------------------	



	//RECEIVING LIST DATATABLE ----------------------------------------------------------------------------------
	function receiving_list_json(){
    	error_reporting(0);
 
        $list = $this->receiving_mod->get_datatables_receiving_list_dt();
        
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {

			$links = base_url()."receiving_detail/".strtr($this->encryption->encrypt($list->receiving_id), '+=/', '.-~');
			
            $no++;
            $row   = array();

            //ETD -------
            if($list->etd == '0000-00-00'){
            	$date_etd = '-';
            } else {
            	$date_etd = $list->etd;
            }
            //-----------

            //ATD -------
            if($list->atd == '0000-00-00'){
            	$date_atd = '-';
            } else {
            	$date_atd = $list->atd;
            }
            //-----------

            //SHIPPING COMPLETE -------
            if($list->shipping_complete == '0000-00-00 00:00:00'){
            	$date_shipping_complete = '-';
            } else {
            	$date_shipping_complete = $list->shipping_complete;
            }
            //-------------------------
            
            $row[] = $list->supply_number;
            $row[] = $list->vendor;
            $row[] = $list->material_package;
            $row[] = $list->weight;
            $row[] = $date_etd;
            $row[] = $date_atd;
            $row[] = $date_shipping_complete;
            $row[] = $list->remarks;
            $row[] = "<a href='".$links."' class='btn btn-secondary text-white' title='Detail'><i class='fas fa-file-alt'></i> Detail </a>&nbsp;";
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->receiving_mod->count_all_receiving_list_dt(),
            "recordsFiltered" => $this->receiving_mod->count_filtered_receiving_list_dt(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
    //-----------------------------------------------------------------------------------------------------------
}