<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Material_catalog extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();

        $this->load->model('home_mod');
        $this->load->model('general_mod');
        $this->load->model('engineering_mod');
        $this->load->model('mrir_mod');
        $this->load->model('material_catalog_mod');
        $this->load->model('catalog_category_mod');
        $this->load->model('mto_category_mod');

        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar = "material_catalog/sidebar";
        
    }

    public function index(){
        redirect('material_catalog/material_catalog_list');
    }

    function material_catalog_list(){
        $this->data['meta_title']       = 'Material Catalog';
        
        $where['status_delete'] = 0;
        $this->data["material_catalog_list"]      = $this->material_catalog_mod->material_catalog_list($where);
        unset($where);

        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;
        $this->data['subview']          = 'material_catalog/material_catalog_list';

        $this->load->view('index', $this->data);
    }

    public function material_catalog_add(){
        $this->data['meta_title']       = 'Material catalog';
        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;
        $this->data['subview']          = 'material_catalog/material_catalog_add';

        $where['status_delete'] = 1;
        $this->data['catalog_category_list']    = $this->material_catalog_mod->data_catalog_category($where);
        unset($where);

        //MTO CATEGORY ------------------------
        $where['status'] = 1;
        $this->data['material_category_list'] = $this->mto_category_mod->getAll($where);
        unset($where);
        //-------------------------------------

        $this->load->view('index', $this->data);
    }

    function material_catalog_add_process(){

        $data = $this->material_catalog_mod->get_last_code();
        if(sizeof($data) == 0){
            $code = '000001';
        } else {
            $code = str_pad($data[0]->code_material + 1, 6, '0', STR_PAD_LEFT);
        }

        $catalog_category   = $this->input->post('catalog_category');
        $description        = $this->input->post('description');
        $status             = 1;

        $form_data = array(
            'code_material'         => $code,
            'catalog_category_id'   => $catalog_category,
            'material'         => $description,
            'status_delete'         => 0
        );

        $this->material_catalog_mod->material_catalog_add($form_data);
        $this->session->set_flashdata('success', 'Your data has been Created!');

        redirect('material_catalog/material_catalog_list');
    }

    public function material_catalog_detail($id = null)
    {
        if (!isset($id)) redirect('material_catalog_list');

        $id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
        
        $this->data['meta_title']   = 'Material Catalog Detail';

        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;

        $where['id'] = $id;
        $this->data["material_catalog_list"]  = $this->material_catalog_mod->material_catalog_list($where);
        unset($where);

        //CATALOG CATEGORY -----
        $where['status_delete'] = 1;
        $this->data['catalog_category_list'] = $this->catalog_category_mod->catalog_category_list($where);
        unset($where);
        //----------------------

        $this->data['subview']     = 'material_catalog/material_catalog_detail';
        $this->load->view('index', $this->data);
    }

    function material_catalog_edit_process(){
        if($this->permission_cookie[28] == 0){
            redirect('auth/logout');
        }

        $id = $this->input->post('id');

        $catalog_category   = $this->input->post('catalog_category');
        $description        = $this->input->post('description');

        $form_data = array(
            'catalog_category_id'   => $catalog_category,
            'material'              => $description
        );

        $where['id'] = $id;
        $this->material_catalog_mod->material_catalog_edit($form_data, $where);
        unset($where);

        $this->session->set_flashdata('success', 'Your data has been Changed!');

        redirect('material_catalog/material_catalog_list');
    }

    function import_material(){
        $this->data['meta_title']       = 'Import Material Catalog';
        
        $where['status_delete'] = 0;
        $this->data["material_catalog_list"]      = $this->material_catalog_mod->getAll($where);
        unset($where);


        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;
        $this->data['subview']          = 'mto/material_catalog/material_catalog_import';

        $this->load->view('index', $this->data);
    }

    public function material_catalog_preview(){
        $id_user = $this->user_cookie;
        $date = date('Y-m-d H:i:s');

        $config['upload_path']          = 'file/mto_material_catalog/';
        $config['file_name']            = 'excel_'.$id_user[0];
        $config['allowed_types']        = 'xlsx';
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('file')){
            $this->session->set_flashdata('error', $this->upload->display_errors());
            self::import_material();
            return false;
        }

        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('file/mto_material_catalog/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

        $numrow = 1;

        foreach($sheet as $key => $row){

            if($numrow > 1 && $row['A'] != ''){

                //CATALOG CATEGORY -----------------------------------
                $where['code']  = $row['A'];
                $datadb         = $this->catalog_category_mod->getAll($where);
                unset($where);
                if($datadb){
                    $datadb     = $datadb[0];
                    $sheet[$key]['id_catalog_code'] = $datadb['id'];
                    $sheet[$key]['catalog_code']    = $datadb['catalog_category'];
                } else {
                    $sheet[$key]['id_catalog_code'] = '';
                    $sheet[$key]['catalog_code']    = $row['A'];
                }
                //----------------------------------------------------

                //MATERIAL GRADE -------------------------------------
                $where['material_grade']    = $row['C'];
                $datadb                     = $this->general_mod->data_material_grade_join($where);
                unset($where);
                if($datadb){
                    $datadb     = $datadb[0];
                    $sheet[$key]['id_material_grade']   = $datadb['mg_id'];
                    $sheet[$key]['name_material_grade'] = $datadb['material_grade'];
                    $sheet[$key]['name_material_class'] = $datadb['name_material_class'];
                } else {
                    $sheet[$key]['id_material_grade']   = '';
                    $sheet[$key]['name_material_grade'] = $row['C'];
                    $sheet[$key]['name_material_class'] = '';
                }
                //----------------------------------------------------

                //SUPPLY CATEGORY -----------------------------------
                $where['initial']  = $row['D'];
                $datadb         = $this->general_mod->supply_category_list($where);
                unset($where);
                if($datadb){
                    $datadb     = $datadb[0];
                    $sheet[$key]['id_supply_category'] = $datadb['id'];
                    $sheet[$key]['supply_category']    = $datadb['description'];
                } else {
                    $sheet[$key]['id_supply_category'] = '';
                    $sheet[$key]['supply_category']    = $row['D'];
                }
                //----------------------------------------------------
            
            }

            $numrow++; // Tambah 1 setiap kali looping
        }

        $data['sheet']          = $sheet;
        $data['read_cookies']   = $this->user_cookie;
        $data['meta_title']     = 'Material Catalog Preview';
        $data['subview']        = 'mto/material_catalog/material_catalog_preview';
        $data['sidebar']        = $this->sidebar;
        $this->load->view('index', $data);
    }

    public function material_catalog_import_process(){

        $data = $this->material_catalog_mod->get_last_code();
        if(sizeof($data) == 0){
            $code = '000001';
        } else {
            $code = str_pad($data[0]->code_material + 1, 6, '0', STR_PAD_LEFT);
        }

        $code_category            = $this->input->post('code_category');
        $material                 = $this->input->post('material');
        $material_grade           = $this->input->post('material_grade');
        $supply_category          = $this->input->post('supply_category');
        $thk_mm                   = $this->input->post('thk_mm');
        $width_m                  = $this->input->post('width_m');
        $length_m                 = $this->input->post('length_m');
        $weight                   = $this->input->post('weight');
        $od                       = $this->input->post('od');
        $sch                      = $this->input->post('sch');

        if(count($code_category) > 0){
            $form_data = array();
            foreach ($code_category as $key => $value) {
                $code = str_pad($code + $key, 6, '0', STR_PAD_LEFT);

                array_push($form_data, array(
                    'code_material'         => $code,
                    'catalog_category_id'   => $code_category[$key],
                    'material'              => $material[$key], // Insert data nis dari kolom A di excel
                    'material_grade'        => $material_grade[$key], // Insert data nis dari kolom A di excel
                    'supply_category'       => $supply_category[$key], // Insert data nis dari kolom A di excel
                    'thk_mm'                => $thk_mm[$key], // Insert data nis dari kolom A di excel
                    'width_m'               => $width_m[$key], // Insert data nis dari kolom A di excel
                    'length_m'              => $length_m[$key], // Insert data nis dari kolom A di excel
                    'weight'                => $weight[$key], // Insert data nis dari kolom A di excel
                    'od'                    => $od[$key], // Insert data nis dari kolom A di excel
                    'sch'                   => $sch[$key] // Insert data nis dari kolom A di excel
                ));
            }

            $this->material_catalog_mod->material_catalog_import_process_db($form_data);
            $this->session->set_flashdata('success', 'Your data has been imported!');
            redirect("material_catalog_list");
        }
        else{
            $this->session->set_flashdata('error', 'No Data to import!');
            redirect("material_catalog_list");
        }

        
    }

    public function delete($id=null)
    {
        $id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

        if (!isset($id)) show_404();
        
        if ($this->material_catalog_mod->delete($id,$cur_stat)) {

            $this->session->set_flashdata('success', 'Your data has been Deleted!');

            redirect('material_catalog');
        }
    }

    function get_material_code(){
        $material_code = $this->input->post('material_code');

        $where['code_material'] = $material_code;
        $result = $this->material_catalog_mod->getAll($where);
        
        $data = 0;
        if(count($result) > 0){
            $data = 1;
        }

        echo json_encode($data);
    }

    function get_grade(){
        $material_grade = $this->input->post('material_grade');
        $data = 0;

        if($material_grade){
            //MATERIAL GRADE ------------------------------------------------------
            $where['mg.id'] = $material_grade;
            $material_grade_list = $this->general_mod->data_material_grade_join($where);
            $material_grade_list = $material_grade_list[0];
            unset($where);
            //---------------------------------------------------------------------
            
            if(count($material_grade_list) > 0){
                $data = 1;
            }
        }

        echo json_encode(array(
            'hasil' => $data,
            'id_material_class' => @$material_grade_list['material_class'],
            'name_material_class' => @$material_grade_list['name_material_class'],
        ));
    }

    //MATERIAL CATALOG DATATABLE ----------------------------------------------------------------------------------
    function catalog_list_json(){
        error_reporting(0);
 
        $list = $this->material_catalog_mod->get_datatables_material_catalog_list_dt();
        
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {

            $links = base_url()."material_catalog/material_catalog_detail/".strtr($this->encryption->encrypt($list->catalog_id), '+=/', '.-~');
            
            $no++;
            $row   = array();
            
            $row[] = $list->code_material;
            $row[] = $list->category_name;
            $row[] = $list->material;

            if($this->permission_cookie[28] == 1){
                $row[] = "<a href='".$links."' class='btn btn-secondary text-white' title='Detail'><i class='fas fa-file-alt'></i> Detail </a>";
            } else {
                $row[] = "&nbsp;";
            }
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "recordsTotal" => $this->material_catalog_mod->count_all_material_catalog_list_dt(),
            "recordsFiltered" => $this->material_catalog_mod->count_filtered_material_catalog_list_dt(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
    //-----------------------------------------------------------------------------------------------------------

}