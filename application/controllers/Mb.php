<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Mb extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('mb_mod');
		$this->load->model('mis_mod');
		$this->load->model('general_mod');

		$this->user_cookie 		 = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));

		$this->sidebar = "mb/sidebar";
	}

	public function index(){
		redirect('mb/msr_list');
	}

	public function mb_list(){
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Material Balance List';
		$data['subview']					= 'mb/mb_list';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function mb_detail($uniq_no_enc){
		$data['t']	   	= '';
		$uniq_no 	= $this->encryption->decrypt(strtr($uniq_no_enc, '.-~', '+=/'));
		// echo $uniq_no_enc.'<br>'.$uniq_no;

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Material Balance Detail';
		$data['subview']					= 'mb/mb_detail';
		$data['sidebar']					= $this->sidebar;
		$this->load->view('index', $data);
	}

	function mb_list_json(){ 
		$datadb = $this->mis_mod->get_project();	   
		foreach ($datadb as $value) {
			$project_list[$value['id']] = $value['project_name'];
		}

		$lists = $this->mb_mod->mb_list_datatables('data');
		// print_r($lists);
		$uniq_no = array();
		foreach ($lists as $list){		  
			$uniq_no[] = $list->unique_no;
		}

		$datadb = $this->mb_mod->get_additional_info($uniq_no);
		foreach ($datadb as $value) {
			$info[$value->uniq_no] = $value;
		}


		$data = array();
		$no   = $_POST['start'];
		foreach ($lists as $list){		  
			$links = base_url()."mb/mb_detail/".strtr($this->encryption->encrypt($list->unique_no), '+=/', '.-~');
			
			$no++;
			$row   = array();

			$data_temp = "";
			if(isset($info[$list->unique_no])){
				$data_temp = $project_list[$info[$list->unique_no]->project_id];
			}
			$row[] = $data_temp; //project

			$row[] = $list->unique_no; //unique_no

			$data_temp = "";
			if(isset($info[$list->unique_no])){
				$data_temp = $info[$list->unique_no]->material;
			}
			$row[] = $data_temp; //material

			$data_temp = "";
			if(isset($info[$list->unique_no])){
				$data_temp = $info[$list->unique_no]->vendor;
			}
			$row[] = $data_temp; //vendor

			$data_temp = "";
			if(isset($info[$list->unique_no])){
				$data_temp = $info[$list->unique_no]->po_item;
			}
			// $row[] = $data_temp; //po item
			
			$row[] = $list->bal_qty; //qty

			$data_temp = "";
			if(isset($info[$list->unique_no])){
				$data_temp = $info[$list->unique_no]->area;
			}
			$row[] = $data_temp; //area

			$data_temp = "";
			if(isset($info[$list->unique_no])){
				$data_temp = $info[$list->unique_no]->location;
			}
			$row[] = $data_temp; //location
		   
			$row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-file-alt'></i> Detail</a>"; //button
					   
			$data[] = $row;
		}

		//print_r($data);
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->mb_mod->mb_list_datatables('count_all'),
			"recordsFiltered" => $this->mb_mod->mb_list_datatables('count_filter'),
			"data" => $data
		);
		//output to json format
		echo json_encode($output);
	}


	//Transaction - Modul
	public function tr_list(){
		$data['read_cookies']	   = $this->user_cookie;
		$data['meta_title']		 = 'Transaction List';
		$data['subview']			= 'mb/tr_list';
		$data['sidebar']			= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	function tr_list_json(){ 
		$lists = $this->mb_mod->tr_list_datatables('data');
		// print_r($lists);
		$user = array();
		foreach ($lists as $list){
			if(!in_array($list->tr_by, $user)){
				$user[] = $list->tr_by;
			}
		}

		$user_list		= user_data($user);
	  
		$data = array();
		$no   = $_POST['start'];
		foreach ($lists as $list){
			
			$no++;
			$row   = array();
			
			$row[] = $list->unique_no;
			$row[] = $list->qty;
			$row[] = $list->tr_status;
			if(isset($user_list[$list->tr_by])){
				$row[] = $user_list[$list->tr_by]['full_name'];
			}
			else{
				$row[] = "";
			}
			
			$row[] = $list->tr_date;
			$row[] = $list->remarks;
					   
			$data[] = $row;
		}

		//print_r($data);
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->mb_mod->tr_list_datatables('count_all'),
			"recordsFiltered" => $this->mb_mod->tr_list_datatables('count_filter'),
			"data" => $data
		);
		//output to json format
		echo json_encode($output);
	}

	//Material Status Report
	public function msr_list(){
		$datadb = $this->mis_mod->get_project();	

		foreach ($datadb as $value) {
			$data['project_list'][$value['id']] = $value['project_name'];
		}

		$data['read_cookies']	= $this->user_cookie;
		$data['meta_title']		= 'Material Status Report';
		$data['subview']		= 'mb/msr_list';
		$data['sidebar']		= $this->sidebar;
		$data['sidebar_close']	= 1;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	function msr_list_json(){ 
		$project_id = NULL;
		$supply_type = 'SS';
		if(isset($_POST['project_id'])){
			$project_id = $_POST['project_id'];
		}
		if(isset($_POST['supply_type'])){
			$supply_type = $_POST['supply_type'];
		}
		$lists = $this->mb_mod->msr_list_datatables('data', $project_id, $supply_type);
		// print_r($lists);
		// $user = array();
		// foreach ($lists as $list){
		// 	if(!in_array($list->tr_by, $user)){
		// 		$user[] = $list->tr_by;
		// 	}
		// }

		// $user_list		= user_data($user);
	  
		$data = array();
		$no   = $_POST['start'];
		foreach ($lists as $list){
			
			$no++;
			$row   = array();
			
			// $row[] = $list->report_no;
			$row[] = $list->unique_no;
			$row[] = $list->project_name;
			$row[] = $list->po_number;
			$row[] = $list->date_receiving;
			$row[] = $list->discipline_name;
			$row[] = $list->catalog_category;
			$row[] = $list->code_material;
			$row[] = $list->material;
			$row[] = $list->length_m;
			$row[] = $list->width_m;
			$row[] = $list->thk_mm;
			$row[] = $list->od;
			$row[] = $list->sch;
			$row[] = $list->spec;
			$row[] = $list->spec_category;
			$row[] = $list->plate_or_tag_no;
			$row[] = $list->heat_or_series_no;
			$row[] = $list->mill_cert_no;
			$row[] = $list->brand;
			$row[] = $list->do_or_pl_no;
			$row[] = $list->rec_qty;
			$row[] = $list->rec_length;
			$row[] = $list->osd_over_qty;
			$row[] = $list->osd_shortage_qty;
			$row[] = $list->osd_damage_qty;
			$row[] = $list->ret_vendor;
			$row[] = $list->iss_qty;
			$row[] = $list->iss_length;
			$row[] = $list->fab_qty;
			$row[] = $list->fab_length;
			$row[] = $list->bal_qty;
			$row[] = $list->bal_length;
			$row[] = $list->remarks;
			$row[] = $list->location_name;
			$row[] = $list->area_name;
			$row[] = $list->position;
			$row[] = "<a class='btn btn-primary' href='".base_url()."mb/msr_edit/".strtr($this->encryption->encrypt($list->mb_id), '+=/', '.-~')."'><i class='fa fa-edit'></i> Add Location</a>";
					   
			$data[] = $row;
		}

		//print_r($data);
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->mb_mod->msr_list_datatables('count_all', $project_id),
			"recordsFiltered" => $this->mb_mod->msr_list_datatables('count_filter', $project_id),
			"data" => $data
		);

		//output to json format
		echo json_encode($output);
	}

	function msr_edit($id){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
		$where['mb_id'] = $id;
		$data['mb_list'] = $this->mb_mod->mb_list($where);
		$data['location_list'] = $this->general_mod->eng_location_get_db();
		$data['area_list'] = $this->general_mod->get_area_name();

		// test_var($data['area_list']);

		$data['read_cookies']	   = $this->user_cookie;
		$data['meta_title']		 = 'Edit Material';
		$data['subview']			= 'mb/msr_edit';
		$data['sidebar']			= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function msr_edit_process(){
		$mb_id 		= $this->input->post('mb_id');
		$location = $this->input->post('location');
		$area 		= $this->input->post('area');
		$position = $this->input->post('position');

		$where['mb_id'] = $mb_id;
		$form_data = array(
			'location' 	=> $location,
			'area' 			=> $area,
			'position' 	=> $position,
		);
		$this->mb_mod->mb_edit_process($form_data, $where);
		$this->session->set_flashdata('success', 'Your data has been updated!');
		redirect("mb/msr_edit/".strtr($this->encryption->encrypt($mb_id), '+=/', '.-~'));
	}
}