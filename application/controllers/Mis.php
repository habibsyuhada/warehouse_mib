<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Mis extends CI_Controller {

    public function __construct() {
            
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();
        $this->load->helper('cookies');
        helper_cookies();

        $this->load->model('home_mod');
        $this->load->model('general_mod');
        $this->load->model('engineering_mod');
        $this->load->model('mrir_mod');

        //FOR mis ===================================
        
        $this->load->model('mis_mod');
        $this->load->model('material_catalog_mod');
        
        //$this->load->model('mis_category_mod');

        //===========================================

        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar = "mis/sidebar";
    }

    public function index(){
        redirect('mis/mis_list/pending');
    }

    public function mis_list($param=null){
       
        if($param == "approved"){
           $req_pages = $param;
        } else if($param == "rejected"){
           $req_pages = $param;
        } else {
           $req_pages = $param;   
        }

        $title_data = ucfirst($req_pages);

        $data['read_cookies']       = $this->user_cookie;
        $data['meta_title']         = 'MIS List '.$title_data;
        $data['subview']            = 'mis/mis_list';
        $data['sidebar']            = $this->sidebar;
        $data['read_permission']    = $this->permission_cookie;
        $data['req_pages']          =  $req_pages;

        $this->load->view('index', $data);
    }

    public function mis_add(){
      
        $data['read_cookies']     = $this->user_cookie;
        $data['meta_title']       = 'Create New MIS';
        $data['subview']          = 'mis/mis_new';
        $data['sidebar']          = $this->sidebar;
        $data['material_class']   = $this->mis_mod->get_material_class();
        $data['get_project']      = $this->mis_mod->get_project();
        $data['get_mis_no']       = $this->mis_mod->generate_batch_no();
        $data['get_mis_location'] = $this->mis_mod->get_location();
        $data['read_permission']  = $this->permission_cookie;
        $this->load->view('index', $data);

    }

    public function drawing_no_check(){

        $drawing_no             = $this->input->post('drawing_no');
        $where['drawing_no']    = $drawing_no;
        $data_mis               = $this->mis_mod->mis_list($where);
        $data_eng               = $this->engineering_mod->draw_get_db($where);
        $data_mto               = $this->mis_mod->searching_mto_drawing_data($drawing_no);
      
        unset($where);

            $hasil = 0;       
            if(sizeof($data_mto) != 0){
                $hasil = 1;
                $data = $data_mto[0];
              
                $data_project           = $this->mis_mod->getProjectDetail($data_mto[0]["project_id"]);
            }
        
         //print_r($data_project[0]["project_name"]);

        echo json_encode(array(
            'project_id'    => @$data_mto[0]["project_id"],
            'project_name'  => @$data_project[0]["project_name"],
            'hasil'         => @$hasil,
        
        ));
    }

 
    
    public function mis_detail($mis_number = null){

        $mis_number = $this->encryption->decrypt(strtr($mis_number, '.-~', '+=/'));

        if(empty($mis_number)){ 
            redirect('mis_list');
        }

        $datadb = $this->mis_mod->getProject();
        foreach ($datadb as $value) {
            $data['project_list'][$value['id']] = $value['project_name'];
        }
        
        $where['mrs_no']    = $mis_number;
        $data['mis_list']   = $this->mis_mod->mis_list($where);
        $data['mis_list_detail']   = $this->mis_mod->mis_list_detail($where);
        unset($where);
       
        $data['read_cookies']       = $this->user_cookie;
        $data['read_permission']    = $this->permission_cookie;
        $data['material_class']     = $this->mis_mod->get_material_class();
        $data['get_project']        = $this->mis_mod->get_project();
        $data['meta_title']         = 'MIS'.'/'.$data['mis_list'][0]["project_id"]."/".$mis_number;
        $data['subview']            = 'mis/mis_detail';
        $data['sidebar']            = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    function get_material_category(){
        $material_cat = $this->input->post('material_cat');

        $where['id'] = $material_cat;
        $data = $this->mis_mod->material_category_list($where);
        unset($where);

        echo json_encode($data);
    }

   

    //DRAWING AUTOCOMPLETE ============================================================================

    function drawing_autocomplete(){
        $drawing_no = $this->input->post('drawing_no');
        if(isset($_GET['term'])){
            $result = $this->mis_mod->search_drawing_autocomplete($_GET['term'], $drawing_no);
            if ($result == TRUE){
                foreach ($result as $row)
                $arr_result[] = $row['document_no'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Drawing Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    //=================================================================================================

     //UNIQUE NO AUTOCOMPLETE ============================================================================
    function uniqno_autocomplete(){
        
      if (($this->input->post('term'))){
            $result = $this->mis_mod->search_unique_autocomplete($this->input->post('term'),$this->input->post('project_id'));
            if ($result == TRUE){
                foreach ($result as $row)
                $arr_result[] = $row['unique_no'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Unique id Not Found";
                echo json_encode($arr_result);
            }
        }
    }
    //=================================================================================================

    //MATERIAL CATALOG AUTOCOMPLETE====================================================================
    function material_catalog_autocomplete(){
        $material_catalog = $this->input->post('material_catalog');
        if (isset($_GET['term'])){
            $result = $this->mis_mod->search_material_catalog_autocomplete($_GET['term'], $material_catalog);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['short_desc'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Material Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    function mis_check_material_catalog(){
        $material = $this->input->post('material');
        $where['short_desc'] = $material;
        $result = $this->material_catalog_mod->getAll($where);
        
        $text = 0;
        $data = array(
            "id" => $text
        );

        if(count($result) > 0){
            $text = $result[0]['id'];
            $data = array(
                "id" => $text,
                "material" => $result[0]['material'],
                "size"  => $result[0]['size'],
                "type" => $result[0]['type'],
                "weight"    => $result[0]['weight'],
                "length"    => $result[0]['length']
            );
        }

        echo json_encode($data);
    }

    //Drawing List DataTables 


    


    public function unique_no_check($unique_no = null,$project_id = null){

        $unique_no    = $this->input->post('unique_no');
        $project_id   = $this->input->post('project_id');
        $drawing_no   = $this->input->post('drawing_no');
        
        $data_unique_no_balance   = $this->mis_mod->get_bal_unique($unique_no);


        if(sizeof($data_unique_no_balance) > 0){
        
        if($data_unique_no_balance[0]["unique_no"] !== ""){

            if($data_unique_no_balance[0]["bal_qty"] > 0){

                      
             $data_unique_no_rec_id  = $this->mis_mod->get_data_unique($unique_no,$project_id);
              $data_catalog_data      = $this->mis_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]); 

                if(isset($data_unique_no_rec_id[0]["unique_ident_no"])){
                   
                     $description    = $data_unique_no_rec_id[0]["description"];

                     $thk_mm         = $data_catalog_data[0]["thk_mm"];
                     $width_m        = $data_catalog_data[0]["width_m"];
                     $length_m       = $data_catalog_data[0]["length_m"];

                     $weight         = $data_catalog_data[0]["weight"];
                     $od             = $data_catalog_data[0]["od"];
                     $sch            = $data_catalog_data[0]["sch"];

                     $unit           = $data_unique_no_rec_id[0]["uom"];
                     $heat_no        = $data_unique_no_rec_id[0]["heat_or_series_no"];
                     $balance        = $data_unique_no_balance[0]["bal_qty"];
                     $balance_length = $data_unique_no_balance[0]["bal_length"];

                     echo $description."; ".$thk_mm."; ".$width_m."; ".$length_m."; ".$weight."; ".$od."; ".$sch."; ".$unit."; ".$heat_no."; ".$balance."; ".$balance_length;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
            } else {
                echo "Error : Balance Empty..";
            }

        } else { 

            echo "Error : Unique Number Not Found..";

        }

    } else {
       echo "Error : Unique Number Not Found.."; 
    }
        
    }


    public function unique_no_check_det($id_detail = null){

        $id_detail   = $this->input->post('id_detail');
        $project_id  = $this->input->post('project_id');
        $drawing_no  = $this->input->post('drawing_no');

        $where["id_mis_det"] = $id_detail;
        $data_detail_list   = $this->mis_mod->mis_list_detail($where);
        unset($where);

        if(sizeof($data_detail_list) > 0){

        $unique_no          = $data_detail_list[0]['unique_no'];
      
        $data_unique_no_balance = $this->mis_mod->get_bal_unique($unique_no);        
        $data_unique_no_rec_id  = $this->mis_mod->get_data_unique($unique_no,$project_id);
        $data_catalog_data      = $this->mis_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]);

        if($data_unique_no_balance[0]["unique_no"] !== ""){
           
                if(isset($data_unique_no_rec_id[0]["unique_ident_no"])){
                     
                     $description    = $data_unique_no_rec_id[0]["description"];

                     $thk_mm         = $data_catalog_data[0]["thk_mm"];
                     $width_m        = $data_catalog_data[0]["width_m"];
                     $length_m       = $data_catalog_data[0]["length_m"];

                     $weight         = $data_catalog_data[0]["weight"];
                     $od             = $data_catalog_data[0]["od"];
                     $sch            = $data_catalog_data[0]["sch"];

                     $unit           = $data_unique_no_rec_id[0]["uom"];
                     $heat_no        = $data_unique_no_rec_id[0]["heat_or_series_no"];
                     $balance        = $data_unique_no_balance[0]["bal_qty"];
                     $balance_length = $data_unique_no_balance[0]["bal_length"];

                     echo $description."; ".$thk_mm."; ".$width_m."; ".$length_m."; ".$weight."; ".$od."; ".$sch."; ".$unit."; ".$heat_no."; ".$balance."; ".$balance_length;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
          

        } else { 

            echo "Error : Unique Number Not Found..";

        }

    } else {
        echo "Error : Unique Number Not Found..";
    }
        
    }




    public function unique_no_check_preview($unique_no = null){

        $unique_no   = $this->input->post('unique_no');

        $data_unique_no_balance   = $this->mis_mod->get_bal_unique($unique_no);

        if(sizeof($data_unique_no_balance) > 0){
        
        if($data_unique_no_balance[0]["unique_no"] !== ""){

             $data_unique_no_rec_id  = $this->mis_mod->get_data_unique($unique_no);
             $data_catalog_data      = $this->mis_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]);

                if(isset($data_unique_no_rec_id[0]["catalog_id"])){

                     $get_data_catalog  = $this->mis_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]);

                     $description    = $data_unique_no_rec_id[0]["description"];

                     $thk_mm         = $data_catalog_data[0]["thk_mm"];
                     $width_m        = $data_catalog_data[0]["width_m"];
                     $length_m       = $data_catalog_data[0]["length_m"];

                     $weight         = $data_catalog_data[0]["weight"];
                     $od             = $data_catalog_data[0]["od"];
                     $sch            = $data_catalog_data[0]["sch"];

                     $unit           = $data_unique_no_rec_id[0]["uom"];
                     $heat_no        = $data_unique_no_rec_id[0]["heat_or_series_no"];
                     $balance        = $data_unique_no_balance[0]["bal_qty"];
                     $balance_length = $data_unique_no_balance[0]["bal_length"];

                     echo $description."; ".$thk_mm."; ".$width_m."; ".$length_m."; ".$weight."; ".$od."; ".$sch."; ".$unit."; ".$heat_no."; ".$balance."; ".$balance_length;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
            

        } else { 

            echo "Error : Unique Number Not Found..";

        }

    } else {
       echo "Error : Unique Number Not Found.."; 
    }
        
    }


     public function balance_checking(){

        $unique_no   = $this->input->post('unique_no');
        $issued_qty   = $this->input->post('issued_qty');

       if(is_numeric($issued_qty)){

                $data_unique_no_balance   = $this->mis_mod->get_bal_unique($unique_no);

                if(sizeof($data_unique_no_balance) > 0){

                    $balance_after_check = $data_unique_no_balance[0]["bal_qty"] - $issued_qty;
                    if($balance_after_check < 0){
                        echo "Error : Sorry, We Just only have ".$data_unique_no_balance[0]["bal_qty"]; 
                    } else {
                        echo "Balance Available"; 
                    }

                } else {
                    echo "Error : Balance Shortage!"; 
                }
        } else {
            echo "Error : Input Correct Number!"; 
        }

        }
    


    function mis_list_json($param=null)
    {
        error_reporting(0);

        if($param == "approved"){
           $req_pages = 3;
        } else if($param == "rejected"){
           $req_pages = 2;
        } else {
           $req_pages = 1;   
        }

        $datadb = $this->mis_mod->get_project();       
        foreach ($datadb as $value) {
            $project_list[$value['id']] = $value['project_name'];
        }


        $datadb = $this->mis_mod->get_user_data();       
        foreach ($datadb as $value) {
            $user_list[$value['id_user']] = $value['full_name'];
        }
 
        $list = $this->mis_mod->get_datatables_mis_list_dt($req_pages);
      
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {
          
            $links = base_url()."mis_detail/".strtr($this->encryption->encrypt($list->mrs_no), '+=/', '.-~');
            
            $no++;
            $row   = array();

            if($list->status == 1){
                 $status = "<span style='font-weight:bold;color:red;'>"."OPEN"."</style>";
            } else if($list->status == 2){
                 $status = "<span style='font-weight:bold;color:red;'>"."REJECTED"."</style>";     
            } else if($list->status == 3){
                 $status = "<span style='font-weight:bold;color:green;'>"."APPROVED"."</style>";
            }
            
            $row[] = (isset($user_list[$list->created_by]) ? $user_list[$list->created_by] : '-');
            $row[] = $list->created_date;
            $row[] = $list->mrs_no;
            $row[] = (isset($project_list[$list->project_id]) ? $project_list[$list->project_id] : '-');
            $row[] = $list->sub_contractor_name;
            $row[] = $list->line_no;
            $row[] = $list->drawing_no;
            $row[] = $list->material_class;
            $row[] = $list->equipment_tag_no;
            $row[] = $status;
           
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-file-alt'></i></a>";
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mis_mod->count_all_mis_list_dt($req_pages),
            "recordsFiltered" => $this->mis_mod->count_filtered_mis_list_dt($req_pages),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }


    function mis_new_form(){
                
        $project_id           = $this->input->post('project_id');
        $sub_contractor_name  = $this->input->post('sub_contractor_name');
        $line_no              = $this->input->post('line_no');
        $drawing_no           = $this->input->post('drawing_no');
        $equipment_tag_no     = $this->input->post('equipment_tag_no');
        $mrs_no               = $this->input->post('mrs_no');
        $material_class       = $this->input->post('material_class');
        $created_by           = $this->user_cookie[0];
        $date_create          = $this->input->post('created_date');        

        $form_data = array(
          'project_id'           => $project_id,
          'sub_contractor_name'  => strtoupper($sub_contractor_name),        
          'line_no'              => strtoupper($line_no),
          'drawing_no'           => $drawing_no,
          'equipment_tag_no'     => strtoupper($equipment_tag_no),
          'mrs_no'               => $mrs_no,
          'material_class'       => $material_class,
          'created_by'           => $created_by,
          'created_date'         => $date_create,
          'status'               => 1
        );

        $this->mis_mod->insert_mis_master($form_data);

        $unique_no = $this->input->post('unique_no');
        if($unique_no){
            foreach ($unique_no as $key => $value) {
                $form_data = array(           
                  'mrs_no'              => $mrs_no,       
                  'unique_no'           => $this->input->post('unique_no')[$key],                  
                  'qty'                 => $this->input->post('issued_qty')[$key],
                  'length'              => $this->input->post('issued_length')[$key],
                  'location'            => $this->input->post('location')[$key],
                  'remarks'             => $this->input->post('remarks')[$key],
                  'created_by'          => $created_by,
                  'created_date'        => $date_create,
                  'status'              => 1
                );
                $this->mis_mod->insert_mis_detail($form_data);
            }
        }
       
        $this->session->set_flashdata('success', 'Your MIS data has been Created!');
        redirect('mis_list');
    }

    function approve_mis_form($mrs_no_send,$param){                
        $mrs_no           = $mrs_no_send;
        $param_approval   = $param;
                
        $where["mrs_no"] = $mrs_no;
        $mis_list        = $this->mis_mod->mis_list($where);
        $mis_list_detail = $this->mis_mod->mis_list_detail($where);
        $form_data = array(
          'status' => $param_approval
        );
        $this->mis_mod->update_mis_data($form_data,$where);
       
        unset($where);

        if( $param_approval == 3){
            foreach ($mis_list_detail as $key) {
                $form_data = array(    
                    'unique_no'    => $key['unique_no'],                  
                    'qty'          => $key['qty'], 
                    'length'       => $key['length'], 
                    'project_id'   => $mis_list[0]['project_id'], 
                    'tr_status'    => "Out",
                    'tr_category'  => "MIS",
                    'remarks'      => "From MIS - ".$key['mrs_no'],
                    'tr_by'        => $this->user_cookie[0],
                );

                $this->mis_mod->insert_mis_transaction($form_data);
             }

             $this->session->set_flashdata('success', 'Your MIS data has been Approved!');
             redirect('mis/mis_list/approved');
        
        } else {
             $this->session->set_flashdata('success', 'Your MIS data has been Rejected!');
             redirect('mis/mis_list/rejected');
        }      
    }

    public function mis_pdf($mrs_no){

        $mrs_no = $this->encryption->decrypt(strtr($mrs_no, '.-~', '+=/'));
       
        $where['mrs_no']                = $mrs_no;
        $data['mis_list']               = $this->mis_mod->mis_list($where);
        $data['mis_list_detail']        = $this->mis_mod->mis_list_detail($where);
        

        $datadb = $this->mis_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_list'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mis_mod->get_data_unique(null,$data['mis_list'][0]['project_id']);
        foreach ($datadb as $value) {
            $data['unit_list'][$value['unique_ident_no']] = $value['uom'];
        }

        foreach ($datadb as $value) {
            $data['heat_no_list'][$value['unique_ident_no']] = $value['heat_or_series_no'];
        }
             
        foreach ($datadb as $value) {
            $data['description_list'][$value['unique_ident_no']] = $value['description'];
        }
        foreach ($datadb as $value) {
            $data['length_list'][$value['unique_ident_no']] = $value['length'];
        }
        foreach ($datadb as $value) {
            $data['width_od_list'][$value['unique_ident_no']] = $value['width_or_od'];
        }
        foreach ($datadb as $value) {
            $data['sch_thk_list'][$value['unique_ident_no']] = $value['sch_or_thk'];
        }

        unset($where);

        
            $this->load->library('Pdfgenerator');
            $html = $this->load->view('mis/mis_pdf', $data,true);
            $this->pdfgenerator->generate($html,$mrs_no);

            //$html = $this->load->view('mis/mis_pdf', $data);
    }


     public function mis_import(){
      
        $data['read_cookies']     = $this->user_cookie;
        $data['meta_title']       = 'Import New MIS';
        $data['subview']          = 'mis/mis_import';
        $data['sidebar']          = $this->sidebar;
        $data['material_class']   = $this->mis_mod->get_material_class();
        $data['get_project']      = $this->mis_mod->get_project();
        $data['get_mis_no']       = $this->mis_mod->generate_batch_no();
        $data['get_mis_location'] = $this->mis_mod->get_location();
        $data['read_permission']  = $this->permission_cookie;
        $this->load->view('index', $data);

    }

    public function mis_import_preview(){

        $id_user = $this->user_cookie;

        $config['upload_path']    = 'file/draw/';
        $config['file_name']      = 'excel_'.$id_user[0];
        $config['allowed_types']  = 'xlsx';
        $config['overwrite']      = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('file')){
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect("mis/mis_import");
            return false;
        }

        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel   = $excelreader->load('file/draw/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
        $sheet       = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
              
        $data['material_class']   = $this->mis_mod->get_material_class();
        $data['get_project']      = $this->mis_mod->get_project();
        $data['get_mis_no']       = $this->mis_mod->generate_batch_no();
        $data['get_mis_location'] = $this->mis_mod->get_location();
        $data['sheet']            = $sheet;

        $data['read_cookies']     = $this->user_cookie;
        $data['read_permission']  = $this->permission_cookie;
        $data['meta_title']       = 'Mis Import Preview';
        $data['subview']          = 'mis/mis_preview';
        $data['sidebar']          = $this->sidebar;
        $this->load->view('index', $data);
    }


}