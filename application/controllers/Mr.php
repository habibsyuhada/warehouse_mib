<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Mr extends CI_Controller {

    public function __construct() {
            
        parent::__construct();
        $this->load->helper('cookies');
        helper_cookies();

        $this->load->model('home_mod');
        $this->load->model('mr_mod');

        //FOR MTO ===================================
        $this->load->model('material_catalog_mod');
        //===========================================

        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar = "mr/sidebar";
    }

    public function index(){
        redirect('mr/mr_list/open');
    }

    public function mr_list($status=null){

        $data['read_cookies']    = $this->user_cookie;
        $data['meta_title']      = 'Material Requisition List';
        $data['subview']         = 'mr/mr_list';
        $data['sidebar']         = $this->sidebar;
        $data['read_permission'] = $this->permission_cookie;

        if($status == "open"){
            $data['status']  = "1";
        } else if($status == "rejected"){
            $data['status']  = "2";
        } else if($status == "approved"){
            $data['status']  = "3";
        } else {
            $data['status']  = "1";
        }

        $this->load->view('index', $data);
    }

    public function mr_list_tab($status=null){

        $data['read_cookies']    = $this->user_cookie;
        $data['meta_title']      = 'Material Requisition List';
        $data['subview']         = 'mr/mr_list_tab';
        $data['sidebar']         = $this->sidebar;
        $data['read_permission'] = $this->permission_cookie;

        if($status == "open"){
            $data['status']  = "1";
        } else if($status == "rejected"){
            $data['status']  = "2";
        } else if($status == "approved"){
            $data['status']  = "3";
        } else {
            $data['status']  = "1";
        }

        $this->load->view('index', $data);
    }

    public function mr_new(){

        $data['read_cookies']    = $this->user_cookie;
        $data['meta_title']      = 'Form Material & Service Request';
        $data['subview']         = 'mr/mr_new';
        $data['sidebar']         = $this->sidebar;
        $data['read_permission'] = $this->permission_cookie;

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $data['department']  = $this->mr_mod->get_data_department($this->user_cookie[4]);

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);

        $this->load->view('index', $data);
    }

    public function budget_check(){


        $budget_cat   = $this->input->post('budget_cat');
        $budget_type  = $this->input->post('budget_type');

        if(isset($budget_cat)){
            $data_send = $budget_cat;
        } else {
            $data_send = 0; 
        }

        $where['ebc.id']         = $data_send;
        $where['ebd.assetable']  = $budget_type;
        $where['eb.year']        = date("Y");
        $data['budgeta']         = $this->mr_mod->get_budget_amount($this->user_cookie[4],$where);
        unset($where);

        $budgeted_amount = $data['budgeta'][0]['total'];

        $where['to_category'] = $data_send;
        $where['assetable']   = $budget_type;
        $where['year']        = date("Y");
        $data['budgetb']      = $this->mr_mod->get_transfered_amount($this->user_cookie[4],$where);
        unset($where);

        $transfered_amount = $data['budgetb'][0]['total'];

        $where['cat_id']      = $data_send;
        $where['dept_id']     = $this->user_cookie[4];
        $where['year(created_date_po)'] = date("Y");
        $data['budgetc']   = $this->mr_mod->get_utilized_amount($where);
        unset($where);

        $utilized_amount = $data['budgetc'][0]['total'];
        $data['budget'] = ( $budgeted_amount + $transfered_amount ) - $utilized_amount;   

        //print_r($data['budget']);exit;


        if($budget_cat == ""){

            echo 'Error : Choice Account Budget Number';

        } else if($data['budget'] <= 0){
           
            echo 'Error : Insuffcient Budget Amount';

        } 

    }

    public function budget_status($module=null,$cat_id=null,$budget_type=null){
       
        if(isset($cat_id)){
            $data_send = $cat_id;
        } else {
            $data_send = 0; 
        }

        $data['module']    = $module; 

       if($module == 'budget_amount'){
            if($data_send != 0){
                $where['ebc.id']  = $data_send;
                $where['ebd.assetable']  = $budget_type;
                $where['eb.year'] = date("Y");
                $data['budget']   = $this->mr_mod->get_budget_amount($this->user_cookie[4],$where);
                unset($where);
            }
        } else if($module == 'transfered_amount'){
            if($data_send != 0){
                $where['to_category']  = $data_send;
                $where['assetable']   = $budget_type;
                $where['year'] = date("Y");
                $data['budget']   = $this->mr_mod->get_transfered_amount($this->user_cookie[4],$where);
                unset($where);
            }
        } else if($module == 'utilized_amount'){
            if($data_send != 0){
                $where['cat_id']     = $data_send;
                $where['dept_id']    = $this->user_cookie[4];
                $where['year(created_date_po)'] = date("Y");
                $data['budget']   = $this->mr_mod->get_utilized_amount($where);
               
                unset($where);
            }
        } else if($module == 'balance_amount'){
            if($data_send != 0){

                $where['ebc.id']  = $data_send;
                $where['ebd.assetable']  = $budget_type;
                $where['eb.year'] = date("Y");
                $data['budgeta']   = $this->mr_mod->get_budget_amount($this->user_cookie[4],$where);
                unset($where);

                $budgeted_amount = $data['budgeta'][0]['total'];

                $where['to_category']  = $data_send;
                $where['year'] = date("Y");
                $where['assetable']   = $budget_type;
                $data['budgetb']   = $this->mr_mod->get_transfered_amount($this->user_cookie[4],$where);
                unset($where);

                $transfered_amount = $data['budgetb'][0]['total'];

                $where['cat_id']     = $data_send;
                $where['dept_id']    = $this->user_cookie[4];
                $where['year(created_date_po)'] = date("Y");
                $data['budgetc']   = $this->mr_mod->get_utilized_amount($where);
                unset($where);

                $utilized_amount = $data['budgetc'][0]['total'];

                 $data['budget'] = ( $budgeted_amount + $transfered_amount ) - $utilized_amount;

            }
        } else if($module == 'balance_amount_cal'){
            if($data_send != 0){

                $where['ebc.id']  = $data_send;
                $where['ebd.assetable']  = $budget_type;
                $where['eb.year'] = date("Y");
                $data['budgeta']   = $this->mr_mod->get_budget_amount($this->user_cookie[4],$where);
                unset($where);

                $budgeted_amount = $data['budgeta'][0]['total'];

                $where['to_category']  = $data_send;
                $where['year'] = date("Y");
                $where['assetable']   = $budget_type;
                $data['budgetb']   = $this->mr_mod->get_transfered_amount($this->user_cookie[4],$where);
                unset($where);

                $transfered_amount = $data['budgetb'][0]['total'];

                $where['cat_id']     = $data_send;
                $where['dept_id']    = $this->user_cookie[4];
                $where['year(created_date_po)'] = date("Y");
                $data['budgetc']   = $this->mr_mod->get_utilized_amount($where);
                unset($where);

                $utilized_amount = $data['budgetc'][0]['total'];

                 $data['budget'] = ( $budgeted_amount + $transfered_amount ) - $utilized_amount;

            }    

        } 

        $data['meta_title']  = 'Counting Budget';
        $data['data_send']  = $data_send;
        $this->load->view("mr/dashboard/data_count", $data);
    }


    function uniqno_autocomplete(){
        
      if (($this->input->post('term'))){
            $result = $this->mr_mod->search_unique_autocomplete($this->input->post('term'));
            if ($result == TRUE){
                foreach ($result as $row)
                $arr_result[] = $row['code_material'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Material Code - Not Found";
                echo json_encode($arr_result);
            }
        }
    }

   


     public function unique_no_check($unique_no = null){

        $unique_no    = $this->input->post('part_number');
        
        $data_catalog_data      = $this->mr_mod->get_data_catalog_check($unique_no); 


                if(isset($data_catalog_data[0]["code_material"])){
                   
                     $description    = $data_catalog_data[0]["material"];

                     echo $description."; ";
            
                } else {

                    echo "Error : Material Code - Not Found.."; 

                }
        
    }




    //----------------------START DATATABLE SERVER SIDE ----------------------------

    function mr_list_json($status=null){

        $pt_id = $this->user_cookie[12];

        $list = $this->mr_mod->get_datatables_mr_list_dt($status,$pt_id);
      
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $list){
          
            $links = base_url()."mr/mr_detail/".strtr($this->encryption->encrypt($list->mr_number), '+=/', '.-~');

            $datadb = $this->mr_mod->get_user_data();
            foreach ($datadb as $value) {
             $user_data[$value['id_user']] = $value['full_name'];
            }

            $datadb = $this->mr_mod->get_data_department();
            foreach ($datadb as $value) {
             $dept[$value['id_department']] = $value['name_of_department'];
            }


            $datadb = $this->mr_mod->get_data_category();
            foreach ($datadb as $value) {
             $budget[$value['id']] = $value['category_name'];
            }
            
            if($list->status == '1'){
                $status_view = '<span class="badge badge-warning">Open</span>';
            } else if($list->status == '2'){
                $status_view = '<span class="badge badge-danger">Rejected</span>';
            } else if($list->status == '3'){
                $status_view = '<span class="badge badge-success">Approved</span>';    
            }

            if($list->request_type == 'Service Request'){
                $type_mr = "Service Request";
            } else {
                $type_mr = "Material Request";
            }



            $no++;
            $row   = array();
                                 
            $row[] = $budget[$list->posting_budget];
            $row[] = $dept[$list->budget_dept_id]; 
            $row[] = $list->mr_number;  
            $row[] = $type_mr;                           
            $row[] = $user_data[$list->created_by];                   
            $row[] = date("d F Y", strtotime($list->created_date));                   
            $row[] = $status_view;                  
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-list-alt'></i></a>";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw"      => $_POST['draw'],
            "recordsTotal" => $this->mr_mod->count_all_mr_list_dt($status,$pt_id),
            "recordsFiltered" => $this->mr_mod->count_filtered_mr_list_dt($status,$pt_id),
            "data" => $data
        );

        echo json_encode($output);
    }

    //----------------------STOP DATATABLE SERVER SIDE ----------------------------

    //----------------------START DATATABLE SERVER SIDE ----------------------------

    function mr_list_json_tab($status=null){

        $pt_id = $this->user_cookie[12];

        $list = $this->mr_mod->get_datatables_mr_list_dt($status,$pt_id);
      
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $list){
          
            $links = base_url()."mr/mr_detail_tab/".strtr($this->encryption->encrypt($list->mr_number), '+=/', '.-~');

            $get_status = $this->mr_mod->get_status_procurement($list->mr_number);
            $get_status_tabulation = $this->mr_mod->get_status_tabulation($list->mr_number);
            $get_status_quotation = $this->mr_mod->get_status_quotation($list->mr_number);

            $datadb = $this->mr_mod->get_user_data();
            foreach ($datadb as $value) {
             $user_data[$value['id_user']] = $value['full_name'];
            }

            $datadb = $this->mr_mod->get_data_department();
            foreach ($datadb as $value) {
             $dept[$value['id_department']] = $value['name_of_department'];
            }


            $datadb = $this->mr_mod->get_data_category();
            foreach ($datadb as $value) {
             $budget[$value['id']] = $value['category_name'];
            }
            
            if($list->status == '1'){
                $status_view = '<span class="badge badge-warning">Open</span>';
            } else if($list->status == '2'){
                $status_view = '<span class="badge badge-danger">Rejected</span>';
            } else if($list->status == '3'){
                $status_view = '<span class="badge badge-success">Approved</span>';    
            }

            if($list->request_type == 'Service Request'){
                $type_mr = "Service Request";
            } else {
                $type_mr = "Material Request";
            }

             if(sizeof($get_status) > 0 OR sizeof($get_status_tabulation) <= 0 OR sizeof($get_status_quotation) <= 0){
                $status_proc = '<span class="badge badge-warning">Waiting Process</span>';
            } else {
                $status_proc = '<span class="badge badge-success">Completed</span>';    
            }

            $no++;
            $row   = array();
                                 
            $row[] = $budget[$list->posting_budget];
            $row[] = $dept[$list->budget_dept_id]; 
            $row[] = $list->mr_number;  
            $row[] = $type_mr;                           
            $row[] = $user_data[$list->created_by];                   
            $row[] = date("d F Y", strtotime($list->created_date));                   
            $row[] = $status_view;                   
            $row[] = $status_proc;                   
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-list-alt'></i></a>";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw"      => $_POST['draw'],
            "recordsTotal" => $this->mr_mod->count_all_mr_list_dt($status,$pt_id),
            "recordsFiltered" => $this->mr_mod->count_filtered_mr_list_dt($status,$pt_id),
            "data" => $data
        );

        echo json_encode($output);
    }

    //----------------------STOP DATATABLE SERVER SIDE ----------------------------

    
    public function mr_detail($mr_number = null){

        $mr_number = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));

        if(empty($mr_number)){ 
            redirect('mr_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->mr_mod->mr_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_detail']     = $this->mr_mod->mr_list_detail($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_quo']        = $this->mr_mod->mr_quotation_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_tabulation'] = $this->mr_mod->mr_tabulation_list($where);
        unset($where);

       
        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_department();
        foreach ($datadb as $value) {
           $data['dept'][$value['id_department']] = $value['name_of_department'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);
       
        $data['src_total_mr_detail'] = sizeof($data['mr_list_detail']);
        $data['read_cookies']        = $this->user_cookie;
        $data['read_permission']     = $this->permission_cookie;
       
        $data['meta_title']          = "Form Material & Service Request";
        $data['subview']             = 'mr/mr_detail';
        $data['sidebar']             = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    public function mr_detail_tab($mr_number = null){

        $mr_number = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));

        if(empty($mr_number)){ 
            redirect('mr_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->mr_mod->mr_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_detail']     = $this->mr_mod->mr_list_detail($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_quo']        = $this->mr_mod->mr_quotation_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_tabulation'] = $this->mr_mod->mr_tabulation_list($where);
        unset($where);

       
        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_department();
        foreach ($datadb as $value) {
           $data['dept'][$value['id_department']] = $value['name_of_department'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);
       
        $data['src_total_mr_detail'] = sizeof($data['mr_list_detail']);
        $data['read_cookies']        = $this->user_cookie;
        $data['read_permission']     = $this->permission_cookie;
       
        $data['meta_title']          = "Form Material & Service Request";
        $data['subview']             = 'mr/mr_tab';
        $data['sidebar']             = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    public function mr_pdf_format($mr_number = null){

        $mr_number = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));

        if(empty($mr_number)){ 
            redirect('mr_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->mr_mod->mr_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_detail']     = $this->mr_mod->mr_list_detail($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_quo']        = $this->mr_mod->mr_quotation_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_tabulation'] = $this->mr_mod->mr_tabulation_list($where);
        unset($where);

       
        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_department();
        foreach ($datadb as $value) {
           $data['dept'][$value['id_department']] = $value['name_of_department'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }

        $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom_data'][$value['id_uom']] = $value['uom'];
        }

        $where['eb.year'] = date("Y");
        $datadb = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        foreach ($datadb as $value) {
           $data['budget_data'][$value['id']] = $value['account_no'];
        }
        unset($where);

        $data['user_list']    = $this->mr_mod->get_user_data();

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);
       
        $data['src_total_mr_detail'] = sizeof($data['mr_list_detail']);
        $data['read_cookies']        = $this->user_cookie;
        $data['read_permission']     = $this->permission_cookie;
       
        $data['meta_title']          = "Form Material & Service Request";
      
        $this->load->view('mr/mr_pdf_format', $data);

        $this->load->library('Pdfgenerator_potrait');
        $html = $this->load->view('mr/mr_pdf_format', $data,true);
        $this->pdfgenerator_potrait->generate($html,$mr_number);
        

    }

    public function mr_update($mr_number = null){

        $mr_number = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));

        if(empty($mr_number)){ 
            redirect('mr_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->mr_mod->mr_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_detail']     = $this->mr_mod->mr_list_detail($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_quo']        = $this->mr_mod->mr_quotation_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_tabulation'] = $this->mr_mod->mr_tabulation_list($where);
        unset($where);

       
        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_department();
        foreach ($datadb as $value) {
           $data['dept'][$value['id_department']] = $value['name_of_department'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);
       
        $data['src_total_mr_detail'] = sizeof($data['mr_list_detail']);
        $data['read_cookies']        = $this->user_cookie;
        $data['read_permission']     = $this->permission_cookie;
       
        $data['meta_title']          = $data['mr_list'][0]['form_number'];
        $data['subview']             = 'mr/mr_update';
        $data['sidebar']             = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    /*
    public function mr_add_process(){

         $created_by       = $this->user_cookie[0];
         $mr_number        = $this->mr_mod->generate_batch_no();
         //$form_no          = $this->input->post('form_no');
         $budget_cat_id    = $this->input->post('budget');
         $budget_dept_id   = $this->input->post('dept_id');
         $created_date     = $this->input->post('created_date');

         $code_material    = $this->input->post('part_number');
         $qty              = $this->input->post('req_qty');
         $uom              = $this->input->post('uom');
         $expected_cost    = $this->input->post('expected_cost');
         $total_amount     = $this->input->post('total_amount');
         $currency         = $this->input->post('currency');
         $remarks          = $this->input->post('remarks');

         $id_vendor         = $this->input->post('vendor');
         $amount            = $this->input->post('total_amount_quo');
         $currency_quo      = $this->input->post('currency_quo');
         $id_attachment     = $this->input->post('id_attachment');

         $form_number = $mr_number."/MEB-M&R-BTM/XII/".date("Y"); 


         //------------quo table-----------//   

        foreach ($id_vendor as $key => $value) {

                $file_name = $mr_number."-".$id_vendor[$key]."-".date("Ymd_His").".pdf";
                $file_input_name = 'file_att_'.$id_attachment[$key];

                $config['upload_path']   = 'upload/mr/quotation';
                $config['file_name']     = $file_name;
                $config['allowed_types'] = 'pdf';
                $config['max_size']      = '10000';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload($file_input_name)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect("mr/mr_new");
                    return false;
                }

                $data_mr_detail = array(
                        "mr_number"    => $mr_number,
                        "id_vendor"    => $id_vendor[$key],
                        "amount"       => $amount[$key],
                        "currency"     => $currency_quo[$key],
                        "attachment"   => $file_name,
                        "status"       => 1,
                        );
                
                $this->mr_mod->mr_quo_new_process_db($data_mr_detail);
       
         }

        //------------quo table-----------//  

        //------------tabulation table-----------//  

        if($_FILES['file_tabulation']['size'] != 0) {

                $file_name_tab = $mr_number."-".date("Ymd_His").".pdf";
                $file_input_name_tab = 'file_tabulation';

                $config_tab['upload_path']   = 'upload/mr/tabulation';
                $config_tab['file_name']     = $file_name_tab;
                $config_tab['allowed_types'] = 'pdf';
                $config_tab['max_size']      = '10000';

                $this->load->library('upload', $config_tab);
                $this->upload->initialize($config_tab);

                if ( ! $this->upload->do_upload($file_input_name_tab)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect("mr/mr_new");
                    return false;
                }

                $data_mr_tab = array(
                        "mr_number"    => $mr_number,
                        "filename"    => $file_name_tab,
                        "status"       => 1,
                        );
                
                $this->mr_mod->mr_tab_new_process_db($data_mr_tab);
        }
        //------------tabulation table-----------//   

        //------------main table-----------//

        $data_mr = array(
                        "budget_cat_id"  => $budget_cat_id,
                        "budget_dept_id" => $budget_dept_id,
                        "form_number"    => $form_number,
                        "mr_number"      => $mr_number,
                        "created_date"   => $created_date,
                        "created_by"     => $created_by,
                        "status"         => 1
                        );


        $this->mr_mod->mr_new_process_db($data_mr); 

        //------------main table-----------//

        //------------detail table-----------//

         foreach ($code_material as $key => $value) {
        
                $data_mr_detail = array(
                        "mr_number"         => $mr_number,
                        "code_material"     => $code_material[$key],
                        "qty"               => $qty[$key],
                        "uom"               => $uom[$key],
                        "expected_cost"     => $expected_cost[$key],
                        "total_amount"      => $total_amount[$key],
                        "currency"          => $currency[$key],
                        "remarks"           => $remarks[$key],
                        "status"            => 1,
                        );
                
               $this->mr_mod->mr_detail_new_process_db($data_mr_detail);

         }

        //------------detail table-----------//  

        

        $this->session->set_flashdata('message','<script type="text/javascript">Swal.fire({type: "success",title: "Success",text: "MR has been successfully created under MR No :'.$form_number.'",}).then(function() { window.location = "'.base_url().'mr/mr_list"});</script>');

        redirect('mr/mr_list/');  
    }

    */

    public function mr_add_process(){

         
         $mr_number        = $this->mr_mod->generate_batch_no();
      
         $pt_id            = $this->user_cookie[12];
         $posting_budget   = $this->input->post('posting_budget');
         $budget_dept_id   = $this->input->post('dept_id');


         $project           = $this->input->post('project');
         $posting_budget    = $this->input->post('posting_budget');

         $request_sche      = $this->input->post('request_sche');
         $request_type      = $this->input->post('request_type');
         $request_checklist = $this->input->post('request_checklist');
         $budget_type       = $this->input->post('budget_type');
       

         $drawing                    = $this->input->post('drawing');
         $justification_drawing      = $this->input->post('justification_drawing');
         $term_of_reference          = $this->input->post('term_of_reference');
         $justification_of_reference = $this->input->post('justification_of_reference');
         $picture                    = $this->input->post('picture');
         $justification_picture      = $this->input->post('justification_picture');
         $email_correspondences      = $this->input->post('email_correspondences');
         $justification_email        = $this->input->post('justification_email');

         $created_by       = $this->user_cookie[0];
         $created_date     = $this->input->post('date');


          $drawing_attach  = $this->input->post('drawing_attach');
          $term_attach     = $this->input->post('term_attach');
          $pic_attach      = $this->input->post('pic_attach');
          $email_attach    = $this->input->post('email_attach');


          //------------main table-----------//

      
         $nos_so               = $this->input->post('nos_so');
         $tec_spec_so          = $this->input->post('tec_spec_so');

         $code_material_so     = $this->input->post('part_number_so');

         $qty_req_so           = $this->input->post('qty_req_so');
         $uom_req_so           = $this->input->post('uom_req_so');

         //$qty_stock_so         = $this->input->post('qty_stock_so');
         //$uom_stock_so         = $this->input->post('uom_stock_so');

         $qty_order_so         = $this->input->post('qty_order_so');
         $uom_order_so         = $this->input->post('uom_order_so');

         $currency_so          = $this->input->post('currency_so');
         $estimate_price_so    = $this->input->post('estimate_price_so');

      
        //------------detail table-----------//


          if($_FILES['drawing_attach']['size'] != 0) {

               $path = $_FILES['drawing_attach']['name'];
               $ext = pathinfo($path, PATHINFO_EXTENSION);

                $file_name_drawing = $mr_number."-DWG-".date("Ymd_His").".".$ext;
                $file_input_name = 'drawing_attach';

                $config['upload_path']   = 'upload/mr/drawing';
                $config['file_name']     = $file_name_drawing;
                $config['allowed_types'] = '*';
                $config['max_size']      = '10000';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload($file_input_name)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect("mr/mr_new");
                    return false;
                }

          } else {
            $file_name_drawing = '-';
          }

          if($_FILES['term_attach']['size'] != 0) {

                $path = $_FILES['term_attach']['name'];
               $ext = pathinfo($path, PATHINFO_EXTENSION);

                $file_name_term = $mr_number."-term-".date("Ymd_His").".".$ext;
                $file_input_name = 'term_attach';

                $config['upload_path']   = 'upload/mr/term';
                $config['file_name']     = $file_name_term;
                $config['allowed_types'] = '*';
                $config['max_size']      = '10000';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload($file_input_name)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect("mr/mr_new");
                    return false;
                }
            
          } else {
            $file_name_term = '-';
          }

          if($_FILES['pic_attach']['size'] != 0) {

                $path = $_FILES['pic_attach']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);

                $file_name_pic = $mr_number."-picture-".date("Ymd_His").".".$ext;
                $file_input_name = 'pic_attach';

                $config['upload_path']   = 'upload/mr/picture';
                $config['file_name']     = $file_name_pic;
                $config['allowed_types'] = '*';
                $config['max_size']      = '10000';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload($file_input_name)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect("mr/mr_new");
                    return false;
                }
            
          } else {
            $file_name_pic = '-';
          }

          if($_FILES['email_attach']['size'] != 0) {

                $path = $_FILES['email_attach']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);

                $file_name_email = $mr_number."-email-".date("Ymd_His").".".$ext;
                $file_input_name = 'email_attach';

                $config['upload_path']   = 'upload/mr/email';
                $config['file_name']     = $file_name_email;
                $config['allowed_types'] = '*';
                $config['max_size']      = '10000';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload($file_input_name)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect("mr/mr_new");
                    return false;
                }
            
          }else {
            $file_name_email = '-';
          }

        //------------main table-----------//

        $data_mr = array(
                        "pt_id"             => $pt_id,
                        "mr_number"         => $mr_number,
                        "budget_cat_id"     => $posting_budget,
                        "budget_dept_id"    => $budget_dept_id,
                       
                        "project"           => $project,                        
                        "posting_budget"    => $posting_budget,

                        "request_sche"      => $request_sche,
                        "request_type"      => $request_type,
                        "request_checklist" => $request_checklist,
                        "budget_type"       => $budget_type,                        

                        "drawing"           => $drawing,
                        "justification_drawing" => $justification_drawing,
                        "term_of_reference"     => $term_of_reference,
                        "justification_of_reference" => $justification_of_reference,
                        "picture"                => $picture,
                        "justification_picture"  => $justification_picture,
                        "email_correspondences"  => $email_correspondences,
                        "justification_email"    => $justification_email,

                        "drawing_attach"    => $file_name_drawing,
                        "term_attach"       => $file_name_term,
                        "pic_attach"        => $file_name_pic,
                        "email_attach"      => $file_name_email,

                        "created_by"        => $created_by,
                        "created_date"      => $created_date,
                        "status"            => 1,
                    );


        $this->mr_mod->mr_new_process_db($data_mr); 

         
            foreach ($nos_so as $key => $value) {
        
                $data_mr_detail = array(
                        "mr_number"     => $mr_number,
                        "code_material" => $code_material_so[$key],
                        "tec_spec"      => $tec_spec_so[$key],
                        "qty_req"       => $qty_req_so[$key],
                        "uom_req"       => $uom_req_so[$key],
                        //"qty_stock"     => $qty_stock_so[$key],
                       // "uom_stock"     => $uom_stock_so[$key],
                        "qty_order"     => $qty_order_so[$key],
                        "uom_order"     => $uom_order_so[$key],
                        "currency"      => $currency_so[$key],
                        "estimate_price"=> $estimate_price_so[$key],
                        "status"        => 1,
                        );
                
               $this->mr_mod->mr_detail_new_process_db($data_mr_detail);

            }
        

        $this->session->set_flashdata('message','<script type="text/javascript">Swal.fire({type: "success",title: "Success",text: "MR has been successfully created under MR No :'.$form_number.'",}).then(function() { window.location = "'.base_url().'mr/mr_list"});</script>');

        redirect('mr/mr_list/');  
    }


    public function process_update_mr(){

        $submitBtn  = $this->input->post('submitBtn');
        $mr_number  = $this->input->post('mr_number');
        $form_no    = $this->input->post('form_no');
         
        if($submitBtn == "approve"){
            $status_upd = "3";
            $alert      = "MR Approved..";
        } else if($submitBtn == "reject"){
            $status_upd = "2";
            $alert      = "MR Rejected..";
        }

        if($submitBtn != "submit"){

            if($submitBtn == "approve"){

                $dt_id = $mr_number; 

                $data_approval = array ( "status" => $status_upd, "manager_on_duty"  => $this->user_cookie[0], "manager_app_status"  => $status_upd, "manager_app_date" => date("Y-m-d H:i:s"), );

                $where["mr_number"] = $dt_id;
                
                $this->mr_mod->mr_update_data_db($data_approval,$where);

                 $this->session->set_flashdata('message','<script type="text/javascript">Swal.fire({type: "success",title: "Success",text: "'.$alert.'",}).then(function() { window.location = "'.base_url().'mr/mr_list/approved"});</script>');

                redirect('mr/mr_list/approved');

            } else {

                $dt_id = $mr_number; 

                 $data_approval = array ( "status" => $status_upd, "manager_on_duty"  => $this->user_cookie[0], "manager_app_status"  => $status_upd, "manager_app_date" => date("Y-m-d H:i:s"), );


                $where["mr_number"] = $dt_id;
                
                $this->mr_mod->mr_update_data_db($data_approval,$where);

                $this->session->set_flashdata('message','<script type="text/javascript">Swal.fire({type: "success",title: "Success",text: "'.$alert.'",}).then(function() { window.location = "'.base_url().'mr/mr_list/rejected"});</script>');

                redirect('mr/mr_list/rejected');

            }

         } else if($submitBtn == "submit"){

            $created_by        = $this->user_cookie[0];

            $id_vendor         = $this->input->post('vendor');
            $amount            = $this->input->post('total_amount_quo');
            $currency_quo      = $this->input->post('currency_quo');
            $id_attachment     = $this->input->post('id_attachment');
            $reference_no      = $this->input->post('reference_no');

            $vendor_data       = $this->input->post('vendor');
            $id_mr_detail      = $this->input->post('id_mr_detail');

            //print_r(sizeof($id_vendor));exit;

            //------------Selected Vendor Table-----------//

             foreach ($vendor_data as $key => $value) {

                    $where["id"] = $id_mr_detail[$key];

                    $data_mr_detail = array(
                        "vendor_winner"  =>  $vendor_data[$key],
                        "vendor_winner_update_by"  =>  $this->user_cookie[0],
                        "vendor_winner_update_date"  => date("Y-m-d H:i:s"),
                    );

                   $update = $this->mr_mod->mr_detail_edit_process_db($data_mr_detail,$where);                
             }

            //------------Selected Vendor Table-----------//   

            //------------quo table-----------//   

             if(sizeof($id_attachment) > 0){

            foreach ($id_attachment as $key => $value) {
                            
                $file_input_name = 'file_att_'.$id_attachment[$key];

                $path = $_FILES[$file_input_name]['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);

                $file_name = $mr_number."-".$id_vendor[$key]."-".date("Ymd_His").".".$ext;

                $config['upload_path']   = 'upload/mr/quotation';
                $config['file_name']     = $file_name;
                $config['allowed_types'] = '*';
                $config['max_size']      = '10000';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload($file_input_name)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                     redirect("mr/mr_detail_tab/".strtr($this->encryption->encrypt($mr_number), '+=/', '.-~'));
                    return false;
                }

                $data_mr_detail = array(
                        "mr_number"    => $mr_number,
                        "reference_no" => $reference_no[$key],
                        "id_vendor"    => $id_vendor[$key],
                        "amount"       => $amount[$key],
                        "currency"     => $currency_quo[$key],
                        "attachment"   => $file_name,
                        "status"       => 1,
                        );
                
                $this->mr_mod->mr_quo_new_process_db($data_mr_detail);
       
                }
            }

           //------------quo table-----------//  

           //------------detail tabulation-----------// 

            $nosX    = $this->input->post('nosX');

            if(sizeof($nosX) > 0){

            foreach ($nosX as $key => $value) {

                $file_input_name = 'file_tabulation_'.$nosX[$key];

                $path = $_FILES[$file_input_name]['name'];
                $ext  = pathinfo($path, PATHINFO_EXTENSION);

                $file_name = $mr_number."-".$nosX[$key]."-".date("Ymd_His").".".$ext;

                $config['upload_path']   = 'upload/mr/tabulation';
                $config['file_name']     = $file_name;
                $config['allowed_types'] = '*';
                $config['max_size']      = '10000';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( ! $this->upload->do_upload($file_input_name)){
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                     redirect("mr/mr_detail_tab/".strtr($this->encryption->encrypt($mr_number), '+=/', '.-~'));
                    return false;
                }

                $data_mr_tab = array(
                        "mr_number"   => $mr_number,
                        "filename"    => $file_name,
                        "created_by"  => $created_by,
                        "status"      => 1,
                        );
                
                $this->mr_mod->mr_tab_new_process_db($data_mr_tab);
       
                }
            }

          //------------detail tabulation-----------// 

         $alertx = "MR Updated..";
         $this->session->set_flashdata('success', "Data saved!");

        redirect("mr/mr_detail_tab/".strtr($this->encryption->encrypt($mr_number), '+=/', '.-~'));

         }
         
    }

    

    function delete_detail_quo($id_quotation,$mr_number){

        $this->mr_mod->delete_mr_quo($id_quotation);

        redirect("mr/mr_update/".strtr($this->encryption->encrypt($mr_number), '+=/', '.-~'));

    }

    public function mr_pdf($mr_no){

        $mr_number = $this->encryption->decrypt(strtr($mr_no, '.-~', '+=/'));
        
        

        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        foreach ($datadb as $value) {
            $data['material'][$value['id']] = $value['material'];
        }
        
        $where['mr_number']     = $mr_number;
        $data['mr_list']        = $this->mr_mod->mr_list($where);
        unset($where);
        $where['mr_number']     = $mr_number;
        $data['mr_list_detail'] = $this->mr_mod->mr_list_detail($where);
        unset($where);
        
    $this->load->library('Pdfgenerator');
    $html = $this->load->view('mr/mr_pdf', $data,true);
    //$html = $this->load->view('mr/mr_pdf', $data);
    $this->pdfgenerator->generate($html,$mr_number);
    
    }

    public function mr_excel($mr_no){

        $mr_number = $this->encryption->decrypt(strtr($mr_no, '.-~', '+=/'));
        
  
        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        foreach ($datadb as $value) {
            $data['material'][$value['id']] = $value['material'];
        }

       
        
        $where['mr_number']     = $mr_number;
        $data['mr_list']        = $this->mr_mod->mr_list($where);
        unset($where);
        $where['mr_number']     = $mr_number;
        $data['mr_list_detail'] = $this->mr_mod->mr_list_detail($where);
        unset($where);
        
    //$this->load->library('Pdfgenerator');

    //$html = $this->load->view('mr/mr_pdf', $data,true);
    $html = $this->load->view('mr/mr_excel', $data);
    
    //$this->pdfgenerator->generate($html,$mr_number);
    }

    public function mr_pdf_2($mr_no){

        $mr_number = $this->encryption->decrypt(strtr($mr_no, '.-~', '+=/'));

        $datadb = $this->mr_mod->get_project();
        foreach ($datadb as $value) {
            $data['project_list'][$value['id']] = $value['project_name'];
            $data['project_code'][$value['id']] = $value['project_code'];
        }
        
        $datadb = $this->mr_mod->get_module();
        foreach ($datadb as $value) {
            $data['module'][$value['mod_id']] = $value['mod_desc'];
        }

        $datadb = $this->mr_mod->get_priority();
        foreach ($datadb as $value) {
            $data['priority'][$value['id']] = $value['priority_name'];
        }

        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        foreach ($datadb as $value) {
            $data['material'][$value['id']] = $value['material'];
        }

        
        foreach ($datadb as $value) {
            $data['grade'][$value['id']] = $value['material_grade'];
        }

        foreach ($datadb as $value) {
            $data['thk_mm'][$value['id']] = $value['thk_mm'];
        }

        foreach ($datadb as $value) {
            $data['weight'][$value['id']] = $value['weight'];
        }

        foreach ($datadb as $value) {
            $data['width_m'][$value['id']] = $value['width_m'];
        }

        foreach ($datadb as $value) {
            $data['length_m'][$value['id']] = $value['length_m'];
        }

        foreach ($datadb as $value) {
            $data['od'][$value['id']] = $value['od'];
        }

        foreach ($datadb as $value) {
            $data['sch'][$value['id']] = $value['sch'];
        }

        
        $where['mr_number']     = $mr_number;
        $data['mr_list']        = $this->mr_mod->mr_list($where);
        unset($where);
        $where['mr_number']     = $mr_number;
        $data['mr_list_detail'] = $this->mr_mod->mr_list_detail($where);
        unset($where);
        
    $this->load->library('Pdfgenerator_potrait');
    $html = $this->load->view('mr/mr_pdf', $data,true);
    //$html = $this->load->view('mr/mr_pdf', $data);
    $this->pdfgenerator_potrait->generate($html,$mr_number);
    
    }

    public function mr_excel_2($mr_no){

        $mr_number = $this->encryption->decrypt(strtr($mr_no, '.-~', '+=/'));
        
        $datadb = $this->mr_mod->get_project();
        foreach ($datadb as $value) {
            $data['project_list'][$value['id']] = $value['project_name'];
            $data['project_code'][$value['id']] = $value['project_code'];
        }
        
        $datadb = $this->mr_mod->get_module();
        foreach ($datadb as $value) {
            $data['module'][$value['mod_id']] = $value['mod_desc'];
        }

        $datadb = $this->mr_mod->get_priority();
        foreach ($datadb as $value) {
            $data['priority'][$value['id']] = $value['priority_name'];
        }

        $datadb = $this->mr_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['code_material'][$value['id']] = $value['code_material'];
        }

        foreach ($datadb as $value) {
            $data['material'][$value['id']] = $value['material'];
        }

       
        foreach ($datadb as $value) {
            $data['grade'][$value['id']] = $value['material_grade'];
        }

        foreach ($datadb as $value) {
            $data['thk_mm'][$value['id']] = $value['thk_mm'];
        }

        foreach ($datadb as $value) {
            $data['width_m'][$value['id']] = $value['width_m'];
        }

        foreach ($datadb as $value) {
            $data['length_m'][$value['id']] = $value['length_m'];
        }

        
        $where['mr_number']     = $mr_number;
        $data['mr_list']        = $this->mr_mod->mr_list($where);
        unset($where);
        $where['mr_number']     = $mr_number;
        $data['mr_list_detail'] = $this->mr_mod->mr_list_detail($where);
        unset($where);
        
    //$this->load->library('Pdfgenerator');

    //$html = $this->load->view('mr/mr_pdf', $data,true);
    $html = $this->load->view('mr/mr_excel', $data);
    
    //$this->pdfgenerator->generate($html,$mr_number);
    }

    public function delete_quotation($id_quotation){

      $this->mr_mod->delete_quotation($id_quotation);

      echo "<script>javascript:history.back();</script>";

    }

    public function delete_tabulation($id_tabulation){

      $this->mr_mod->delete_tabulation($id_tabulation);

      echo "<script>javascript:history.back();</script>";

    }


    public function delete_material_id($id){

      $this->mr_mod->delete_material_id($id);

      echo "<script>javascript:history.back();</script>";

    }

}