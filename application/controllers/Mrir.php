<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class MRIR extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		//check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->helper('approval');
		
		$this->load->model('home_mod');
		$this->load->model('general_mod');
		$this->load->model('Mrir_mod');

		//Read Receiving Table
		$this->load->model('receiving_mod');
		$this->load->model('Material_catalog_mod');
		$this->load->model('po_mod');
		$this->load->model('osd_mod');
		$this->load->model('department_mod');
		$this->load->model('mr_mod');

		$this->user_cookie 		 = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
		
		$this->sidebar 			 = "mrir/sidebar";

	}

	public function index(){
		redirect('mrir/mrir_data/request');
	}

	//FOR DISCIPLINE ==========================================
	public function discipline(){
		$data['read_cookies'] 	  = $this->user_cookie;
		$data['meta_title'] 	  = 'Discipline Management';
		$data['subview']    	  = 'mrir/material/discipline';
		$data['sidebar']    	  = $this->sidebar;
		$data['query']    		  = $this->Mrir_mod->data_discipline();
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function add_discipline(){
		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Add New Discipline';
		$data['subview']    	= 'mrir/material/add_discipline';
		$data['sidebar']    	= $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function add_discipline_process(){		
		$data = array(
				'initial' => $this->input->post('initial_name'),
				'discipline_name' => $this->input->post('discipline_name'),
				'status' => 1
			);

		$this->Mrir_mod->add_discipline($data);

		$this->session->set_flashdata('success', 'Data added successfull!');

		redirect('mrir/discipline');
	}

	public function edit_discipline(){
		$data['id'] = $this->input->get('id');
		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Edit Discipline';
		$data['subview']    	= 'mrir/material/edit_discipline';
		$data['sidebar']    	= $this->sidebar;
		$data['query']    		= $this->Mrir_mod->get_discipline($this->input->get('id'));
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function edit_discipline_process(){	
		$id = $this->input->post('id');	

		$data = array(
				'id' => $id,
				'initial' => $this->input->post('initial_name'),
				'discipline_name' => $this->input->post('discipline_name'),
				'status' => 1
			);

		$this->Mrir_mod->edit_discipline($id, $data);

		$this->session->set_flashdata('success', 'Success!');

		redirect('mrir/discipline');
	}

	//=========================================================


	//MRIR ===================================================

	//MENU MRIR DISCIPLINE (PIPING & STRUCTURE)
	public function mrir_discipline($param){
		$data['read_cookies'] 	= $this->user_cookie;

		$id = $this->encryption->decrypt(strtr($param, '.-~', '+=/'));

		//print_r($id);
		//return false;

		$where['id'] = $id;
		$data_discipline = $this->general_mod->data_discipline($where);
		unset($where);

		$discipline_name = $data_discipline[0]['discipline_name'];
		$discipline_id = $data_discipline[0]['id'];

		$data['discipline_name'] = $discipline_name;
		$data['meta_title'] 	= 'Material Receiving Report - '.$discipline_name;
		$data['subview']    	= 'mrir/material/mrir_discipline';
		$data['sidebar']    	= $this->sidebar;
		$data['query']    		= $this->Mrir_mod->mrir_data($discipline_id,$this->user_cookie[10]);
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	//MATERIAL ==============================================
	public function material_list(){
		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Material List';
		$data['subview']    	= 'mrir/material/material';
		$data['sidebar']    	= $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;

		$this->load->view('index', $data);
	}

	public function add_material(){
		$report_number	= $this->input->get('report_number');
		$discipline		= $this->input->get('discipline');

		$data['report_number']	= $report_number;
		$data['discipline']		= $discipline;
		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Add New Material';
		$data['subview']    	= 'mrir/material/add_material';
		$data['sidebar']    	= $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;

		$data['q_discipline']	= $this->Mrir_mod->data_discipline();
		$data['q_mrir']			= $this->Mrir_mod->detail_mrr($report_number, $discipline);

		$this->load->view('index', $data);
	}

	public function add_material_proccess(){

		$report_no = $this->input->post('report_no');
		$discipline = $this->input->post('discipline');

		$data = array(
				'project_id' 		=> 1,
				'report_no'			=> $this->input->post('report_no'),
				'category' 			=> $this->input->post('category'),
				'discipline' 		=> $this->input->post('discipline'),
				'description' 		=> $this->input->post('description'),
				'length' 			=> $this->input->post('length'),
				'width_or_od' 		=> $this->input->post('width_or_od'),
				'sch_or_thk' 		=> $this->input->post('sch_or_thk'),
				'spec' 				=> $this->input->post('spec'),
				'spec_category' 	=> $this->input->post('spec_category'),
				'type' 				=> $this->input->post('type'),
				'plate_or_tag_no'	=> $this->input->post('plate_or_tag_no'),
				'heat_or_series_no' => $this->input->post('heat_or_series_no'),
				'ceq' 				=> $this->input->post('ceq'),
				'mill_cert_no' 		=> $this->input->post('mill_cert_no'),
				'date_manufacturing'=> date('Y-m-d', strtotime($this->input->post('date_manufacturing'))),
				'supplier_name'		=> $this->input->post('supplier_name'),
				'do_or_pl_no' 		=> $this->input->post('do_or_pl_no'),
				'unique_ident_no' 	=> $this->input->post('unique_ident_no'),
				'qty'	 			=> $this->input->post('qty'),
				'uom'	 			=> $this->input->post('uom'),
				'color_code' 		=> $this->input->post('color_code'),
				'remarks' 			=> '',
				'location' 			=> '',
				'status' 			=> '',
				'is_imported' 		=> 0,
				'timestamp' 		=> date('Y-m-d H:i:s')
			);

		$this->Mrir_mod->add_material($data);

		$this->session->set_flashdata('success', 'Data added successfull!');

		redirect('mrir/detail_mrr/?report_number='.$report_no.'&discipline='.$discipline);
	}
	//=======================================================

	//ACTION ADD MRIR

	public function mrir_for_review($doplNumber){		

		$report_number	= $this->encryption->decrypt(strtr($doplNumber, '.-~', '+=/'));

		$data['read_permission']  	= $this->permission_cookie;
		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'Create MRIR For DO / PL : '.$report_number;
		$data['subview']    		= 'mrir/mrir/mrir_review';
		$data['sidebar']    		= $this->sidebar;

		$data['get_mrir_no']        = $this->Mrir_mod->generate_batch_no();
		$data['material']			= $this->Mrir_mod->get_mrir_review($report_number);

		$data['mto']				= $this->Mrir_mod->get_mto_data($data['material'][0]['mto_id']);
		$data['get_drawing_data']	= $this->Mrir_mod->get_drawing_data($data['mto'][0]['drawing_no']);

		//print_r($data['get_drawing_data'][0]["discipline"]);
		//return false;

		
		$this->load->view('index', $data);

	}

	public function insert_mrir_data(){		

		$data['read_permission']  	= $this->permission_cookie;
		$data['read_cookies'] 		= $this->user_cookie;

		$project_id 	= $this->input->post('project_id');
		$report_no 		= $this->input->post('report_no');
		$do_no 			= $this->input->post('do_no');
		$vendorMaster 	= $this->input->post('vendor');
		$date_receiving = $this->input->post('date_receiving');
		$discipline 	= $this->input->post('discipline');
		$comments 		= $this->input->post('comments');
		$created_date 	= $this->input->post('created_date');
		$status 		= $this->input->post('status');
		$created_by 	= $this->user_cookie[0];

		$form_master = array(
			"project_id" 		=> $project_id,
			"report_no" 		=> $report_no,
			"do_no" 			=> $do_no,
			"vendor" 		 	=> $vendorMaster,
			"date_receiving" 	=> $date_receiving,
			"discipline" 		=> $discipline,
			"comments" 			=> $comments,
			"created_date" 		=> $created_date,
			"status" 			=> $status,
			"created_by" 		=> $created_by
		);
		
		$this->Mrir_mod->insert_master_mrir($form_master);

		$uniq_no 			= $this->input->post('uniq_no');
		$do_pl1 			= $this->input->post('do_pl');
		$category 			= $this->input->post('category');
		$po_item 			= $this->input->post('po_item');
		$vendor 			= $this->input->post('vendorx');
		$material 			= $this->input->post('material');
		$length 			= $this->input->post('length');
		$width_od 			= $this->input->post('width_od');
		$sch_thk 			= $this->input->post('sch_thk');
		$spec 				= $this->input->post('spec');
		$spec_category 		= $this->input->post('spec_category');
		$type 			 	= $this->input->post('type');
		$plate_or_tag_no 	= $this->input->post('plate_or_tag_no');
		$heat_no 			= $this->input->post('heat_no');
		$ceq 				= $this->input->post('ceq');
		$mill_certificate 	= $this->input->post('mill_certificate');
		$date_manufacturing = $this->input->post('date_manufacturing');
		$country_origin 	= $this->input->post('country_origin');
		$qty 				= $this->input->post('qty');
		$uom 				= $this->input->post('uom');
		$color_code 		= $this->input->post('color_code');
		$brand 				= $this->input->post('brand');
		$supplier_name 		= $this->input->post('supplier_name');
		
		foreach ($uniq_no as $key => $value) {
				
				$form_detail = array(

					"ceq" 					=> $ceq[$key],
					"project_id" 			=> $project_id,
					"report_no" 			=> $report_no,
					"type" 					=> $type[$key],
					"description" 			=> $material[$key],
					"length" 				=> $length[$key],
					"width_or_od" 			=> $width_od[$key],
					"sch_or_thk" 			=> $sch_thk[$key],
					"spec" 					=> $spec[$key],
					"spec_category" 		=> $spec_category[$key],
					"plate_or_tag_no" 		=> $plate_or_tag_no[$key],	
					"heat_or_series_no" 	=> $heat_no[$key],
					"mill_cert_no" 			=> $mill_certificate[$key],
					"supplier_name" 		=> $supplier_name[$key],
					"country_origin" 		=> $country_origin[$key],
					"do_or_pl_no" 			=> $do_pl1[$key],
					"unique_ident_no" 		=> $uniq_no[$key],
					"qty" 					=> $qty[$key],
					"uom" 					=> $uom[$key],
					"color_code" 			=> $color_code[$key],
					"discipline" 			=> $discipline,
					"status" 				=> 0,
					"category" 				=> $category[$key],
					"date_manufacturing" 	=> $date_manufacturing[$key],
					"po_number" 			=> $po_item[$key],
					"vendor" 				=> $vendor[$key],
					"brand" 				=> $brand[$key],
					
				);

				$this->Mrir_mod->insert_detail_mrir($form_detail);
				
		}

		


		$this->session->set_flashdata('success', 'Success!');

		//redirect('mrir/mrir_data/request');
		//redirect('mrir/mrir_discipline/'.strtr($this->encryption->encrypt($discipline),'+=/', '.-~'));
		redirect('mrir/detail_mrr/?report_number='.$report_no.'&discipline='.$discipline);

	}

	public function detail_mrr(){

		$report_number	= $this->input->get('report_number');
		$discipline		= $this->input->get('discipline');

		if($discipline == "1"){
			$id_discipline = 1;
		} else if($discipline == "2"){
			$id_discipline = 2;
		}

		$data['report_number']	= $report_number;
		$data['discipline']		= $discipline;
		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'MRIR '.$report_number;
		$data['subview']    	= 'mrir/material/detail_mrr';
		$data['sidebar']    	= $this->sidebar;



		$data['query']			  = $this->Mrir_mod->detail_mrr($report_number, $id_discipline);
		$data['q_mrir']			  = $this->Mrir_mod->get_mrir_by_report_no($report_number);
		$data['read_permission']  = $this->permission_cookie;

		$this->load->view('index', $data);
	}

	public function mis(){
		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Material Issue Slip';
		$data['subview']    	= 'mrir/material/mis';
		$data['sidebar']    	= $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function draw_list(){
		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Drawing List';
		$data['subview']    	= 'engineering/drawing/draw_list';
		$data['sidebar']    	= $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	//MRIR REQUEST, APPROVED, REJECTED ====================================================
	public function mrir_data($param){

		if($param == "request"){
			$meta_title = 'Request';
			$status = 1;
			$query = $this->Mrir_mod->get_mrir_by_status(1);
		} else if($param == "approved"){
			$meta_title = 'Approved';
			$status = 3;
			$query = $this->Mrir_mod->get_mrir_by_status(3);
		} else if($param == "rejected"){
			$meta_title = 'Rejected';
			$status = 2;
			$query = $this->Mrir_mod->get_mrir_by_status(2);
		}

		$data['param'] 			 = $status;
		$data['read_cookies'] 	 = $this->user_cookie;
		$data['meta_title'] 	 = 'MRIR '.$meta_title.' List';
		$data['subview']    	 = 'mrir/mrir/mrir_data';
		$data['sidebar']    	 = $this->sidebar;
		$data['read_permission'] = $this->permission_cookie;

		$data['query']  = $query;

		$this->load->view('index', $data);
	}

	public function update_mrir($id = NULL){

		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ redirect('mrir/mrir_data/request'); }

		$where['id'] = $id;
		$data['mrir_list'] = $this->Mrir_mod->get_mrir_list($where);
		unset($where);

		$where['mrir_id'] = $id;
		$mrir_detail_list = $this->Mrir_mod->get_mrir_detail_list($where);
		unset($where);

		foreach ($mrir_detail_list as $key => $value) {
			
			// RECEIVING DETAIL --------------
			$where['id'] 	= $value['receiving_detail_id'];
			$datadb = $this->receiving_mod->receiving_detail_data($where);
			$receiving_id 	= $datadb[0]['receiving_id']; 
			$po_number 		= $datadb[0]['po_number']; 
			$catalog_id 	= $datadb[0]['catalog_id']; 
			$mrir_detail_list[$key]['brand'] = $datadb[0]['brand'];
			$mrir_detail_list[$key]['color_code'] = $datadb[0]['color_code'];
			$mrir_detail_list[$key]['date_manufacturing'] = $datadb[0]['date_manufacturing'];
			unset($where);
			// -------------------------------

			//RECEIVING MASTER -------------------
			$where['id'] = $receiving_id;
			$datadb = $this->receiving_mod->receiving_data($where);
			$datadb = $datadb[0];
			$mrir_detail_list[$key]['do_pl'] = $datadb['do_pl'];
			unset($where);
			// -----------------------------------

			//MATERIAL CATALOG -------------------
			$where['mc.id'] = $catalog_id;
			$datadb = $this->Material_catalog_mod->get_material_catalog_join($where);
			$datadb = $datadb[0];
			$mrir_detail_list[$key]['code_material'] 		= $datadb['code_material'];
			$mrir_detail_list[$key]['catalog_category'] 	= $datadb['catalog_category'];
			$mrir_detail_list[$key]['material'] 			= $datadb['material'];
			$mrir_detail_list[$key]['id_material'] 		= $datadb['catalog_id'];
			unset($where);
			// -----------------------------------

			//PO NUMBER --------
            $where['po_number'] = $po_number;
            $where['cat_id'] 	= $catalog_id;
            $datadb = $this->po_mod->po_list($where);
            $datadb = $datadb[0];

            $dept_id 		= $datadb['dept_id'];
            $qty_request 	= $datadb['po_qty'];
            $mr_detail_id 	= $datadb['mr_detail_id'];
            unset($where); 
            //-----------------

            //DEPARTMENT ------
            $where['id_department'] = $dept_id;
            $datadb = $this->department_mod->all_list(null, $where);
            $datadb = $datadb[0];
            $mrir_detail_list[$key]['name_of_department'] 	= $datadb['name_of_department'];
            $mrir_detail_list[$key]['dept_id']				= $datadb['id_department'];
            unset($where);
            // ----------------

            // MR DETAIL ------
            $where['id'] = $mr_detail_id;
            $datadb = $this->mr_mod->mr_list_detail($where);
            $datadb = $datadb[0];
            $uom = $datadb['uom'];
            unset($where);
            //-----------------

            //UOM -------------
            $where['id_uom'] = $uom;
            $datadb = $this->general_mod->uom_list($where);
            $datadb = $datadb[0];
            $mrir_detail_list[$key]['uom'] = $datadb['uom'];
            unset($where);
            //-----------------
		}

		$data['mrir_detail_list'] = $mrir_detail_list;

		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'Inspection MRIR';
		$data['subview']    		= 'mrir/mrir/update_mrir';
		$data['sidebar']    		= $this->sidebar;
		$data['read_permission']  	= $this->permission_cookie;

		$this->load->view('index', $data);
	}

	public function detail_mrir($id = NULL){

		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ 
			redirect('mrir/mrir_data/request');
		}

		$where['id'] = $id;
		$data['mrir_list'] = $this->Mrir_mod->get_mrir_list($where);
		unset($where);

		$where['mrir_id'] = $id;
		$mrir_detail_list = $this->Mrir_mod->get_mrir_detail_list($where);
		unset($where);

		foreach ($mrir_detail_list as $key => $value) {

			// RECEIVING DETAIL --------------
			$where['id'] 	= $value['receiving_detail_id'];
			$datadb = $this->receiving_mod->receiving_detail_data($where);
			$receiving_id 	= $datadb[0]['receiving_id']; 
			$po_number 		= $datadb[0]['po_number']; 
			$catalog_id 	= $datadb[0]['catalog_id']; 
			$mrir_detail_list[$key]['brand'] = $datadb[0]['brand'];
			$mrir_detail_list[$key]['color_code'] = $datadb[0]['color_code'];
			$mrir_detail_list[$key]['date_manufacturing'] = $datadb[0]['date_manufacturing'];
			unset($where);
			// -------------------------------

			//RECEIVING MASTER -------------------
			$where['id'] = $receiving_id;
			$datadb = $this->receiving_mod->receiving_data($where);
			$datadb = $datadb[0];
			$mrir_detail_list[$key]['do_pl'] = $datadb['do_pl'];
			unset($where);
			// -----------------------------------

			//MATERIAL CATALOG -------------------
			$where['mc.id'] = $catalog_id;
			$datadb = $this->Material_catalog_mod->get_material_catalog_join($where);
			$datadb = $datadb[0];
			$mrir_detail_list[$key]['code_material'] 		= $datadb['code_material'];
			$mrir_detail_list[$key]['catalog_category'] 	= $datadb['catalog_category'];
			$mrir_detail_list[$key]['material'] 			= $datadb['material'];
			$mrir_detail_list[$key]['id_material'] 		= $datadb['catalog_id'];
			unset($where);
			// -----------------------------------

			//PO NUMBER --------
            $where['po_number'] = $po_number;
            $where['cat_id'] 	= $catalog_id;
            $datadb = $this->po_mod->po_list($where);
            $datadb = $datadb[0];

            $dept_id 		= $datadb['dept_id'];
            $qty_request 	= $datadb['po_qty'];
            $mr_detail_id 	= $datadb['mr_detail_id'];
            unset($where); 
            //-----------------

            //DEPARTMENT ------
            $where['id_department'] = $dept_id;
            $datadb = $this->department_mod->all_list(null, $where);
            $datadb = $datadb[0];
            $mrir_detail_list[$key]['name_of_department'] 	= $datadb['name_of_department'];
            $mrir_detail_list[$key]['dept_id']				= $datadb['id_department'];
            unset($where);
            // ----------------

            // MR DETAIL ------
            $where['id'] = $mr_detail_id;
            $datadb = $this->mr_mod->mr_list_detail($where);
            $datadb = $datadb[0];
            $uom = $datadb['uom'];
            unset($where);
            //-----------------

            //UOM -------------
            $where['id_uom'] = $uom;
            $datadb = $this->general_mod->uom_list($where);
            $datadb = $datadb[0];
            $mrir_detail_list[$key]['uom'] = $datadb['uom'];
            unset($where);
            //-----------------
		}

		$data['mrir_detail_list'] = $mrir_detail_list;

		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'MRIR Detail';
		$data['subview']    		= 'mrir/mrir/detail_approved';
		$data['sidebar']    		= $this->sidebar;
		$data['read_permission']  	= $this->permission_cookie;

		$this->load->view('index', $data);
	}

	public function update_mrir_proccess(){

		$id = $this->input->post('id');	

		$data = array(
				'status' => $this->input->post('approval'),
				'comments' => $this->input->post('comments')
			);

		$this->Mrir_mod->edit_mrir($id, $data);

		$this->session->set_flashdata('success', 'Success!');

		redirect('mrir/mrir_data/request');
	}

	// public function detail_material($unique_id){
	// 	$unique = $this->encryption->decrypt(strtr($unique_id, '.-~', '+=/'));
	// 	$data['read_cookies'] 	= $this->user_cookie;
	// 	$data['meta_title'] 	= 'Material '.$unique;
	// 	$data['subview']    	= 'mrir/material/detail_material';
	// 	$data['sidebar']    	= $this->sidebar;
	// 	$data['read_permission']  = $this->permission_cookie;

	// 	$data['q_spec_category']	= $this->Mrir_mod->data_spec_category();
	// 	$data['q_material']    	= $this->Mrir_mod->get_material_by_unique($unique);
	// 	$data['q_discipline']   = $this->Mrir_mod->data_discipline();
	// 	$data['q_document_material'] = $this->Mrir_mod->get_document_material_by_unique($unique);

	// 	$this->load->view('index', $data);
	// }

	public function check_unique_ident($unique_id){
		$data = $this->Mrir_mod->get_material_by_unique($unique_id);
		echo json_encode(sizeof($data));
	}

	public function check_report_no(){
		$report_number = $this->input->post('r_no');
		$data = $this->Mrir_mod->get_mrir_by_report_no($report_number);
		echo json_encode(sizeof($data));
	}

	public function update_material_proccess(){
		$id = $this->input->post('id');	

		$unique = $this->input->post('unique_ident_no');

		$data = array(
				'report_no'			=> $this->input->post('report_no'),
				'category'			=> $this->input->post('category'),
				'discipline' 		=> $this->input->post('discipline'),
				'description' 		=> $this->input->post('description'),
				'length' 			=> $this->input->post('length'),
				'width_or_od' 		=> $this->input->post('width_or_od'),
				'sch_or_thk' 		=> $this->input->post('sch_or_thk'),
				'spec' 				=> $this->input->post('spec'),
				'type' 				=> $this->input->post('type'),
				'plate_or_tag_no'	=> $this->input->post('plate_or_tag_no'),
				'heat_or_series_no' => $this->input->post('heat_or_series_no'),
				'ceq' 				=> $this->input->post('ceq'),
				'mill_cert_no' 		=> $this->input->post('mill_cert_no'),
				'date_manufacturing'=> date('Y-d-m', strtotime($this->input->post('date_manufacturing'))),
				'do_or_pl_no' 		=> $this->input->post('do_or_pl_no'),
				'unique_ident_no' 	=> $this->input->post('unique_ident_no'),
				'qty'	 			=> $this->input->post('qty'),
				'uom'	 			=> $this->input->post('uom'),
				'color_code' 		=> $this->input->post('color_code'),
				'remarks' 			=> '',
				'supplier_name' 	=> $this->input->post('supplier_name'),
				'location' 			=> '',
				'status' 			=> '',
				'is_imported' 		=> 0,
				'timestamp' 		=> date('Y-m-d H:i:s'),
			);

		$this->Mrir_mod->edit_material($id, $data);

		$this->session->set_flashdata('success', 'Data updated successfull!');

		$unique = strtr($this->encryption->encrypt($unique),'+=/', '.-~');

		redirect('mrir/detail_material/'.$unique);
	}

	public function disposition($report_no){

		$report_no = $this->encryption->decrypt(strtr($report_no, '.-~', '+=/'));

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Report Number '.$report_no;
		$data['subview']    	= 'mrir/material/disposition';
		$data['sidebar']    	= $this->sidebar;
		$data['report_no']    	= $report_no;
		
		$mrir 					= $this->Mrir_mod->get_mrir_by_report_no($report_no);

		$material 				= $this->Mrir_mod->get_material_by_report_no($report_no);

		$data2['status'] = 1;

		$this->Mrir_mod->update_status_material_to_disposition($report_no, $data2);

		$data['count_material']	= sizeof($material);
		$data['discipline'] = $mrir[0]->discipline;
		$data['id_mrir'] = $mrir[0]->id;
		$data['read_permission']  = $this->permission_cookie;

		$this->load->view('index', $data);
	}

	public function disposition_proccess(){
		$report_no = $this->input->post('report_no');

		$id = $this->input->post('id');
		$discipline = $this->input->post('discipline');

		$data = array(
				'status' 	=> 1,
				'comments'	=> $this->input->post('remarks')
			);

		$this->Mrir_mod->edit_mrir($id, $data);

		self::email_approve($report_no);

		$this->session->set_flashdata('success', 'Data disposition successful!');

		redirect('mrir/mrir_discipline/'.strtr($this->encryption->encrypt($discipline),'+=/', '.-~'));
	}

	public function email_approve($report_no){
		$links = 'http://localhost/mrir/mrir/mrir_data/approved';
		$requested_by 	= '';
		$department 	= '';

		$ci =& get_instance();
		$ci->load->library('email');
		$config['protocol'] 	= "smtp";
		$config['smtp_host'] 	= "10.5.252.31";
		$config['smtp_port'] 	= "25";
		$config['smtp_user'] 	= "";
		$config['smtp_pass'] 	= "";
		$config['charset'] 		= "utf-8";
		$config['mailtype'] 	= "html";
		$config['newline'] 		= "\r\n";
		$config['wordwrap'] 	= TRUE;
		$ci->email->initialize($config);
		$ci->email->set_crlf( "\r\n" );
		$ci->email->from('smtpservice.batam@sembmarine.com', 'MRIR Approval');
		$ci->email->subject("MRIR Approval - MR No : $report_no");
		$ci->email->message("<html>
							   <body>
							   		<p>Dear All, </p>
							   		<p>The following Material request is awaiting your approval.</p>
							   		<p>Please refer to data on below :</p>
							   		<p>
							   		<table>
							   			<tr>
							   				<td>MR No</td>
							   				<td>:</td>
							   				<td>$report_no</td>
							   			</tr>
							   			<tr>
							   				<td>Requested By</td>
							   				<td>:</td>
							   				<td>$requested_by / $department</td>
							   			</tr>
							   			<tr>
							   				<td>Approval Link</td>
							   				<td>:</td>
							   				<td><a href='$links' target='_blank'><b>Link</b></a></td>
							   			</tr>
							   		</table>
							   		</p>
							   		
							   		<p>Regards,<br/>PT. SMOE Portal<br>(Auto Reminder System)</p>
							   		<br/>
							   		<p><b>This is a system generated Email. <br/> Please do not reply to this email address.</b></p>
							   </body>
							 </html>
							");
		// $ci->email->send();
	}

	public function import_material($report_no,$discipline){
		
		//print_r($discipline);
		//return false;

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Import Material';
		$data['subview']    	= 'mrir/material/material_import';
		$data['sidebar']    	= $this->sidebar;

		$data['report_no']		= $this->encryption->decrypt(strtr($report_no, '.-~', '+=/'));
		$data['discipline']		= $discipline;

		//print_r($this->encryption->decrypt(strtr($report_no, '.-~', '+=/')));
		//return false;

		$data['read_permission']  = $this->permission_cookie;

		$this->load->view('index', $data);
	}

	public function import_material_proccess($report_no){

		$filename= $_FILES["file"]["name"];
		$file_ext = pathinfo($filename,PATHINFO_EXTENSION);

		$report_no_red = strtr($this->encryption->encrypt($report_no),'+=/', '.-~');
		$discipline = $this->input->post('discipline');

		if($file_ext != 'xlsx'){
			
			$this->session->set_flashdata('error', 'Extension File Not Supported..');
			redirect("mrir/import_material/".$report_no_red."/".$discipline);
			return false;

		} else { 
		
		

		$id_user = $this->user_cookie;

		$config['upload_path']          = 'file/material/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 			= TRUE;

		$this->load->library('upload', $config);

		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("mrir/import_material/".$report_no_red."/".$discipline);
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/material/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data = array();
		$data_check = array();

		$project_saved = $id_user[10];
		
		$numrow = 1;

		foreach($sheet as $row){
			
			if($numrow == 1 AND $row['A'] != "CATEGORY"){

			$this->session->set_flashdata('error',"Please Upload using MRIR Material template!");
			redirect("mrir/import_material/".$report_no_red."/".$discipline);
			return false;
			
		   	} else {
			
			if($numrow > 1){
				
				$spec_category = $this->Mrir_mod->get_id_for_spec_cat($row['G']);

				$data_check[] = $row['P'];
				
				if($row['A'] != "" AND $row['B'] != "" AND $row['C'] != ""){

				array_push($data, array(
					'project_id'		=>	$project_saved, // Insert data nis dari kolom A di excel
					'category'			=>	$row['A'], // Insert data nis dari kolom A di excel
					'description'		=>	$row['B'], // Insert data nis dari kolom A di excel
					'length'			=>	$row['C'],
					'width_or_od'		=>	$row['D'], // Insert data nama dari kolom B di excel
					'sch_or_thk'		=>	$row['E'], // Insert data jenis kelamin dari kolom C di excel
					'spec'				=>	$row['F'], // Insert data alamat dari kolom D di excel
					'spec_category'		=>	$spec_category[0]["id"], // Insert data alamat dari kolom D di excel
					'type'				=>	$row['H'], // Insert data nis dari kolom A di excel
					'plate_or_tag_no'	=>	$row['I'], // Insert data alamat dari kolom D di excel
					'heat_or_series_no'	=>	$row['J'], // Insert data alamat dari kolom D di excel
					'ceq'				=>	$row['K'], // Insert data alamat dari kolom D di excel
					'mill_cert_no'		=>	$row['L'], // Insert data alamat dari kolom D di excel
					'date_manufacturing'=>	$row['M'], // Insert data alamat dari kolom D di excel
					'supplier_name'		=>	$row['N'], // Insert data alamat dari kolom D di excel
					'do_or_pl_no'		=>	$row['O'], // Insert data alamat dari kolom D di excel
					'unique_ident_no'	=>	$row['P'], // Insert data alamat dari kolom D di excel
					'qty'				=>	$row['Q'], // Insert data alamat dari kolom D di excel
					'uom'				=>	$row['R'], // Insert data alamat dari kolom D di excel
					'color_code'		=>	$row['S'], // Insert data alamat dari kolom D di excel
					'is_imported'		=> 	1,
					'report_no'			=>	$report_no,
					'discipline'		=>	$discipline,
					'po_number'		    =>	$row['T'], // Insert data alamat dari kolom D di excel
					'vendor'		    =>	$row['U'] // Insert data alamat dari kolom D di excel
				));
			}
		}

	
	}
			
			$numrow++; // Tambah 1 setiap kali looping


		}
		/*
		echo str_replace(array('&lt;?php&nbsp;','?&gt;'), '', highlight_string( '<?php ' .     var_export($data_check, true) . ' ?>', true ) );
		return false;
		*/

		//print_r($data);
			//return false;


		$material_duplicate		= $this->Mrir_mod->check_material(null,$data_check);

		if(count($material_duplicate) != 0){
			foreach ($material_duplicate as $key => $value) {
				$d[] = $value['unique_ident_no'];
			}

			$this->session->set_flashdata('error', 'Duplicate Unique Identity Number : '.join(", ",$d));
			redirect("mrir/import_material/".$report_no_red."/".$discipline);
		}
		else{
			$this->Mrir_mod->import_material_proccess($data);
			$this->session->set_flashdata('success', 'Your data has been imported!');
			redirect("mrir/detail_mrr/?report_number=".$report_no."&discipline=".$discipline);
		}
	}
	}

	function approval_status(){
		$approve = $this->input->post('approve');

		$report_no = $this->input->post('report_no');

		if(!$approve){
			$approve = array();
		}
		foreach (array_keys($approve, 'on', true) as $key) {
		  unset($approve[$key]);
		}

		// Ubah semua jadi rejected
		$dt_reject = array(
		  'status' => 1
		);

		$where['report_no'] = $report_no;

		$this->Mrir_mod->edit_status_approval($dt_reject, $where);
		unset($where);

		// Ubah yg dicentang jadi approved
		if(count($approve) > 0){
			$dt_approve = array(
			  'status' => 3
			);
			$where_in['id'] = $approve;
			$this->Mrir_mod->edit_status_approval($dt_approve, null,$where_in);
			unset($where_in);
		}		
	}

	public function update_status_mrir_proccess(){
		
		$approve 		  = $this->input->post('approve');
		$report_no 		  = $this->input->post('report_no');
		$mrir_id 		  = $this->input->post('mrir_id');
		
		$unique_ident_no  = $this->input->post('unique_ident_no');
		$receive_det_id   = $this->input->post('receive_det_id');
		$qty 			  = $this->input->post('qty');
		$qty_demage 	  = $this->input->post('qty_demage');
		$qty_approve 	  = $this->input->post('qty_approve');
		$length 		  = $this->input->post('length');
		$catalog_id 	  = $this->input->post('catalog_id');
		$qty_shortage 	  = $this->input->post('qty_shortage');
		$qty_over 		  = $this->input->post('qty_over');
		$unique_mrir_detail = $this->input->post('unique_mrir_detail');

		$id_approve 	= [];
		$id_reject 		= [];
	
		//print_r($approve);
		//return false;
		$osd = array();
		if($approve){
			//for($i=0;$i<sizeof($approve);$i++){
			foreach ($approve as $key => $value) {

				if($value != "ignored"){

					//save osd
					if($qty_over[$key] + $qty_shortage[$key] + $qty_demage[$key] != 0){
						$osd[] = $key;
					}					
					//save osd END

					$dt_approve = explode(" ",$value);
					if($dt_approve[0] == 'A'){
						$id_approve[] 		= $dt_approve[1];
			
					} else {
						$id_reject[] = $dt_approve[1];
					}
					//======================================================================
				}

			}

		//submit osd
		if(sizeof($osd) != 0){

				$osd_no         = $this->osd_mod->generate_batch_no();
				$po_number 		= $this->input->post('po_number');
				$created_by 	= $this->user_cookie[0];
				$created_date	= date('Y-m-d H:i:s');

				$osd_form = array(
          			'osd_no'      => $osd_no,
          			'po_number'   => $po_number,
         			'create_by'   => $created_by,
          			'create_date' => $created_date,
          			'status'      => 1
        		);

        		$this->osd_mod->insert_osd_master($osd_form);

				foreach ($osd as $key => $value) {
					$osd_detail = array(
			  			'osd_no'      	=> $osd_no,
			  			'catalog_id'  	=> $catalog_id[$value],
			  			'unique_no'  	=> $unique_mrir_detail[$value],
			  			'over'  		=> $qty_over[$value],
			  			'shortage'  	=> $qty_shortage[$value],
			  			'damage'  		=> $qty_demage[$value],              
            			'create_by'   	=> $created_by,
            			'create_date' 	=> $created_date,
            			'status_osd'  	=> 1
					);
          		
          			$this->osd_mod->insert_osd_detail($osd_detail);
					
				}
			}
			//submit osd END

			//print_r($id_approve);
			//return false;


			if(sizeof($id_approve) != 0){
				$dt_approve = array(
				  'status' => 3,
				  'checked_by' => $this->user_cookie[0],
				  'checked_date' => date("Y-m-d H:i:s")
				);

				$where['id'] = $id_approve;
				$this->Mrir_mod->edit_status_approval($dt_approve, null,$where);
				unset($where);
				
			}

			if(sizeof($id_reject) != 0){
				$dt_approve = array(
				  'status' => 2,
				  'checked_by' => $this->user_cookie[0],
				  'checked_date' => date("Y-m-d H:i:s")
				);
				$where['id'] = $id_reject;
				$this->Mrir_mod->edit_status_approval($dt_approve,null, $where);
				unset($where);
			}
		}

		

		$id_material = [];
		$i = 0;

		//GET ALL ID MATERIAL
		foreach($this->input->post('id_material') as $id){
			$id_material[] = $id;
		}

		//INPUT REMARK TO MATERIAL
		// foreach($this->input->post('remark_material') as $key => $remark_material){
		// 	$data = array(
		// 		'remarks' 		=> $remark_material,
		// 		'qty_demage' 	=> $qty_demage[$key],
		// 		'qty_approve' 	=> $qty_approve[$key],
		// 	);
		// 	$this->Mrir_mod->approve_unapprove_material($id_material[$i], $data);

		// 	$i++;
		// }

		$all_data 		   = $this->Mrir_mod->get_all_count($mrir_id); //CARI APAKAH ADA DATA YANG UNAPPROVE
		$material_approved = $this->Mrir_mod->get_approve_material_by_report_no($mrir_id); //CARI APAKAH ADA DATA YANG UNAPPROVE
		$material_rejected = $this->Mrir_mod->get_unapprove_material_by_report_nox($mrir_id); //CARI APAKAH ADA DATA YANG UNAPPROVE

		$total_submit_data = sizeof($material_approved) + sizeof($material_rejected);

		if($approve){
			$this->session->set_flashdata('success', 'Your document has been updated!');
		}
		else{
			$this->session->set_flashdata('error', 'No Data Selected!');
		}
		if(sizeof($all_data) == sizeof($material_approved)){

			$module 	= $this->input->post('module');
			$user_id 	= $this->input->post('user_id');
			$mrir_id  = $this->input->post('report_no');

			//print_r("Approved");
			//return false;

			$data2['status'] = 3;
			$data2['approved_by'] = $this->user_cookie[0];
			$data2['approved_date'] = date("Y-m-d");

			$this->Mrir_mod->update_status_mrir_by_report_no($mrir_id, $data2);
			$status = "approved";
			// redirect('mrir/mrir_data/'.$status);

			redirect('mrir/mrir_data/'.$status);
						
		} else if(sizeof($all_data) == sizeof($material_rejected)) {

			//print_r("Reject ALL");
			//return false;

			$data2['status'] = 2;
			$this->Mrir_mod->update_status_mrir_by_report_no($mrir_id, $data2);
			$status = "rejected";
			redirect('mrir/mrir_data/'.$status);

		} else if(sizeof($material_rejected) > 0 AND $total_submit_data == sizeof($all_data)) {

			//print_r("Reject ALL 2");
			//return false;

			$data2['status'] = 2;
			$this->Mrir_mod->update_status_mrir_by_report_no($mrir_id, $data2);
			$status = "rejected";
			redirect('mrir/mrir_data/'.$status);	


		} else {

			//print_r("Requests");
			//return false;

			$status = "request";
			redirect('mrir/update_mrir/?report_no='.$mrir_id);

		}

		
	}

	public function manual_sign($module = null,$user_id = null,$report_no = null){


		// if(!empty($this->input->post('save'))){

		// 	$report_no = $this->input->post('report_no');

		// 	$data2['status'] = 3;
		// 	$data2['approved_by'] = $this->input->post('user_id');
		// 	$data2['sign_approved'] = $this->input->post('hdnSignature');
		// 	$data2['approved_date'] = date("Y-m-d");
		// 	$this->Mrir_mod->update_status_mrir_by_report_no($report_no, $data2);
		// 	$status = "approved";

			
			
		// 	redirect('mrir/mrir_data/'.$status);

		// }

		$data['read_cookies'] 	 = $this->user_cookie;
		$data['read_permission'] = $this->permission_cookie;
		
		$data['meta_title'] 	= 'Material Verify';
		$data['subview']    	= 'mrir/mrir/manual_approved';
		$data['sidebar']    	= $this->sidebar;
		$data['module']    		= $module;
		$data['user_id']    	= $user_id;
		$data['report_no']    	= $report_no;

		$this->load->view('index', $data);
	}

	public function re_submit($report_no){

		$data_mrir = $this->Mrir_mod->get_mrir_by_report_no($report_no); //AMBIL DATA MRIR BERDASARKAN REPORT NUMBER

		$total_submit = $data_mrir[0]->total_submit + 1; //TOTAL REVISI + 1;

		$data = array(
			'total_submit' => $total_submit,
			'status'	=> 1
		);

		$this->Mrir_mod->update_status_mrir_by_report_no($report_no, $data); //UPDATE TOTAL SUBMIT + STATUS MRIR
		$this->Mrir_mod->update_material_resubmit($report_no, $data); //UPDATE TOTAL SUBMIT + STATUS MATERIAL

		self::email_approve($report_no);

		redirect('mrir/mrir_data/request');
	}

	public function add_file_material_proccess(){

		$id_user			= $this->user_cookie;

		$document_unique 	= $this->input->post('document_unique');
		$document_name 		= $this->input->post('document_name');
		
		$name_file 			= $id_user[0].'-'.$document_unique.'-'.date('YmdHis');

		$config['upload_path']          = 'upload/material_document';
		$config['file_name']            = $name_file;
		$config['allowed_types']        = 'pdf';
		$config['max_size']        		= '2000';

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());

			redirect('mrir/detail_material/'.strtr($this->encryption->encrypt($document_unique),'+=/', '.-~'));
			return false;
		}

		$form_data = array(
			'material_id' 	=> $document_unique,
			'document_name' => $document_name,
			'document_file' => $this->upload->data('file_name')
		);

		$this->Mrir_mod->upload_material_document_proccess($form_data);
		$this->session->set_flashdata('success', 'Your document has been uploaded!');

		$doc_un_red = strtr($this->encryption->encrypt($document_unique),'+=/', '.-~');

		redirect('mrir/detail_material/'.$doc_un_red);
	}

	public function export_mrir($id){

		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ 
			redirect('mrir/mrir_data/approve');
		}

		$where['id'] = $id;
		$data['mrir_list'] = $this->Mrir_mod->get_mrir_list($where);
		unset($where);

		$where['mrir_id'] = $id;
		$data['mrir_detail_list'] = $this->Mrir_mod->get_mrir_detail_list($where);
		unset($where);


		//$data['approval_log'] 	= $this->general_mod->getApprovalLog($id,"MRIR");
		// $approval_log 			= $this->general_mod->getApprovalLog($id,"MRIR");

		// $total_array 	= count($approval_log);
		
		// if($total_array > 0){
		// 	$app_nos = $approval_log[0]["approved_category"]."/".$approval_log[0]["id"]."/".$approval_log[0]["approved_request_no"];
		// } else {
			$app_nos = "Document Not Approved By QC Inspector";	
		// }

	    $this->load->library('Pdfgenerator');

	    $html = $this->load->view('mrir/export/mrir_export_pdf', $data, true);
	    
	    $this->pdfgenerator->generate($html,$id,$app_nos);
	}

	public function export_mrir_inspection($report_no){

		$report_no = $this->encryption->decrypt(strtr($report_no, '.-~', '+=/'));

		$data['q_mrir'] 		= $this->Mrir_mod->get_mrir_by_report_no($report_no);
		$data['q_material'] 	= $this->Mrir_mod->get_material_by_report_no($report_no);
		$approval_log 			= $this->general_mod->getApprovalLog($report_no,"MRIR");

		$total_array 	= count($approval_log);
		
		if($total_array > 0){
			$app_nos = $approval_log[0]["approved_category"]."/".$approval_log[0]["id"]."/".$approval_log[0]["approved_request_no"];
		} else {
			$app_nos = "Document Not Approved By QC Inspector";	
		}

		
		
		$datadb = $this->general_mod->vendor_list();
		foreach ($datadb as $value) {
			$data['vendor_list'][$value['id_vendor']] = $value['vendor_name'];
		}

		foreach ($data['q_material'] as $key => $value) {
			$catalog_id[] = $value->catalog_id;
		}
		if(count($catalog_id) > 0){
			$where['id IN ('.join(',', $catalog_id).')'] = NULL;
			$datadb	= $this->Material_catalog_mod->getAll($where);
			foreach ($datadb as $key => $value) {
				$catalog_list[$value['id']] = $value;
			}
			$data['catalog_list']	= $catalog_list;
		}

	    $this->load->library('Pdfgenerator');

	    $html = $this->load->view('mrir/export/mrir_export_pdf_inspection', $data, true);
	    
	    $this->pdfgenerator->generate($html,$report_no,$app_nos);
	}

	//=====================================================================================
	public function mrir_new_choose_po($receiving_id = NULL){
		$data['po_list']			= $this->general_mod->manual_query("SELECT DISTINCT po_number FROM eproc_receiving_detail WHERE mrir_created = 0");
		$data['meta_title'] 	  	= 'Choose PO for New MRIR';
		$data['subview']    	  	= 'mrir/mrir_new_choose_po';
		$data['read_permission'] 	= $this->permission_cookie;
		$data['sidebar']    	  	= $this->sidebar;
		$this->load->view('index', $data);
	}

	public function mrir_new_choose_do($receiving_id = NULL){
		$data['do_list']				= $this->Mrir_mod->get_data_receicing_cs();

		$data['category'] 	  	= 'CS';
		$data['meta_title'] 	  = '(CS) Choose Shipment for New MRIR';
		$data['subview']    	  = 'mrir/mrir_new_choose_po';
		$data['sidebar']    	  = $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function mrir_new(){
		$po_number 		= $this->input->post('po_number');
		$discipline 	= $this->input->post('discipline');
		$category 		= $this->input->post('category');

		$where['po_number']	= $po_number;
		$data['po_list']	= $this->po_mod->po_list($where);
		unset($where);

		$where['po_number']		= $po_number;
		$where['mrir_created']	= 0;
		$receiving_detail_list	= $this->Mrir_mod->receiving_detail_data($where);
		unset($where);

		foreach ($receiving_detail_list as $key => $value) {

			//RECEIVING MASTER -------------------
			$where['id'] = $value['receiving_id'];
			$datadb = $this->receiving_mod->receiving_data($where);
			$datadb = $datadb[0];
			$receiving_detail_list[$key]['do_pl'] = $datadb['do_pl'];
			unset($where);
			// -----------------------------------

			//MATERIAL CATALOG -------------------
			$where['mc.id'] = $value['catalog_id'];
			$datadb = $this->Material_catalog_mod->get_material_catalog_join($where);
			$datadb = $datadb[0];
			$receiving_detail_list[$key]['code_material'] 		= $datadb['code_material'];
			$receiving_detail_list[$key]['catalog_category'] 	= $datadb['catalog_category'];
			$receiving_detail_list[$key]['material'] 			= $datadb['material'];
			$receiving_detail_list[$key]['id_material'] 		= $datadb['catalog_id'];
			unset($where);
			// -----------------------------------

			//PO NUMBER --------
            $where['po_number'] = $value['po_number'];
            $where['cat_id'] 	= $value['catalog_id'];
            $datadb = $this->po_mod->po_list($where);
            $datadb = $datadb[0];

            $dept_id 		= $datadb['dept_id'];
            $qty_request 	= $datadb['po_qty'];
            $mr_detail_id 	= $datadb['mr_detail_id'];
            unset($where); 
            //-----------------

            //DEPARTMENT ------
            $where['id_department'] = $dept_id;
            $datadb = $this->department_mod->all_list(null, $where);
            $datadb = $datadb[0];
            $receiving_detail_list[$key]['name_of_department'] 	= $datadb['name_of_department'];
            $receiving_detail_list[$key]['dept_id']				= $datadb['id_department'];
            unset($where);
            // ----------------

            // MR DETAIL ------
            $where['id'] = $mr_detail_id;
            $datadb = $this->mr_mod->mr_list_detail($where);
            $datadb = $datadb[0];
            $uom = $datadb['uom'];
            unset($where);
            //-----------------

            //UOM -------------
            $where['id_uom'] = $uom;
            $datadb = $this->general_mod->uom_list($where);
            $datadb = $datadb[0];
            $receiving_detail_list[$key]['uom'] = $datadb['uom'];
            unset($where);
            //-----------------
		}

		$data['receiving_detail_list'] = $receiving_detail_list;

		$data['read_permission']  = $this->permission_cookie;
		$data['meta_title'] 	  = 'Create New MRIR';
		$data['subview']    	  = 'mrir/mrir_new';
		$data['sidebar']    	  = $this->sidebar;
		$this->load->view('index', $data);
	}

	public function mrir_new_process(){

		$po_number 			 = $this->input->post('po_number');
		$dept_id 			 = $this->input->post('dept_id');

		$vendor 			 = $this->input->post('vendor');
		$report_no 			 = $this->Mrir_mod->generate_batch_no();

		$receiving_detail_id = $this->input->post('receiving_detail_id');
		$do_no 				 = $this->input->post('do_no');
		$description 		 = $this->input->post('description');
		$date_manufacturing  = $this->input->post('date_manufacturing');
		$date_receiving 	 = $this->input->post('date_receiving');
		$catalog_id			 = $this->input->post('id_material');
		
		$qty 				 = $this->input->post('qty');
		$qty_shortage 		 = $this->input->post('qty_shortage');
		$qty_over		 	 = $this->input->post('qty_over');

		$remarks 			 = $this->input->post('remarks');

		$form_data = array(
			'report_no' 	=> $report_no,
			'po_number' 	=> $po_number,
			'created_date' 	=> date('Y-m-d'),
			'created_by' 	=> $this->user_cookie[0],
			'status' 		=> 1,
			'approved_by'	=> 0,
			'approved_date'	=> '0000-00-00',
			'total_submit'	=> 0,
			'sign_approved'	=> ''
		);

		$mrir_id = $this->Mrir_mod->mrir_add($form_data);
		unset($form_data);

		$form_data = array();
		foreach ($receiving_detail_id as $key => $value) {

			array_push($form_data, array(
				'mrir_id' 				=> $mrir_id,
				'receiving_detail_id'	=> $receiving_detail_id[$key],
				'date_receiving'		=> date('Y-m-d'),
				'catalog_id' 			=> $catalog_id[$key],
				'unique_no' 			=> uniqid(),
				'qty' 					=> $qty[$key],
				'qty_shortage' 			=> $qty_shortage[$key],
				'qty_over' 				=> $qty_over[$key],		
				'qty_demage' 			=> 0,		
				'remarks' 				=> $remarks[$key],
				'status' 				=> 1,
				'total_submit' 			=> 0,
				'checked_by' 			=> 0,
				'checked_date' 			=> '0000-00-00'
			));

		}

		$this->Mrir_mod->add_material($form_data);

		foreach ($receiving_detail_id as $key => $value) {
			$form_data = array(
				'mrir_created' => 1, 
			);

			$where['id'] = $receiving_detail_id[$key];
			$this->receiving_mod->receiving_detail_edit_process_db($form_data, $where);
			unset($where);
		}

		$this->session->set_flashdata('success', 'Your data has been Submitted!');
		redirect('mrir/mrir_data/request/');
	}

	public function detail_material($unique_id){
		$unique 					= $this->encryption->decrypt(strtr($unique_id, '.-~', '+=/'));
		$data['material'] = $this->Mrir_mod->get_material_by_unique($unique);
		$data['material'] = $data['material'][0];
		$data['document_list'] = $this->Mrir_mod->get_document_material_by_unique($unique);

		$data['read_permission']  = $this->permission_cookie;
		$data['meta_title'] 	  = $data['material']->description;
		$data['subview']    	  = 'mrir/mrir/detail_material';
		$data['sidebar']    	  = $this->sidebar;
		$this->load->view('index', $data);
	}

	public function test_var($value){
		echo '<pre>';
		print_r($value);
		echo '</pre>';
		exit;
	}

	function mrir_list_json($param){
        error_reporting(0);
 
        $list = $this->Mrir_mod->get_datatables_mrir_list_dt($param);
        
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {

            $links = base_url()."mrir/detail_mrir/".strtr($this->encryption->encrypt($list->id), '+=/', '.-~');
            $link_approve = base_url()."mrir/update_mrir/".strtr($this->encryption->encrypt($list->id), '+=/', '.-~');
            
            $no++;
            $row   = array();
            
            $row[] = $list->report_no;
            $row[] = $list->po_number;
            $row[] = $list->created_date;

            if($param == 1){
            	$status = 'Open';
            	$approve = "<a href='".$link_approve."' class='btn btn-success text-white' title='Detail'><i class='fas fa-check'></i> Approve </a>";
            } else if($param == 3){
            	$status = 'Approve';
            	$approve = '';
            }

            $row[] = $status;
            
            // if($this->permission_cookie[10] == 1){
                $row[] = "<a href='".$links."' class='btn btn-secondary text-white' title='Detail'><i class='fas fa-file-alt'></i> Detail </a>".' '.$approve;
            // } else {
            //     $row[] = "&nbsp;";
            // }
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "recordsTotal" => $this->Mrir_mod->count_all_mrir_list_dt($param),
            "recordsFiltered" => $this->Mrir_mod->count_filtered_mrir_list_dt($param),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    function export_mrir_excel($id){
    	$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ 
			redirect('mrir/mrir_data/approve');
		}

		$where['id'] 		= $id;
		$data['mrir_list'] 	= $this->Mrir_mod->get_mrir_list($where);
		unset($where);

		$where['mrir_id'] 			= $id;
		$data['mrir_detail_list'] 	= $this->Mrir_mod->get_mrir_detail_list($where);
		unset($where);
		
		$this->load->view('mrir/export/mrir_export_excel', $data);
    }

  public function coba(){
  	test_var($this->input->post());
  }

}