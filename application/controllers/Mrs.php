<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Mrs extends CI_Controller {

    public function __construct() {
            
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();
        $this->load->helper('cookies');
        helper_cookies();

        $this->load->model('home_mod');
        $this->load->model('general_mod');
        $this->load->model('engineering_mod');
        $this->load->model('mrir_mod');

        //FOR mrs ===================================
        
        $this->load->model('mrs_mod');
        $this->load->model('material_catalog_mod');
        
        //$this->load->model('mrs_category_mod');

        //===========================================

        $this->user_cookie       = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar           = "mrs/sidebar";
    }

    public function index(){
        redirect('mrs/mrs_list/pending');
    }

    public function mrs_list($param=null){
       
        if($param == "approved"){
           $req_pages = $param;
        } else if($param == "rejected"){
           $req_pages = $param;
        } else {
           $req_pages = $param;   
        }

        $title_data = ucfirst($req_pages);

        $data['read_cookies']       = $this->user_cookie;
        $data['meta_title']         = 'MRS List '.$title_data;
        $data['subview']            = 'mrs/mrs_list';
        $data['sidebar']            = $this->sidebar;
        $data['read_permission']    = $this->permission_cookie;
        $data['req_pages']          =  $req_pages;

        $this->load->view('index', $data);
    }

    public function mrs_add(){
      
        $data['read_cookies']     = $this->user_cookie;
        $data['meta_title']       = 'Create New MRS';
        $data['subview']          = 'mrs/mrs_new';
        $data['sidebar']          = $this->sidebar;
        $data['material_class']   = $this->mrs_mod->get_material_class();
        $data['get_project']      = $this->mrs_mod->get_project();
        $data['get_mrs_no']       = $this->mrs_mod->generate_batch_no();
        $data['get_mrs_location'] = $this->mrs_mod->get_location();
        $data['read_permission']  = $this->permission_cookie;
        $this->load->view('index', $data);

    }

    public function drawing_no_check(){

        $drawing_no             = $this->input->post('drawing_no');
        $where['drawing_no']    = $drawing_no;
        $data_mrs               = $this->mrs_mod->mrs_list($where);
        $data_eng               = $this->engineering_mod->draw_get_db($where);
        $data_mto               = $this->mrs_mod->searching_mto_drawing_data($drawing_no);
      
        unset($where);

            $hasil = 0;       
            if(sizeof($data_mto) != 0){
                $hasil = 1;
                $data = $data_mto[0];
              
                $data_project           = $this->mrs_mod->getProjectDetail($data_mto[0]["project_id"]);
            }
      
        echo json_encode(array(
            'project_id'    => @$data_mto[0]["project_id"],
            'project_name'  => @$data_project[0]["project_name"],
            'hasil'         => @$hasil,
        
        ));
    }

 
    
    public function mrs_detail($mrs_number = null){

        $mrs_number = $this->encryption->decrypt(strtr($mrs_number, '.-~', '+=/'));

        if(empty($mrs_number)){ 
            redirect('mrs_list');
        }

        $datadb = $this->mrs_mod->getProject();
        foreach ($datadb as $value) {
            $data['project_list'][$value['id']] = $value['project_name'];
        }
        
        $where['mrs_no']    = $mrs_number;
        $data['mrs_list']   = $this->mrs_mod->mrs_list($where);
        $data['mrs_list_detail']   = $this->mrs_mod->mrs_list_detail($where);
        unset($where);
       
        $data['read_cookies']       = $this->user_cookie;
        $data['read_permission']    = $this->permission_cookie;
        $data['material_class']     = $this->mrs_mod->get_material_class();
        $data['get_project']        = $this->mrs_mod->get_project();
        $data['meta_title']         = 'MRS'.'/'.$data['mrs_list'][0]["project_id"]."/".$mrs_number;
        $data['subview']            = 'mrs/mrs_detail';
        $data['sidebar']            = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    function get_material_category(){
        $material_cat = $this->input->post('material_cat');

        $where['id'] = $material_cat;
        $data = $this->mrs_mod->material_category_list($where);
        unset($where);

        echo json_encode($data);
    }

   

    //DRAWING AUTOCOMPLETE ============================================================================

    function drawing_autocomplete(){
        $drawing_no = $this->input->post('drawing_no');
        if(isset($_GET['term'])){
            $result = $this->mrs_mod->search_drawing_autocomplete($_GET['term'], $drawing_no);
            if ($result == TRUE){
                foreach ($result as $row)
                $arr_result[] = $row['document_no'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Drawing Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    //=================================================================================================

     //UNIQUE NO AUTOCOMPLETE ============================================================================
    function uniqno_autocomplete(){
        
      if (($this->input->post('term'))){
            $result = $this->mrs_mod->search_unique_autocomplete($this->input->post('term'),$this->input->post('project_id'));
            if ($result == TRUE){
                foreach ($result as $row)
                $arr_result[] = $row['unique_no'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Unique id Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    function uniqno_autocomplete_select2(){
        
      if (($this->input->post('search'))){
            $result = $this->mrs_mod->search_unique_autocomplete($this->input->post('search'),$this->input->post('project_id'));
            if ($result == TRUE){
            $list = array();
            $key=0;
                foreach ($result as $row){
                    $list[$key]['id']  = $row['unique_no'];
                    $list[$key]['text'] = $row['unique_no'];
                    $key++;
                }
            echo json_encode($list);
            } else {
            echo "Data Not Found";    
            }
        }
    }
    //=================================================================================================

    //MATERIAL CATALOG AUTOCOMPLETE====================================================================
    function material_catalog_autocomplete(){
        $material_catalog = $this->input->post('material_catalog');
        if (isset($_GET['term'])){
            $result = $this->mrs_mod->search_material_catalog_autocomplete($_GET['term'], $material_catalog);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['short_desc'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Material Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    function mrs_check_material_catalog(){
        $material = $this->input->post('material');
        $where['short_desc'] = $material;
        $result = $this->material_catalog_mod->getAll($where);
        
        $text = 0;
        $data = array(
            "id" => $text
        );

        if(count($result) > 0){
            $text = $result[0]['id'];
            $data = array(
                "id" => $text,
                "material" => $result[0]['material'],
                "size"  => $result[0]['size'],
                "type" => $result[0]['type'],
                "weight"    => $result[0]['weight'],
                "length"    => $result[0]['length']
            );
        }

        echo json_encode($data);
    }

    //Drawing List DataTables 


    


    public function unique_no_check($unique_no = null,$project_id = null){

        $unique_no    = $this->input->post('unique_no');
        $project_id   = $this->input->post('project_id');
        $drawing_no   = $this->input->post('drawing_no');
        
        $data_unique_no_balance   = $this->mrs_mod->get_bal_unique($unique_no);

        if(sizeof($data_unique_no_balance) > 0){
        
        if($data_unique_no_balance[0]["unique_no"] !== ""){

            if($data_unique_no_balance[0]["bal_qty"] > 0){

             $data_unique_no_rec_id      = $this->mrs_mod->get_data_unique($unique_no,$project_id);
             $data_catalog_data      = $this->mrs_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]); 

                if(isset($data_unique_no_rec_id[0]["unique_ident_no"])){
                   
                    $description    = $data_unique_no_rec_id[0]["description"];

                     $thk_mm         = $data_catalog_data[0]["thk_mm"];
                     $width_m        = $data_catalog_data[0]["width_m"];
                     $length_m       = $data_catalog_data[0]["length_m"];

                     $weight         = $data_catalog_data[0]["weight"];
                     $od             = $data_catalog_data[0]["od"];
                     $sch            = $data_catalog_data[0]["sch"];

                     $unit           = $data_unique_no_rec_id[0]["uom"];
                     $heat_no        = $data_unique_no_rec_id[0]["heat_or_series_no"];
                     $balance        = $data_unique_no_balance[0]["bal_qty"];
                     $balance_length = $data_unique_no_balance[0]["bal_length"];

                     echo $description."; ".$thk_mm."; ".$width_m."; ".$length_m."; ".$weight."; ".$od."; ".$sch."; ".$unit."; ".$heat_no."; ".$balance."; ".$balance_length;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
            } else {
                echo "Error : Balance Empty..";
            }

        } else { 

            echo "Error : Unique Number Not Found..";

        }

    } else {
       echo "Error : Unique Number Not Found.."; 
    }
        
    }


    public function unique_no_check_det($id_detail = null){

        $id_detail   = $this->input->post('id_detail');
        $project_id  = $this->input->post('project_id');
        $drawing_no  = $this->input->post('drawing_no');

        $where["id_mrs_det"] = $id_detail;
        $data_detail_list   = $this->mrs_mod->mrs_list_detail($where);
        unset($where);

        if(sizeof($data_detail_list) > 0){

        $unique_no                = $data_detail_list[0]['unique_no'];
      
        $data_unique_no_balance   = $this->mrs_mod->get_bal_unique($unique_no);
        
        $data_unique_no_rec_id    = $this->mrs_mod->get_data_unique($unique_no,$project_id);

        $data_catalog_data        = $this->mrs_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]);

        
        if($data_unique_no_balance[0]["unique_no"] !== ""){
           
                if(isset($data_unique_no_rec_id[0]["unique_ident_no"])){
                     
                     $description    = $data_unique_no_rec_id[0]["description"];

                     $thk_mm         = $data_catalog_data[0]["thk_mm"];
                     $width_m        = $data_catalog_data[0]["width_m"];
                     $length_m       = $data_catalog_data[0]["length_m"];

                     $weight         = $data_catalog_data[0]["weight"];
                     $od             = $data_catalog_data[0]["od"];
                     $sch            = $data_catalog_data[0]["sch"];

                     $unit           = $data_unique_no_rec_id[0]["uom"];
                     $heat_no        = $data_unique_no_rec_id[0]["heat_or_series_no"];
                     $balance        = $data_unique_no_balance[0]["bal_qty"];
                     $balance_length = $data_unique_no_balance[0]["bal_length"];

                     echo $description."; ".$thk_mm."; ".$width_m."; ".$length_m."; ".$weight."; ".$od."; ".$sch."; ".$unit."; ".$heat_no."; ".$balance."; ".$balance_length;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
          

        } else { 

            echo "Error : Unique Number Not Found..";

        }

    } else {
        echo "Error : Unique Number Not Found..";
    }
        
    }




    public function unique_no_check_preview($unique_no = null){

        $unique_no   = $this->input->post('unique_no');

        $data_unique_no_balance   = $this->mrs_mod->get_bal_unique($unique_no);

        if(sizeof($data_unique_no_balance) > 0){
        
        if($data_unique_no_balance[0]["unique_no"] !== ""){

            

             $data_unique_no_rec_id      = $this->mrs_mod->get_data_unique($unique_no);

                if(isset($data_unique_no_rec_id[0]["catalog_id"])){

                     $get_data_catalog           = $this->mrs_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]);

                     //print_r($data_unique_no_rec_id);

                     $description   = $get_data_catalog[0]["material"];
                     $od_width      = $get_data_catalog[0]["width_od"];
                     $sch_thk       = $get_data_catalog[0]["sch_thk"];
                     $length        = $get_data_catalog[0]["length"];
                     $unit          = $data_unique_no_rec_id[0]["uom"];
                     $heat_no       = $data_unique_no_rec_id[0]["heat_no"];
                     $balance       = $data_unique_no_balance[0]["bal_qty"];

                     echo $description."; ".$od_width."; ".$sch_thk."; ".$length."; ".$unit."; ".$heat_no."; ".$balance;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
            

        } else { 

            echo "Error : Unique Number Not Found..";

        }

    } else {
       echo "Error : Unique Number Not Found.."; 
    }
        
    }


     public function balance_checking(){

        $unique_no   = $this->input->post('unique_no');
        $issued_qty   = $this->input->post('issued_qty');

       if(is_numeric($issued_qty)){

                $data_unique_no_balance   = $this->mrs_mod->get_bal_unique($unique_no);

                if(sizeof($data_unique_no_balance) > 0){

                    $balance_after_check = $data_unique_no_balance[0]["bal_qty"] - $issued_qty;
                    if($balance_after_check < 0){
                        echo "Error : Sorry, We Just only have ".$data_unique_no_balance[0]["bal_qty"]; 
                    } else {
                        echo "Balance Available"; 
                    }

                } else {
                    echo "Error : Balance Shortage!"; 
                }
        } else {
            echo "Error : Input Correct Number!"; 
        }

        }
    


    function mrs_list_json($param=null)
    {
        error_reporting(0);

        if($param == "approved"){
           $req_pages = 3;
        } else if($param == "rejected"){
           $req_pages = 2;
        } else {
           $req_pages = 1;   
        }

        $datadb = $this->mrs_mod->get_project();       
        foreach ($datadb as $value) {
            $project_list[$value['id']] = $value['project_name'];
        }


        $datadb = $this->mrs_mod->get_user_data();       
        foreach ($datadb as $value) {
            $user_list[$value['id_user']] = $value['full_name'];
        }
 
        $list = $this->mrs_mod->get_datatables_mrs_list_dt($req_pages);
      
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {
          
            $links = base_url()."mrs_detail/".strtr($this->encryption->encrypt($list->mrs_no), '+=/', '.-~');
            
            $no++;
            $row   = array();

            if($list->status == 1){
                 $status = "<span style='font-weight:bold;color:red;'>"."OPEN"."</style>";
            } else if($list->status == 2){
                 $status = "<span style='font-weight:bold;color:red;'>"."REJECTED"."</style>";     
            } else if($list->status == 3){
                 $status = "<span style='font-weight:bold;color:green;'>"."APPROVED"."</style>";
            }
            
            $row[] = (isset($user_list[$list->created_by]) ? $user_list[$list->created_by] : '-');
            $row[] = $list->created_date;
            $row[] = $list->mrs_no;
            $row[] = (isset($project_list[$list->project_id]) ? $project_list[$list->project_id] : '-');
            $row[] = $list->sub_contractor_name;
            $row[] = $list->line_no;
            $row[] = $list->drawing_no;
            $row[] = $list->material_class;
            $row[] = $list->equipment_tag_no;
            $row[] = $status;
           
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-file-alt'></i></a>";
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mrs_mod->count_all_mrs_list_dt($req_pages),
            "recordsFiltered" => $this->mrs_mod->count_filtered_mrs_list_dt($req_pages),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }


    function mrs_new_form(){
                
        $project_id           = $this->input->post('project_id');
        $sub_contractor_name  = $this->input->post('sub_contractor_name');
        $line_no              = $this->input->post('line_no');
        $drawing_no           = $this->input->post('drawing_no');
        $equipment_tag_no     = $this->input->post('equipment_tag_no');
        $mrs_no               = $this->input->post('mrs_no');
        $material_class       = $this->input->post('material_class');
        $created_by           = $this->user_cookie[0];
        $date_create          = $this->input->post('created_date');
        

        $form_data = array(
          'project_id'           => $project_id,
          'sub_contractor_name'  => strtoupper($sub_contractor_name),        
          'line_no'              => strtoupper($line_no),
          'drawing_no'           => $drawing_no,
          'equipment_tag_no'     => strtoupper($equipment_tag_no),
          'mrs_no'               => $mrs_no,
          'material_class'       => $material_class,
          'created_by'           => $created_by,
          'created_date'         => $date_create,
          'status'               => 1
        );

        $this->mrs_mod->insert_mrs_master($form_data);

        $unique_no = $this->input->post('unique_no');
        if($unique_no){
            foreach ($unique_no as $key => $value) {
                $form_data = array(           
                  'mrs_no'              => $mrs_no,       
                  'unique_no'           => $this->input->post('unique_no')[$key],                  
                  'qty'                 => $this->input->post('issued_qty')[$key],
                  'length'              => $this->input->post('issued_length')[$key],
                  'location'            => $this->input->post('location')[$key],
                  'remarks'             => $this->input->post('remarks')[$key],
                  'created_by'          => $created_by,
                  'created_date'        => $date_create,
                  'status'              => 1
                );
                $this->mrs_mod->insert_mrs_detail($form_data);
            }
        }
       
        $this->session->set_flashdata('success', 'Your mrs data has been Created!');
        redirect('mrs_list');
    }

    function approve_mrs_form($mrs_no_send,$param){
                
        $mrs_no           = $mrs_no_send;
        $param_approval   = $param;
                
        $where["mrs_no"] = $mrs_no;
        $mrs_list        = $this->mrs_mod->mrs_list($where);
        $mrs_list_detail = $this->mrs_mod->mrs_list_detail($where);
        $form_data = array(
          'status' => $param_approval
        );
        $this->mrs_mod->update_mrs_data($form_data,$where);

        unset($where);

        if( $param_approval == 3){
            foreach ($mrs_list_detail as $key) {
                $form_data = array(
                    'unique_no'    => $key['unique_no'],                  
                    'qty'          => $key['qty'], 
                    'length'       => $key['length'], 
                    'project_id'   => $mrs_list[0]['project_id'], 
                    'tr_status'    => "In",
                    'tr_category'  => "MRS",
                    'remarks'      => "From MRS - ".$key['mrs_no'],
                    'tr_by'        => $this->user_cookie[0],
                );

                $this->mrs_mod->insert_mrs_transaction($form_data);
             }

             $this->session->set_flashdata('success', 'Your mrs data has been Approved!');
             redirect('mrs/mrs_list/approved');
        
        } else {
             $this->session->set_flashdata('success', 'Your mrs data has been Rejected!');
             redirect('mrs/mrs_list/rejected');
        }
       
       
       
    }

    public function mrs_pdf($mrs_no){

        $mrs_no = $this->encryption->decrypt(strtr($mrs_no, '.-~', '+=/'));
       
        $where['mrs_no']                = $mrs_no;
        $data['mrs_list']               = $this->mrs_mod->mrs_list($where);
        $data['mrs_list_detail']        = $this->mrs_mod->mrs_list_detail($where);        

        $datadb = $this->mrs_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_list'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mrs_mod->get_data_unique(null,$data['mrs_list'][0]['project_id']);
        foreach ($datadb as $value) {
            $data['unit_list'][$value['unique_ident_no']] = $value['uom'];
        }

        foreach ($datadb as $value) {
            $data['heat_no_list'][$value['unique_ident_no']] = $value['heat_or_series_no'];
        }
             
        foreach ($datadb as $value) {
            $data['description_list'][$value['unique_ident_no']] = $value['description'];
        }
        foreach ($datadb as $value) {
            $data['length_list'][$value['unique_ident_no']] = $value['length'];
        }
        foreach ($datadb as $value) {
            $data['width_od_list'][$value['unique_ident_no']] = $value['width_or_od'];
        }
        foreach ($datadb as $value) {
            $data['sch_thk_list'][$value['unique_ident_no']] = $value['sch_or_thk'];
        }

        unset($where);

        
            $this->load->library('Pdfgenerator');
            $html = $this->load->view('mrs/mrs_pdf', $data,true);
            $this->pdfgenerator->generate($html,$mrs_no);

            //$html = $this->load->view('mrs/mrs_pdf', $data);
    }


     public function mrs_import(){
      
        $data['read_cookies']     = $this->user_cookie;
        $data['meta_title']       = 'Import New MRS';
        $data['subview']          = 'mrs/mrs_import';
        $data['sidebar']          = $this->sidebar;
        $data['material_class']   = $this->mrs_mod->get_material_class();
        $data['get_project']      = $this->mrs_mod->get_project();
        $data['get_mrs_no']       = $this->mrs_mod->generate_batch_no();
        $data['get_mrs_location'] = $this->mrs_mod->get_location();
        $data['read_permission']  = $this->permission_cookie;
        $this->load->view('index', $data);

    }

    public function mrs_import_preview(){

        $id_user = $this->user_cookie;

        $config['upload_path']          = 'file/draw/';
        $config['file_name']            = 'excel_'.$id_user[0];
        $config['allowed_types']        = 'xlsx';
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('file')){
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect("engineering/draw_import");
            return false;
        }

        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('file/draw/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

        //print_r($sheet);
              
        $data['material_class']   = $this->mrs_mod->get_material_class();
        $data['get_project']      = $this->mrs_mod->get_project();
        $data['get_mrs_no']       = $this->mrs_mod->generate_batch_no();
        $data['get_mrs_location'] = $this->mrs_mod->get_location();
        $data['sheet']            = $sheet;

        $data['read_cookies']     = $this->user_cookie;
        $data['read_permission']  = $this->permission_cookie;
        $data['meta_title']       = 'MRS Import Preview';
        $data['subview']          = 'mrs/mrs_preview';
        $data['sidebar']          = $this->sidebar;
        $this->load->view('index', $data);
    }


}