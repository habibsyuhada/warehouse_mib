<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Osd extends CI_Controller {

    public function __construct() {
            
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();
        $this->load->helper('cookies');
        helper_cookies();

        $this->load->model('home_mod');
        $this->load->model('general_mod');
        $this->load->model('engineering_mod');
        $this->load->model('mrir_mod');

        //FOR osd ===================================
        $this->load->model('osd_mod');
        $this->load->model('material_catalog_mod');
        //$this->load->model('osd_category_mod');
        //===========================================

        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar = "osd/sidebar";
    }

    public function index(){
        redirect('osd/osd_list/pending');
    }

    public function osd_list($param=null){
       
        if($param == "approved"){
           $req_pages = $param;
        } else if($param == "rejected"){
           $req_pages = $param;
        } else {
           $req_pages = $param;   
        }

        $title_data = ucfirst($req_pages);

        $data['read_cookies']       = $this->user_cookie;
        $data['meta_title']         = 'O S & D List '.$title_data;
        $data['subview']            = 'osd/osd_list';
        $data['sidebar']            = $this->sidebar;
        $data['read_permission']    = $this->permission_cookie;
        $data['req_pages']          =  $req_pages;

        $this->load->view('index', $data);
    }

    public function osd_add(){
      
        $data['read_cookies']     = $this->user_cookie;
        $data['meta_title']       = 'Create New O S & D';
        $data['subview']          = 'osd/osd_new';
        $data['sidebar']          = $this->sidebar;
        $data['material_class']   = $this->osd_mod->get_material_class();
        $data['get_osd_no']       = $this->osd_mod->generate_batch_no();
        $data['get_osd_location'] = $this->osd_mod->get_location();
        $data['read_permission']  = $this->permission_cookie;
        $this->load->view('index', $data);

    }

    public function check_po_number(){

        $po_number             = $this->input->post('po_number');
        $data_po_number        = $this->osd_mod->check_po($po_number);
     
            $hasil = 0;       
            if(sizeof($data_po_number) != 0){
                $hasil = 1;
                $data  = $data_po_number[0];

                //GET project detail -------------
               
                $project = $this->osd_mod->get_project_detail($data['project_id']);
                $client_data = $project[0]['client'];
                $project_title = $project[0]['project_name'];
                $project_ref = $project[0]['project_ref'];
                $project_id = $data['project_id'];
                //----------------------------

            }
        

        echo json_encode(array(
            'client'   => @$client_data,
            'project_title' => @$project_title,
            'project_ref'   => @$project_ref,
            'project_id'   => @$project_id,
            
        ));
    }

 
    
    public function osd_detail($osd_number = null){

        $osd_number = $this->encryption->decrypt(strtr($osd_number, '.-~', '+=/'));

        if(empty($osd_number)){ 
            redirect('osd_list');
        }
        
        $where['osd_no']    = $osd_number;
        $data['osd_list']   = $this->osd_mod->osd_list($where);
        unset($where);

        $where['eproc_osd.osd_no']    = $osd_number;
        $data['osd_list_detail']   = $this->osd_mod->osd_list_detail($where);
        unset($where);

        $data['read_cookies']       = $this->user_cookie;
        $data['read_permission']    = $this->permission_cookie;
        $data['meta_title']         = 'Osd No : '.$osd_number;
        $data['subview']            = 'osd/osd_detail';
        $data['sidebar']            = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    function get_material_category(){
        $material_cat = $this->input->post('material_cat');

        $where['id'] = $material_cat;
        $data = $this->osd_mod->material_category_list($where);
        unset($where);

        echo json_encode($data);
    }

   

    //DRAWING AUTOCOMPLETE ============================================================================

    function po_number_list(){
        if (isset($_GET['term'])){
            $result = $this->osd_mod->search_po_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['po_number'];
                    echo json_encode($arr_result);
            } else {
                    $arr_result[] = "PO Number Not Found";
                    echo json_encode($arr_result);
            }
        }
    }

    //=================================================================================================

    //UNIQUE NO AUTOCOMPLETE ==========================================================================

    function uniqno_autocomplete(){
      if (($this->input->post('term'))){
            $result = $this->osd_mod->search_unique_autocomplete($this->input->post('term'),$this->input->post('po_number'));
            if ($result == TRUE){
                foreach ($result as $row)
                $arr_result[] = $row['unique_ident_no'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Unique No Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    //=================================================================================================

    //MATERIAL CATALOG AUTOCOMPLETE====================================================================

    function material_catalog_autocomplete(){
        $material_catalog = $this->input->post('material_catalog');
        if (isset($_GET['term'])){
            $result = $this->osd_mod->search_material_catalog_autocomplete($_GET['term'], $material_catalog);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['short_desc'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Material Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    function osd_check_material_catalog(){
        $material = $this->input->post('material');
        $where['short_desc'] = $material;
        $result = $this->material_catalog_mod->getAll($where);
        
        $text = 0;
        $data = array(
            "id" => $text
        );

        if(count($result) > 0){
            $text = $result[0]['id'];
            $data = array(
                "id" => $text,
                "material" => $result[0]['material'],
                "size"  => $result[0]['size'],
                "type" => $result[0]['type'],
                "weight"    => $result[0]['weight'],
                "length"    => $result[0]['length']
            );
        }

        echo json_encode($data);
    }

    //Drawing List DataTables 

    public function unique_no_check_det($id_detail = null){

        $osd_det_id  = $this->input->post('osd_det_id');
        
        $unique_by_detail  = $this->osd_mod->get_data_unique_by_detail($osd_det_id);

        $unique_no    = $unique_by_detail[0]['unique_no'];
        $po_number    = $this->input->post('po_number');


            $data_unique_no_qcs_material_id      = $this->osd_mod->get_data_unique($unique_no,$po_number);



                if(sizeof($data_unique_no_qcs_material_id) > 0){
                     
                                        
                     $mrir_no   = $data_unique_no_qcs_material_id[0]["report_no"];

                     //echo $do_pl."; ".$mrir_no;
                     echo $mrir_no;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
    }


    public function unique_no_check(){

        $unique_no   = $this->input->post('unique_no');
        $po_number   = $this->input->post('po_number');
        
             $data_unique_no_qcs_material_id      = $this->osd_mod->get_data_unique($unique_no,$po_number);

                if(sizeof($data_unique_no_qcs_material_id) > 0){
                     
                     $description   = $data_unique_no_qcs_material_id[0]["description"];
                     $od_width      = $data_unique_no_qcs_material_id[0]["width_or_od"];
                     $sch_thk       = $data_unique_no_qcs_material_id[0]["sch_or_thk"];
                     $length        = $data_unique_no_qcs_material_id[0]["length"];
                     $unit          = $data_unique_no_qcs_material_id[0]["uom"];
                     $heat_no       = $data_unique_no_qcs_material_id[0]["heat_or_series_no"];
                     $spec          = $data_unique_no_qcs_material_id[0]["spec"];
                     $brand         = $data_unique_no_qcs_material_id[0]["brand"];
                     $do_pl         = $data_unique_no_qcs_material_id[0]["do_or_pl_no"];
                     $plate_tag_no  = $data_unique_no_qcs_material_id[0]["plate_or_tag_no"];
                     $mrir_no       = $data_unique_no_qcs_material_id[0]["report_no"];

                     echo $description."; ".$od_width."; ".$sch_thk."; ".$length."; ".$unit."; ".$heat_no."; ".$spec."; ".$brand."; ".$do_pl."; ".$plate_tag_no."; ".$mrir_no;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
    }


    public function unique_no_check_preview($unique_no = null){

        $unique_no   = $this->input->post('unique_no');
        $data_unique_no_balance   = $this->osd_mod->get_bal_unique($unique_no);

        if(sizeof($data_unique_no_balance) > 0){
        
        if($data_unique_no_balance[0]["unique_no"] !== ""){

             $data_unique_no_rec_id      = $this->osd_mod->get_data_unique($unique_no);

                if(isset($data_unique_no_rec_id[0]["catalog_id"])){

                     $get_data_catalog           = $this->osd_mod->get_data_catalog($data_unique_no_rec_id[0]["catalog_id"]);

                     $description   = $get_data_catalog[0]["material"];
                     $od_width      = $get_data_catalog[0]["width_od"];
                     $sch_thk       = $get_data_catalog[0]["sch_thk"];
                     $length        = $get_data_catalog[0]["length"];
                     $unit          = $data_unique_no_rec_id[0]["uom"];
                     $heat_no       = $data_unique_no_rec_id[0]["heat_no"];
                     $balance       = $data_unique_no_balance[0]["bal_qty"];

                     echo $description."; ".$od_width."; ".$sch_thk."; ".$length."; ".$unit."; ".$heat_no."; ".$balance;
          
                } else {

                     echo "Error : Unique Number Not Found..";
          
                }
            

        } else { 

            echo "Error : Unique Number Not Found..";

        }

    } else {

       echo "Error : Unique Number Not Found.."; 

    }
        
    }


     public function balance_checking(){

        $unique_no   = $this->input->post('unique_no');
        $issued_qty   = $this->input->post('issued_qty');

        if(is_numeric($issued_qty)){

                $data_unique_no_balance   = $this->osd_mod->get_bal_unique($unique_no);

                if(sizeof($data_unique_no_balance) > 0){

                    $balance_after_check = $data_unique_no_balance[0]["bal_qty"] - $issued_qty;
                    if($balance_after_check < 0){
                        echo "Error : Sorry, We Just only have ".$data_unique_no_balance[0]["bal_qty"]; 
                    } else {
                        echo "Balance Available"; 
                    }

                } else {
                    echo "Error : Balance Shortage!"; 
                }
        } else {
            echo "Error : Input Correct Number!"; 
        }
    }

    function osd_list_json($param=null)
    {
        error_reporting(0);

        if($param == "approved"){
           $req_pages = 3;
        } else if($param == "rejected"){
           $req_pages = 2;
        } else {
           $req_pages = 1;   
        }

        $datadb = $this->osd_mod->get_user_data();       
        foreach ($datadb as $value) {
            $user_list[$value['id_user']] = $value['full_name'];
        }
 
        $list = $this->osd_mod->get_datatables_osd_list_dt($req_pages);
      
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {
          
            $links = base_url()."osd_detail/".strtr($this->encryption->encrypt($list->osd_no), '+=/', '.-~');
            
            $no++;
            $row   = array();

            if($list->status == 1){
                 $status = "<span style='font-weight:bold;color:red;'>"."PENDING"."</style>";
            } else if($list->status == 2){
                 $status = "<span style='font-weight:bold;color:red;'>"."CANCELED"."</style>";     
            } else if($list->status == 3){
                 $status = "<span style='font-weight:bold;color:green;'>"."CLOSED"."</style>";
            }
            
            $row[] = (isset($user_list[$list->create_by]) ? $user_list[$list->create_by] : '-');
            $row[] = date("Y-m-d",strtotime($list->create_date));
            $row[] = $list->osd_no;
            $row[] = $list->po_number;
            $row[] = $status;
           
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-file-alt'></i></a>";
                       
            $data[] = $row;
            
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->osd_mod->count_all_osd_list_dt($req_pages),
            "recordsFiltered" => $this->osd_mod->count_filtered_osd_list_dt($req_pages),
            "data" => $data
        );
       
        echo json_encode($output);
    }


    function osd_new_form(){
                
        $osd_no        = $this->input->post('osd_no');
        $po_number     = $this->input->post('po_number');
        $project_id    = $this->input->post('project_id');
        $created_by    = $this->user_cookie[0];
        $created_date  = date("Y-m-d");
     

        $form_data = array(
          'osd_no'      => $osd_no,
          'po_number'   => $po_number,        
          'project_id'  => $project_id,
          'create_by'   => $created_by,
          'create_date' => $created_date,
          'status'      => 1
        );

        $this->osd_mod->insert_osd_master($form_data);

        $unique_no = $this->input->post('unique_no');
        if($unique_no){
            foreach ($unique_no as $key => $value) {
                $form_data = array(           
                  'osd_no'      => $osd_no,       
                  'unique_no'   => $this->input->post('unique_no')[$key],                  
                  'over'        => $this->input->post('over')[$key],
                  'shortage'    => $this->input->post('shortage')[$key],
                  'damage'      => $this->input->post('damage')[$key],
                  'remarks'     => $this->input->post('remarks')[$key],                  
                  'create_by'   => $created_by,
                  'create_date' => $created_date,
                  'status_osd'  => 1
                );
                $this->osd_mod->insert_osd_detail($form_data);
            }
        }
       
        $this->session->set_flashdata('success', 'Your osd data has been Created!');
        redirect('osd_list');
    }



    function approve_osd_form(){
        
      $id_detail    = $this->input->post("id");
      $id_checkbox  = $this->input->post("id_checkbox");

      $osd_no       = $this->input->post("osd_no");
      $action_osd   = $this->input->post("action_osd");        
      $remarks      = $this->input->post("remarks");
      $unique_no    = $this->input->post("unique_no");

      $shortage     = $this->input->post("shortage");
      $damage       = $this->input->post("damage");
      $over         = $this->input->post("over");


      $where["osd_no"] = $osd_no[1];
      $osd_list = $this->osd_mod->osd_list($where);
      $osd_list_detail = $this->osd_mod->osd_list_detail_search($where);
       unset($where);

      $po_number_cur  = $osd_list[0]["po_number"];


      foreach ($id_detail as $key => $value) {

        $total_osd_qty =  $shortage[$key] + $damage[$key] + $over[$key];

        if($id_checkbox[$key] == 1){

            $where["osd_det_id"] = $id_detail[$key];

            $update_data_detail  =    array(           
                                      'remarks'      => $remarks[$key],              
                                      'action_osd'   => $action_osd[$key],
                                      'status_osd'   => "1",
                                    );

            $this->osd_mod->osd_detail_edit_process_db($update_data_detail,$where);
            unset($where);


        }

      }

      $where["osd_no"] = $osd_no[1];
      $where["status_osd"] = 0;
      $get_totalOSDClosed = $this->osd_mod->osd_list_detail_search($where);
      unset($where);


      if(sizeof($get_totalOSDClosed) > 0){
        $param_approval = 1;
      } else if(sizeof($get_totalOSDClosed) <= 0){
        $param_approval = 3;
      }

   
      $where["osd_no"] = $osd_no[1];
      $form_data = array(
         'status' => $param_approval
       );
      $this->osd_mod->update_osd_data($form_data,$where);

      if($param_approval == 3){        

             $this->session->set_flashdata('success', 'Your osd data has been Completed!');
             redirect('osd/osd_list/approved');
        
        } else {

             $this->session->set_flashdata('success', 'Your osd data has been Updated!');
             redirect('osd/osd_list/pending');

        }
       
     
    }

  

    public function osd_pdf($report_no){

        $osd_no = $this->encryption->decrypt(strtr($report_no, '.-~', '+=/'));
       
        $where['osd_no']          = $osd_no;
        $data['osd_list']         = $this->osd_mod->osd_list($where);
        $data['osd_list_detail']  = $this->osd_mod->osd_list_detail_search($where);
                
        $this->load->library('Pdfgenerator');
       
        //$html = $this->load->view('osd/osd_pdf', $data);
        
        $html = $this->load->view('osd/osd_pdf', $data, true);
        $this->pdfgenerator->generate($html,$osd_no);
    }

}