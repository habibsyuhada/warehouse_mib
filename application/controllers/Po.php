<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Po extends CI_Controller {

	public function __construct() {
            
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();
        $this->load->helper('cookies');
        helper_cookies();

        $this->load->model('home_mod');
        
        $this->load->model('mr_mod');
        $this->load->model('po_mod');
       
        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar = "po/sidebar";
    }

	public function index(){
        redirect('po/mr_list');
    }

    public function mr_list($status=null){

        $data['read_cookies']    = $this->user_cookie;
        $data['meta_title']      = 'Material Requisition List';
        $data['subview']         = 'po/mr_list';
        $data['sidebar']         = $this->sidebar;
        $data['read_permission'] = $this->permission_cookie;

        $data['status']    = "3";
        if(isset($status)){
             $data['type_req']  = $status;
        } else {
             $data['type_req']  = "po";
        }
       
        $this->load->view('index', $data);
    }


    //----------------------START DATATABLE SERVER SIDE ----------------------------

    function mr_list_json($status=null,$type_req=null){



        $list = $this->po_mod->get_datatables_mr_list_dt($status,$type_req);
      
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $list){

            

            $links = base_url()."po/mr_detail/".strtr($this->encryption->encrypt($list->mr_number), '+=/', '.-~')."/".$list->vendor_winner;

            $datadb = $this->mr_mod->get_user_data();
            foreach ($datadb as $value) {
             $user_data[$value['id_user']] = $value['full_name'];
            }

            $datadb = $this->mr_mod->get_data_department();
            foreach ($datadb as $value) {
             $dept[$value['id_department']] = $value['name_of_department'];
            }


            $datadb = $this->mr_mod->get_data_category();
            foreach ($datadb as $value) {
             $budget[$value['id']] = $value['category_name'];
            }

            $datadb = $this->mr_mod->get_vendor_list();
             foreach ($datadb as $value) {
             $vendor_type[$value['id_vendor']] = $value['name'];
            }
            
          
            $no++;
            $row   = array();
                                 
            $row[] = $budget[$list->budget_cat_id];
            $row[] = $dept[$list->budget_dept_id];
            $row[] = $list->mr_number;                
            $row[] = $vendor_type[$list->vendor_winner];                
                           
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-list-alt'></i></a>";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw"      => $_POST['draw'],
            "recordsTotal" => $this->po_mod->count_all_mr_list_dt($status,$type_req),
            "recordsFiltered" => $this->po_mod->count_filtered_mr_list_dt($status,$type_req),
            "data" => $data
        );

        echo json_encode($output);
    }

    //----------------------STOP DATATABLE SERVER SIDE ----------------------------

    
    public function mr_detail($mr_number = null,$status_po= null,$vendor_id = null){


        $mr_number  = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));
        $status_pox  = $this->encryption->decrypt(strtr($status_po, '.-~', '+=/'));
        
        //print_r($status_po);


        if(empty($mr_number)){ 
            redirect('po/mr_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->po_mod->mr_list($where);
        unset($where);

        $where['`eproc_mr_detail.mr_number`']  = $mr_number;
        $where['`eproc_mr_detail.vendor_winner`']  = $status_po;
        $data['mr_list_detail']     = $this->po_mod->mr_list_detail_po_create_data($where,null,null);
        unset($where);

        $data['vendor']            = $this->po_mod->get_vendor();
        $data['currency_list']     = $this->mr_mod->get_currency_list();
        $data['vendor_quo']        = $this->po_mod->get_vendor_list_of_mr($mr_number);

        $datadb            = $this->po_mod->get_vendor();
        foreach ($datadb as $value) {
            $data['vendor_data'][$value['id_vendor']] = $value['name'];
        }

        $datadb = $this->po_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['material'][$value['code_material']] = $value['material'];
        }


        $datadb = $this->po_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_department();
        foreach ($datadb as $value) {
           $data['dept'][$value['id_department']] = $value['name_of_department'];
        }

        $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom'][$value['id_uom']] = $value['uom'];
        }

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);
       
       
        $data['src_total_mr_detail']    = sizeof($data['mr_list_detail']);
        $data['status_po']              = $status_po;
        $data['read_cookies']           = $this->user_cookie;
        $data['read_permission']        = $this->permission_cookie;
       
        $data['meta_title']             =  $data['mr_list'][0]['mr_number'];
        $data['subview']                = 'po/mr_detail';
        $data['sidebar']                = $this->sidebar;
        
        $this->load->view('index', $data);

    }


    public function mr_detail_after($mr_number = null,$status_po= null){

        $mr_number       = $this->input->post("mr_numberx");
        $mr_numberx      = $this->input->post("mr_number");

        $tec_spec        = $this->input->post("tec_spec");
        $req_qty         = $this->input->post("req_qty");
        $uom_req         = $this->input->post("uom_req");

        $price_per_unit  = $this->input->post("price_per_unit");
        $total_amount    = $this->input->post("total_amount");
        $currency        = $this->input->post("currency");
        $remarks_po      = $this->input->post("remarks_po");
        $id              = $this->input->post("id");
        $id_checkbox     = $this->input->post("id_checkbox");

        $cat_id          = $this->input->post("cat_id");
        $dept_id         = $this->input->post("dept_id");

        $data['mr_numberx']         = $mr_numberx;
        $data['tec_spec']           = $tec_spec;
        $data['req_qty']            = $req_qty;
        $data['uom_req']            = $uom_req;
        $data['price_per_unit']     = $price_per_unit;
        $data['total_amount']       = $total_amount;
        $data['currency']           = $currency;
        $data['remarks_po']         = $remarks_po;
        $data['id']                 = $id;
        $data['id_checkbox']        = $id_checkbox;

        $data['cat_id']             = $cat_id;
        $data['dept_id']            = $dept_id;

        $data['total_all_amount']        = array_sum($total_amount);

        //print_r($data['total_all_amount']);


        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->po_mod->mr_list($where);
        unset($where);

        $where['`eproc_mr_detail.mr_number`']  = $mr_number;
        $data['mr_list_detail']     = $this->po_mod->mr_list_detail_po_create($where,null,null);
        unset($where);

        $data['vendor']            = $this->po_mod->get_vendor();
        $data['currency_list']     = $this->mr_mod->get_currency_list();
        $data['vendor_quo']        = $this->po_mod->get_vendor_list_of_mr($mr_number);

        $datadb            = $this->po_mod->get_vendor();
        foreach ($datadb as $value) {
            $data['vendor_data'][$value['id_vendor']] = $value['name'];
        }

        $datadb = $this->po_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['material'][$value['code_material']] = $value['material'];
        }


        $datadb = $this->po_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_department();
        foreach ($datadb as $value) {
           $data['dept'][$value['id_department']] = $value['name_of_department'];
        }

        $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom'][$value['id_uom']] = $value['uom'];
        }

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);
       
       
        $data['src_total_mr_detail']    = sizeof($data['mr_list_detail']);
        $data['status_po']              = $status_po;
        $data['read_cookies']           = $this->user_cookie;
        $data['read_permission']        = $this->permission_cookie;
       
        $data['meta_title']             =  $data['mr_list'][0]['mr_number'];
        $data['subview']                = 'po/mr_detail_2';
        $data['sidebar']                = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    public function mr_excel_2($mr_no,$po_nos){

        $mr_number = $this->encryption->decrypt(strtr($mr_no, '.-~', '+=/'));
        $po_number = $this->encryption->decrypt(strtr($po_nos, '.-~', '+=/'));
        
      
        $datadb = $this->po_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->po_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['material'][$value['code_material']] = $value['material'];
        }

        $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom'][$value['id_uom']] = $value['uom'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }

              
        $where['mr_number']     = $mr_number;
        $data['mr_list']        = $this->po_mod->mr_list($where);
        unset($where);

        $where['po_number']     = $po_number;
        $data['mr_list_detail'] = $this->po_mod->mr_list_detail_po($where);
        unset($where);

           
        $html = $this->load->view('po/mr_excel', $data);

    }

    public function process_insert_po(){

         $id          = $this->input->post('id');
         $id_checkbox = $this->input->post('id_checkbox');
         $no          = $this->input->post('no');
         
         $mr_number   = $this->input->post('mr_number');

         $po_number   = $this->input->post('po_number');
         $cat_id      = $this->input->post('cat_id');
         $dept_id     = $this->input->post('dept_id');

         $po_qty      = $this->input->post('po_qty');

         $price_per_unit = $this->input->post('price_per_unit');
         $total_amount   = $this->input->post('total_amount');
         $currency       = $this->input->post('currency');

         //$mr_number   = $this->input->post('mr_no');
         $remarks_po  = $this->input->post('remarks_po');
         $mat_code    = $this->input->post('mat_code');

         $vendor      = $this->input->post('vendor');
         $eta_date    = $this->input->post('eta_date');

         

        foreach ($no as $key => $value) {
        
        if($id_checkbox[$key] == 1){
                
            $dt_id     = $id[$key]; 
            $data_poz  = $this->po_mod->check_id_po($dt_id);    

            $data_po = array(
                        "po_number"        => $po_number[$key],
                        "cat_id"           => $cat_id,
                        "dept_id"          => $dept_id,
                        "po_qty"           => $po_qty[$key],
                        "price_per_unit"   => $price_per_unit[$key],
                        "total_amount"     => $total_amount[$key],
                        "id_cur"           => $currency[$key],
                        "mr_detail_id"     => $id[$key],
                        "remarks_po"       => $remarks_po[$key],
                        "vendor"           => $vendor[$key],
                        "eta_date"         => $eta_date[$key],
                        "created_by_po"    => $this->user_cookie[0],
                        "created_date_po"  => date("Y-m-d H:i:s"),
                        "status"           => "1"
                    );
                
               $this->po_mod->po_new_process_db($data_po);

        }    
         
        }

        $mr_number_enc = strtr($this->encryption->encrypt($mr_number[0]), '+=/', '.-~'); 
        $this->session->set_flashdata('message','<script type="text/javascript">Swal.fire({type: "success",title: "Success",text: "PO has been filled up..",}).then(function() { window.location = "'.base_url().'po/mr_detail/'.$mr_number_enc.'"});</script>');

       redirect('po/mr_detail/'.$mr_number_enc);  
    }


    public function process_update_po(){

        $submitBtn             = $this->input->post('submitBtn');
        $po_number             = $this->input->post('po_number');        
        $mr_number             = $this->input->post('mr_numberx');

    if($submitBtn == "save_term"){
        
         $cat_id            = $this->input->post('cat_id');
         $dept_id           = $this->input->post('dept_id');

         $description       = $this->input->post('tec_spec');
         $po_qty            = $this->input->post('req_qty');
         $price_per_unit    = $this->input->post('price_per_unit');
         $total_amount      = $this->input->post('total_amount');
         $id_cur            = $this->input->post('currency');
         $remarks_po        = $this->input->post('remarks_po');
         $total_all_amount  = $this->input->post('total_all_amount');
         $diskon            = $this->input->post('diskon');
         $ppn               = $this->input->post('ppn');
         $grand_total       = $this->input->post('grand_total');
         $mr_detail_id      = $this->input->post('id');

         foreach ($mr_detail_id as $key => $value) {
          
            $update_data_po = array(
                'cat_id'            => $cat_id, 
                'dept_id'           => $dept_id, 
                'mr_number'         => $mr_number, 
                'po_number'         => $po_number, 
                'description'       => $description[$key], 
                'po_qty'            => $po_qty[$key],
                'price_per_unit'    => $price_per_unit[$key],
                'total_amount'      => $total_amount[$key],
                'id_cur'            => $id_cur[$key],
                'total_all_amount'  => $total_all_amount,
                'diskon'            => $diskon,
                'ppn'               => $ppn,
                'grand_total'       => $grand_total,
                'mr_detail_id'      => $mr_detail_id[$key],
                'created_by_po'     => $this->user_cookie[0],
                'created_date_po'   => date("Y-m-d H:i:s"),
            );

            $search_term = $this->po_mod->save_data_po_new($update_data_po);
        
        }

        

         $kepada_to             = $this->input->post('kepada_to');
         $jadwal_pekerjaan      = $this->input->post('jadwal_pekerjaan');
         $lokasi_kerja          = $this->input->post('lokasi_kerja');
         $lokasi_pengiriman     = $this->input->post('lokasi_pengiriman');
         $pembayaran            = $this->input->post('pembayaran');
         $lain_lain             = $this->input->post('lain_lain');

         

         $search_term = $this->po_mod->get_term_data($mr_number);

         $update_data = array(
            'kepada_to'         => $kepada_to, 
            'mr_number'         => $mr_number, 
            'po_number'         => $po_number, 
            'jadwal_pekerjaan'  => $jadwal_pekerjaan,
            'lokasi_kerja'      => $lokasi_kerja,
            'lokasi_pengiriman' => $lokasi_pengiriman,
            'pembayaran'        => $pembayaran,
            'lain_lain'         => $lain_lain
        );

         if(sizeof($search_term) <= 0){
            $search_term = $this->po_mod->insert_po_data($update_data);
         } else {
            $search_term = $this->po_mod->update_po_data($update_data,$mr_number);
         }

         

     } else if($submitBtn == "approve_po_manager"){

        $update_data = array(
            'manager_approval_status'     => "3",
            'manager_approval_by'         => $this->user_cookie[0],
            'manager_approval_date'       => date("Y-m-d H:i:s"),
        );
        $where['po_number'] = $po_number;

        $update_status = $this->po_mod->update_po_data_item($update_data,$where);
        unset($where);

    } else if($submitBtn == "reject_po_manager"){

        $update_data = array(
            'manager_approval_status'     => "2",
            'manager_approval_by'         => $this->user_cookie[0],
            'manager_approval_date'       => date("Y-m-d H:i:s"),
        );
        $where['po_number'] = $po_number;
        $update_status = $this->po_mod->update_po_data_item($update_data,$where);  
        unset($where);

  
    } else if($submitBtn == "approve_po_bod"){

        $update_data = array(
            'status_approval'         => "3",
            'bod_approval_status'     => "3",
            'bod_approval_by'         => $this->user_cookie[0],
            'bod_approval_date'       => date("Y-m-d H:i:s"),
        );
        $where['po_number'] = $po_number;

        $update_status = $this->po_mod->update_po_data_item($update_data,$where);
        unset($where);

    } else if($submitBtn == "reject_po_bod"){

        $update_data = array(
            'status_approval'         => "2",
            'bod_approval_status'     => "2",
            'bod_approval_by'         => $this->user_cookie[0],
            'bod_approval_date'       => date("Y-m-d H:i:s"),
        );
        $where['po_number'] = $po_number;
        $update_status = $this->po_mod->update_po_data_item($update_data,$where);  
        unset($where);

     }

     $mr_number_enc = strtr($this->encryption->encrypt($mr_number), '+=/', '.-~'); 
     $po_number_enc = strtr($this->encryption->encrypt($po_number), '+=/', '.-~'); 
      redirect('po/po_detail/'.$mr_number_enc."/".$po_number_enc);  

        
    }

    public function po_list($type_req=null){

        $data['read_cookies']       = $this->user_cookie;
        $data['meta_title']         = 'PO List';
        $data['subview']            = 'po/po_list';
        $data['sidebar']            = $this->sidebar;
        $data['read_permission']    = $this->permission_cookie;

         if(isset($type_req)){
             $data['type_req']  = $type_req;
        } else {
             $data['type_req']  = "po";
        }


        $this->load->view('index', $data);
    }

    function po_list_json($type_req=null){
        error_reporting(0);

        $datadb = $this->po_mod->get_user_data();       
        foreach ($datadb as $value) {
            $user_list[$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_category();
            foreach ($datadb as $value) {
             $budget[$value['id']] = $value['category_name'];
        }
      
        $list = $this->po_mod->get_datatables_po_list_dt($req_pages,$type_req);
      
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $list){
          
            $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($list->mr_number), '+=/', '.-~')."/".strtr($this->encryption->encrypt($list->po_number), '+=/', '.-~');
            
            $no++;
            $row   = array();

            $row[] = (isset($budget[$list->budget_cat_id]) ? $budget[$list->budget_cat_id] : '-');              
            $row[] = $list->po_number;
            
            $row[] = $list->mr_number;
            $row[] = (isset($user_list[$list->created_by]) ? $user_list[$list->created_by] : '-');
            $row[] = $list->created_date_po;                 
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-file-alt'></i></a>";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->po_mod->count_all_po_list_dt($req_pages,$type_req),
            "recordsFiltered" => $this->po_mod->count_filtered_po_list_dt($req_pages,$type_req),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function po_detail($mr_number = null,$po_number= null){

        $mr_number = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));
        $po_number = $this->encryption->decrypt(strtr($po_number, '.-~', '+=/'));

        $data['vendor_data']  = $this->po_mod->get_vendor();
          
        if(empty($mr_number)){ 
            redirect('po/po_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->po_mod->mr_list($where);
        unset($where);

        $where['po_number']         = $po_number;
        $data['mr_list_detail']     = $this->po_mod->mr_list_detail_po($where);
        $data['total_amount_val']   = $this->po_mod->mr_list_detail_po_sum($where);
        
        unset($where);

        $data['term']   = $this->po_mod->check_term_updated($mr_number,$po_number);

        if(sizeof($data['mr_list_detail']) <= 0){
            redirect('po/po_list');
        }

        $datadb  = $this->po_mod->get_vendor();
        foreach ($datadb as $value) {
            $data['data_vendor'][$value['id_vendor']] = $value['name'];
        }

        $datadb = $this->po_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['material'][$value['code_material']] = $value['material'];
        }
       
        $datadb = $this->po_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

         $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom'][$value['id_uom']] = $value['uom'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }
       
        $data['src_total_mr_detail']    = sizeof($data['mr_list_detail']);
        $data['read_cookies']           = $this->user_cookie;
        $data['read_permission']        = $this->permission_cookie;
        $data['po_nos']                 = $po_number;
       
        $data['meta_title']             = "PO Reference : ".$po_number;
        $data['subview']                = 'po/po_detail';
        $data['sidebar']                = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    public function check_po_number(){

        $pos        = $this->input->post('pos');
        
        $data_vendor = $this->po_mod->get_vendor_by_po($pos);
        
        if(sizeof($data_vendor) > 0){ 
            echo  $data_vendor[0]['vendor'];
        } 
    }

    public function delete_data($param = null,$mr = null, $po = null){

      $this->po_mod->delete_data_po($param);

      $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($mr), '+=/', '.-~')."/".strtr($this->encryption->encrypt($po), '+=/', '.-~');
       
      redirect($links); 

    }


     public function po_pdf_format($mr_number = null,$po_number= null){

         $mr_number = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));
        $po_number = $this->encryption->decrypt(strtr($po_number, '.-~', '+=/'));

        $data['vendor_data']  = $this->po_mod->get_vendor();
          
        if(empty($mr_number)){ 
            redirect('po/po_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->po_mod->mr_list($where);
        unset($where);

        $where['po_number']         = $po_number;
        $data['mr_list_detail']     = $this->po_mod->mr_list_detail_po($where);
        $data['total_amount_val']   = $this->po_mod->mr_list_detail_po_sum($where);
        
        unset($where);

        $data['term']   = $this->po_mod->check_term_updated($mr_number,$po_number);

        if(sizeof($data['mr_list_detail']) <= 0){
            redirect('po/po_list');
        }

        $datadb  = $this->po_mod->get_vendor();
        foreach ($datadb as $value) {
            $data['data_vendor'][$value['id_vendor']] = $value['name'];
        }

        $datadb = $this->po_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['material'][$value['code_material']] = $value['material'];
        }
       
        $datadb = $this->po_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

         $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom'][$value['id_uom']] = $value['uom'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }
       
        $data['src_total_mr_detail']    = sizeof($data['mr_list_detail']);
        $data['read_cookies']           = $this->user_cookie;
        $data['read_permission']        = $this->permission_cookie;
        $data['po_nos']                 = $po_number;
       
        $data['meta_title']             = "PO Reference : ".$po_number;
        $data['subview']                = 'po/po_pdf';
        $data['sidebar']                = $this->sidebar;

        //$this->load->view('po/po_pdf', $data);
      
        //$this->load->view('mr/mr_pdf_format', $data);

        $this->load->library('Pdfgenerator_potrait');
        $html = $this->load->view('po/po_pdf', $data,true);
        $this->pdfgenerator_potrait->generate($html,$mr_number);
        

    }

  //end
}