<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Po extends CI_Controller {

	public function __construct() {
            
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();
        $this->load->helper('cookies');
        helper_cookies();

        $this->load->model('home_mod');
        
        $this->load->model('mr_mod');
        $this->load->model('po_mod');


        $this->load->model('material_catalog_mod');

       
        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
        $this->sidebar = "po/sidebar";
    }

	public function index(){
        redirect('po/po_new');
    }

    public function po_new($status=null){

        $data['read_cookies']    = $this->user_cookie;
        $data['meta_title']      = 'Create New PO';
        $data['subview']         = 'po/po_new';
        $data['sidebar']         = $this->sidebar;
        $data['read_permission'] = $this->permission_cookie;

        $data['status']  = "3";
       
        $this->load->view('index', $data);
    }


    //----------------------START DATATABLE SERVER SIDE ----------------------------

    function mr_list_json($status=null){

        $list = $this->po_mod->get_datatables_mr_list_dt($status);
      
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $list){

            $data_count = $this->po_mod->count_mr_po_data($list->mr_number);    

            $sum_total_mr = 0;
            $sum_total_po = 0;

            foreach($data_count as $key=>$value)
            {
                $sum_total_mr+= $value["total_mr_qty"];
                $sum_total_po+= $value["total_all_po"];
            }

            $balance_qty = $sum_total_mr - $sum_total_po;

            if($balance_qty > 0){
               $status_po = '<span class="badge badge-danger">Open Item : '.$balance_qty.' Pcs</span>';
              
            } else {
               $status_po = '<span class="badge badge-success">PO Number Added</span>';
               
            }

            $links = base_url()."po/mr_detail/".strtr($this->encryption->encrypt($list->mr_number), '+=/', '.-~');

            $datadb = $this->mr_mod->get_user_data();
            foreach ($datadb as $value) {
             $user_data[$value['id_user']] = $value['full_name'];
            }

            $datadb = $this->mr_mod->get_data_department();
            foreach ($datadb as $value) {
             $dept[$value['id_department']] = $value['name_of_department'];
            }


            $datadb = $this->mr_mod->get_data_category();
            foreach ($datadb as $value) {
             $budget[$value['id']] = $value['category_name'];
            }
            
          
            $no++;
            $row   = array();
                                 
            $row[] = $budget[$list->budget_cat_id];
            $row[] = $dept[$list->budget_dept_id];
            $row[] = $list->form_number;                
            $row[] = $user_data[$list->created_by];                   
            $row[] = date("d F Y", strtotime($list->created_date));                   
            $row[] = $status_po;                   
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-list-alt'></i></a>";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw"      => $_POST['draw'],
            "recordsTotal" => $this->po_mod->count_all_mr_list_dt($status),
            "recordsFiltered" => $this->po_mod->count_filtered_mr_list_dt($status),
            "data" => $data
        );

        echo json_encode($output);
    }

    //----------------------STOP DATATABLE SERVER SIDE ----------------------------

    
    public function mr_detail($mr_number = null,$status_po= null){


        $mr_number  = $this->encryption->decrypt(strtr($mr_number, '.-~', '+=/'));
        $status_po  = $this->encryption->decrypt(strtr($status_po, '.-~', '+=/'));

        //print_r($mr_number);

        if(empty($mr_number)){ 
            redirect('po/mr_list');
        }

        $where['mr_number']         = $mr_number;
        $data['mr_list']            = $this->po_mod->mr_list($where);
        unset($where);

        $where['mr_number']         = $mr_number;
        $data['mr_list_detail']     = $this->po_mod->mr_list_detail_po_create($where,null,null);
        unset($where);

        $data['vendor']            = $this->po_mod->get_vendor();
        $data['currency_list']     = $this->mr_mod->get_currency_list();

        $datadb            = $this->po_mod->get_vendor();
        foreach ($datadb as $value) {
            $data['vendor_data'][$value['id_vendor']] = $value['name'];
        }

        $datadb = $this->po_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['material'][$value['code_material']] = $value['material'];
        }


        $datadb = $this->po_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_department();
        foreach ($datadb as $value) {
           $data['dept'][$value['id_department']] = $value['name_of_department'];
        }

        $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom'][$value['id_uom']] = $value['uom'];
        }

        $data['vendor_list']    = $this->mr_mod->get_vendor_list();
        $data['uom_list']       = $this->mr_mod->get_uom_list();
        $data['currency_list']  = $this->mr_mod->get_currency_list();

        $where['eb.year'] = date("Y");
        $data['budget']  = $this->mr_mod->get_data_budgeting($this->user_cookie[4],$where);
        unset($where);
       
       
        $data['src_total_mr_detail']    = sizeof($data['mr_list_detail']);
        $data['status_po']              = $status_po;
        $data['read_cookies']           = $this->user_cookie;
        $data['read_permission']        = $this->permission_cookie;
       
        $data['meta_title']             =  $data['mr_list'][0]['form_number'];
        $data['subview']                = 'po/mr_detail';
        $data['sidebar']                = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    public function mr_excel_2($mr_no,$po_nos){

        $mr_number = $this->encryption->decrypt(strtr($mr_no, '.-~', '+=/'));
        $po_number = $this->encryption->decrypt(strtr($po_nos, '.-~', '+=/'));
        
      
        $datadb = $this->po_mod->get_user_data();
        foreach ($datadb as $value) {
            $data['user_data'][$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->po_mod->get_data_catalog();
        foreach ($datadb as $value) {
            $data['material'][$value['code_material']] = $value['material'];
        }

        $datadb = $this->mr_mod->get_uom_list();
        foreach ($datadb as $value) {
           $data['uom'][$value['id_uom']] = $value['uom'];
        }

        $datadb = $this->mr_mod->get_currency_list();
        foreach ($datadb as $value) {
           $data['cur'][$value['id_cur']] = $value['currency'];
        }

              
        $where['mr_number']     = $mr_number;
        $data['mr_list']        = $this->po_mod->mr_list($where);
        unset($where);

        $where['po_number']     = $po_number;
        $data['mr_list_detail'] = $this->po_mod->mr_list_detail_po($where);
        unset($where);

           
        $html = $this->load->view('po/mr_excel', $data);

    }

    public function process_insert_po(){

        $user_id            = $this->user_cookie[0];
        $code               = $this->po_mod->po_generate_batch_no();
        $quotation          = $this->input->post('quotation');         
        $date_po            = date('Y-m-d', strtotime($this->input->post('date_po')));
        $date_quotation     = date('Y-m-d', strtotime($this->input->post('date_quotation')));
        $address            = $this->input->post('address');
        $reference          = $this->input->post('reference');
        $assign_to          = $this->input->post('assign_to');

        $form_data = array(
            'po_number'         => $code,
            'quotation'         => $quotation,
            'date_po'           => $date_po,
            'date_quotation'    => $date_quotation,
            'address'           => $address,
            'reference'         => $reference,
            'assign_to'         => $assign_to,
            'created_date_po'   => date('Y-m-d'),
            'created_by_po'     => $user_id,
            'status'            => 1
        );

        $this->po_mod->po_new_process_db($form_data);
        unset($form_data);

        // PO DETAIL -------------------------------------------------
        $mr_number          = $this->input->post('mr_number');
        $id_material        = $this->input->post('id_material');
        $mr_detail_id       = $this->input->post('mr_detail_id');
        $qty                = $this->input->post('qty');
        $id_uom             = $this->input->post('id_uom');
        $price_per_unit     = $this->input->post('price_per_unit');
        $total_price        = $this->input->post('total_price');

        $form_data = array();
        foreach ($id_material as $key => $value) {
            array_push($form_data, array(
                'po_number'         => $code,
                'mr_detail_id'      => $mr_detail_id[$key],
                'catalog_id'        => $id_material[$key],
                'qty'               => $qty[$key],
                'price_per_unit'    => $price_per_unit[$key],
            ));
        }

        $this->po_mod->po_detail_add_process($form_data);
        // -----------------------------------------------------------

        $this->session->set_flashdata('success', 'Data successful added!');

       redirect('po/po_list/waiting');
    }


    public function process_update_po(){

         $id          = $this->input->post('id');
         $id_checkbox = $this->input->post('id_checkbox');

         $po_number   = $this->input->post('po_number');
         $mr_number   = $this->input->post('mr_no');

         foreach ($id_checkbox as $key => $value) {
        
            if($id_checkbox[$key] == 1){

                $dt_id   = $id[$key];
                $data_po = array(
                    "po_number"         => $po_number[$key],
                    "modified_by_po"    => $this->user_cookie[0],
                    "modified_date_po"  => date("Y-m-d H:i:s")
                );
                
                $where["id_po"] = $dt_id;

               $this->po_mod->po_update_data($data_po,$where);
               unset($where);

            }
         }

        $mr_number_enc = strtr($this->encryption->encrypt($mr_number), '+=/', '.-~'); 
        redirect('po/po_list');  
    }

    public function po_list($param){
        $data['param']              = $param;
        $data['read_cookies']       = $this->user_cookie;
        $data['meta_title']         = 'PO '.ucfirst($param).' List';
        $data['subview']            = 'po/po_list';
        $data['sidebar']            = $this->sidebar;
        $data['read_permission']     = $this->permission_cookie;

        $this->load->view('index', $data);
    }

    function po_list_json(){
        error_reporting(0);

        $datadb = $this->po_mod->get_user_data();       
        foreach ($datadb as $value) {
            $user_list[$value['id_user']] = $value['full_name'];
        }

        $datadb = $this->mr_mod->get_data_category();
            foreach ($datadb as $value) {
             $budget[$value['id']] = $value['category_name'];
        }
      
        $list = $this->po_mod->get_datatables_po_list_dt($req_pages);
      
        $data = array();
        $no   = $_POST['start'];

        foreach ($list as $list){
          
            $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($list->po_number), '+=/', '.-~');
            
            $no++;
            $row   = array();

            $row[] = substr($list->po_number, 3).'/MEB-M&R-BTM/II/'.date('Y', strtotime($list->date_po));
            $row[] = $list->quotation;
            $row[] = $list->date_po;
            $row[] = $list->date_quotation;
            $row[] = $list->address;
            $row[] = $list->reference;
            $row[] = $list->assign_to;
            $row[] = '';
            $row[] = "<a href='".$links."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-file-alt'></i></a>";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->po_mod->count_all_po_list_dt($req_pages),
            "recordsFiltered" => $this->po_mod->count_filtered_po_list_dt($req_pages),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function po_detail($po_number = null){

        $po_number = $this->encryption->decrypt(strtr($po_number, '.-~', '+=/'));

        $where['po_number']         = $po_number;
        $data['po_list']            = $this->po_mod->po_list($where);
        $data['po_detail_list']     = $this->po_mod->po_detail_list($where);
        unset($where);
       
        $data['read_cookies']           = $this->user_cookie;
        $data['read_permission']        = $this->permission_cookie;
        $data['po_nos']                 = $po_number;
       
        $data['meta_title']             = "PO Number : ".$po_number;
        $data['subview']                = 'po/po_detail';
        $data['sidebar']                = $this->sidebar;
        
        $this->load->view('index', $data);

    }

    public function check_po_number(){

        $pos        = $this->input->post('pos');
        
        $data_vendor = $this->po_mod->get_vendor_by_po($pos);
        
        if(sizeof($data_vendor) > 0){ 
            echo  $data_vendor[0]['vendor'];
        } 
    }

    public function delete_data($param = null,$mr = null, $po = null){

      $this->po_mod->delete_data_po($param);

      $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($mr), '+=/', '.-~')."/".strtr($this->encryption->encrypt($po), '+=/', '.-~');
       
      redirect($links); 

    }



    function mr_autocomplete(){
        if (isset($_GET['term'])){
            $result = $this->po_mod->search_mr_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['mr_number'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "MR Not Found";
                echo json_encode($arr_result);
            }
        }
    }

    function check_mr(){
        $mr_number = $this->input->post('mr_number');
        $hasil = 0;

        $where['mr_number'] = $mr_number;
        if($this->input->post('order_type')){
            $where['service_request'] = 1;
        }
        $datadb = $this->mr_mod->mr_list($where);
        unset($where);

        if(sizeof($datadb) != 0){
            $hasil = 1;
        } 

        echo json_encode(array(
            'hasil' => $hasil
        ));
    }

    function po_mr_list(){
        $mr_number              = $this->input->post('mr_number');

        $where['mr_number']   = $mr_number;
        $mr_master              = $this->mr_mod->mr_list($where);
        $mr_master              = $mr_master[0];
        unset($where);

        $where['mr_number']     = $mr_master['mr_number'];
        $mr_detail              = $this->mr_mod->mr_list_detail($where);
        unset($where);

        foreach ($mr_detail as $key => $value) {

            //MATERIAL CATALOG ----------
            $where['code_material'] = $value['code_material'];
            $datadb = $this->material_catalog_mod->material_catalog_list($where);
            $datadb = $datadb[0];

            $mr_detail[$key]['id_material'] = $datadb['id'];
            $mr_detail[$key]['material'] = $datadb['material'];
            unset($where);
            //---------------------------

            //UOM -----------------------
            $where['id_uom'] = $value['uom_order'];
            $datadb = $this->general_mod->uom_list($where);
            $datadb = $datadb[0];

            $mr_detail[$key]['id_uom'] = $datadb['id_uom'];
            $mr_detail[$key]['uom'] = $datadb['uom'];
            unset($where);
            //---------------------------
        }

        echo json_encode($mr_detail);
    }

  //end

    public function so_new($status=null){

        $data['read_cookies']    = $this->user_cookie;
        $data['meta_title']      = 'Create New SO';
        $data['subview']         = 'po/so_new';
        $data['sidebar']         = $this->sidebar;
        $data['read_permission'] = $this->permission_cookie;

        $data['status']  = "3";
       
        $this->load->view('index', $data);
    }

    public function process_insert_so(){

        $user_id            = $this->user_cookie[0];
        $code               = $this->po_mod->po_generate_batch_no();
        $quotation          = $this->input->post('quotation');         
        $date_po            = date('Y-m-d', strtotime($this->input->post('date_po')));
        $date_quotation     = date('Y-m-d', strtotime($this->input->post('date_quotation')));
        $address            = $this->input->post('address');
        $reference          = $this->input->post('reference');
        $assign_to          = $this->input->post('assign_to');

        $form_data = array(
            'po_number'         => $code,
            'quotation'         => $quotation,
            'date_po'           => $date_po,
            'date_quotation'    => $date_quotation,
            'address'           => $address,
            'reference'         => $reference,
            'assign_to'         => $assign_to,
            'order_type'        => 2,
            'created_date_po'   => date('Y-m-d'),
            'created_by_po'     => $user_id,
            'status'            => 1
        );

        $this->po_mod->po_new_process_db($form_data);
        unset($form_data);

        // PO DETAIL -------------------------------------------------
        $mr_number          = $this->input->post('mr_number');
        $id_material        = $this->input->post('id_material');
        $mr_detail_id       = $this->input->post('mr_detail_id');
        $qty                = $this->input->post('qty');
        $id_uom             = $this->input->post('id_uom');
        $price_per_unit     = $this->input->post('price_per_unit');
        $total_price        = $this->input->post('total_price');

        $form_data = array();
        foreach ($id_material as $key => $value) {
            array_push($form_data, array(
                'po_number'         => $code,
                'mr_detail_id'      => $mr_detail_id[$key],
                'catalog_id'        => $id_material[$key],
                'qty'               => $qty[$key],
                'price_per_unit'    => $price_per_unit[$key],
            ));
        }

        $this->po_mod->po_detail_add_process($form_data);
        // -----------------------------------------------------------

        $this->session->set_flashdata('success', 'Data successful added!');

       redirect('po/po_list/waiting');
    }

    function so_mr_list(){
        $mr_number              = $this->input->post('mr_number');

        $where['mr_number']   = $mr_number;
        $mr_master              = $this->mr_mod->mr_list($where);
        $mr_master              = $mr_master[0];
        unset($where);

        $where['mr_number']     = $mr_master['mr_number'];
        $mr_detail              = $this->mr_mod->mr_list_detail($where);
        unset($where);

        foreach ($mr_detail as $key => $value) {

            //MATERIAL CATALOG ----------
            // $where['code_material'] = $value['code_material'];
            // $datadb = $this->material_catalog_mod->material_catalog_list($where);
            // $datadb = $datadb[0];

            // $mr_detail[$key]['id_material'] = $datadb['id'];
            // $mr_detail[$key]['material'] = $datadb['material'];
            // unset($where);
            //---------------------------

            //UOM -----------------------
            $where['id_uom'] = $value['uom_order'];
            $datadb = $this->general_mod->uom_list($where);
            $datadb = $datadb[0];

            $mr_detail[$key]['id_uom'] = $datadb['id_uom'];
            $mr_detail[$key]['uom'] = $datadb['uom'];
            unset($where);
            //---------------------------
        }
        // test_var($mr_detail);
        echo json_encode($mr_detail);
    }
}