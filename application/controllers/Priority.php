<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Priority extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('home_mod');
		$this->load->model('general_mod');
		$this->load->model('engineering_mod');
		$this->load->model('mrir_mod');

		//FOR MTO ===================================
		$this->load->model('priority_mod');
		//===========================================

		$this->user_cookie = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_qcs'));
		$this->sidebar = "mto/sidebar";
	}

	public function index()
    {
        $this->data['meta_title']       = 'Priority List';
        
        $where['status_delete'] = 1;
        $this->data["priority_list"]    = $this->priority_mod->getAll($where);
        unset($where);

        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;
        $this->data['subview']          = 'mto/priority/priority_list';

        $this->load->view('index', $this->data);
    }
}