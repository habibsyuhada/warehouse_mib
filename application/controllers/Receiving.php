<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Receiving extends CI_Controller {
	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('home_mod'); 
		$this->load->model('general_mod');
		$this->load->model('engineering_mod');
		$this->load->model('mrir_mod');

		//FOR MTO ===================================
		$this->load->model('mto_mod');
		$this->load->model('material_catalog_mod');
		$this->load->model('mto_category_mod');
		//===========================================

		//RECEIVING ---------------------------------
		$this->load->model('receiving_mod');
		//-------------------------------------------

		//MR ----------------------------------------
		$this->load->model('mr_mod');
		//-------------------------------------------

		//OSD ---------------------------------------
		$this->load->model('osd_mod');
		//-------------------------------------------

		//CATALOG CATEGORY --------------------------
		$this->load->model('catalog_category_mod');
		//-------------------------------------------

		//VENDOR ------------------------------------
		$this->load->model('vendor_mod');
		//-------------------------------------------

		//DEPARTMENT --------------------------------
		$this->load->model('department_mod');
		//-------------------------------------------

		//PO --------------------------------
		$this->load->model('po_mod');
		//-------------------------------------------

		$this->user_cookie = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
		$this->sidebar = "receiving/sidebar";
	}

	function index(){
		redirect('receiving_list/waiting');
	}

	function po_autocomplete(){
		if (isset($_GET['term'])){
            $result = $this->receiving_mod->search_po_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['po_number'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "PO Not Found";
                echo json_encode($arr_result);
            }
        }
	}


	//VENDOR -------------------------------------------------------------------------------
	function vendor_autocomplete(){
		if (isset($_GET['term'])){
            $result = $this->receiving_mod->search_vendor_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['vendor_name'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Vendor Not Found";
                echo json_encode($arr_result);
            }
        }
	}

	function vendor_check(){
		$vendor 				= $this->input->post('mto_number');
		$where['vendor_name'] 	= $vendor;
		$datadb 				= $this->receiving_mod->data_vendor($where);
		unset($where);

		$hasil = 0;

		if(sizeof($datadb) != 0){
			$hasil = 1;
		}

		echo json_encode(array(
			'hasil' => $hasil
		));
	}
	//------------------------------------------------------------------------------------------

	function receiving_list($param){
		$data['param'] 				= $param;
		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'Receiving '.ucfirst($param).' List';
		$data['subview']    		= 'receiving/receiving_list';
		$data['sidebar']    		= $this->sidebar;
		$data['read_permission']  	= $this->permission_cookie;

		if($this->user_cookie[10] == 0){

			$this->session->set_flashdata('message','<script type="text/javascript">swal.fire({title: "Sorry!",text: "Your ID is not registered for any project.\n\nPlease call Portal Administrator to get support for this issue!",type: "warning"}).then(function() { window.location = "'.$this->user_cookie[9].'"});</script>');
			
		}	

		$this->load->view('index', $data);
	}

	function receiving_add(){
		//MTO CATEGORY -----------------------------------------------------------
		$where['status'] = 1; 
		$data['mto_category_list']   = $this->mto_mod->mto_category_get_db($where);
		unset($where);
		//------------------------------------------------------------------------

		//PROJECT CODE -----------------------------------------------------------
		// $data['project_code']   = $this->general_mod->data_project(null);
		//------------------------------------------------------------------------

		$data['read_cookies'] 	  = $this->user_cookie;
		$data['meta_title'] 	  = 'Add New Receiving';
		$data['subview']    	  = 'receiving/receiving_new';
		$data['sidebar']    	  = $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);

	}

	function mto_number_check(){

		//MTO MASTER -------------------------------------------------------------
		$mto_number 			= $this->input->post('mto_number');
		$where['mto_number'] 	= $mto_number;
		$datadb 				= $this->mto_mod->mto_list($where);
		unset($where);

		$hasil = 0;

		if(sizeof($datadb) != 0){
			$datadb 				= $datadb[0];

			$id_mto_number 			= $datadb['id'];
			//------------------------------------------------------------------------

			//RECEIVING  -------------------------------------------------------------
			$where['mto_id'] 		= $id_mto_number;
			$data_receiving_list 	= $this->receiving_mod->receiving_data($where);
			unset($where);
			//------------------------------------------------------------------------

			if(sizeof($data_receiving_list) != 0){
				$hasil = 1;
			}
		}

		echo json_encode(array(
			'hasil' => $hasil
		));
	}

	function receiving_add_process(){

		// RECEIVING MASTER -----------------------------------------------------
		$user_id 				= $this->user_cookie[0];
		$do_pl 					= $this->input->post('do_pl');
		$ata 					= date('Y-m-d', strtotime($this->input->post('ata')));

		$form_data = array(
			'do_pl' 		=> $do_pl,
			'date_created' 	=> date('Y-m-d'),
			'ata' 			=> $ata,
			'user_created'	=> $user_id,
			'status' => 1
		);

		$id_receiving = $this->receiving_mod->receiving_new_process_db($form_data);
		// -----------------------------------------------------------------------

		// RECEIVING DETAIL ------------------------------------------------------
		$select_material		= $this->input->post('select_material');
		$user_id 				= $this->user_cookie[0];
		
		if($select_material){

			$form_receiving_detail = array();

			$shortage_categories = $this->input->post('shortage_categories');
			$project_id 		= $this->input->post('project_id');

			$po_number 			= $this->input->post('po_number');
			$po_id 				= $this->input->post('po_id');

			$country_origin 	= $this->input->post('country_origin');
			$brand 				= $this->input->post('brand');

			$qty 				= $this->input->post('qty');
			$total_qty_po 		= $this->input->post('total_qty_po');
			$total_receiving 	= $this->input->post('total_receiving');
			$color_code 		= $this->input->post('color_code');

			foreach ($select_material as $key => $material) {
				
				$qty_receiving 	= intval($qty[$key]) + intval($total_receiving[$key]);
				
				if($shortage_categories[$material] == 1){
					$str_category = 'OS&D Shortage';
					//FOR OSD DETAIL -----------------------------------------------------
					if($qty_receiving != $total_qty_po[$key]){
						if($qty_receiving > $total_qty_po[$key]){ //LEBIH BANYAK DARIPADA ORDERAN
							$total_osd = $qty_receiving - $total_qty_po[$key];
							$over 		= $total_osd;
							$shortage 	= 0;
						} else if($qty_receiving < $total_qty_po[$key]){
							$total_osd = $total_qty_po[$key] - $qty_receiving;
							$over		= 0;
							$shortage 	= $total_osd;
						}
					}
					//---------------------------------------------------------------------
				} else if($shortage_categories[$material] == 2){
					$str_category = 'PO Shortage';
					$over = 0;
					$shortage = 0;
				} else {
					$str_category 	= '';
					$over 			= intval($qty_receiving) - intval($total_qty_po[$key]);
					$shortage 		= 0;
				}

				//FOR RECEIVING DETAIL ------------------------------------------------
				array_push($form_receiving_detail, array(
					'receiving_id' 			=> $id_receiving,
					'po_number' 			=> $po_number,
					'po_id' 				=> $po_id[$key],
					'country_origin' 		=> $country_origin[$key],
					'brand' 				=> $brand[$key],
					'qty' 					=> $qty[$key],
					'color_code' 			=> $color_code[$key],
					'shortage_categories'	=> $str_category,
					'qty_over'				=> $over,
					'qty_shortage'			=> $shortage,
					'status' 				=> 1
				));
				//---------------------------------------------------------------------
			}

			$this->receiving_mod->receiving_detail_import_process_db($form_receiving_detail);
		}
		// -----------------------------------------------------------------------

		$this->session->set_flashdata('success', 'Your data has been Created!');

		redirect('receiving_list/waiting');
	}

	function update_receiving(){
		$master_receiving_id 	= $this->input->post('id_receiving_master');
		$do_pl 					= $this->input->post('do_pl');
		$date_received 			= date('Y-m-d', strtotime($this->input->post('date_received')));
		$ata 					= date('Y-m-d', strtotime($this->input->post('ata')));
		$atd 					= date('Y-m-d', strtotime($this->input->post('atd')));

		$form_data = array(
			'do_pl' 		=> $do_pl,
			'date_created' 	=> $date_received,
			'ata' 			=> $ata,
			'atd' 			=> $atd
		);

		$where['id'] = $master_receiving_id;
		$this->receiving_mod->receiving_edit_process_db($form_data, $where);
		unset($where);

		$this->session->set_flashdata('success', 'Data successful updated!');
        redirect("receiving_detail/".strtr($this->encryption->encrypt($master_receiving_id), '+=/', '.-~'));

	}


	//RECEIVING DETAIL ------------------------------------------------------------------------------------------
	function receiving_detail($id = null){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ 
			redirect('receiving');
		}

		$data['master_receiving_id'] = $id;

		if($this->input->get('t')){
			$data['t'] 	= $this->input->get('t');
		}else{
			$data['t'] 	= '';
		}

		//RECEIVING MASTER -------------------------------------------
		$where['id'] = $id;
		$receiving_list = $this->receiving_mod->receiving_data($where);
		$receiving_list = $receiving_list[0];
		unset($where);
		//------------------------------------------------------------

		$where['id_user'] 	= $receiving_list['user_created'];
		$data_user			= $this->general_mod->get_all_user($where);
		unset($where);
		$data_user			= $data_user[0];

		$receiving_list['user_name'] = $data_user['full_name'];

		$title = 'Receiving '.$receiving_list['do_pl'];
		$data['receiving_list'] = $receiving_list;

		//RECEIVING DETAIL -------------------------------------------
		$where['rd.receiving_id'] = $receiving_list['id'];
		$receiving_detail_list = $this->receiving_mod->receiving_detail_join($where);
		unset($where);
		//------------------------------------------------------------

		//RECEIVING ATTACHMENT ---------------------------------------
		$where['receiving_id'] 	= $id;
		$receiving_attachment 	= $this->receiving_mod->receiving_attachment($where);
		unset($where);
		//------------------------------------------------------------

		$data['receiving_detail_list'] 		= $receiving_detail_list;
		$data['receiving_attachment_list'] 	= $receiving_attachment;

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= $title;
		$data['subview']    		= 'receiving/receiving_detail';
		$data['sidebar']    		= $this->sidebar;
		
		$this->load->view('index', $data);
	}

	//RECEIVING DETAIL PREVIEW ---------------
	function receiving_detail_preview(){

		$master_receiving_id = $this->input->post('master_receiving_id');

		$id_user = $this->user_cookie;
		$date = date('Y-m-d H:i:s');

		$config['upload_path']          = 'file/receiving/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 			= TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("receiving_detail/".strtr($this->encryption->encrypt($master_receiving_id), '+=/', '.-~'));
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/receiving/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data_check = array();
		
		$numrow = 1;

		//DECLARE ARRAY --------------
		$arr_material_catalog = [];
		$arr_unique_no = [];
		$arr_area = [];
		//---------------------------

		foreach($sheet as $row){
			
			if($numrow > 1){
				$data_check[] = $row['A'];
			}

			//MATERIAL CATALOG ===========================================================
			$sheet[$numrow]['id_material_catalog'] = '';
			$sheet[$numrow]['material_catalog'] = '';

			$where['short_desc'] = $row['A'];
			$datadb				 = $this->material_catalog_mod->getAll($where);
			unset($where);
			if($datadb){
				$datadb			 = $datadb[0];

				$arr_material_catalog[] 			= $datadb['short_desc']; //UNTUK DICEK MATERIAL CATALOGNYA

				$sheet[$numrow]['id_material_catalog']	= $datadb['id'];
				$sheet[$numrow]['material_catalog'] = $datadb['material'];
			}
			// ===========================================================================

			//UNIQUE NUMBER ==============================================================
			$where['uniq_no'] 	= $row['B'];
			$where['status'] 	= 1;
			$datadb 			= $this->receiving_mod->receiving_detail_data($where);
			if($datadb){
				$datadb 		= $datadb[0];
				$arr_unique_no[] 	= $datadb['uniq_no'];
			}
			unset($where);
			//============================================================================

			//AREA =======================================================================
			$sheet[$numrow]['id_area'] = '';
			$sheet[$numrow]['name_area'] = '';

			$where['area_name'] = $row['K'];
			$datadb 			= $this->general_mod->data_area($where);
			if($datadb){
				$datadb 		= $datadb[0];
				$arr_area[] 	= $datadb['area_name'];

				$sheet[$numrow]['id_area'] = $datadb['id'];
				$sheet[$numrow]['name_area'] = $datadb['area_name'];
			}
			unset($where);
			//============================================================================

			//SPEC CATEGORY ==============================================================
			$sheet[$numrow]['id_spec_category'] = '';
			$sheet[$numrow]['name_spec_category'] = '';

			$where['spec_code'] 	= $row['O'];
			$datadb 				= $this->general_mod->data_spec_category($where);
			if($datadb){
				$datadb 			= $datadb[0];
				$arr_spec_category[]= $datadb['spec_code'];

				$sheet[$numrow]['id_spec_category'] = $datadb['id'];
				$sheet[$numrow]['name_spec_category'] = $datadb['spec_desc'];
			}
			unset($where);
			//============================================================================

			$numrow++; // Tambah 1 setiap kali looping
		}

		$data['master_receiving_id'] 		= $master_receiving_id;
		$data['sheet']						= $sheet;

		$data['material_catalog_list'] 		= $arr_material_catalog;
		$data['unique_no_list'] 			= $arr_unique_no;
		$data['area_list'] 					= $arr_area;
		$data['spec_category_list'] 		= $arr_spec_category;

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'Receiving Detail Preview';
		$data['subview']    	= 'receiving/receiving_preview';
		$data['sidebar']    	= $this->sidebar;
		$this->load->view('index', $data);
	}
	//----------------------------------------

    //RECEIVING DETAIL ADD PROCESS -----------
	function receiving_detail_add_process(){

		$select_material		= $this->input->post('select_material');
		$user_id 				= $this->user_cookie[0];
		
		if($select_material){

			$form_receiving_detail = array();

			$shortage_categories = $this->input->post('shortage_categories');
			$id_receiving 		= $this->input->post('master_receiving_id');
			$project_id 		= $this->input->post('project_id');

			$po_number 			= $this->input->post('po_number');
			$po_id 				= $this->input->post('po_id');

			$country_origin 	= $this->input->post('country_origin');
			$brand 				= $this->input->post('brand');

			$qty 				= $this->input->post('qty');
			$total_qty_po 		= $this->input->post('total_qty_po');
			$total_receiving 	= $this->input->post('total_receiving');
			$color_code 		= $this->input->post('color_code');

			foreach ($select_material as $key => $material) {
				
				$qty_receiving 	= intval($qty[$key]) + intval($total_receiving[$key]);
				
				if($shortage_categories[$material] == 1){
					$str_category = 'OS&D Shortage';
					//FOR OSD DETAIL -----------------------------------------------------
					if($qty_receiving != $total_qty_po[$key]){
						if($qty_receiving > $total_qty_po[$key]){ //LEBIH BANYAK DARIPADA ORDERAN
							$total_osd = $qty_receiving - $total_qty_po[$key];
							$over 		= $total_osd;
							$shortage 	= 0;
						} else if($qty_receiving < $total_qty_po[$key]){
							$total_osd = $total_qty_po[$key] - $qty_receiving;
							$over		= 0;
							$shortage 	= $total_osd;
						}
					}
					//---------------------------------------------------------------------
				} else if($shortage_categories[$material] == 2){
					$str_category = 'PO Shortage';
					$over = 0;
					$shortage = 0;
				} else {
					$str_category 	= '';
					$over 			= intval($qty_receiving) - intval($total_qty_po[$key]);
					$shortage 		= 0;
				}

				//FOR RECEIVING DETAIL ------------------------------------------------
				array_push($form_receiving_detail, array(
					'receiving_id' 			=> $id_receiving,
					'po_number' 			=> $po_number,
					'po_id' 				=> $po_id[$key],
					'country_origin' 		=> $country_origin[$key],
					'brand' 				=> $brand[$key],
					'qty' 					=> $qty[$key],
					'color_code' 			=> $color_code[$key],
					'shortage_categories'	=> $str_category,
					'qty_over'				=> $over,
					'qty_shortage'			=> $shortage,
					'status' 				=> 1
				));
				//---------------------------------------------------------------------
			}

			$this->receiving_mod->receiving_detail_import_process_db($form_receiving_detail);

			$this->session->set_flashdata('success', 'Data successful added!');

			redirect("receiving_detail/".strtr($this->encryption->encrypt($id_receiving), '+=/', '.-~'));
		}

	}
	//----------------------------------------

	function receiving_detail_list(){
		//MTO DETAIL ------------------------------------------------
		$where['id'] = $this->input->post('master_receiving_id');
		$where['status'] = 1;
		$data = $this->receiving_mod->receiving_detail_data($where);
		unset($where);

		//-----------------------------------------------------------

		for($i=0;$i<sizeof($data);$i++){

			//MATERIAL CATALOG ----------------------------------------------------
			$where['id'] = $data[$i]['catalog_id'];
			$datadb = $this->material_catalog_mod->getAll($where);
			unset($where);

			$data[$i]['name_catalog'] = $datadb[0]['material'];
			//---------------------------------------------------------------------

			//AREA ----------------------------------------------------------------
			$where['id'] = $data[$i]['area'];
			$datadb = $this->general_mod->data_area($where);
			unset($where);

			$data[$i]['name_area'] = $datadb[0]['area_name'];
			//---------------------------------------------------------------------
		}

		echo json_encode($data);
	}

	function receiving_check_do_pl(){
		$receiving_id = $this->input->post('receiving_id');
		$do_pl = $this->input->post('do_pl');
		$hasil = 0;

		$where['do_pl'] = $do_pl;

		if($receiving_id != ''){
			$where['receiving_id != '] = $receiving_id;
		}

		$datadb = $this->receiving_mod->receiving_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo json_encode(array(
			'hasil' => $hasil
		));
	}

	function receiving_check_po(){
		$po_number = $this->input->post('po');
		$hasil = 0;

		$where['po_number'] = $po_number;
		$datadb = $this->receiving_mod->po_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo json_encode(array(
			'hasil' => $hasil
		));
	}

	function receiving_check_unique_no(){
		$unique_no = $this->input->post('unique_no');
		$hasil = 0;

		$where['uniq_no'] = $unique_no;
		$where['status'] = 1;
		$datadb = $this->receiving_mod->receiving_detail_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo $hasil;
	}	

	function receiving_detail_check_mto_catalog(){
		$receiving_id = $this->input->post('receiving_id');
		$mto_id = $this->input->post('mto_id');
		$hasil = 0;

		$where['catalog_id'] 	= $mto_id;
		$where['receiving_id'] 	= $receiving_id;
		$where['status'] 		= 1;
		$datadb = $this->receiving_mod->receiving_detail_data($where);
		unset($where);

		if(sizeof($datadb) != 0){
			$hasil = 1;
		} 

		echo $hasil;
	}

	function receiving_detail_edit_process(){		
		$col 		= $this->input->post('col');
		$id 		= $this->input->post('id');
		$value 		= $this->input->post('value');

		$form_data = array(
			$col => $value,
			'update_date' => date('Y-m-d H:i:s'),
			'update_by' => $this->user_cookie[0]
		);
		$where['rec_det_id'] = $id;
		$this->receiving_mod->receiving_detail_edit_process_db($form_data, $where);
	}

	function receiving_detail_delete_process(){		

		$id = $this->input->post('id');

		$form_data = array(
			'status' => 0,
			'delete_date' => date('Y-m-d H:i:s'),
			'delete_by' => $this->user_cookie[0]
		);

		$where['rec_det_id'] = $id;
		$this->receiving_mod->receiving_detail_edit_process_db($form_data, $where);
		unset($where);
	}

	//RECEIVING ATTACHMENT ------------------------------------
	function receiving_attachment_list(){
		$receiving_id = $this->input->post('receiving_id');
		$where['receiving_id'] 	= $receiving_id;
		$datadb 				= $this->receiving_mod->receiving_attachment($where);
		unset($where);
		echo json_encode($datadb);
	}

	function receiving_attachment_upload(){
		$id_user			= $this->user_cookie;
		$receiving_id 		= $this->input->post('id_receiving_master');

		if (!file_exists('file/receiving/'.$receiving_id)) {
		    mkdir('file/receiving/'.$receiving_id, 0777, true);
		}

		if($_FILES['file']['name']){

			$config['upload_path']          = 'file/receiving/'.$receiving_id;
	        $config['file_name']            = $filename;
	        $config['allowed_types']        = 'pdf';
	        $config['overwrite']            = TRUE;

	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);

	        if ($this->upload->do_upload('file')){

	        	$form_data = array(
					'receiving_id' 	=> $receiving_id,
					'attachment' 	=> $_FILES['file']['name']
				);

				$this->receiving_mod->receiving_attachment_add_process($form_data);
				$uploadData = $this->upload->data();
				echo 'good';
	        } else {
	        	echo 'bad';
	        }
		} 
	}


	function receiving_attachment_delete_process($receiving_id){		

		$id = $this->input->post('id');
		$where['id'] = $id;
		$receiving_attachment_list = $this->receiving_mod->receiving_attachment($where);
		unset($where);
		$receiving_attachment_list = $receiving_attachment_list[0];

		$form_data = array(
			'id' 	=> $id,
		);

		$this->receiving_mod->receiving_attachment_delete_process($form_data);

		$file = 'file/receiving/'.$receiving_id.'/'.$receiving_attachment_list['attachment'];

		unlink($file);

	}

	function catalog_list_by_po(){
		$po_number	= $this->input->post('po');

		//PO ------------------------------------------------
		$where['po.po_number'] 			= $po_number;
		$where['po.status_approval'] 	= 3;
		$data_po 						= $this->receiving_mod->po_data($where);
		unset($where);
		//-----------------------------------------------------------

		for($i=0; $i<sizeof($data_po); $i++){

			$data_po[$i]['qty_request']		= $data_po[$i]['po_qty'];

			//MR DETAIL -------------------------------------
			$where['id'] 					= $data_po[$i]['mr_detail_id'];
			$datadb 						= $this->mr_mod->mr_list_detail($where);
			$datadb 						= $datadb[0];
			unset($where);
			$vendor_id 						= $datadb['vendor_winner'];
			//-----------------------------------------------

			//VENDOR ----------------------------------------
			$where['id_vendor'] 			= $vendor_id;
			$datadb 						= $this->vendor_mod->getAll($where);
			$datadb 						= $datadb[0];
			unset($where);
			$data_po[$i]['vendor_id'] 		= $datadb['id_vendor'];
			$data_po[$i]['vendor_name'] 	= $datadb['name'];
			//-----------------------------------------------

			//QTY RECEIVING ---------------------------------
			$where['po_number']		= $data_po[$i]['po_number'];
			$where['po_id']			= $data_po[$i]['id_po'];
			$datadb 				= $this->receiving_mod->count_total_qty_receiving($where);
			unset($where);
			$datadb 				= $datadb[0];
			$data_po[$i]['qty_receiving'] = $datadb['qty_receiving'];
			//-----------------------------------------------

			if($datadb['qty_receiving'] == 0){
				$status = '-';
			} else if($data_po[$i]['qty_request'] == $datadb['qty_receiving']){	
				$status = '<span class="badge badge-success">PO Complete</span>';
			} else if($data_po[$i]['qty_request'] > $datadb['qty_receiving']){
				$status = '<span class="badge badge-warning">PO Shortage</span>';
			} else if($data_po[$i]['qty_request'] < $datadb['qty_receiving']){
				$status = '<span class="badge badge-danger">PO Over</span>';
			}

			$data_po[$i]['status_receiving'] = $status;
			
		}

		echo json_encode($data_po);
	}

	//RECEIVING LIST DATATABLE ----------------------------------------------------------------------------------
	function receiving_list_json($param){
    	error_reporting(0);

    	$role_user	= $this->user_cookie[7];
 		
 		if($param == 'waiting'){
 			$where['status'] = 1;
 		} else if($param == 'complete'){
 			$where['status'] = 3;
 		}

 		$list = $this->receiving_mod->get_datatables_receiving_list_dt($where);
 		unset($where);
        
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {
			$links = base_url()."receiving_detail/".strtr($this->encryption->encrypt($list->id), '+=/', '.-~');
			
            $no++;
            $row   = array();
            
            $row[] = $list->do_pl;
            $row[] = $list->ata;

            // TOTAL MATERIAL ----------------------------
            $where['receiving_id'] = $list->receiving_id;
            $datadb = $this->receiving_mod->receiving_detail_data($where);
            unset($where);
            // -------------------------------------------

            $row[] = sizeof($datadb);

            $row[] = $list->full_name;
            $row[] = $list->date_created;

            // if(sizeof($datadb) > 0 ){
            	if($list->status < 3){
	        		$button = '<a href="'.base_url().'receiving/receiving_approval/'.strtr($this->encryption->encrypt($list->id), '+=/', '.-~').'" class="btn btn-success"><i class="fas fa-check"></i> Approval</a>';
            	} else {
            		$button = '<a href="'.base_url().'receiving/receiving_approval/'.strtr($this->encryption->encrypt($list->id), '+=/', '.-~').'" class="btn btn-secondary"><i class="fas fa-file-alt"></i> Detail</a>';
            	}
        	// } else {
        	// 	$button = '<button class="btn btn-success" disabled><i class="fas fa-check"></i> Approval</button>';
        	// }

        	// $row[] = "<a href='".$links."' class='btn btn-secondary text-white' title='Detail'><i class='fas fa-file-alt'></i> Detail </a>&nbsp; ".$button;
        	$row[] = $button;
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->receiving_mod->count_all_receiving_list_dt($where),
            "recordsFiltered" => $this->receiving_mod->count_filtered_receiving_list_dt($where),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
        unset($where);
    }

    //-----------------------------------------------------------------------------------------------------------

    //RECEIVING DETAIL LIST DATATABLE ----------------------------------------------------------------------------------
	function receiving_detail_list_json($receiving_id){
    	error_reporting(0);
 
        $list = $this->receiving_mod->get_datatables_receiving_detail_list_dt($receiving_id);
        
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {			
            $no++;
            $row   = array();

            //PO NUMBER --------
            $where['po_number'] 	= $list->po_number;
            $where['id_po'] 		= $list->po_id;
            $datadb = $this->po_mod->po_list($where);
            $datadb = $datadb[0];

            $qty_request 	= $datadb['po_qty'];
            $mr_detail_id 	= $datadb['mr_detail_id'];
            
            $row[] = $datadb['po_number'];
            unset($where); 
            //-----------------

            // MR DETAIL ------
            $where['id'] 	= $mr_detail_id;
            $datadb 		= $this->mr_mod->mr_list_detail($where);
            $datadb 		= $datadb[0];
            $uom 			= $datadb['uom_order'];
            $material 		= $datadb['tec_spec'];
            unset($where);
            //-----------------

            $row[] = $material;
            $row[] = $qty_request;
            $row[] = $list->qty_receiving;

            //UOM -------------
            $where['id_uom'] = $uom;
            $datadb = $this->general_mod->uom_list($where);
            $datadb = $datadb[0];
            $row[] 	= $datadb['uom'];
            unset($where);
            //-----------------

            $row[] = $list->country_origin;
            $row[] = $list->brand;
            $row[] = $list->color_code;

            if($qty_request != $list->qty_receiving){
				if($qty_request > $list->qty_receiving){
					$remarks = '<span class="badge badge-warning">PO Shortage</span>';
				} else if($qty_request < $list->qty_receiving){
					$remarks = '<span class="badge badge-danger">PO Over</span>';
				}
            } else {
              	$remarks = '<span class="badge badge-success">Complete</span>';
            }

            $row[] = $remarks;
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->receiving_mod->count_all_receiving_detail_list_dt($receiving_id),
            "recordsFiltered" => $this->receiving_mod->count_filtered_receiving_detail_list_dt($receiving_id),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
    //-----------------------------------------------------------------------------------------------------------


    function receiving_detail_import_process(){
    	$id_receiving 		= $this->input->post('master_receiving_id');

    	$material 			= $this->input->post('material');
    	$project 			= $this->input->post('project');
    	$discipline 		= $this->input->post('discipline');
    	$country_origin 	= $this->input->post('country_origin');
    	$uom_length 		= $this->input->post('uom_length');
    	$brand 				= $this->input->post('brand');
    	$vendor 			= $this->input->post('vendor');
    	$delivery_condition = $this->input->post('delivery_condition');
    	$spec_grade 		= $this->input->post('spec_grade');
    	$spec_category 		= $this->input->post('spec_category');
    	$ceq 				= $this->input->post('ceq');
    	$qty 				= $this->input->post('qty');
    	$qty_shortage 		= $this->input->post('qty_shortage');
    	$shortage_category 	= $this->input->post('shortage_category');
    	$qty_over 			= $this->input->post('qty_over');
    	$mill_cert_no 		= $this->input->post('mill_cert_no');
    	$date_manufacturing = $this->input->post('date_manufacturing');
    	$color_code 		= $this->input->post('color_code');

    	$form_data = array();
    	foreach($material as $key => $row){
    		array_push($form_data, array(
				'receiving_id' 			=> $id_receiving,
				'catalog_id' 			=> $material[$key],
				'project_id' 			=> $project[$key],
				'discipline' 			=> $discipline[$key],
				'country_origin' 		=> $country_origin[$key],
				'uom_length' 			=> $uom_length[$key],
				'brand' 				=> $brand[$key],
				'vendor_id' 			=> $vendor[$key],
				'delivery_condition' 	=> $delivery_condition[$key],
				'spec_grade' 			=> $spec_grade[$key],
				'spec_category' 		=> $spec_category[$key],
				'ceq' 					=> $ceq[$key],
				'qty' 					=> $qty[$key],
				'qty_shortage' 			=> $qty_shortage[$key],
				'shortage_category' 	=> $shortage_category[$key],
				'qty_over' 				=> $qty_over[$key],
				'mill_cert_no' 			=> $mill_cert_no[$key],
				'date_manufacturing' 	=> $date_manufacturing[$key],
				'color_code' 			=> $color_code[$key],
				'status' 				=> 1
			));
    	}

    	$this->receiving_mod->receiving_cs_detail_new_process_db($form_data);
		$this->session->set_flashdata('success', 'Your data has been Created!');

		redirect('receiving_cs_detail/'.strtr($this->encryption->encrypt($id_receiving), '+=/', '.-~'));
    }


    function receiving_approval($id){

    	$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ 
			redirect('receiving');
		}

    	$where['id'] 				= $id;
    	$data['receiving_master'] 	= $this->receiving_mod->receiving_data($where);
    	unset($where);

    	$where['receiving_id'] 		= $id;
    	$receiving_detail_list 		= $this->receiving_mod->receiving_detail_data($where);
    	unset($where);

    	foreach ($receiving_detail_list as $key => $value) {

    		// PO -------------------------
    		$where['id_po']			= $value['po_id'];
    		$datadb 				= $this->receiving_mod->po_data($where);
    		$datadb 				= $datadb[0];
    		$receiving_detail_list[$key]['qty_request'] = $datadb['po_qty'];
    		$qty_request 			= $datadb['po_qty'];
    		$mr_detail_id 			= $datadb['mr_detail_id'];
    		unset($where); 
    		// ----------------------------

    		//MR DETAIL -------------------
    		$where['id'] 			= $mr_detail_id;
    		$datadb 				= $this->mr_mod->mr_list_detail($where);
    		unset($where);
    		$datadb 				= $datadb[0];
    		$receiving_detail_list[$key]['material'] = $datadb['tec_spec'];
    		$uom 					= $datadb['uom_order'];
    		// ----------------------------

    		// UOM ------------------------
    		$where['id_uom']		= $uom;
    		$datadb					= $this->general_mod->uom_list($where);
    		$datadb					= $datadb[0];
    		$receiving_detail_list[$key]['uom_name'] = $datadb['uom'];
    		unset($where);
    		// ----------------------------

    		if($qty_request != $value['qty']){
				if($qty_request > $value['qty']){
					$remarks = '<span class="badge badge-warning">PO Shortage</span>';
				} else if($qty_request < $value['qty']){
					$remarks = '<span class="badge badge-danger">PO Over</span>';
				}
            } else {
              	$remarks = '<span class="badge badge-success">Complete</span>';
            }

            $receiving_detail_list[$key]['status_material'] = $remarks;
    	}

    	$data['receiving_detail_list'] = $receiving_detail_list;

		$data['read_permission'] 	= $this->permission_cookie;
		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'Receiving Approval';
		$data['subview']    		= 'receiving/receiving_approval';
		$data['sidebar']    		= $this->sidebar;
		$this->load->view('index', $data);
    }

    public function receiving_approval_process(){
    	$user_id 			= $this->user_cookie[0];
    	$receiving_id 		= $this->input->post('receiving_id');
    	$access 			= $this->input->post('access');

    	$current_date 		= date('Y-m-d H:i:s');

    	if($access == 'user'){
    		$form_data = array(
				'user_approved' 		=> $user_id,
				'user_approved_date' 	=> $current_date,
			);
    	} else if($access == 'warehouse'){
    		$form_data = array(
				'warehouse_approved' 		=> $user_id,
				'warehouse_approved_date' 	=> $current_date,
			);
    	} else if($access == 'procurement'){
    		$form_data = array(
				'procurement_approved' 		=> $user_id,
				'warehouse_approved_date' 	=> $current_date,
			);
    	}

    	$where['id'] = $receiving_id;
    	$this->receiving_mod->receiving_edit_process_db($form_data, $where);
    	$datadb 	= $this->receiving_mod->receiving_data($where);
    	$datadb 	= $datadb[0];
    	unset($where);

		if($datadb['user_approved'] == 0 || $datadb['warehouse_approved'] == 0 || $datadb['warehouse_approved'] == 0){
			$this->session->set_flashdata('success', 'Data successful approved!');
			redirect("receiving_list/complete");
		} else {
			$form_data = array(
				'status' => 3
			);

			$where['id'] = $receiving_id;
			$this->receiving_mod->receiving_edit_process_db($form_data, $where);
			unset($where);

			$this->session->set_flashdata('success', 'Data successful approved!');
			redirect("receiving_list/complete");
		}
		
	}

	function receiving_submitted_process($id){

    	$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
		if(empty($id)){ 
			redirect('receiving');
		}

		$form_data = array(
			'status' => 1
		);

		$where['id'] = $id;
		$this->receiving_mod->receiving_edit_process_db($form_data, $where);

		$this->session->set_flashdata('success', 'Data successful submitted!');
		redirect("receiving_list/submitted");
    }


}