<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('user_mod');
		$this->load->model('general_mod');

		$this->user_cookie 		 = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));

		$this->sidebar = "user/sidebar";
	}

	public function index(){
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Master Data - Carefull with your updated!';
		$data['subview']					= 'home/home_blank';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function userList(){
		$data['all_list']	   			= $this->user_mod->all_list();

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'User List';
		$data['subview']					= 'user/user_list';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function userDetailNew($filter = NULL){
		$filter = explode("-", $filter);

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->general_mod->role_list();
		foreach ($datadb as $value) {
			$data['role_list'][$value['id']] = $value;
		}

		$data['filter']	   				= $filter;
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'New User';
		$data['subview']					= 'user/user_new';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function userDetailNewProcess(){
		if($this->permission_cookie[89] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$form_data = array(
			'full_name' => $data_post['full_name'],
			'badge_no' 			=> $data_post['badge_no'],
			'username' 		=> $data_post['username'],
			'password' 		=> md5('12345'),
			'department' 		=> $data_post['department'],
			'email' 		=> $data_post['email'],
			'id_role' 		=> $data_post['id_role'],
			'update_by' 	=> $this->user_cookie[0],
			'last_update' 	=> date('Y-m-d H:i:s'),
		);
		$id = $this->user_mod->user_detail_new_process($form_data);

		$this->session->set_flashdata('success', 'Your data has been inserted!');
		redirect('user/userList/'.$data_post['department']);
	}

	public function userDetailEdit($id){
		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
		$datadb	  					= $this->user_mod->all_list($id);
		$user	  	= $datadb[0];
		$data['user']	  	= $datadb[0];
		$permission_wh = explode(';', $user['warehouse_permission']);

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$datadb = $this->general_mod->role_list();
		foreach ($datadb as $value) {
			$data['role_list'][$value['id']] = $value;
		}

		$data['permission_wh']	   = $permission_wh;
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Edit User';
		$data['subview']					= 'user/user_edit';
		$data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function userDetailEditProcess(){
		if($this->permission_cookie[90] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$where['id_user'] = $data_post['id_user'];
		$form_data = array(
			'full_name' => $data_post['full_name'],
			'badge_no' 			=> $data_post['badge_no'],
			'username' 		=> $data_post['username'],
			'department' 		=> $data_post['department'],
			'email' 		=> $data_post['email'],
			'id_role' 		=> $data_post['id_role'],
			'status_user' 		=> $data_post['status_user'],
		);
		$id = $this->user_mod->user_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been updated!');
		redirect('user/userDetailEdit/'.strtr($this->encryption->encrypt($data_post['id_user']), '+=/', '.-~'));
	}

	public function userDetailDeleteProcess($id_user){
		if($this->permission_cookie[91] == 0){
			redirect('auth/logout');
		}
		$id_user = $this->encryption->decrypt(strtr($id_user, '.-~', '+=/'));

		$datadb	  	= $this->user_mod->all_list($id_user);
		$user	  	= $datadb[0];

		$where['id_user'] = $id_user;
		$form_data = array(
			'status_user' => 0,
		);
		$id = $this->user_mod->user_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been deleted!');
		redirect('user/userList/'.$user['department']);
	}

	public function userUpdatePermissionProcess(){
		if($this->permission_cookie[101] == 0){
			redirect('auth/logout');
		}
		$data_post 			= $this->input->post();

		$permission = '0';
		for ($i=1; $i <= 101; $i++) { 
			$permission .= ';'.(isset($data_post[$i]) ? $data_post[$i] : '0');
		}

		$where['id_user'] = $data_post['id_user'];
		$form_data = array(
			'warehouse_permission' => $permission,
		);
		$id = $this->user_mod->user_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been updated!');
		redirect('user/userDetailEdit/'.strtr($this->encryption->encrypt($data_post['id_user']), '+=/', '.-~'));
	}

	public function profile(){
		$id = $this->user_cookie[0];
		$datadb	  					= $this->user_mod->all_list($id);
		$user	  	= $datadb[0];
		$data['user']	  	= $datadb[0];
		$permission_wh = explode(';', $user['warehouse_permission']);

		$datadb = $this->general_mod->dept_list();
		foreach ($datadb as $value) {
			$data['dept_list'][$value['id_department']] = $value;
		}

		$data['permission_wh']	   = $permission_wh;
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'My Profile';
		$data['subview']					= 'user/user_profile';
		// $data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function user_signature(){
		$data['read_cookies']	   	= $this->user_cookie;
		$data['meta_title']		 		= 'Edit My Digital Signature';
		$data['subview']					= 'user/user_signature';
		// $data['sidebar']					= $this->sidebar;
		$data['read_permission']	= $this->permission_cookie;
		$this->load->view('index', $data);
	}

	public function userSignatureEditProcess(){
		$data_post 			= $this->input->post();

		$where['id_user'] = $this->user_cookie[0];
		$form_data = array(
			'sign_approval' => $data_post['hdnSignature']
		);
		$id = $this->user_mod->user_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your Digital Signature has been updated!');
		redirect('user/profile');
	}

	public function userPasswordEditProcess(){
		$data_post 			= $this->input->post();
		$id 						= $this->user_cookie[0];
		$datadb	  			= $this->user_mod->all_list($id);
		$user	  				= $datadb[0];

		if($user['password'] != md5($data_post['old_password'])){
			$this->session->set_flashdata('error', 'Your Old Password is Wrong!');
			redirect("user/profile");
			return false;
		}

		if(md5($data_post['new_password']) != md5($data_post['confirm_password'])){
			$this->session->set_flashdata('error', 'New Passwords dont match!');
			redirect("user/profile");
			return false;
		}

		$where['id_user'] = $this->user_cookie[0];
		$form_data = array(
			'password' => md5($data_post['new_password'])
		);
		$id = $this->user_mod->user_detail_edit_process($form_data, $where);

		$this->session->set_flashdata('success', 'Your Password has been updated!');
		redirect('user/profile');
	}


}