<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller {
	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('home_mod'); 
		$this->load->model('general_mod');
		$this->load->model('engineering_mod');
		$this->load->model('mrir_mod');

		$this->load->model('vendor_mod');

		$this->user_cookie = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
		$this->sidebar = "user/sidebar";
	}

	function index(){
		redirect('vendor_list');
	}

	function vendor_check($param){

		if($param == 'vendor_number'){
			$vendor_number = $this->input->post('vendor_number');

			//CHECK VENDOR NUMBER -----------------------------
			$where['vendor_no'] = $vendor_number;
			$datadb = $this->vendor_mod->getAll($where);
			unset($where);

			$hasil = 0;
			if(sizeof($datadb) != 0){
				$hasil = 1;
			}
			//-------------------------------------------------

			echo json_encode(array(
				'hasil' => $hasil
			));
		}
	}

	function vendor_list(){
		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'Vendor List';
		$data['subview']    		= 'vendor/vendor_list';
		$data['sidebar']    		= $this->sidebar;
		$data['read_permission']  	= $this->permission_cookie;

		if($this->user_cookie[10] == 0){
			$this->session->set_flashdata('message','<script type="text/javascript">swal.fire({title: "Sorry!",text: "Your ID is not registered for any project.\n\nPlease call Portal Administrator to get support for this issue!",type: "warning"}).then(function() { window.location = "'.$this->user_cookie[9].'"});</script>');
		}	

		$this->load->view('index', $data);
	}

	function vendor_add(){
		$data['read_cookies'] 	  = $this->user_cookie;
		$data['meta_title'] 	  = 'Add New Vendor';
		$data['subview']    	  = 'vendor/vendor_new';
		$data['sidebar']    	  = $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;

		$where['status'] = 1;
		$data['verification_list'] = $this->vendor_mod->vendor_verification_list($where);
		unset($where);
		
		$this->load->view('index', $data);
	}

	function vendor_add_process(){
		$user_id 		= $this->user_cookie[0];
		$name 			= $this->input->post('name');
		$remarks 		= $this->input->post('remarks');
		$address 		= $this->input->post('address');
		// $pic 			= $this->input->post('pic');
		$email 			= $this->input->post('email');
		$phone_number 	= $this->input->post('phone_number');
		$fax 			= $this->input->post('fax');
		$contact_name 	= $this->input->post('contact_name');
		$contact_phone 	= $this->input->post('contact_phone');
		
		$verification 		= $this->input->post('verification');
		$id_verification 	= $this->input->post('id_verification');

		$arr_verification = array();
		foreach ($id_verification as $key => $value) {
			if(in_array($value, $verification)){
				array_push($arr_verification, $value);
			} else {
				array_push($arr_verification, 0);
			}
		}
		$str_verification = implode(";", $arr_verification);

		$form_data = array(
			'name' 			=> $name,
			'remarks' 		=> $remarks,
			'address' 		=> $address,
			'email' 		=> $email,
			'phone_no' 		=> $phone_number,
			'fax' 			=> $fax,
			'contact_name' 	=> $contact_name,
			'contact_number'=> $contact_phone,
			'verification' 	=> $str_verification,
			'created_by' 	=> $user_id,
			'created_date' 	=> date('Y-m-d H:i:s'),
			'status' 		=> 1
		);

		$this->vendor_mod->vendor_add($form_data);
		$this->session->set_flashdata('success', 'Your data has been Created!');
		redirect('vendor_list');
	}

	function vendor_edit($id = null){

		$id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

		if(empty($id)){ redirect('vendor_list'); }

		$where['id_vendor'] 	= $id;
		$data['vendor_list'] 	= $this->vendor_mod->getAll($where);
		unset($where);

		$where['status'] = 1;
		$data['verification_list'] = $this->vendor_mod->vendor_verification_list($where);
		unset($where);

		$data['read_cookies'] 	  = $this->user_cookie;
		$data['meta_title'] 	  = 'Edit Vendor';
		$data['subview']    	  = 'vendor/vendor_detail';
		$data['sidebar']    	  = $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		
		$this->load->view('index', $data);
	}

	function vendor_edit_process(){
		$id 			= $this->input->post('id_vendor');
		$name 			= $this->input->post('name');
		$remarks 		= $this->input->post('remarks');
		$address 		= $this->input->post('address');
		// $pic 			= $this->input->post('pic');
		$email 			= $this->input->post('email');
		$phone_number 	= $this->input->post('phone_number');
		$fax 			= $this->input->post('fax');
		$contact_name 	= $this->input->post('contact_name');
		$contact_phone 	= $this->input->post('contact_phone');
		
		$verification 		= $this->input->post('verification');
		$id_verification 	= $this->input->post('id_verification');

		$arr_verification = array();
		foreach ($id_verification as $key => $value) {
			if(in_array($value, $verification)){
				array_push($arr_verification, $value);
			} else {
				array_push($arr_verification, 0);
			}
		}
		$str_verification = implode(";", $arr_verification);

		$form_data = array(
			'name' 			=> $name,
			'remarks' 		=> $remarks,
			'address' 		=> $address,
			// 'pic' 			=> $pic,
			'email' 		=> $email,
			'phone_no' 		=> $phone_number,
			'fax' 			=> $fax,
			'contact_name' 	=> $contact_name,
			'contact_number' => $contact_phone,
			'verification' 	=> $str_verification
		);

		$where['id_vendor'] = $id;
		$this->vendor_mod->vendor_edit($form_data, $where);

		$this->session->set_flashdata('success', 'Your data has been Updated!');
		redirect('vendor_list');
	}

	function vendor_delete_process(){		
		$id = $this->input->post('id');

		$form_data = array(
			'status' => 0
		);

		$where['id_vendor'] = $id;
		$this->vendor_mod->vendor_edit($form_data, $where);
		unset($where);
	}

	function pic_autocomplete(){
		if (isset($_GET['term'])){
            $result = $this->vendor_mod->search_pic_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['badge_no'].' - '.$row['full_name'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "User Not Found";
                echo json_encode($arr_result);
            }
        }
	}

	function vendor_import(){
		$this->data['meta_title']       = 'Import Vendor';
        $this->data['read_cookies']     = $this->user_cookie;
        $this->data['read_permission']  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;
        $this->data['subview']          = 'vendor/vendor_import';

        $this->load->view('index', $this->data);
	}

	function vendor_preview(){
        $id_user = $this->user_cookie;
        $date = date('Y-m-d H:i:s');

        $config['upload_path']          = 'file/vendor/';
        $config['file_name']            = 'excel_'.$id_user[0];
        $config['allowed_types']        = 'xlsx';
        $config['overwrite']            = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('file')){
            $this->session->set_flashdata('error', $this->upload->display_errors());
            self::import_material();
            return false;
        }

        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('file/vendor/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

        $numrow = 1;

        foreach($sheet as $key => $row){

            if($numrow > 1 && $row['A'] != ''){

                //VENDOR NAME ----------------------------------------
                // $where['name']  = $row['A'];
                // $datadb         = $this->vendor_mod->getAll($where);
                // unset($where);
                // if($datadb){
                //     $datadb     = $datadb[0];
                //     $sheet[$key]['id_vendor'] = $datadb['id'];
                //     $sheet[$key]['vendor']    = $datadb['name'];
                // } else {
                //     $sheet[$key]['id_vendor'] = '';
                //     $sheet[$key]['vendor']    = $row['A'];
                // }
                //----------------------------------------------------
            
            }

            $numrow++; // Tambah 1 setiap kali looping
        }

        $data['sheet']          = $sheet;
        $data['read_cookies']   = $this->user_cookie;
        $data['meta_title']     = 'Vendor Preview';
        $data['subview']        = 'vendor/vendor_preview';
        $data['sidebar']        = $this->sidebar;
        $this->load->view('index', $data);
    }

    public function vendor_import_process(){

        // $data = $this->vendor_mod->get_last_code();
        // if(sizeof($data) == 0){
        //     $code = '000001';
        // } else {
        //     $code = str_pad($data[0]->code_material + 1, 6, '0', STR_PAD_LEFT);
        // }

        $vendor_name        = $this->input->post('vendor_name');
        $remarks            = $this->input->post('remarks');
        $address           	= $this->input->post('address');
        $pic          		= $this->input->post('pic');
        $phone_no           = $this->input->post('phone_no');
        $email              = $this->input->post('email');
        $fax                = $this->input->post('fax');
        $contact_name       = $this->input->post('contact_name');
        $contact_phone      = $this->input->post('contact_phone');

        if(count($vendor_name) > 0){
            $form_data = array();
            foreach ($vendor_name as $key => $value) {
                // $code = str_pad($code + $key, 6, '0', STR_PAD_LEFT);

                array_push($form_data, array(
                    'name'   				=> $vendor_name[$key],
                    'address'              	=> $address[$key], // Insert data nis dari kolom A di excel
                    'pic'        			=> $pic[$key], // Insert data nis dari kolom A di excel
                    'phone_no'       		=> $phone_no[$key], // Insert data nis dari kolom A di excel
                    'email'                	=> $email[$key], // Insert data nis dari kolom A di excel
                    'fax'               	=> $fax[$key], // Insert data nis dari kolom A di excel
                    'contact_name'          => $contact_name[$key], // Insert data nis dari kolom A di excel
                    'contact_number'        => $contact_phone[$key], // Insert data nis dari kolom A di excel
                    'remarks'        		=> $remarks[$key], // Insert data nis dari kolom A di excel
                    'status'        		=> 1, // Insert data nis dari kolom A di excel
                ));
            }

            $this->vendor_mod->vendor_import_process_db($form_data);
            $this->session->set_flashdata('success', 'Your data has been imported!');
            redirect("vendor_list");
        }
        else{
            $this->session->set_flashdata('error', 'No Data to import!');
            redirect("material_catalog_list");
        }

        
    }

	//RECEIVING DETAIL LIST DATATABLE ----------------------------------------------------------------------------------
	function vendor_list_json(){
    	error_reporting(0);
 
        $list = $this->vendor_mod->get_datatables_vendor_list_dt();
        
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {			
            $no++;
            $row   = array();
            
            $links = base_url()."vendor_edit/".strtr($this->encryption->encrypt($list->id_vendor), '+=/', '.-~');

            $row[] = $list->name;
            $row[] = $list->remarks;
            $row[] = $list->address;
            // $row[] = $list->pic;
            $row[] = $list->email;
            $row[] = $list->phone_no;
            $row[] = $list->fax;
            $row[] = $list->contact_name;
            $row[] = $list->contact_number;

            if(strpos($list->verification, '0') !== false || $list->verification == ''){
            	$status_verification = '<span class="badge badge-danger">Not Completed</span>';
            } else {
            	$status_verification = '<span class="badge badge-success">Completed</span>';
            }

            $row[] = $status_verification;
            $row[] = "<a href='".$links."' class='btn btn-warning text-white' title='Edit'><i class='fa fa-edit'></i> Edit </a>&nbsp; 
            		  <button class='btn btn-danger text-white' title='Delete' onclick='delete_action(".$list->id_vendor.")'><i class='fas fa-trash'></i> Delete </button>&nbsp;
            		 ";
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->vendor_mod->count_all_vendor_list_dt(),
            "recordsFiltered" => $this->vendor_mod->count_filtered_vendor_list_dt(),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }
    //-----------------------------------------------------------------------------------------------------------
}