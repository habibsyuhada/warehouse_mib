<?php

date_default_timezone_set('Asia/Jakarta');

defined('BASEPATH') OR exit('No direct script access allowed');

class Mto extends CI_Controller {

	public function __construct() {
			
		parent::__construct();
		$this->load->helper('browser');
		// check_browser();
		$this->load->helper('cookies');
		helper_cookies();

		$this->load->model('home_mod');
		$this->load->model('general_mod');
		$this->load->model('engineering_mod');
		$this->load->model('mrir_mod');

		//FOR MTO ===================================
		$this->load->model('mto_mod');
		$this->load->model('material_catalog_mod');
		$this->load->model('mto_category_mod');
		$this->load->model('priority_mod');
		//===========================================

		$this->user_cookie = explode(";",$this->input->cookie('portal_user'));
		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
		$this->sidebar = "mto/sidebar";
	}

	public function index(){
		redirect('mto_list/draft');
	}

	public function mto_list($param){

		$str_param = ucfirst($param);
		$data['_parameter'] = $param;

		$datadb  = $this->mto_mod->mto_category_get_db();

		foreach ($datadb as $value) {
			$data['mto_category_list'][$value['id']] = $value['description'];
		}

		$data['read_cookies'] 		= $this->user_cookie;
		$data['meta_title'] 		= 'MTO List '.$str_param;
		$data['subview']    		= 'mto/mto/mto_list';
		$data['sidebar']    		= $this->sidebar;
		$data['read_permission']  	= $this->permission_cookie;

		if($this->user_cookie[10] == 0){

			$this->session->set_flashdata('message','<script type="text/javascript">swal.fire({title: "Sorry!",text: "Your ID is not registered for any project.\n\nPlease call Portal Administrator to get support for this issue!",type: "warning"}).then(function() { window.location = "'.$this->user_cookie[9].'"});</script>');
			
		}	

		$this->load->view('index', $data);
	}

	public function mto_add(){

		$where['status'] = 1;
		$data = array(
            'project_chain' 		 => $this->general_mod->data_project($where),
            'module_chain' 			 => $this->general_mod->data_module(null),
           	'project_chain_selected' => '',
            'module_chain_selected'  => ''
        );
        unset($where);

		//PROJECT ----------------------------------------------------------------
		$where['status'] = 1;
		$data['project_list'] = $this->general_mod->data_project($where);
		unset($where);
		//-----------------------------------------------------------------------

		//MODULE ----------------------------------------------------------------
		$data['module_list'] = $this->general_mod->data_module(null);
		//-----------------------------------------------------------------------

		//PRIORITY  ----------------------------------------------------------
		$where['status_delete'] = 1;
		$data['priority_list'] = $this->priority_mod->getAll($where);
		unset($where);

		//-----------------------------------------------------------------------

		$data['read_cookies'] 	  = $this->user_cookie;
		$data['meta_title'] 	  = 'Add New MTO';
		$data['subview']    	  = 'mto/mto/mto_new';
		$data['sidebar']    	  = $this->sidebar;
		$data['read_permission']  = $this->permission_cookie;
		$this->load->view('index', $data);

	}

	public function mto_add_process(){

		$dt_mto_no = $this->mto_mod->get_last_mto_number();
		if(sizeof($dt_mto_no) == 0){
			$mto_number = '000001';
		} else {
			$mto_number = str_pad($dt_mto_no[0]->mto_number + 1, 6, '0', STR_PAD_LEFT);
		}

		$project 				= $this->input->post('project');
		$user_id 				= $this->user_cookie[0];
		$module 				= $this->input->post('module');
		$priority				= $this->input->post('priority');

		$form_data = array(
			'project_id' 	=> $project,
			'created_by' 	=> $user_id,
			'module'     	=> $module,
			'priority'	 	=> $priority,
			'mto_number' 	=> $mto_number,
			'created_date' 	=> date('Y-m-d H:i:s'),
		);

		$this->mto_mod->mto_new_process_db($form_data);
		$this->session->set_flashdata('success', 'Your data has been Created!');

		redirect('mto_list/draft');
	}

	public function mto_material_draft_add_process(){
		
		$mto_id 				= $this->input->post('mto_id');
		$material_cat 			= $this->input->post('id_material_cat');
		$discipline 			= $this->input->post('discipline');
		$nett_area				= $this->input->post('nett_area');
		$nett_length			= $this->input->post('nett_length');
		$unit_wt				= $this->input->post('unit_wt');
		$cont					= $this->input->post('cont');
		$certification			= $this->input->post('certification');
		$remarks				= $this->input->post('remarks');
		$total					= $this->input->post('total_pcs');
		$status					= 1;

		$form_data = array(
			'mto_id' 	 	=> $mto_id,
			'catalog_id' 	=> $material_cat,
			'discipline' 	=> $discipline,
			'nett_area' 	=> $nett_area,
			'nett_length' 	=> $nett_length,
			'unit_wt' 		=> $unit_wt,
			'cont' 			=> $cont,
			'certification' => $certification,
			'remarks' 		=> $remarks,
			'total_qty'		=> $total,
			'status' 		=> $status
		);

		$this->mto_mod->mto_detail_new_process_db($form_data);
	}

	public function mto_detail($mto_id = null){
		$mto_id = $this->encryption->decrypt(strtr($mto_id, '.-~', '+=/'));

		// echo $mto_id;exit;

		if(empty($mto_id)){ 
			redirect('mto_list/draft');
		}

		//MTO --------------------------------------------------------------------------
		$where['id'] = $mto_id;
		$mto_list = $this->mto_mod->mto_list($where);
		$data['mto_list'] = $mto_list;
		unset($where);

		$mto_list = $mto_list[0];
		$data['mto_number'] = $mto_list['mto_number'];
		//------------------------------------------------------------------------------

		//PROJECT ----------------------------------------------------------------------
		$where['id'] 	= $mto_list['project_id'];
		$project_list 	= $this->general_mod->data_project($where);
		unset($where);
		$project_list 	= $project_list[0];
		$data['project_name'] = $project_list['project_name'];
		//------------------------------------------------------------------------------

		//MODULE -----------------------------------------------------------------------
		$where['mod_id'] = $mto_list['module'];
		$module_list = $this->general_mod->data_module($where);
		unset($where);
		$data['module_name'] = $module_list[0]['mod_desc'];
		//------------------------------------------------------------------------------

		//DISCIPLINE -------------------------------------------------------------------
		$data['discipline_list'] = $this->general_mod->data_discipline(null);
		//------------------------------------------------------------------------------

		//PRIORITY ---------------------------------------------------------------------
		$where['id'] = $mto_list['priority'];
		$priority_list = $this->priority_mod->getAll($where);
		unset($where);
		$data['priority_name'] = $priority_list[0]['priority_name'];
		//------------------------------------------------------------------------------

		//CERTIFICATION ---------------------------------------------------------
		$where['status_delete'] = 1;
		$data['certification_list'] = $this->general_mod->data_certification($where);
		unset($where);
		//-----------------------------------------------------------------------

		if($this->input->get('t')){
			$data['t'] 	= $this->input->get('t');
		}else{
			$data['t'] 	= '';
		}

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= 'MTO '.$mto_list['mto_number'];
		$data['subview']    		= 'mto/mto/mto_detail';
		$data['sidebar']    		= $this->sidebar;
		
		$this->load->view('index', $data);
	}

	public function mto_detail_approval($mto_id = null){
		$mto_id = $this->encryption->decrypt(strtr($mto_id, '.-~', '+=/'));

		// echo $mto_id;exit;

		if(empty($mto_id)){ 
			redirect('mto_list/waiting');
		}

		//MTO --------------------------------------------------------------------------
		$where['id'] = $mto_id;
		$mto_list = $this->mto_mod->mto_list($where);
		$data['mto_list'] = $mto_list;
		unset($where);

		$mto_list = $mto_list[0];
		//------------------------------------------------------------------------------

		//MTO DETAIL -------------------------------------------------------------------
		$where['mto_id'] = $mto_id;
		$mto_detail_list = $this->mto_mod->mto_material_join_list($where);
		$data['mto_detail_list'] = $mto_detail_list;
		unset($where);

		for($i=0; $i<sizeof($mto_detail_list); $i++){

			//AREA PER PLATE --------------------
			if($mto_detail_list[$i]['length_m'] && $mto_detail_list[$i]['width_m']){
				$area_per_plate = intval($mto_detail_list[$i]['length_m']) * intval($mto_detail_list[$i]['width_m']); 
				$data['mto_detail_list'][$i]['area_per_plate'] = $area_per_plate;
			} else{
				$data['mto_detail_list'][$i]['area_per_plate'] = '';
			}
			//-----------------------------------

			// TOTAL PCS ------------------------
			if($data['mto_detail_list'][$i]['nett_area']){
				$total_pcs = @intval((($data['mto_detail_list'][$i]['nett_area'] * ($data['mto_detail_list'][$i]['cont'] / 100) + $data['mto_detail_list'][$i]['nett_area']) / @$area_per_plate) + 1);
			} else if($data['mto_detail_list'][$i]['nett_length']){
				$total_pcs = @intval((($data['mto_detail_list'][$i]['nett_length'] * ($data['mto_detail_list'][$i]['cont'] / 100) + $data['mto_detail_list'][$i]['nett_length']) / $mto_detail_list[$i]['length_m']) + 1);
			}

			$data['mto_detail_list'][$i]['total_pcs'] = $total_pcs;
			//-----------------------------------

			//WEIGHT PER PIECE ------------------
			if($mto_detail_list[$i]['width_m']){
				$weight_per_piece = round(intval($mto_detail_list[$i]['thk_mm']) * intval($mto_detail_list[$i]['width_m']) * intval($mto_detail_list[$i]['length_m']) * 7.85 / 1000, 2);
			} else {
				$weight_per_piece = round(intval($mto_detail_list[$i]['length_m']) * intval($data['mto_detail_list'][$i]['unit_wt']) / 1000, 2);
			}

			$data['mto_detail_list'][$i]['weight_per_piece'] = $weight_per_piece;
			//-----------------------------------

			//WEIGHT TOTAL ----------------------
			$weight_total = round($total_pcs * $weight_per_piece, 2);
			$data['mto_detail_list'][$i]['weight_total'] = $weight_total;
			//-----------------------------------
		}
		//------------------------------------------------------------------------------

		//PROJECT ----------------------------------------------------------------------
		$where['id'] = $mto_list['project_id'];
		$datadb = $this->general_mod->data_project($where);
		$datadb = $datadb[0];
		unset($where);

		$data['name_project'] = $datadb['project_name'];
		//------------------------------------------------------------------------------

		//MODULE -----------------------------------------------------------------------
		$where['mod_id'] = $mto_list['module'];
		$module_list = $this->general_mod->data_module($where);
		unset($where);
		$data['module_name'] = $module_list[0]['mod_desc'];
		//------------------------------------------------------------------------------

		//PRIORITY ---------------------------------------------------------------------
		$where['id'] = $mto_list['priority'];
		$priority_list = $this->priority_mod->getAll($where);
		unset($where);
		$data['priority_name'] = $priority_list[0]['priority_name'];
		//------------------------------------------------------------------------------

		if($this->input->get('t')){
			$data['t'] 	= $this->input->get('t');
		}else{
			$data['t'] 	= '';
		}

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= 'MTO '.$module_list[0]['mod_desc'];
		$data['subview']    		= 'mto/mto/mto_approval_detail';
		$data['sidebar']    		= $this->sidebar;
		
		$this->load->view('index', $data);
	}

	function material_data(){
		$mto_id = $this->input->post('mto_id');

		//MTO MASTER ------------------------------------------------
		$where['id'] = $mto_id;
		$data_mto = $this->mto_mod->mto_list($where);
		unset($where);
		$data_mto = $data_mto[0];
		//-----------------------------------------------------------
		
		//MTO DETAIL ------------------------------------------------
		$where['mto_id'] = $data_mto['id'];
		$where['status !='] = 0;
		$data = $this->mto_mod->mto_material_list($where);
		unset($where);
		//-----------------------------------------------------------

		for($i=0;$i<sizeof($data);$i++){

			// MATERIAL CATALOG ============================================
			$material_catalog_id = $data[$i]['catalog_id'];
			
			$where['id'] = $material_catalog_id;
			$datadb = $this->material_catalog_mod->getAll($where);
			unset($where);

			$data[$i]['code_material'] 	= $datadb[0]['code_material'];
			$data[$i]['material'] 		= $datadb[0]['material'];
			$data[$i]['steel_type']		= $datadb[0]['steel_type'];
			$data[$i]['grade']			= $datadb[0]['grade'];
			$data[$i]['thk_mm']			= $datadb[0]['thk_mm'];
			$data[$i]['width_m']		= $datadb[0]['width_m'];
			$data[$i]['length_m']		= $datadb[0]['length_m'];

			//AREA PER PLATE --------------------
			if($datadb[0]['length_m'] && $datadb[0]['width_m']){
				$area_per_plate = intval($datadb[0]['length_m']) * intval($datadb[0]['width_m']); 
				$data[$i]['area_per_plate'] = $area_per_plate;
			} else{
				$data[$i]['area_per_plate'] = '';
			}
			//-----------------------------------

			// TOTAL PCS ------------------------
			if($data[$i]['width_m']){
				$total_pcs = intval((($data[$i]['nett_area'] * ($data[$i]['cont'] / 100) + $data[$i]['nett_area']) / @$area_per_plate) + 1);
			} else if($data[$i]['nett_length']){
				$total_pcs = intval((($data[$i]['nett_length'] * ($data[$i]['cont'] / 100) + $data[$i]['nett_length']) / $datadb[0]['length_m']) + 1);
			}

			$data[$i]['total_pcs'] = $total_pcs;
			//-----------------------------------

			//WEIGHT PER PIECE ------------------
			if($datadb[0]['width_m']){
				$weight_per_piece = round($datadb[0]['thk_mm'] * $datadb[0]['width_m'] * $datadb[0]['length_m'] * 7.85 / 1000, 2);
			} else {
				$weight_per_piece = round($datadb[0]['length_m'] * $data[$i]['unit_wt'] / 1000, 2);
			}

			$data[$i]['weight_per_piece'] = $weight_per_piece;
			//-----------------------------------

			//WEIGHT TOTAL ----------------------
			$weight_total = round($total_pcs * $weight_per_piece, 2);
			$data[$i]['weight_total'] = $weight_total;
			//-----------------------------------

			// ==============================================================
		}

		echo json_encode($data);
	}

	public function mto_material_draft_delete_process(){		

		//if($this->user_cookie[4] != '14'){ // UPDATE READ ENGINGEERING MEMBER 14 -- > ID ENGINEERING DEPT
			//redirect($this->user_cookie[9]); // REDIRECT TO PORTAL
		//}

		$id = $this->input->post('id');

		$form_data = array(
			'status' => 0,
			'modify_date' => date('Y-m-d H:i:s'),
			'modify_by' => $this->user_cookie[0]
		);

		$where['id'] = $id;
		$this->mto_mod->mto_detail_edit_process_db($form_data, $where);
	}

	function mto_material_edit_process(){		
		$col 		= $this->input->post('col');
		$id 		= $this->input->post('id');
		$value 		= $this->input->post('value');

		$form_data = array(
			$col => $value,
		);
		$where['id'] = $id;
		$this->mto_mod->mto_detail_edit_process_db($form_data, $where);
	}

	//MTO DETAIL PREVIEW ==================================================================
	public function mto_detail_preview(){

		$mto_number = $this->input->post('mto_number');
		$id_user = $this->user_cookie;
		$date = date('Y-m-d H:i:s');

		$config['upload_path']          = 'file/mto/';
		$config['file_name']            = 'excel_'.$id_user[0];
		$config['allowed_types']        = 'xlsx';
		$config['overwrite'] 			= TRUE;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if ( ! $this->upload->do_upload('file')){
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect("mto_detail/".strtr($this->encryption->encrypt($mto_number), '+=/', '.-~'));
			return false;
		}

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('file/mto/'.$this->upload->data('file_name')); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		$data_check = array();
		
		$numrow = 1;
		// $id_material_catalog = [];
		$arr_material_catalog = [];

		// $id_mto_category = [];
		$arr_mto_category = [];
		foreach($sheet as $row){
			
			if($numrow > 1){
				$data_check[] = $row['A'];
			}

			//MATERIAL CATALOG ===========================================================
			$where['code_material'] = $row['A'];
			$datadb				 = $this->material_catalog_mod->getAll($where);	
			if($datadb){
				$datadb			   						= $datadb[0];
				$arr_material_catalog[] 				= $datadb['code_material'];
				$sheet[$numrow]['id_material_catalog']	= $datadb['id'];
				$sheet[$numrow]['material_catalog'] 	= $datadb['material'];
				$sheet[$numrow]['steel_type'] 			= $datadb['steel_type'];
				$sheet[$numrow]['grade'] 				= $datadb['grade'];
				$sheet[$numrow]['thk_mm'] 				= $datadb['thk_mm'];
				$sheet[$numrow]['width_m'] 				= $datadb['width_m'];
				$sheet[$numrow]['length_m'] 			= $datadb['length_m'];
			}
			unset($where);
			// ===========================================================================

			$numrow++; // Tambah 1 setiap kali looping
		}

		$data['sheet']						= $sheet;
		$data['mto_number'] 				= $mto_number;

		$data['material_catalog_list'] 		= $arr_material_catalog;
		$data['mto_category_list'] 			= $arr_mto_category;

		$data['read_cookies'] 	= $this->user_cookie;
		$data['meta_title'] 	= 'MTO Preview';
		$data['subview']    	= 'mto/mto/mto_preview';
		$data['sidebar']    	= $this->sidebar;
		$this->load->view('index', $data);
	}

	//=================================================================================================

    //DRAWING AUTOCOMPLETE ============================================================================
	function drawing_autocomplete(){
		if (isset($_GET['term'])){
            $result = $this->mto_mod->search_drawing_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['drawing_no'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Drawing Not Found";
                echo json_encode($arr_result);
            }
        }
	}
	//=================================================================================================

	//DRAWING AUTOCOMPLETE ============================================================================
	function mto_number_autocomplete(){
		if (isset($_GET['term'])){
            $result = $this->mto_mod->search_mto_number_autocomplete($_GET['term']);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['mto_number'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "MTO Number Not Found";
                echo json_encode($arr_result);
            }
        }
	}
	//=================================================================================================

	//IMPORT MATERIAL =================================================================================
	function mto_detail_import_process(){

		//MTO ID ------------------------------------------
		$mto_number = $this->input->post('mto_number');
		$where['mto_number'] = $mto_number;
		$datadb = $this->mto_mod->mto_list($where);
		unset($where);

		if($datadb){
			$datadb = $datadb[0];
			$id_mto = $datadb['id'];
		}
		//-------------------------------------------------

        $mto_id      			= $id_mto;
        $material_catalog       = $this->input->post('material_catalog');
        $nett_area 				= $this->input->post('nett_area');
        $nett_length 			= $this->input->post('nett_length');
        $unit_wt 				= $this->input->post('unit_wt');
        $cont 					= $this->input->post('cont');
        $certification 			= $this->input->post('certification');
        $remarks 				= $this->input->post('remarks');

        if(count($material_catalog) > 0){
            $form_data = array();

            foreach ($material_catalog as $key => $value) {
                array_push($form_data, array(
                    'mto_id' 				=> $id_mto, // Insert data nis dari kolom A di excel
                    'catalog_id'			=> $material_catalog[$key], // Insert data nis dari kolom A di excel
                    'nett_area'				=> $nett_area[$key],
                    'nett_length'			=> $nett_length[$key],
                    'unit_wt'				=> $unit_wt[$key],
                    'cont'					=> $cont[$key],
                    'certification'			=> $certification[$key],
                    'remarks'				=> $remarks[$key],
                    'status' 				=> 1
                ));
            }

            $this->mto_mod->mto_detail_import_process_db($form_data);
            $this->session->set_flashdata('success', 'Your data has been imported!');
            redirect("mto_detail/".strtr($this->encryption->encrypt($mto_id), '+=/', '.-~'));
        }
        else{
            $this->session->set_flashdata('error', 'No Data to import!');
            redirect("mto_detail/".strtr($this->encryption->encrypt($mto_id), '+=/', '.-~'));
        }

        
    }
	//=================================================================================================

	//MATERIAL CATALOG AUTOCOMPLETE====================================================================
	function material_catalog_autocomplete(){
		$material_catalog = $this->input->post('material_catalog');
		if (isset($_GET['term'])){
            $result = $this->mto_mod->search_material_catalog_autocomplete($_GET['term'], $material_catalog);
            if ($result == TRUE){
                foreach ($result as $row)
                    $arr_result[] = $row['code_material'].' - '.$row['material'];
                echo json_encode($arr_result);
            } else {
                $arr_result[] = "Material Not Found";
                echo json_encode($arr_result);
            }
        }
	}

	function mto_check_material_catalog(){
		$material 	= $this->input->post('material');
		$mto_id 	= $this->input->post('mto_id');

		$where['mc.code_material'] = $material;
		$result = $this->material_catalog_mod->get_material_catalog_join($where);
		unset($where);

		$text = 0;
		$data = array(
			"id" => $text
		);

		if(count($result) > 0){
			$result = $result[0];

			//MTO DETAIL ------------------
			$where['mto_id'] 		= $mto_id;
			$where['catalog_id'] 	= $result['catalog_id'];
			$where['status != '] 	= 0;
			$data_mto_detail = $this->mto_mod->mto_material_list($where);
			unset($where);
			//-----------------------------

			if(count($data_mto_detail) < 1){
				$text = $result['catalog_id'];
				$data = array(
					"id" 				=> $text,
					"description"		=> $result['material'],
					"material_grade"	=> $result['name_material_grade'],
					"material_class"	=> $result['name_material_class'],
					"thk_mm"			=> $result['thk_mm'],
					"width_m"			=> $result['width_m'],
					"length_m"			=> $result['length_m'],
					"weight"			=> $result['weight'],
					"od"				=> $result['od'],
					"sch"				=> $result['sch'],
				);
			}
		}

		echo json_encode($data);
	}

	function mto_submit($mto_id = null){
		$mto_id = $this->encryption->decrypt(strtr($mto_id, '.-~', '+=/'));

		if(empty($mto_id)){ 
			redirect('mto_list/draft');
		}

		//MTO --------------------------------------------------------------------------
		$where['id'] = $mto_id;
		$mto_list = $this->mto_mod->mto_list($where);
		$data['mto_list'] = $mto_list;
		unset($where);

		$mto_list = $mto_list[0];
		//------------------------------------------------------------------------------

		//MODULE -----------------------------------------------------------------------
		$where['mod_id'] = $mto_list['module'];
		$module_list = $this->general_mod->data_module($where);
		unset($where);
		$data['module_name'] = $module_list[0]['mod_desc'];
		//------------------------------------------------------------------------------

		//PRIORITY ---------------------------------------------------------------------
		$where['id'] = $mto_list['priority'];
		$priority_list = $this->priority_mod->getAll($where);
		unset($where);
		$data['priority_name'] = $priority_list[0]['priority_name'];
		//------------------------------------------------------------------------------

		//MTO DETAIL -------------------------------------------------------------------
		$where['mto_id'] = $mto_id;
		$mto_detail_list = $this->mto_mod->mto_material_list($where);
		unset($where);
		$data['count_mto_detail'] = sizeof($mto_detail_list);
		//------------------------------------------------------------------------------

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= 'MTO '.$module_list[0]['mod_desc'].' | '.$priority_list[0]['priority_name'];
		$data['subview']    		= 'mto/mto/mto_submit';
		$data['sidebar']    		= $this->sidebar;
		
		$this->load->view('index', $data);
	}

	function mto_submit_process(){
		$id 	= $this->input->post('mto_id');
		$remarks = $this->input->post('remarks');

		$form_data = array(
			'status' => 1,
			'remarks' => $remarks,
		);
		$where['id'] = $id;

		$this->mto_mod->mto_edit_process_db($form_data, $where);

		redirect('mto_list/draft');
	}

	public function mto_approval($mto_id = null){
		$mto_id = $this->encryption->decrypt(strtr($mto_id, '.-~', '+=/'));

		// echo $mto_id;exit;

		if(empty($mto_id)){ 
			redirect('mto_list/waiting');
		}

		//MTO --------------------------------------------------------------------------
		$where['id'] = $mto_id;
		$mto_list = $this->mto_mod->mto_list($where);
		$data['mto_list'] = $mto_list;
		unset($where);

		$mto_list = $mto_list[0];
		//------------------------------------------------------------------------------

		//MTO DETAIL -------------------------------------------------------------------
		$where['mto_id'] = $mto_id;
		$mto_detail_list = $this->mto_mod->mto_material_join_list($where);
		$data['mto_detail_list'] = $mto_detail_list;
		unset($where);

		for($i=0; $i<sizeof($mto_detail_list); $i++){

			//AREA PER PLATE --------------------
			if($mto_detail_list[$i]['length_m'] && $mto_detail_list[$i]['width_m']){
				$area_per_plate = intval($mto_detail_list[$i]['length_m']) * intval($mto_detail_list[$i]['width_m']); 
				$data['mto_detail_list'][$i]['area_per_plate'] = $area_per_plate;
			} else{
				$data['mto_detail_list'][$i]['area_per_plate'] = '';
			}
			//-----------------------------------

			// TOTAL PCS ------------------------
			if($data['mto_detail_list'][$i]['nett_area']){
				$total_pcs = @intval((($data['mto_detail_list'][$i]['nett_area'] * ($data['mto_detail_list'][$i]['cont'] / 100) + $data['mto_detail_list'][$i]['nett_area']) / @$area_per_plate) + 1);
			} else if($data['mto_detail_list'][$i]['nett_length']){
				$total_pcs = @intval((($data['mto_detail_list'][$i]['nett_length'] * ($data['mto_detail_list'][$i]['cont'] / 100) + $data['mto_detail_list'][$i]['nett_length']) / $mto_detail_list[$i]['length_m']) + 1);
			}

			$data['mto_detail_list'][$i]['total_pcs'] = $total_pcs;
			//-----------------------------------

			//WEIGHT PER PIECE ------------------
			if($mto_detail_list[$i]['width_m']){
				$weight_per_piece = round(intval($mto_detail_list[$i]['thk_mm']) * intval($mto_detail_list[$i]['width_m']) * intval($mto_detail_list[$i]['length_m']) * 7.85 / 1000, 2);
			} else {
				$weight_per_piece = round(intval($mto_detail_list[$i]['length_m']) * intval($data['mto_detail_list'][$i]['unit_wt']) / 1000, 2);
			}

			$data['mto_detail_list'][$i]['weight_per_piece'] = $weight_per_piece;
			//-----------------------------------

			//WEIGHT TOTAL ----------------------
			$weight_total = round($total_pcs * $weight_per_piece, 2);
			$data['mto_detail_list'][$i]['weight_total'] = $weight_total;
			//-----------------------------------
		}
		//------------------------------------------------------------------------------

		//MODULE -----------------------------------------------------------------------
		$where['mod_id'] = $mto_list['module'];
		$module_list = $this->general_mod->data_module($where);
		unset($where);
		$data['module_name'] = $module_list[0]['mod_desc'];
		//------------------------------------------------------------------------------

		//PRIORITY ---------------------------------------------------------------------
		$where['id'] = $mto_list['priority'];
		$priority_list = $this->priority_mod->getAll($where);
		unset($where);
		$data['priority_name'] = $priority_list[0]['priority_name'];
		//------------------------------------------------------------------------------

		//PROJECT
		$where['id'] = $mto_list['project_id'];
		$datadb = $this->general_mod->data_project($where);
		$datadb = $datadb[0];
		unset($where);
		$data['name_project'] = $datadb['project_name'];

		if($this->input->get('t')){
			$data['t'] 	= $this->input->get('t');
		}else{
			$data['t'] 	= '';
		}

		$data['read_cookies'] 		= $this->user_cookie;
		$data['read_permission'] 	= $this->permission_cookie;
		$data['meta_title'] 		= 'MTO '.$module_list[0]['mod_desc'];
		$data['subview']    		= 'mto/mto/mto_approval';
		$data['sidebar']    		= $this->sidebar;
		
		$this->load->view('index', $data);
	}

	function mto_approval_proccess(){
		$_approve 		= $this->input->post('approve');
		$reject_remarks = $this->input->post('reject_remarks');

		foreach ($_approve as $key => $approve) {
			$str_id = explode(" ",$approve);

			if($str_id[0] == 'A'){
				$form_data['status'] = 3;
			} else{
				$form_data['status'] = 2;
			}

			$form_data['reject_remarks'] = $reject_remarks[$key];

			$where['id'] = $str_id[1];
			$this->mto_mod->mto_detail_edit_process_db($form_data, $where);
			unset($where);
		}

		//CHECK ALL APPROVE / REJECT --------------------------------------
		$mto_id 	= $this->input->post('mto_id');
		$datadb 	= $this->mto_mod->count_data_approval($mto_id);
		$datadb 	= $datadb[0];

		$current_status = 'waiting';

		$where['id'] = $mto_id;
		if($datadb['total'] == $datadb['apr']){
			$form['status'] = 3;
			$current_status = 'approved';

			$this->mto_mod->mto_edit_process_db($form, $where);
		} else if($datadb['total'] == $datadb['rej'] || $datadb['pdg'] == 0){
			$form['status'] = 2;
			$current_status = 'rejected';

			$this->mto_mod->mto_edit_process_db($form, $where);
		}

		$this->session->set_flashdata('success', 'Your data has been '.$current_status.' !');
		//-----------------------------------------------------------------

		redirect('mto_list/'.$current_status);
	}

	function re_submit_mto($mto_id){
		$mto_id = $this->encryption->decrypt(strtr($mto_id, '.-~', '+=/'));

		if(empty($mto_id)){ 
			redirect('mto_list/waiting');
		}

		// MTO ---------------------------------------
		$where['id'] = $mto_id;
		$mto_data = $this->mto_mod->mto_list($where);
		$mto_data = $mto_data[0];
		$mto_rev = intval($mto_data['mto_rev']) + 1;

		$form_data = array(
			'status' => 0,
			'mto_rev' => $mto_rev,
		);

		$this->mto_mod->mto_edit_process_db($form_data, $where);
		unset($where);
		unset($form_data);
		//-------------------------------------------

		// MTO DETAIL -------------------------------
		$where['mto_id'] = $mto_id;
		$form_data['status'] = 1;

		$mto_detail_data = $this->mto_mod->mto_detail_edit_process_db($form_data, $where);
		unset($where);
		//-------------------------------------------

		$this->session->set_flashdata('success', 'Your data has been resubmited !');

		redirect('mto_list/draft');

	}

	function export_mto($mto_id){

		$mto_id = $this->encryption->decrypt(strtr($mto_id, '.-~', '+=/'));

		if(empty($mto_id)){
			redirect('mto_list/approved');
		}

		$where['id'] 		= $mto_id;
		$data['mto_list'] 	= $this->mto_mod->mto_list($where);
		unset($where);

		$where['mto_id'] 	= $mto_id;
		$mto_detail_list 	= $this->mto_mod->mto_material_list($where);
		unset($where);

		foreach ($mto_detail_list as $key => $mto_detail) {
			$where['id'] = $mto_detail['catalog_id'];
			$datadb = $this->material_catalog_mod->getAll($where);
			unset($where);

			$datadb = $datadb[0];
			$mto_detail_list[$key]['catalog_name']  = $datadb['material']; 
			$mto_detail_list[$key]['steel_type'] 	= $datadb['steel_type']; 
			$mto_detail_list[$key]['grade'] 		= $datadb['grade'];
			$mto_detail_list[$key]['thk_mm'] 		= $datadb['thk_mm'];
			$mto_detail_list[$key]['width_m'] 		= $datadb['width_m'];
			$mto_detail_list[$key]['length_m'] 		= $datadb['length_m'];

			//AREA PER PLATE --------------------
			if($datadb['length_m'] && $datadb['width_m']){
				$area_per_plate = $datadb['length_m'] * $datadb['width_m']; 
				$mto_detail_list[$key]['area_per_plate'] = $area_per_plate;
			} else{
				$mto_detail_list[$key]['area_per_plate'] = '';
			}
			//-----------------------------------

			// TOTAL PCS ------------------------
			if($mto_detail_list[$key]['nett_area']){
				$total_pcs = intval((($mto_detail_list[$key]['nett_area'] * ($mto_detail_list[$key]['cont'] / 100) + $mto_detail_list[$key]['nett_area']) / @$area_per_plate) + 1);
			} else if($mto_detail_list[$key]['nett_length']){
				$total_pcs = intval((($mto_detail_list[$key]['nett_length'] * ($mto_detail_list[$key]['cont'] / 100) + $mto_detail_list[$key]['nett_length']) / $datadb['length_m']) + 1);
			}

			$mto_detail_list[$key]['total_pcs'] = $total_pcs;
			//-----------------------------------

			//WEIGHT PER PIECE ------------------
			if($datadb['width_m']){
				$weight_per_piece = round($datadb['thk_mm'] * $datadb['width_m'] * $datadb['length_m'] * 7.85 / 1000, 2);
			} else {
				$weight_per_piece = round($datadb['length_m'] * $mto_detail_list[$key]['unit_wt'] / 1000, 2);
			}

			$mto_detail_list[$key]['weight_per_piece'] = $weight_per_piece;
			//-----------------------------------

			//WEIGHT TOTAL ----------------------
			$weight_total = round($total_pcs * $weight_per_piece, 2);
			$mto_detail_list[$key]['weight_total'] = $weight_total;
			//-----------------------------------
		}

		$data['mto_detail_list'] = $mto_detail_list;

		$data['material_catalog_list'] = $this->material_catalog_mod->getAll(null);

		$approval_log 				= $this->general_mod->getApprovalLog($mto_id,"MTO Number");
		$total_array 				= count($approval_log);
		
		if($total_array > 0){
			$app_nos = $approval_log[0]["approved_category"]."/".$approval_log[0]["id"]."/".$approval_log[0]["approved_request_no"];
		} else {
			$app_nos = "Document Not Approved By QC Inspector";	
		}

		//print_r($app_nos);
		//return false;

	    $this->load->library('Pdfgenerator');

	    $html = $this->load->view('mto/mto/mto_export', $data, true);
	    
	    $this->pdfgenerator->generate($html,$mto_id,$app_nos);
	}

	//Drawing List DataTables 


	function mto_list_json($param)
    {
    	error_reporting(0);
 
        // $list = $this->mto_mod->get_datatables_mto_list_dt($this->user_cookie[10]);
        $list = $this->mto_mod->get_datatables_mto_list_dt($param);

        // print_r($list);exit;
        
        //return false
        // dd($type);
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {

   			//PRIORITY --------------------------------------------------------------------------
   			if($list->priority == 1){
   				$str_priority = '<span class="badge badge-danger">Primary</span>';
   			} else {
   				$str_priority = '<span class="badge badge-warning">Secondary</span>';
   			}
   			//-----------------------------------------------------------------------------------

   			if($param == 'draft'){
				$links = base_url()."mto_detail/".strtr($this->encryption->encrypt($list->mto_id), '+=/', '.-~');
   			} else {
   				$links = base_url()."mto_detail_approval/".strtr($this->encryption->encrypt($list->mto_id), '+=/', '.-~');
   			}

			if($param == 'draft'){
				if($this->permission_cookie[8] == 1){
				$links_action = "<a href='".base_url()."mto_submit/".strtr($this->encryption->encrypt($list->mto_id), '+=/', '.-~')."' class='btn btn-primary text-white' title='Detail'><i class='fas fa-share'></i> Submit to Engineer</a>";
				}
			} else if($param == 'waiting'){
				if($this->permission_cookie[9] == 1){
				$links_action = "<a href='".base_url()."mto_approval/".strtr($this->encryption->encrypt($list->mto_id), '+=/', '.-~')."' class='btn btn-success text-white' title='Detail'><i class='fas fa-check'></i> Approve</a>";
				}
			}
			
            $no++;
            $row   = array();
            
            $row[] = $list->mto_number;
            $row[] = isset($list->project_name) ? $list->project_name : '-';
            $row[] = isset($list->mod_desc) ? $list->mod_desc : '-';
            $row[] = isset($list->full_name) ? $list->full_name : '-';
            $row[] = date('Y-m-d', strtotime($list->mto_created_date));
            $row[] = $str_priority;
            if($param != 'draft'){
            	$row[] = $list->remarks;
            }
            $row[] = "<a href='".$links."' class='btn btn-secondary text-white' title='Detail'><i class='fas fa-file-alt'></i> Detail</a>&nbsp;".
            		 $links_action;
                       
            $data[] = $row;
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mto_mod->count_all_mto_list_dt($param),
            "recordsFiltered" => $this->mto_mod->count_filtered_mto_list_dt($param),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }

    function mto_detail_list_json($mto_id)
    {
    	error_reporting(0);
 
        $list = $this->mto_mod->get_datatables_mto_detail_list_dt($mto_id);

        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $list)
        {
			
            $no++;
            $row   = array();

            if($this->permission_cookie[5] == '1'){
            	$link = '<button class="btn btn-danger" onclick="delete_material('.$list->mto_detail_id.')"><i class="fa fa-trash"></i></button>';
        	} else {
        		$link = '-';
        	}

            $row[] = $list->code_material;
            $row[] = $list->material;
            $row[] = $list->name_material_grade;
            $row[] = $list->name_material_class;
            $row[] = $list->thk_mm;
            $row[] = $list->width_m;
            $row[] = $list->length_m;
            $row[] = $list->weight;
            $row[] = $list->od;
            $row[] = $list->sch;
            $row[] = isset($list->discipline_name) ? $list->discipline_name : '-';
            $row[] = $list->nett_area;

            //AREA PER PLATE---------
            $area_per_plate = $list->width_m * $list->length_m;
            $row[] = $area_per_plate;
            //-----------------------

            $row[] = $list->nett_length;
            $row[] = $list->unit_wt;
            $row[] = floatval($list->cont);

            //TOTAL PCS -------------
            $row[] = $list->total_qty;
            //-----------------------

            $row[] = $list->certification;

            //WEIGHT PER PIECE ------
            if($list->width_m){
            	$weight_per_piece = ($list->thk_mm * $list->width_m * $list->length_m * 7.85 / 1000);
            } else if($list->unit_wt){
            	$weight_per_piece = ($list->length_m * $list->unit_wt / 1000);
            }
            $row[] = round($weight_per_piece, 2);
            //-----------------------

            //WEIGHT TOTAL ----------
            $weight_total = $list->total_qty * $weight_per_piece;
            $row[] = round($weight_total, 2);
            //-----------------------

            $row[] = $list->remarks;
            $row[] = $link;
                       
            $data[] = $row;
        }

        //print_r($data);
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mto_mod->count_all_mto_detail_list_dt($mto_id),
            "recordsFiltered" => $this->mto_mod->count_filtered_mto_detail_list_dt($mto_id),
            "data" => $data
        );
        //output to json format
        echo json_encode($output);
    }


	//Drawing List DataTables 

  public function mto_excel($mto_id = null){
		$mto_id = $this->encryption->decrypt(strtr($mto_id, '.-~', '+=/'));
  	//MTO --------------------------------------------------------------------------
		$where['id'] = $mto_id;
		$mto_list = $this->mto_mod->mto_list($where);
		$data['mto_list'] = $mto_list;
		unset($where);

		$mto_list = $mto_list[0];
		//------------------------------------------------------------------------------

		//MTO DETAIL -------------------------------------------------------------------
		$where['mto_id'] = $mto_id;
		$mto_detail_list = $this->mto_mod->mto_material_list($where);
		$data['mto_detail_list'] = $mto_detail_list;
		unset($where);

		for($i=0; $i<sizeof($mto_detail_list); $i++){
			//---- MATERIAL CATALOG-----
			$where['id'] = $mto_detail_list[$i]['catalog_id'];
			$datadb = $this->material_catalog_mod->getAll($where);
			unset($where);

			$data['mto_detail_list'][$i]['code_material'] = $datadb[0]['code_material'];
			$data['mto_detail_list'][$i]['material'] = $datadb[0]['material'];
			$data['mto_detail_list'][$i]['steel_type'] = $datadb[0]['steel_type'];
			$data['mto_detail_list'][$i]['grade'] = $datadb[0]['grade'];
			$data['mto_detail_list'][$i]['thk_mm'] = $datadb[0]['thk_mm'];
			$data['mto_detail_list'][$i]['width_m'] = $datadb[0]['width_m'];
			$data['mto_detail_list'][$i]['length_m'] = $datadb[0]['length_m'];
			//-------------------
		}
		//------------------------------------------------------------------------------

		//MODULE -----------------------------------------------------------------------
		$where['mod_id'] = $mto_list['module'];
		$module_list = $this->general_mod->data_module($where);
		unset($where);
		$data['module_name'] = $module_list[0]['mod_desc'];
		//------------------------------------------------------------------------------
  	$this->load->view('mto/mto/mto_excel', $data);
  }
}