<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mto_category extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('browser');
        // check_browser();

        $this->load->model('home_mod');
        $this->load->model('general_mod');
        $this->load->model('engineering_mod');
        $this->load->model('mrir_mod');
        $this->load->model('mto_category_mod');

        $this->user_cookie = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_qcs'));
        $this->sidebar = "mto/sidebar";
        
    }

    public function index()
    {
        $this->data['meta_title']       = 'MTO Category';
        
        $where['status_delete'] = 0;
        $this->data["mto_category_list"]      = $this->mto_category_mod->getAll($where);
        unset($where);

        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;
        $this->data['subview']          = 'mto/mto_category/list';

        $this->load->view('index', $this->data);
    }

    public function add()
    {
        $this->data['meta_title']       = 'MTO Category';
        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;
        $this->data['subview']          = 'mto/mto_category/new_form';

        $this->load->view('index', $this->data);
        
    }

    function add_process(){
        $initial = $this->input->post('initial');
        $description = $this->input->post('description');

        $form_data = array(
            'initial'       => $initial,
            'description'   => $description,
            'status' => 1,
            'status_delete' => 0
        );

        $this->mto_category_mod->mto_category_add($form_data);
        $this->session->set_flashdata('success', 'Your data has been Created!');

        redirect('mto_category');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('pcms/mto_category');

        $id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));
        
        $this->data['meta_title']   = 'MTO Category';

        $this->data["read_cookies"]     = $this->user_cookie;
        $this->data["read_permission"]  = $this->permission_cookie;
        $this->data['sidebar']          = $this->sidebar;

        $where['id'] = $id;
        $this->data["mto_category_list"]      = $this->mto_category_mod->getAll($where);
        unset($where);

        $this->data['subview']          = 'mto/mto_category/edit_form';
        $this->load->view('index', $this->data);
    }

    function edit_process(){
        $id = $this->input->post('id');
        $initial = $this->input->post('initial');
        $description = $this->input->post('description');

        $form_data = array(
            'initial'       => $initial,
            'description'   => $description,
            'status' => 1,
            'status_delete' => 0
        );

        $where['id'] = $id;
        $this->mto_category_mod->mto_category_edit($form_data, $where);
        unset($where);

        $this->session->set_flashdata('success', 'Your data has been Changed!');

        redirect('mto_category');
    }

    public function delete($id=null)
    {
        $id = $this->encryption->decrypt(strtr($id, '.-~', '+=/'));

        if (!isset($id)) show_404();
        
        if ($this->mto_category_mod->delete($id,$cur_stat)) {
            redirect('mto_category');
        }
    }

}