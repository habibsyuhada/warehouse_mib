<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function helper_cookies(){
   	
    $CI =& get_instance();
    $read_cookies = explode(";",$CI->input->cookie('portal_user'));
    $permission_cookie = $CI->input->cookie('portal_wh');

    $CI->load->model('general_mod');

    //print_r($permission_cookie);
    //return false;
    $CI->general_mod->check_cookies($read_cookies[0],$permission_cookie,$read_cookies[9]);

}