<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity_design_mod extends CI_Model {

	public function __construct()
 	{
	  	parent::__construct();
	    
 	}

 	public function activity_design_new_process_db($data){
		$this->db->insert('pcms_eng_activity_design', $data);
	}

	public function activity_design_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('pcms_eng_activity_design',$data);
	}

	function activity_design_list($id = null, $where = null, $where_in = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		elseif(isset($where_in)){
			$this->db->where_in('document_no', $where_in);
		}
		else{
			$this->db->where('status_delete', '1');
			$this->db->where('drawing_type', '1');
		}
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->get('pcms_eng_activity_design');
		return $query->result_array();
	}

	public function activity_design_detail_new_process_db($data){
		$this->db->insert('pcms_eng_activity_design_detail', $data);
	}

	public function activity_design_detail_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('pcms_eng_activity_design_detail',$data);
	}

	function activity_design_detail_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		else{
			$this->db->where('status_delete', '1');
		}
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->get('pcms_eng_activity_design_detail');
		return $query->result_array();
	}

	public function activity_design_revision_new_process_db($data){
		$this->db->insert('pcms_eng_activity_design_revision', $data);
	}

	function activity_design_revision_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		else{
			$this->db->where('status_delete', '1');
		}
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->get('pcms_eng_activity_design_revision');
		return $query->result_array();
	}

	function activity_design_new_import_process_db($data) {
		$this->db->insert_batch('pcms_eng_activity_design', $data);
	}
}
/*
	End Model Auth_mod
*/