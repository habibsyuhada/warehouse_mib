<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity_mod extends CI_Model {

	public function __construct()
 	{
	  	parent::__construct();
	    
 	}

 	public function activity_new_process_db($data){
		$this->db->insert('pcms_eng_activity', $data);
	}

	public function activity_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('pcms_eng_activity',$data);
	}

	function activity_list($id = null, $where = null, $where_in = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		elseif(isset($where_in)){
			$this->db->where_in('document_no', $where_in);
		}
		else{
			$this->db->where('status_delete', '1');
			$this->db->where('drawing_type', '1');
		}
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->get('pcms_eng_activity');
		return $query->result_array();
	}

	public function activity_detail_new_process_db($data){
		$this->db->insert('pcms_eng_activity_detail', $data);
	}

	public function activity_detail_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('pcms_eng_activity_detail',$data);
	}

	function activity_detail_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		else{
			$this->db->where('status_delete', '1');
		}
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->get('pcms_eng_activity_detail');
		return $query->result_array();
	}

	public function activity_revision_new_process_db($data){
		$this->db->insert('pcms_eng_activity_revision', $data);
	}

	function activity_revision_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		else{
			$this->db->where('status_delete', '1');
		}
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->get('pcms_eng_activity_revision');
		return $query->result_array();
	}

	function activity_new_import_process_db($data) {
		$this->db->insert_batch('pcms_eng_activity', $data);
	}
}
/*
	End Model Auth_mod
*/