<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Auth_mod extends CI_Model {

	 public	function cek_user($data) {
			$query = $this->db->get_where('portal_user_db', $data);
			return $query;
		}		
		
	
	public	function cek_role($id_role) {

		$this->db->select("*"); 
        $this->db->from('portal_permission_rule');
        $this->db->where(array('id_role' => $id_role));
      
        $query = $this->db->get();
        return $query->result();

	}	


	public	function get_slider() {

		$this->db->select("*"); 
        $this->db->from("portal_login_slider");
        $this->db->where("status","1");
      
        $query = $this->db->get();
        return $query->result_array();

	}	
			

	}