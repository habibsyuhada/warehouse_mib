<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Budget_mod extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function all_list($id = NULL, $where = NULL){
		if($id){
			$query = $this->db->where('det.id', $id);
		}
		elseif($where){
			$query = $this->db->where($where);
		}
		else{
			$this->db->where('det.status_delete', 1);
		}
		$this->db->where('bud.nama_pt', $this->user_cookie[12]);
		$this->db->select('det.*, bud.year, bud.dept, dept.name_of_department, cat.account_no, cat.category_name');
		$query = $this->db->join('eproc_budgeting bud', 'bud.id = det.id_budget', 'left outer');
		$query = $this->db->join('portal_department dept', 'dept.id_department = bud.dept', 'left outer');
		$query = $this->db->join('eproc_budgeting_category cat', 'cat.id = det.id_category', 'left outer');
		$query = $this->db->get('eproc_budgeting_detail det');
		return $query->result_array();
	}

	function budget_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		else{
			$this->db->where('status_delete', 1);
		}
		$query = $this->db->get('eproc_budgeting');
		return $query->result_array();
	}

	public function budget_new_process($data){
		$this->db->insert('eproc_budgeting', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	function budget_categoty_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		else{
			$this->db->where('status_delete', 1);
		}
		$query = $this->db->get('eproc_budgeting_category');
		return $query->result_array();
	}

	public function budget_detail_new_process($data){
		$this->db->insert('eproc_budgeting_detail', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function budget_detail_import_process($data){
		$this->db->insert_batch('eproc_budgeting_detail', $data);
	}

	public function budget_detail_edit_process($data, $where){
		$this->db->where($where);
    $this->db->update('eproc_budgeting_detail', $data);
	}

	// Transfer Model
	public function transfer_list($id = NULL, $where = NULL){
		if($id){
			$query = $this->db->where('id', $id);
		}
		elseif($where){
			$query = $this->db->where($where);
		}
		else{
			$this->db->where('status_delete', 1);
		}
		$this->db->where('nama_pt', $this->user_cookie[12]);
		$query = $this->db->get('eproc_budgeting_transfer');
		return $query->result_array();
	}

	public function transfer_detail_new_process($data){
		$this->db->insert('eproc_budgeting_transfer', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function transfer_detail_edit_process($data, $where){
		$this->db->where($where);
    $this->db->update('eproc_budgeting_transfer', $data);
	}

	// Transfer Model
	public function budget_category_list($id = NULL, $where = NULL){
		if($id){
			$query = $this->db->where('id', $id);
		}
		elseif($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_budgeting_category');
		return $query->result_array();
	}

	public function budget_category_new_process($data){
		$this->db->insert('eproc_budgeting_category', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function budget_category_edit_process($data, $where){
		$this->db->where($where);
    $this->db->update('eproc_budgeting_category', $data);
	}
}
/*
	End Model Auth_mod
*/