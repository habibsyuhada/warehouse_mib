<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_mod extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function all_list($id = NULL, $where = NULL){
		if($id){
			$query = $this->db->where('id_department', $id);
		}
		elseif($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('portal_department');
		return $query->result_array();
	}

	public function department_detail_new_process($data){
		$this->db->insert('portal_department', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function department_detail_edit_process($data, $where){
		$this->db->where($where);
    $this->db->update('portal_department', $data);
	}
}
/*
	End Model Auth_mod
*/