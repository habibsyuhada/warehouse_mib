<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Engineering_mod extends CI_Model {

	public function __construct()
 	{
  	parent::__construct();
    
    
 	}

 	public function draw_new_process_db($data){
		$this->qcs->insert('eng_drawing', $data);
		//user history log
           helper_log("add", "Add table eng_drawing ".$data["drawing_no"]);
        //user history log
	}

	public function draw_get_db($where = NULL,$where_in = NULL,$project_id = NULL){

		//print_r($where_in);
		//return false;

		if($where){
			$query = $this->qcs->where($where);
		}
		if($where_in){
			$query = $this->qcs->where_in('drawing_no', $where_in);
		}
		if($project_id){
			$query = $this->qcs->where('project_id', $project_id);
		}
		$query = $this->qcs->get('eng_drawing');
		return $query->result_array();
	}

	public function draw_edit_process_db($data, $where){
		$this->qcs->where($where);
		$this->qcs->update('eng_drawing',$data);

		 //user history log
           helper_log("update", "Update table eng_drawing ".$data["drawing_no"]);
        //user history log
	}

	public function piecemark_new_process_db($data){
		$this->qcs->insert('eng_drawing_piece_mark', $data);

		//user history log
           helper_log("add", "Add to table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
	}

	public function peacemark_get_db($where = NULL){
		if($where){
			$query = $this->qcs->where($where);
		}
		$query = $this->qcs->get('eng_drawing_piece_mark');
		return $query->result_array();
	}

	public function peacemark_edit_process_db($data, $where){
		$this->qcs->where($where);
		$this->qcs->update('eng_drawing_piece_mark',$data);
		//user history log
           helper_log("update", "Update table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
	}

	public function joint_new_process_db($data){
		$this->qcs->insert('eng_drawing_joint', $data);
		//user history log
           helper_log("add", "Add table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
	}

	public function joint_get_db($where = NULL){
		if($where){
			$query = $this->qcs->where($where);
		}
		$query = $this->qcs->get('eng_drawing_joint');
		return $query->result_array();
	}

	public function joint_edit_process_db($data, $where){
		$this->qcs->where($where);
		$this->qcs->update('eng_drawing_joint',$data);

		//user history log
           helper_log("update", "Update table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
	}

	public function revision_get_db($where = NULL){
		if($where){
			$query = $this->qcs->where($where);
		}
		$query = $this->qcs->order_by('rev_date', 'DESC');
		$query = $this->qcs->get('eng_drawing_rev');
		return $query->result_array();
	}

	public function revision_new_process_db($data){
		$this->qcs->insert('eng_drawing_rev', $data);
		//user history log
           helper_log("add", "Add table eng_drawing_rev Drawing no :".$data["drawing_no"]);
        //user history log
	}

	function draw_new_import_process_db($data) {
		$this->qcs->insert_batch('eng_drawing', $data);
		//user history log
           //helper_log("add", "Import table eng_drawing Drawing no :".$data["drawing_no"]);
        //user history log
	}

	function peacemark_new_import_process_db($data) {
		$this->qcs->insert_batch('eng_drawing_piece_mark', $data);
		//user history log
           helper_log("add", "Import table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
	}

	function joint_new_import_process_db($data) {
		$this->qcs->insert_batch('eng_drawing_joint', $data);
		//user history log
           helper_log("add", "Import table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
	}

	function draw_report_peacemark_db($where) {
		$this->qcs->select('a.type, a.received_date, a.sheet_no, a.rev_no, a.discipline, a.module, a.timestamp, a.material_class, a.area, a.descriptions,b.*');
		$this->qcs->from('eng_drawing a');
		$this->qcs->join('eng_drawing_piece_mark b', 'a.drawing_no = b.drawing_no');
		if($where){
			$query = $this->qcs->where($where);
		}

		$query = $this->qcs->get(); 
		return $query->result_array();
	}

	function draw_report_joint_db($where) {
		$this->qcs->select('a.type, a.received_date, a.sheet_no, a.rev_no, a.discipline, a.module, a.timestamp, a.material_class, a.area, a.descriptions,b.*');
		$this->qcs->from('eng_drawing a');
		$this->qcs->join('eng_drawing_joint b', 'a.drawing_no = b.drawing_no');
		if($where){
			$query = $this->qcs->where($where);
		}

		$query = $this->qcs->get(); 
		return $query->result_array();
	}

	// MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION//


	// ----------- Try Datatables ------ //

	// ----------- Try Datatables ------ //

	// MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
    var $table_drawing_list_dt		 	= 'eng_drawing';
    var $column_order_drawing_list_dt  	= array('drawing_no', 'sheet_no', 'module', 'discipline', 'material_class','id');
    var $column_search_drawing_list_dt 	= array('drawing_no', 'sheet_no', 'module', 'discipline', 'material_class','id');
    var $order_drawing_list_dt 		 	= array('id' => 'desc'); // default order 

    private function _get_datatables_drawing_list_dt_query($project_id)
    {
        
        $this->qcs->from($this->table_drawing_list_dt);
        $this->qcs->where("project_id = '$project_id'");
        //$this->qcs->where('date(time_in) !=',"0000-00-00 00:00:00");
               
        $i = 0;
        
        foreach ($this->column_search_drawing_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->qcs->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->qcs->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->qcs->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_drawing_list_dt) - 1 == $i) //last loop
                    $this->qcs->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
        	$this->qcs->order_by($this->column_order_drawing_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_drawing_list_dt))
        {
            $order = $this->order_drawing_list_dt;
            $this->qcs->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_drawing_list_dt($project_id)
    {
        $this->_get_datatables_drawing_list_dt_query($project_id);

        if ($_POST['length'] != -1)
        $this->qcs->limit($_POST['length'], $_POST['start']);

        $query = $this->qcs->get();
        return $query->result();
    }

    public function count_all_drawing_list_dt($project_id)
    {
        $this->qcs->from($this->table_drawing_list_dt);
       	return $this->qcs->count_all_results();
    }

    function count_filtered_drawing_list_dt($project_id)
    {
        $this->_get_datatables_drawing_list_dt_query($project_id);
       	$query = $this->qcs->get();
        return $query->num_rows();
    }

    
   
	// MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//


	// ----------- Try Datatables ------ //
	

    public function getWeldtypeData()
    {
        $query = $this->qcs->get('master_weld_type');
        return $query->result();
    }
     public function getJointTypeData()
    {
        $query = $this->qcs->get('master_joint_type');
        return $query->result();
    }
    public function getClassData()
    {
        $query = $this->qcs->get('master_class');
        return $query->result_array();
    }
      


}
/*
	End Model Auth_mod
*/