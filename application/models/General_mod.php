<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_mod extends CI_Model {

	public function __construct()
 	{
	  	parent::__construct();
	    
	    
 	}

 	function dept_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		else{
			$this->db->where('status', 1);
		}
		$query = $this->db->get('portal_department');
		return $query->result_array();
	}

	function dept_list_all($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		$query = $this->db->get('portal_department');
		return $query->result_array();
	}

	function role_list($id = null, $where = null){
		if(isset($where)){
			$this->db->where($where);
		}
		elseif(isset($id)){
			$this->db->where('id', $id);
		}
		$query = $this->db->get('portal_role');
		return $query->result_array();
	}

 	function check_cookies($id_user,$permission_cookie,$link){		
		$this->db2->where('id_user', $id_user);
		$query = $this->db2->get('portal_user_db')->result();

		$db_cookies = $query[0]->warehouse_permission;
		
		if($db_cookies !== $permission_cookie){
			redirect($link."/auth/logout");
		}
		
	}
	
	function eng_module_get_db($id = null){
		if(isset($id)){
			$this->qcs->where('mod_id', $id);
		}
		$query = $this->qcs->get('master_module');
		return $query->result_array();
	}

	function eng_discipline_get_db($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status', '1');
		$query = $this->qcs->get('master_discipline');
		return $query->result_array();
	}

	function eng_spec_category_get_db($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status', '1');
		$query = $this->qcs->get('master_spec_category');
		return $query->result_array();
	}

	function eng_inspection_item_get_db($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_inspection_item');
		return $query->result_array();
	}

	function eng_area_get_db($where = NULL){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$this->qcs->where('status', '1');
		$query = $this->qcs->get('master_area');
		return $query->result_array();
	}

	function eng_equipment_get_db($where = NULL){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_equipment');
		return $query->result_array();
	}

	function eng_location_get_db($where = NULL){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$this->qcs->where('status', '1');
		$query = $this->qcs->get('master_location');
		return $query->result_array();
	}

	function get_all_user(){
		$query = $this->db->get('portal_user_db');
		return $query->result_array();
	}

	function portal_user_get_db($ids = null){
		if(isset($ids)){
		$this->db2->where_in('id_user', $ids);
		}
		$this->db2->where('status_user', '1');
		$query = $this->db2->get('portal_user_db');
		return $query->result_array();
	}

	function search_badge($badge){
		$this->db2->where('status_user', '1');
    	$this->db2->like('badge_no', $badge);
    	$this->db2->order_by('badge_no', 'ASC');
    	$this->db2->limit(20);
    	return $this->db2->get('portal_user_db')->result_array();
	}

	function material_class_get_db($project_id = null,$id = null){
		// $this->qcs->where('status', '1');
		if($project_id){
		   $this->qcs->where('project_id', $project_id);
		}
		
		if(isset($id)){
		   $this->qcs->where('id', $id);
		}

		$query = $this->qcs->get('master_material_class');
		return $query->result_array();
	}

	function material_grade_get_db($id = NULL, $project_id = null){
		
		if($id){
			$this->qcs->where('id', $id);
		}

		if($project_id){

			$query1 = $this->qcs->query("SELECT id FROM master_material_class WHERE project_id = '$project_id'");
  			$query1_result = $query1->result();
  			$view_class_id = array();
  			foreach($query1_result as $row){
     			$view_class_id[] = $row->id;
   			}
 		
 			$view_id 	= implode(",",$view_class_id);
  			$ids 		= explode(",", $view_id);

  			//print_r($ids);

  			$this->qcs->where_in('material_class', $ids);

		}

		$query = $this->qcs->get('master_material_grade');
		return $query->result_array();
	}

	function read_portal_user($id){
		$this->db2->where('id_user',$id);
		$query = $this->db2->get('portal_user_db');
		return $query->result_array();
	}

	function read_initial_master_discipline($id){
		$this->qcs->where('initial', $id);
		$query = $this->qcs->get('master_discipline');
		return $query->result_array();
	}

	function read_id_master_discipline($id){
		$this->qcs->where('id', $id);
		$query = $this->qcs->get('master_discipline');
		return $query->result_array();
	}


	function read_project_name($id){
		$this->db2->where('id', $id);
		$query = $this->db2->get('portal_project');
		return $query->result_array();
	}

	function data_project($where = NULL){
		if($where){
			$query = $this->db2->where($where);
		}

		$query = $this->db2->get('portal_project');
		return $query->result_array();
	}

	//FOR LOCATION
	function data_location($where = NULL){
		if($where){
			$query = $this->qcs->where($where);
		}

		$query = $this->qcs->get('master_location');
		return $query->result_array();
	}

	//FOR DISCIPLINE
	function data_discipline($where = null){
		if($where){
			$query = $this->qcs->where($where);
		}

 		$query = $this->qcs->get('master_discipline');
		return $query->result_array();
	}

	function data_module($where = null){
		if($where){
			$query = $this->qcs->where($where);
		}

 		$query = $this->qcs->get('master_module');
		return $query->result_array();
	}

	//FOR SPOOL NO
	function data_spool($where = null){
		if($where){
			$query = $this->qcs->where($where);
		}

 		$query = $this->qcs->get('master_spool_no');
		return $query->result_array();
	}

	function data_certification($where = null){
		if($where){
			$query = $this->db->where($where);
		}

 		$query = $this->db->get('eproc_master_certification');
		return $query->result_array();
	}


	function data_wps($where = null){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$query = $this->qcs->get('master_wps');
		return $query->result_array();
	}

	function data_welder($where = null, $where_in = null){
		if(isset($where)){
			$this->qcs->where($where);
		}
		if(isset($where_in)){
			$this->qcs->where_in('id', $where_in);
		}
		$query = $this->qcs->get('qcs_welder');
		return $query->result_array();
	}

	function data_area($where = null){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$query = $this->qcs->get('master_area');
		return $query->result_array();
	}

	function data_spec_category($where = null){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$query = $this->qcs->get('master_spec_category');
		return $query->result_array();
	}

	function data_material_grade($where = null){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$query = $this->qcs->get('master_material_grade');
		return $query->result_array();
	}

	function data_material_grade_join($where = null){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$query = $this->qcs->select('*, mg.id as mg_id, mc.material_class as name_material_class');
		$query = $this->qcs->join('master_material_class mc','mc.id = mg.material_class');
		$query = $this->qcs->get('master_material_grade mg');
		return $query->result_array();
	}

	function data_material_class($where = null){
		if(isset($where)){
			$this->qcs->where($where);
		}
		$query = $this->qcs->get('master_material_class');
		return $query->result_array();
	}

	function getApprovalLog($id=null,$module=null){
		$query = $this->qcs->where("approved_request_no",$id);
		$query = $this->qcs->where("approved_category",$module);
		$query = $this->qcs->get('qcs_approval_nos');
		return $query->result_array();
	}


	function get_dicipline_name(){
		$this->qcs->where('status_delete', '1');
		$query = $this->qcs->get('master_discipline');
		return $query->result_array();
	}

	function get_inspection_item(){
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_inspection_item');
		return $query->result_array();
	}

	function get_module_name(){
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_module');
		return $query->result_array();
	}

	function get_material_type(){
		$this->qcs->where('status_delete', '1');
		$query = $this->qcs->get('master_spec_category');
		return $query->result_array();
	}

	function get_area_name(){
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_area');
		return $query->result_array();
	}

	function get_equipment_name(){
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_equipment');
		return $query->result_array();
	}


	function get_qualification($id = NULL){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_qualification');
		return $query->result_array();
	}


	function get_surface_condition_all(){
		$query = $this->qcs->get('master_surface_condition');
		return $query->result_array();
	}

	function get_equipment_name_all(){
		$query = $this->qcs->get('master_equipment');
		return $query->result_array();
	}

	function get_data_welder_ref($where){

		//print_r($where);

		$this->qcs->where("id IN ('$where')");
		$query = $this->qcs->get('qcs_welder');
		return $query->result_array();
	}

	function get_method($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_method');
		return $query->result_array();
	}

	function get_temperature($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_temperature');
		return $query->result_array();
	}
	
	function get_contrast_medium($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_contrast_medium');
		return $query->result_array();
	}

	function get_testing_medium($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_testing_medium');
		return $query->result_array();
	}

	function get_flaw_type($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_flaw_type');
		return $query->result_array();
	}

	function get_joint_details($joint_no,$drawing_no){
		$this->qcs->select("weld_map,spool_no");
		$this->qcs->where("joint_no",$joint_no);
		$this->qcs->where("drawing_no",$drawing_no);
		$query = $this->qcs->get('eng_drawing_joint');
		return $query->result_array();
	}

	function get_surface_condition($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_surface_condition');
		return $query->result_array();
	}

	function get_brand_of_chemical($id = null){
		if(isset($id)){
			$this->qcs->where('id', $id);
		}
		$this->qcs->where('status_delete', '0');
		$query = $this->qcs->get('master_brand_of_chemical');
		return $query->result_array();
	}

	function get_procedure_no($project_id){
	
			$this->qcs->where('project_id', $project_id);
			$this->qcs->order_by('id',"DESC");
			$this->qcs->limit('1');
		
		
		$query = $this->qcs->get('master_procedure_no');
		return $query->result_array();
	}

	function get_acceptance_criteria($project_id){
	
			$this->qcs->where('project_id', $project_id);
			$this->qcs->order_by('id',"DESC");
			$this->qcs->limit('1');
		
		
		$query = $this->qcs->get('master_acceptance_criteria');
		return $query->result_array();
	}

	function portal_user_db_id($where_in){
		$this->db2->where_in('id_user', $where_in);
		$query = $this->db2->get('portal_user_db');
		return $query->result_array();
	}

	public function manual_query($query){
		$query = $this->db->query($query);
		return $query->result_array();
	}

	function vendor_list($where = NULL){
		if(isset($where)){
			$this->db->where($where);
		}
		$this->db->where('status', 1);
		$query = $this->db->get('eproc_master_vendor');
		return $query->result_array();
	}

	function supply_category_list($where = NULL){
		if(isset($where)){
			$this->db->where($where);
		}
		
		$query = $this->db->get('eproc_mto_cat');
		return $query->result_array();
	}

	function uom_list($where = NULL){
		if(isset($where)){
			$this->db->where($where);
		}
		
		$query = $this->db->get('eproc_uom');
		return $query->result_array();
	}

}
/*
	End Model Auth_mod
*/