<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_mod extends CI_Model {

	public function __construct()
 	{
  	parent::__construct();
    
 	}


    function cek_role($id_role) {

		$this->db2->select("*"); 
        $this->db2->from('permission_rule');
        $this->db2->where("`id_role`", $id_role);
      
        $query = $this->db2->get();
        return $query->result();

	}

	function checkPD_mod(){
		$query = $this->db->query("SELECT authorized_by FROM `emr_table` WHERE status = 1 AND rejected_by = '' AND approved_by != '' AND reviewed_by !='' AND authorized_by =''");
  		return $query;
	}

	function checkHOD_mod(){
		$query = $this->db->query("SELECT approved_by FROM `emr_table` WHERE status = 1 AND rejected_by = '' AND approved_by = '' AND reviewed_by ='' AND authorized_by ='' AND category_emr ='cost_centre'");
  		return $query;
	}

	function checkPM_mod(){
		$query = $this->db->query("SELECT approved_by FROM `emr_table` WHERE status = 1 AND rejected_by = '' AND approved_by = '' AND reviewed_by ='' AND authorized_by ='' AND category_emr ='job_cost'");
  		return $query;
	}

	function checkContract_mod(){
		$query = $this->db->query("SELECT approved_by FROM `emr_table` WHERE status = 1 AND rejected_by = '' AND approved_by != '' AND reviewed_by ='' AND authorized_by ='' AND category_emr ='job_cost'");
  		return $query;
	}
	

	function req_count_status_pd(){
		$query = $this->db->query("SELECT 'data', SUM(IF(authorized_by = '' AND rejected_by = '' AND reviewed_by != '', 1, 0)) as 'Pending', SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1");
  	return $query->result_array();
	}

	function req_count_status_hod($cost_id){
		$query = $this->db->query("SELECT 'data', SUM(IF(approved_by = '' AND rejected_by = '', 1, 0)) as 'Pending', SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 AND project_id IN ('".join("', '",$cost_id)."')");
  	return $query->result_array();
	}


	function req_count_status_pending_hod_11($cost_id){
		$query = $this->db->query("SELECT 'data', SUM(IF(approved_by = '' AND rejected_by = '', 1, 0)) as 'Pending' FROM `emr_table` WHERE status = 1 AND project_id IN ('".join("', '",$cost_id)."')");
  		return $query->result_array();
	}

	function req_count_status_pending_cat_hod_11($cost_id){
		$query = $this->db->query("SELECT category_emr, SUM(IF(approved_by = '' AND rejected_by = '', 1, 0)) as 'Pending' FROM `emr_table` WHERE status = 1 AND project_id IN ('".join("', '",$cost_id)."') GROUP BY category_emr");
  	return $query->result_array();
	}

	function req_count_status_hod_11($cost_id){
		$query = $this->db->query("SELECT 'data', SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 ");
  	return $query->result_array();
	}

	function req_count_status_cat_hod_11($cost_id){
		$query = $this->db->query("SELECT category_emr, project_id as cost_id, SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 GROUP BY category_emr, project_id");
  	return $query->result_array();
	}
	

	function req_count_status_admin(){
		$query = $this->db->query("SELECT 'data', SUM(IF(authorized_by = '' AND rejected_by = '' AND reviewed_by != '', 1, 0)) as 'Pending', SUM(IF(rejected_by = '' AND reviewed_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1");
  	return $query->result_array();
	}

	

	function req_count_status($cost_id){
		$query = $this->db->query("SELECT 'data', SUM(0) as 'Pending', SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 AND project_id IN ('".join("', '",$cost_id)."')");
  	return $query->result_array();
	}

	function req_count_status_contract(){
		$query = $this->db->query("SELECT 'data', SUM(IF(reviewed_by = '' AND rejected_by = '' AND approved_by != '', 1, 0)) as 'Pending', SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1");
  	return $query->result_array();
	}

	function req_count_status_cat_pd(){
		$query = $this->db->query("SELECT category_emr, project_id as cost_id, SUM(IF(authorized_by = '' AND rejected_by = '' AND reviewed_by != '', 1, 0)) as 'Pending', SUM(IF(rejected_by = '' AND reviewed_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 GROUP BY category_emr, project_id");
  	return $query->result_array();
	}

	
	function req_count_status_cat_contract(){
		$query = $this->db->query("SELECT category_emr, project_id as cost_id, SUM(IF(reviewed_by = '' AND rejected_by = '' AND approved_by != '', 1, 0)) as 'Pending', SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 GROUP BY category_emr, project_id");
  	return $query->result_array();
	}

	function req_count_status_cat_hod($cost_id){
		$query = $this->db->query("SELECT category_emr, project_id as cost_id, SUM(IF(approved_by = '' AND rejected_by = '', 1, 0)) as 'Pending', SUM(IF(rejected_by = '' AND approved_by != '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 AND project_id IN ('".join("', '",$cost_id)."') GROUP BY category_emr, project_id");
  	return $query->result_array();
	}



	function req_count_status_cat($cost_id){
		$query = $this->db->query("SELECT category_emr, project_id as cost_id, SUM(0) as 'Pending', SUM(IF(rejected_by = '' AND authorized_by = '', 1, 0)) as 'In-Progress', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by = '', 1, 0)) as 'Approved', SUM(IF(authorized_by != '' AND rejected_by = '' AND completed_by != '', 1, 0)) as 'Completed', SUM(IF(rejected_by != '', 1, 0)) as 'Rejected' FROM `emr_table` WHERE status = 1 AND project_id IN ('".join("', '",$cost_id)."') GROUP BY category_emr, project_id");
  	return $query->result_array();
	}


	function chart_db($where = NULL){
		$query = "(select `emr_db`.`emr_table`.`category_emr` AS `category_emr`,'pd' AS `project_id`,sum(if(((`emr_db`.`emr_table`.`authorized_by` = '') and (`emr_db`.`emr_table`.`reviewed_by` <> '')),1,0)) AS `num` from `emr_db`.`emr_table` where (`emr_db`.`emr_table`.`status` = 1) $where group by `emr_db`.`emr_table`.`category_emr`) 
union 
(select `emr_db`.`emr_table`.`category_emr` AS `category_emr`,`emr_db`.`emr_table`.`project_id` AS `project_id`,sum(if((`emr_db`.`emr_table`.`approved_by` = ''),1,0)) AS `num` from `emr_db`.`emr_table` where ((`emr_db`.`emr_table`.`status` = 1) and (`emr_db`.`emr_table`.`rejected_by` = '')) $where group by `emr_db`.`emr_table`.`category_emr`,`emr_db`.`emr_table`.`project_id`) 
union 
(select `emr_db`.`emr_table`.`category_emr` AS `category_emr`,'review' AS `project_id`,sum(if(((`emr_db`.`emr_table`.`approved_by` <> '') and (`emr_db`.`emr_table`.`reviewed_by` = '')),1,0)) AS `num` from `emr_db`.`emr_table` where ((`emr_db`.`emr_table`.`status` = 1) and (`emr_db`.`emr_table`.`rejected_by` = '') and (`emr_db`.`emr_table`.`category_emr` = 'job_cost')) $where group by `emr_db`.`emr_table`.`category_emr`)";
		$query = $this->db->query($query);
  	return $query->result_array();
	}

	function check_userDraft($id_user){
		$query = $this->db->query("SELECT status FROM `emr_table` WHERE status = 2 AND rejected_by = '' AND request_by='$id_user'");
  		return $query;
	}

	
}
/*
	End Model Auth_mod
*/