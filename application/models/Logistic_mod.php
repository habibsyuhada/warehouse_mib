<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logistic_mod extends CI_Model {

	public function __construct()
 	{
	  	parent::__construct();
	    
	    
 	}

    //RECEIVING MASTER ---------------------------------------
 	function receiving_data($where = NULL){
 		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_receiving');
		return $query->result_array();
 	}

    function receiving_new_process_db($data){
        $this->db->insert('eproc_receiving', $data);
        
        $insert_id = $this->db->insert_id();
        return $insert_id;
        //user history log
           // helper_log("add", "Add table eproc_receiving ".$data["mto_number"]);
        //user history log
    }

    function receiving_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_receiving',$data);

         //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["mto_number"]);
        //user history log
    }

    function receiving_delete_process_db($where){
        $this->db->where($where);
        $this->db->delete('eproc_receiving');

         //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["mto_number"]);
        //user history log
    }
    //--------------------------------------------------------

    //RECEIVING DETAIL ---------------------------------------
    function receiving_detail_data($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by('rec_det_id', 'DESC');
        $query = $this->db->get('eproc_receiving_detail');
        return $query->result_array();
    }

    function receiving_detail_new_process_db($data){
        $this->db->insert('eproc_receiving_detail', $data);
        //user history log
           // helper_log("add", "Add table eproc_receiving ".$data["mto_number"]);
        //user history log
    }

    function receiving_detail_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_receiving_detail',$data);

         //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["mto_number"]);
        //user history log
    }

    function receiving_detail_import_process_db($data) {
        $this->db->insert_batch('eproc_receiving_detail', $data);
        //user history log
           // helper_log("add", "Import table eproc_mto_detail MTO Number :".$data["mto_number"]);
        //user history log
    }
    //--------------------------------------------------------

    //RECEIVING ATTACHMENT ---------------------------------------
    function receiving_attachment_add($data){
        $this->db->insert_batch('eproc_receiving_doc', $data);
        //user history log
           // helper_log("add", "Add table eproc_receiving ".$data["mto_number"]);
        //user history log
    }
    //--------------------------------------------------------

    //AUTOCOMPLETE -------------------------------------------
    function search_po_autocomplete($po){
        $this->db->like('po_number', $po);
        $this->db->group_by('po_number');
        $this->db->order_by('po_number', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_po')->result_array();
    }
    //--------------------------------------------------------


 	//RECEIVING LIST DATATABLE -------------------------------------------
 	var $table_receiving_list_dt		 	= 'eproc_receiving';
    var $column_order_receiving_list_dt  	= array('supply_number', 'vendor','material_package','weight','etd','atd','shipping_complete','remarks');
    var $column_search_receiving_list_dt 	= array('supply_number', 'vendor','material_package','weight','etd','atd','shipping_complete','remarks');
    var $order_receiving_list_dt 		 	= array('receiving_id' => 'desc'); // default order 

    private function _get_datatables_receiving_list_dt_query()
    {
        
        $this->db->from($this->table_receiving_list_dt);
               
        $i = 0;
        
        foreach ($this->column_search_receiving_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_receiving_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
        	$this->db->order_by($this->column_order_receiving_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_receiving_list_dt))
        {
            $order = $this->order_receiving_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_receiving_list_dt()
    {
        $this->_get_datatables_receiving_list_dt_query();

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_receiving_list_dt()
    {
        $this->db->from($this->table_receiving_list_dt);
       	return $this->db->count_all_results();
    }

    function count_filtered_receiving_list_dt()
    {
        $this->_get_datatables_receiving_list_dt_query();
       	$query = $this->db->get();
        return $query->num_rows();
    }
 	//---------------------------------------------------------------------
}