<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Material_catalog_mod extends CI_Model{

    function material_catalog_add($data){
        $this->db->insert('eproc_material_catalog', $data);
        //user history log
           // helper_log("add", "Add table eproc_material_catalog ".$data["description"]);
        //user history log
    }

    function material_catalog_import_process_db($data) {
        $this->db->insert_batch('eproc_material_catalog', $data);
        //user history log
           // helper_log("add", "Import table eproc_material_catalog Short Desc :".$data["short_desc"]);
        //user history log
    }

    public function material_catalog_list($where = NULL)
    {
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_material_catalog');
        return $query->result_array();
    }

    public function get_material_catalog_join($where = NULL)
    {
        if($where){
            $query = $this->db->where($where);
        }

        $query = $this->db->select('*, mc.id as catalog_id');
        $query = $this->db->join('eproc_budgeting_category cc', 'cc.id = mc.catalog_category_id');
        $query = $this->db->get('eproc_material_catalog mc');
        return $query->result_array();
    }
      
    public function getById($id)
    {
        return $this->db->get_where($this->_table, array("id" => $id))->row();
    }

    public function material_catalog_edit($data, $where)
    {
        $this->db->where($where);
        $this->db->update('eproc_material_catalog',$data);

         //user history log
           helper_log("update", "Update table eproc_material_catalog ".$data["material"]);
        //user history log
    }

    public function delete($id)
    {
        $status_delete = 1;
        //user history log
           helper_log("delete", "Update Delete Status Master Area data id ".$id." Status to ".$status_delete);
        //user history log
        
       return $this->db->update($this->_table, array("status_delete" => $status_delete), array("id" => $id));
    }

    function get_last_code(){
        $this->db->select('*');
        $this->db->from('eproc_material_catalog');
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        return $this->db->get()->result();
    }

    public function data_catalog_category($where = NULL)
    {
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_budgeting_category');
        return $query->result_array();
    }

    //RECEIVING LIST DATATABLE -------------------------------------------
    var $table_material_catalog_list_dt            = 'eproc_material_catalog mc, eproc_budgeting_category cc ';
    var $column_order_material_catalog_list_dt     = array('mc.code_material', 'cc.catalog_category', 'mc.material','mg.material_grade','mmc.material_class');
    var $column_search_material_catalog_list_dt    = array('mc.code_material', 'cc.catalog_category', 'mc.material','mg.material_grade','mmc.material_class');
    var $order_material_catalog_list_dt            = array('mc.id' => 'desc'); // default order 

    private function _get_datatables_material_catalog_list_dt_query()
    {
        $this->db->select('*, mc.id as catalog_id');
        $this->db->from('eproc_material_catalog mc');
        $this->db->join('eproc_budgeting_category cc', 'mc.catalog_category_id = cc.id');
        $this->db->order_by("mc.id", DESC);
        $this->db->group_by("mc.id");
               
        $i = 0;
        
        foreach ($this->column_search_material_catalog_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_material_catalog_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_material_catalog_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_material_catalog_list_dt))
        {
            $order = $this->order_material_catalog_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_material_catalog_list_dt()
    {
        $this->_get_datatables_material_catalog_list_dt_query();

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_material_catalog_list_dt()
    {
        $this->db->from($this->table_material_catalog_list_dt);
        return $this->db->count_all_results();
    }

    function count_filtered_material_catalog_list_dt()
    {
        $this->_get_datatables_material_catalog_list_dt_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    //---------------------------------------------------------------------
}