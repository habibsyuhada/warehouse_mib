<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mb_mod extends CI_Model {

	public function __construct(){
		parent::__construct();
		
		

		$this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
		$this->user_cookie 		 = explode(";",$this->input->cookie('portal_user'));
	}

	public function mb_list($where = NULL){
		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_mat_bal');
		return $query->result_array();
	}

	function mb_edit_process($data, $where){
		$this->db->where($where);
    $this->db->update('eproc_mat_bal', $data);
	}

	public function mb_list_datatables($cat){
		$table	  = 'eproc_mat_bal';
		$column	 = array('', 'unique_no', '', '', 'bal_qty');
		$csearch	 = array('unique_no','bal_qty');

		if($cat == 'count_all'){
			$this->db->from($table);
			return $this->db->count_all_results();
		}

		$this->db->from($table);
		
		$i = 0;
		
		foreach ($csearch as $item) // loop column 
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{
				
				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				
				if (count($csearch) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if (isset($column))
		{
			$this->db->order_by('mb_id', 'desc');
			// $order = $column;
			// $this->db->order_by(key($order), $order[key($order)]);
		}

		if($cat == 'data'){
			if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);

			$query = $this->db->get();
			return $query->result();
		}
		elseif($cat == 'count_filter'){
			$query = $this->db->get();
			return $query->num_rows();
		}
		
	}

	public function tr_list_datatables($cat){
		$table	  = 'eproc_transaction';
		$column	 = array('', 'unique_no', 'qty','tr_status','tr_by','tr_date','remarks');
		$column_search	 = array('unique_no', 'qty','tr_status','tr_by','tr_date','remarks');


		if($cat == 'count_all'){
			$this->db->from($table);
			return $this->db->count_all_results();
		}

		$this->db->from($table);
		
		$i = 0;
		
		foreach ($column_search as $item) // loop column 
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{
				
				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				
				if (count($column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if (isset($column))
		{
			$this->db->order_by('tr_id', 'desc');
			// $order = $column;
			// $this->db->order_by(key($order), $order[key($order)]);
		}

		if($cat == 'data'){
			if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);

			$query = $this->db->get();
			return $query->result();
		}
		elseif($cat == 'count_filter'){
			$query = $this->db->get();
			return $query->num_rows();
		}
		
	}

	public function msr_list_datatables($cat, $project_id = NULL, $supply_type = NULL){
		$table	  = 'eproc_transaction';
		$column	 = array('bal.unique_no','pro.project_name','mrir.po_number','det.date_receiving','dis.discipline_name','cat_cat.catalog_category','cat.code_material','cat.material','cat.length_m','cat.width_m','cat.thk_mm','cat.od','cat.sch','det.spec','det.spec_category','det.plate_or_tag_no','det.heat_or_series_no','det.mill_cert_no','det.brand','det.do_or_pl_no','bal.rec_qty','bal.rec_length','bal.osd_over_qty','bal.osd_shortage_qty','bal.osd_damage_qty','bal.ret_vendor','bal.iss_qty','bal.iss_length','bal.fab_qty','bal.fab_length','bal.bal_qty','bal.bal_length','bal.remarks','loc.location_name','area.area_name','bal.position','bal.mb_id','det.category');


		if($cat == 'count_all'){
			$this->db->from($table);
			return $this->db->count_all_results();
		}

		// $this->db->from($table);
		$this->db->select('bal.unique_no, pro.project_name, mrir.po_number, det.date_receiving, dis.discipline_name, cat_cat.catalog_category, cat.code_material, cat.material, cat.length_m, cat.width_m, cat.thk_mm, cat.od, cat.sch, det.spec, det.spec_category, det.plate_or_tag_no, det.heat_or_series_no, det.mill_cert_no, det.brand, det.do_or_pl_no, bal.rec_qty, bal.rec_length, bal.osd_over_qty, bal.osd_shortage_qty, bal.osd_damage_qty, bal.ret_vendor, bal.iss_qty, bal.iss_length, bal.fab_qty, bal.fab_length, bal.bal_qty, bal.bal_length, bal.remarks, loc.location_name, area.area_name, bal.position, bal.mb_id, det.category');
    $this->db->from('eproc_mat_bal bal');
    $this->db->join('eproc_mrir_material det', 'bal.unique_no = det.unique_ident_no');
    $this->db->join('eproc_mrir mrir', 'det.report_no = mrir.report_no');
    $this->db->join('eproc_material_catalog cat', 'cat.id = det.catalog_id');
    $this->db->join('eproc_catalog_category cat_cat', 'cat.catalog_category_id = cat_cat.id');
    $this->db->join('portal_database.portal_project pro', 'pro.id = bal.project_id');
    $this->db->join('qcs_database.master_discipline dis', 'dis.id = det.discipline');
    $this->db->join('qcs_database.master_location loc', 'loc.id = bal.location', 'left outer');
    $this->db->join('qcs_database.master_area area', 'area.id = bal.area', 'left outer');
		
		$i = 0;

		if($supply_type){
			$this->db->where("det.category", $supply_type);
		}

		if($this->permission_cookie[64] == 1){

			if($project_id){
				$this->db->like("bal.project_id", $project_id);
			}

		} else {

			$this->db->where("bal.project_id", $this->user_cookie[10]);

		}
		
		foreach ($column as $item) // loop column 
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{
				
				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				
				if (count($column) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		}
		else if (isset($column))
		{
			$this->db->order_by('bal.timestamp', 'desc');
			// $order = $column;
			// $this->db->order_by(key($order), $order[key($order)]);
		}

		if($cat == 'data'){
			if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);

			$query = $this->db->get();
			return $query->result();
		}
		elseif($cat == 'count_filter'){
			$query = $this->db->get();
			return $query->num_rows();
		}
		
	}

	public function get_additional_info($uniq_no){

		$uniq_no_imp = implode("','",$uniq_no);
		$uniq_id_impld = "'".$uniq_no_imp."'";

		//print_r($uniq_id_impld);
		//return false;

		$query = $this->db->query("SELECT c.material, a.project_id, a.vendor, b.po_item, b.uniq_no, b.area, b.location FROM eproc_material_catalog c JOIN eproc_receiving_detail b ON c.id = b.catalog_id JOIN eproc_receiving a ON a.receiving_id = b.receiving_id WHERE b.uniq_no IN ($uniq_id_impld)");
		return $query->result();
	}
}
/*
	End Model Auth_mod
*/