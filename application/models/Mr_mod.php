<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mr_mod extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->user_cookie       = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));
    }
   
    public function mr_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mr');
        return $query->result_array();
    }

    public function mr_list_detail($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }

    public function mr_quotation_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mr_quotation');
        return $query->result_array();
    }

     public function mr_tabulation_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mr_tabulation');
        return $query->result_array();
    }


    public function get_user_data(){
        $query = $this->db->where("status_user","1");
        $query = $this->db->get('portal_user_db');
        return $query->result_array();
    }

     public function get_user_data_sign($id_user){
        $query = $this->db->where("id_user",$id_user);
        $query = $this->db->get('portal_user_db');
        return $query->result_array();
    }

    public function generate_batch_no(){
        $query = $this->db->order_by('mr_number', 'DESC');
        $query = $this->db->limit("1");
        $query = $this->db->get('eproc_mr');
        
        $query1_result = $query->result_array();

        if($query1_result){
            $batch_no_gen = str_pad($query1_result[0]["mr_number"] + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $batch_no_gen = "000001";
        }

        return $batch_no_gen;

    }

    public function material_category_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_material_catalog');
        return $query->result_array();
    }

    public function mr_material_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }


    public function mr_detail_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mr_detail',$data);

         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function update_mr_data($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mr',$data);

         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function insert_mr_transaction($data){
        $this->db->insert('eproc_transaction', $data);
    }

    
    function get_last_mr_number(){
        $this->db->select('*');
        $this->db->from('eproc_mr');
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        return $this->db->get()->result();
    }

    


     function get_data_catalog($catalog_id = null){
        if(isset($catalog_id)){
            $this->db->where('id', $catalog_id);
        }
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function get_bal_unique($uniq_no){
        $this->db->where('unique_no', $uniq_no);
        $this->db->order_by('unique_no', 'ASC');
        return $this->db->get('eproc_mat_bal')->result_array();
    }


    function search_material_catalog_autocomplete($material_catalog){
        $this->db->like('short_desc', $material_catalog);
        $this->db->order_by('short_desc', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function insert_mr_master($data){
        $this->db->insert("eproc_mr", $data);
        return $this->db->insert_id();
    }


    function insert_mr_detail($data){
        $this->db->insert("eproc_mr_detail", $data);
        return $this->db->insert_id();
    }

    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION//


    // ----------- Try Datatables ------ //

    // ----------- Try Datatables ------ //

    // MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
   
    var $column_order_mr_list_dt   = array('mr.budget_cat_id','mr.budget_dept_id','mr.mr_number','mr.created_by','mr.created_date');
    var $column_search_mr_list_dt  = array('mr.budget_cat_id','mr.budget_dept_id','mr.mr_number','mr.created_by','mr.created_date');
    var $order_mr_list_dt          = array('id_mr' => 'desc'); // default order 

    private function _get_datatables_mr_list_dt_query($status,$pt_id)
    {

        //$query = $this->db->select('mr.budget_cat_id,mr.budget_dept_id,mr.mr_number,mr.request_sche,mr.request_type,mr_status,');
        $query = $this->db->from('eproc_mr mr');
        //$query = $this->db->join('eproc_mr_detail emd', 'mr.mr_number = emd.mr_number', 'left outer');
        $query = $this->db->where('mr.status',$status);
        $query = $this->db->where('mr.pt_id',$pt_id);

        if($this->permission_cookie[81] != 1){

            $query = $this->db->where('mr.budget_dept_id',$this->user_cookie[4]);

        }
        
        $i = 0;
        
        foreach ($this->column_search_mr_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_mr_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_mr_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_mr_list_dt))
        {
            $order = $this->order_mr_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_mr_list_dt($status,$pt_id)
    {
        $this->_get_datatables_mr_list_dt_query($status,$pt_id);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_mr_list_dt($status,$pt_id)
    {
        $query = $this->db->from('eproc_mr mr');
        return $this->db->count_all_results();
    }

    function count_filtered_mr_list_dt($status,$pt_id)
    {
        $this->_get_datatables_mr_list_dt_query($status,$pt_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
   
    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//

    // ----------- Try Datatables ------ //
    
   




       
    function get_total_mr($where = NULL){
        if($where){
            $query = $this->db->where("mto_id",$where);
        }
        
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }

     public function mr_new_process_db($data){
        $this->db->insert('eproc_mr', $data);
        //user history log
           ///helper_log("add", "Add table eproc_mr ".$data["mr_number"]);
        //user history log
    }

    public function mr_detail_new_process_db($data){
        $this->db->insert('eproc_mr_detail', $data);
        //user history log
           //helper_log("add", "Add table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function mr_quo_new_process_db($data){
        $this->db->insert('eproc_mr_quotation', $data);
        //user history log
           //helper_log("add", "Add table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function mr_tab_new_process_db($data){
        $this->db->insert('eproc_mr_tabulation', $data);
        //user history log
           //helper_log("add", "Add table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }


     public function mr_update_detail_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mr_detail',$data);

         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function mr_update_data_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mr',$data);

         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function get_vendor_list(){
        $query = $this->db->where('status',"1");
        $query = $this->db->get('eproc_master_vendor');
        return $query->result_array();
    }

    public function get_uom_list(){
        $query = $this->db->where('status',"1");
        $query = $this->db->get('eproc_uom');
        return $query->result_array();
    }

    public function get_currency_list(){
        $query = $this->db->where('status',"1");
        $query = $this->db->get('eproc_currency');
        return $query->result_array();
    }

    public function get_data_department($id=null){  
        if(isset($id)){      
            $query = $this->db->where("id_department",$id);
        }
        $query = $this->db->where('status',"1");
        $query = $this->db->get('portal_department');
        return $query->result_array();
    }

    public function get_data_category(){        
        $query = $this->db->where('status_delete',"1");
        $query = $this->db->get('eproc_budgeting_category');
        return $query->result_array();
    }

    public function get_data_budgeting($id=null,$where=null){  
       

        $query = $this->db->from('eproc_budgeting_detail ebd');
        $query = $this->db->join('eproc_budgeting eb', 'ebd.id_budget = eb.id', 'left outer');
        $query = $this->db->join('eproc_budgeting_category ebc', 'ebc.id = ebd.id_category', 'left outer');
        if(isset($id)){      
            $query = $this->db->where("eb.dept",$id);
        }
        if(isset($where)){      
            $query = $this->db->where($where);
        }
        $query = $this->db->group_by("ebc.category_name");
        $query = $this->db->get();
        return $query->result_array();

    }

     public function get_budget_amount($id=null,$where=null){  
       
        $query = $this->db->select('SUM(ebd.budget) as total');
         $query = $this->db->from('eproc_budgeting_detail ebd');
        $query = $this->db->join('eproc_budgeting eb', 'ebd.id_budget = eb.id', 'left outer');
        $query = $this->db->join('eproc_budgeting_category ebc', 'ebc.id = ebd.id_category', 'left outer');
        if(isset($id)){      
            $query = $this->db->where("eb.dept",$id);
        }
        if(isset($where)){      
            $query = $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->result_array();
        
    }

    public function get_transfered_amount($id=null,$where=null){  
       
        $query = $this->db->select('SUM(budget) as total');
        $query = $this->db->from('eproc_budgeting_transfer');
       
        if(isset($id)){      
            $query = $this->db->where("to_dept",$id);
        }
        if(isset($where)){      
            $query = $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->result_array();
        
    }

    public function get_utilized_amount($where=null){  
       
        $query = $this->db->select('SUM(grand_total) as total');
        $query = $this->db->from('eproc_po');
      
        if(isset($where)){      
            $query = $this->db->where($where);
        }
        
        $query = $this->db->get();
        return $query->result_array();
        
    }

    function search_unique_autocomplete($uniq_no){
   
            $query = $this->db->select("*"); 
            $query = $this->db->from('eproc_material_catalog emc');
            $query = $this->db->join('eproc_catalog_category ecc', 'emc.catalog_category_id = ecc.id');
            $query = $this->db->like("emc.code_material",$uniq_no);
          
            $query = $this->db->get();
            return $query->result_array();
    }

    function get_data_catalog_check($uniq_no){
   
            $query = $this->db->select("*"); 
            $query = $this->db->from('eproc_material_catalog emc');
            $query = $this->db->join('eproc_catalog_category ecc', 'emc.catalog_category_id = ecc.id', 'left outer');
            $query = $this->db->where("emc.code_material",$uniq_no);
          
            $query = $this->db->get();
            return $query->result_array();
    }

  

    function delete_quotation($id_quotation){
        $this->db->where("id_quotation",$id_quotation);
        $this->db->delete("eproc_mr_quotation");
    }

    function delete_tabulation($id_tabulation){
        $this->db->where("id_tabulation",$id_tabulation);
        $this->db->delete("eproc_mr_tabulation");
    }

    function delete_material_id($id){
        $this->db->where("id",$id);
        $this->db->delete("eproc_mr_detail");
    }


    function get_status_procurement($where = NULL){
        
        $query = $this->db->where("mr_number",$where);       
        $query = $this->db->where("vendor_winner","0");       
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }

    function get_status_tabulation($where = NULL){
        
        $query = $this->db->where("mr_number",$where);     
        $query = $this->db->get('eproc_mr_tabulation');
        return $query->result_array();
    }

    function get_status_quotation($where = NULL){
        
        $query = $this->db->where("mr_number",$where);      
        $query = $this->db->get('eproc_mr_quotation');
        return $query->result_array();
    }


}

/*
    End Model Auth_mod
*/