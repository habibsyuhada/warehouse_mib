<?php

class Mrir_mod extends CI_Model{
	
	public function __construct()
 	{
  		parent::__construct();
 	}


	function search_project_data($project_id){
		$query = $this->db->query("SELECT * FROM portal_project WHERE id = '$project_id'");
		return $query->result_array();
	}

	function search_sign_data($id){
		$query = $this->db->query("SELECT sign_id,full_name,sign_approval FROM portal_user_db WHERE id_user = '$id'");
		return $query->result_array();
	}

	function get_id_for_spec_cat($id){
		$query = $this->db->query("SELECT id FROM master_spec_category WHERE spec_code = '$id'");
		return $query->result_array();
	}

	//ADD MRIR
	function mrir_add($data){
		$this->db->insert("eproc_mrir", $data);
		return $this->db->insert_id();

		//user history log
           // helper_log("update", "Update data eproc_mrir report ID :".$data["report_no"]);
        //user history log
	}

	function mrir_data($param,$project_id){
		$query = $this->db->query("SELECT * FROM eproc_mrir WHERE discipline = '$param' AND project_id ='$project_id' ORDER BY id DESC");
		return $query->result();
	}

	function detail_mrr($report_no){

		//$query = $this->db->query("SELECT * FROM eproc_mrir_material WHERE report_no = '$report_no' AND discipline = '$discipline' ORDER BY id DESC");
		//return $query->result();

		  $this->db->select("*"); 
          $this->db->from('eproc_mrir_material');
          $this->db->join('eproc_mrir_material_document', 'eproc_mrir_material.unique_ident_no = eproc_mrir_material_document.material_id','left');
    	  $this->db->where("eproc_mrir_material.report_no",$report_no);
    	  // $this->db->where("eproc_mrir_material.discipline",$discipline);    	  		
    	  $this->db->group_by("eproc_mrir_material.unique_ident_no");    	  		
          $query = $this->db->get();
       	  return $query->result();
	}

	function check_material($where = NULL,$where_in = NULL){
		if($where){
			$query = $this->db->where($where);
		}
		if($where_in){
			$query = $this->db->where_in('unique_ident_no', $where_in);
		}
		$query = $this->db->get('eproc_mrir_material');
		return $query->result_array();
	}


	//FOR DISCIPLINE ==================================================


	function data_material(){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material');
        return $this->db->get()->result();
	}

	// SPEC CATEGORY
	
	function add_discipline($data){
		$this->db->insert("master_discipline", $data);
		return $this->db->insert_id();
	}

	function get_discipline($id){
		$this->db->select('*');
        $this->db->from('master_discipline');
        $this->db->where('id', $id);
        return $this->db->get()->result();
	}

	function edit_discipline($id, $data){
		$this->db->where('id', $id);
        $this->db->update('master_discipline', $data);
	}

	function edit_status_approval($data, $where = NULL, $where_in = NULL){
		if($where){
			$query = $this->db->where($where);
		}
		if($where_in){
			$query = $this->db->where_in(array_keys($where_in)[0],$where_in[array_keys($where_in)[0]]);
		}
		$this->db->update('eproc_mrir_material',$data);

		//user history log
           // helper_log("update", "Update data eproc_mrir_material");
        //user history log

	}
	//================================================================


	//FOR MATERIAL ===================================================
	function add_material($data){
		$this->db->insert_batch("eproc_mrir_material", $data);
		return $this->db->insert_id();
	}

	function get_material_by_unique($unique_id){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material');
        $this->db->where('unique_ident_no', $unique_id);
        return $this->db->get()->result();
	}

	function get_material_by_report_no($mrir_id){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material');
        $this->db->where('mrir_id', $mrir_id);
        return $this->db->get()->result();
	}

	function edit_material($id, $data){
		$this->db->where('id', $id);
        $this->db->update('eproc_mrir_material', $data);
	}
	//================================================================

	//=======================================================
	function get_mrir_by_status($param){
		$this->db->select('*, mr.status AS mr_status');
        $this->db->from('eproc_mrir mr');
        $this->db->where('mr.status', $param);
        $this->db->order_by('mr.report_no', 'DESC');
        return $this->db->get()->result();
	}

	function get_mrir_by_report_no($mrir_id){
		$this->db->select('*');
        $this->db->from('eproc_mrir');
        $this->db->where('id', $mrir_id);
        return $this->db->get()->result();
	}

	function get_mrir_by_id($id){
		$this->db->select('*');
        $this->db->from('eproc_mrir');
        $this->db->where('id', $id);
        return $this->db->get()->result();
	}

	function get_mrir_list($where = NULL){
		if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by('id', 'DESC');
        $query = $this->db->get('eproc_mrir');
        return $query->result_array();
	}

	function get_mrir_detail_list($where = NULL){
		if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by('id', 'DESC');
        $query = $this->db->get('eproc_mrir_material');
        return $query->result_array();
	}

	function edit_mrir($id, $data){
		$this->db->where('id', $id);
        $this->db->update('eproc_mrir', $data);

        //user history log
           helper_log("update", "Update data eproc_mrir ID :".$id);
        //user history log
	}

	function approve_unapprove_material($id, $data){
		$this->db->where('id', $id);
        $this->db->update('eproc_mrir_material', $data);

        //user history log
           // helper_log("update", "Update data eproc_mrir_material ID :".$id);
        //user history log
	}

	function get_unapprove_material_by_report_no($report_no){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material');
        $this->db->where('report_no', $report_no);
        $this->db->where('status', 1);
        return $this->db->get()->result();
	}

	function get_all_count($mrir_id){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material');
        $this->db->where('mrir_id', $mrir_id);
        return $this->db->get()->result();
	}

	function get_unapprove_material_by_report_nox($mrir_id){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material');
        $this->db->where('mrir_id', $mrir_id);
        $this->db->where('status', 2);
        return $this->db->get()->result();
	}

	function get_approve_material_by_report_no($mrir_id){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material');
        $this->db->where('mrir_id', $mrir_id);
        $this->db->where('status', 3);
        return $this->db->get()->result();
	}


	function update_status_mrir_by_report_no($report_no, $data){	

		$this->db->where('report_no', $report_no);
        $this->db->update('eproc_mrir', $data);

        //user history log
           // helper_log("update", "Process Approval eproc_mrir report_no ID :".$report_no);
        //user history log

          if($data['status'] == 3){
        //user approval_log
           // approval_log("eproc_mrir", "MRIR", $report_no);
        //user approval_log   
          }


	}

	function update_material_to_unapprove($report_no,$data){
		$this->db->where('report_no', $report_no);
		$this->db->where('status', 1);
        $this->db->update('eproc_mrir_material', $data);

        //user history log
           helper_log("update", "Update data eproc_mrir_material report_no ID :".$report_no);
        //user history log
	}

	function update_status_material_to_disposition($report_no,$data){
		$this->db->where('report_no', $report_no);
        $this->db->update('eproc_mrir_material', $data);

        //user history log
           helper_log("update", "Update data eproc_mrir_material report_no ID :".$report_no);
        //user history log
	}

	function update_material_by_report_no($report_no, $data){
		$this->db->where('report_no', $report_no);
        $this->db->update('eproc_mrir_material', $data);

         //user history log
           helper_log("update", "Update data eproc_mrir_material report_no ID :".$report_no);
        //user history log
	}

	function update_material_resubmit($report_no, $data){
		$this->db->where('status', "2");
		$this->db->where('report_no', $report_no);
        $this->db->update('eproc_mrir_material', $data);

         //user history log
           helper_log("update", "Update data eproc_mrir_material report_no ID :".$report_no);
        //user history log
	}

	function import_material_proccess($data) {
		$this->db->insert_batch('eproc_mrir_material', $data);

		//user history log
           helper_log("update", "Import data eproc_mrir_material report_no ID :".$data['report_no']);
        //user history log
	}

	function upload_material_document_proccess($data){
		$this->db->insert('eproc_mrir_material_document', $data);

		//user history log
           helper_log("update", "Import data eproc_mrir_material_document report_no ID :".$data['material_id']);
        //user history log
	}

	function get_document_material_by_unique($unique_id){
		$this->db->select('*');
        $this->db->from('eproc_mrir_material_document');
        $this->db->where('material_id', $unique_id);
        $this->db->order_by('timestamp', 'DESC');
        return $this->db->get()->result();
	}

	function get_mrir_review($doplNumber){
		$this->db->select('eproc_receiving.date_received,eproc_receiving.receiving_id,eproc_receiving.mto_id,eproc_receiving.do_pl,eproc_receiving_detail.uniq_no,eproc_receiving_detail.category,eproc_receiving_detail.po_item,eproc_receiving.vendor,eproc_material_catalog.material,eproc_material_catalog.length,eproc_material_catalog.width_od,eproc_material_catalog.sch_thk,eproc_receiving_detail.spec,eproc_receiving_detail.spec_category,eproc_receiving_detail.type,eproc_receiving_detail.plate_or_tag_no,eproc_receiving_detail.heat_no,eproc_receiving_detail.ceq,eproc_receiving_detail.mill_certificate,eproc_receiving_detail.date_manufacturing,eproc_receiving_detail.country_origin,eproc_rec_qty.qty,eproc_receiving_detail.uom,eproc_receiving_detail.color_code,eproc_receiving_detail.brand,eproc_receiving_detail.country_origin,eproc_receiving_detail.supplier_name');
        $this->db->from('eproc_receiving');
        $this->db->join('eproc_receiving_detail', 'eproc_receiving.receiving_id = eproc_receiving_detail.receiving_id');
        $this->db->join('eproc_rec_qty', 'eproc_receiving_detail.rec_uniq_id = eproc_rec_qty.rec_uniq_id');
        $this->db->join('eproc_material_catalog', 'eproc_receiving_detail.catalog_id = eproc_material_catalog.id');
        $this->db->where('eproc_receiving.do_pl', $doplNumber);
        return $this->db->get()->result_array();
	}

	function get_mto_data($mtoNumber){
		$this->db->select('*');
        $this->db->from('eproc_mto');
        $this->db->where('id', $mtoNumber);
        return $this->db->get()->result_array();
	}



	public function generate_batch_no(){
    $query = $this->db->order_by('report_no', 'DESC');
    $query = $this->db->limit("1");
    $query = $this->db->get('eproc_mrir');
    
    $query1_result = $query->result_array();

    if($query1_result){
        $batch_no_gen = str_pad($query1_result[0]["report_no"] + 1, 6, '0', STR_PAD_LEFT);
    } else {
        $batch_no_gen = "000001";
    }

    return $batch_no_gen;

  }

  public function generate_uniq_no($project_id, $discipline, $category){
    $query = $this->db->order_by('unique_ident_no', 'DESC');
    $query = $this->db->limit("1");
    $query = $this->db->where('project_id', $project_id);
    $query = $this->db->where('discipline', $discipline);
    $query = $this->db->where('category', $category);
    $query = $this->db->get('eproc_mrir_material');
    
    $query1_result = $query->result_array();

    if($query1_result){
    	// echo '<pre>';
    	// print_r($query1_result);
    	// echo '</pre>';
    	$str = $query1_result[0]["unique_ident_no"];
			$str2 = substr($str, -6); // "quick brown fox jumps over the lazy dog."
      // $uniq_no_gen = "SS-".$project_id."-".str_pad($str2 + 1, 6, '0', STR_PAD_LEFT);
      $uniq_no_gen = str_pad($str2 + 1, 6, '0', STR_PAD_LEFT);
    } 
    else{
      // $uniq_no_gen = "SS-".$project_id."-000001";
      $uniq_no_gen = "000001";
    }

    return $uniq_no_gen;
  }

  function insert_master_mrir($data){
    $this->db->insert('eproc_mrir', $data);
	}

  function insert_detail_mrir($data){
    $this->db->insert('eproc_mrir_material', $data);
	}

	function insert_to_transaction($data){
    $this->db->insert('eproc_transaction', $data);
	}

	function get_data_receicing_cs($where = NULL){
    if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_receiving_cs');
		return $query->result_array();
	}

	function receiving_cs_detail_data($where = NULL){
    if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_receiving_cs_detail');
		return $query->result_array();
	}


	function receiving_detail_data($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }

        $query = $this->db->select('*, id AS rd_id, qty AS qty_receiving');
        $query = $this->db->get('eproc_receiving_detail');

        return $query->result_array();
    }

    var $table_mrir_list_dt            = 'eproc_mrir';
    var $column_order_mrir_list_dt     = array('mc.material','rd.country_origin','rd.brand','mv.vendor_name','rd.delivery_condition','rd.spec_grade','rd.spec_category','rd.ceq','rd.qty');
    var $column_search_mrir_list_dt    = array('mc.material','rd.country_origin','rd.brand','mv.vendor_name','rd.delivery_condition','rd.spec_grade','rd.spec_category','rd.ceq','rd.qty');
    var $order_mrir_list_dt            = array('id' => 'desc'); // default order 

    private function _get_datatables_mrir_list_dt_query($param)
    {
        
        $this->db->from('eproc_mrir');
        $this->db->where('status', $param);
               
        $i = 0;
        
        foreach ($this->column_search_mrir_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_mrir_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_mrir_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_mrir_list_dt))
        {
            $order = $this->order_mrir_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_mrir_list_dt($id)
    {
        $this->_get_datatables_mrir_list_dt_query($id);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_mrir_list_dt($param)
    {
        $this->db->from($this->table_mrir_list_dt);
        $this->db->where('status', $param);
        return $this->db->count_all_results();
    }

    function count_filtered_mrir_list_dt($id)
    {
        $this->_get_datatables_mrir_list_dt_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

}
?>