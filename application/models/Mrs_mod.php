<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mrs_mod extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
        
        $this->pcms = $this->load->database('db_pcms', TRUE);
    }

    public function mrs_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mrs');
        return $query->result_array();
    }

    public function mrs_list_detail($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mrs_detail');
        return $query->result_array();
    }

    public function get_location(){
        $query = $this->qcs->where("status_delete","1");
        $query = $this->qcs->get('master_location');
        return $query->result_array();
    }

     public function get_material_class(){
        $query = $this->qcs->where("status_delete","1");
        $query = $this->qcs->get('master_material_class');
        return $query->result_array();
    }

    public function get_project(){
        $query = $this->db2->where("status","1");
        $query = $this->db2->get('portal_project');
        return $query->result_array();
    }

    public function get_user_data(){
        $query = $this->db2->where("status_user","1");
        $query = $this->db2->get('portal_user_db');
        return $query->result_array();
    }

    public function generate_batch_no(){
        $query = $this->db->order_by('mrs_no', 'DESC');
        $query = $this->db->limit("1");
        $query = $this->db->get('eproc_mrs');
        
        $query1_result = $query->result_array();

        if($query1_result){
            $batch_no_gen = str_pad($query1_result[0]["mrs_no"] + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $batch_no_gen = "000001";
        }

        return $batch_no_gen;

    }

    public function material_category_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_material_catalog');
        return $query->result_array();
    }

    public function mrs_material_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mrs_detail');
        return $query->result_array();
    }

    public function mrs_new_process_db($data){
        $this->db->insert('eproc_mrs', $data);
        //user history log
           helper_log("add", "Add table eproc_mrs ".$data["mrs_number"]);
        //user history log
    }

    public function mrs_detail_new_process_db($data){
        $this->db->insert('eproc_mrs_detail', $data);
        //user history log
           helper_log("add", "Add table eproc_mrs_detail ".$data["mrs_number"]);
        //user history log
    }

    function mrs_detail_import_process_db($data) {
        $this->db->insert_batch('eproc_mrs_detail', $data);
        //user history log
           helper_log("add", "Import table eproc_mrs_detail mrs Number :".$data["mrs_number"]);
        //user history log
    }

    public function draw_get_db($where = NULL,$where_in = NULL,$project_id = NULL){

        //print_r($where_in);
        //return false;

        if($where){
            $query = $this->qcs->where($where);
        }
        if($where_in){
            $query = $this->qcs->where_in('drawing_no', $where_in);
            $query = $this->qcs->group_by('drawing_no');
        }
        if($project_id){
            $query = $this->qcs->where('project_id', $project_id);
        }

        $query = $this->qcs->get('eng_drawing');
        return $query->result_array();
    }

    public function mrs_detail_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mrs_detail',$data);

         //user history log
           // helper_log("update", "Update table eproc_mrs_detail ".$data["mrs_number"]);
        //user history log
    }

    public function update_mrs_data($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mrs',$data);

         //user history log
           // helper_log("update", "Update table eproc_mrs_detail ".$data["mrs_number"]);
        //user history log
    }

    public function insert_mrs_transaction($data){
        $this->db->insert('eproc_transaction', $data);
    }

    public function peacemark_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eng_drawing_piece_mark',$data);
        //user history log
           helper_log("update", "Update table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function joint_new_process_db($data){
        $this->db->insert('eng_drawing_joint', $data);
        //user history log
           helper_log("add", "Add table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function joint_get_db($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eng_drawing_joint');
        return $query->result_array();
    }

    public function joint_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eng_drawing_joint',$data);

        //user history log
           helper_log("update", "Update table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function revision_get_db($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by('rev_date', 'DESC');
        $query = $this->db->get('eng_drawing_rev');
        return $query->result_array();
    }

    public function revision_new_process_db($data){
        $this->db->insert('eng_drawing_rev', $data);
        //user history log
           helper_log("add", "Add table eng_drawing_rev Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function draw_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing', $data);
        //user history log
           //helper_log("add", "Import table eng_drawing Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function peacemark_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing_piece_mark', $data);
        //user history log
           helper_log("add", "Import table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function joint_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing_joint', $data);
        //user history log
           helper_log("add", "Import table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function get_last_mrs_number(){
        $this->db->select('*');
        $this->db->from('eproc_mrs');
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        return $this->db->get()->result();
    }

    function search_drawing_autocomplete($drawing){
       
        $this->pcms->from('pcms_eng_activity');
        $this->pcms->like('pcms_eng_activity.document_no', $drawing);
        $this->pcms->where('pcms_eng_activity.drawing_type', "1");
        $query = $this->pcms->get();
        return $query->result_array();
    }

    function searching_mto_drawing_data($drawing){
     
        $this->pcms->from('pcms_eng_activity');
        $this->pcms->where('pcms_eng_activity.document_no', $drawing);
        $this->pcms->where('pcms_eng_activity.drawing_type', "1");
        $this->pcms->limit(1);
        
        $query = $this->pcms->get();
        return $query->result_array();
    }


    function search_unique_autocomplete($uniq_no,$project_id){
   
            $query = $this->db->select("*"); 
            $query = $this->db->from('eproc_mat_bal');
            $query = $this->db->like("eproc_mat_bal.unique_no",$uniq_no);
            $query = $this->db->where("eproc_mat_bal.project_id",$project_id);
            $query = $this->db->order_by("eproc_mat_bal.unique_no","ASC");
          
            $query = $this->db->get();
            return $query->result_array();
    }




    function get_data_unique($uniq_no = null,$project_id = null){
       
          $this->db->select("*"); 
          $this->db->from('eproc_mrir_material');
        
          if(isset($uniq_no)){
               $this->db->where("eproc_mrir_material.unique_ident_no",$uniq_no);
          }

          if(isset($project_id)){
               $this->db->where("eproc_mrir_material.project_id",$project_id);
          }

          $query = $this->db->get();
          return $query->result_array();
   
    }

     function get_data_catalog($catalog_id = null){
        if(isset($catalog_id)){
            $this->db->where('id', $catalog_id);
        }
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function get_bal_unique($uniq_no){
        $this->db->where('unique_no', $uniq_no);
        $this->db->order_by('unique_no', 'ASC');
        return $this->db->get('eproc_mat_bal')->result_array();
    }


    function search_material_catalog_autocomplete($material_catalog){
        $this->db->like('short_desc', $material_catalog);
        $this->db->order_by('short_desc', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function insert_mrs_master($data){
        $this->db->insert("eproc_mrs", $data);
        return $this->db->insert_id();
    }


    function insert_mrs_detail($data){
        $this->db->insert("eproc_mrs_detail", $data);
        return $this->db->insert_id();
    }

    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION//


    // ----------- Try Datatables ------ //

    // ----------- Try Datatables ------ //

    // MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
    var $table_mrs_list_dt          = 'eproc_mrs';
    var $column_order_mrs_list_dt   = array('project_id', 'sub_contractor_name','line_no','drawing_no','equipment_tag_no','mrs_no','created_by','created_date','material_class','id_mrs');
    var $column_search_mrs_list_dt  = array('project_id', 'sub_contractor_name','line_no','drawing_no','equipment_tag_no','mrs_no','created_by','created_date','material_class','id_mrs');
    var $order_mrs_list_dt          = array('id_mrs' => 'desc'); // default order 

    private function _get_datatables_mrs_list_dt_query($req_pages)
    {

        $this->db->where("status",$req_pages);
        $this->db->from($this->table_mrs_list_dt);

        
        $i = 0;
        
        foreach ($this->column_search_mrs_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_mrs_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_mrs_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_mrs_list_dt))
        {
            $order = $this->order_mrs_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_mrs_list_dt($req_pages)
    {
        $this->_get_datatables_mrs_list_dt_query($req_pages);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_mrs_list_dt($req_pages)
    {
        $this->db->from($this->table_mrs_list_dt);
        return $this->db->count_all_results();
    }

    function count_filtered_mrs_list_dt($req_pages)
    {
        $this->_get_datatables_mrs_list_dt_query($req_pages);
        $query = $this->db->get();
        return $query->num_rows();
    }

    
   
    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//


    // ----------- Try Datatables ------ //
    

    public function getWeldtypeData()
    {
        $query = $this->db->get('master_weld_type');
        return $query->result();
    }
     public function getJointTypeData()
    {
        $query = $this->db->get('master_joint_type');
        return $query->result();
    }
    public function getClassData()
    {
        $query = $this->db->get('master_class');
        return $query->result_array();
    }
      
       public function getProjectDetail($project_id)
    {   
        $query = $this->db2->where('id', $project_id);
        $query = $this->db2->get('portal_project');
        return $query->result_array();
    }

    public function getProject()
    {   
        $query = $this->db2->get('portal_project');
        return $query->result_array();
    }

}
/*
    End Model Auth_mod
*/