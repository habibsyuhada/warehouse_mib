<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mto_mod extends CI_Model {

	public function __construct()
 	{
	  	parent::__construct();
	    
	    
 	}

 	public function mto_list($where = NULL){
 		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_mto');
		return $query->result_array();
 	}

 	public function material_category_list($where = NULL){
 		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_material_catalog');
		return $query->result_array();
 	}

 	public function mto_material_list($where = NULL){
 		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->order_by('id', 'DESC');
		$query = $this->db->get('eproc_mto_detail');
		return $query->result_array();
 	}

 	function mto_category_get_db($where = NULL){
		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_mto_cat');
		return $query->result_array();
	}

 	public function mto_new_process_db($data){
		$this->db->insert('eproc_mto', $data);
		//user history log
           helper_log("add", "Add table eproc_mto ".$data["mto_number"]);
        //user history log
	}

	public function mto_detail_new_process_db($data){
		$this->db->insert('eproc_mto_detail', $data);
		//user history log
           helper_log("add", "Add table eproc_mto_detail ".$data["mto_number"]);
        //user history log
	}

	function mto_detail_import_process_db($data) {
        $this->db->insert_batch('eproc_mto_detail', $data);
        //user history log
           helper_log("add", "Import table eproc_mto_detail MTO Number :".$data["mto_number"]);
        //user history log
    }

	public function draw_get_db($where = NULL,$where_in = NULL,$project_id = NULL){

		//print_r($where_in);
		//return false;

		if($where){
			$query = $this->qcs->where($where);
		}
		if($where_in){
			$query = $this->qcs->where_in('drawing_no', $where_in);
		}
		if($project_id){
			$query = $this->qcs->where('project_id', $project_id);
		}
		$query = $this->qcs->get('eng_drawing');
		return $query->result_array();
	}

	public function mto_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('eproc_mto',$data);

		 //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["mto_number"]);
        //user history log
	}

	public function mto_detail_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('eproc_mto_detail',$data);

		 //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["mto_number"]);
        //user history log
	}

	public function peacemark_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('eng_drawing_piece_mark',$data);
		//user history log
           helper_log("update", "Update table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
	}

	public function joint_new_process_db($data){
		$this->db->insert('eng_drawing_joint', $data);
		//user history log
           helper_log("add", "Add table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
	}

	public function joint_get_db($where = NULL){
		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eng_drawing_joint');
		return $query->result_array();
	}

	public function joint_edit_process_db($data, $where){
		$this->db->where($where);
		$this->db->update('eng_drawing_joint',$data);

		//user history log
           helper_log("update", "Update table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
	}

	public function revision_get_db($where = NULL){
		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->order_by('rev_date', 'DESC');
		$query = $this->db->get('eng_drawing_rev');
		return $query->result_array();
	}

	function get_last_mto_number(){
		$this->db->select('*');
        $this->db->from('eproc_mto');
        $this->db->limit(1);
		$this->db->order_by('id',"DESC");
        return $this->db->get()->result();
	}

	function search_drawing_autocomplete($drawing){
        $this->qcs->like('drawing_no', $drawing);
        $this->qcs->order_by('drawing_no', 'ASC');
        $this->qcs->limit(20);
        return $this->qcs->get('eng_drawing')->result_array();
	}

	function search_material_catalog_autocomplete($material_catalog){
        $this->db->like('code_material', $material_catalog);
        $this->db->order_by('code_material', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_material_catalog')->result_array();
	}

	function search_mto_number_autocomplete($mto_number){
        $this->db->like('mto_number', $mto_number);
        $this->db->order_by('mto_number', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_mto')->result_array();
	}

    function search_sign_data($id){
        $query = $this->db2->query("SELECT sign_id,full_name,sign_approval FROM portal_user_db WHERE id_user = '$id'");
        return $query->result_array();
    }

    function count_data_approval($mto_id){
        $query = $this->db->query('SELECT 
                COUNT(*) as total,
                (SELECT COUNT(*) FROM eproc_mto_detail WHERE status = 1 AND mto_id = '.$mto_id.') as pdg,
                (SELECT COUNT(*) FROM eproc_mto_detail WHERE status = 3 AND mto_id = '.$mto_id.') as apr,
                (SELECT COUNT(*) FROM eproc_mto_detail WHERE status = 2 AND mto_id = '.$mto_id.') as rej
                FROM eproc_mto_detail WHERE mto_id = '.$mto_id);
        return $query->result_array();
    }

    function mto_material_join_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }

        $query = $this->db->select('*, mto_d.id as id_mto_detail, mto_d.status as status_mto_detail, mmc.material_class as name_material_class');
        $query = $this->db->join('eproc_material_catalog mc', 'mto_d.catalog_id = mc.id');
        $query = $this->db->join('qcs_database.master_material_grade mmg', 'mmg.id = mc.material_grade');
        $query = $this->db->join('qcs_database.master_material_class mmc', 'mmc.id = mmg.material_class');
        $query = $this->db->join('qcs_database.master_discipline dis', 'dis.id = mto_d.discipline');
        $query = $this->db->get('eproc_mto_detail mto_d');
        return $query->result_array();
    }

	// MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION//


	// ----------- Try Datatables ------ //

	// ----------- Try Datatables ------ //

	// MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
    var $table_mto_list_dt          = 'eproc_mto mto, qcs_database.master_module mm, portal_database.portal_project pp';
    var $column_order_mto_list_dt  	= array('mto.mto_number','mto.priority','mm.mod_desc', 'pp.project_name','DATE(mto.created_date)', 'usr.full_name');
    var $column_search_mto_list_dt 	= array('mto.mto_number','mto.priority','mm.mod_desc', 'pp.project_name','DATE(mto.created_date)', 'usr.full_name');
    var $order_mto_list_dt 		 	= array('mto.id' => 'desc'); // default order 

    private function _get_datatables_mto_list_dt_query($param)
    {
        $this->db->select('*, mto.id as mto_id, mto.created_date as mto_created_date');
        $this->db->from('eproc_mto mto');
        $this->db->join('qcs_database.master_module mm', 'mm.mod_id = mto.module');
        $this->db->join('portal_database.portal_project pp', 'pp.id = mto.project_id');
        $this->db->join('portal_database.portal_user_db usr', 'usr.id_user = mto.created_by');

        if($param == 'draft'){
        	$this->db->where('mto.status', 0);
        } else if($param == 'waiting'){
        	$this->db->where('mto.status', 1);
        } else if($param == 'rejected'){
        	$this->db->where('mto.status', 2);
        } else if($param == 'approved'){
        	$this->db->where('mto.status', 3);
        }
               
        $i = 0;
        
        foreach ($this->column_search_mto_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_mto_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
        	$this->db->order_by($this->column_order_mto_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_mto_list_dt))
        {
            $order = $this->order_mto_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_mto_list_dt($param)
    {
        $this->_get_datatables_mto_list_dt_query($param);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_mto_list_dt($param)
    {
        $this->db->select('*, mto.id as mto_id');
        $this->db->from('eproc_mto mto');
        $this->db->join('qcs_database.master_module mm', 'mm.mod_id = mto.module');
        $this->db->join('portal_database.portal_project pp', 'pp.id = mto.project_id');
        $this->db->join('portal_database.portal_user_db usr', 'usr.id_user = mto.created_by');
       	return $this->db->count_all_results();
    }

    function count_filtered_mto_list_dt($param)
    {
        $this->_get_datatables_mto_list_dt_query($param);
       	$query = $this->db->get();
        return $query->num_rows();
    }


    //MTO DETAIL DATATABLE --------------------------------------------------------------------------------
    var $table_mto_detail_list_dt          = 'eproc_mto_detail mto_d, eproc_material_catalog mc, qcs_database.master_discipline dis';
    var $column_order_mto_detail_list_dt   = array('mc.code_material','mc.material');
    var $column_search_mto_detail_list_dt  = array('mc.code_material','mc.material');
    var $order_mto_detail_list_dt          = array('mto_d.id' => 'desc'); // default order 

    private function _get_datatables_mto_detail_list_dt_query($mto_id)
    {
        $this->db->select('*, mto_d.id as mto_detail_id, mmg.material_grade as name_material_grade, mmc.material_class as name_material_class');
        $this->db->from('eproc_mto_detail mto_d');
        $this->db->join('eproc_material_catalog mc', 'mc.id = mto_d.catalog_id');
        $this->db->join('qcs_database.master_discipline md', 'md.id = mto_d.discipline');
        $this->db->join('qcs_database.master_material_grade mmg', 'mmg.id = mc.material_grade');
        $this->db->join('qcs_database.master_material_class mmc', 'mmc.id = mmg.material_class');
        $this->db->join('eproc_master_certification m_cer', 'm_cer.id = mto_d.certification');
        $this->db->where('mto_d.mto_id', $mto_id);
        $this->db->where('mto_d.status !=', 0);
               
        $i = 0;
        
        foreach ($this->column_search_mto_detail_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_mto_detail_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_mto_detail_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_mto_detail_list_dt))
        {
            $order = $this->order_mto_detail_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_mto_detail_list_dt($mto_id)
    {
        $this->_get_datatables_mto_detail_list_dt_query($mto_id);

        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_mto_detail_list_dt($mto_id)
    {
        $this->db->select('*, mto_d.id as mto_detail_id');
        $this->db->from('eproc_mto_detail mto_d');
        $this->db->join('eproc_material_catalog mc', 'mc.id = mto_d.catalog_id');
        $this->db->where('mto_d.mto_id', $mto_id);
        return $this->db->count_all_results();
    }

    function count_filtered_mto_detail_list_dt($param)
    {
        $this->_get_datatables_mto_detail_list_dt_query($param);
        $query = $this->db->get();
        return $query->num_rows();
    }
    //-----------------------------------------------------------------------------------------------------

    
   
	// MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//


	// ----------- Try Datatables ------ //
	

    public function getWeldtypeData()
    {
        $query = $this->db->get('master_weld_type');
        return $query->result();
    }
     public function getJointTypeData()
    {
        $query = $this->db->get('master_joint_type');
        return $query->result();
    }
    public function getClassData()
    {
        $query = $this->db->get('master_class');
        return $query->result_array();
    }
      


}
/*
	End Model Auth_mod
*/