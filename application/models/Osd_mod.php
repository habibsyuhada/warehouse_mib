<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Osd_mod extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        
        
    }

    public function osd_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_osd');
        return $query->result_array();
    }

    public function osd_list_detail($where = NULL){

        $query = $this->db->from('eproc_osd');
        $query = $this->db->join('eproc_osd_detail', 'eproc_osd.osd_no=eproc_osd_detail.osd_no',"left");
        $query = $this->db->join('eproc_material_catalog', 'eproc_osd_detail.catalog_id=eproc_material_catalog.id');

        if($where){
            $query = $this->db->where($where);
        }
  
        $query = $this->db->get();
        return $query->result_array();
    }

     public function osd_list_detail_search($where = NULL){

        $query = $this->db->from('eproc_osd_detail');

        if($where){
             $query = $this->db->where($where);
        }
  
        $query = $this->db->get();
        return $query->result_array();
    }


     public function mr_list_detail_po($where = NULL){
        
        if($where){
            $query = $this->db->where($where);
        }

        $query = $this->db->from('eproc_mr_detail');
        $query = $this->db->join('eproc_mr', 'eproc_mr_detail.mr_number=eproc_mr.mr_number',"left");
        $query = $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $query = $this->db->join('eproc_master_vendor', 'eproc_po.vendor=eproc_master_vendor.id_vendor',"left");
        $query = $this->db->order_by('`eproc_mr_detail.code_material`', 'ASC');

        $query = $this->db->get();
        return $query->result_array();
        
    }

  


    public function get_user_data(){
        $query = $this->db->where("status_user","1");
        $query = $this->db->get('portal_user_db');
        return $query->result_array();
    }

    public function generate_batch_no(){
        $query = $this->db->order_by('osd_no', 'DESC');
        $query = $this->db->limit("1");
        $query = $this->db->get('eproc_osd');
        
        $query1_result = $query->result_array();

        if($query1_result){
            $batch_no_gen = str_pad($query1_result[0]["osd_no"] + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $batch_no_gen = "000001";
        }

        return $batch_no_gen;

    }

    public function material_category_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_material_catalog');
        return $query->result_array();
    }

    public function osd_material_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_osd_detail');
        return $query->result_array();
    }

    public function osd_new_process_db($data){
        $this->db->insert('eproc_osd', $data);
        return $this->db->insert_id();
        //user history log
           // helper_log("add", "Add table eproc_osd ".$data["osd_number"]);
        //user history log
    }

    public function osd_detail_new_process_db($data){
        $this->db->insert('eproc_osd_detail', $data);
        //user history log
           // helper_log("add", "Add table eproc_osd_detail ".$data["osd_number"]);
        //user history log
    }

    function osd_detail_import_process_db($data) {
        $this->db->insert_batch('eproc_osd_detail', $data);
        //user history log
           // helper_log("add", "Import table eproc_osd_detail osd Number :".$data["osd_number"]);
        //user history log
    }

    

    public function osd_detail_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_osd_detail',$data);

         //user history log
           // helper_log("update", "Update table eproc_osd_detail ".$data["osd_number"]);
        //user history log
    }

    public function update_osd_data($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_osd',$data);

         //user history log
           // helper_log("update", "Update table eproc_osd_detail ".$data["osd_number"]);
        //user history log
    }

    public function insert_osd_transaction($data){
        $this->db->insert('eproc_transaction', $data);
    }

    public function peacemark_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eng_drawing_piece_mark',$data);
        //user history log
           helper_log("update", "Update table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function joint_new_process_db($data){
        $this->db->insert('eng_drawing_joint', $data);
        //user history log
           helper_log("add", "Add table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function joint_get_db($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eng_drawing_joint');
        return $query->result_array();
    }

    public function joint_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eng_drawing_joint',$data);

        //user history log
           helper_log("update", "Update table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function revision_get_db($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by('rev_date', 'DESC');
        $query = $this->db->get('eng_drawing_rev');
        return $query->result_array();
    }

    public function revision_new_process_db($data){
        $this->db->insert('eng_drawing_rev', $data);
        //user history log
           helper_log("add", "Add table eng_drawing_rev Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function draw_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing', $data);
        //user history log
           //helper_log("add", "Import table eng_drawing Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function peacemark_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing_piece_mark', $data);
        //user history log
           helper_log("add", "Import table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function joint_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing_joint', $data);
        //user history log
           helper_log("add", "Import table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function get_last_osd_number(){
        $this->db->select('*');
        $this->db->from('eproc_osd');
        $this->db->limit(1);
        $this->db->order_by('osd_id',"DESC");
        return $this->db->get()->result();
    }

  

    function search_unique_autocomplete($uniq_no,$po_number){
        $this->db->select("*"); 
        $this->db->from('eproc_mrir');
        $this->db->join('eproc_mrir_material', 'eproc_mrir.report_no = eproc_mrir_material.report_no');
        
        $this->db->like('eproc_mrir_material.unique_no', $uniq_no);
        $this->db->where('eproc_mrir.po_number', $po_number);
        $this->db->where('eproc_mrir_material.status', "3");
        $this->db->order_by('eproc_mrir_material.unique_no', 'ASC');
        $this->db->limit(20);

        $query = $this->db->get();
          return $query->result_array();
    }


    function get_data_catalog($catalog_id = null){
        if(isset($catalog_id)){
            $this->db->where('id', $catalog_id);
        }
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function get_bal_unique($uniq_no){
        $this->db->where('unique_no', $uniq_no);
        $this->db->order_by('unique_no', 'ASC');
        return $this->db->get('eproc_mat_bal')->result_array();
    }


    function search_material_catalog_autocomplete($material_catalog){
        $this->db->like('short_desc', $material_catalog);
        $this->db->order_by('short_desc', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function insert_osd_master($data){
        $this->db->insert("eproc_osd", $data);
        return $this->db->insert_id();
    }


    function insert_osd_detail($data){
        $this->db->insert("eproc_osd_detail", $data);
        return $this->db->insert_id();
    }

    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION//


    // ----------- Try Datatables ------ //

    // ----------- Try Datatables ------ //

    // MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
    var $table_osd_list_dt          = 'eproc_osd';
    var $column_order_osd_list_dt   = array('project_id', 'osd_no','po_number','project_id','equipment_tag_no','osd_no','created_by','date_create','create_date','status');
    var $column_search_osd_list_dt  = array('project_id', 'osd_no','po_number','project_id','equipment_tag_no','osd_no','created_by','date_create','create_date','status');
    var $order_osd_list_dt          = array('osd_id' => 'desc'); // default order 

    private function _get_datatables_osd_list_dt_query($req_pages)
    {

        $this->db->where("status",$req_pages);
        $this->db->from($this->table_osd_list_dt);

        
        $i = 0;
        
        foreach ($this->column_search_osd_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_osd_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_osd_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_osd_list_dt))
        {
            $order = $this->order_osd_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_osd_list_dt($req_pages)
    {
        $this->_get_datatables_osd_list_dt_query($req_pages);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_osd_list_dt($req_pages)
    {
        $this->db->from($this->table_osd_list_dt);
        return $this->db->count_all_results();
    }

    function count_filtered_osd_list_dt($req_pages)
    {
        $this->_get_datatables_osd_list_dt_query($req_pages);
        $query = $this->db->get();
        return $query->num_rows();
    }

    
   
    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//


    // ----------- Try Datatables ------ //
    

    public function getWeldtypeData()
    {
        $query = $this->db->get('master_weld_type');
        return $query->result();
    }
     public function getJointTypeData()
    {
        $query = $this->db->get('master_joint_type');
        return $query->result();
    }
    public function getClassData()
    {
        $query = $this->db->get('master_class');
        return $query->result_array();
    }


       //----------------------------------------------------

    function search_po_autocomplete($po_number){
        $this->db->like('po_number', $po_number);
        $this->db->where('status','3');
        $this->db->order_by('po_number', 'ASC');
        $this->db->group_by('po_number');
        $this->db->limit(20);
        return $this->db->get('eproc_mrir')->result_array();
    }

    function check_po($po_number){
        $this->db->where('po_number', $po_number);
        $this->db->where('status','3');
        $this->db->order_by('po_number', 'ASC');
        $this->db->group_by('po_number');
        return $this->db->get('eproc_mrir')->result_array();
    }

   //----------------------------------------------------

    function get_project_detail($project_id){
        $this->db->where('id', $project_id);
        $this->db->order_by('id', 'ASC');
        return $this->db->get('portal_project')->result_array();
    } 

    function get_data_unique($uniq_no,$po_number){

        $this->db->select("*"); 
        $this->db->from('eproc_mrir');
        $this->db->join('eproc_mrir_material', 'eproc_mrir.id = eproc_mrir_material.mrir_id');
        
        $this->db->like('eproc_mrir_material.unique_no', $uniq_no);
        $this->db->where('eproc_mrir.po_number', $po_number);
        $this->db->where('eproc_mrir_material.status', "3");
        $this->db->order_by('eproc_mrir_material.unique_no', 'ASC');
                
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_data_mr($uniq_no,$po_number){

        $this->db->select("*"); 
        $this->db->from('eproc_mrir');
        $this->db->join('eproc_mrir_material', 'eproc_mrir.id = eproc_mrir_material.mrir_id');
        
        $this->db->like('eproc_mrir_material.unique_no', $uniq_no);
        $this->db->where('eproc_mrir.po_number', $po_number);
        $this->db->where('eproc_mrir_material.status', "3");
        $this->db->order_by('eproc_mrir_material.unique_no', 'ASC');
                
        $query = $this->db->get();
        return $query->result_array();
    }


    function get_data_unique_by_detail($osd_det_id){      
        $this->db->where('osd_det_id', $osd_det_id);    
        return $this->db->get('eproc_osd_detail')->result_array();
    }


    function read_osd_status($osd_no){
        
        $query = $this->db->from('eproc_osd');
        $query = $this->db->join('eproc_osd_detail', 'eproc_osd.osd_no = eproc_osd_detail.osd_no');
        $query = $this->db->where('eproc_osd_detail.osd_no', $osd_no);
        $query = $this->db->where('eproc_osd_detail.status_osd', "0");
        $query = $this->db->get();
        return $query->result_array();

    }

    function get_material_unique_no($unique_no){
        $this->db->select('*');
        $this->db->from('eproc_mrir_material');
        $this->db->where('unique_no', $unique_no);
        $query = $this->db->get();
        return $query->result_array();

    }

    function get_receiving_data($receiving_detail){
              
        $query = $this->db->select('eproc_receiving.date_created');
        $query = $this->db->from('eproc_receiving');
        $query = $this->db->join('eproc_receiving_detail', 'eproc_receiving.id = eproc_receiving_detail.receiving_id');
        $query = $this->db->where('eproc_receiving_detail.id', $receiving_detail);
        $query = $this->db->get();
        return $query->result_array();
    }


}
/*
    End Model Auth_mod
*/