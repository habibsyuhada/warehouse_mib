<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Po_mod extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->user_cookie       = explode(";",$this->input->cookie('portal_user'));
        $this->permission_cookie = explode(";",$this->input->cookie('portal_wh'));        
        
    }


    public function mr_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mr');
        return $query->result_array();
    }

    public function mr_list_detail($where = NULL,$mr_number = null,$mr_nopo = null){
        
        if(isset($where)){
            $query = $this->db->where($where);
        }

        if(isset($mr_number)){
            $query = $this->db->where("mr_number",$mr_number);
            $query = $this->db->where("po_number IS NOT NULL");
        }

        if(isset($mr_nopo)){
            $query = $this->db->where("mr_number",$mr_nopo);
            $query = $this->db->where("(po_number IS NULL OR po_number = '')");
        }

        
        $query = $this->db->from('eproc_mr_detail');
        $query = $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $query = $this->db->order_by('`eproc_mr_detail.code_material`', 'ASC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function mr_list_detail_po($where = NULL){
        
        if($where){
            $query = $this->db->where($where);
        }

        $query = $this->db->from('eproc_mr_detail');
        $query = $this->db->join('eproc_mr', 'eproc_mr_detail.mr_number=eproc_mr.mr_number',"left");
        $query = $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $query = $this->db->join('eproc_master_vendor', 'eproc_mr_detail.vendor_winner=eproc_master_vendor.id_vendor',"left");        
        $query = $this->db->join('eproc_mr_quotation', 'eproc_mr_detail.mr_number=eproc_mr_quotation.mr_number',"left");
        $query = $this->db->group_by('`eproc_mr_detail.id`');
        $query = $this->db->order_by('`eproc_mr_detail.code_material`', 'ASC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function mr_list_detail_po_sum($where = NULL){
        
       
        $query = $this->db->select('SUM(total_amount) as total_amount');
        $query = $this->db->from('eproc_mr_detail');
        $query = $this->db->join('eproc_mr', 'eproc_mr_detail.mr_number=eproc_mr.mr_number',"left");
        $query = $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $query = $this->db->join('eproc_master_vendor', 'eproc_po.vendor=eproc_master_vendor.id_vendor',"left");  
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->group_by('`eproc_mr_detail.id`');
        $query = $this->db->order_by('`eproc_mr_detail.code_material`', 'ASC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function mto_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mto');
        return $query->result_array();
    }


   
    public function mto_list_detail($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mto_detail');
        return $query->result_array();
    }

    
    public function get_user_data(){
        $query = $this->db->where("status_user","1");
        $query = $this->db->get('portal_user_db');
        return $query->result_array();
    }

    public function generate_batch_no(){
        $query = $this->db->order_by('mr_number', 'DESC');
        $query = $this->db->limit("1");
        $query = $this->db->get('eproc_mr');
        
        $query1_result = $query->result_array();

        if($query1_result){
            $batch_no_gen = str_pad($query1_result[0]["mr_number"] + 1, 6, '0', STR_PAD_LEFT);
        } else {
            $batch_no_gen = "000001";
        }

        return $batch_no_gen;

    }

    public function material_category_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_material_catalog');
        return $query->result_array();
    }

    public function mr_material_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }

   

   

    public function mr_detail_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mr_detail',$data);

         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function update_mr_data($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_mr',$data);

         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function insert_mr_transaction($data){
        $this->db->insert('eproc_transaction', $data);
    }

    public function peacemark_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eng_drawing_piece_mark',$data);
        //user history log
           helper_log("update", "Update table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function joint_new_process_db($data){
        $this->db->insert('eng_drawing_joint', $data);
        //user history log
           helper_log("add", "Add table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function joint_get_db($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eng_drawing_joint');
        return $query->result_array();
    }

    public function joint_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eng_drawing_joint',$data);

        //user history log
           helper_log("update", "Update table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    public function revision_get_db($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by('rev_date', 'DESC');
        $query = $this->db->get('eng_drawing_rev');
        return $query->result_array();
    }

    public function revision_new_process_db($data){
        $this->db->insert('eng_drawing_rev', $data);
        //user history log
           helper_log("add", "Add table eng_drawing_rev Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function draw_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing', $data);
        //user history log
           //helper_log("add", "Import table eng_drawing Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function peacemark_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing_piece_mark', $data);
        //user history log
           helper_log("add", "Import table eng_drawing_piece_mark Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function joint_new_import_process_db($data) {
        $this->db->insert_batch('eng_drawing_joint', $data);
        //user history log
           helper_log("add", "Import table eng_drawing_joint Drawing no :".$data["drawing_no"]);
        //user history log
    }

    function get_last_mr_number(){
        $this->db->select('*');
        $this->db->from('eproc_mr');
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        return $this->db->get()->result();
    }

   




    function get_data_unique($uniq_no = null,$project_id = null){
        //if(isset($uniq_no)){
           // $this->db->where('uniq_no', $uniq_no);
       // }
        //$this->db->order_by('uniq_no', 'ASC');
        //return $this->db->get('eproc_receiving_detail')->result_array();

        $this->db->select("*"); 
          $this->db->from('eproc_receiving');
          $this->db->join('eproc_receiving_detail', 'eproc_receiving.receiving_id = eproc_receiving_detail.receiving_id');
        
          if(isset($uniq_no)){
               $this->db->where("eproc_receiving_detail.uniq_no",$uniq_no);
          }

          if(isset($project_id)){
               $this->db->where("eproc_receiving.project_id",$project_id);
          }

          $query = $this->db->get();
          return $query->result_array();
   
    }

     function get_data_catalog($catalog_id = null){
        if(isset($catalog_id)){
            $this->db->where('id', $catalog_id);
        }
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function get_bal_unique($uniq_no){
        $this->db->where('unique_no', $uniq_no);
        $this->db->order_by('unique_no', 'ASC');
        return $this->db->get('eproc_mat_bal')->result_array();
    }


    function search_material_catalog_autocomplete($material_catalog){
        $this->db->like('short_desc', $material_catalog);
        $this->db->order_by('short_desc', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_material_catalog')->result_array();
    }

    function insert_mr_master($data){
        $this->db->insert("eproc_mr", $data);
        return $this->db->insert_id();
    }


    function insert_mr_detail($data){
        $this->db->insert("eproc_mr_detail", $data);
        return $this->db->insert_id();
    }

    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION//


    // ----------- Try Datatables ------ //

    // ----------- Try Datatables ------ //

    // MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
    var $column_order_mr_list_dt   = array('mr.budget_cat_id','mr.budget_dept_id','mr.mr_number','mr.created_by','mr.created_date');
    var $column_search_mr_list_dt  = array('mr.budget_cat_id','mr.budget_dept_id','mr.mr_number','mr.created_by','mr.created_date');
    var $order_mr_list_dt          = array('mr.id_mr' => 'desc'); // default order 

    private function _get_datatables_mr_list_dt_query($status,$type_req)
    {
        $query = $this->db->from('eproc_mr mr');
        $query = $this->db->join('eproc_mr_detail emd', 'mr.mr_number=emd.mr_number',"left");
        //$query = $this->db->join('eproc_po epo', 'mr.mr_number=epo.mr_number',"left");

        if($type_req == 'so'){
            $query = $this->db->where('mr.request_type',"Service Request");
        } else if($type_req == 'po'){
             $query = $this->db->where('mr.request_type',"Material Request");    
        }

        $query = $this->db->where('mr.status',$status);
        $query = $this->db->where('emd.vendor_winner <> ""');
        //$query = $this->db->where('epo.bod_approval_status <> 3');

        if($this->permission_cookie[84] != 1){

            $query = $this->db->where('mr.budget_dept_id',$this->user_cookie[4]);

        }

         $query = $this->db->group_by('emd.vendor_winner',"desc");

        
        $i = 0;
        
        foreach ($this->column_search_mr_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_mr_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_mr_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_mr_list_dt))
        {
            $order = $this->order_mr_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_mr_list_dt($status,$type_req)
    {
        $this->_get_datatables_mr_list_dt_query($status,$type_req);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_mr_list_dt($status,$type_req)
    {
        $query = $this->db->from('eproc_mr mr');
        return $this->db->count_all_results();
    }

    function count_filtered_mr_list_dt($status,$type_req)
    {
        $this->_get_datatables_mr_list_dt_query($status,$type_req);
        $query = $this->db->get();
        return $query->num_rows();
    }
   
    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//

    // ----------- Try Datatables ------ //
    

    public function getWeldtypeData()
    {
        $query = $this->db->get('master_weld_type');
        return $query->result();
    }
     public function getJointTypeData()
    {
        $query = $this->db->get('master_joint_type');
        return $query->result();
    }
    public function getClassData()
    {
        $query = $this->db->get('master_class');
        return $query->result_array();
    }
      
   
    function search_mto_autocomplete($mto_number){
        $this->db->like('mto_number', $mto_number);
        $this->db->where('status', "3");
        $this->db->order_by('mto_number', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_mto')->result_array();
    }

    function mto_category_get_db($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_mto_cat');
        return $query->result_array();
    }


    // MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
    var $table_mto_list_dt          = 'eproc_mto';
    var $column_order_mto_list_dt   = array('module','priority','id');
    var $column_search_mto_list_dt  = array('module','priority','id');
    var $order_mto_list_dt          = array('id' => 'desc'); // default order 

    private function _get_datatables_mto_list_dt_query($param)
    {
        
        $this->db->from($this->table_mto_list_dt);

        if($param == 'draft'){
            $this->db->where('status', 0);
        } else if($param == 'pending'){
            $this->db->where('status', 1);
        } else if($param == 'rejected'){
            $this->db->where('status', 2);
        } else if($param == 'approved'){
            $this->db->where('status', 3);
        }
               
        $i = 0;
        
        foreach ($this->column_search_mto_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_mto_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_mto_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_mto_list_dt))
        {
            $order = $this->order_mto_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_mto_list_dt($param)
    {
        $this->_get_datatables_mto_list_dt_query($param);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_mto_list_dt($param)
    {
        $this->db->from($this->table_mto_list_dt);
        return $this->db->count_all_results();
    }

    function count_filtered_mto_list_dt($param)
    {
        $this->_get_datatables_mto_list_dt_query($param);
        $query = $this->db->get();
        return $query->num_rows();
    }

    
   
    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//


    function get_total_mto_detail($where = NULL){

        if($where){
            $query = $this->db->where("mto_id",$where);
        }

        $query = $this->db->get('eproc_mto_detail');
        return $query->result_array();
    }

   
    function get_total_mr($where = NULL){
        if($where){
            $query = $this->db->where("mto_id",$where);
        }
        
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }

     public function po_new_process_db($data){
        $this->db->insert('eproc_po', $data);
        //user history log
           ///helper_log("add", "Add table eproc_mr ".$data["mr_number"]);
        //user history log
    }


    // MAHMUD : DATA TABLES AJAX QUERY FOR GROUP DATA DISPOSITION GROUP//

    
    var $table_po_list_dt          = 'eproc_mr';
    var $column_order_po_list_dt   = array('eproc_mr.budget_cat_id','eproc_po.po_number', 'eproc_mr.mr_number','eproc_mr.form_number','eproc_po.created_by_po','eproc_po.created_date_po');
    var $column_search_po_list_dt  = array('eproc_mr.budget_cat_id','eproc_po.po_number', 'eproc_mr.mr_number','eproc_mr.form_number','eproc_po.created_by_po','eproc_po.created_date_po');
    var $order_po_list_dt          = array('eproc_po.id_po' => 'desc'); // default order 

    private function _get_datatables_po_list_dt_query($req_pages,$type_req)
    {

        //$this->db->where("status",$req_pages);
        $this->db->from($this->table_po_list_dt);
        $this->db->join('eproc_mr_detail', 'eproc_mr.mr_number=eproc_mr_detail.mr_number',"left");
        $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $this->db->where('eproc_po.po_number <> ""');
        if($type_req == 'so'){
            $this->db->where('eproc_mr.request_type = "Service Request"');
        } else {
            $this->db->where('eproc_mr.request_type = "Material Request"'); 
        }

        


        //if($this->permission_cookie[84] != 1){

            //$query = $this->db->where('eproc_mr.budget_dept_id',$this->user_cookie[4]);

       // }
               
        $i = 0;
        
        foreach ($this->column_search_po_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_po_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_po_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            $this->db->group_by("eproc_po.po_number");

        }
        else if (isset($this->order_po_list_dt))
        {
            $order = $this->order_po_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
            $this->db->group_by("eproc_po.po_number");
        }
    }


    function get_datatables_po_list_dt($req_pages,$type_req)
    {
        $this->_get_datatables_po_list_dt_query($req_pages,$type_req);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_po_list_dt($req_pages,$type_req)
    {
        $this->db->select("eproc_po.po_number");
        $this->db->from($this->table_po_list_dt);
        $this->db->join('eproc_mr_detail', 'eproc_mr.mr_number=eproc_mr_detail.mr_number',"left");
        $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $this->db->group_by("eproc_po.po_number");
        return $this->db->count_all_results();
    }

    function count_filtered_po_list_dt($req_pages,$type_req)
    {
        $this->_get_datatables_po_list_dt_query($req_pages,$type_req);
        $query = $this->db->get();
        return $query->num_rows();
    }
   
    // MAHMUD : DATA TABLES AJAX GROUP DATA DISPOSITION GROUP//


    public function po_update_data($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_po',$data);

         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }

    public function check_id_po($where){
        $query = $this->db->where("mr_detail_id",$where);
        $query = $this->db->get('eproc_po');
        return $query->num_rows();
         //user history log
           // helper_log("update", "Update table eproc_mr_detail ".$data["mr_number"]);
        //user history log
    }


    public function get_vendor(){
        $query = $this->db->order_by('id_vendor','asc');   
        $query = $this->db->get('eproc_master_vendor');
        return $query->result_array();
    }

    public function get_vendor_by_po($po){
        $query = $this->db->where('po_number',$po);
        $query = $this->db->get('eproc_po');
        return $query->result_array();  
    }


    function po_list($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_po');
        return $query->result_array();
    }

    public function mr_total_by_item_call($where = NULL){
        $query = $this->db->select("SUM(qty) as total_mr");
        $query = $this->db->where("mto_id_detail",$where);
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }


    public function po_total_by_item_call($where = NULL){
        $query = $this->db->select("SUM(po_qty) as total_po");
        $query = $this->db->where("mr_detail_id",$where);
        $query = $this->db->get('eproc_po');
        return $query->result_array();
    }

    public function mr_list_detail_po_create($where = NULL,$mr_number = null,$mr_nopo = null){
        
        if(isset($where)){
            $query = $this->db->where($where);
        }

        if(isset($mr_number)){
            $query = $this->db->where("`eproc_mr_detail.mr_number`",$mr_number);
        }

        
        $query = $this->db->from('eproc_mr_detail');
        $query = $this->db->join('eproc_master_vendor', 'eproc_mr_detail.vendor_winner=eproc_master_vendor.id_vendor',"left");
        $query = $this->db->join('eproc_mr_quotation', 'eproc_mr_detail.mr_number=eproc_mr_quotation.mr_number',"left");
        $query = $this->db->group_by('`eproc_mr_detail.id`');
        $query = $this->db->order_by('`eproc_mr_detail.vendor_winner`', 'ASC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function mr_list_detail_po_create_data($where = NULL,$mr_number = null,$mr_nopo = null){
        
        if(isset($where)){
            $query = $this->db->where($where);
        }

        if(isset($mr_number)){
            $query = $this->db->where("`eproc_mr_detail.mr_number`",$mr_number);
        }

        
        $query = $this->db->from('eproc_mr_detail');
        $query = $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $query = $this->db->join('eproc_master_vendor', 'eproc_mr_detail.vendor_winner=eproc_master_vendor.id_vendor',"left");
        $query = $this->db->join('eproc_mr_quotation', 'eproc_mr_detail.mr_number=eproc_mr_quotation.mr_number',"left");
        $query = $this->db->where('`eproc_po.mr_detail_id` IS NULL');
        $query = $this->db->group_by('`eproc_mr_detail.id`');
        $query = $this->db->order_by('`eproc_mr_detail.vendor_winner`', 'ASC');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function po_list_detail_call($where = NULL){
        $query = $this->db->where("mr_detail_id",$where);
        $query = $this->db->get('eproc_po');
        return $query->result_array();
    }


    public function src_total_mr_qty($where = NULL){
        $query = $this->db->where("id",$where);
        $query = $this->db->get('eproc_mr_detail');
        return $query->result_array();
    }

    public function src_total_po_qty($where = NULL){
        $query = $this->db->select("SUM(po_qty) as total_po_qty");
        $query = $this->db->where("mr_detail_id",$where);
        $query = $this->db->get('eproc_po');
        return $query->result_array();
    }

    public function delete_data_po($id_po = NULL){

        $query = $this->db->where('id_po', $id_po);
        $query = $this->db->delete('eproc_po');

        return false;
    }


    public function count_mr_po_data($where = NULL){
        
        $query = $this->db->select("eproc_mr_detail.mr_number,eproc_mr_detail.qty_req as total_mr_qty,SUM(eproc_po.po_qty) as total_all_po");
        $query = $this->db->where("eproc_po.mr_number",$where);
        $query = $this->db->from('eproc_mr_detail');
        $query = $this->db->join('eproc_po', 'eproc_mr_detail.id=eproc_po.mr_detail_id',"left");
        $query = $this->db->group_by('`eproc_mr_detail.id`');
        $query = $this->db->order_by('`eproc_mr_detail.code_material`', 'ASC');

        $query = $this->db->get();
        return $query->result_array();
   
    }

    public function get_vendor_list_of_mr($mr_no = NULL){
       
        $query = $this->db->where("mr_number",$mr_no);
        $query = $this->db->get('eproc_mr_quotation');
        return $query->result_array();
    }


     public function get_term_data($data){
        $query = $this->db->where("mr_number",$data);
        $query = $this->db->get('eproc_term');
        return $query->result_array();
    }

    public function update_po_data($data, $where){
        $this->db->where("mr_number",$where);
        $this->db->update('eproc_term',$data);

    }

    public function insert_po_data($data){
        $this->db->insert('eproc_term',$data);
    }

     public function check_term_updated($mr_no = NULL,$po_no = NULL){
       
        $query = $this->db->where("mr_number",$mr_no);
        $query = $this->db->where("po_number",$po_no);
        $query = $this->db->get('eproc_term');
        return $query->result_array();
    }

    public function update_po_data_item($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_po',$data);

    }


    public function save_data_po_new($data){
        $this->db->insert('eproc_po',$data);
    }

}

/*
    End Model Auth_mod
*/