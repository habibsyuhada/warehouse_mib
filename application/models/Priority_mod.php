<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Priority_mod extends CI_Model
{
    private $_table = "eproc_priority";

    public $area_name;
    public $status;

    function mto_category_add($data){
        $this->db->insert('eproc_priority', $data);
        //user history log
           helper_log("add", "Add table eproc_priority ".$data["description"]);
        //user history log
    }

    public function getAll($where = NULL)
    {
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_priority');
        return $query->result_array();
    }
      
    public function getById($id)
    {
        return $this->db->get_where($this->_table, array("id" => $id))->row();
    }

    public function mto_category_edit($data, $where)
    {
        $this->db->where($where);
        $this->db->update('eproc_priority',$data);

         //user history log
           helper_log("update", "Update table eproc_mto_detail ".$data["description"]);
        //user history log
    }

    public function delete($id)
    {
        $status_delete = 1;
        //user history log
           helper_log("delete", "Update Delete Status Master Area data id ".$id." Status to ".$status_delete);
        //user history log
        
       return $this->db->update($this->_table, array("status_delete" => $status_delete), array("id" => $id));

        

    }
}