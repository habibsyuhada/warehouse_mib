<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receiving_mod extends CI_Model {

	public function __construct(){
	  	parent::__construct();
 	}

    //VENDOR -------------------------------------------------
    function data_vendor($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_master_vendor');
        return $query->result_array();
    }
    //--------------------------------------------------------

    //RECEIVING MASTER ---------------------------------------
 	function receiving_data($where = NULL){
 		if($where){
			$query = $this->db->where($where);
		}
		$query = $this->db->get('eproc_receiving');
		return $query->result_array();
 	}

    function receiving_new_process_db($data){
        $this->db->insert('eproc_receiving', $data);
        $last_id = $this->db->insert_id();
        return $last_id;
        
        //user history log
           // helper_log("add", "Add table eproc_receiving ".$data["mto_number"]);
        //user history log
    }

    function receiving_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_receiving',$data);

         //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["mto_number"]);
        //user history log
    }
    //--------------------------------------------------------

    //RECEIVING DETAIL ---------------------------------------
    function receiving_detail_data($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by('id', 'DESC');
        $query = $this->db->get('eproc_receiving_detail');
        return $query->result_array();
    }

    function receiving_detail_join($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        // $query = $this->db->select('*, mrd.qty as total_order, rd.qty as qty_attendance');
        // $query = $this->db->join('eproc_po po', 'rd.po_number = po.po_number');
        // $query = $this->db->join('eproc_material_catalog cat', 'rd.catalog_id = cat.id');
        // $query = $this->db->join('eproc_mr_detail mrd', 'mrd.id = po.mr_detail_id');
        // $query = $this->db->join('eproc_catalog_category cc', 'cc.id = cat.catalog_category_id');
        // $query = $this->db->group_by('rd.id');
        // $query = $this->db->order_by('rd.id', 'DESC');
        $query = $this->db->get('eproc_receiving_detail rd');
        return $query->result_array();
    }

    function receiving_detail_new_process_db($data){
        $this->db->insert('eproc_receiving_detail', $data);
        //user history log
           // helper_log("add", "Add table eproc_receiving ".$data["mto_number"]);
        //user history log
    }

    function receiving_detail_edit_process_db($data, $where){
        $this->db->where($where);
        $this->db->update('eproc_receiving_detail',$data);

         //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["mto_number"]);
        //user history log
    }

    function receiving_detail_import_process_db($data) {
        $this->db->insert_batch('eproc_receiving_detail', $data);
        //user history log
           // helper_log("add", "Import table eproc_mto_detail MTO Number :".$data["mto_number"]);
        //user history log
    }

    function count_total_qty_receiving($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }

        $query = $this->db->select('IF(COUNT(qty), qty, 0) as qty_receiving');
        $query = $this->db->get('eproc_receiving_detail');
        return $query->result_array();
    }
    //--------------------------------------------------------

    //AUTOCOMPLETE -------------------------------------------
    function search_po_autocomplete($po){
        $this->db->like('po_number', $po);
        $this->db->group_by('po_number');
        $this->db->order_by('po_number', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_po')->result_array();
    }

    function search_vendor_autocomplete($po){
        $this->db->like('vendor_name', $po);
        $this->db->group_by('vendor_name');
        $this->db->order_by('vendor_name', 'ASC');
        $this->db->limit(20);
        return $this->db->get('eproc_master_vendor')->result_array();
    }

    function po_data($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_po po');
        return $query->result_array();
    }

    function po_data_join($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }

        $query = $this->db->select('*, 
                material_catalog.id AS material_catalog_id, 
                material_catalog.material AS material_catalog_name,
                material_catalog.code_material AS material_catalog_code,
                po_detail.qty AS qty_request,
                uom.uom AS uom_name,
        ');
        $query = $this->db->join('eproc_po_detail po_detail','po_detail.po_number = po.po_number'); //JOIN TO MR DETAIL
        $query = $this->db->join('eproc_mr_detail mr_detail','mr_detail.id = po_detail.mr_detail_id'); //JOIN TO MR DETAIL
        // $query = $this->db->join('portal_department department','department.id_department = po.dept_id'); // JOIN TO DEPARTMENT
        $query = $this->db->join('eproc_material_catalog material_catalog','material_catalog.code_material = mr_detail.code_material'); // JOIN TO MATERIAL CATALOG
        // $query = $this->db->join('eproc_master_vendor vendor','vendor.id_vendor = po.vendor'); // JOIN TO VENDOR
        $query = $this->db->join('eproc_uom uom','uom.id_uom = mr_detail.uom_order'); // JOIN TO UOM
        $query = $this->db->get('eproc_po po');
        return $query->result_array();
    }
    //--------------------------------------------------------


 	//RECEIVING LIST DATATABLE -------------------------------------------
 	var $table_receiving_list_dt		 	= 'eproc_receiving r';
    var $column_order_receiving_list_dt  	= array('do_pl','date_created','full_name', 'ata', 'atd');
    var $column_search_receiving_list_dt 	= array('do_pl','date_created','full_name', 'ata', 'atd');
    var $order_receiving_list_dt 		 	= array('id' => 'desc'); // default order 

    private function _get_datatables_receiving_list_dt_query($where = null)
    {
        $this->db->select('*, r.id AS receiving_id');
        $this->db->from($this->table_receiving_list_dt);
        $this->db->join("portal_user_db u","u.id_user = r.user_created");

        if($where){
            $this->db->where($where);
        }
               
        $i = 0;
        
        foreach ($this->column_search_receiving_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_receiving_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
        	$this->db->order_by($this->column_order_receiving_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_receiving_list_dt))
        {
            $order = $this->order_receiving_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_receiving_list_dt($where)
    {
        $this->_get_datatables_receiving_list_dt_query($where);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_receiving_list_dt($where)
    {
        $this->db->from($this->table_receiving_list_dt);
        $this->db->join("portal_user_db u","u.id_user = r.user_created");

        if($where){
            $this->db->where($where);
        }

       	return $this->db->count_all_results();
    }

    function count_filtered_receiving_list_dt($where)
    {
        $this->_get_datatables_receiving_list_dt_query($where);
       	$query = $this->db->get();
        return $query->num_rows();
    }
 	//---------------------------------------------------------------------

    //RECEIVING ATTACHMENT ------------------------------------------------
    function receiving_attachment($where = NULL){
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_receiving_document');
        return $query->result_array();
    }

    function receiving_attachment_add_process($data){
        $this->db->insert('eproc_receiving_document', $data);
    }

    function receiving_attachment_delete_process($data){
        $this->db->delete('eproc_receiving_document', $data);
    }
    //---------------------------------------------------------------------

    //RECEIVING DETAIL LIST DATATABLE -------------------------------------------
    var $table_receiving_detail_list_dt            = 'eproc_receiving';
    var $column_order_receiving_detail_list_dt     = array('po_number');
    var $column_search_receiving_detail_list_dt    = array('po_number');
    var $order_receiving_detail_list_dt            = array('rd.id' => 'desc'); // default order 

    private function _get_datatables_receiving_detail_list_dt_query($receiving_id)
    {
        $this->db->select('*, rd.qty AS qty_receiving');
        $this->db->from('eproc_receiving_detail rd');
        $this->db->where("rd.receiving_id = '".$receiving_id."'");
        $this->db->group_by('rd.id');
        $this->db->order_by('rd.id', 'ASC');
               
        $i = 0;
        
        foreach ($this->column_search_receiving_detail_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_receiving_detail_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_receiving_detail_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_receiving_detail_list_dt))
        {
            $order = $this->order_receiving_detail_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_receiving_detail_list_dt($receiving_id)
    {
        $this->_get_datatables_receiving_detail_list_dt_query($receiving_id);

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_receiving_detail_list_dt($receiving_id)
    {
        $this->db->from($this->table_receiving_detail_list_dt);
        return $this->db->count_all_results();
    }

    function count_filtered_receiving_detail_list_dt($receiving_id)
    {
        $this->_get_datatables_receiving_detail_list_dt_query($receiving_id);
        $query = $this->db->get();
        return $query->num_rows();
    }
    //---------------------------------------------------------------------
}