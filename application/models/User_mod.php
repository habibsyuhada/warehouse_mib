<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_mod extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function all_list($id = NULL, $where = NULL){
		if($id){
			$query = $this->db->where('user.id_user', $id);
		}
		elseif($where){
			$query = $this->db->where($where);
		}
		$this->db->select('user.*, dept.name_of_department, role.name_of_role');
		$query = $this->db->join('portal_department dept', 'dept.id_department = user.department', 'left outer');
		$query = $this->db->join('portal_role role', 'role.id = user.id_role', 'left outer');
		$query = $this->db->get('portal_user_db user');
		return $query->result_array();
	}

	public function user_detail_new_process($data){
		$this->db->insert('portal_user_db', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	public function user_detail_edit_process($data, $where){
		$this->db->where($where);
    $this->db->update('portal_user_db', $data);
	}
}
/*
	End Model Auth_mod
*/