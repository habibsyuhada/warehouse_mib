<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor_verification_mod extends CI_Model
{        
    public function rules()
    {
        return array(
                array('field' => 'area_name','label' => 'Area Name','rules' => 'required'),
                array('field' => 'status','label' => 'Area Status','rules' => 'required'));
		
    }

    function vendor_verification_add($data){
        $this->db->insert('eproc_master_vendor_verification', $data);
        //user history log
           helper_log("add", "Add table eproc_master_vendor ".$data["description"]);
        //user history log
    }

    function vendor_import_process_db($data) {
        $this->db->insert_batch('eproc_master_vendor', $data);
        //user history log
           helper_log("add", "Import table eproc_master_vendor Short Desc :".$data["short_desc"]);
        //user history log
    }

    public function getAll($where = NULL)
    {
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_master_vendor_verification');
        return $query->result_array();
    }
      
    public function getById($id)
    {
        return $this->db->get_where($this->_table, array("id" => $id))->row();
    }

    public function vendor_verification_edit($data, $where)
    {
        $this->db->where($where);
        $this->db->update('eproc_master_vendor_verification',$data);

         //user history log
           // helper_log("update", "Update table eproc_mto_detail ".$data["description"]);
        //user history log
    }

    public function delete($id)
    {
        $status_delete = 1;
        //user history log
           helper_log("delete", "Update Delete Status Master Area data id ".$id." Status to ".$status_delete);
        //user history log
        
       return $this->db->update($this->_table, array("status_delete" => $status_delete), array("id" => $id));
    }

    function get_last_code(){
        $this->db->select('*');
        $this->db->from('eproc_master_vendor');
        $this->db->limit(1);
        $this->db->order_by('id',"DESC");
        return $this->db->get()->result();
    }

    public function data_catalog_category($where = NULL)
    {
        if($where){
            $query = $this->db->where($where);
        }
        $query = $this->db->get('eproc_catalog_category');
        return $query->result_array();
    }

    function search_pic_autocomplete($name){
        $this->db->like('full_name', $name);
        $this->db->group_by('full_name');
        $this->db->order_by('full_name', 'ASC');
        $this->db->limit(20);
        return $this->db->get('portal_user_db')->result_array();
    }

    //RECEIVING LIST DATATABLE -------------------------------------------
    var $table_vendor_verification_list_dt            = 'eproc_master_vendor_verification';
    var $column_order_vendor_verification_list_dt     = array('verification');
    var $column_search_vendor_verification_list_dt    = array('verification');
    var $order_vendor_verification_list_dt            = array('id' => 'desc'); // default order 

    private function _get_datatables_vendor_verification_list_dt_query()
    {
        $this->db->from($this->table_vendor_verification_list_dt);
        $this->db->where('status', 1);
               
        $i = 0;
        
        foreach ($this->column_search_vendor_verification_list_dt as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {
                
                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                
                if (count($this->column_search_vendor_verification_list_dt) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order_vendor_verification_list_dt[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if (isset($this->order_vendor_verification_list_dt))
        {
            $order = $this->order_vendor_verification_list_dt;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_vendor_verification_list_dt()
    {
        $this->_get_datatables_vendor_verification_list_dt_query();

        if ($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);

        $query = $this->db->get();
        return $query->result();
    }

    public function count_all_vendor_verification_list_dt()
    {
        $this->db->from($this->table_vendor_verification_list_dt);
        $this->db->where('status', 1);
        return $this->db->count_all_results();
    }

    function count_filtered_vendor_verification_list_dt()
    {
        $this->_get_datatables_vendor_verification_list_dt_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    //---------------------------------------------------------------------
}