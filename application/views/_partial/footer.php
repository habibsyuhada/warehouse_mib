    <footer class="container-fluid bg-white py-2">
      <div class="row">
        <div class="col-12 col-md">
          <center><small class="d-block my-1 text-muted">&copy; 2020 - WAREHOUSE MANAGEMENT SYSTEM </small></center>
        </div>
      </div>
    </footer>
    <script type="text/javascript">
      $('#dataTable').DataTable( {
         drawCallback: function () {
           console.log( 'Table redrawn '+new Date() );
         }
      } );

      $('.select2').select2({
        theme: 'bootstrap'
      });

      var delayTimer_sidebarCollapse;
    	function sidebarCollapse(){
        $('#sidebar').toggleClass('active');
        clearTimeout(delayTimer_sidebarCollapse);
        delayTimer_sidebarCollapse = setTimeout(function() {
          $.ajax({            
            url: "<?php echo base_url();?>home/sidebarCollapse/",
          });
        }, 500);
    	}

      <?php if($this->session->flashdata('success') == TRUE): ?>
      Swal.fire(
        'Success!',
        '<?php echo $this->session->flashdata('success'); ?>',
        'success'
      )
      <?php endif; ?>
      <?php if($this->session->flashdata('error') == TRUE): ?>
      Swal.fire(
        'Error!',
        '<?php echo $this->session->flashdata('error'); ?>',
        'error'
      )
      <?php endif; ?>
      $('form').attr('autocomplete', 'off');
      $('#sidebar').on('show.bs.collapse','.collapse', function() {
        $('#sidebar').find('.collapse.show').collapse('hide');
      });

      function sweetalert(type, text) {
        if(type == 'success'){
          Swal.fire(
            'Success!',
            text,
            'success'
          )
        }
        else if(type == 'error'){
          Swal.fire(
            'Error!',
            text,
            'error'
          )
        }
      }

      function goBack(){
        window.history.back();
      }

      // $("#source_copy").clone().appendTo("#topmenu");
      $("#topmenu").html($("#source_copy").html())

    </script>

    <!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Deleted Data Can Not Be Returned!</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
      </div>
    </div>
  </div>
</div>



  </body>
</html>

