<?php 
  error_reporting(0);
  if(empty($this->user_cookie[9]) || empty($read_cookies[9])){
    // redirect($read_cookies[9]);
    // echo '<script type="text/javascript">window.location.href="localhost/portal_git"</script>';
  }
?>
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title><?php echo $meta_title ?></title>


    <!-- <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon-proc.jpeg"/> -->
    <link rel="shortcut icon" href="<?php echo base_url();?>img/favicon.png"/>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/bootstrap/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url();?>assets/jquery/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/bootstrap.min.js"></script>

    <!-- Font Awasome -->
    <link href="<?php echo base_url();?>assets/fontawesome-free/css/all.min.css" rel="stylesheet">

    <!-- Datatable -->
    <link href="<?php echo base_url();?>assets/datatables/jquery.dataTables.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url();?>assets/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/datatables/ext/dataTables.fixedColumns.min.js"></script>

    <!-- SweetAlert2 -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/sweetalert2/sweetalert2.all.min.js"></script>
     <script type="text/javascript"  src="<?php echo base_url(); ?>assets/sweetalert2/sweetalert.min.js"></script>

    <!-- Jquery UI -->
    <link href="<?php echo base_url();?>assets/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url();?>assets/jquery-ui/jquery-ui.min.js"></script>

    <!-- CanvasJS -->
    <script src="<?php echo base_url();?>assets/canvasjs/canvasjs.min.js"></script>

    <!-- Highcharts -->
    <script src="<?php echo base_url();?>assets/highcharts/highcharts.js"></script>

    <!-- Jquery UI -->
    <link href="<?php echo base_url();?>assets/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url();?>assets/jquery-ui/jquery-ui.min.js"></script>

    <!-- DatePicker -->
    <link href="<?php echo base_url();?>assets/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <!-- Selct2 -->
    <link href="<?php echo base_url();?>assets/select2/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/select2/select2-bs4.css" rel="stylesheet">
    <script src="<?php echo base_url();?>assets/select2/select2.min.js"></script>

    <!-- Sidebar -->
    <link href="<?php echo base_url();?>assets/sidebar/style.css" rel="stylesheet">

    <!-- Bootstrap select -->
    <link href="<?php echo base_url();?>assets/bootstrap_select/bootstrap-select.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap_select/bootstrap-select.min.js"></script>

    <!-- Zebra -->
    <link href="<?php echo base_url();?>assets/dist_zebra_date_picker/css/default/zebra_datepicker.min.css" rel="stylesheet">
    <script type="text/javascript" src="<?= base_url() ?>assets/dist_zebra_date_picker/zebra_datepicker.min.js"></script>

    <!-- Dropzone -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/dropzone/dropzone.css">
    <script type="text/javascript" src="<?= base_url() ?>assets/dropzone/dropzone.js"></script>

    <style>

      #content{
        overflow: auto;
      }

      .bg-white{
        background: white;
      }
      .bg-aqua{
        background: #00c0ef;
      }
      .bg-yellow{
        background: #f39c12;
      }
      .bg-yellow-2{
        background: #f3e412;
      }
      .bg-orange {
        background-color: #FF851B !important;
      }
      .bg-green-smoe{
         /*background-color:#17467e;*/
        background-color:#008060;
      }
      .btn-flat{
        border-radius: 0px;
      }
      .btn-green-smoe{
         /*background-color:#17467e;
        border-color: #17467e*/; 

        /*/*background-color:#008060;*/
        /*border-color: #008060; */*/
      }
      .btn-green-smoe:hover{
        
       /* background-color:#17467e;
        border-color: #17467e; */

         background-color:#008060;
        border-color: #008060;

      }
      .dropdown-toggle.collapsed::after{
        border-left: .3em solid;
        border-top: .3em solid transparent;
        border-right: 0;
        border-bottom: .3em solid transparent;
      }
      .font-size-9{
        font-size: 0.9rem;
      }
    </style>

    <style type="text/css">
      /* The container-checkbox */
      .container-checkbox {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        /*font-size: 22px;*/
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      /* Hide the browser's default checkbox */
      .container-checkbox input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
      }

      /* Create a custom checkbox */
      .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 1.3rem;
        width: 1.3rem;
        background-color: #eee;
      }

      /* On mouse-over, add a grey background color */
      .container-checkbox:hover input ~ .checkmark {
        background-color: #ccc;
      }

      /* When the checkbox is checked, add a blue background */
      .container-checkbox input:checked ~ .checkmark {
        background-color: #2196F3;
      }

      /* Create the checkmark/indicator (hidden when not checked) */
      .checkmark:after {
        content: "";
        position: absolute;
        display: none;
      }

      /* Show the checkmark when checked */
      .container-checkbox input:checked ~ .checkmark:after {
        display: block;
      }

      /* Style the checkmark/indicator */
      .container-checkbox .checkmark:after {
        left: .5rem;
        top: .3rem;
        width: .4rem;
        height: .6rem;
        border: solid white;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
      }
    </style>

<script>
  function deleteConfirm(url){
      $('#btn-delete').attr('href', url);
      $('#deleteModal').modal();
  }
</script>
<script>
  $( function() {
    $( "#dpjq" ).datepicker({
        //isRTL: true,
        dateFormat: "yy-mm-dd 23:59:59",
        changeMonth: true,
        changeYear: true
      });
  } );
  </script>
  </head>