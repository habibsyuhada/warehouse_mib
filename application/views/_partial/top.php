<style type="text/css">
  #topbar-mobile{
    display: none;
  }
  @media (max-width: 767px) {
    #topbar-mobile{
      display: block;
    }
    #topbar-desktop{
      display: none;
    }
  }
  body, .form-control, .custom-select, .btn{
    font-size: 0.9rem
  }
}

.dropdown-menu {
  min-width: 200px;
}
.dropdown-menu.columns-2 {
  min-width: 400px;
}
.dropdown-menu.columns-3 {
  min-width: 600px;
}
.dropdown-menu li a {
  padding: 5px 15px;
  font-weight: 300;
}
.multi-column-dropdown {
  list-style: none;
  margin: 0px;
  padding: 0px;
}
.multi-column-dropdown li a {
  display: block;
  clear: both;
  line-height: 1.428571429;
  color: #333;
  white-space: normal;
}
.multi-column-dropdown li a:hover {
  text-decoration: none;
  color: #262626;
  background-color: #999;
}
 
@media (max-width: 767px) {
  .dropdown-menu.multi-column {
    min-width: 240px !important;
    overflow-x: hidden;
  }
}
</style>

<body style="background-color: whitesmoke;">
  <div class="bg-white">
    <div class="container-fluid" style="overflow: auto;">
      <div class="row py-3 align-items-center" style="overflow: auto">
        <div class="col-md">
          <!-- <img src="<?php echo base_url();?>img/main-logo.png" width="225"> -->
          <img src="<?php echo base_url();?>img/logo.png" width="225">
           
        </div>     
        <div class="col-md text-right">
           <a href="<?php echo base_url(); ?>user/profile" class="btn btn-flat btn-primary text-white">
            <i class="fa fa-user"></i> &nbsp;<?php echo $this->user_cookie[1]; ?> (<?php echo ($this->user_cookie[12] == 1 ? 'MEB' : ($this->user_cookie[12] == 2 ? 'DEB' : '-' )); ?>)
          </a>
          <a href='<?php echo base_url() ?>auth/logout' type="button" class="btn btn-flat btn-danger text-white">
            <i class="fa fa-sign-out"></i> Log Out
          </a>
        </a>
        </div>
      </div>
    </div>
  </div>
  
  <nav class="sticky-top py-1 bg-green-smoe container-fluid">
    
      <div class="row align-items-center" id="topbar-mobile">
        <div class="col-xl">
          <div class="row">
            <div class="col-md-auto d-flex justify-content-between">
              <div class="btn-group">
                <button type="button" class="btn btn-flat btn-dark text-white" onClick="sidebarCollapse();" <?php echo (isset($sidebar) ? '' : 'disabled'); ?>>
                  <i class="fas fa-align-justify"></i>
                </button>
              </div>
              <div id="idbtncol" class="btn-group">
                <button type="button" class="btn btn-flat btn-light text-black" data-toggle="collapse" data-target="#topmenu">
                  <i class="fas fa-align-justify"></i>
                </button>
              </div>
            </div>            
          </div>
          <div class="row collapse" id="topmenu">

          </div>
        </div> 
      </div>

      <div class="row align-items-center" id="topbar-desktop">
        <div class="col-xl">
          <div class="row">
            <div class="col-md-auto d-flex justify-content-between">
              <div class="btn-group">
                <button type="button" class="btn btn-flat btn-green-smoe text-white" onClick="sidebarCollapse();" <?php echo (isset($sidebar) ? '' : 'disabled'); ?>>
                  <i class="fas fa-align-justify"></i>
                </button>
              </div>
            </div>

            <div id="source_copy" class="col-md row">

            <div class="col-md-auto text-center">
              <div class="btn-group">
                <a href="<?php echo base_url();?>home/home" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-home"></i> &nbsp;Home
                </a>
              </div>
            </div>

            <?php if(in_array(1, array($this->permission_cookie[88],$this->permission_cookie[95],$this->permission_cookie[96],$this->permission_cookie[7],$this->permission_cookie[8],$this->permission_cookie[32],$this->permission_cookie[33]))){ ?>
            <div class="col-md-auto text-left">
              <div class="btn-group">
                <a href="<?php echo base_url();?>user" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-database"></i> &nbsp;Master Data
                </a>
              </div>
            </div>
            <?php } ?>

            <?php if($this->permission_cookie[1] == 1){ ?>
            <div class="col-md-auto text-left">
              <div class="btn-group">
                <a href="<?php echo base_url();?>budget" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-coins"></i> &nbsp;Budget
                </a>
              </div>
            </div>
            <?php } ?>

            <?php if($this->permission_cookie[26] == 1){ ?>
            <div class="col-md-auto text-center">
              <div class="btn-group">
                <a href="<?php echo base_url();?>material_catalog_list" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-business-time"></i> &nbsp;Material
                </a>
              </div>
            </div>
            <?php } ?>

            <?php if($this->permission_cookie[19] == 1){ ?>
            <div class="col-md-auto text-center">
              <div class="btn-group">
                <a href="<?php echo base_url(); ?>mr" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-dolly-flatbed"></i> &nbsp;MR
                </a>
              </div>
            </div>
            <?php } ?>

            <?php if($this->permission_cookie[52] == 1){ ?>
            <div class="col-md-auto text-center">
              <div class="btn-group">
                <a href="<?php echo base_url(); ?>po" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-calculator"></i> &nbsp;PO
                </a>
              </div>
            </div>
            <?php } ?>

            <?php if($this->permission_cookie[59] == 1){ ?>
            <div class="col-md-auto text-center">
              <div class="btn-group">
                <a href="<?php echo base_url();?>receiving_list/waiting" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-truck"></i> &nbsp;Receiving
                </a>
              </div>
            </div>
            <?php } ?>

            <?php if($this->permission_cookie[66] == 1){ ?>
            <!-- <div class="col-md-auto text-center">
              <div class="btn-group">
                <a href="<?php echo base_url();?>mrir/mrir_data/request" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-tasks"></i> &nbsp;MRIR
                </a>
              </div>
            </div> -->
            <?php } ?>

            <?php if($this->permission_cookie[73] == 1){ ?>
            <div class="col-md-auto text-center">
              <div class="btn-group">
                <a href="<?php echo base_url();?>osd" class="btn btn-flat btn-green-smoe text-white">
                  <i class="fas fa-list-alt"></i> &nbsp;O S & D
                </a>
              </div>
            </div>
            <?php } ?>

            </div>

          </div>
        </div>

        <div class="col-xl-auto text-center">
          <div class="btn-group">
            <button type="button" class="btn btn-flat btn-green-smoe text-white">
              <span class="text-white"><?php echo date("l, d F Y"); ?></span>
            </button>
          </div>
        </div> 
      </div>
    

  </nav>