<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>budget/budgetCategoryEditProcess">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Account</label>
                <div class="col-sm-10">
                  <input type="text" name="account_no" class="form-control" value="<?php echo $category['account_no'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Category <?php echo $category['assetable'] ?></label>
                <div class="col-sm-10">
                  <select class="custom-select" name="assetable" required>
                    <option value="Capex" <?php echo ($category['assetable'] == 'Capex' ? 'selected' : '') ?>>Capex</option>
                    <option value="Opex" <?php echo ($category['assetable'] == 'Opex' ? 'selected' : '') ?>>Opex</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Account Description</label>
                <div class="col-sm-10">
                  <input type="text" name="category_name" class="form-control" value="<?php echo $category['category_name'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="status_delete">
                    <option value="1" <?php echo (1 == $category['status_delete'] ? 'selected' : '') ?>>Actived</option>
                    <option value="0" <?php echo (0 == $category['status_delete'] ? 'selected' : '') ?>>Disactived</option>
                  </select>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <input type="hidden" name="id" value="<?php echo $category['id'] ?>" required>
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url() ?>budget/budgetCategoryList" class="btn btn-danger" title="Submit"><i class="fa fa-times"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
</script>