<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">

            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center dataTable">
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>Account</th>
                  <th>Category</th>
                  <th>Account Description</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($all_list as $key => $all): ?>
                  <tr>
                    <td><?php echo $all['account_no'] ?></td>
                    <td><?php echo $all['assetable'] ?></td>
                    <td><?php echo $all['category_name'] ?></td>
                    <td><?php echo ($all['status_delete'] == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-danger">Disactived</span>') ?></td>
                    <td>
                      <?php if($this->permission_cookie[9] == 1){ ?>
                      <a href="<?php echo base_url() ?>budget/budgetCategoryEdit/<?php echo strtr($this->encryption->encrypt($all['id']), '+=/', '.-~') ?>" class="btn btn-flat btn-warning"><i class="fa fa-edit"></i> Edit</a>
                      <?php } ?>
                      <?php if($this->permission_cookie[10] == 1){ ?>
                      <a href="<?php echo base_url() ?>budget/budgetCategoryDeleteProcess/<?php echo strtr($this->encryption->encrypt($all['id']), '+=/', '.-~') ?>" onclick="return confirm('Are you sure?')" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Delete</a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('.dataTable').DataTable({
    
  });
</script>