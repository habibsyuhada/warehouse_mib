<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>budget/budgetDetailEditProcess">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Year</label>
                <div class="col-sm-10">
                  <input type="text" name="year" class="form-control yearpicker" value="<?php echo $budget['year'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Department</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="dept">
                    <?php foreach($dept_list as $dept): ?>
                    <option value="<?php echo $dept['id_department'] ?>" <?php echo ($dept['id_department'] == $budget['dept'] ? 'selected' : '') ?>><?php echo $dept['name_of_department'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Category</label>
                <div class="col-sm-10">
                  <select class="custom-select" onchange="load_account();" name="assetable">
                    <option value="Capex" <?php echo ("Capex" == $budget['assetable'] ? 'selected' : '') ?>>Capex</option>
                    <option value="Opex" <?php echo ("Opex" == $budget['assetable'] ? 'selected' : '') ?>>Opex</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Account</label>
                <div class="col-sm-10">
                  <select class="form-control select2" name="category" required>
                    <?php foreach($cat_list as $cat): ?>
                      <?php if($cat['assetable'] == $budget['assetable']): ?>
                        <option value="<?php echo $cat['id'] ?>" <?php echo ($cat['id'] == $budget['id_category'] ? 'selected' : '') ?>><?php echo $cat['account_no'] ?> - <?php echo $cat['category_name'] ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Item</label>
                <div class="col-sm-10">
                  <input type="text" name="description" class="form-control" value="<?php echo $budget['description'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Amount</label>
                <div class="col-sm-10">
                  <input type="number" name="budget" class="form-control" value="<?php echo $budget['budget'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Remarks</label>
                <div class="col-sm-10">
                  <input type="text" name="remarks" class="form-control" value="<?php echo $budget['remarks'] ?>" required>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <input type="hidden" name="id" value="<?php echo $budget['id'] ?>" required>
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url() ?>budget/budgetList" class="btn btn-danger" title="Submit"><i class="fa fa-times"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $(".yearpicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years",
    orientation: "bottom left",
    autoclose: true
  });

  function load_account() {
    var assetable = $("select[name=assetable]").val();
    $.ajax({
      url: "<?php echo base_url();?>budget/load_account/"+assetable,
      success: function(data) {
        var data = JSON.parse(data);
        var html = "<option>---</option>";
        data.forEach(function( row ) {
          html += "<option value='"+row['id']+"'>"+row['account_no']+" - "+row['category_name']+"</option>"
        });
        $('select[name=category]').select2("destroy");
        $('select[name=category]').html(html);
        $('select[name=category]').select2({
          theme: 'bootstrap'
        });
      }
    });
  }
</script>