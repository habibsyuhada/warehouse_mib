<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>budget/budgetDetailImportPreview" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <!-- <div class="form-group row">
                <label class="col-sm-2 col-form-label">Year</label>
                <div class="col-sm-10">
                  <input type="text" name="year" class="form-control yearpicker" value="<?php echo ($filter[0] == "" ? '2020' : $filter[0]) ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Department</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="dept">
                    <?php foreach($dept_list as $dept): ?>
                    <option value="<?php echo $dept['id_department'] ?>" <?php echo ($dept['id_department'] == $filter[1] ? 'selected' : '') ?>><?php echo $dept['name_of_department'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Category</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="category">
                    <?php foreach($cat_list as $cat): ?>
                    <option value="<?php echo $cat['id'] ?>" <?php echo ($cat['id'] == $filter[2] ? 'selected' : '') ?>><?php echo $cat['category_name'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
              <hr> -->
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Template Excel</label>
                <div class="col-sm-10 col-form-label">
                  <a href="<?php echo base_url(); ?>/file/budget/Template_Import_Budget.xlsx">Template_Import_Budget.xlsx</a>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Upload</label>
                <div class="col-sm-10">
                  <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" onChange="$('#label1.custom-file-label').html($(this).val())" required>
                    <label id="label1" class="custom-file-label">Choose file</label>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $(".yearpicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years",
    orientation: "bottom left",
    autoclose: true
  });
</script>