<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>budget/budgetDetailImportProcess">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-hover text-center">
                <thead class="bg-success text-white">
                  <tr>
                    <th>Year</th>
                    <th>Department</th>
                    <th>Account</th>
                    <th>Account Description</th>
                    <th>Item</th>
                    <th>Amount</th>
                    <th>Remarks</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no=1; 
                    foreach($sheet as $row):
                      if($no > 1): 

                        $status = '';
                        $disabled = 0;
                       
                        if(!isset($dept_list[$row['B']])){
                          $status = 'Department Not Found';
                          $disabled = 1;
                        }
                        if(!isset($cat_list[$row['C']])){
                          $status = 'Budget Category Not Found';
                          $disabled = 1;
                        }
                        if(!isset($row['A']) || $row['A'] =="" || !isset($row['B']) || $row['B'] =="" || !isset($row['C']) || $row['C'] =="" || !isset($row['D']) || $row['D'] =="" || !isset($row['E']) || $row['E'] ==""){
                          $status = 'Your is not Complete';
                          $disabled = 2;
                        }

                       if($row['A'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td class="align-middle">
                      <input type="text" name="year[]" class="form-control" value="<?php echo $row['A'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" name="dept_preview[]" class="form-control" value="<?php echo (isset($dept_list[$row['B']]) ? $row['B'] : '-') ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="hidden" name="dept[]" class="form-control" value="<?php echo (isset($dept_list[$row['B']]) ? $dept_list[$row['B']]['id_department'] : '-') ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" name="category_preview[]" class="form-control" value="<?php echo (isset($cat_list[$row['C']]) ? $row['C'] : '-') ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="hidden" name="category[]" class="form-control" value="<?php echo (isset($cat_list[$row['C']]) ? $cat_list[$row['C']]['id'] : '-') ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" name="description[]" class="form-control" value="<?php echo $row['D'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" name="budget[]" class="form-control" value="<?php echo $row['E'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" name="remarks[]" class="form-control" value="<?php echo $row['F'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle"><?php echo $status ?></td>
                  </tr>
                  <?php } endif; ?>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>
              <input type="hidden" name="drawing_type" class="form-control" value="<?php echo $drawing_type ?>">

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>activity/activity_import" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
</script>