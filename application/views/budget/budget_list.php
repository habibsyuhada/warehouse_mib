<?php 
  $f_year = $this->input->get('year');
  $f_dept = $this->input->get('dept');
  $f_category = $this->input->get('category');
  $f_assetable = $this->input->get('assetable');
?>
<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">
    
    <div class="col-md-12" id="container-filter">
      <form method="get">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Filter</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Year</label>
                <div class="col-sm-10">
                  <input onchange="change_filter();" oninput="change_filter();" type="text" name="year" class="form-control yearpicker" value="<?php echo ($f_year == '' ? '2020' : $f_year) ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Department</label>
                <div class="col-sm-10">
                  <select onchange="change_filter();" class="custom-select" name="dept">
                    <option value="">---</option>
                    <?php foreach($dept_list as $dept): ?>
                    <option value="<?php echo $dept['id_department'] ?>" <?php echo ($dept['id_department'] == $f_dept ? 'selected' : '') ?>><?php echo $dept['name_of_department'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Category</label>
                <div class="col-sm-10">
                  <select onchange="change_filter();load_account()" class="custom-select" name="assetable">
                    <option value="">---</option>
                    <option value="Capex" <?php echo ("Capex" == $f_assetable ? 'selected' : '') ?>>Capex</option>
                    <option value="Opex" <?php echo ("Opex" == $f_assetable ? 'selected' : '') ?>>Opex</option>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Account</label>
                <div class="col-sm-10">
                  <select onchange="change_filter();" class="form-control select2" name="category">
                    <option value="">---</option>
                    <!-- <?php foreach($cat_list as $cat): ?>
                    <option value="<?php echo $cat['id'] ?>" <?php echo ($cat['id'] == $f_category ? 'selected' : '') ?>><?php echo $cat['account_no'] ?> - <?php echo $cat['category_name'] ?></option>
                    <?php endforeach; ?> -->
                  </select>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" class="btn btn-flat btn-info" title="Submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>
    </div>

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="p-3 my-3 bg-light border">
              <div class="row">
                <div class="col-sm-12 col-md">
                  <button type="button" class="btn btn-flat btn-primary" id="total_amount">Total Amount : IDR 1000000</button>
                </div>
                <div class="col-sm-12 col-md-auto">
                  <?php if($this->permission_cookie[2] == 1){ ?>
                  <a name="add_data" href="<?php echo base_url();?>budget/budgetDetailNew/<?php echo $f_year.'-'.$f_dept.'-'.$f_category.'-'.$f_assetable ?>" class="btn btn-flat btn-info"><i class="fa fa-plus"></i> Add Data</a>
                  <?php } ?>
                  <?php if($this->permission_cookie[5] == 1){ ?>
                  <a href="<?php echo base_url();?>budget/budgetDetailImport/<?php echo $f_year.'-'.$f_dept.'-'.$f_category ?>" class="btn btn-flat btn-info"><i class="fa fa-file-import"></i> Import Data</a>
                  <?php } ?>
                </div>
              </div>
            </div>

            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center dataTable">
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>Year</th>
                  <th>Department</th>
                  <th>Category</th>
                  <th>Account</th>
                  <th>Account Description</th>
                  <th>Item</th>
                  <th>Amount</th> 
                  <th>Remarks</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                  $total_amount = 0;
                  foreach ($all_list as $key => $all): 
                    $total_amount += $all['budget'];
                ?>
                  <tr>
                    <td><?php echo $all['year'] ?></td>
                    <td><?php echo $all['name_of_department'] ?></td>
                    <td><?php echo $all['assetable'] ?></td>
                    <td><?php echo $all['account_no'] ?></td>
                    <td><?php echo $all['category_name'] ?></td>
                    <td><?php echo $all['description'] ?></td>
                    <td>IDR <?php echo number_format($all['budget'],2,",",".") ?></td>
                    <td><?php echo $all['remarks'] ?></td>
                    <td>
                      <?php if($this->permission_cookie[3] == 1){ ?>
                      <a href="<?php echo base_url() ?>budget/budgetDetailEdit/<?php echo strtr($this->encryption->encrypt($all['id']), '+=/', '.-~') ?>" class="btn btn-flat btn-warning"><i class="fa fa-edit"></i> Edit</a>
                      <?php } ?>
                      <?php if($this->permission_cookie[4] == 1){ ?>
                      <a href="<?php echo base_url() ?>budget/budgetDetailDeleteProcess/<?php echo strtr($this->encryption->encrypt($all['id']), '+=/', '.-~') ?>" onclick="return confirm('Are you sure?')" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Delete</a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('.dataTable').DataTable({
    
  });
  $('#total_amount').text('Total Amount: IDR <?php echo number_format($total_amount, 2,",",".") ?>');
  $(".yearpicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years",
    orientation: "bottom left",
    autoclose: true
  });

  function change_filter() {
    var year = $("#container-filter input[name=year]").val();
    var dept = $("#container-filter select[name=dept]").val();
    var category = $("#container-filter select[name=category]").val();
    var assetable = $("#container-filter select[name=assetable]").val();

    $('a[name=add_data]').attr("href", "<?php echo base_url();?>budget/budgetDetailNew/"+year+"-"+dept+"-"+category+"-"+assetable);
  }

  function load_account() {
    var assetable = $("select[name=assetable]").val();
    $.ajax({
      url: "<?php echo base_url();?>budget/load_account/"+assetable,
      success: function(data) {
        var data = JSON.parse(data);
        var html = "<option>---</option>";
        data.forEach(function( row ) {
          html += "<option value='"+row['id']+"'>"+row['account_no']+" - "+row['category_name']+"</option>"
        });
        $('select[name=category]').select2("destroy");
        $('select[name=category]').html(html);
        $('select[name=category]').select2({
          theme: 'bootstrap'
        });
      }
    });
  }
</script>