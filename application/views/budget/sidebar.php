<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) || $sidebar_close == 1 ? 'active' : '') ?>">
  <ul class="list-unstyled components">

    <!-- <li>
      <a href="<?php //echo base_url();?>mb/mb_list">
        <i class="fas fa-pallet"></i> &nbsp; Material Balance
      </a>
    </li> -->
    <?php if($this->permission_cookie[1] == 1){ ?>
    <li>
      <a href="<?php echo base_url();?>budget/budgetList">
        <i class="fas fa-coins"></i> &nbsp; Budget List
      </a>
    </li>
    <?php } ?>

    <?php if($this->permission_cookie[13] == 1 || $this->permission_cookie[14] == 1){ ?>
    <li>
      <a href="#Submenu1" data-parent="#sidebar" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
        <i class="fas fa-exchange-alt"></i> &nbsp; Transfer Budget
      </a>
      <ul class="list-unstyled collapse show" id="Submenu1">
        <?php if($this->permission_cookie[13] == 1){ ?>
        <li>
          <a href="<?= base_url();?>budget/transferList"><i class="fas fa-list"></i> &nbsp; Transfer Budget List</a>
        </li>
        <?php } ?>
        <?php if($this->permission_cookie[14] == 1){ ?>
        <li>
          <a href="<?= base_url();?>budget/transferDetailNew"><i class="fa fa-plus"></i> &nbsp; Add New Transfer Budget</a>
        </li>
      </ul>
      <?php } ?>
    </li>
    <?php } ?>

  </ul>
</nav>