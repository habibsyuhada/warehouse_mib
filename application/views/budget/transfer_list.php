<?php 
  $f_year = $this->input->get('year');
  $f_dept = $this->input->get('dept');
  $f_category = $this->input->get('category');
?>
<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">

            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center dataTable">
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>Year</th>
                  <th>Category</th>
                  <th>From Account</th>
                  <th>From Department</th>
                  <th>To Account</th>
                  <th>To Department</th>
                  <th>Amount</th> 
                  <th>Remarks</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($transfer_list as $key => $transfer): ?>
                  <tr>
                    <td><?php echo $transfer['year'] ?></td>
                    <td><?php echo $transfer['assetable'] ?></td>
                    <td><?php echo $cat_list[$transfer['from_category']]['account_no'] ?> - <?php echo $cat_list[$transfer['from_category']]['category_name'] ?></td>
                    <td><?php echo $dept_list[$transfer['from_dept']]['name_of_department'] ?></td>
                    <td><?php echo $cat_list[$transfer['to_category']]['account_no'] ?> - <?php echo $cat_list[$transfer['to_category']]['category_name'] ?></td>
                    <td><?php echo $dept_list[$transfer['to_dept']]['name_of_department'] ?></td>
                    <td>IDR <?php echo number_format($transfer['budget'],2,",",".") ?></td>
                    <td><?php echo $transfer['remarks'] ?></td>
                    <td>
                      <?php if($this->permission_cookie[15] == 1){ ?>
                      <a href="<?php echo base_url() ?>budget/transferDetailEdit/<?php echo strtr($this->encryption->encrypt($transfer['id']), '+=/', '.-~') ?>" class="btn btn-flat btn-warning"><i class="fa fa-edit"></i> Edit</a>
                      <?php } ?>
                      <?php if($this->permission_cookie[16] == 1){ ?>
                      <a href="<?php echo base_url() ?>budget/transferDetailDeleteProcess/<?php echo strtr($this->encryption->encrypt($transfer['id']), '+=/', '.-~') ?>" onclick="return confirm('Are you sure?')" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Delete</a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('.dataTable').DataTable({
    'order': []
  });

  $(".yearpicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years",
    orientation: "bottom left",
    autoclose: true
  });

  function change_filter() {
    var year = $("#container-filter input[name=year]").val();
    var dept = $("#container-filter select[name=dept]").val();
    var category = $("#container-filter select[name=category]").val();

    $('a[name=add_data]').attr("href", "<?php echo base_url();?>budget/budgetDetailNew/"+year+"-"+dept+"-"+category);
  }
</script>