<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>department/departmentDetailEditProcess">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Name Of Department</label>
                <div class="col-sm-10">
                  <input type="text" name="name_of_department" class="form-control" value="<?php echo $department['name_of_department'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="status">
                    <option value="1" <?php echo (1 == $department['status'] ? 'selected' : '') ?>>Actived</option>
                    <option value="0" <?php echo (0 == $department['status'] ? 'selected' : '') ?>>Disactived</option>
                  </select>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <input type="hidden" name="id_department" value="<?php echo $department['id_department'] ?>" required>
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url() ?>department/departmentList" class="btn btn-danger" title="Submit"><i class="fa fa-times"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
</script>