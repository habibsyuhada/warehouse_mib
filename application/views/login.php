
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" >
	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>
			Warehouse Portal | <?php echo $meta_title;?>
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--begin::Web font -->
		<script src="<?php echo base_url();?>assets/js/webfont.js"></script>
		
		<!--end::Web font -->
        <!--begin::Base Styles -->
		<!-- <link href="<?php //echo base_url();?>content2/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" /> -->
		<link href="<?php echo base_url();?>assets/login/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Base Styles -->
		<link rel="shortcut icon" href="<?php echo base_url();?>assets/login/favicon.png"/>
		 <script src="<?php echo base_url();?>assets/js/sweetalert.min.js"></script>
		
		 <style>
		 	body {
		 		font-family: 'Poppins', sans-serif;	
		 	}
		 	.carousel-item {
  				height: 100vh;
  				min-height: 350px;
  				background: no-repeat center center scroll;
  				-webkit-background-size: cover;
  				-moz-background-size: cover;
 				-o-background-size: cover;
  				background-size: cover;
			}

			.carousel-caption {
				top: 40%;
				transform: translateY(-40%);
				bottom: initial;
				-webkit-transform-style: preserve-3d;
				-moz-transform-style: preserve-3d;
				transform-style: preserve-3d;
			}
					
		 </style>

		  <script src="<?php echo base_url();?>assets/jquery/jquery.slim.js"></script>
		  <script src="<?php echo base_url();?>assets/bootstrap/bootstrap.min.js"></script>
		  
	</head>
	<!-- end::Head -->
    <!-- end::Body -->
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="#">
										<img src="<?php echo base_url();?>assets/login/logo.png" width="100%">
									</a>
								</div>
								<div class="m-login__signin">
									<div class="m-login__head">
										<h3 class="m-login__title">
											Sign In To Portal
											<?php  echo $this->session->flashdata('message');?>
										</h3>

									</div>
									<form class="m-login__form m-form" action="<?php echo base_url();?>auth/checking" method="post">
										<div class="form-group m-form__group">
											<select name="nama_pt" class="form-control pb-0">
												<option value="1">MEB</option>
												<option value="2">DEB</option>
											</select>
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input" type="email" placeholder="Email" name="email" autocomplete="off" required autofocus>
										</div>
										<div class="form-group m-form__group">
											<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" required>
										</div>
										<div class="row m-login__form-sub">
											<div class="col m--align-left">
												
											</div>
											<div class="col m--align-right">
												
											</div>
										</div>
										<div class="m-login__form-action">
											<button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
												Sign In
											</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="m-stack__item m-stack__item--center">
							<div class="m-login__account">
								
							</div>
						</div>
					</div>
				</div>
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1">
					
						
							<!-- Navigation -->


  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <!-- <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> -->
    </ol>
    <div class="carousel-inner" role="listbox">

      <?php $no=1; foreach ($slider as $key => $value) { ?>
      
      	<!-- Slide One - Set the background image for this slide in the line below -->
      		<!-- <div class="carousel-item <?php if($no == 1 ){ echo 'active'; } ?>" style="background-image: url(<?php echo base_url();?>assets/login/<?php echo $value['filename']; ?>)">
        		<div class="carousel-caption d-none d-md-block">
          			<h2 class="display-4"><?php //echo $value['title']; ?></h2>
          			<p class="lead"><?php //echo $value['description']; ?></p>
       	 		</div>
      		</div> -->
 	
      <?php $no++; } ?>

    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>


<!-- Page Content -->

						
				</div>
			</div>
		</div>

		

	</body>
	<!-- end::Body -->
</html>
