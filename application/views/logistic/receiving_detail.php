<style type="text/css">
  .nav-link{
    color: #000;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #007bff;
    background: #fff;
    border-bottom: 2px solid #007bff;
    border-radius: 0px;
  }
</style>

<!-- <input type="hidden" name="mto_id" value="<?= $receiving_list['mto_id'] ?>"> -->
<div id="content" class="container-fluid">
 
  <div class="row">

    <div class="col-md-12">
       <div class="overflow-auto text-muted  ">
          <h6>
            <ol class="breadcrumb bg-white">Supply Number : <?= $receiving_list['supply_number'] ?></ol>
          </h6>
       </div>
    </div>

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == '' ? 'active' : '') ?>" data-toggle="pill" href="#pills-receiving">Receiving Detail</a>
          </li>

          <?php if($read_permission[2] == 1){ ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'rd' ? 'active' : '') ?>" data-toggle="pill" href="#pills-detail">Material List</a>
          </li>

          <?php } ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'ird' ? 'active' : '') ?>" data-toggle="pill" href="#pills-import-detail">Import Receiving Detail</a>
          </li>

        </ul>
        <div class="overflow-auto media text-muted py-3 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="tab-content">

              <!-- RECEIVING MASTER -->
              <div class="tab-pane fade <?php echo ($t == '' ? 'show active' : '') ?>" id="pills-receiving">
                <form method="POST" id="receiving_master" action="<?php echo base_url();?>receiving/update_receiving_process">

                  <input type="hidden" name="id_receiving_master" value="<?= $receiving_list['receiving_id'] ?>">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Supply Number :</label>
                        <div class="col-xl">
                          <input type="text" name="supply_number" class="form-control" value="<?= $receiving_list['supply_number'] ?>" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Shipment Number :</label>
                        <div class="col-xl">
                          <input type="text" name="shipping_number" class="form-control" value="<?= $receiving_list['shipping_number'] ?>" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">PSA Date :</label>
                        <div class="col-xl">
                          <input type="text" name="date_psa" class="form-control datepicker" value="<?= date('d-m-Y', strtotime($receiving_list['date_psa'])) ?>" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">FSA Date :</label>
                        <div class="col-xl">
                          <input type="text" name="date_fsa" class="form-control datepicker" value="<?= date('d-m-Y', strtotime($receiving_list['date_psa'])) ?>" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Vendor / Supplier Name :</label>
                        <div class="col-xl">
                          <input type="text" name="vendor" class="form-control" value="<?= $receiving_list['vendor'] ?>" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">CIPL :</label>
                        <div class="col-xl">
                          <input type="text" name="cipl" class="form-control" value="<?= $receiving_list['cipl'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">BL / AWB :</label>
                        <div class="col-xl">
                          <input type="text" name="srn" class="form-control" value="<?= $receiving_list['srn'] ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Shipping Line :</label>
                        <div class="col-xl">
                          <input type="text" name="shipping_line" class="form-control" value="<?= $receiving_list['shipping_line'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Vessel / Flight :</label>
                        <div class="col-xl">
                          <input type="text" name="vessel_or_flight" class="form-control" value="<?= $receiving_list['vessel_or_flight'] ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">POL :</label>
                        <div class="col-xl">
                          <input type="text" name="pol" class="form-control" value="<?= $receiving_list['pol'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">POD :</label>
                        <div class="col-xl">
                          <input type="text" name="pod" class="form-control" value="<?= $receiving_list['pod'] ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Total Package :</label>
                        <div class="col-xl">
                          <input type="number" name="package_total" class="form-control" value="<?= $receiving_list['package_total'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Weight (Kg) :</label>
                        <div class="col-xl">
                          <input type="number" name="weight" class="form-control" value="<?= $receiving_list['weight'] ?>">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Measurement (CBM) :</label>
                        <div class="col-xl">
                          <input type="number" name="measurements" class="form-control" value="<?= $receiving_list['measurements'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>

                  <br>

                  <label><b>Material Package</b></label>
                  <div class="py-3 border-top border-gray">
                    <div class="container-fluid">
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <input type="text" class="form-control" name="material_package" placeholder='Material Package' readonly>
                        </div>
                      </div>
                    </div>
                  </div>

                  <label><b>PO</b></label>
                  <div class="py-3 border-top border-gray">
                    <div class="container-fluid">
                      <?php 
                        $arr_po = explode(';', $receiving_list['po_list']);
                        for($i=0; $i<sizeof($arr_po)-1; $i++){
                      ?>
                      <div class="form-row">
                        <div class="form-group col-md-12">
                          <input type="text" class="form-control" name="po[]" placeholder='PO' value="<?= $arr_po[$i] ?>" readonly>
                        </div>
                      </div>
                      <?php } ?>
                    </div>
                  </div>

                  <label><b>Container & Cargo Type</b></label>
                  <div class="py-3 border-bottom border-top border-gray">
                    <div class="row">
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">20"GP :</label>
                          <div class="col-xl">
                            <input type="number" name="gp_20" class="form-control" value="<?= $receiving_list['gp_20'] ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">40"GP :</label>
                          <div class="col-xl">
                            <input type="number" name="gp_40" class="form-control" value="<?= $receiving_list['gp_40'] ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">45"GP :</label>
                          <div class="col-xl">
                            <input type="number" name="gp_45" class="form-control" value="<?= $receiving_list['gp_45'] ?>" >
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">20"OT :</label>
                          <div class="col-xl">
                            <input type="number" name="ot_20" class="form-control" value="<?= $receiving_list['ot_20'] ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">40"OT :</label>
                          <div class="col-xl">
                            <input type="number" name="ot_40" class="form-control" value="<?= $receiving_list['ot_40'] ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">

                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">20"FR :</label>
                          <div class="col-xl">
                            <input type="number" name="fr_20" class="form-control" value="<?= $receiving_list['fr_20'] ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">40"FR :</label>
                          <div class="col-xl">
                            <input type="number" name="fr_40" class="form-control" value="<?= $receiving_list['fr_40'] ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">Other :</label>
                          <div class="col-xl">
                            <input type="text" name="other" class="form-control" value="<?= $receiving_list['other'] ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">

                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="py-3 border-top border-gray">
                    <div class="row">
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">ETD :</label>
                          <div class="col-xl">
                            <input type="text" name="etd" class="form-control datepicker" value="<?= ($receiving_list['etd'] != '0000-00-00' ? date('d-m-Y', strtotime($receiving_list['etd'])) : '') ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">ETA :</label>
                          <div class="col-xl">
                            <input type="text" name="eta" class="form-control datepicker" value="<?= ($receiving_list['eta'] != '0000-00-00' ? date('d-m-Y', strtotime($receiving_list['eta'])) : '') ?>" >
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">ATD :</label>
                          <div class="col-xl">
                            <input type="text" name="atd" class="form-control datepicker" value="<?= ($receiving_list['atd'] != '0000-00-00' ? date('d-m-Y', strtotime($receiving_list['atd'])) : '') ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md">
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">ATA :</label>
                          <div class="col-xl">
                            <input type="text" name="ata" class="form-control datepicker" value="<?= ($receiving_list['ata'] != '0000-00-00' ? date('d-m-Y', strtotime($receiving_list['ata'])) : '') ?>" >
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <?php //if($read_permission[5] == 1){ ?>
                            <button type="submit" name='submit_receiving' value='submit' class="btn btn-success" title="Submit"><i class="fa fa-edit"></i> Update</button>
                            <a href="<?= base_url() ?>receiving_list" class="btn btn-secondary" title="Cancel"><i class="fas fa-arrow-left"></i> Cancel</a>
                          <?php //} ?>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
              <!-- RECEIVING MASTER END -->

              <!-- RECEIVING DETAIL -->
              <div class="tab-pane fade <?php echo ($t == 'rd' ? 'show active' : '') ?>" id="pills-detail">
                <!-- TABLE  -->
                <label><b>Material Receiving List</b></label>
                <table class="table dataTable table-hover text-center">
                  <thead>
                    <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                      <th><input type="checkbox" id="selectall_material" style="width: 15px; height: 15px; text-align: center;"></th>
                      <th>PO REFERENCE</th>
                      <th>MATERIAL CODE</th>
                      <th>DESCRIPTION</th>
                      <th>SIZE (MM)</th>
                      <th>UOM</th>
                      <th>QTY</th>
                      <th>CURRENCY</th>
                      <th>EXPECTED COST IN</th>
                      <th>TOTAL AMOUNT</th>
                      <th>REASON / PURPOSE</th>
                      <th>REMARKS</th>
                      <th>NSI NO.</th>
                    </tr>
                  </thead>
                  <tbody id="show_data">
                    
                  </tbody>
                </table>
              </div>
              <!-- RECEIVING DETAIL END -->

              <!-- Import Drawing Detail -->
              <div class="tab-pane fade <?php echo ($t == 'ird' ? 'show active' : '') ?>" id="pills-import-detail">
                
                <form method="POST" action="<?php echo base_url();?>receiving_detail_preview" enctype="multipart/form-data">
                  <input type="hidden" name="master_receiving_id" value="<?= $master_receiving_id ?>">

                  <div class="container-fluid">
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Template</label>
                      <div class="col-sm-10 col-form-label">
                        <a href="<?php echo base_url(); ?>file/receiving/Template_Import_Receiving_Detail.xlsx">Template_Import_Receiving_Detail.xlsx</a>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Upload</label>
                      <div class="col-sm-10">
                        <div class="custom-file">
                          <input type="file" name="file" class="custom-file-input" required onChange="$('#label1.custom-file-label').html($(this).val())">
                          <label id="label1" class="custom-file-label">Choose file</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="text-right mt-3">
                    <button type="submit" name='submit_import' class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                  </div>
                </form>

              </div>
              <!-- Import Drawing Detail END -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<!-- GENERAL -->
<script type="text/javascript">

  //GET FIRST VALUE =============================================================
  var first_value;
  function save_first_value(cell){
    var value = $(cell).html();
    first_value = value;
  }
  //=============================================================================

  //MTO DETAIL EDIT ====================================================================
  function receiving_edit(cell, param){

    if(param == 'receiving_detail'){
      _url = "<?php echo base_url();?>receiving/receiving_detail_edit_process";
    } else {
      _url = "<?php echo base_url();?>receiving/receiving_qty_edit_process";
    }

    var col = $(cell).data("col");
    var id = $(cell).data("id");
    var value = $(cell).html();
    if(value != first_value){
      Swal.fire({
        title: 'Are you sure to <b class="text-warning">&nbsp;Edit&nbsp;</b> this?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: _url,
            type: "post",
            data: {
              col: col,
              id: id,
              value: value
            }, success(data){
              Swal.fire(
                'Success!',
                'Your data has been changed.',
                'success'
              )
            }
          });
        }
        else{
          $(cell).html(first_value);
        }
      })
    }
  }
  //=============================================================================

  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true,
  })
  // .datepicker("setDate", new Date());

  function submit_action(param){

    if(param == 'receiving_detail'){
      var _url = '<?= base_url() ?>receiving/receiving_detail_add_process';
      var _status =  $('.detail_required').filter(function(){return this.value==''}).length;
    } else {
      var _url = '<?= base_url() ?>receiving/receiving_qty_add_process';
      var _status =  $('.qty_required').filter(function(){return this.value==''}).length;
    }

    if(_status > 0){
      Swal.fire(
        'Error!',
        'Please Complete the form',
        'error'
      );
      return false;
    }

    $.ajax({
      data: $('#'+param).serialize(),
      url: _url,
      type: "post",
      success : function(data){
        $('#'+param).trigger('reset');
        
        Swal.fire(
          'Success!',
          'Data Successful Added',
          'success'
        )

        if(param == 'receiving_detail'){
          show_receiving_detail();
        } else {
          show_receiving_qty();
        }
        
      }
    });
  }

  function delete_action(param, id){

    if(param == 'receiving_detail'){
      var _url = '<?= base_url() ?>receiving/receiving_detail_delete_process';
    } else {
      var _url = '<?= base_url() ?>receiving/receiving_qty_delete_process';
    }

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          data: {
            'id' : id,
          },
          url: _url,
          type: "post",
          success : function(data){

            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )

            if(param == 'receiving_detail'){
              show_receiving_detail();
            } else {
              show_receiving_qty();
            }

          }
        });
      }
    })
  }


</script>
<!--  -->

<!-- RECEIVING -->
<script type="text/javascript">
  function check_do_pl(input){
    var val_do_pl = $(input).val();
    var val_receiving_id = $('input[name="id_receiving_master"]').val();

    $.ajax({
      url: "<?= base_url() ?>receiving/receiving_check_do_pl",
      type: "POST",
      data: {
        'receiving_id' : val_receiving_id,
        'do_pl': val_do_pl
      },
      success: function(data){
        var hasil = JSON.parse(data);
        if(hasil.hasil == 0){
          $(input).addClass('is-valid');
          $(input).removeClass('is-invalid');

          $('button[name="submit_receiving"]').removeAttr('disabled');
          $('p[name="alert_do_pl"]').addClass('d-none');

        } else {
          $(input).addClass('is-invalid');
          $(input).removeClass('is-valid');

          $('button[name="submit_receiving"]').attr('disabled', true);
          $('p[name="alert_do_pl"]').removeClass('d-none');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });

  }
</script>
<!--  -->