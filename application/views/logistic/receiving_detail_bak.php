<style type="text/css">
  .nav-link{
    color: #000;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #007bff;
    background: #fff;
    border-bottom: 2px solid #007bff;
    border-radius: 0px;
  }
</style>

<!-- <input type="hidden" name="mto_id" value="<?= $receiving_list['mto_id'] ?>"> -->
<div id="content" class="container-fluid">
 
  <div class="row">

    <div class="col-md-12">
       <div class="overflow-auto text-muted  ">
          <h6>
            <ol class="breadcrumb bg-white">DO / PL No : <?= $receiving_list['do_pl'] ?></ol>
          </h6>
       </div>
    </div>

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == '' ? 'active' : '') ?>" data-toggle="pill" href="#pills-receiving">Receiving</a>
          </li>

          <?php if($read_permission[2] == 1){ ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'rd' ? 'active' : '') ?>" data-toggle="pill" href="#pills-detail">Receiving Detail</a>
          </li>

          <?php } ?>

          <?php if($read_permission[9] == 1){ ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'qty' ? 'active' : '') ?>" data-toggle="pill" href="#pills-qty">Quantity</a>
          </li>

          <?php } ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'ird' ? 'active' : '') ?>" data-toggle="pill" href="#pills-import-detail">Import Receiving Detail</a>
          </li>

        </ul>
        <div class="overflow-auto media text-muted py-3 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="tab-content">

              <!-- RECEIVING MASTER -->
              <div class="tab-pane fade <?php echo ($t == '' ? 'show active' : '') ?>" id="pills-receiving">
                <form method="POST" id="receiving_master" action="<?php echo base_url();?>receiving/update_receiving">

                  <input type="hidden" name="id_receiving_master" value="<?= $receiving_list['receiving_id'] ?>">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">DO / PL :</label>
                        <div class="col-xl">
                          <input type="text" name="do_pl" class="form-control" value="<?= $receiving_list['do_pl'] ?>" onblur="check_do_pl(this)" required>
                          <p name="alert_do_pl" class="d-none text-danger">* Duplicate DO / PL</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Vendor :</label>
                        <div class="col-xl">
                          <input type="text" name="vendor" class="form-control" value="<?= $receiving_list['vendor'] ?>" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Date Receiving :</label>
                        <div class="col-xl">
                          <input type="text" name="date_received" class="form-control datepicker" value="<?= date('d-m-Y', strtotime($receiving_list['date_received'])) ?>" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <?php //if($read_permission[5] == 1){ ?>
                            <button type="submit" name='submit_receiving' value='submit' class="btn btn-success" title="Submit"><i class="fa fa-edit"></i> Update</button>
                          <?php //} ?>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
              <!-- RECEIVING MASTER END -->

              <!-- RECEIVING DETAIL -->
              <div class="tab-pane fade <?php echo ($t == 'rd' ? 'show active' : '') ?>" id="pills-detail">
                <form method="POST" id="receiving_detail">
                  <input type="hidden" name="master_receiving_id" value="<?= $master_receiving_id ?>">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">PO Number :</label>
                        <div class="col-xl">
                          <input type="text" name="po_number" class="form-control detail_required" placeholder="PO Number" onchange="check_po_number(this)" required>
                          <p class="d-none text-danger" name="alert_detail_po_number">* Duplicate PO Number</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Material Code :</label>
                        <div class="col-xl">
                          <input type="text" name="material_code" class="form-control detail_required" placeholder="Material Code" onchange="check_material_code(this)" onfocus="$(this).removeClass('is-valid is-invalid')" required>
                          <p class="d-none text-danger" name="alert_detail_mto_catalog">* Duplicate Material Code</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Description :</label>
                        <div class="col-xl">
                          <input type="text" name="description" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Unique Number :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control detail_required" name="unique_no" placeholder="Unique Number" onblur="check_unique_number(this)" required>
                          <p class="d-none text-danger" name="alert_unique_number">* Duplicate Unique Number</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">PO Item :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control detail_required" name="po_item" placeholder="PO Item" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Heat Number :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control detail_required" name="heat_no" placeholder="Heat Number" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Mill Certificate :</label>
                        <div class="col-xl">
                          <input type="text" name="mill_certificate" class="form-control detail_required" placeholder="Mill Ceritificate" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Country Origin :</label>
                        <div class="col-xl">
                          <input type="text" name="country_origin" class="form-control detail_required" placeholder="Country Origin" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Brand :</label>
                        <div class="col-xl">
                          <input type="text" name="brand" class="form-control detail_required" placeholder="Brand" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">UoM :</label>
                        <div class="col-xl">
                          <select name="uom" class="form-control detail_required" required>
                            <option value=""> - </option>
                            <option value="Length">Length</option>
                            <option value="Meter">Meter</option>
                            <option value="Mili Meter">Mili Meter</option>
                            <option value="Square Meter">Square Meter</option>
                            <option value="Pcs">Pcs</option>
                            <option value="Each">Each</option>
                            <option value="Unit">Unit</option>
                            <option value="Sets">Sets</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Color Code :</label>
                        <div class="col-xl">
                          <input type="text" name="color_code" class="form-control detail_required" placeholder="Color Code" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Valuta :</label>
                        <div class="col-xl">
                          <input type="text" name="valuta" class="form-control detail_required" placeholder="Valuta" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Area :</label>
                        <div class="col-xl">
                          <select class="form-control detail_required" name="area">
                            <option value=""> - </option>
                            <?php foreach($area_list as $area){ ?>
                              <option value="<?= $area['id'] ?>"><?= $area['area_name'] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Location :</label>
                        <div class="col-xl">
                          <input type="text" name="location" class="form-control detail_required" placeholder="Location" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Type :</label>
                        <div class="col-xl">
                          <input type="text" name="type" class="form-control detail_required" placeholder="Type" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Spec :</label>
                        <div class="col-xl">
                          <input type="text" name="spec" class="form-control detail_required" placeholder="Spec" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Spec Category :</label>
                        <div class="col-xl">
                          <select class="form-control detail_required" name="spec_category">
                            <option value=""> - </option>
                            <?php foreach($spec_cat_list as $spec_cat){ ?>
                              <option value="<?= $spec_cat['id'] ?>"><?= $spec_cat['spec_desc'] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Plate/Tag No :</label>
                        <div class="col-xl">
                          <input type="text" name="plate_or_tag_no" class="form-control detail_required" placeholder="Plate/Tag No" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Supplier Name :</label>
                        <div class="col-xl">
                          <input type="text" name="supplier_name" class="form-control detail_required" placeholder="Supplier Name" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">CEQ :</label>
                        <div class="col-xl">
                          <input type="text" name="ceq" class="form-control detail_required" placeholder="CEQ" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Date Manufacturing :</label>
                        <div class="col-xl">
                          <input type="text" name="date_manufacturing" class="form-control detail_required datepicker" placeholder="Date Manufacturing" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Category :</label>
                        <div class="col-xl">
                          <input type="text" name="category" class="form-control detail_required" placeholder="Category" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Remarks :</label>
                        <div class="col-xl">
                          <textarea class="form-control detail_required" name="remarks" placeholder="--"></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Status Receiving :</label>
                        <div class="col-xl">
                          <input type="text" name="status_receiving" class="form-control" autocomplete="off" required placeholder="Status Receiving">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <?php if($read_permission[5] == 1){ ?>
                            <!-- <a class="btn btn-success" href='<?php echo base_url();?>mrir/mrir_for_review/<?php echo strtr($this->encryption->encrypt($receiving_list['do_pl']),'+=/', '.-~') ?>'>Create MRR</a> -->
                          <?php } ?>
                            <button type="button" name='submit_detail' class="btn btn-success" title="Submit" onclick="submit_action('receiving_detail')"><i class="fa fa-check"></i> Submit</button>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>

                <!-- TABLE  -->
                <table class="table dataTable table-hover text-center">
                  <thead class="bg-success text-white">
                    <tr>
                      <!-- <th></th> -->
                      <th>MTO Catalog</th>
                      <th>Unique Number</th>
                      <th>PO Item</th>
                      <th>Heat Number</th>
                      <th>Mill Certificate</th>
                      <th>Country Origin</th>
                      <th>Brand</th>
                      <th>UoM</th>
                      <th>Color Code</th>
                      <th>Valuta</th>
                      <th>Area</th>
                      <th>Location</th>
                      <th>Remarks</th>
                      <th>Status Receiving</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="show_data">
                    
                  </tbody>
                </table>
              </div>
              <!-- RECEIVING DETAIL END -->

              <!-- RECEIVING QTY -->
              <div class="tab-pane fade <?php echo ($t == 'qty' ? 'show active' : '') ?>" id="pills-qty">
                
                <?php //if($read_permission[10] == 1){ ?>

                <form method="POST" id="receiving_qty" enctype="multipart/form-data">

                  <input type="hidden" name="master_receiving_id" value="<?= $master_receiving_id ?>">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Description :</label>
                        <div class="col-xl">
                          <select class="form-control select2 qty_required" name="mto_catalog_qty" required>
                            <option value=""> - </option>
                            <?php foreach($mto_detail_list as $mto_detail){ ?>
                              <option value="<?= $mto_detail['id_material'] ?>"><?= $mto_detail['name_material'] ?></option>
                            <?php } ?>
                          </select>
                          <p class="d-none text-danger" name="alert_qty_mto_material">* Duplicate Description</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Qty :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control qty_required" name="qty" placeholder="Qty" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Unit Weight (Length, Sheet) :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="unit_weight_length_sheet" placeholder="Unit Weight (Length, Sheet)" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Total Weight (Length, Sheet) :</label>
                        <div class="col-xl">
                          <input type="text" name="total_weight_length_sheet" class="form-control" placeholder="Total Weight (Length, Sheet)" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Unit Weight (mm, sqm) :</label>
                        <div class="col-xl">
                          <input type="text" name="unit_weight_mm_sqm" class="form-control" placeholder="Unit Weight (mm, sqm)" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Total Weight (mm, sqm) :</label>
                        <div class="col-xl">
                          <input type="text" name="total_weight_mm_sqm" class="form-control" placeholder="Total Weight (mm,sqm)" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Value (mm, sqm) :</label>
                        <div class="col-xl">
                          <input type="text" name="value_mm_sqm" class="form-control" placeholder="Value (mm, sqm)" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Total Value (length, sheet) :</label>
                        <div class="col-xl">
                          <input type="text" name="total_value_length_sheet" class="form-control" placeholder="Total Value (length, sheet)" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Total Value (mm, sqm) :</label>
                        <div class="col-xl">
                          <input type="text" name="total_value_mm_sqm" class="form-control" placeholder="Total Value (mm, sqm)" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        
                      </div>
                    </div>
                  </div>

                  <div class="col-xl text-right">
                    <?php //if($read_permission[5] == 1){ ?>
                      <button type="button" name='submit_qty' class="btn btn-success" title="Submit" onclick="submit_action('receiving_qty')"><i class="fa fa-check"></i> Submit</button>
                    <?php //} ?>
                  </div>
                  <br>

                </form>

                <?php //} ?>

                <!-- TABLE  -->
                <table class="table dataTable table-hover text-center">
                  <thead class="bg-success text-white">
                    <tr>
                      <!-- <th></th> -->
                      <th>MTO Catalog</th>
                      <th>Qty</th>
                      <th>Unit Weight (Length, Sheet)</th>
                      <th>Unit Weight (mm, sqm)</th>
                      <th>Total Weight (Length, Sheet)</th>
                      <th>Total Weight (mm, sqm)</th>
                      <th>Value (mm, sqm)</th>
                      <th>Total Value (Length, Sheet)</th>
                      <th>Total Value (mm, sqm)</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="show_data_qty">
                    
                  </tbody>
                </table>

              </div>
              <!-- RECEIVING QTY END -->

              <!-- Import Drawing Detail -->
              <div class="tab-pane fade <?php echo ($t == 'ird' ? 'show active' : '') ?>" id="pills-import-detail">
                
                <form method="POST" action="<?php echo base_url();?>receiving_detail_preview" enctype="multipart/form-data">
                  <input type="hidden" name="master_receiving_id" value="<?= $master_receiving_id ?>">

                  <div class="container-fluid">
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Template</label>
                      <div class="col-sm-10 col-form-label">
                        <a href="<?php echo base_url(); ?>file/receiving/Template_Import_Receiving_Detail.xlsx">Template_Import_Receiving_Detail.xlsx</a>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Upload</label>
                      <div class="col-sm-10">
                        <div class="custom-file">
                          <input type="file" name="file" class="custom-file-input" required onChange="$('#label1.custom-file-label').html($(this).val())">
                          <label id="label1" class="custom-file-label">Choose file</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="text-right mt-3">
                    <button type="submit" name='submit_import' class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
                  </div>
                </form>

              </div>
              <!-- Import Drawing Detail END -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<!-- GENERAL -->
<script type="text/javascript">

  //GET FIRST VALUE =============================================================
  var first_value;
  function save_first_value(cell){
    var value = $(cell).html();
    first_value = value;
  }
  //=============================================================================

  //MTO DETAIL EDIT ====================================================================
  function receiving_edit(cell, param){

    if(param == 'receiving_detail'){
      _url = "<?php echo base_url();?>receiving/receiving_detail_edit_process";
    } else {
      _url = "<?php echo base_url();?>receiving/receiving_qty_edit_process";
    }

    var col = $(cell).data("col");
    var id = $(cell).data("id");
    var value = $(cell).html();
    if(value != first_value){
      Swal.fire({
        title: 'Are you sure to <b class="text-warning">&nbsp;Edit&nbsp;</b> this?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: _url,
            type: "post",
            data: {
              col: col,
              id: id,
              value: value
            }, success(data){
              Swal.fire(
                'Success!',
                'Your data has been changed.',
                'success'
              )
            }
          });
        }
        else{
          $(cell).html(first_value);
        }
      })
    }
  }
  //=============================================================================

  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true,
  })
  // .datepicker("setDate", new Date());

  function submit_action(param){

    if(param == 'receiving_detail'){
      var _url = '<?= base_url() ?>receiving/receiving_detail_add_process';
      var _status =  $('.detail_required').filter(function(){return this.value==''}).length;
    } else {
      var _url = '<?= base_url() ?>receiving/receiving_qty_add_process';
      var _status =  $('.qty_required').filter(function(){return this.value==''}).length;
    }

    if(_status > 0){
      Swal.fire(
        'Error!',
        'Please Complete the form',
        'error'
      );
      return false;
    }

    $.ajax({
      data: $('#'+param).serialize(),
      url: _url,
      type: "post",
      success : function(data){
        $('#'+param).trigger('reset');
        
        Swal.fire(
          'Success!',
          'Data Successful Added',
          'success'
        )

        if(param == 'receiving_detail'){
          show_receiving_detail();
        } else {
          show_receiving_qty();
        }
        
      }
    });
  }

  function delete_action(param, id){

    if(param == 'receiving_detail'){
      var _url = '<?= base_url() ?>receiving/receiving_detail_delete_process';
    } else {
      var _url = '<?= base_url() ?>receiving/receiving_qty_delete_process';
    }

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          data: {
            'id' : id,
          },
          url: _url,
          type: "post",
          success : function(data){

            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )

            if(param == 'receiving_detail'){
              show_receiving_detail();
            } else {
              show_receiving_qty();
            }

          }
        });
      }
    })
  }


</script>
<!--  -->

<!-- RECEIVING -->
<script type="text/javascript">
  function check_do_pl(input){
    var val_do_pl = $(input).val();
    var val_receiving_id = $('input[name="id_receiving_master"]').val();

    $.ajax({
      url: "<?= base_url() ?>receiving/receiving_check_do_pl",
      type: "POST",
      data: {
        'receiving_id' : val_receiving_id,
        'do_pl': val_do_pl
      },
      success: function(data){
        var hasil = JSON.parse(data);
        if(hasil.hasil == 0){
          $(input).addClass('is-valid');
          $(input).removeClass('is-invalid');

          $('button[name="submit_receiving"]').removeAttr('disabled');
          $('p[name="alert_do_pl"]').addClass('d-none');

        } else {
          $(input).addClass('is-invalid');
          $(input).removeClass('is-valid');

          $('button[name="submit_receiving"]').attr('disabled', true);
          $('p[name="alert_do_pl"]').removeClass('d-none');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });

  }
</script>
<!--  -->

<!-- RECEIVING DETAIL -->
<script type="text/javascript">

  // $('select[name="mto_catalog"]').on('change', function(){

  //   var val_mto_id = $(this).val();
  //   var val_receiving_id = $('input[name="id_receiving_master"]').val();

  //   $.ajax({
  //     url: "<?= base_url() ?>receiving/receiving_detail_check_mto_catalog",
  //     type: "POST",
  //     data: {
  //       'receiving_id' : val_receiving_id,
  //       'mto_id': val_mto_id
  //     },
  //     success: function(data){
  //       if(data == 0){
  //         $('select[name="mto_catalog"]').addClass('is-valid');
  //         $('select[name="mto_catalog"]').removeClass('is-invalid');

  //         $('button[name="submit_detail"]').removeAttr('disabled');
  //         $('p[name="alert_detail_mto_catalog"]').addClass('d-none');

  //       } else {
  //         $('select[name="mto_catalog"]').addClass('is-invalid');
  //         $('select[name="mto_catalog"]').removeClass('is-valid');

  //         $('button[name="submit_detail"]').attr('disabled', true);
  //         $('p[name="alert_detail_mto_catalog"]').removeClass('d-none');
  //       }
  //     },
  //     error: function(jqXHR, textStatus, errorThrown) {
  //        console.log(textStatus, errorThrown);
  //     }
  //   });

  // });

  function check_po_number(input){
    var val_po_number = $(input).val();

    // if(val_po_number == ""){
    //   $('p[name="alert_detail_po_number"]').removeClass('d-none').html('* Unique Number is Required');

    //   $(input).addClass('is-invalid');
    //   $(input).removeClass('is-valid');

    //   $('button[name="submit_detail"]').attr('disabled', true);
            
    // } else {
    //   $.ajax({
    //     url: "<?= base_url() ?>receiving/receiving_check_unique_no",
    //     type: "POST",
    //     data: {
    //       'po_no': val_po_number,
    //       'param' : ''
    //     },
    //     success: function(data){
    //       if(data == '0'){

    //         $('p[name="alert_unique_number"]').addClass('d-none');
    //         $(input).addClass('is-valid');
    //         $(input).removeClass('is-invalid');

    //         $('button[name="submit_detail"]').removeAttr('disabled');

    //       } else {

    //         $('p[name="alert_unique_number"]').removeClass('d-none');
    //         $('p[name="alert_unique_number"]').html('* Duplicate Unique Number');

    //         $('button[name="submit_detail"]').attr('disabled', true);

    //         $(input).removeClass('is-valid');
    //         $(input).addClass('is-invalid');
    //       }
    //     },
    //     error: function(jqXHR, textStatus, errorThrown) {
    //        console.log(textStatus, errorThrown);
    //     }
    //   });
    // }
  }

  function check_material_catalog(input){
    var val_material_catalog = $(this).val();

    if(!val_material_catalog){
      $('#text_alert').removeClass('d-none');
      $('input[name="material_cat"]').addClass('is-invalid');
      $('button[name="submit"]').attr('disabled', true);
      $('input[name="id_material_cat"]').val(0);

      $('input[name="material"]').val('');
      $('input[name="steel_type"]').val('');
      $('input[name="grade"]').val('');
      $('input[name="thk_mm"]').val('');
      $('input[name="width_m"]').val('');
      $('input[name="length_m"]').val('');

      $('input[name="area_per_plate"]').val('');
            
    } else {
      $.ajax({
        url: "<?= base_url() ?>mto/mto_check_material_catalog",
        type: "POST",
        data: {
          'material': val_material_catalog
        },
        success: function(data){
          var hasil = JSON.parse(data);
          if(hasil.id == '0'){
            $('input[name="material_cat"]').addClass('is-invalid');
            $('input[name="material_cat"]').removeClass("is-valid");
            $('#text_alert').removeClass('d-none');
            $('button[name="submit"]').attr('disabled', true);
            $('input[name="id_material_cat"]').val(0);

            // VALUE
            $('input[name="material"]').val('');
            $('input[name="steel_type"]').val('');
            $('input[name="grade"]').val('');
            $('input[name="thk_mm"]').val('');
            $('input[name="width_m"]').val('');
            $('input[name="length_m"]').val('');

            $('input[name="area_per_plate"]').val('');

          } else {
            $('input[name="material_cat"]').removeClass("is-invalid");
            $('input[name="material_cat"]').addClass('is-valid');
            $('#text_alert').addClass('d-none');
            $('button[name="submit"]').removeAttr('disabled');
            $('input[name="id_material_cat"]').val(hasil.id);

            // VALUE
            $('input[name="material"]').val(hasil.material);
            $('input[name="steel_type"]').val(hasil.steel_type);
            $('input[name="grade"]').val(hasil.grade);
            $('input[name="thk_mm"]').val(hasil.thk_mm);
            $('input[name="width_m"]').val(hasil.width_m);
            $('input[name="length_m"]').val(hasil.length_m);

            autofill_area_per_plate();

          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });
    }
  }

  show_receiving_detail(); //call function show all product
  
  function show_receiving_detail(){

    var param = 'receiving_detail';

    $.ajax({
      data: {
        'master_receiving_id': $('input[name="master_receiving_id"]').val()
      },
      url: '<?php echo base_url()?>receiving/receiving_detail_list',
      type: "post",
      success : function(data){
        // console.log(data);
        var hasil = JSON.parse(data);
        var html = '';
        var i;
        for(i=0; i<hasil.length; i++){
            html += '<tr>'+
                    // '<td><button class="btn btn-success"><i class="fas fa-angle-double-down"></i></button></td>'+
                    '<td>'+hasil[i].name_catalog+'</td>'+
                    '<td>'+hasil[i].uniq_no+'</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="po_item" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].po_item+ '</div>' +  
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="heat_no" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].heat_no+ '</div>' +  
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="mill_certificate" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].mill_certificate+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="country_origin" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].country_origin+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="brand" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].brand+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="uom" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].uom+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="color_code" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].color_code+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="valuta" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].valuta+ '</div>' +
                    '</td>'+
                    '<td>'+hasil[i].name_area+'</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="location" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].location+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this, \'receiving_detail\');" data-col="remarks" data-id="' +hasil[i].rec_det_id+ '">' +hasil[i].remarks+ '</div>' +
                    '</td>'+
                    '<td>'+hasil[i].status_receiving+'</td>'+
                    // '<td>' +
                    //   '<div contenteditable onclick="save_first_value(this);" onblur="material_edit(this);" data-col="qty" data-id="' +hasil[i].id+ '">' +hasil[i].qty+ '</div>' +  
                    // '</td>'+
                    // '<td>'+
                    //   '<div contenteditable onclick="save_first_value(this);" onblur="material_edit(this);" data-col="total_weight" data-id="' +hasil[i].id+ '">' +hasil[i].total_weight+ '</div>' +
                    // '</td>'+
                    '<td>'+
                      '<button class="btn btn-danger" onclick="delete_action(\'receiving_detail\', ' + hasil[i].rec_det_id + ')"><i class="fa fa-trash"></i></button>'+
                    '</td>'+
                    '</tr>';
        }

        $('#show_data').html(html);
      }
    });
  }  

</script>

<!-- RECEIVING QTY -->
<script type="text/javascript">

  show_receiving_qty(); //call function show all product

  function show_receiving_qty(){

    $.ajax({
      data: {
        'master_receiving_id': $('input[name="master_receiving_id"]').val()
      },
      url: '<?php echo base_url()?>receiving/receiving_qty_list',
      type: "post",
      success : function(data){
        // console.log(data);
        var hasil = JSON.parse(data);
        var html = '';
        var i;
        for(i=0; i<hasil.length; i++){
            html += '<tr>'+
                    '<td>'+hasil[i].name_catalog+'</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="qty" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].qty+ '</div>' +  
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="unit_weight_length_sheet" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].unit_weight_length_sheet+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="total_weight_length_sheet" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].total_weight_length_sheet+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="unit_weight_mm_sqm" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].unit_weight_mm_sqm+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="total_weight_mm_sqm" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].total_weight_mm_sqm+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="value_mm_sqm" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].value_mm_sqm+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="total_value_length_sheet" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].total_value_length_sheet+ '</div>' +
                    '</td>'+
                    '<td>' +
                      '<div contenteditable onclick="save_first_value(this);" onblur="receiving_edit(this,\'receiving_qty\');" data-col="total_value_mm_sqm" data-id="' +hasil[i].rec_val_id+ '">' +hasil[i].total_value_mm_sqm+ '</div>' +
                    '</td>'+
                    '<td>'+
                      '<button class="btn btn-danger" onclick="delete_action(\'receiving_qty\', ' + hasil[i].rec_val_id + ')"><i class="fa fa-trash"></i></button>'+
                    '</td>'+
                    '</tr>';
        }

        $('#show_data_qty').html(html);
      }
    });
  }

  $('select[name="mto_catalog_qty"]').on('change', function(){

    var val_mto_id = $(this).val();
    var val_receiving_id = $('input[name="id_receiving_master"]').val();
    $.ajax({
      url: "<?= base_url() ?>receiving/receiving_qty_check_mto_catalog",
      type: "POST",
      data: {
        'receiving_id' : val_receiving_id,
        'mto_id': val_mto_id
      },
      success: function(data){
        if(data == 0){
          $('button[name="submit_qty"]').removeAttr('disabled');
          $('p[name="alert_qty_mto_material"]').addClass('d-none');

        } else {
          $('button[name="submit_qty"]').attr('disabled', true);
          $('p[name="alert_qty_mto_material"]').removeClass('d-none');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });

  });

  //CHECK MTO CATALOG IN RECEIVING QTY ----------------------------------
  $('select[name="mto_catalog_qty"]').on('change', function(){

    var val_mto_id = $(this).val();
    var val_receiving_id = $('input[name="id_receiving_master"]').val();

    $.ajax({
      url: "<?= base_url() ?>receiving/receiving_qty_check_mto_catalog",
      type: "POST",
      data: {
        'receiving_id' : val_receiving_id,
        'mto_id': val_mto_id
      },
      success: function(data){
        if(data == 0){
          $('select[name="mto_catalog"]').addClass('is-valid');
          $('select[name="mto_catalog"]').removeClass('is-invalid');

          $('button[name="submit_detail"]').removeAttr('disabled');
          $('p[name="alert_detail_mto_catalog"]').addClass('d-none');

        } else {
          $('select[name="mto_catalog"]').addClass('is-invalid');
          $('select[name="mto_catalog"]').removeClass('is-valid');

          $('button[name="submit_detail"]').attr('disabled', true);
          $('p[name="alert_detail_mto_catalog"]').removeClass('d-none');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });

  });
  //---------------------------------------------------------------------S


</script>