<div id="content" class="container-fluid">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <?php echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center" id='received_list_dt'>
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>Supply Number</th>
                  <th>Vendor / Supplier Name</th>
                  <th>Material Package</th>
                  <th>Weight (Kg)</th>
                  <th>ETD</th>
                  <th>ATD</th>
                  <th>Complete Date</th>
                  <th>Remarks</th>
                  <th>Action</th>
                </tr>
              </thead>
              
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('#received_list_dt').DataTable({
    "language": { 
      "infoFiltered": "" },
      "paging": true,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        "url": "<?php echo base_url();?>receiving/receiving_list_json",
        "type": "POST",
      },
      "columnDefs": [{
        "targets": [0],
        "orderable": true,
      }, ],
  });
</script>

<script type="text/javascript">
  function submit_to_qc(param){
     Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, submit it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          data: {
            'id' : param,
          },
          url: "<?= base_url() ?>receiving/submit_to_qc",
          type: "post",
          success : function(data){
            
          }
        });
      }
    })
  }
</script>