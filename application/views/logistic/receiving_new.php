<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>receiving/receiving_add_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">MTO Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="mto_number" id="mto_number" placeholder="MTO Number" onblur="check_mto_number(this)" required>
                  <label class="d-none" id="label_alert" style="color: red;">MTO Number is Required</label>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">DO / PL</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="do_pl" placeholder="DO PL" onblur="check_do_pl(this)" required>
                  <label class="d-none" id="label_alert_do_pl" style="color: red;">DO / PL is Required</label>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Vendor</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="vendor" placeholder="Vendor" value="PT SMOE" required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Date Received</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control datepicker" name="date_received" placeholder="dd-mm-yyyy" required>                
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>receiving" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).datepicker("setDate", new Date());

  $("#mto_number").autocomplete({
     source: "<?php echo base_url(); ?>mto/mto_number_autocomplete",
     autoFocus: true,
     classes: {
         "ui-autocomplete": "highlight"
     }
  });

  function check_mto_number(input) {
    var mto_number = $(input).val();

    if(mto_number == '' || mto_number == 'MTO Number Not Found'){
        $(input).addClass('is-invalid');
        $('#label_alert').removeClass('d-none');
        $('#label_alert').text('MTO Number is Required');
        $('button[name=submit]').prop("disabled", true);    
    } else {
      $.ajax({
        url: "<?php echo base_url();?>receiving/mto_number_check/",
        type: "post",
        data: {
          mto_number: mto_number
        },
        success: function(data) {
          var data = JSON.parse(data);

          if(data.hasil != 0){ // KALAU GAK DUPLICATE ----
            $(input).addClass('is-invalid');
            $(input).removeClass('is-valid');

            $('#label_alert').removeClass('d-none');
            $('#label_alert').text('Duplicate MTO Number');

            $('button[name=submit]').attr("disabled", true);
            
          } else{ // KALAU DUPLICATE -----
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            $('#label_alert').addClass('d-none');

            $('button[name=submit]').removeAttr("disabled");
          }

        }
      });

    }
  }

  function check_do_pl(input) {
    var do_pl = $(input).val();

    if(do_pl == ''){
        $(input).addClass('is-invalid');
        $('#label_alert_do_pl').removeClass('d-none');
        $('#label_alert_do_pl').text('DO / PL is Required');
        $('button[name=submit]').prop("disabled", true);    
    } else {
      $.ajax({
        url: "<?php echo base_url();?>receiving/receiving_check_do_pl/",
        type: "post",
        data: {
          do_pl: do_pl,
          receiving_id: ''
        },
        success: function(data) {
          var data = JSON.parse(data);

          if(data.hasil > 0){ // KALAU DUPLICATE ----
            $(input).addClass('is-invalid');
            $(input).removeClass('is-valid');

            $('#label_alert_do_pl').removeClass('d-none');
            $('#label_alert_do_pl').text('Duplicate DO / PL');

            $('button[name=submit]').attr("disabled", true);
            
          } else{ // KALAU GAK DUPLICATE -----
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            $('#label_alert_do_pl').addClass('d-none');

            $('button[name=submit]').removeAttr("disabled");
          }

        }
      });

    }
  }
</script>