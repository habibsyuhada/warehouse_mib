<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
  <ul class="list-unstyled components">
   
      
    <li>
      <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-truck"></i> Receiving
      </a>
      <ul class="list-unstyled" id="homeSubmenu2">
        <li>
          <a href="<?php echo base_url();?>receiving_add"><i class="fas fa-plus"></i> &nbsp; Add Receiving</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>receiving"><i class="fas fa-minus-square"></i> &nbsp; Receiving List</a>
        </li>
      </ul>
    </li>

  </ul>
</nav>