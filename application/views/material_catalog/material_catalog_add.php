  <div id="content" class="container-fluid">
    <form method="POST" action="<?php echo base_url();?>material_catalog/material_catalog_add_process">
      <div class="row">

        <div class="col-md-12">
          <div class="my-3 p-3 bg-white rounded shadow-sm">
            <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
            <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
              <div class="container-fluid">

                <div class="form-group">
                  <label for="description">Material Category</label>
                  <select class="form-control" name="catalog_category" required>
                    <option value="">-</option>
                    <?php foreach($catalog_category_list as $catalog_category){ ?>
                      <option value="<?= $catalog_category['id'] ?>"><?= $catalog_category['category_name'] ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="description">Name Material</label>
                  <input type="text" class="form-control" name="description" id="description" placeholder="Fill Up Name Material" required>
                </div>

              </div>
            </div>
            
            <div class="text-right mt-3">
              <?php //if($read_permission[11] == 1){ ?>
                  <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
              <?php //} ?>
              <a href="<?php echo base_url();?>material_catalog_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div><!-- ini div dari sidebar yang class wrapper -->