<?php 
  $material_catalog_list = $material_catalog_list[0];
?>

<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>material_catalog/material_catalog_edit_process">

    <input type="hidden" name="id" value="<?= $material_catalog_list['id'] ?>">

    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">

              <div class="form-group">
                <label for="code">Code</label>
                <input type="text" class="form-control" name="code" id="code" placeholder="Fill Up Code" value="<?= $material_catalog_list['code_material'] ?>" readonly>
              </div>

              <div class="form-group">
                <label for="description">Material Category</label>
                <select class="form-control" name="catalog_category" required>
                  <option value="">-</option>
                  <?php foreach($catalog_category_list as $catalog_category){ ?>
                    <option value="<?= $catalog_category['id'] ?>" <?= $catalog_category['id'] == $material_catalog_list['catalog_category_id'] ? 'selected' : '' ?> ><?= $catalog_category['category_name'] ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label for="description">Name Material</label>
                <input type="text" class="form-control" name="description" id="description" placeholder="Fill Up Name Material" value="<?= $material_catalog_list['material'] ?>" required>
              </div>

            </div>
          </div>
          
          <div class="text-right mt-3">
            <?php //if($read_permission[12] == 1){ ?>

              <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Update"><i class="fa fa-edit"></i> Update</button>
              
            <?php //} ?>

            <a href="<?php echo base_url();?>material_catalog_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">

  var material_grade = $('select[name="material_grade"]').val();
  get_material_class(material_grade);

  $('select[name="material_grade"]').on('change', function(){ 
    var material_grade = this.value;
    // alert(material_grade);
    $.ajax({
      url: "<?php echo base_url();?>material_catalog/get_grade/",
      type: "post",
      data: {
        material_grade: material_grade
      },
      success: function(data) {
        var hasil = JSON.parse(data);
        if(hasil.data == 0){
          $('input[name="id_material_class"]').val('');
          $('input[name="material_class"]').val('');
        } else {
          $('input[name="id_material_class"]').val(hasil.id_material_class);
          $('input[name="material_class"]').val(hasil.name_material_class);
        }
      }
    });
  });

  function get_material_class(material_grade){
    $.ajax({
      url: "<?php echo base_url();?>material_catalog/get_grade/",
      type: "post",
      data: {
        material_grade: material_grade
      },
      success: function(data) {
        var hasil = JSON.parse(data);
        if(hasil.data == 0){
          $('input[name="id_material_class"]').val('');
          $('input[name="material_class"]').val('');
        } else {
          $('input[name="id_material_class"]').val(hasil.id_material_class);
          $('input[name="material_class"]').val(hasil.name_material_class);
        }
      }
    });
  }

</script>