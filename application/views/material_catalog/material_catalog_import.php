  <div id="content" class="container-fluid">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
              <?php echo $this->session->flashdata('message'); ?>

              <form method="POST" action="<?= base_url() ?>material_catalog/material_catalog_preview" enctype="multipart/form-data">
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Template : </label>
                  <div class="col-sm-10">
                    <a href="<?= base_url() ?>file/mto_material_catalog/Template_Import_Material_Catalog.xlsx">Template_Import_Material_Catalog.xlsx</a>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Upload</label>
                  <div class="col-sm-10">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="customFile" name="file" required>
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div>
                  </div>
                </div>

                <div class="text-right">
                   <?php if($read_permission[11] == 1){ ?>
                      <button class="btn btn-success"><i class="fas fa-plus"></i> Submit</button>
                  <?php } ?>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
</script>