<?php 
  // print_r($sheet);exit;
?>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>material_catalog/material_catalog_import_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-borderless table-hover text-center">
                <thead class="bg-green-smoe text-white">
                  <tr>
                    <th rowspan="2">Category</th>
                    <th rowspan="2">Description</th>
                    <th colspan="2">Material</th>
                    <th rowspan="2">Supply Category</th>
                    <th colspan="6">Plate / Profile Size</th>
                    <th rowspan="2">Status</th>
                  </tr>
                  <tr>
                    <th>Grade</th>
                    <th>Class</th>

                    <th>Thk (mm)</th>
                    <th>Width (M)</th>
                    <th>Length (M)</th>
                    <th>Weight</th>
                    <th>OD</th>
                    <th>Schedule</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    foreach($sheet as $key => $row){
                      if($key > 1){

                        $status = '';
                        $disabled = 0;

                        //CATALOG CODE ---------------------------
                        if($row['id_catalog_code'] == ''){
                          $status = 'Catalog Code Not Found';
                          $disabled = 2;
                        }
                        //----------------------------------------

                        //MATERIAL GRADE -------------------------
                        else if($row['id_material_grade'] == ''){
                          $status = 'Material Grade Not Found';
                          $disabled = 2;
                        }
                        //----------------------------------------

                        //SUPPLY CATEGORY ------------------------
                        else if($row['id_supply_category'] == ''){
                          $status = 'Supply Not Found';
                          $disabled = 2;
                        }
                        //----------------------------------------

                        if($row['A'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td class="align-middle">
                      <input type="hidden" name="code_category[]" value="<?= $row['id_catalog_code'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="text" class="form-control" value="<?= $row['catalog_code'] ?>" readonly <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle"><input type="text" name="material[]" class="form-control" value="<?php echo $row['B'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle">
                      <input type="hidden" name="material_grade[]" value="<?php echo $row['id_material_grade'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="text" class="form-control" value="<?= $row['name_material_grade'] ?>" readonly <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" value="<?= $row['name_material_class'] ?>" readonly <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="hidden" name="supply_category[]" value="<?php echo $row['id_supply_category'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="text" class="form-control" value="<?= $row['supply_category'] ?>" readonly <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle"><input type="text" name="thk_mm[]" class="form-control" value="<?php echo $row['E'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="width_m[]" class="form-control" value="<?php echo $row['F'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="length_m[]" class="form-control" value="<?php echo $row['G'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="weight[]" class="form-control" value="<?php echo $row['H'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="od[]" class="form-control" value="<?php echo $row['I'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="sch[]" class="form-control" value="<?php echo $row['J'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><?php echo $status ?></td>
                  </tr>
                  <?php } } } ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>material_catalog_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->