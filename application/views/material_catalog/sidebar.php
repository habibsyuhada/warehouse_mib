<div class="wrapper" style="min-height: 79vh">
  <nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
    <ul class="list-unstyled components">

      <?php if($this->permission_cookie[26] == 1 || $this->permission_cookie[27] == 1 || $this->permission_cookie[30] == 1){ ?>
      <li>
        <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fas fa-home"></i> Material Catalog
        </a>
        <ul class="list-unstyled" id="homeSubmenu3">
          <?php if($this->permission_cookie[27] == 1){ ?>
          <li>
            <a href="<?php echo base_url();?>material_catalog/material_catalog_add"><i class="fas fa-plus"></i> &nbsp; Add Material Catalog</a>
          </li>
          <?php } ?>
          <?php if($this->permission_cookie[26] == 1){ ?>
          <li>
            <a href="<?php echo base_url();?>material_catalog/material_catalog_list"><i class="fas fa-minus-square"></i> &nbsp; Material Catalog</a>
          </li>
          <?php } ?>
          <?php if($this->permission_cookie[30] == 1){ ?>
          <!-- <li>
            <a href="<?php echo base_url();?>import_material_catalog"><i class="fas fa-file-excel"></i> &nbsp; Import Material Catalog</a>
          </li> -->
          <?php } ?>
        </ul>
      </li>
      <?php } ?>
      
    </ul>
  </nav>