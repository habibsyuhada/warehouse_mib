<style type="text/css">
  .nav-link{
    color: #000;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #007bff;
    background: #fff;
    border-bottom: 2px solid #007bff;
    border-radius: 0px;
  }
</style>
<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <ul class="nav nav-pills" id="pills-tab" role="tablist">

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == '' ? 'active' : '') ?>" data-toggle="pill" href="#pills-material">Material</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'd' ? 'active' : '') ?>" data-toggle="pill" href="#pills-order">Order</a>
          </li>

        </ul>

        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            
            <div class="tab-pane fade" id="pills-material">
              <form>
                <div class="form-row">
                  <div class="form-group col-md pr-4">
                    <label>Document Number :</label>
                    <input type="text" class="form-control" name="document_no" disabled>
                  </div>
                  <div class="form-group col-md pl-4">
                    <label>Project :</label>
                    <input type="text" class="form-control" name="document_no" disabled>
                  </div>
                </div>
              </form>
            </div>

            <div class="tab-pane fade" id="pills-order">
              <form>
                <div class="form-row">
                  <div class="form-group col-md pr-4">
                    <label>Document Number2 :</label>
                    <input type="text" class="form-control" name="document_no" disabled>
                  </div>
                  <div class="form-group col-md pl-4">
                    <label>Project :</label>
                    <input type="text" class="form-control" name="document_no" disabled>
                  </div>
                </div>
              </form>
            </div>

          </div>
        </div>

        <!-- <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <form>
              <div class="form-row">
                <div class="form-group col-md pr-4">
                  <label>Document Number :</label>
                  <input type="text" class="form-control" name="document_no" disabled>
                </div>
                <div class="form-group col-md pl-4">
                  <label>Project :</label>
                  <input type="text" class="form-control" name="document_no" disabled>
                </div>
              </div>
            </form>
          </div>
        </div> -->

      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->