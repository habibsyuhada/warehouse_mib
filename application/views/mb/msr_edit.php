<?php
	$mb = $mb_list[0];
?>
<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url() ?>mb/msr_edit_process">
  	<div class="row">

	    <div class="col-md-12">
	      <div class="my-3 p-3 bg-white rounded shadow-sm">
	      	<h6 class="pb-2 mb-0"><?php echo $meta_title.' '.$mb['unique_no'] ?></h6>
	      	<div class="overflow-auto media text-muted py-3 border-bottom border-top border-gray">
	      		<div class="container-fluid">
	      			
	      			<div class="form-group row">
	              <label class="col-sm-2 col-form-label">Location</label>
	              <div class="col-sm-10">
	                <select name="location" class="form-control" onchange="change_loc(this)">
	                	<option value="">---</option>
	                	<?php foreach ($location_list as $key => $value): ?>
	                		<option value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $mb['location'] ? 'selected' : '') ?>><?php echo $value['location_name'] ?></option>
	                	<?php endforeach ?>
	                </select>
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-sm-2 col-form-label">Area</label>
	              <div class="col-sm-10">
	                <select name="area" class="form-control">
	                	<option value="" name='blank'>---</option>
	                	<?php foreach ($area_list as $key => $value): ?>
	                		<option class="<?php echo ($mb['location'] == 0 ? 'd-none' : ($mb['location'] == $value['location'] ? '' : 'd-none' )) ?>" name="opt_area" data-loc="<?php echo $value['location'] ?>" value="<?php echo $value['id'] ?>" <?php echo ($value['id'] == $mb['area'] ? 'selected' : '') ?>><?php echo $value['area_name'] ?></option>
	                	<?php endforeach ?>
	                </select>
	              </div>
	            </div>

	            <div class="form-group row">
	              <label class="col-sm-2 col-form-label">Position</label>
	              <div class="col-sm-10">
	                <input type="text" name="position" class="form-control" value="<?php echo $mb['position'] ?>">
	                <input type="hidden" name="mb_id" class="form-control" value="<?php echo $mb['mb_id'] ?>">
	              </div>
	            </div>

	            <div class="form-group row">
	              <div class="col-sm-12 text-right">
	                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Submit</button>
	              </div>
	            </div>

	      		</div>
	      	</div>
	      </div>
	    </div>
    
  	</div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
	function change_loc(input) {
		var loc_id = $(input).val();
		if(loc_id != ""){
			$('option[name=opt_area]').addClass('d-none');
			$('option[data-loc='+loc_id+']').removeClass('d-none');
		}
		else{
			$('option[name=opt_area]').addClass('d-none');
		}		
		$('select[name=area]').val('').change();
	}
</script>