<style type="text/css">
  .table-hover tbody tr:hover{
    background-color: #eee;
  }
  /*.DTFC_LeftWrapper{
    overflow: auto;
  }*/
</style>
<div id="content" class="container-fluid" style="overflow: auto;">
  <?php if($read_permission[64] == 1){ ?>
  <div class="row">
    <div class="col-md-6">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0">Filter</h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <form action="" method="GET">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group row">
                    <label class="col-xl-2 col-form-label">Project :</label>
                    <div class="col-xl">
                      <select class="custom-select" name="project_id">
                        <option value="">---</option>
                        <?php foreach($project_list as $key => $value): ?>
                        <option value="<?php echo $key ?>" <?php echo ($this->input->get('project_id') == $key ? 'selected' : '') ?>><?php echo $value ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group row">
                    <label class="col-xl-2 col-form-label">Supply Type :</label>
                    <div class="col-xl">
                      <select class="custom-select" name="supply_type">
                        <option value="SS" <?php echo ($this->input->get('supply_type') == 'SS' ? 'selected' : '') ?>>SMOE Supply</option>
                        <option value="CS" <?php echo ($this->input->get('supply_type') == 'CS' ? 'selected' : '') ?>>Client Supply</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 text-right">
                  <button class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php } ?>

  <div class="row">
    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-bordered table-hover text-center text-nowrap" id='msr_list_data'>
              <thead class="bg-green-smoe text-white">
                <tr>
                  <!-- <th rowspan="2" class="align-middle">REPORT NO.</th> -->
                  <th rowspan="2" class="align-middle">UNIQUE NO</th>
                  <th rowspan="2" class="align-middle">PROJECT NAME</th><!-- 
                  <th rowspan="2" class="align-middle">PROJECT NO.</th>
                  <th rowspan="2" class="align-middle">CLIENT</th> -->
                  <th rowspan="2" class="align-middle">PURCHASING ORDER NO.</th>
                  <!-- <th rowspan="2" class="align-middle">SUPPLIER NAME</th> -->
                  <th rowspan="2" class="align-middle">DATE OF RECEIVED</th>
                  <th rowspan="2" class="align-middle">DICIPLINE</th>
                  <th rowspan="2" class="align-middle">CATEGORY</th>
                  <th rowspan="2" class="align-middle">MATERIAL CODE</th>
                  <th rowspan="2" class="align-middle">ITEM DESCRIPTION</th>
                  <th colspan="5">SIZE</th>
                  <th rowspan="2" class="align-middle">SPEC GRADE</th>
                  <th rowspan="2" class="align-middle">SPEC CATEGORIES</th>
                  <th rowspan="2" class="align-middle">PLATE / TAG NO.</th>
                  <th rowspan="2" class="align-middle">HEAT / SERIES NO.</th>
                  <th rowspan="2" class="align-middle">MILL CERTIFICATE NO.</th>
                  <th rowspan="2" class="align-middle">BRAND</th>
                  <th rowspan="2" class="align-middle">DO / PL NO.</th>
                  <th colspan="2" class="bg-success">QTY RECEIVED</th><!-- column -->
                  <!-- <th colspan="4" class="bg-success">WEIGHT</th> --><!-- column -->
                  <!-- <th colspan="4" class="bg-success">VALUE</th> --><!-- column -->
                  <th colspan="3" class="bg-danger">OS&D</th><!-- column -->
                  <th rowspan="2" class="align-middle">RETURN TO VENDOR</th>
                  <th colspan="2" class="bg-orange">ISSUED</th><!-- column -->
                  <th colspan="2" class="bg-secondary">RETURN FROM FABRICATION</th><!-- column -->
                  <th colspan="2" class="bg-info">STOCK ON HAND</th><!-- column -->
                  <!-- <th rowspan="2" class="align-middle">VALUTA</th> -->
                  <!-- <th rowspan="2" class="align-middle">LOCATION</th> -->
                  <th rowspan="2" class="align-middle">REMARKS</th>
                  <th rowspan="2" class="align-middle">LOCATION</th>
                  <th rowspan="2" class="align-middle">AREA</th>
                  <th rowspan="2" class="align-middle">POSITION</th>
                  <th rowspan="2" class="align-middle">ACTION</th>
                </tr>
                <tr>
                  <!-- SIZE (MM) -->
                  <th>LENGTH</th>
                  <th>WIDTH</th>
                  <th>THK</th>
                  <th>OD</th>
                  <th>SCH</th>
                  <!-- QTY RECEIVED -->
                  <th class="bg-success">QTY</th>
                  <th class="bg-success">LENGTH</th>
                  <!-- WEIGHT -->
                  <!-- <th class="bg-success">UNIT WEIGHT<br>(LENGTH,SHEET)</th>
                  <th class="bg-success">UNIT WEIGHT<br>mm,1Sqmm)</th>
                  <th class="bg-success">TOTAL WEIGHT<br>Length/Sheet)</th>
                  <th class="bg-success">TOTAL WEIGHT<br>mm/Sqm)</th> -->
                  <!-- VALUE -->
                  <!-- <th class="bg-success">VALUE<br>(Length/Sheet)</th>
                  <th class="bg-success">VALUE<br>(mm/1Sqm)</th>
                  <th class="bg-success">TOTAL VALUE<br>(Length/Sheet)</th>
                  <th class="bg-success">TOTAL VALUE<br>(mm/Sqm)</th> -->
                  <!-- OS&D -->
                  <th class="bg-danger">OVER <!-- <br>(Length,Sheet) --></th>
                  <th class="bg-danger">SHORTAGE <!-- <br>(Length,Sheet) --></th>
                  <th class="bg-danger">DAMAGE <!-- <br>(Length,Sheet) --></th>
                  <!-- ISSUED -->
                  <th class="bg-orange"><!-- (ISSUED)<br> -->QTY<!-- (Length,Sheet) --></th>
                  <th class="bg-orange"><!-- (ISSUED)<br> -->LENGTH<!-- (Length,Sheet) --></th>
                  <!-- RETURN FROM FABRICATION -->
                  <th class="bg-secondary"><!-- (RETURN)<br> -->QTY<!--  (Length,Sheet) --></th>
                  <th class="bg-secondary"><!-- (RETURN)<br> -->LENGTH<!--  (mm,Sqm) --></th>
                  <!-- <th class="bg-secondary">(RETURN)<br>TOTAL WEIGHT </th>
                  <th class="bg-secondary">(RETURN)<br>TOTAL VALUE</th> -->
                  <!-- STOCK ON HAND -->
                  <th class="bg-info"><!-- (Balance)<br> -->QTY<!--  (Length,Sheet) --></th>
                  <th class="bg-info"><!-- (Balance)<br> -->LENGTH<!--  (mm,Sqm) --></th>
                  <!-- <th class="bg-info">(Balance)<br>WEIGHT </th>
                  <th class="bg-info">(Balance)<br>VALUE</th> -->
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  // $('#msr_list_data').DataTable({
  //   "scrollX": true,
  //   "order": [],
  //   "fixedColumns":   {
  //     "leftColumns": 2,
  //   }
  // });
  $('#msr_list_data').DataTable({
    "language": { 
    "infoFiltered": "" },
    "paging": true,
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax": {
        "url": "<?php echo base_url();?>mb/msr_list_json",
        "type": "POST",
        "data":{
        <?php if($this->input->get('project_id')): ?>
          "project_id" : <?php echo $this->input->get('project_id') ?>,
        <?php endif; ?>
        <?php if($this->input->get('supply_type')): ?>
          "supply_type" : '<?php echo $this->input->get('supply_type') ?>',
        <?php else: ?>
          "supply_type" : 'SS',
        <?php endif; ?>
        }
    },
    "scrollX": true,
    "columnDefs": [{
      "targets": 'no-sort',
      "orderable": false,
    }, ],
    "fixedColumns":   {
      "leftColumns": 2,
    }

  });
</script>