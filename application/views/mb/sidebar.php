<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) || $sidebar_close == 1 ? 'active' : '') ?>">
  <ul class="list-unstyled components">

    <!-- <li>
      <a href="<?php echo base_url();?>mb/mb_list">
        <i class="fas fa-pallet"></i> &nbsp; Material Balance
      </a>
    </li> -->
    <?php if($read_permission[62] == 1){ ?>
    
    <li>
      <a href="<?php echo base_url();?>mb/msr_list">
        <i class="fas fa-cubes"></i> &nbsp; Material Status Report
      </a>
    </li>
    <?php if($read_permission[63] == 1){ ?>
    <li>
      <a href="<?php echo base_url();?>mb/tr_list">
        <i class="fas fa-exchange-alt"></i> &nbsp; Transaksi
      </a>
    </li>
    <?php } ?>
    <?php } ?>

  </ul>
</nav>