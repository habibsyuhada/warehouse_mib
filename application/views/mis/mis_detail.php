<?php 
  
  $mis_list = $mis_list[0];


?>
<script>
  <?php for($i=1;$i<=sizeof($mis_list_detail);$i++){ ?>

   function checkunique<?php echo $i; ?>(input, num) {

    var no=1;
    var text<?php echo $i; ?> = input;
    var empty_val = "-";
    var project_selected = $("input[id='project_id']").val();
    var drawing_no = $("input[id='drawing_no']").val();
        
         
      $.ajax({
        
        url: "<?php echo base_url(); ?>mis/unique_no_check_det",
        type: "post",
        data: {
          'id_detail': text<?php echo $i; ?>,
          'project_id': project_selected,
          'drawing_no': drawing_no,
        },

        success: function(data) {
          var dup = 0;
          
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');
                  
            $("input[name='description["+num+"]']").val(empty_val);

            $("input[name='thk_mm["+num+"]']").val(empty_val);
            $("input[name='width_m["+num+"]']").val(empty_val);
            $("input[name='length_m["+num+"]']").val(empty_val);
            $("input[name='weight["+num+"]']").val(empty_val);
            $("input[name='od["+num+"]']").val(empty_val);
            $("input[name='sch["+num+"]']").val(empty_val);

            $("input[name='unit["+num+"]']").val(empty_val);
            $("input[name='tag_heat_no["+num+"]']").val(empty_val);

            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            
            var res = data.split("; ");

            $("input[name='description["+num+"]']").val(res[0]);

            $("input[name='thk_mm["+num+"]']").val(res[1]);
            $("input[name='width_m["+num+"]']").val(res[2]);
            $("input[name='length_m["+num+"]']").val(res[3]);
            $("input[name='weight["+num+"]']").val(res[4]);
            $("input[name='od["+num+"]']").val(res[5]);
            $("input[name='sch["+num+"]']").val(res[6]);

            $("input[name='unit["+num+"]']").val(res[7]);
            $("input[name='tag_heat_no["+num+"]']").val(res[8]);
            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
   }


<?php } ?>

</script>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mis/approve_mis_form" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">


              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                   <label>Project</label>
                      <input type="hidden" class="form-control" name="project_id" id="project_id" value='<?php echo $mis_list['project_id']; ?>' readonly>         
                      <input type="text" class="form-control" name="project_name" id="project_name" value='<?php echo $project_list[$mis_list['project_id']]; ?>' readonly>

                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                    <label>MIS No</label>
                        <input type="text" class="form-control" name="mrs_no" id="mrs_no" value='<?php echo $mis_list['mrs_no']; ?>' readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Sub Contractor Name</label>
                        <input type="text" class="form-control" name="sub_contractor_name" value='<?php echo $mis_list['sub_contractor_name']; ?>' placeholder="---" readonly>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                     <label>Date</label>
                        <input type="date" class="form-control" name="created_date" id="created_date"  value='<?php echo date("Y-m-d",strtotime($mis_list['created_date'])); ?>' placeholder="---" readonly> 
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Line No.</label>
                        <input type="text" class="form-control" name="line_no" id="line_no"  value='<?php echo $mis_list['line_no']; ?>' placeholder="---" readonly>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Material Class</label>
                     <select  class="custom-select select2" id='material_class' name="material_class" required>
                          <option value="">---</option>
                          <?php foreach($material_class as $material_class): ?>
                            <option value="<?php echo $material_class['material_class'] ?>"  <?php if($mis_list['material_class'] != $material_class['material_class']){ echo "disabled"; } else { echo "selected"; }  ?>><?php echo $material_class['material_class'] ?></option>
                          <?php endforeach; ?>
                      </select>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Drawing Number</label>
                        <input type="text" class="form-control" name="drawing_no" id="drawing_no" value='<?php echo $mis_list['drawing_no']; ?>'  readonly>                
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Equipment Tag No.</label>
                        <input type="text" class="form-control" name="equipment_tag_no"  value='<?php echo $mis_list['equipment_tag_no']; ?>' readonly>
                  </div>
                </div>
              </div>
            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" >
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th rowspan="2">UNIQUE NO.</th>
                          <th rowspan="2">DESCRIPTION</th>
                          <th  colspan="6">SIZE</th>
                        
                          
                          <th rowspan="2">TAG/HEAT NO</th>
                          <th colspan="2">ISSUED</th>
                          <th rowspan="2">UOM</th>
                         
                          <th rowspan="2">LOCATION</th>
                          <th rowspan="2">REMARKS</th>
                          
                        </tr>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th>THK<br/>(MM)</th>
                          <th>WIDTH<br/>(M)</th>
                          <th>LENGTH<br/>(M)</th>
                          <th>WEIGHT<br/>(KG)</th>
                          <th>OD<br/>(M)</th>
                          <th>SCH<br/>(M)</th>

                          <th>QTY<br/>(PCS)</th>
                          <th>LENGTH<br/>(M)</th>
                        </tr> 

                      </thead>
                      <tbody id="table_element" class="table-border">

                      <?php $no=1; foreach ($mis_list_detail as $key) { ?>
                        <tr>
                          <td>
                            <input type="text" class='form-control' id='unique_no[<?php echo $no; ?>]' value='<?php echo $key['unique_no']; ?>' readonly>

                             <script> checkunique<?php echo $no;  ?>(<?php echo $key["id_mis_det"]; ?>,<?php echo $no; ?>);</script>
                          </td>
                          <td><input type="text" class='form-control' name='description[<?php echo $no; ?>]' readonly></td>


                          <td><input type="text" class='form-control' name='thk_mm[<?php echo $no; ?>]' readonly></td>
                          <td><input type="text" class='form-control' name='width_m[<?php echo $no; ?>]' readonly></td>
                          <td><input type="text" class='form-control' name='length_m[<?php echo $no; ?>]' readonly></td>
                          <td><input type="text" class='form-control' name='weight[<?php echo $no; ?>]' readonly></td>
                          <td><input type="text" class='form-control' name='od[<?php echo $no; ?>]' readonly></td>
                          <td><input type="text" class='form-control' name='sch[<?php echo $no; ?>]' readonly></td>

                          <td><input type="text" class='form-control' name='tag_heat_no[<?php echo $no; ?>]' readonly></td>
                          <td><input type="text" class='form-control' value='<?php echo $key['qty']; ?>' readonly> </td>
                          <td><input type="text" class='form-control' value='<?php echo $key['length']; ?>' readonly> </td>
                          <td><input type="text" class='form-control' name='unit[<?php echo $no; ?>]' readonly></td>
                          <td><input type="text" class='form-control' value='<?php echo $key['location']; ?>' readonly> </td>
                          <td><input type="text" class='form-control' value='<?php echo $key['remarks']; ?>' readonly> </td>

                           </tr>
                      <?php $no++;} ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <?php if($read_permission[49] == 1){ ?>
                <a href="<?php echo base_url(); ?>mis/mis_pdf/<?php echo strtr($this->encryption->encrypt($mis_list['mrs_no']), '+=/', '.-~'); ?>" class="btn btn-primary" title="Print Out Document"><i class="fas fa-file-pdf"></i> Print Out</a>
            <?php } ?>

            <?php if($read_permission[50] == 1){ ?>
           
              <?php if($mis_list['status'] == '1'){ ?>
                <a href="#" class="btn btn-success" title="Approve This Request" onclick="validate_approve(3);"><i class="far fa-check-circle"></i> &nbsp;Approval</a>
                <a href="#" class="btn btn-danger" title="Rejected This Request" onclick="validate_approve(2);"><i class="fas fa-times-circle"></i></i> &nbsp;Rejected</a>
              <?php } ?>

            <?php } ?>


            <a href="<?php echo base_url();?>mis/mis_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mis_list['mrs_no']; ?>/"+param;
      }
    })

  }
</script>