<div id="content" class="container-fluid">
  
  <div class="row">
    <div class="col-md-12">

      <?php //if($read_permission[2] == 1){ ?>

      <form method="POST" action="<?php echo base_url();?>mis/mis_import_preview" enctype="multipart/form-data">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?> </h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Template Excel</label>
              <div class="col-sm-10 col-form-label">
                <a class='btn btn-primary' href="<?php echo base_url(); ?>/file/mis/Template_import_mis.xlsx"><i class="fas fa-download"></i> &nbsp;Download MIS Template</a>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Upload MIS Data</label>
              <div class="col-sm-10">
                <div class="custom-file">
                  <input type="file" name="file" class="custom-file-input" onChange="$('#label1.custom-file-label').html($(this).val())">
                  <label id="label1" class="custom-file-label">Choose file</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="text-right mt-3">
          <?php if($read_permission[45] == 1){ ?>
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
          <?php } ?>  
        </div>
      </div>
      </form>

      <?php //} ?>

    </div>
  </div>

  
  
</div>
</div><!-- ini div dari sidebar yang class wrapper -->