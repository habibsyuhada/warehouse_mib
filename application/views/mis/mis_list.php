
<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center" id='mis_list_data'>
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>Request By</th>
                  <th>Date Create</th> 
                  <th>MIS No</th>                   
                  <th>Project</th>
                  <th>Sub Contractor Name</th>
                  <th>Line No.</th>
                  <th>Drawing NO</th>
                  <th>Material Class</th>                                  
                  <th>Equipment Tag No</th>                                  
                  <th>Status</th>                                  
                  <th>Action</th>                                  
                </tr>
              </thead>
              
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('#mis_list_data').DataTable({
    "language": { 
      "infoFiltered": "" },
      "paging": true,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
          "url": "<?php echo base_url();?>mis/mis_list_json/<?php echo $req_pages; ?>",
          "type": "POST",
      },
      "columnDefs": [{
        "targets": [0],
        "orderable": true,
      }, ],
  });
</script>