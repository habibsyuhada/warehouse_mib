<?php 

  $mis_list     = $mis_list[0];
  $project_list = $this->general_mod->read_project_name($mis_list['project_id']);

  //print_r($receiving_list_detail);

?>
<!DOCTYPE html>
<html><head>
  <title>MIS/<?php echo $project_list[0]['project_code']; ?>/<?php echo $mis_list['mrs_no']; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 5.2cm;
      margin-left: 2.3cm;
      margin-right: 2.3cm;
      margin-bottom: 3cm;
      font-family: "helvetica";
      font-size: 10px !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 5cm;
      padding-top: 1px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    
    }

    footer {
      position: fixed;
      bottom: 1cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      padding-bottom: 2.5px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    
    }


    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;
     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table>thead>tr>td,table>tbody>tr>td{
      vertical-align: top;
    }

    .br_break{
      line-height: 15px;
    }

    .br_break_no_bold{
      line-height: 18px;
    }

    .br{
      border-right: 1px #000 solid;
    }
    .bl{
      border-left: 1px #000 solid;
    }
    .bt{
      border-top: 1px #000 solid;
    }
    .bb{
      border-bottom:  1px #000 solid;
    }
    .bx{
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .by{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
    }
    .ball{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .tab{
      display: inline-block; 
      width: 100px;
    }
    .tab2{
      display: inline-block; 
      width: 100px;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }

  </style>
</head><body>

  <header>
  	<img src="img/logo.png" style="width: 100px; padding-top: 25px;"><br><br>
  	<table width="100%">
  		<tr>
  			<td style="padding-bottom: 4px;font-size: 15px !important;"><center><u><h1>MATERIAL ISSUE SLIP</h1></u></center></td>
      </tr>     
  	</table>
    <table width="100%" align="center">
      <tr>
        <td style="width: 150px !important;">PROJECT</td>
        <td style="width: 5px !important;">:</td>
        <td style="width: 300px !important;"><?php echo $project_list[0]['project_name']; ?></td>
        <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
      </tr>  
      <tr>
        <td style="width: 150px !important;">SUBCONTRACTOR NAME</td>
        <td style="width: 5px !important;">:</td>
        <td style="width: 300px !important;"><?php echo $mis_list['sub_contractor_name']; ?></td>
        <td>&nbsp;</td>
        <td style="width: 150px !important;">MRS NO.</td>
        <td style="width: 5px !important;">:</td>
        <td style="width: 300px !important;">MIS/<?php echo $project_list[0]['project_code']; ?>/<?php echo $mis_list['mrs_no']; ?></td>
      </tr> 
      <tr>
        <td style="width: 150px !important;">DRAWING NO.</td>
        <td style="width: 5px !important;">:</td>
        <td style="width: 300px !important;"><?php echo $mis_list['drawing_no']; ?></td>
        <td>&nbsp;</td>
        <td style="width: 150px !important;">DATE</td>
        <td style="width: 5px !important;">:</td>
        <td style="width: 300px !important;"><?php echo $mis_list['created_date']; ?></td>
      </tr> 
      <tr>
        <td style="width: 150px !important;">EQUIPMENT TAG NO.</td>
        <td style="width: 5px !important;">:</td>
        <td style="width: 300px !important;"><?php echo $mis_list['equipment_tag_no']; ?></td>
        <td>&nbsp;</td>
        <td style="width: 150px !important;">MATERIAL CLASS</td>
        <td style="width: 5px !important;">:</td>
        <td style="width: 300px !important;"><?php echo $mis_list['material_class']; ?></td>
      </tr>   
    </table>
  </header>
  <footer>
    <table width='100%' style='border-collapse: collapse; font-size: 8 !important;'>
      <tr>
        <td width='100px'>Request By</td>
        <td>:</td>
        <td width='150px'><?= (isset($user_list[$mis_list['created_by']]) ? $user_list[$mis_list['created_by']] : '-') ?></td>
        <td>&nbsp;</td>
        <td width='150px'>Approved By</td>
        <td>:</td>
        <td></td>
        <td>&nbsp;</td>
        <td width='150px'>Approved By</td>
        <td>:</td>
        <td></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width='100px'>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td width='150px'>( Area Supervisor )</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td width='150px'>( Material Controller )</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width='100px'>Material Issued By</td>
        <td>:</td>
        <td></td>
        <td>&nbsp;</td>
        <td width='150px'>Acknowledged by</td>
        <td>:</td>
        <td></td>
        <td>&nbsp;</td>
        <td width='150px'>Input in to computer</td>
        <td>YES</td>
        <td></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width='100px'>( Warehouse Man )</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td width='150px'>( Sub Contractor )</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td width='150px'>Date</td>
        <td>NO</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </footer>

  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-left: -25px;">
  <thead><tr>
        <th rowspan="2" style="font-size: 9px; width: 20px;"><b>ITEM NO.</b></th>
        <th rowspan="2" style="font-size: 9px; width: 20px;"><b>UNIQUE NO.</b></th>
        <th rowspan="2" style="font-size: 9px; width: 70px;"><b>DESCRIPTION</b></th>
        <th colspan="6" style="font-size: 9px; width: 100px; max-height: 80;"><b>SIZE</b></th>
        <th rowspan="2" style="font-size: 9px; width: 20px;"><b>TAG/HEAT NO.</b></th>
        <th colspan="2" style="font-size: 9px; width: 30px;"><b>ISSUED</b></th>
        <th rowspan="2" style="font-size: 9px; width: 20px;"><b>REMARKS</b></th>        
    </tr><tr>
      <th style="font-size: 9px;">THK<br/>(MM)</th>
      <th style="font-size: 9px;">WIDTH<br/>(M)</th>
      <th style="font-size: 9px;">LENGTH<br/>(M)</th>
      <th style="font-size: 9px;">WEIGHT<br/>(KG)</th>
      <th style="font-size: 9px;">OD<br/>(M)</th>
      <th style="font-size: 9px;">SCH<br/>(M)</th>

      <th style="font-size: 9px;">QTY (PCS)</th>
      <th style="font-size: 9px;">LENGTH (M)</th>

    </tr></thead>

  <tbody><?php $no=1; foreach($mis_list_detail as $mis_list_detail){ ?><tr>
      <td><?= $no ?></td>
      <td><?php echo strtoupper($mis_list_detail["unique_no"]); ?></td>
      <td><?= (isset($description_list[$mis_list_detail["unique_no"]]) ? $description_list[$mis_list_detail["unique_no"]] : '-') ?></td>

      <td><?= (isset($length_list[$mis_list_detail["unique_no"]]) ? $length_list[$mis_list_detail["unique_no"]] : '-') ?></td>
      <td><?= (isset($width_od_list[$mis_list_detail["unique_no"]]) ? $width_od_list[$mis_list_detail["unique_no"]] : '-') ?></td>
      <td><?= (isset($sch_thk_list[$mis_list_detail["unique_no"]]) ? $sch_thk_list[$mis_list_detail["unique_no"]] : '-') ?></td>
      <td><?= (isset($sch_thk_list[$mis_list_detail["unique_no"]]) ? $sch_thk_list[$mis_list_detail["unique_no"]] : '-') ?></td>
      <td><?= (isset($sch_thk_list[$mis_list_detail["unique_no"]]) ? $sch_thk_list[$mis_list_detail["unique_no"]] : '-') ?></td>
      <td><?= (isset($sch_thk_list[$mis_list_detail["unique_no"]]) ? $sch_thk_list[$mis_list_detail["unique_no"]] : '-') ?></td>

      
      <td><?= (isset($heat_no_list[$mis_list_detail["unique_no"]]) ? $heat_no_list[$mis_list_detail["unique_no"]] : '-') ?></td>
      <td><?= $mis_list_detail["qty"] ?></td>
      <td><?= $mis_list_detail["length"] ?></td>
      
      <td>&nbsp;</td>  
    </tr><?php $no++; } ?></tbody>  
  </table>

  
  

</body></html>