<?php 
  $sheet_master = $sheet[2];
?>
<script>
  <?php for($i=1;$i<=sizeof($sheet);$i++){ ?>

    var no=1;
    var delayTimer<?php echo $i; ?>;
    var arr_uniq = [];

   function checkunique<?php echo $i; ?>(input, num) {

    var text<?php echo $i; ?> = input;
    var empty_val = "";

      $.ajax({
        url: "<?php echo base_url(); ?>mis/unique_no_check",
        type: "post",
        data: {
          'unique_no': text<?php echo $i; ?>,
        },
        success: function(data) {
          var dup = 0;
           
          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='unique_no"+i+"']").val() == text<?php echo $i; ?>){
                 data = 'Error : Duplicate Unique No on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

            $("input[id='unique_no"+num+"']").addClass('is-invalid');
            $("input[id='unique_no"+num+"']").after('<div class="invalid-feedback">'+data+'</div>');
            $("input[name='description["+num+"]']").val(empty_val);
            $("input[name='width["+num+"]']").val(empty_val);
            $("input[name='thk["+num+"]']").val(empty_val);
            $("input[name='length["+num+"]']").val(empty_val);
            $("input[name='unit["+num+"]']").val(empty_val);
            $("input[name='tag_heat_no["+num+"]']").val(empty_val);
            $("input[name='balance["+num+"]']").val(empty_val);
            $("input[name='balance_length["+num+"]']").val(empty_val);
            $("input[name='issued_qty["+num+"]']").val(empty_val);
            $("input[name='location["+num+"]']").val(empty_val);
            $("input[name='remarks["+num+"]']").val(empty_val);
            
          } else {

            $("input[id='unique_no"+num+"']").removeClass('is-invalid');
            $("input[id='unique_no"+num+"']").addClass('is-valid');

            var res = data.split("; ");

            $("input[name='description["+num+"]']").val(res[0]);
            $("input[name='width["+num+"]']").val(res[1]);
            $("input[name='thk["+num+"]']").val(res[2]);
            $("input[name='length["+num+"]']").val(res[3]);
            $("input[name='unit["+num+"]']").val(res[4]);
            $("input[name='tag_heat_no["+num+"]']").val(res[5]);
            $("input[name='balance["+num+"]']").val(res[6]);
            $("input[name='balance_length["+num+"]']").val(res[7]);

            <?php $nox = $i + 1; ?>

            var text    = '<?php echo $sheet[$nox]["H"]; ?>';

            console.log(text);

            var bal_qty = res[6];
            var length  = res[7];
            var total_length_by_unt = Number(length) / Number(bal_qty);
            var actual_length       = Number(text) * total_length_by_unt;
            var actual_length_dsp   = actual_length.toFixed(2);

            $("input[name='issued_length["+num+"]']").val(actual_length_dsp); 

            $('button[name=submitBtn]').prop("disabled", false);
           
          }
         
        }
      });
    
  }


function checkbalance<?php echo $i; ?>(input, num) {

    var text<?php echo $i; ?>      = input;
    var unique_no<?php echo $i; ?> = $('input[id="unique_no'+num+'"').val();
    var empty_val = "-";
   
      $.ajax({
        url: "<?php echo base_url(); ?>mis/balance_checking",
        type: "post",
        data: {
          'issued_qty': text<?php echo $i; ?>,
          'unique_no': unique_no<?php echo $i; ?>,
        },
        success: function(data) {
        
          if(data.includes("Error")){
            $("input[id='issued_qty"+num+"']").addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $("input[id='issued_qty"+num+"']").after('<div class="invalid-feedback">'+data+'</div>');

            $('button[id=submitBtn]').prop("disabled", true);

            console.log(data);
            
          } else {

            $("input[id='issued_qty"+num+"']").addClass('is-valid');
            $("input[id='issued_qty"+num+"']").after('<div class="valid-feedback">'+data+'</div>');
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  
  }

<?php } ?>

function unique_no_func(no){
   
  $("input[name='unique_no["+no+"]']").autocomplete({
      source: function(request,response){
        $.post('<?php echo base_url(); ?>mis/uniqno_autocomplete',{term: request.term}, response, 'json');
      },
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });
  }

 function checkunique(input, num) {

    var text = $(input).val();
    var empty_val = "-";
   
      $.ajax({
        url: "<?php echo base_url(); ?>mis/unique_no_check",
        type: "post",
        data: {
          'unique_no': text,
        },
        success: function(data) {
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='unique_no"+i+"']").val() == text){
                console.log($("input[id='unique_no"+num+"']").val()+"--"+text);
                data = 'Error : Duplicate Unique No on the list!';
              }
            }
          }
          

          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $("input[name='description["+num+"]']").val(empty_val);
            $("input[name='width["+num+"]']").val(empty_val);
            $("input[name='thk["+num+"]']").val(empty_val);
            $("input[name='length["+num+"]']").val(empty_val);
            $("input[name='unit["+num+"]']").val();
            $("input[name='tag_heat_no["+num+"]']").val(empty_val);
            $("input[name='balance["+num+"]']").val(empty_val);
            $("input[name='balance_length["+num+"]']").val(empty_val);
                                 
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $("input[name='description["+num+"]']").val(res[0]);
            $("input[name='width["+num+"]']").val(res[1]);
            $("input[name='thk["+num+"]']").val(res[2]);
            $("input[name='length["+num+"]']").val(res[3]);
            $("input[name='unit["+num+"]']").val(res[4]);
            $("input[name='tag_heat_no["+num+"]']").val(res[5]);
            $("input[name='balance["+num+"]']").val(res[6]);
            $("input[name='balance_length["+num+"]']").val(res[6]);
            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
    
  }


  function checkbalance(input, num) {

    var text      = $(input).val();
    var unique_no = $('input[id="unique_no'+num+'"').val();
    var empty_val = "-";

      $.ajax({
        url: "<?php echo base_url(); ?>mis/balance_checking",
        type: "post",
        data: {
          'issued_qty': text,
          'unique_no': unique_no,
        },
        success: function(data) {
        
          if(data.includes("Error")){
            
            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $('.valid-feedback').remove( ":contains('Available')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else{

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            $('.valid-feedback').remove( ":contains('Available')" );
            $(input).after('<div class="valid-feedback">'+data+'</div>');
            $('button[name=submitBtn]').prop("disabled", false);
         
          }
        }
      });
    }


  function delete_element(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_process(count);

      }
    })
  }

  function delete_element_process(count){
    $('#tr_element_' + count).remove();
  }



  var count_element = <?php echo sizeof($sheet); ?>;
  function add_element(){

    var html_element =  '<tr id="tr_element_' + count_element + '">' +
                          '<td><input type="text" id="unique_no'+count_element+'" onfocus="unique_no_func(' + count_element + ');" name="unique_no[' + count_element + ']" class="form-control" onblur="checkunique(this,' + count_element + ');"></td>' +
                          '<td><input type="text" id="description[' + count_element + ']" name="description[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="width[' + count_element + ']" name="width[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="thk[' + count_element + ']" name="thk[' + count_element + ']" class="form-control" readonly></td>' + 
                          '<td><input type="text" id="length[' + count_element + ']" name="length[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="unit[' + count_element + ']" name="unit[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="tag_heat_no[' + count_element + ']" name="tag_heat_no[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="balance[' + count_element + ']" name="balance[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="balance_length[' + count_element + ']" name="balance_length[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="number" name="issued_qty[' + count_element + ']" class="form-control" placeholder="*" required onblur="checkbalance(this,' + count_element + ');" oninput="auto_length(this,' + count_element + ');"></td>' +
                           '<td><input type="number" name="issued_length[' + count_element + ']" class="form-control" placeholder="*" required  step="any" readonly></td>' +
                          '<td><input type="text" name="location[' + count_element + ']" class="form-control" placeholder="*" required></td>' +
                          '<td><textarea type="text" name="remarks[' + count_element + ']" class="form-control" placeholder="*" ></textarea></td>' +
                          '<td><button class="btn btn-danger" type="button" onclick="delete_element(' + count_element + ')"><i class="fa fa-trash"></i></button></td>' + 
                        '</tr>';

    $('#table_element').append(html_element);

    count_element++;
  }

</script>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mis/mis_new_form" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">


              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                   <label>Project</label>
                     <select  class="custom-select select2" name="project_id" required>
                        
                          <?php foreach($get_project as $get_project): ?>
                            <option value="<?php echo $get_project['id'] ?>" <?php if($sheet_master['A'] != $get_project['project_code']){ echo "disabled"; } else { echo "selected"; } ?>><?php echo $get_project['project_name'] ?></option>
                          <?php endforeach; ?>
                      </select>          
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                    <label>MIS No</label>
                        <input type="text" class="form-control" name="mrs_no" id="mrs_no" value='<?php echo $get_mis_no; ?>' readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Sub Contractor Name</label>
                        <input type="text" class="form-control" name="sub_contractor_name" value='<?php echo $sheet_master['E']; ?>' placeholder="---" readonly>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                     <label>Date</label>
                        <input type="date" class="form-control" name="created_date" id="created_date"  value='<?php echo date("Y-m-d"); ?>' placeholder="---" readonly> 
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Line No.</label>
                        <input type="text" class="form-control" name="line_no" id="line_no"  value='<?php echo $sheet_master['G']; ?>' placeholder="---" readonly>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Material Class</label>
                     <select  class="custom-select select2" id='material_class' name="material_class" required>
                       
                          <?php foreach($material_class as $material_class): ?>
                            <option value="<?php echo $material_class['material_class'] ?>"  <?php if($sheet_master['C'] != $material_class['material_class']){ echo "disabled"; } else { echo "selected"; }  ?>><?php echo $material_class['material_class'] ?></option>
                          <?php endforeach; ?>
                      </select>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Drawing Number</label>
                        <input type="text" class="form-control" name="drawing_no" id="drawing_no" value='<?php echo $sheet_master['B']; ?>'  readonly>                
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Equipment Tag No.</label>
                        <input type="text" class="form-control" name="equipment_tag_no"  value='<?php echo $sheet_master['F']; ?>' readonly>
                  </div>
                </div>
              </div>
            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" >
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th>UNIQUE NO.</th>
                          <th>DESCRIPTION</th>
                          <th>OD/WIDTH</th>
                          <th>THK</th>
                          <th>LENGTH</th>
                          <th>UNIT</th>
                          <th>TAG/HEAT NO</th>
                          <th>BALANCE</th>
                          <th>BALANCE LENGTH</th>
                          <th>ISSUED QTY</th>
                          <th>ISSUED LENGTH</th>                          
                          <th>LOCATION</th>
                          <th>REMARKS</th>
                          <th width="180">
                            <button class="btn btn-success float-right" style='background-color: #004cc2;' type="button" onclick="add_element()"><i class="fa fa-plus"></i> Add Material</button>
                          </th>
                          
                        </tr>
                        
                      </thead>
                      <tbody id="table_element" class="table-border">

                      <?php $no=1; foreach ($sheet as $key) { ?>
                        <?php if($key["G"] !== "UNIQUE NO"){ ?>
                        <tr id="tr_element_<?php echo $no; ?>">
                          <td>
                            <input type="text" class='form-control' id='unique_no<?php echo $no; ?>' name='unique_no[<?php echo $no; ?>]' value='<?php echo $key["G"]; ?>' onfocus="unique_no_func(<?php echo $no; ?>);" onblur="checkunique(this,<?php echo $no; ?>);" required>

                             <script> checkunique<?php echo $no;  ?>('<?php echo $key["G"]; ?>',<?php echo $no; ?>);</script>
                          </td>
                          <td><input type="text" class='form-control' name='description[<?php echo $no; ?>]' readonly required></td>
                          <td><input type="text" class='form-control' name='width[<?php echo $no; ?>]' readonly required></td>
                          <td><input type="text" class='form-control' name='thk[<?php echo $no; ?>]' readonly required></td>
                          <td><input type="text" class='form-control' name='length[<?php echo $no; ?>]' readonly required></td>                        
                          <td><input type="text" class='form-control' name='unit[<?php echo $no; ?>]' readonly required></td>
                          <td><input type="text" class='form-control' name='tag_heat_no[<?php echo $no; ?>]' readonly required></td>
                          <td><input type="text" class='form-control' name='balance[<?php echo $no; ?>]' readonly required></td>
                          <td><input type="text" class='form-control' name='balance_length[<?php echo $no; ?>]' readonly required></td>
                          
                          <td>
                            <input type="number" class='form-control' id='issued_qty<?php echo $no; ?>' name='issued_qty[<?php echo $no; ?>]'  onblur="checkbalance(this,'<?php echo $no; ?>');" value='<?php echo $key["H"]; ?>' oninput="auto_length(this,<?php echo $no; ?>);" required> 

                            <script> checkbalance<?php echo $no;  ?>('<?php echo $key["H"]; ?>',<?php echo $no; ?>);</script>
                          </td>

                          <td>
                            <input type="number" name="issued_length[<?php echo $no; ?>]" class="form-control" placeholder="*" required   readonly step="any" />

                          </td>

                          <td><input type="text" class='form-control' name='location[<?php echo $no; ?>]' value='<?php echo $key['I']; ?>'  required> </td>
                          <td><input type="text" class='form-control' name='remarks[<?php echo $no; ?>]' value='<?php echo $key['J']; ?>'  required> </td>
                          <td><button class="btn btn-danger" type="button" onclick="delete_element('<?php echo $no; ?>')"><i class="fa fa-trash"></i></button></td>
                           </tr>
                      <?php 
                      $no++; }
                      } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-right mt-3">

            <button type='submit' name='submitBtn' id='submitBtn' value='submit' class="btn btn-success" title="Submit" disabled ><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>pcms/mto" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
function auto_length(input,num) {

    var text = $(input).val();

    var bal_qty = $("input[name='balance["+num+"]']").val();
    var length = $("input[name='balance_length["+num+"]']").val();

    var total_length_by_unt = Number(length) / Number(bal_qty);

    var actual_length = Number(text) * total_length_by_unt;
    var actual_length_dsp = actual_length.toFixed(2);

    $("input[name='issued_length["+num+"]']").val(actual_length_dsp);      

   }

</script>