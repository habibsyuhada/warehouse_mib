<?php 
  $mr_list            = $mr_list[0];
  //$mr_list_detail     = $mr_list_detail[0];
  //$mr_list_quo        = $mr_list_quo[0];
  //$mr_list_tabulation = $mr_list_tabulation[0];

  //print_r($mr_list_tabulation);

  $total_material = sizeof($mr_list_detail);
  $total_mat_add = $total_material + 1;


 
?>

<?php
  $get_user    = $this->mr_mod->get_user_data_sign($mr_list['created_by']);
  $get_manager = $this->mr_mod->get_user_data_sign($mr_list['manager_on_duty']);


  ?>
<style>
/* The container */
.containerx {
  font-family: arial;
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.containerx input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmarkx {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.containerx:hover input ~ .checkmarkx {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.containerx input:checked ~ .checkmarkx {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmarkx:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.containerx input:checked ~ .checkmarkx:after {
  display: block;
}

/* Style the checkmark/indicator */
.containerx .checkmarkx:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

.red_title{
  color: red;
  font-weight: bold;
  font-size: 9px;
  font-style: italic;
}
.red_detail {
  color: red;
  font-style: italic;
  font-size: 10px;
  font-style: italic;
}
</style>

<script type='text/javascript'>
  $(window).on('load', function() { 
    <?php if(sizeof($mr_list_tabulation) <= 0){ ?>
      add_list_tab();
    <?php } ?>  

      <?php if(sizeof($mr_list_quo) <= 0){ ?>
        add_list_quotation();
      <?php } ?>
  });

  var count_element = <?php echo $total_mat_add; ?>;

  function add_list_material(){

    var html_element =  '<tr id="tr_element_material_' + count_element + '">' +

                          '<td>' + count_element + '<input type="hidden" id="nos'+count_element+'" name="nos[' + count_element + ']" value="' + count_element + '" class="form-control" readonly min="0"></td>' + 

                          '<td><textarea class="form-control" id="tec_spec'+count_element+'" name="tec_spec[' + count_element + ']"  required></textarea></td>' + 

                          '<td><input type="number" id="qty_req'+count_element+'" name="qty_req[' + count_element + ']" class="form-control" min="0"></td>' +  
                          
                          '<td><select id="uom_req'+count_element+'" name="uom_req[' + count_element + ']" class="form-control" required><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                         

                          '<td><input type="number" id="qty_order'+count_element+'" name="qty_order[' + count_element + ']" class="form-control"  min="0" required></td>' +  
                          
                          '<td><select id="uom_order'+count_element+'" name="uom_order[' + count_element + ']" class="form-control" required><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                          '<td><select id="currency'+count_element+'" name="currency[' + count_element + ']" onchange="change_overalcur(' + count_element + ');" class="form-control" required><?php foreach ($currency_list as $value) { echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; } ?></select> </td>' +

                          '<td><input type="number" id="estimate_price'+count_element+'" name="estimate_price[' + count_element + ']" class="form-control" required></td>' + 

                          '<td><button class="btn btn-danger" type="button" onclick="delete_element_material(' + count_element + ')"><i class="fas fa-trash-alt"></i></button></td>' +   

                        '</tr>';

    $('#table_material').append(html_element);

    count_element++;
  }

//----------------------------------------------------------------------------------------------------------------------------------------------------------
  var count_element_tab = 1;

  function add_list_tab(){

  var html_element_tab =  '<tr id="tr_element_so_' + count_element_tab + '">' +   
                            '<td>'+count_element_tab+'</td>' +
                            '<td><input type="hidden" id="nosX'+count_element_tab+'" name="nosX[' + count_element_tab + ']" value="' + count_element_tab + '" class="form-control" readonly min="0"><input type="file" id="file_tabulation'+count_element_tab+'" name="file_tabulation_' + count_element_tab + '" value="' + count_element_tab + '" class="form-control" readonly min="0"></td>' +
                            '</tr>';

    $('#table_tab').append(html_element_tab);

    count_element_tab++;
  }
  //----------------------------------------------------------------------------------------------------------------------------------------------------------

  //----------------------------------------------------------------------------------------------------------------------------------------------------------
  var count_elementx = 1;

  function add_list_quotation(){

    var html_element =  '<tr id="tr_element_quotation_' + count_elementx + '">' +
                          '<td><input type="text" id="reference_no'+count_elementx+'" name="reference_no[' + count_elementx + ']" class="form-control" placeholder="----" required></td>' +

                          '<td><select id="vendor'+count_elementx+'" name="vendor[' + count_elementx + ']" class="form-control" required><option value="">---</option><?php foreach ($vendor_list as $value) { echo "<option value=".$value["id_vendor"].">".$value["name"]."</option>"; } ?></select> </td>' +
                          
                          '<td><input type="number" id="total_amount_quo'+count_elementx+'" name="total_amount_quo[' + count_elementx + ']" class="form-control" min="0" required></td>' +

                          '<td><select id="currency_quo'+count_elementx+'" name="currency_quo[' + count_elementx + ']" class="form-control" required><?php foreach ($currency_list as $value) { echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; } ?></select> </td>' +

                          '<td>'+
                          '<input type="hidden" name="id_attachment[' + count_elementx + ']" value="' + count_elementx + '">' +
                          '<input type="file" name="file_att_' + count_elementx + '"  size = "10000">'+
                          ' </td>' + 
                          '<td><button class="btn btn-danger" type="button" onclick="delete_element_quotation(' + count_elementx + ')"><i class="fas fa-trash-alt"></i></button></td>' +   

                        '</tr>';

    $('#table_quotation').append(html_element);

    count_elementx++;
  }
  //----------------------------------------------------------------------------------------------------------------------------------------------------------

</script>
<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>mr/process_update_mr" enctype="multipart/form-data">
     <input type="hidden" class="form-control" name="dept_id" id="dept_id" value='<?php echo $department[0]['id_department']; ?>'>
     <input type="hidden" class="form-control" name="mr_number" id="mr_number" value='<?php echo $mr_list['mr_number']; ?>'>
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                       <table class="table">
                        <tr>
                            <td>Date</td>
                            <td>:</td>
                            <td><input type='date' class='form-control' name='date' value='<?php echo date("Y-m-d"); ?>' readonly></td>
                        </tr>
                        <tr>
                            <td>Category Request</td>
                            <td>:</td>
                            <td>
                                  <select class="form-control" name='request_sche' required="">
                                   
                                    <option value='Schedule' <?PHP if($mr_list["request_sche"] == "Schedule"){ echo "selected"; } else { echo "disabled"; } ?>>Schedule</option>
                                    <option value='Unschedule' <?PHP if($mr_list["request_sche"] == "Unschedule"){ echo "selected"; } else { echo "disabled"; } ?>>Unschedule</option>
                                  </select>
                                  <br>
                                  <select class="form-control" name='request_type' required="" disabled="">
                                   
                                    <option value='Material Request' <?PHP if($mr_list["request_type"] == "Material Request"){ echo "selected";  } else { echo "disabled"; } ?>>Material Request</option>
                                    <option value='Service Request' <?PHP if($mr_list["request_type"] == "Service Request"){ echo "selected";  } else { echo "disabled"; } ?>>Service Request</option>
                                  </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Project</td>
                            <td>:</td>
                            <td>
                                <input type='text' class='form-control' name='project' placeholder="---" required="" value="<?php echo $mr_list['project']; ?>">
                                <br/>
                                <select class="form-control" name='request_checklist' required="" disabled="">
                                   
                                    <option value='BOP' <?PHP if($mr_list["request_checklist"] == "BOP"){ echo "selected";  } else { echo "disabled"; } ?>>BOP</option>
                                    <option value='CCPP' <?PHP if($mr_list["request_checklist"] == "CCPP"){ echo "selected";  } else { echo "disabled"; } ?>>CCPP</option>
                                    <option value='Chiller' <?PHP if($mr_list["request_checklist"] == "Chiller"){ echo "selected";  } else { echo "disabled"; } ?>>Chiller</option>
                                    <option value='DEB1' <?PHP if($mr_list["request_checklist"] == "DEB1"){ echo "selected";  } else { echo "disabled"; } ?>>DEB1</option>
                                    <option value='DEB2' <?PHP if($mr_list["request_checklist"] == "DEB2"){ echo "selected";  } else { echo "disabled"; } ?>>DEB2</option>
                                    <option value='MEB1' <?PHP if($mr_list["request_checklist"] == "MEB1"){ echo "selected";  } else { echo "disabled"; } ?>>MEB1</option>
                                    <option value='MEB2' <?PHP if($mr_list["request_checklist"] == "MEB2"){ echo "selected";  } else { echo "disabled"; } ?>>MEB2</option>
                                    <option value='TM2500' <?PHP if($mr_list["request_checklist"] == "TM2500"){ echo "selected";  } else { echo "disabled"; } ?>>TM2500</option>
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Budget</label></td>
                            <td>:</td>
                            <td>
                              <select class="form-control" name='budget_type' id='budget_type'>
                                
                                <option value='Capex' <?PHP if($mr_list["budget_type"] == "Capex"){ echo "selected";  } else { echo "disabled"; } ?>>Capex</option>
                                <option value='Opex' <?PHP if($mr_list["budget_type"] == "Opex"){ echo "selected";  } else { echo "disabled"; } ?>>Opex</option>
                              </select>
                           </td>
                           </tr>
                        <tr>
                            <td>Posting Budget</td>
                            <td>:</td>
                            <td>
                              <select id='posting_budget' name='posting_budget' class="form-control" onchange="fnc(this);" style="display: inline;">
                                
                                <?php foreach ($budget as $value) { ?>
                                  <option value='<?php echo $value['id']; ?>' <?PHP if($mr_list["posting_budget"] == $value['id']){ echo "selected";  } else { echo "disabled"; } ?>><?php echo $value['account_no']; ?> </option>
                                <?php  } ?>
                              </select> 
                              <div class="col-md-6">
                                <div class="form-group">
                                  <table width="100%">
                                    <tr>
                                      <td><label><b>Budgeted Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='budget_amount'></span></b></label></td>
                                    </tr>
                                    <tr>
                                      <td><label><b>Transfered Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='transfered_amount'></span></b></label></td>
                                    </tr>
                                    <tr>
                                      <td><label><b>Utilized Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='utilized_amount'></span></b></label></td>
                                    </tr>
                                    <tr>
                                      <td><label><b>Balance Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='balance_amount'></span></b></label></td>
                                    </tr>  
                                  </table>    
                                </div>
                              </div>     
                            </td>
                        </tr>
                      </table>
                  </div>
                </div>
            </div>
            
            </div>
          </div>
        </div>
      </div>

      


      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="row align-items-center">
          <div class="col-md">
          <h6 class="pb-2 mb-0">List of Material Requisition</h6>
          </div>
          <div class="col-md text-right">
            <?php if($read_cookies[0] == $mr_list["created_by"]){ ?>
            <div class="btn btn-primary" onclick='add_list_material();'><i class="fas fa-plus"></i> Add</div>
            <?php } ?>
          </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <table id="table_material" class="table table-hover text-center">
                  <thead class="bg-green-smoe text-white">
                    <tr>
                      <th rowspan="2">No</th>
                     
                      <th rowspan="2">Tecnical Spesification</th>
                      <th colspan="2">Request*</th>                  
                      
                      <th colspan="2">Order*</th>
                      <th rowspan="2">Currency*</th>
                      <th rowspan="2">Estimate Price*</th>
                      <?php if($read_cookies[0] == $mr_list["created_by"]){ ?>
                      <th rowspan="2">Action</th>      
                       <?php } ?>   
                    </tr>
                    <tr>
                      <th>Qty</th>
                      <th>Unit</th>
                      
                      <th>Qty</th>
                      <th>Unit</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php $no = 1; foreach ($mr_list_detail as $detail) { ?>

                        <tr id="tr_element_material_<?php echo $no; ?>">

                          <td>
                              <?php echo $no; ?>
                              <input type="hidden" id="nos<?php echo $no; ?>" name="nos[<?php echo $no; ?>]" value="<?php echo $no; ?>" class="form-control" readonly min="0">
                          </td>

                         

                          <td>
                            <textarea class="form-control" id="tec_spec<?php echo $no; ?>" name="tec_spec[<?php echo $no; ?>]"  required><?php echo $detail['tec_spec']; ?></textarea>
                          </td> 

                          <td><input type="number" id="qty_req<?php echo $no; ?>" name="qty_req[<?php echo $no; ?>]" class="form-control" min="0" value='<?php echo $detail['qty_req']; ?>'></td>  
                          
                          <td>
                            <select id="uom_req<?php echo $no; ?>" name="uom_req[<?php echo $no; ?>]" class="form-control" required>
                              <?php foreach ($uom_list as $value) { ?>
                                <option value="<?php echo $value['id_uom']; ?>" <?php if($value['id_uom'] == $detail['uom_req']){ echo "selected"; } ?>>
                                  <?php echo $value["uom"]; ?>
                                </option>
                              <?php } ?>                              
                            </select>
                          </td>

                    

                          <td>
                            <input type="number" id="qty_order<?php echo $no; ?>" name="qty_order[<?php echo $no; ?>]" class="form-control"  min="0" required value='<?php echo $detail['qty_order']; ?>'>
                          </td>  
                          
                          <td>                           
                            <select id="uom_order<?php echo $no; ?>" name="uom_order[<?php echo $no; ?>]" class="form-control" required>
                              <?php foreach ($uom_list as $value) { ?>
                                <option value="<?php echo $value['id_uom']; ?>" <?php if($value['id_uom'] == $detail['uom_order']){ echo "selected"; } ?>>
                                  <?php echo $value["uom"]; ?>
                                </option>
                              <?php } ?>                              
                            </select>
                          </td>

                          <td>
                            <select id="currency<?php echo $no; ?>" name="currency[<?php echo $no; ?>]" class="form-control" required>
                              <?php foreach ($currency_list as $value) { ?>
                                <option value="<?php echo $value['id_cur']; ?>" <?php if($value['id_cur'] == $detail['currency']){ echo "selected"; } ?>>
                                  <?php echo $value["currency"]; ?>
                                </option>
                              <?php } ?>                              
                            </select>
                          </td>

                          <td><input type="number" id="estimate_price<?php echo $no; ?>" name="estimate_price[<?php echo $no; ?>]" class="form-control" required value='<?php echo $detail['estimate_price']; ?>'></td> 

                          <?php if($read_cookies[0] == $mr_list["created_by"]){ ?>
                          <td><button class="btn btn-danger" type="button" onclick="delete_element_material(<?php echo $no; ?>)"><i class="fas fa-trash-alt"></i></button></td>   
                          <?php } ?>
                        </tr>

                     <?php $no++; } ?>
                  </tbody>
                </table>

              </div>
             
            </div>
          </div>
         
        </div>
      </div>



       <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Document Data :</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <div class="col-md-5">
                  <div class="form-group">
                    <label>Document Attached :</label>
                    
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Justification :</label>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-2">
                  <div class="form-group">
                    <label class="containerx">Drawing
                      <input type="checkbox" name='drawing' value='1' <?php if($mr_list['drawing'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_drawing" id="justification_drawing" placeholder="----" value='<?php echo $mr_list['justification_drawing']; ?>'>
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                      <a target="_blank" href='<?php echo base_url(); ?>/upload/mr/drawing/<?php echo $mr_list['drawing_attach']; ?>'><?php echo $mr_list['drawing_attach']; ?></a>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-md-2">
                  <div class="form-group">
                    <label class="containerx">Term Of Reference
                      <input type="checkbox" name='term_of_reference' value='1' <?php if($mr_list['term_of_reference'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_of_reference" id="justification_of_reference" placeholder="----" value='<?php echo $mr_list['justification_of_reference']; ?>'>
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                      <a target="_blank" href='<?php echo base_url(); ?>/upload/mr/term/<?php echo $mr_list['term_attach']; ?>'><?php echo $mr_list['term_attach']; ?></a>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-md-2">
                  <div class="form-group">
                    <label class="containerx">Pictures
                      <input type="checkbox" name='picture'  value='1' <?php if($mr_list['picture'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_picture" id="justification_picture" placeholder="----" value='<?php echo $mr_list['justification_picture']; ?>'>
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                      <a target="_blank" href='<?php echo base_url(); ?>/upload/mr/picture/<?php echo $mr_list['pic_attach']; ?>'><?php echo $mr_list['pic_attach']; ?></a>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-md-2">
                  <div class="form-group">
                    <label class="containerx">Email Correspondences
                      <input type="checkbox" name='email_correspondences'  value='1' <?php if($mr_list['email_correspondences'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_email" id="justification_email" placeholder="----" value='<?php echo $mr_list['justification_email']; ?>'>
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                      <a target="_blank" href='<?php echo base_url(); ?>/upload/mr/email/<?php echo $mr_list['email_attach']; ?>'><?php echo $mr_list['email_attach']; ?></a>
                  </div>
                </div> 

              </div>
              
                           
            </div>
          </div>
        </div>
      </div>

      

      
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="row align-items-center">
          <div class="col-md">
          <h6 class="pb-2 mb-0">Approval</h6>
          </div>
          
          </div>
         <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
         

              <div class="row">
                <div class="text-right mt-12">
                   <?php if($mr_list["status"] != '3'){ ?>

                        <?php if($read_permission[25] == 1){ ?>

                        <button type="submit" name='submitBtn' id='approve' value='approve' class="btn btn-success" title="Approve"><i class="fa fa-check"></i> Approved</button>

                        <button type="submit" name='submitBtn' id='rejected' value='reject' class="btn btn-danger " title="Reject"><i class="fas fa-times-circle"></i> Reject</button>

                        <?php } ?>

                    <?php } ?>

                    <?php if($mr_list["status"] == '3'){


                       print_r($mr_list['manager_on_duty']);

                     ?>




                      
                      <table class="table" width="100%">
                      <tr>
                          <td> <b>USER</b> </td>
                          <td> <b>MANAGER ON DUTY</b> </td>
                          <td> <b>EXPORT</b> </td>    
                      </tr>
                      <tr>
                          <td>            
                            <img src="data:image/png;base64, <?php echo $get_user[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' /><br/><?php echo $get_user[0]["full_name"]; ?>
                          </td>
                          <td>            
                            <img src="data:image/png;base64, <?php echo $get_manager[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' /><br/><?php echo $get_manager[0]["full_name"]; ?>
                          </td>
                          <td>
                            <a target='_blank' href="<?php echo base_url();?>mr/mr_pdf_format/<?php echo strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~'); ?>" class="btn btn-danger " title="Submit"><i class="fas fa-file-pdf"></i> PDF</a>
                          </td>
                      </tr>
                      <tr>
                          <td> Date : <?php echo date("Y-m-d",strtotime($mr_list["created_date"])) ?></td>
                          <td> Date : <?php echo date("Y-m-d",strtotime($mr_list["manager_app_date"])) ?></td>
                          <td>&nbsp;</td>
                      </tr>
                      </table>
                         

                    <?php } ?>

                </div>
              </div>
            <!-- <div class="text-right mt-3">
              <button type="submit" name='submitBtn' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fas fa-plus"></i> Submit</button>
              <a href="<?php //echo base_url();?>mr/mr_list/open" class="btn btn-secondary " title="Submit"><i class="fas fa-times-circlee"></i> Cancel</a>
            </div> -->
             </div>
          </div>
        </div>
      </div>






    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepickerx').datepicker({
    format: 'yyyy-mm-dd',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).datepicker("setDate", new Date());

function delete_element_material(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_material_process(count);

      }
    })
  }

  function delete_element_material_process(count){
    $('#tr_element_material_' + count).remove();
  }

  function delete_element_so(count){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_so_process(count);

      }
    })
  }

  function delete_element_so_process(count){
    $('#tr_element_so_' + count).remove();
  }

  function delete_element_quotation(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_quotation_process(count);

      }
    })
  }

  function delete_element_quotation_process(count){
    $('#tr_element_quotation_' + count).remove();
  }

 
</script>

<script>
  $(document).ready(function(){
    fnc();
  });

  var fnc=function(input){
    
    var budget_cat = $('#posting_budget :selected').val();
    var budget_type = $('#budget_type :selected').val();

    $.ajax({
        url: "<?php echo base_url();?>mr/budget_check/",
        type: "post",
        data: {
          'budget_cat': budget_cat,
          'budget_type': budget_type
        },
        success: function(data) {
          if(data.includes("Error")){
            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $('input[name="date"]').attr('disabled', true);
            $('input[name="request_sche"]').attr('disabled', true);
            $('input[name="request_type"]').attr('disabled', true);
            $('input[name="request_checklist"]').attr('disabled', true);
            $('input[name="project"]').attr('disabled', true);  
            
            $('input[name="drawing"]').attr('disabled', true);
            $('input[name="term_of_reference"]').attr('disabled', true);
            $('input[name="picture"]').attr('disabled', true);
            $('input[name="email_correspondences"]').attr('disabled', true);          

          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $('input[name="date"]').removeAttr('disabled');
            $('select[name="request_sche"]').removeAttr('disabled');
            $('select[name="request_type"]').removeAttr('disabled');
            $('select[name="request_checklist"]').removeAttr('disabled');
            $('input[name="project"]').removeAttr('disabled');

            $('input[name="drawing"]').removeAttr('disabled');
            $('input[name="term_of_reference"]').removeAttr('disabled');
            $('input[name="picture"]').removeAttr('disabled');
            $('input[name="email_correspondences"]').removeAttr('disabled');
            

          }
          if (!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }
        }
    });
   

    $(".budget_amount").load("<?php echo base_url(); ?>mr/budget_status/budget_amount/"+budget_cat+"/"+budget_type);
    $(".transfered_amount").load("<?php echo base_url(); ?>mr/budget_status/transfered_amount/"+budget_cat+"/"+budget_type);
    $(".utilized_amount").load("<?php echo base_url(); ?>mr/budget_status/utilized_amount/"+budget_cat+"/"+budget_type);
    $(".balance_amount").load("<?php echo base_url(); ?>mr/budget_status/balance_amount/"+budget_cat+"/"+budget_type);
    $(".balance_amount_cal").load("<?php echo base_url(); ?>mr/budget_status/balance_amount_cal/"+budget_cat+"/"+budget_type);
                             
    //setTimeout(fnc, 10000);
  };


  function unique_no_func(nox){
    
    $("input[name='part_number["+nox+"]']").autocomplete({

      source: function(request,response){
        $.post('<?php echo base_url(); ?>mr/uniqno_autocomplete',{term: request.term }, response, 'json');
      },
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });

  }

  function change_overalcur(nox){
     var current_cur = $('#currency'+nox+' :selected').text();

      $("input[name='overal_cur']").val(current_cur);

  }


  function calculate_amount(nox,input){
    

    var qty          =  $("input[name='req_qty["+nox+"]']").val();
    var priceperunit =  $(input).val();

    var total_amountx = Number(qty) * (priceperunit);

    $("input[name='total_amount["+nox+"]']").val(total_amountx);


    if(nox > 0){

    var totalx = 0;
    for (var i = 1; i < nox; i++) {

      totalx += Number($("input[name='total_amount["+i+"]']").val());
      
    } 

    var overal_amount = totalx + total_amountx;

    } else {

     var totalx = total_amountx;

     var overal_amount = totalx;

    }

    

    $("input[name='overal_amount']").val(overal_amount);

    var total_overall_amount = overal_amount;

    var total_balance_amount = $('#balance_amount_cal').text();

 
    if(Number(total_balance_amount) < total_overall_amount){

     $("input[name='total_amount["+nox+"]']").val(0);
     $("input[name='expected_cost["+nox+"]']").val(0);
      $("input[name='overal_amount']").val(0);
            
      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'Insuficient Budget Balance Amount..',
      });

      return false;
    }


  }

 
  function checkunique(input, num) {

    var text             = $(input).val();
    var empty_val        = "-";

      $.ajax({

        url: "<?php echo base_url(); ?>mr/unique_no_check",
        type: "post",
        data: {
          'part_number': text,
        },

        success: function(data) {
          
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='part_number"+i+"']").val() == text){
                data = 'Error : Duplicate Material Code on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $("textarea[name='tec_spec["+num+"]']").val(empty_val);

                                 
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $("textarea[name='tec_spec["+num+"]']").val(res[0]);

            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  }

</script>