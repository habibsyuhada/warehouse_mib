<?php 
  $mr_list            = $mr_list[0];
  //$mr_list_detail     = $mr_list_detail[0];
  //$mr_list_quo        = $mr_list_quo[0];
  $mr_list_tabulation = $mr_list_tabulation[0];

  $total_mr_detail = sizeof($mr_list_detail) + 1;
  $total_mr_quo    = sizeof($mr_list_quo) + 1;

?>
<script type='text/javascript'>

      

<?php for($x=1;$x<=sizeof($mr_list_detail);$x++){ ?>

 function checkunique<?php echo $x; ?>(input, num) {


    var text             = input;
    var empty_val        = "-";

      $.ajax({

        url: "<?php echo base_url(); ?>mr/unique_no_check",
        type: "post",
        data: {
          'part_number': text,
        },

        success: function(data) {
          
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='part_number"+i+"']").val() == text){
                data = 'Error : Duplicate Material Code on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $("input[name='description["+num+"]']").val(empty_val);

                                 
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $("input[name='description["+num+"]']").val(res[0]);

            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  }    
<?php } ?>
  var count_element = <?php echo $total_mr_detail; ?>;

  //console.log(count_element);

  function add_list_material(){

    var html_element =  '<tr id="tr_element_material_' + count_element + '">' +
                          '<td><input type="text" id="part_number'+count_element+'" onfocus="unique_no_func(' + count_element + ');" name="part_number[' + count_element + ']" class="form-control" onblur="checkunique(this,' + count_element + ');"></td>' +  
                          '<td><input type="text" id="description'+count_element+'" name="description[' + count_element + ']" class="form-control" readonly></td>' +  
                          '<td><input type="text" id="req_qty'+count_element+'" name="req_qty[' + count_element + ']" class="form-control"></td>' + 
                          '<td><select id="uom'+count_element+'" name="uom[' + count_element + ']" class="form-control" required><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                          '<td><input type="number" id="expected_cost'+count_element+'" name="expected_cost[' + count_element + ']" class="form-control" oninput="calculate_amount(' + count_element + ',this);"></td>' + 

                          '<td><input type="number" id="total_amount'+count_element+'" name="total_amount[' + count_element + ']" class="form-control" readonly></td>' +

                          '<td><select id="currency'+count_element+'" name="currency[' + count_element + ']" onchange="change_overalcur(' + count_element + ');" class="form-control" required><?php foreach ($currency_list as $value) { echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; } ?></select> </td>' +

                          '<td><input type="text" id="remarks'+count_element+'" name="remarks[' + count_element + ']" class="form-control"></td>' +  
                          
                        '</tr>';

    $('#table_material').append(html_element);

    count_element++;
  }

  var count_elementx = <?php echo $total_mr_quo; ?>;

  function add_list_quotation(){

    var html_element =  '<tr id="tr_element_quotation_' + count_elementx + '">' +
                          '<td><select id="vendor'+count_elementx+'" name="vendor[' + count_elementx + ']" class="form-control" required><option value="">---</option><?php foreach ($vendor_list as $value) { echo "<option value=".$value["id_vendor"].">".$value["name"]."</option>"; } ?></select> </td>' +
                          
                          '<td><input type="text" id="total_amount_quo'+count_elementx+'" name="total_amount_quo[' + count_elementx + ']" class="form-control" required></td>' +

                          '<td><select id="currency_quo'+count_elementx+'" name="currency_quo[' + count_elementx + ']" class="form-control" required><?php foreach ($currency_list as $value) { echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; } ?></select> </td>' +

                          '<td>'+
                          '<input type="hidden" name="id_attachment[' + count_elementx + ']" value="' + count_elementx + '">' +
                          '<input type="file" name="file_att_' + count_elementx + '"  size = "10000">'+
                          ' </td>' + 
                         

                        '</tr>';

    $('#table_quotation').append(html_element);

    count_elementx++;
  }


</script>


<div id="content" class="container-fluid" style="overflow: auto;">

     <form method="POST" action="<?php echo base_url();?>mr/process_update_mr" enctype="multipart/form-data">
      <div class="row">
      <?php if($mr_list["status"] != '3'){ ?>

        <?php if($read_permission[21] == '1' OR $read_permission[47] == '1' ){ ?>

       <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <a href='<?php echo base_url()."mr/mr_update/".strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~'); ?>' class='btn btn-warning'> <b><i class="far fa-edit"></i> Update</b></a>
        </div>
      </div>

      <?php } ?>
      <?php } ?>

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                    <label>Material Requitision No.</label>
                    <input type="hidden" name='mr_number'  value='<?php echo $mr_list['mr_number']; ?>' readonly>
                    <input type="text" class="form-control" name="form_no" id="form_no" placeholder="Type Material Requitision No." value='<?php echo $mr_list['form_number']; ?>' required readonly>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label>Material Requitision Date</label>
                    <input type="text" class="form-control datepickerx" name="created_date" id="created_date"  value='<?php echo $mr_list['created_date']; ?>' disabled>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                    <label>Budget Category</label>
                    <select id='budget' name='budget' class="form-control" onchange="fnc();">
                      <?php foreach ($budget as $value) { ?>
                         <option value='<?php echo $value['id']; ?>' <?php if($mr_list['budget_cat_id'] == $value['id']){ echo "selected"; } else { echo "disabled";} ?>><?php echo $value['category_name']; ?> </option>
                      <?php  } ?>
                    </select>         
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label>Department</label>
                    <input type="hidden" class="form-control" name="dept_id" id="dept_id" value='<?php echo $mr_list["budget_dept_id"]; ?>' readonly>
                    <input type="text" class="form-control" name="dept_id_show" id="dept_id_show" value='<?php echo $dept[$mr_list["budget_dept_id"]]; ?>' readonly>
                  </div>
                </div> 

              </div>
             
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">

        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Budget Status</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid text-center">

              <div class="row">

                <div class="col-md">

                  <div class="form-group">
                    <label><b>Budgeted Amount</b></label></br>
                    <h1><div class="badge badge-pill badge-primary"><div class='budget_amount'></div></div></h1>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label><b>Transferred Amount</b></label>
                     <h1><div class="badge badge-pill badge-success"><div class='transfered_amount'></div></div></h1>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label><b>Utilized Amount</b></label>
                     <h1><div class="badge badge-pill badge-warning"><div class='utilized_amount'></div></div></h1>
                         
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label><b>Balance Amount</b></label>
                      <h1><div class="badge badge-pill badge-info">
                        <div id='balance_amount' class='balance_amount'></div>
                        
                      </div></h1> 
                      <span style='visibility: hidden;' id='balance_amount_cal' class='balance_amount_cal'></span>
                  </div>
                </div> 

              </div>

              
             
            </div>
          </div>
        
        </div>
      </div>


      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="row align-items-center">
          <div class="col-md">
          <h6 class="pb-2 mb-0">List of Material Requisition</h6>
          </div>
          <div class="col-md text-right">
           
          </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <table id="table_material" class="table table-hover text-center">
                  <thead class="bg-green-smoe text-white">
                    <tr>
                      <th>Material Code</th>
                      <th>Description</th>
                      <th>Req Qty</th>                  
                      <th>UOM</th>
                      <th>Expected Cost / Unit</th>                                                                
                      <th>Total Amount</th>                                                                
                      <th>Currency</th>                                                              
                      <th>Remarks</th>                                                               
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; foreach ($mr_list_detail as $detail) { ?>
                    <tr id="tr_element_material_<?php echo $no; ?>">

                          <td>
                            <input type="text" id="part_number<?php echo $no; ?>" onfocus="unique_no_func(<?php echo $no; ?>);" name="part_number[<?php echo $no; ?>]" class="form-control" onblur="checkunique(this,<?php echo $no; ?>);" value='<?php echo $detail['code_material'] ?>' readonly>

                            <script> checkunique<?php echo $no;  ?>("<?php echo $detail['code_material'] ?>", <?php echo $no;  ?>);</script>

                          </td>  

                          <td><input type="text" id="description<?php echo $no; ?>" name="description[<?php echo $no; ?>]" class="form-control" readonly></td> 

                          <td><input type="text" id="req_qty<?php echo $no; ?>" name="req_qty[<?php echo $no; ?>]" class="form-control" value='<?php echo $detail['qty'] ?>' readonly></td> 

                          <td>
                            <select id="uom'<?php echo $no; ?>'" name="uom[<?php echo $no; ?>]" class="form-control" required>
                              <?php 
                                foreach ($uom_list as $value) { 
                                  ?>
                                 <option value='<?php echo $value["id_uom"]?>' <?php if($detail["uom"] == $value['id_uom']){ echo "selected"; } else {echo "disabled"; } ?>>
                                  <?php echo $value["uom"]; ?>
                                 </option>"; 
                               <?php
                                } 
                              ?>
                              
                            </select> </td>

                          <td>
                            <input type="number" id="expected_cost'<?php echo $no; ?>'" name="expected_cost[<?php echo $no; ?>]" class="form-control" oninput="calculate_amount(<?php echo $no; ?>,this);" value='<?php echo $detail['expected_cost'] ?>' readonly>

                          </td> 

                          <td><input type="number" id="total_amount<?php echo $no; ?>" name="total_amount[<?php echo $no; ?>]"  value='<?php echo $detail['total_amount'] ?>' class="form-control" readonly></td>

                          <td>
                            <select id="currency<?php echo $no; ?>" name="currency[<?php echo $no; ?>]" onchange="change_overalcur(<?php echo $no; ?>);" class="form-control" required>
                              <?php 
                              foreach ($currency_list as $value) { 
                                ?>
                               <option value="<?php echo $value["id_cur"]; ?>" <?php if($detail["currency"] == $value['id_cur']){ echo "selected"; } else { echo "disabled"; } ?>>
                               <?php echo $value["currency"]; ?>
                                </option>
                              <?php
                                } 
                              ?>
                                  
                            </select>
                          </td>

                          <td><input type="text" id="remarks<?php echo $no; ?>" name="remarks[<?php echo $no; ?>]" class="form-control" value='<?php echo $detail['remarks'] ?>' readonly></td>  

                         
                        </tr>
                      <?php $no++; } ?>

                  </tbody>
                  <tfoot class="bg-green-smoe text-white">
                      <th colspan='5'><h5>Overall Amount</h5></th>
                                                                                  
                      <th><h5><input type='number' name='overal_amount' value='0' class="form-control" readonly=""></h5></th>                                                                
                      <th colspan="3"><h5><input type='text' name='overal_cur' value='<?php echo $cur[$mr_list_detail[0]['currency']] ?>' class="form-control" readonly=""></h5></th> 
                  </tfoot>
                </table>

              </div>
             
            </div>
          </div>
         
        </div>
      </div>


      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="row align-items-center">
          <div class="col-md">
          <h6 class="pb-2 mb-0">List of Quotation</h6>
          </div>
          <div class="col-md text-right">
           
          </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <table id='table_quotation' class="table table-hover text-center">
                  <thead class="bg-green-smoe text-white">
                    <tr>
                      <th>Vendor</th>
                      <th>Total Amount</th>
                      <th>Currency</th>
                      <th>Attachment</th>                  
                                                                             
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; foreach ($mr_list_quo as $detail) { ?>
                    <tr id="tr_element_quotation_<?php echo $nox; ?>">

                          <td>

                            <select id="vendor<?php echo $nox; ?>" name="vendor[<?php echo $nox; ?>]" class="form-control" required>

                            <?php 
                              foreach ($vendor_list as $value) { 
                            ?>
                             <option value="<?php echo $value["id_vendor"]; ?>" 
                               <?php if($detail["id_vendor"] == $value['id_vendor']){ echo "selected"; } else { echo "disabled"; } ?>><?php echo $value["name"]; ?></option> 
                            <?php
                              } 
                            ?>                                
                            </select> 

                          </td>
                          
                          <td><input type="text" id="total_amount_quo<?php echo $nox; ?>" name="total_amount_quo[<?php echo $nox; ?>]" class="form-control" required value='<?php echo $detail['amount'] ?>' readonly></td>

                          <td>
                            <select id="currency_quo<?php echo $nox; ?>" name="currency_quo[<?php echo $nox; ?>]" class="form-control" required>
                              <?php 
                                foreach ($currency_list as $value) {
                              ?>
                              <option value="<?php echo $value["id_cur"]; ?>"  <?php if($detail["currency"] == $value['id_cur']){ echo "selected"; } else { echo "disabled"; } ?>>
                                <?php echo $value["currency"]; ?>
                              </option>

                              <?php
                                } 
                              ?>                              
                            </select>
                          </td>

                          <td>
                          <a target='_blank' href='<?php echo base_url();?>upload/mr/quotation/<?php echo $detail["attachment"];?>'><?php echo $detail["attachment"];?></a>
                          </td>

                         

                        </tr>
                      <?php $no++; } ?>

                  </tbody>
                </table>

              </div>
             
            </div>
          </div>
           
        </div>
      </div>

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="row align-items-center">
          <div class="col-md">
          <h6 class="pb-2 mb-0">Tabulation</h6>
          </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <table width="90%">
                <tr>
                  <td width="50%">
                    <div class="row">
                      <div class="col-md-6">
                        <?php if(!isset($mr_list_tabulation["filename"])){ ?>
                          <h6><div class="badge badge-danger">No File Chosen</div></h6>
                        <?php } else { ?>
                         <h6><a href="<?php echo base_url(); ?>upload/mr/tabulation/<?php echo $mr_list_tabulation["filename"]; ?>" target='_blank'><div class="badge badge-danger"><?php echo $mr_list_tabulation["filename"]; ?></div></a></h6>
                        <?php }  ?>
                      </div>
                    </div>
                  </td>
                  <td width="50%">
                     <div class="row">
                      <div class="col-md-6">
                        <div class="input-group">
                        
                       
                        </div>
                      </div>
                     </div>
                  </td>
                </tr>
              </table>

              </div>
             
            </div>
          </div>
            <div class="text-right mt-3">

              <?php if($mr_list["status"] != '3'){ ?>

           <!-- <button type="submit" name='submitBtn' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fas fa-plus"></i> Submit</button> -->

            <?php if(isset($mr_list_tabulation["filename"])){ ?>

              <?php if($read_permission[25] == 1){ ?>

              <button type="submit" name='submitBtn' id='approve' value='approve' class="btn btn-success" title="Approve"><i class="fa fa-check"></i> Approved</button>

              <button type="submit" name='submitBtn' id='rejected' value='reject' class="btn btn-danger " title="Reject"><i class="fas fa-times-circle"></i> Reject</button>

            <?php } ?>

            <?php } ?>

            <a href="<?php echo base_url();?>mr/mr_list" class="btn btn-secondary " title="Submit"><i class="fas fa-times-circle"></i> Cancel</a>

          <?php } ?>
          </div>
        </div>
      </div>


    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">

  $(window).on('ready', function() { 
        //calculate_amount_loop();
        //add_list_material();
        //add_list_quotation();
      });
  
  $('.datepickerx').datepicker({
    format: 'yyyy-mm-dd',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).datepicker("setDate", new Date());

function delete_element_material(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_material_process(count);

      }
    })
  }

  function delete_element_material_process(count){
    $('#tr_element_material_' + count).remove();
  }

  function delete_element_quotation(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_quotation_process(count);

      }
    })
  }

  function delete_element_quotation_process(count){
    $('#tr_element_quotation_' + count).remove();
  }

 
</script>

<script>
  $(document).ready(function(){
    fnc();
    calculate_amount_loop();
    
  });

  var fnc=function(){
    
    var budget_cat = $('#budget :selected').val();

    $(".budget_amount").load("<?php echo base_url(); ?>mr/budget_status/budget_amount/"+budget_cat);
    $(".transfered_amount").load("<?php echo base_url(); ?>mr/budget_status/transfered_amount/"+budget_cat);
    $(".utilized_amount").load("<?php echo base_url(); ?>mr/budget_status/utilized_amount/"+budget_cat);
    $(".balance_amount").load("<?php echo base_url(); ?>mr/budget_status/balance_amount/"+budget_cat);
    $(".balance_amount_cal").load("<?php echo base_url(); ?>mr/budget_status/balance_amount_cal/"+budget_cat);
                             
    setTimeout(fnc, 10000);
  };


  function unique_no_func(nox){
    
    $("input[name='part_number["+nox+"]']").autocomplete({

      source: function(request,response){
        $.post('<?php echo base_url(); ?>mr/uniqno_autocomplete',{term: request.term }, response, 'json');
      },
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });

  }

  function change_overalcur(nox){
     var current_cur = $('#currency'+nox+' :selected').text();

      $("input[name='overal_cur']").val(current_cur);

  }


  function calculate_amount_loop(){

    var totalx = 0;
    for (var i = 1; i <= <?php echo sizeof($mr_list_detail); ?>; i++) {
      totalx += Number($("input[name='total_amount["+i+"]']").val());

    } 

    var overal_amount = totalx;
   

    $("input[name='overal_amount']").val(overal_amount);




<?php if($mr_list["status"] != '3'){ ?>

    var total_overall_amount = overal_amount;

    var total_balance_amount = $('#balance_amount_cal').text();

    console.log(total_balance_amount+"-"+total_overall_amount);
 
    if(Number(total_balance_amount) < total_overall_amount){
    /*           
      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'Insuficient Budget Balance Amount..',
      });

      return false;
      */
    }

<?php } ?>


  }

  function calculate_amount(nox,input){

    var qty          =  $("input[name='req_qty["+nox+"]']").val();
    var priceperunit =  $(input).val();

    var total_amountx = Number(qty) * (priceperunit);

    $("input[name='total_amount["+nox+"]']").val(total_amountx);


    if(nox > 0){

    var totalx = 0;
    for (var i = 1; i < nox; i++) {

      totalx += Number($("input[name='total_amount["+i+"]']").val());
      
    } 

    var overal_amount = totalx + total_amountx;

    } else {

     var totalx = total_amountx;

     var overal_amount = totalx;

    }

    

    $("input[name='overal_amount']").val(overal_amount);



  }

 
  function checkunique(input, num) {

    var text             = $(input).val();
    var empty_val        = "-";

      $.ajax({

        url: "<?php echo base_url(); ?>mr/unique_no_check",
        type: "post",
        data: {
          'part_number': text,
        },

        success: function(data) {
          
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='part_number"+i+"']").val() == text){
                data = 'Error : Duplicate Material Code on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $("input[name='description["+num+"]']").val(empty_val);

                                 
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $("input[name='description["+num+"]']").val(res[0]);

            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  }

</script>