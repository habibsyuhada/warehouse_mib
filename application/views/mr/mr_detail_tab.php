<?php 
  $mr_list            = $mr_list[0];
  //$mr_list_detail     = $mr_list_detail[0];
  //$mr_list_quo        = $mr_list_quo[0];
  $mr_list_tabulation = $mr_list_tabulation[0];

 
?>
<style>
/* The container */
.containerx {
  font-family: arial;
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.containerx input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmarkx {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.containerx:hover input ~ .checkmarkx {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.containerx input:checked ~ .checkmarkx {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmarkx:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.containerx input:checked ~ .checkmarkx:after {
  display: block;
}

/* Style the checkmark/indicator */
.containerx .checkmarkx:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

.red_title{
  color: red;
  font-weight: bold;
  font-size: 9px;
  font-style: italic;
}
.red_detail {
  color: red;
  font-style: italic;
  font-size: 10px;
  font-style: italic;
}
</style>

<script type='text/javascript'>
  //$(window).on('load', function() { 
   // add_list_material();
    //  });

  var count_element = 1;

  function add_list_material(){

    var html_element =  '<tr id="tr_element_material_' + count_element + '">' +

                          '<td>' + count_element + '<input type="hidden" id="nos'+count_element+'" name="nos[' + count_element + ']" value="' + count_element + '" class="form-control" readonly min="0"></td>' + 

                          '<td><textarea class="form-control" id="tec_spec'+count_element+'" name="tec_spec[' + count_element + ']"  required></textarea></td>' + 

                          '<td><input type="number" id="qty_req'+count_element+'" name="qty_req[' + count_element + ']" class="form-control" min="0"></td>' +  
                          
                          '<td><select id="uom_req'+count_element+'" name="uom_req[' + count_element + ']" class="form-control" required><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                          '<td><input type="number" id="qty_stock'+count_element+'" name="qty_stock[' + count_element + ']" class="form-control"  min="0" required></td>' +  
                          
                          '<td><select id="uom_stock'+count_element+'" name="uom_stock[' + count_element + ']" class="form-control" required><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                          '<td><input type="number" id="qty_order'+count_element+'" name="qty_order[' + count_element + ']" class="form-control"  min="0" required></td>' +  
                          
                          '<td><select id="uom_order'+count_element+'" name="uom_order[' + count_element + ']" class="form-control" required><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                          '<td><select id="currency'+count_element+'" name="currency[' + count_element + ']" onchange="change_overalcur(' + count_element + ');" class="form-control" required><?php foreach ($currency_list as $value) { echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; } ?></select> </td>' +

                          '<td><input type="number" id="estimate_price'+count_element+'" name="estimate_price[' + count_element + ']" class="form-control" required></td>' + 

                          '<td><button class="btn btn-danger" type="button" onclick="delete_element_material(' + count_element + ')"><i class="fas fa-trash-alt"></i></button></td>' +   

                        '</tr>';

    $('#table_material').append(html_element);

    count_element++;
  }



</script>
<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>mr/mr_add_process" enctype="multipart/form-data">
     <input type="hidden" class="form-control" name="dept_id" id="dept_id" value='<?php echo $department[0]['id_department']; ?>'>
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <div class="col-md">
                  <div class="form-group">

                    <label class="containerx">Unschedule 
                      <input type="checkbox" name='unschedule' value='1' <?php if($mr_list['unschedule'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>

                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">Schedule
                      <input type="checkbox" name='schedule'  value='1' <?php if($mr_list['schedule'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md">
                  <div class="form-group">
                    <label class="form-control"> Date :</label>
                    
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md">
                  <div class="form-group">

                    <label class="containerx">Service Request
                      <input type="checkbox" name='service_request'  value='1' <?php if($mr_list['service_request'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>

                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">Material Request
                      <input type="checkbox" name='material_request'  value='1' <?php if($mr_list['material_request'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md">
                  <div class="form-group">
                    <input name='mr_date' class='form-control datepickerx' value='<?php echo $mr_list['created_date']; ?>'>
                  </div>
                </div> 

              </div>



              <div class="row">

                <div class="col-md-3">
                  <div class="form-group">
                   <label>Project Name :</label>
                   <input type="text" class="form-control" name="project" placeholder="Project Name" value='<?php echo $mr_list['project'] ?>'>                   
                  </div>
                </div>

              </div>

              <div class="row"> 

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>               

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">MEB1
                      <input type="checkbox" name='meb1' value='1' <?php if($mr_list['meb1'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">MEB2
                      <input type="checkbox" name='meb2' value='1' <?php if($mr_list['meb2'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">BOP
                      <input type="checkbox" name='meb_bop' value='1' <?php if($mr_list['meb_bop'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">CCPP
                      <input type="checkbox" name='meb_ccpp' value='1' <?php if($mr_list['meb_ccpp'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">CHILLER
                      <input type="checkbox" name='meb_ciller' value='1' <?php if($mr_list['meb_ciller'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

              </div>


              <div class="row">  

              <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>              

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">DEB1
                      <input type="checkbox" name='deb1' value='1' <?php if($mr_list['deb1'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">DEB2
                      <input type="checkbox" name='deb2' value='1' <?php if($mr_list['deb2'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">BOP
                      <input type="checkbox" name='deb_bop' value='1' <?php if($mr_list['deb_bop'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">CCPP
                      <input type="checkbox" name='deb_ccpp' value='1' <?php if($mr_list['deb_ccpp'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">CHILLER
                      <input type="checkbox" name='deb_ciller' value='1' <?php if($mr_list['deb_ciller'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

              </div>

              <div class="row"> 

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>               

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">TM2500
                      <input type="checkbox" name='tm2500' value='1' <?php if($mr_list['tm2500'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">BOP
                      <input type="checkbox" name='tm_bop' value='1' <?php if($mr_list['tm_bop'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>
                
                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

              </div>

              <div class="row"> 

              <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>               

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">GS
                      <input type="checkbox" name='gs' value='1' <?php if($mr_list['gs'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">HSE & SEC
                      <input type="checkbox" name='hsensec' value='1' <?php if($mr_list['hsensec'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">W/H
                      <input type="checkbox" name='wh' value='1' <?php if($mr_list['wh'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

              </div>



              <div class="row">



                <div class="col-md-3">
                  <div class="form-group">
                    <label>Posting Budget :</label>
                    <select id='posting_budget' name='posting_budget' class="form-control" onchange="fnc();" style="display: inline;">
                      <option value=''>~ Posting Budget ~</option>
                      <?php foreach ($budget as $value) { ?>
                         <option value='<?php echo $value['id']; ?>'  <?php if($mr_list['posting_budget'] == $value['id']){ echo "selected"; } ?>><?php echo $value['account_no']; ?> </option>
                      <?php  } ?>
                    </select>         
                  </div>
                </div>

              </div>

               <div class="row">

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>


                 <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">CAPEX
                      <input type="checkbox" name='capex' value='1' <?php if($mr_list['capex'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx">OPEX
                      <input type="checkbox" name='opex' value='1' <?php if($mr_list['opex'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>


              </div>

              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>


                 <div class="col-md">
                  <div class="form-group">
                    <label class="containerx red">NB
                      <input type="checkbox" name='nb' value='1' <?php if($mr_list['nb'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                     <div class="red_title text-left">
                      * Non Budget<br/>
                    </div>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx red">LB
                      <input type="checkbox" name='lb' value='1' <?php if($mr_list['lb'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                    <div class="red_title text-left">
                      * Limitation Budget<br/>
                    </div>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label class="containerx red">RB
                      <input type="checkbox" name='rb' value='1' <?php if($mr_list['rb'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                     <div class="red_title text-left">
                      * Replacement Budget<br/>
                    </div>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>


              </div>

              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>


                 <div class="col-md-5">
                  <div class="form-group">
                    <div class="red_detail text-left">
                     ( Should be fill in Business Case Form with expalanation from User & Approved by Head of Department )
                    </div>
                  </div>
                </div>

                

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                  </div>
                </div>


              </div>



                          
            </div>
          </div>
        </div>
      </div>

      


      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="row align-items-center">
          <div class="col-md">
          <h6 class="pb-2 mb-0">List of Material Requisition</h6>
          </div>
          <div class="col-md text-right">
            <div class="btn btn-primary" onclick='add_list_material();'><i class="fas fa-plus"></i> Material</div>
          </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <table id="table_material" class="table table-hover text-center">
                  <thead class="bg-green-smoe text-white">
                    <tr>
                      <th rowspan="2">No</th>
                      <?php if($mr_list["material_request"] == 1){ ?>
                      <th rowspan="2">Code Material</th>
                      <?php } ?>
                      <th rowspan="2">Tecnical Spesification</th>
                      <th colspan="2">Request*</th>                  
                      <th colspan="2">Stock*</th>
                      <th colspan="2">Order*</th>
                      <th rowspan="2">Currency*</th>
                      <th rowspan="2">Estimate Price*</th>
                      <th rowspan="2">Action</th>         
                    </tr>
                    <tr>
                      <th>Qty</th>
                      <th>Unit</th>
                      <th>Qty</th>
                      <th>Unit</th>
                      <th>Qty</th>
                      <th>Unit</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php $no = 1; foreach ($mr_list_detail as $detail) { ?>

                        <tr id="tr_element_material_<?php echo $no; ?>">

                          <td>
                              <?php echo $no; ?>
                              <input type="hidden" id="nos<?php echo $no; ?>" name="nos[<?php echo $no; ?>]" value="<?php echo $no; ?>" class="form-control" readonly min="0">
                          </td>

                          <?php if($mr_list["material_request"] == 1){ ?>
                          <td>
                              <input type="text" id="code_material<?php echo $no; ?>" name="code_material[<?php echo $no; ?>]" value="<?php echo $detail['code_material']; ?>" class="form-control" readonly min="0">
                          </td>  
                          <?php } ?>

                          <td>
                            <textarea class="form-control" id="tec_spec<?php echo $no; ?>" name="tec_spec[<?php echo $no; ?>]"  required><?php echo $detail['tec_spec']; ?></textarea>
                          </td> 

                          <td><input type="number" id="qty_req<?php echo $no; ?>" name="qty_req[<?php echo $no; ?>]" class="form-control" min="0" value='<?php echo $detail['qty_req']; ?>'></td>  
                          
                          <td>
                            <select id="uom_req<?php echo $no; ?>" name="uom_req[<?php echo $no; ?>]" class="form-control" required>
                              <?php foreach ($uom_list as $value) { ?>
                                <option value="<?php echo $value['id_uom']; ?>" <?php if($value['id_uom'] == $detail['uom_req']){ echo "selected"; } ?>>
                                  <?php echo $value["uom"]; ?>
                                </option>
                              <?php } ?>                              
                            </select>
                          </td>

                          <td><input type="number" id="qty_stock<?php echo $no; ?>" name="qty_stock[<?php echo $no; ?>]" class="form-control"  min="0" value='<?php echo $detail['qty_stock']; ?>' required value='<?php echo $detail['qty_stock']; ?>'></td>  
                          
                          <td>
                            <select id="uom_stock<?php echo $no; ?>" name="uom_stock[<?php echo $no; ?>]" class="form-control" required>
                              <?php foreach ($uom_list as $value) { ?>
                                <option value="<?php echo $value['id_uom']; ?>" <?php if($value['id_uom'] == $detail['uom_stock']){ echo "selected"; } ?>>
                                  <?php echo $value["uom"]; ?>
                                </option>
                              <?php } ?>                              
                            </select>
                          </td>

                          <td>
                            <input type="number" id="qty_order<?php echo $no; ?>" name="qty_order[<?php echo $no; ?>]" class="form-control"  min="0" required value='<?php echo $detail['qty_order']; ?>'>
                          </td>  
                          
                          <td>                           
                            <select id="uom_order<?php echo $no; ?>" name="uom_order[<?php echo $no; ?>]" class="form-control" required>
                              <?php foreach ($uom_list as $value) { ?>
                                <option value="<?php echo $value['id_uom']; ?>" <?php if($value['id_uom'] == $detail['uom_order']){ echo "selected"; } ?>>
                                  <?php echo $value["uom"]; ?>
                                </option>
                              <?php } ?>                              
                            </select>
                          </td>

                          <td>
                            <select id="currency<?php echo $no; ?>" name="currency[<?php echo $no; ?>]" class="form-control" required>
                              <?php foreach ($currency_list as $value) { ?>
                                <option value="<?php echo $value['id_cur']; ?>" <?php if($value['id_cur'] == $detail['currency']){ echo "selected"; } ?>>
                                  <?php echo $value["currency"]; ?>
                                </option>
                              <?php } ?>                              
                            </select>
                          </td>

                          <td><input type="number" id="estimate_price<?php echo $no; ?>" name="estimate_price[<?php echo $no; ?>]" class="form-control" required value='<?php echo $detail['estimate_price']; ?>'></td> 

                          <td><button class="btn btn-danger" type="button" onclick="delete_element_material(<?php echo $no; ?>)"><i class="fas fa-trash-alt"></i></button></td>   

                        </tr>

                     <?php $no++; } ?>
                  </tbody>
                </table>

              </div>
             
            </div>
          </div>
         
        </div>
      </div>



       <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Document Data :</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <div class="col-md-5">
                  <div class="form-group">
                    <label>Document Attached :</label>
                    
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Justification :</label>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-5">
                  <div class="form-group">
                    <label class="containerx">Drawing
                      <input type="checkbox" name='drawing' value='1' <?php if($mr_list['drawing'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_drawing" id="justification_drawing" placeholder="----" value='<?php echo $mr_list['justification_drawing']; ?>'>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-5">
                  <div class="form-group">
                    <label class="containerx">Term Of Reference
                      <input type="checkbox" name='term_of_reference' value='1' <?php if($mr_list['term_of_reference'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_of_reference" id="justification_of_reference" placeholder="----" value='<?php echo $mr_list['justification_of_reference']; ?>'>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-5">
                  <div class="form-group">
                    <label class="containerx">Pictures
                      <input type="checkbox" name='picture'  value='1' <?php if($mr_list['picture'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_picture" id="justification_picture" placeholder="----" value='<?php echo $mr_list['justification_picture']; ?>'>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-5">
                  <div class="form-group">
                    <label class="containerx">Email Correspondences
                      <input type="checkbox" name='email_correspondences'  value='1' <?php if($mr_list['email_correspondences'] == 1){ echo "checked"; } ?>>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_email" id="justification_email" placeholder="----" value='<?php echo $mr_list['justification_email']; ?>'>
                  </div>
                </div> 
                
              </div>

              <div class="row">

                <div class="col-md-12">
                 <div class="text-right ">

                    <a href="<?php echo base_url();?>mr/mr_pdf_format/<?php echo strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~'); ?>" class="btn btn-danger " title="Submit"><i class="fas fa-times-circlee"></i> PDF</a>

                    <a href="<?php echo base_url();?>mr/mr_list/open" class="btn btn-secondary " title="Submit"><i class="fas fa-times-circlee"></i> Cancel</a>
                    
                </div>
                </div> 

              </div>

                           
            </div>
          </div>
        </div>
      </div>



    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepickerx').datepicker({
    format: 'yyyy-mm-dd',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).datepicker("setDate", new Date());

function delete_element_material(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_material_process(count);

      }
    })
  }

  function delete_element_material_process(count){
    $('#tr_element_material_' + count).remove();
  }

  function delete_element_quotation(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_quotation_process(count);

      }
    })
  }

  function delete_element_quotation_process(count){
    $('#tr_element_quotation_' + count).remove();
  }

 
</script>

<script>
  $(document).ready(function(){
    fnc();
  });

  var fnc=function(){
    
    var budget_cat = $('#budget :selected').val();

    $(".budget_amount").load("<?php echo base_url(); ?>mr/budget_status/budget_amount/"+budget_cat);
    $(".transfered_amount").load("<?php echo base_url(); ?>mr/budget_status/transfered_amount/"+budget_cat);
    $(".utilized_amount").load("<?php echo base_url(); ?>mr/budget_status/utilized_amount/"+budget_cat);
    $(".balance_amount").load("<?php echo base_url(); ?>mr/budget_status/balance_amount/"+budget_cat);
    $(".balance_amount_cal").load("<?php echo base_url(); ?>mr/budget_status/balance_amount_cal/"+budget_cat);
                             
    setTimeout(fnc, 10000);
  };


  function unique_no_func(nox){
    
    $("input[name='part_number["+nox+"]']").autocomplete({

      source: function(request,response){
        $.post('<?php echo base_url(); ?>mr/uniqno_autocomplete',{term: request.term }, response, 'json');
      },
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });

  }

  function change_overalcur(nox){
     var current_cur = $('#currency'+nox+' :selected').text();

      $("input[name='overal_cur']").val(current_cur);

  }


  function calculate_amount(nox,input){
    

    var qty          =  $("input[name='req_qty["+nox+"]']").val();
    var priceperunit =  $(input).val();

    var total_amountx = Number(qty) * (priceperunit);

    $("input[name='total_amount["+nox+"]']").val(total_amountx);


    if(nox > 0){

    var totalx = 0;
    for (var i = 1; i < nox; i++) {

      totalx += Number($("input[name='total_amount["+i+"]']").val());
      
    } 

    var overal_amount = totalx + total_amountx;

    } else {

     var totalx = total_amountx;

     var overal_amount = totalx;

    }

    

    $("input[name='overal_amount']").val(overal_amount);

    var total_overall_amount = overal_amount;

    var total_balance_amount = $('#balance_amount_cal').text();

 
    if(Number(total_balance_amount) < total_overall_amount){

     $("input[name='total_amount["+nox+"]']").val(0);
     $("input[name='expected_cost["+nox+"]']").val(0);
      $("input[name='overal_amount']").val(0);
            
      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'Insuficient Budget Balance Amount..',
      });

      return false;
    }


  }

 
  function checkunique(input, num) {

    var text             = $(input).val();
    var empty_val        = "-";

      $.ajax({

        url: "<?php echo base_url(); ?>mr/unique_no_check",
        type: "post",
        data: {
          'part_number': text,
        },

        success: function(data) {
          
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='part_number"+i+"']").val() == text){
                data = 'Error : Duplicate Material Code on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $("input[name='description["+num+"]']").val(empty_val);

                                 
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $("input[name='description["+num+"]']").val(res[0]);

            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  }

</script>