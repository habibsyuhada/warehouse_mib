<?php 

  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Report-".date('YmdHis').".xls");
  header("Pragma: no-cache");
  header("Expires: 0");

  $mr_list       = $mr_list[0];
  $year          = date("Y",strtotime($mr_list["created_date"]));
  $created_by    = (isset($user_data[$mr_list['created_by']]) ? $user_data[$mr_list['created_by']] : '-');
  $created_date  = $mr_list["created_date"];
  $module        = (isset($module[$mr_list['module']]) ? $module[$mr_list['module']] : '-');
  $data_priority = (isset($priority[$mr_list['priority']]) ? $priority[$mr_list['priority']] : '-');
  $project_name  = (isset($project_list[$mr_list['project_id']]) ? $project_list[$mr_list['project_id']] : '-');
  $project_code  = (isset($project_code[$mr_list['project_id']]) ? $project_code[$mr_list['project_id']] : '-');

?>


<!DOCTYPE html>
<html><head>
  <title>MR Number :  MR-<?php echo $year; ?>-<?php echo $mr_list['mr_number']; ?></title>
  
</head><body>  

  <table width='100%' border="1">
       <thead><tr>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">REFERENCE</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">DESCRIPTION</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">SIZE (MM)</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">UOM</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">QTY</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">CURRENCY</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">EXPECTED COST IN</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">TOTAL AMOUNT</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">REASON / PURPOSE</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">REMARKS</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">NSI NO.</th>
              </tr></thead>
        <tbody><?php $no=1; foreach ($mr_list_detail as $key) { ?><tr><td>MR-<?php echo $year; ?>-<?php echo $key['mr_number']; ?>-<?php echo $key['id']; ?></td><td><?php echo (isset($material[$key['catalog_id']]) ? $material[$key['catalog_id']] : '-'); ?></td><td><?php echo $key['size_mm']; ?></td><td><?php echo $key['uom']; ?></td><td><?php echo $key['qty']; ?></td><td><?php echo $key['currency']; ?></td><td><?php echo round($key['expected_cost_id'],2); ?></td><td><?php echo round($key['total_amount'],2); ?></td><td><?php echo $key['reason_purpose']; ?></td><td><?php echo $key['remarks']; ?></td><td><?php echo $key['nsi_no']; ?></td></tr><?php $no++;} ?></tbody>  
  </table>

</body></html>