<?php 

  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Report-".date('YmdHis').".xls");
  header("Pragma: no-cache");
  header("Expires: 0");

  $mr_list       = $mr_list[0];
  $year          = date("Y",strtotime($mr_list["created_date"]));
  $created_by    = (isset($user_data[$mr_list['created_by']]) ? $user_data[$mr_list['created_by']] : '-');
  $created_date  = $mr_list["created_date"];
  $module        = (isset($module[$mr_list['module']]) ? $module[$mr_list['module']] : '-');
  $data_priority = (isset($priority[$mr_list['priority']]) ? $priority[$mr_list['priority']] : '-');
  
?>

<!DOCTYPE html>
<html><head>
  <title>MR Number :  MR-<?php echo $year; ?>-<?php echo $mr_list['mr_number']; ?></title>  
</head><body>
  <table width="100%" align="center">
      <tr>
        <td>
          <table>
            <tr>
              <td style="width: 150px !important;">MR Number</td>
              <td style="width: 5px !important;">:</td>
              <td style="width: 300px !important;">MR-<?php echo $year; ?>-<?php echo $mr_list['mr_number']; ?></td>
              <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
            </tr>  
            <tr>
              <td style="width: 150px !important;">Requested By</td>
              <td style="width: 5px !important;">:</td>
              <td style="width: 300px !important;"><?php echo $created_by; ?></td>
              <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 150px !important;">Requested Date</td>
              <td style="width: 5px !important;">:</td>
              <td style="width: 300px !important;"><?php echo $created_date; ?></td>
              <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
            </tr>   
          </table>
        </td>  
        <td>
            &nbsp;
        </td>
        <td>
          <table>
            <tr>
              <td style="width: 150px !important;">MTO Number</td>
              <td style="width: 5px !important;">:</td>
              <td style="width: 300px !important;">MTO-<?php echo $year; ?>-<?php echo $mr_list['mto_number']; ?></td>
              <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
            </tr>  
            <tr>
              <td style="width: 150px !important;">Module</td>
              <td style="width: 5px !important;">:</td>
              <td style="width: 300px !important;"><?php echo $module; ?></td>
              <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
            </tr>
            <tr>
              <td style="width: 150px !important;">Priority</td>
              <td style="width: 5px !important;">:</td>
              <td style="width: 300px !important;"><?php echo $data_priority; ?></td>
              <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td>
            </tr>   
          </table>
        </td>  
      </tr>   
    </table>

  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-left: -25px;">
       <thead><tr bgcolor="#008060" style="color: white !important; text-align: center;">
          <th>DESCRIPTION</th>
          <th>STEEL TYPE</th>
          <th>GRADE</th>
          <th>THK<br/>(mm)</th>
          <th>WIDTH<br/>(M)</th>
          <th>LENGTH<br/>(M)</th>
          <th>NET AREA<br/>(M2)</th>
          <th>AREA PER<br/>PLATE<br/>(M2)</th>
          <th>NET<br/>LENGTH<br/>(M)</th>
          <th>UNIT WT<br/>(KG/M)</th>
          <th>CONT<br/>(%)</th>
          <th>TOTAL<br/>(Pcs)</th>
          <th>CERT<br/>(3.1/3.2)</th>
          <th>WEIGTH<br/>(MT) / per<br/>Piece</th>
          <th>WEIGTH<br/>(MT) / Total</th>
          <th>REMARKS</th>
        </tr></thead>
        <tbody><?php $no=1; foreach ($mr_list_detail as $key) {if(!empty($width_m[$key['catalog_id']]) AND !empty($length_m[$key['catalog_id']])){$area_perplate = $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']]; $area_perplate_show = round($area_perplate,3);} else {echo "-";} if(!empty($width_m[$key['catalog_id']])){ $total = (($key['nett_area'] * ($key['cont'] / 100)) + $key['nett_area'])/$area_perplate_show; $total_show = ceil($total); } else { $total = (($key['nett_length'] * ($key['cont'] / 100)) + $key['nett_length'])/$length_m[$key['catalog_id']]; $total_show = ceil($total); } if(!empty($width_m[$key['catalog_id']])){ $weigth_mt_per_piece = ( $thk_mm[$key['catalog_id']] * $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']] * 7.85 ) / 1000; $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2); } else { $weigth_mt_per_piece = ( $length_m[$key['catalog_id']] * $key['unit_wt'] ) / 1000; $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2); } $total_weight_mt = $total_show * $weigth_mt_per_piece_show; $total_weight_mt_show = round($total_weight_mt,2);?><tr><td><?php echo (isset($material[$key['catalog_id']]) ? $material[$key['catalog_id']] : '-'); ?></td><td><?php echo (isset($steel_type[$key['catalog_id']]) ? $steel_type[$key['catalog_id']] : '-'); ?></td><td><?php echo (isset($grade[$key['catalog_id']]) ? $grade[$key['catalog_id']] : '-'); ?></td><td><?php echo (isset($thk_mm[$key['catalog_id']]) ? $thk_mm[$key['catalog_id']] : '-'); ?></td><td><?php echo (isset($width_m[$key['catalog_id']]) ? $width_m[$key['catalog_id']] : '-'); ?></td><td><?php echo (isset($length_m[$key['catalog_id']]) ? $length_m[$key['catalog_id']] : '-'); ?></td><td><?php echo $key['nett_area']; ?></td><td><?php echo $area_perplate_show; ?></td><td><?php echo $key['nett_length']; ?></td><td><?php echo $key['unit_wt']; ?></td><td><?php echo $key['cont']; ?></td><td><?php echo $total_show; ?></td><td><?php echo $key['certification']; ?></td><td><?php echo $weigth_mt_per_piece_show; ?></td><td><?php echo $total_weight_mt_show; ?></td><td><?php echo $key['remarks']; ?></td></tr><?php $no++;} ?></tbody>  
  </table>
</body></html>