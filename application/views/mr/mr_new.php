<style type="text/css">
.show {
  
  opacity: 1;
}

.hide {
  display: none;
  opacity: 0;
}

/* The container */
.containerx {
  font-family: arial;
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.containerx input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmarkx {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.containerx:hover input ~ .checkmarkx {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.containerx input:checked ~ .checkmarkx {
  background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmarkx:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.containerx input:checked ~ .checkmarkx:after {
  display: block;
}

/* Style the checkmark/indicator */
.containerx .checkmarkx:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

.red_title{
  color: red;
  font-weight: bold;
  font-size: 9px;
  font-style: italic;
}
.red_detail {
  color: red;
  font-style: italic;
  font-size: 10px;
  font-style: italic;
}
</style>

<script type='text/javascript'>

  $(window).on('load', function() { 
    add_list_so();
    add_list_tab();
  });


  var count_element_so = 1;

  function add_list_so(){

    var html_element_so =  '<tr id="tr_element_so_' + count_element_so + '">' +

                          '<td>' + count_element_so + '<input type="hidden" id="nos_so'+count_element_so+'" name="nos_so[' + count_element_so + ']" value="' + count_element_so + '" class="form-control" readonly min="0"></td>' +                            

                          '<td><input type="hidden" id="part_number_so'+count_element_so+'" name="part_number_so[' + count_element_so + ']" class="form-control" value="0"><textarea class="form-control" id="tec_spec_so'+count_element_so+'" name="tec_spec_so[' + count_element_so + ']"  ></textarea></td>' + 

                          '<td><input type="number" id="qty_req_so'+count_element_so+'" name="qty_req_so[' + count_element_so + ']" class="form-control" min="0"></td>' +  
                          
                          '<td><select id="uom_req_so'+count_element_so+'" name="uom_req_so[' + count_element_so + ']" class="form-control" ><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                         

                          '<td><input type="number" id="qty_order_so'+count_element_so+'" name="qty_order_so[' + count_element_so + ']" class="form-control"  min="0" ></td>' +  
                          
                          '<td><select id="uom_order_so'+count_element_so+'" name="uom_order_so[' + count_element_so + ']" class="form-control" ><?php foreach ($uom_list as $value) { echo "<option value=".$value["id_uom"].">".$value["uom"]."</option>"; } ?></select> </td>' +

                          '<td><select id="currency_so'+count_element_so+'" name="currency_so[' + count_element_so + ']" onchange="change_overalcur(' + count_element_so + ');" class="form-control" ><?php foreach ($currency_list as $value) { echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; } ?></select> </td>' +

                          '<td><input type="number" id="estimate_price_so'+count_element_so+'" name="estimate_price_so[' + count_element_so + ']" class="form-control" ></td>' + 

                          '<td><button class="btn btn-danger" type="button" onclick="delete_element_so(' + count_element_so + ')"><i class="fas fa-trash-alt"></i></button></td>' +   

                        '</tr>';

    $('#table_so').append(html_element_so);

    count_element_so++;
  }

var count_element_tab = 1;

  function add_list_tab(){

    var html_element_tab =  '<tr id="tr_element_so_' + count_element_tab + '">' +   
                            '<td>'+count_element_tab+'</td>' +
                            '<td><input type="hidden" id="nosX'+count_element_tab+'" name="nosX[' + count_element_tab + ']" value="' + count_element_tab + '" class="form-control" readonly min="0"><input type="file" id="file_tabulation'+count_element_tab+'" name="file_tabulation_' + count_element_tab + '" value="' + count_element_tab + '" class="form-control" readonly min="0"></td>' +
                            '</tr>';

    $('#table_tab').append(html_element_tab);

    count_element_tab++;
  }

</script>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>mr/mr_add_process" enctype="multipart/form-data">
     <input type="hidden" class="form-control" name="dept_id" id="dept_id" value='<?php echo $department[0]['id_department']; ?>'>
     <input type="hidden" class="form-control" name="pt_id" id="pt_id" value='<?php echo $read_cookies[12]; ?>'>
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">


              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                       <table class="table">
                        <tr>
                            <td>Date</td>
                            <td>:</td>
                            <td><input type='date' class='form-control' name='date' value='<?php echo date("Y-m-d"); ?>' readonly></td>
                        </tr>
                        <tr>
                            <td>Category Request</td>
                            <td>:</td>
                            <td>
                                  <select class="form-control" name='request_sche' required="" disabled="">
                                    <option value=''>---</option>
                                    <option value='Schedule'>Schedule</option>
                                    <option value='Unschedule'>Unschedule</option>
                                  </select>
                                  <br>
                                  <select class="form-control" name='request_type' required="" disabled="">
                                    <option value=''>---</option>
                                    <option value='Material Request'>Material Request</option>
                                    <option value='Service Request'>Service Request</option>
                                  </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Project</td>
                            <td>:</td>
                            <td>
                                <input type='text' class='form-control' name='project' placeholder="---" required="" disabled="">
                                <br/>
                                <select class="form-control" name='request_checklist' required="" disabled="">
                                    <option value=''>---</option>
                                    <option value='BOP'>BOP</option>
                                    <option value='CCPP'>CCPP</option>
                                    <option value='Chiller'>Chiller</option>
                                    <option value='DEB1'>DEB1</option>
                                    <option value='DEB2'>DEB2</option>
                                    <option value='MEB1'>MEB1</option>
                                    <option value='MEB2'>MEB2</option>
                                    <option value='TM2500'>TM2500</option>
                                 </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Budget</label></td>
                            <td>:</td>
                            <td>
                              <select class="form-control" name='budget_type' id='budget_type'>
                                <option value=''>---</option>
                                <option value='Capex'>Capex</option>
                                <option value='Opex'>Opex</option>
                              </select>
                           </td>
                           </tr>
                        <tr>
                            <td>Posting Budget</td>
                            <td>:</td>
                            <td>
                              <select id='posting_budget' name='posting_budget' class="form-control" onchange="fnc(this);" style="display: inline;">
                                <option value=''>---</option>
                                <?php foreach ($budget as $value) { ?>
                                  <option value='<?php echo $value['id']; ?>'><?php echo $value['account_no']; ?> </option>
                                <?php  } ?>
                              </select> 
                              <div class="col-md-6">
                                <div class="form-group">
                                  <table width="100%">
                                    <tr>
                                      <td><label><b>Budgeted Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='budget_amount'></span></b></label></td>
                                    </tr>
                                    <tr>
                                      <td><label><b>Transfered Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='transfered_amount'></span></b></label></td>
                                    </tr>
                                    <tr>
                                      <td><label><b>Utilized Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='utilized_amount'></span></b></label></td>
                                    </tr>
                                    <tr>
                                      <td><label><b>Balance Amount</b></label></td>
                                      <td><label><b>:</b></label></td>
                                      <td><label><b><span class='balance_amount'></span></b></label></td>
                                    </tr>  
                                  </table>    
                                </div>
                              </div>     
                            </td>
                        </tr>
                      </table>
                  </div>
                </div>
            </div>
          </div>
       </div>
     </div>
    </div>

              
             
      <div id='form_so' class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="row align-items-center">
          <div class="col-md">
          <h6 class="pb-2 mb-0">Request List</h6>
          </div>
          <div class="col-md text-right">
            <div class="btn btn-primary" onclick='add_list_so();'><i class="fas fa-plus"></i> Add</div>
          </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <table id="table_so" class="table table-hover text-center">
                  <thead class="bg-green-smoe text-white">
                    <tr>
                      <th rowspan="2">No</th>
                      <th rowspan="2">Tecnical Spesification</th>
                      <th colspan="2">Request*</th>                  
                      
                      <th colspan="2">Order*</th>
                      <th rowspan="2">Currency*</th>
                      <th rowspan="2">Estimate Price*</th>
                      <th rowspan="2">Action</th>         
                    </tr>
                    <tr>
                      <th>Qty</th>
                      <th>Unit</th>
                    
                      <th>Qty</th>
                      <th>Unit</th>
                    </tr>
                  </thead>
                </table>

              </div>
             
            </div>
          </div>
         
        </div>
      </div>

 



       <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Document Data :</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <div class="col-md-5">
                  <div class="form-group">
                    <label>Document Attached :</label>
                    
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Justification :</label>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="containerx">Drawing
                      <input type="checkbox" disabled name='drawing' value='1'>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_drawing" id="justification_drawing" placeholder="----">
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                    <input type="file" class="form-control" name="drawing_attach" id="drawing_attach" placeholder="----">
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="containerx">Term Of Reference
                      <input type="checkbox" disabled name='term_of_reference' value='1'>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_of_reference" id="justification_of_reference" placeholder="----">
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                    <input type="file" class="form-control" name="term_attach" id="term_attach" placeholder="----">
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="containerx">Pictures
                      <input type="checkbox" disabled name='picture'  value='1'>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_picture" id="justification_picture" placeholder="----">
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                    <input type="file" class="form-control" name="pic_attach" id="pic_attach" placeholder="----">
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md-3">
                  <div class="form-group">
                    <label class="containerx">Email Correspondences
                      <input type="checkbox" disabled name='email_correspondences'  value='1'>
                      <span class="checkmarkx"></span>
                    </label>
                  </div>
                </div> 

                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" class="form-control" name="justification_email" id="justification_email" placeholder="----">
                  </div>
                </div> 

                <div class="col-md-3">
                  <div class="form-group">
                    <input type="file" class="form-control" name="email_attach" id="email_attach" placeholder="----">
                  </div>
                </div> 

               
              </div>

                           
            </div>
          </div>
        </div>
      </div>


      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          
            <div class="text-right mt-3">
             <button type="submit" name='submitBtn' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fas fa-plus"></i> Submit</button>
            <a href="<?php echo base_url();?>mr/mr_list/open" class="btn btn-secondary " title="Submit"><i class="fas fa-times-circlee"></i> Cancel</a>
          </div>
        </div>
      </div>



    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepickerx').datepicker({
    format: 'yyyy-mm-dd',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  }).datepicker("setDate", new Date());

function delete_element_material(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_material_process(count);

      }
    })
  }

  function delete_element_material_process(count){
    $('#tr_element_material_' + count).remove();
  }

  function delete_element_so(count){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_so_process(count);

      }
    })
  }

  function delete_element_so_process(count){
    $('#tr_element_so_' + count).remove();
  }

  function delete_element_quotation(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_quotation_process(count);

      }
    })
  }

  function delete_element_quotation_process(count){
    $('#tr_element_quotation_' + count).remove();
  }

 
</script>

<script>
  $(document).ready(function(){
    fnc();
  });

  var fnc=function(input){
    
    var budget_cat = $('#posting_budget :selected').val();
    var budget_type = $('#budget_type :selected').val();

    $.ajax({
        url: "<?php echo base_url();?>mr/budget_check/",
        type: "post",
        data: {
          'budget_cat': budget_cat,
          'budget_type': budget_type
        },
        success: function(data) {
          if(data.includes("Error")){
            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $('input[name="date"]').attr('disabled', true);
            $('input[name="request_sche"]').attr('disabled', true);
            $('input[name="request_type"]').attr('disabled', true);
            $('input[name="request_checklist"]').attr('disabled', true);
            $('input[name="project"]').attr('disabled', true);  
            
            $('input[name="drawing"]').attr('disabled', true);
            $('input[name="term_of_reference"]').attr('disabled', true);
            $('input[name="picture"]').attr('disabled', true);
            $('input[name="email_correspondences"]').attr('disabled', true);          

          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $('input[name="date"]').removeAttr('disabled');
            $('select[name="request_sche"]').removeAttr('disabled');
            $('select[name="request_type"]').removeAttr('disabled');
            $('select[name="request_checklist"]').removeAttr('disabled');
            $('input[name="project"]').removeAttr('disabled');

            $('input[name="drawing"]').removeAttr('disabled');
            $('input[name="term_of_reference"]').removeAttr('disabled');
            $('input[name="picture"]').removeAttr('disabled');
            $('input[name="email_correspondences"]').removeAttr('disabled');
            

          }
          if (!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }
        }
    });
   

    $(".budget_amount").load("<?php echo base_url(); ?>mr/budget_status/budget_amount/"+budget_cat+"/"+budget_type);
    $(".transfered_amount").load("<?php echo base_url(); ?>mr/budget_status/transfered_amount/"+budget_cat+"/"+budget_type);
    $(".utilized_amount").load("<?php echo base_url(); ?>mr/budget_status/utilized_amount/"+budget_cat+"/"+budget_type);
    $(".balance_amount").load("<?php echo base_url(); ?>mr/budget_status/balance_amount/"+budget_cat+"/"+budget_type);
    $(".balance_amount_cal").load("<?php echo base_url(); ?>mr/budget_status/balance_amount_cal/"+budget_cat+"/"+budget_type);
                             
    //setTimeout(fnc, 10000);
  };


  function unique_no_func(nox){
    
    $("input[name='part_number["+nox+"]']").autocomplete({

      source: function(request,response){
        $.post('<?php echo base_url(); ?>mr/uniqno_autocomplete',{term: request.term }, response, 'json');
      },
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });

  }

  function change_overalcur(nox){
     var current_cur = $('#currency'+nox+' :selected').text();

      $("input[name='overal_cur']").val(current_cur);

  }


  function calculate_amount(nox,input){
    

    var qty          =  $("input[name='req_qty["+nox+"]']").val();
    var priceperunit =  $(input).val();

    var total_amountx = Number(qty) * (priceperunit);

    $("input[name='total_amount["+nox+"]']").val(total_amountx);


    if(nox > 0){

    var totalx = 0;
    for (var i = 1; i < nox; i++) {

      totalx += Number($("input[name='total_amount["+i+"]']").val());
      
    } 

    var overal_amount = totalx + total_amountx;

    } else {

     var totalx = total_amountx;

     var overal_amount = totalx;

    }

    

    $("input[name='overal_amount']").val(overal_amount);

    var total_overall_amount = overal_amount;

    var total_balance_amount = $('#balance_amount_cal').text();

 
    if(Number(total_balance_amount) < total_overall_amount){

     $("input[name='total_amount["+nox+"]']").val(0);
     $("input[name='expected_cost["+nox+"]']").val(0);
      $("input[name='overal_amount']").val(0);
            
      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'Insuficient Budget Balance Amount..',
      });

      return false;
    }


  }

 
  function checkunique(input, num) {

    var text             = $(input).val();
    var empty_val        = "-";

      $.ajax({

        url: "<?php echo base_url(); ?>mr/unique_no_check",
        type: "post",
        data: {
          'part_number': text,
        },

        success: function(data) {
          
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='part_number"+i+"']").val() == text){
                data = 'Error : Duplicate Material Code on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $("textarea[name='tec_spec["+num+"]']").val(empty_val);

                                 
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $("textarea[name='tec_spec["+num+"]']").val(res[0]);

            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  }

</script>