<?php 
  $mr_list       = $mr_list[0];
  $year          = date("Y",strtotime($mr_list["created_date"]));
  $created_by    = (isset($user_data[$mr_list['created_by']]) ? $user_data[$mr_list['created_by']] : '-');
  $created_date  = $mr_list["created_date"];
  $module        = (isset($module[$mr_list['module']]) ? $module[$mr_list['module']] : '-');
  $data_priority = (isset($priority[$mr_list['priority']]) ? $priority[$mr_list['priority']] : '-');
  $project_name  = (isset($project_list[$mr_list['project_id']]) ? $project_list[$mr_list['project_id']] : '-');
  $project_code  = (isset($project_code[$mr_list['project_id']]) ? $project_code[$mr_list['project_id']] : '-');

?>

<!DOCTYPE html>
<html><head>
  <title>MR Number :  MR-<?php echo $year; ?>-<?php echo $mr_list['mr_number']; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 5.3cm;
      margin-left: 1.4cm;
      margin-right: 1.5cm;
      margin-bottom: 3cm;
      font-family: "helvetica";
      font-size: 9px !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 5cm;
      padding-top: 1px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    
    }

    footer {
      position: fixed;
      bottom: 1cm;
      left: 13cm;
      right: 0cm;
      height: 1cm;
      padding-bottom: 2.5px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
      font-style: italic;
    }


    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;
     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table>thead>tr>td,table>tbody>tr>td{
      vertical-align: top;
    }

    .br_break{
      line-height: 15px;
    }

    .br_break_no_bold{
      line-height: 18px;
    }

    .br{
      border-right: 1px #000 solid;
    }
    .bl{
      border-left: 1px #000 solid;
    }
    .bt{
      border-top: 1px #000 solid;
    }
    .bb{
      border-bottom:  1px #000 solid;
    }
    .bx{
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .by{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
    }
    .ball{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .tab{
      display: inline-block; 
      width: 100px;
    }
    .tab2{
      display: inline-block; 
      width: 100px;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }

  </style>
</head><body>
  <header>
    <img src="img/logo.png" style="width: 100px; padding-top: 25px;"><br><br>
    <table width="100%">
      <tr>
        <td style="padding-bottom: 4px;font-size: 15px !important;"><center><u><h1>MATERIAL/SERVICE REQUISITION FORM</h1></u></center></td>
      </tr>     
    </table>
    
    <table width='100%' border="1" style="text-align: center;border-collapse: collapse !important;">
      
      <tr>
        <td>
          <table>
            <tr>
                <td>
                  Project Name:
                  <br><?php echo $project_name; ?>
                </td>
            </tr> 
            <tr>
                <td>
                  Project No :
                  <br><?php echo $project_code; ?>
                </td>
            </tr>   
            <tr>
                <td>
                  Received By:
                 
                  <br>Date :
                  <br>
                </td>
            </tr>  
          </table>
        </td>
        <td>
          <table>
            <tr>
                <td>
                  Requested By :
                  <br>Name : ......
                </td>
            </tr> 
            <tr>
                <td>
                  Date : ......
                  <br>
                  Signature : ......
                </td>
            </tr>   
           
          </table>
        </td>
        <td>
          <table>
            <tr>
                <td>
                  Approved By :
                  <br>Name : ......
                </td>
            </tr> 
            <tr>
                <td>
                  Date : ......
                  <br>
                  Signature : ......
                </td>
            </tr>   
           
          </table>
        </td>  
        <td>
          <table>
            <tr>
                <td>
                  Acknowledge By :
                  <br>Name : ......
                </td>
            </tr> 
            <tr>
                <td>
                  Date : ......
                  <br>
                  Signature : ......
                </td>
            </tr>   
           
          </table>
        </td>
        <td>
          <table>
            <tr>
                <td>
                  Authorized By :
                  <br>Name : ......
                </td>
            </tr> 
            <tr>
                <td>
                  Date : ......
                  <br>
                  Signature : ......
                </td>
            </tr>   
           
          </table>
        </td>
      </tr>   
    </table>
  </header>

  <footer>
    Prepared By : <?php echo $created_by; ?>
  </footer>


  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; ">
       <thead><tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th rowspan="2">DESCRIPTION</th>
                          <th colspan="6">SIZE</th>
                          <th rowspan="2">REQ<br/>QTY</th>
                          <th rowspan="2">UOM</th>
                          <th rowspan="2">EXPECTED<br/>COST IN</th>
                          <th rowspan="2">TOTAL<br/>AMOUNT</th>
                          <th rowspan="2">CURRENCY</th>
                          <th rowspan="2">REASON /<br/>PURPOSE</th>
                          <th rowspan="2">REMARKS</th>
                          <th rowspan="2">NSI NO.</th>
              </tr><tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th>LENGTH</th>
                          <th>THK</th>
                          <th>WIDTH</th>
                          <th>WEIGHT</th>
                          <th>OD</th>
                          <th>SCH</th>                         
                        </tr></thead>
        <tbody><?php $no=1; foreach ($mr_list_detail as $key) {if(!empty($width_m[$key['catalog_id']]) AND !empty($length_m[$key['catalog_id']])){$area_perplate = $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']]; $area_perplate_show = round($area_perplate,3);} else {echo "";} if(!empty($width_m[$key['catalog_id']])){ $total = (($key['nett_area'] * ($key['cont'] / 100)) + $key['nett_area'])/$area_perplate_show; $total_show = ceil($total); } else { $total = (($key['nett_length'] * ($key['cont'] / 100)) + $key['nett_length'])/$length_m[$key['catalog_id']]; $total_show = ceil($total); } if(!empty($width_m[$key['catalog_id']])){ $weigth_mt_per_piece = ( $thk_mm[$key['catalog_id']] * $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']] * 7.85 ) / 1000; $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2); } else { $weigth_mt_per_piece = ( $length_m[$key['catalog_id']] * $key['unit_wt'] ) / 1000; $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2); } $total_weight_mt = $total_show * $weigth_mt_per_piece_show; $total_weight_mt_show = round($total_weight_mt,2);?><tr>
          <td><?php echo (isset($material[$key['catalog_id']]) ? $material[$key['catalog_id']] : '-'); ?></td>
          <td><?php echo $length_m[$key['catalog_id']]; ?></td>
          <td><?php echo $thk_mm[$key['catalog_id']]; ?></td>
          <td><?php echo $width_m[$key['catalog_id']]; ?></td>
          <td><?php echo $weight[$key['catalog_id']]; ?></td>
          <td><?php echo $od[$key['catalog_id']]; ?></td>
          <td><?php echo $sch[$key['catalog_id']]; ?></td>
          <td><?php echo $key['qty']; ?></td>
          <td><?php echo $key['uom']; ?></td>
          <td><?php if($key['expected_cost_id'] > 0){ echo round($key['expected_cost_id'],2); } ?></td>
          <td><?php if($key['expected_cost_id'] > 0){ echo round($key['total_amount'],2); } ?></td>
          <td><?php if($key['expected_cost_id'] > 0){ echo $key['currency']; } ?></td>
          <td><?php echo $key['reason_purpose']; ?></td>
          <td><?php echo $key['remarks']; ?></td>
          <td><?php echo $key['nsi_no']; ?></td></tr><?php $no++;} ?></tbody>  
  </table>

</body></html>