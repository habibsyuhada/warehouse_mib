<?php 
  $mr_list            = $mr_list[0];
  //$mr_list_detail     = $mr_list_detail[0];
  //$mr_list_quo        = $mr_list_quo[0];
  //$mr_list_tabulation = $mr_list_tabulation[0];
  $year               = date("Y",strtotime($mr_list["created_date"]));

 
?>

<?php
  $get_user    = $this->mr_mod->get_user_data_sign($mr_list['created_by']);
  $get_manager = $this->mr_mod->get_user_data_sign($mr_list['manager_on_duty']);
  ?>

<!DOCTYPE html>
<html><head>
  <title>MR Number :  MR-<?php echo $year; ?>-<?php echo $mr_list['mr_number']; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 3cm;
      margin-left: 1.4cm;
      margin-right: 1.5cm;
      margin-bottom: 3cm;
      font-family: "helvetica";
      font-size: 12px !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 5cm;
      padding-top: 1px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    
    }

    footer {
      position: fixed;
      bottom: 1.7cm;
      left:8cm;
      right: 0cm;
      height: 3cm;
      padding-bottom: 2.5px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    }


    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;
     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table>thead>tr>td,table>tbody>tr>td{
      vertical-align: top;
    }

    .br_break{
      line-height: 15px;
    }

    .br_break_no_bold{
      line-height: 18px;
    }

    .br{
      border-right: 1px #000 solid;
    }
    .bl{
      border-left: 1px #000 solid;
    }
    .bt{
      border-top: 1px #000 solid;
    }
    .bb{
      border-bottom:  1px #000 solid;
    }
    .bx{
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .by{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
    }
    .ball{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .tab{
      display: inline-block; 
      width: 100px;
    }
    .tab2{
      display: inline-block; 
      width: 100px;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }

  </style>
   <style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size: 11px !important;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>

  <style>
#customers2 {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size: 11px !important;
  width: 250px;
}

#customers2 td, #customers2 th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers2 tr:nth-child(even){background-color: #f2f2f2;}

#customers2 tr:hover {background-color: #ddd;}

#customers2 th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head><body>
  <header>
   
    <table width="100%">
      <tr>
        <td>
           <img src="img/logo_MEB3.png" style="width: 50px; padding-top: 25px;">
           <img src="img/logo_ompi.png" style="width: 50px; height: 70px; padding-top: 25px;">
        </td>
        <td width="300px"><br/>
          <u><h1 style="font-size: 20px !important;">Form Material & Service Request</h1></u>
        </td>
        <td><br/><br/>
         <table>
           <tr>
              <td>No. Form</td>
              <td>:</td>
              <td>16/FM-MB/MR</td>
           </tr>
           <tr>
              <td>Revisi</td>
              <td>:</td>
              <td>0</td>
           </tr>
           <tr>
              <td>Tanggal</td>
              <td>:</td>
              <td>1 MARET 2014</td>
           </tr>
         </table>
        </td>
      </tr>     
    </table>
    
  </header>

  <table width="100%">
     <tr>
        <td>
          <input type="checkbox" name='unschedule' value='1' <?php if($mr_list['request_sche'] == "Unschedule"){ echo "checked"; } ?>> Unschedule
        </td>
        <td>
          <input type="checkbox" name='schedule' value='1' <?php if($mr_list['request_sche'] == "Schedule"){ echo "checked"; } ?>> Schedule
        </td>
        <td>
          <span style="text-align: right !important;">Date : <?php echo date("Y-m-d",strtotime($mr_list['created_date'])); ?> </span>
        </td>
      </tr>
      <tr>
        <td>
          <input type="checkbox" name='service_request'  value='1' <?php if($mr_list['request_type'] == "Service Request"){ echo "checked"; } ?>> Service Request
        </td>
        <td>
         <input type="checkbox" name='material_request'  value='1' <?php if($mr_list['request_type'] == "Material Request"){ echo "checked"; } ?>> Material Request
        </td>
        <td>
          &nbsp;
        </td>
      </tr>   
  </table>
  <br/>
  <table width="100%">
     <tr>
        <td>
          Project Name : <?php echo $mr_list['project'] ?>      
        </td>
        <td>
          <input type="checkbox" name='meb1' value='1' <?php if($mr_list['request_checklist'] == "MEB1"){ echo "checked"; } ?>> MEB1
        </td>
        <td>
          <input type="checkbox" name='meb2' value='1' <?php if($mr_list['request_checklist'] == "MEB2"){ echo "checked"; } ?>> MEB2
        </td>
        <td>
          <input type="checkbox" name='meb_bop' value='1' <?php if($mr_list['request_checklist'] == "BOP"){ echo "checked"; } ?>> BOP
        </td>
        <td>
          <input type="checkbox" name='meb_ccpp' value='1' <?php if($mr_list['request_checklist'] == "CCPP"){ echo "checked"; } ?>> CCPP
        </td>
        <td>
          <input type="checkbox" name='meb_ciller' value='1' <?php if($mr_list['request_checklist'] == "Chiller"){ echo "checked"; } ?>> CHILLER
        </td>
      </tr>
      <tr>
        <td>
         &nbsp;
        </td>
        <td>
          <input type="checkbox" name='deb1' value='1' <?php if($mr_list['request_checklist'] == "DEB1"){ echo "checked"; } ?>> DEB1
        </td>
        <td>
           <input type="checkbox" name='deb2' value='1' <?php if($mr_list['request_checklist'] == "DEB2"){ echo "checked"; } ?>> DEB2
        </td>
        <td>
          <input type="checkbox" name='deb_bop' value='1' <?php if($mr_list['request_checklist'] == "BOP"){ echo "checked"; } ?>> BOP
        </td>
        <td>
          <input type="checkbox" name='meb_ccpp' value='1' <?php if($mr_list['request_checklist'] == "CCPP"){ echo "checked"; } ?>> CCPP
        </td>
        <td>
          <input type="checkbox" name='deb_ciller' value='1' <?php if($mr_list['request_checklist'] == "Chiller"){ echo "checked"; } ?>> CHILLER
        </td>
      </tr>
      <tr>
        <td>
         &nbsp;
        </td>
        <td>
           <input type="checkbox" name='tm2500' value='1' <?php if($mr_list['request_checklist'] == "TM2500"){ echo "checked"; } ?>> TM2500
        </td>
        <td>
           <input type="checkbox" name='tm_bop' value='1' <?php if($mr_list['request_checklist'] == "BOP"){ echo "checked"; } ?>> BOP
        </td>
        <td>
          &nbsp;
        </td>
        <td>
          &nbsp;
        </td>
        <td>
         &nbsp;
        </td>
      </tr>
      <tr>
        <td>
         &nbsp;
        </td>
        <td>
           <input type="checkbox" name='gs' value='1' > GS
        </td>
        <td>
           <input type="checkbox" name='hsensec' value='1' > HSE & SEC
        </td>
        <td>
         <input type="checkbox" name='wh' value='1' > W/H
        </td>
        <td>
          &nbsp;
        </td>
        <td>
         &nbsp;
        </td>
      </tr>

      <tr>
        <td>
         Posting Budget : <?php echo $budget_data[$mr_list['posting_budget']]; ?>
        </td>
        <td>
           <input type="checkbox" name='capex' value='1' <?php if($mr_list['budget_type'] == "Capex"){ echo "checked"; } ?>> CAPEX
        </td>
        <td>
          <input type="checkbox" name='opex' value='1' <?php if($mr_list['budget_type'] == "Opex"){ echo "checked"; } ?>> OPEX
        </td>
        <td>
        &nbsp;
        </td>
        <td>
          &nbsp;
        </td>
        <td>
         &nbsp;
        </td>
      </tr>

      <tr>
        <td>
        &nbsp;
        </td>
        <td>
           <input type="checkbox" name='nb' value='1'> NB
           <br/>* Non Budget
        </td>
        <td>
           <input type="checkbox" name='nb' value='1'> NB
           <br/>* Non Budget
        </td>
        <td>
          <input type="checkbox" name='lb' value='1'> LB
          <br/>* Limitation Budget
        </td>
        <td>
          <input type="checkbox" name='rb' value='1'> RB
          <br/>* Replacement Budget
        </td>
        <td>
          &nbsp;
        </td>
        <td>
         &nbsp;
        </td>
      </tr>
         
  </table>
  <br/>
  
  <table id='customers' width='100%' border="1" style="text-align: center;border-collapse: collapse !important; "><thead><tr><th rowspan="2">Tecnical Spesification</th><th colspan="2">Request*</th><th colspan="2">Order*</th><th rowspan="2">Currency*</th><th rowspan="2">Estimate Price*</th></tr><tr><th>Qty</th><th>Unit</th><th>Qty</th><th>Unit</th></tr></thead><tbody><?php $no = 1; foreach ($mr_list_detail as $detail) { ?><tr><td><?php echo $detail['tec_spec']; ?></td><td><?php echo $detail['qty_req']; ?></td><td><?php echo $uom_data[$detail['uom_req']]; ?></td><td><?php echo $detail['qty_order']; ?></td><td><?php echo $uom_data[$detail['uom_order']]; ?></td><td><?php echo $cur[$detail['currency']]; ?></td><td><?php echo $detail['estimate_price']; ?></td></tr><?php $no++; } ?></tbody></table>

    <br/>
    <table width="100%">
      <tr>
        <td>
            Document Attached :
        </td>
         <td>
           Justification :
        </td>
        <td>
           File Attachment :
        </td>
      </tr>
      <tr>
        <td>
           <input type="checkbox" name='drawing' value='1' <?php if($mr_list['drawing'] == 1){ echo "checked"; } ?>> Drawing
        </td>
         <td>
           <?php echo $mr_list['justification_drawing']; ?>
        </td>
          <td>
          <a target='_blank' href="<?php echo  base_url(); ?>upload/mr/drawing/<?php echo $mr_list['drawing_attach'] ?>"><?php echo $mr_list['drawing_attach'] ?></a>
        </td>
      </tr>
       <tr>
        <td>
           <input type="checkbox" name='term_of_reference' value='1' <?php if($mr_list['term_of_reference'] == 1){ echo "checked"; } ?>> Term Of Reference
        </td>
         <td>
           <?php echo $mr_list['justification_of_reference']; ?>
        </td>
         <td>
          <a target='_blank' href="<?php echo  base_url(); ?>upload/mr/term/<?php echo $mr_list['term_attach'] ?>"><?php echo $mr_list['term_attach'] ?></a>
        </td>
      </tr>
      <tr>
        <td>
           <input type="checkbox" name='picture'  value='1' <?php if($mr_list['picture'] == 1){ echo "checked"; } ?>> Pictures
        </td>
         <td>
           <?php echo $mr_list['justification_picture']; ?>
        </td>
         <td>
          <a target='_blank' href="<?php echo  base_url(); ?>upload/mr/picture/<?php echo $mr_list['pic_attach'] ?>"><?php echo $mr_list['pic_attach'] ?></a>
        </td>
      </tr>
      <tr>
        <td>
           <input type="checkbox" name='email_correspondences'  value='1' <?php if($mr_list['email_correspondences'] == 1){ echo "checked"; } ?>> Email Correspondences
        </td>
         <td>
           <?php echo $mr_list['justification_email']; ?>
        </td>
        <td>
          <a target='_blank' href="<?php echo  base_url(); ?>upload/mr/email/<?php echo $mr_list['email_attach'] ?>"><?php echo $mr_list['email_attach'] ?></a>
        </td>
      </tr>
    </table>
    <br/>


  <footer>
    <table>
      <tr>
        <td width="60%">
          &nbsp;
        </td>
        <td width="40%">
    <table id='customers2' width='100%'>
      <tr>
        <td width="50px"> USER </td>
        <td width="50px"> MANAGER ON DUTY </td>
      
      
      </tr>
      <tr>
        <td>            
            <img src="data:image/png;base64, <?php echo $get_user[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' /><br/><?php echo $get_user[0]["full_name"]; ?>
        </td>
        <td>            
            <img src="data:image/png;base64, <?php echo $get_manager[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' /><br/><?php echo $get_manager[0]["full_name"]; ?>
        </td>
      </tr>
      <tr>
        <td> Date : <?php echo date("Y-m-d",strtotime($mr_list["created_date"])) ?></td>
        <td> Date : <?php echo date("Y-m-d",strtotime($mr_list["manager_app_date"])) ?></td>
      
      </tr>
    </table>
      </td>
    </tr>
    </table>
  </footer>


  

</body></html>