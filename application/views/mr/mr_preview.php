<?php 
  
  $mto_list = $mto_list[0];

?>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>mis/create_mr" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">


              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Module</label>
                        <input type="text" class="form-control" name="module" id="module" value='<?php echo (isset($module[$mto_list['module']]) ? $module[$mto_list['module']] : '-'); ?>'  readonly>   
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Priority</label>
                        <input type="text" class="form-control" name="mto_number" id="mto_number"  value='<?php echo (isset($priority[$mto_list['priority']]) ? $priority[$mto_list['priority']] : '-'); ?>' placeholder="---" readonly>              
                  </div>
                </div>
              </div>
                          
            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" >
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th><input type="checkbox" id="selectall_omission" style="width: 15px; height: 15px; text-align: center;"></th>
                          <th>MATERIAL CODE</th>
                          <th>DESCRIPTION</th>
                          <th>STEEL TYPE</th>
                          <th>GRADE</th>
                          <th>THK (mm)</th>
                          <th>WIDTH (M)</th>
                          <th>LENGTH (M)</th>
                          <th>NET AREA (M2)</th>
                          <th>AREA PER PLATE (M2)</th>
                          <th>NET LENGTH (M)</th>
                          <th>UNIT WT (KG/M)</th>
                          <th>CONT (%)</th>
                          <th>TOTAL (Pcs)</th>
                          <th>CERTIFICATION (3.1/3.2)</th>
                          <th>WEIGTH (MT) / per Piece</th>
                          <th>WEIGTH (MT) / Total</th>
                          <th>REMARKS</th>
                          <th>MR STATUS</th>
                          
                        </tr>
                        
                      </thead>
                      <tbody>

                      <?php $no=1; foreach ($mto_list_detail as $key) { ?>

                              <?php 
                                //searching net area
                                if(isset($width_m[$key['catalog_id']]) AND isset($length_m[$key['catalog_id']])){
                                    $area_perplate = $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']];
                                    $area_perplate_show = round($area_perplate,3);
                                } else {
                                   echo "-";
                                }

                                //searching total
                                if(isset($width_m[$key['catalog_id']])){
                                   $total = (($key['nett_area'] * ($key['cont'] / 100)) + $key['nett_area'])/$area_perplate_show;
                                   $total_show = round($total,0);
                                } else {
                                   $total = (($key['nett_length'] * ($key['cont'] / 100)) + $key['nett_length'])/$length_m[$key['catalog_id']];
                                   $total_show = round($total,0);
                                }

                                //searching weigth mt / piece
                                if(isset($width_m[$key['catalog_id']])){
                                   $weigth_mt_per_piece = ( $thk_mm[$key['catalog_id']] * $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']] * 7.85 ) / 1000; 
                                   $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2);
                                } else {
                                   $weigth_mt_per_piece = ( $length_m[$key['catalog_id']] * $key['unit_wt'] ) / 1000; 
                                   $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2);
                                }

                                //Searching weigth mt / total
                                $total_weight_mt      = $total_show * $weigth_mt_per_piece_show;
                                $total_weight_mt_show = round($total_weight_mt,2);

                                //searching MR Status
                                if(isset($mr_detail[$key['id']])){
                                  $data_mr_status = '<span class="badge badge-success">MR-'.$mr_detail[$key['id']].'</span>';
                                } else {
                                  $data_mr_status = '<span class="badge badge-danger">Waiting</span>';
                                }
                        ?>
                        <tr>
                          <td>
                            <?php if(isset($mr_detail[$key['id']])){ ?>
                              -
                            <?php } else { ?>

                             <input type="hidden" name="id[]" value="<?= $key['id'] ?>">
                             <input type="hidden" name="id_checkbox[]" value="0">
                             <input type="checkbox" name="cb_add_mr[]" style="width: 15px; height: 15px; text-align: center;">

                            <?php } ?>
                          </td>
                          <td><?php echo (isset($code_material[$key['catalog_id']]) ? $code_material[$key['catalog_id']] : '-'); ?></td>
                          <td><?php echo (isset($material[$key['catalog_id']]) ? $material[$key['catalog_id']] : '-'); ?></td>
                          <td><?php echo (isset($steel_type[$key['catalog_id']]) ? $steel_type[$key['catalog_id']] : '-'); ?></td>
                          <td><?php echo (isset($grade[$key['catalog_id']]) ? $grade[$key['catalog_id']] : '-'); ?></td>
                          <td><?php echo (isset($thk_mm[$key['catalog_id']]) ? $thk_mm[$key['catalog_id']] : '-'); ?></td>
                          <td><?php echo (isset($width_m[$key['catalog_id']]) ? $width_m[$key['catalog_id']] : '-'); ?></td>
                          <td><?php echo (isset($length_m[$key['catalog_id']]) ? $length_m[$key['catalog_id']] : '-'); ?></td>
                          <td><?php echo $key['nett_area']; ?></td>
                          <td><?php echo $area_perplate_show; ?></td>
                          <td><?php echo $key['nett_length']; ?></td>
                          <td><?php echo $key['unit_wt']; ?></td>
                          <td><?php echo $key['cont']; ?></td>
                          <td><?php echo $total_show; ?></td>
                          <td><?php echo $key['certification']; ?></td>
                          <td><?php echo $weigth_mt_per_piece_show; ?></td>
                          <td><?php echo $total_weight_mt_show; ?></td>
                          <td><?php echo $key['remarks']; ?></td>
                          <td><?php echo $data_mr_status; ?></td>
                        </tr>
                      <?php $no++;} ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-right mt-3">

            <a href="<?php echo base_url(); ?>mis/mis_pdf/<?php echo strtr($this->encryption->encrypt($mto_list['mrs_no']), '+=/', '.-~'); ?>" class="btn btn-primary" title="Submit MR"> Submit</a>

            <a href="<?php echo base_url();?>mr/mto_list/approved" class="btn btn-secondary " title="Submit"> Cancel</a>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mto_list['mrs_no']; ?>/"+param;
      }
    })

  }
</script>


<script type="text/javascript">


  $('#selectall_omission').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);

    if($(this).prop("checked") == true){
      $('button[name="btnsubmit_omission"]').removeAttr('disabled');
      $('input[name="id_checkbox[]"]').val(1);
    }
    else if($(this).prop("checked") == false){
      $('button[name="btnsubmit_omission"]').attr('disabled', true);
      $('input[name="id_checkbox[]"]').val(0);
    }
  });

  $('input[name="cb_add_mr[]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1);
    }

    else if($(this).prop("checked") == false){
      $(id_array).val(0);
    }

    if ($('input[name="cb_add_mr[]"]:checked').length > 0){
      $('button[name="btnsubmit_omission"]').removeAttr('disabled');
    } else {
      $('button[name="btnsubmit_omission"]').attr('disabled', true);
    }
    
  });

  </script>