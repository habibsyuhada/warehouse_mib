<?php 
  $mto_list = $mto_list[0];
?>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>mr/process_create_mr" id='form-mr' onsubmit="return validateForm()" enctype="multipart/form-data">
 
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <?php  echo $this->session->flashdata('message');?>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                      <label>Project Name</label>

                      <input type="hidden" class="form-control" name="project_id" id="project_id"  value='<?php echo $mto_list["project_id"] ?>'>

                        <input type="text" class="form-control" name="project_id_show" id="project_id_show"  value='<?php echo (isset($project_name[$mto_list['project_id']]) ? $project_name[$mto_list['project_id']] : '-'); ?>' placeholder="---" readonly>              
                    </div>
                  </div>
              </div> 
                            
              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Module</label>
                        <input type="hidden" class="form-control" name="mto_number" id="mto_number" value='<?php echo $mto_list['mto_number']; ?>' readonly>   
                        <input type="hidden" class="form-control" name="module" id="module" value='<?php echo $mto_list['module']; ?>' readonly>   
                        <input type="hidden" class="form-control" name="priority" id="priority" value='<?php echo $mto_list['priority']; ?>' readonly>   
                        <input type="hidden" class="form-control" name="project_id" id="priority" value='<?php echo $mto_list['project_id']; ?>' readonly>   
                        <input type="text" class="form-control" name="module_show" id="module_show" value='<?php echo (isset($module[$mto_list['module']]) ? $module[$mto_list['module']] : '-'); ?>'  readonly>   
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Priority</label>
                        <input type="text" class="form-control" name="priority_show" id="priority_show"  value='<?php echo $priority[$mto_list['priority']]; ?>' placeholder="---" readonly>
                  </div>
                </div>
              </div>                          
            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center">
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <?php if($read_permission[17] == 1){  ?>
                          <th><input type="checkbox" id="selectall_omission" style="width: 15px; height: 15px; text-align: center;"></th>
                          <?php } ?>
                          <th>MATERIAL CODE</th>
                          <th>DESCRIPTION</th>
                          <th>MTO QTY</th>
                          <th>REQ QTY</th>
                          <th>UOM</th>
                          
                          <th>EXPECTED COST IN</th>
                          <th>TOTAL AMOUNT</th>
                          <th>CURRENCY</th>
                          <th>REASON / PURPOSE</th>
                          <th>REMARKS</th>
                          <th>NSI NO.</th>
                          <th>STATUS</th>
                        </tr>                        
                      </thead>
                      <tbody>

                      <?php $no=1; foreach ($mto_list_detail as $key) { ?>

                             <?php 

                              $search_total_mr_partial = $this->mr_mod->mr_total_by_item_call($key['id']);

                              //searching net area
                                if(!empty($width_m[$key['catalog_id']]) AND !empty($length_m[$key['catalog_id']])){
                                    $area_perplate = $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']];
                                    $area_perplate_show = round($area_perplate,3);
                                } else {
                                   echo " ";
                                }

                                //searching total
                                if(!empty($width_m[$key['catalog_id']])){
                                   $total = (($key['nett_area'] * ($key['cont'] / 100)) + $key['nett_area'])/$area_perplate_show;
                                   $total_show = ceil($total);
                                } else {
                                   $total = (($key['nett_length'] * ($key['cont'] / 100)) + $key['nett_length'])/$length_m[$key['catalog_id']];
                                   $total_show = ceil($total);
                                }

                                //searching weigth mt / piece
                                if(!empty($width_m[$key['catalog_id']])){
                                   $weigth_mt_per_piece = ( $thk_mm[$key['catalog_id']] * $width_m[$key['catalog_id']] * $length_m[$key['catalog_id']] * 7.85 ) / 1000; 
                                   $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2);
                                } else {
                                   $weigth_mt_per_piece = ( $length_m[$key['catalog_id']] * $key['unit_wt'] ) / 1000; 
                                   $weigth_mt_per_piece_show = round($weigth_mt_per_piece,2);
                                }

                                //Searching weigth mt / total
                                $total_weight_mt      = $total_show * $weigth_mt_per_piece_show;
                                $total_weight_mt_show = round($total_weight_mt,2);

                                //searching MR Status
                                $total_mr_qty = $total_show - $search_total_mr_partial[0]['total_mr'];
                        ?>
                       
                        <tr>
                           <?php if($read_permission[17] == 1){  ?>
                          <td>
                            <?php //echo //$balance_item; ?>

                            <?php if($total_mr_qty <= 0){ ?>
                           
                            <?php //echo $data_mr_status; ?>
                            <input type="hidden" name="id[<?php echo $no; ?>]" value="<?= $key['id'] ?>">
                            <input type="hidden" id='id_checkbox' name="id_checkbox[<?php echo $no; ?>]" value="0">
                            <input type="hidden" name="cb_add_mr[<?php echo $no; ?>]" style="width: 15px; height: 15px; text-align: center;">
                            <!-- AS -->
                            <input type="hidden" name="category_material[<?php echo $no; ?>]" value='<?php echo $category_material[$key['catalog_id']]; ?>'>
                            <input type="hidden" name="catalog_id[<?php echo $no; ?>]" value='<?php echo $key['catalog_id']; ?>'>
                            
                            <input type="hidden" name="nett_area[<?php echo $no; ?>]" value='<?php echo $key['nett_area']; ?>'>
                            <input type="hidden" name="nett_length[<?php echo $no; ?>]" value='<?php echo $key['nett_length']; ?>'>
                            <input type="hidden" name="unit_wt[<?php echo $no; ?>]" value='<?php echo $key['unit_wt']; ?>'>
                            <input type="hidden" name="cont[<?php echo $no; ?>]" value='<?php echo $key['cont']; ?>'>
                            <input type="hidden" name="certification[<?php echo $no; ?>]" value='<?php echo $key['certification']; ?>'>
                            <input type="hidden" name="remarks[<?php echo $no; ?>]" value='<?php echo $key['remarks']; ?>'>
                            <input type="hidden" name="mto_id[<?php echo $no; ?>]" value='<?php echo $key['mto_id']; ?>'>

                            <?php } else { ?>

                            <input type="hidden" name="id[<?php echo $no; ?>]" value="<?= $key['id'] ?>">
                            <input type="hidden" id='id_checkbox' name="id_checkbox[<?php echo $no; ?>]" value="0">
                            <input type="checkbox" name="cb_add_mr[<?php echo $no; ?>]" style="width: 15px; height: 15px; text-align: center;">
                            <!-- AS -->
                            <input type="hidden" name="category_material[<?php echo $no; ?>]" value='<?php echo $category_material[$key['catalog_id']]; ?>'>
                            <input type="hidden" name="catalog_id[<?php echo $no; ?>]" value='<?php echo $key['catalog_id']; ?>'>
                            
                            <input type="hidden" name="nett_area[<?php echo $no; ?>]" value='<?php echo $key['nett_area']; ?>'>
                            <input type="hidden" name="nett_length[<?php echo $no; ?>]" value='<?php echo $key['nett_length']; ?>'>
                            <input type="hidden" name="unit_wt[<?php echo $no; ?>]" value='<?php echo $key['unit_wt']; ?>'>
                            <input type="hidden" name="cont[<?php echo $no; ?>]" value='<?php echo $key['cont']; ?>'>
                            <input type="hidden" name="certification[<?php echo $no; ?>]" value='<?php echo $key['certification']; ?>'>
                            <input type="hidden" name="remarks[<?php echo $no; ?>]" value='<?php echo $key['remarks']; ?>'>
                            <input type="hidden" name="mto_id[<?php echo $no; ?>]" value='<?php echo $key['mto_id']; ?>'>
                            <!-- AS -->

                            <?php } ?>
                          </td>
                          <?php } ?>
                          
                          <td>
                              <?php echo (isset($code_material[$key['catalog_id']]) ? $code_material[$key['catalog_id']] : '-'); ?>
                              <input type="hidden" name="mat_code[<?php echo $no; ?>]" value='<?php echo (isset($code_material[$key['catalog_id']]) ? $code_material[$key['catalog_id']] : '-'); ?>'>                              
                          </td>

                          <td>
                            <?php //echo $key['id']; ?>
                              <b><?php echo (isset($material[$key['catalog_id']]) ? $material[$key['catalog_id']] : '-'); ?></b> 
                              <br/>
                              <br/>
                              
                              <table>
                                  <?php if(!empty($length_m[$key['catalog_id']])){ ?>
                                  <tr>
                                      <td>Length</td>
                                      <td>:</td>
                                      <td><?php echo $length_m[$key['catalog_id']] ?></td>
                                  </tr>
                                  <?php } ?>
                                  <?php if(!empty($width_m[$key['catalog_id']])){ ?>
                                  <tr>
                                      <td>Width</td>
                                      <td>:</td>
                                      <td><?php echo $width_m[$key['catalog_id']] ?></td>
                                  </tr>
                                  <?php } ?>
                                  <?php if(!empty($weight[$key['catalog_id']])){ ?>
                                  <tr>
                                      <td>Weight</td>
                                      <td>:</td>
                                      <td><?php echo $weight[$key['catalog_id']] ?></td>
                                  </tr>
                                  <?php } ?>
                                  <?php if(!empty($od_m[$key['catalog_id']])){ ?>
                                  <tr>
                                      <td>OD</td>
                                      <td>:</td>
                                      <td><?php echo $od_m[$key['catalog_id']] ?></td>
                                  </tr>
                                  <?php } ?>
                                  <?php if(!empty($sch[$key['catalog_id']])){ ?>
                                  <tr>
                                      <td>Schedule</td>
                                      <td>:</td>
                                      <td><?php echo $sch[$key['catalog_id']] ?></td>
                                  </tr>
                                  <?php } ?>
                              </table>

                              <input type="hidden" name="catalog_id[<?php echo $no; ?>]" value='<?php echo $key['catalog_id']; ?>'>
                              <input type="hidden" id='size' name="size[<?php echo $no; ?>]" <?php if(isset($mr_size[$key['id']])){ ?> value='<?php echo $mr_size[$key['id']] ?>' <?php } else { ?> value='<?php echo $length_m[$key['catalog_id']] ?>' <?php } ?> disabled required style='width: 50px;'>
                          </td>
                          
                          <td>
                            <input type="number"  class='form-control' id='mto_qty' name="mto_qty[<?php echo $no; ?>]" <?php if(isset($mr_qty[$key['id']])){ ?> value='<?php echo $total_show; ?>' <?php } else { ?> value='<?php echo $total_show; ?>' <?php } ?> readonly required step="any" style='width: 50px;'/>
                            <input type="hidden" id='waiting_qty' name="waiting_qty[<?php echo $no; ?>]" <?php if(isset($mr_qty[$key['id']])){ ?> value='<?php echo $total_mr_qty; ?>' <?php } else { ?> value='<?php echo $total_mr_qty; ?>' <?php } ?> readonly required step="any" style='width: 50px;'/>
                          </td>
                          
                          <td>
                            <input type="number"  class='form-control' id='qty' name="qty[<?php echo $no; ?>]" <?php if($total_mr_qty > 0){ ?> value='0' <?php } else { ?> value='<?php echo $total_show; ?>' <?php } ?> disabled required step="any"  min="0" max="<?php  echo $total_mr_qty; ?>" style='width: 50px;'/>
                          </td>

                          <td>
                              <select type='text' id='uom'  class='form-control' name='uom[<?php echo $no; ?>]'  required style='width: 75px;'>
                                <option value="PCS">PCS</option>                               
                              </select>
                          </td>

                          

                          <td><input type="number" class='form-control' id='expected_cost' name="expected_cost[<?php echo $no; ?>]" placeholder="Expected Cost" <?php if(isset($mr_expected_cost_id[$key['id']])){ ?> value='<?php echo $mr_expected_cost_id[$key['id']] ?>' <?php } else { ?> placeholder="Expected Cost" <?php } ?> oninput="autoCalAmmount(<?php echo $no; ?>,this)" disabled required step="any" style='width: 200px;'/></td>

                          <td><input type="number" class='form-control' id='total_amount' name="total_amount[<?php echo $no; ?>]" placeholder="Total Amount" <?php if(isset($mr_total_amount[$key['id']])){ ?> value='<?php echo $mr_total_amount[$key['id']] ?>' <?php } else { ?> placeholder="Total Amount" <?php } ?> disabled required  step="any"  style='width: 200px;'/></td>

                          <td>
                              <select type='text' class='form-control' id='currency' name='currency[<?php echo $no; ?>]' disabled  required>
                                <option value="N/A" <?php if($mr_currency[$key['id']] == "N/A"){ ?> selected <?php } ?>>N/A</option>
                                <option value="IDR" <?php if($mr_currency[$key['id']] == "IDR"){ ?> selected <?php } ?>>IDR</option>
                                <option value="SGD" <?php if($mr_currency[$key['id']] == "SGD"){ ?> selected <?php } ?>>SGD</option>
                                <option value="USD" <?php if($mr_currency[$key['id']] == "USD"){ ?> selected <?php } ?>>USD</option>
                              </select>
                          </td>

                          <td><input type="text" class='form-control' id='reason_purpose' name="reason_purpose[<?php echo $no; ?>]" placeholder="Reason / Purpose" <?php if(isset($mr_reason_purpose[$key['id']])){ ?> value='<?php echo $mr_reason_purpose[$key['id']] ?>' <?php } else { ?> placeholder="Reason Purpose" <?php } ?> disabled required></td>

                          <td><input type="text" class='form-control' id='remarks' name="remarks[<?php echo $no; ?>]" placeholder="Remarks" <?php if(isset($mr_remarks[$key['id']])){ ?> value='<?php echo $mr_remarks[$key['id']] ?>' <?php } else { ?> placeholder="Remarks" <?php } ?> disabled required></td>

                          <td>
                            <input type="text" class='form-control' id='nsi_no' name="nsi_no[<?php echo $no; ?>]" placeholder="NSI No" <?php if(isset($mr_nsi_no[$key['id']])){ ?> value='<?php echo $mr_nsi_no[$key['id']] ?>' <?php } else { ?> placeholder="NSI No" <?php } ?> disabled required>
                          </td>  

                          <td>
                            <?php
                              if(!empty($mr_detail[$key['id']])){

                                $datadb = $this->mr_mod->mr_list_detail_call($key['id']);
                                foreach($datadb as $data_mr){
                                 echo $data_mr_status = "<a href=".base_url()."mr/mr_detail/".strtr($this->encryption->encrypt($data_mr['mr_number']), '+=/', '.-~')."><span class='badge badge-primary'>MR-".date("Y",strtotime($mto_list['created_date']))."-".$data_mr['mr_number']." ( ".$data_mr['qty']." Pcs )</span></a>";
                                }

                              } else {

                                 echo $data_mr_status = '<span class="badge badge-danger">OPEN</span>';

                              }
                            ?>                           
                          </td>                       
                          
                        </tr>

                      <?php $no++; } ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-left mt-3">

            <?php if($read_permission[17] == 1){  ?>

              <?php if($balance_item > 0){ ?>
                <button class="btn btn-success" type="button" name="btnsubmit_omission" onclick="_alert()" type="button" disabled><i class="fa fa-check"></i> Create MR</button>
                <input type="submit" style="display:none">
              <?php } ?>
            
            <?php } ?>
            
              <a href="<?php echo base_url();?>mr/mto_list/approved" class="btn btn-secondary " title="Submit"> Cancel</a>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mto_list['mrs_no']; ?>/"+param;
      }
    })

  }
</script>


<script type="text/javascript">


  $('#selectall_omission').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);

    if($(this).prop("checked") == true){
      $('button[name="btnsubmit_omission"]').removeAttr('disabled');

      $('input[id="id_checkbox"]').val(1);
      $('input[id="size"]').removeAttr('disabled');
      $('input[id="uom"]').removeAttr('disabled');
      $('input[id="qty"]').removeAttr('disabled');
      $('input[id="expected_cost"]').removeAttr('disabled');
      $('input[id="total_amount"]').removeAttr('disabled');
      $('input[id="reason_purpose"]').removeAttr('disabled');
      $('input[id="remarks"]').removeAttr('disabled');
      $('input[id="nsi_no"]').removeAttr('disabled');
      $('select[id="currency"]').removeAttr('disabled');
    }
    else if($(this).prop("checked") == false){
      $('button[name="btnsubmit_omission"]').attr('disabled', true);
      $('input[id="id_checkbox"]').val(0);
      $('input[id="size"]').attr('disabled', true);
      $('input[id="uom"]').attr('disabled', true);
      $('input[id="qty"]').attr('disabled', true);
      $('input[id="expected_cost"]').attr('disabled', true);
      $('input[id="total_amount"]').attr('disabled', true);
      $('input[id="reason_purpose"]').attr('disabled', true);
      $('input[id="remarks"]').attr('disabled', true);
      $('input[id="nsi_no"]').attr('disabled', true);
      $('select[id="currency"]').attr('disabled', true);
    }
  });


<?php for($i=1;$i<=$src_total_mto_detail;$i++){ ?>
  $('input[name="cb_add_mr[<?php echo $i; ?>]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[<?php echo $i; ?>]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1);
    }

    else if($(this).prop("checked") == false){
      $(id_array).val(0);
    }

    if ($('input[name="cb_add_mr[<?php echo $i; ?>]"]:checked').length > 0){

      $('button[name="btnsubmit_omission"]').removeAttr('disabled');
      $('input[name="size[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="uom[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="qty[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="expected_cost[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="total_amount[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="reason_purpose[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="remarks[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="nsi_no[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('select[name="currency[<?php echo $i; ?>]"]').removeAttr('disabled');

    } else {
      $('button[name="btnsubmit_omission"]').attr('disabled', true);

      $('input[name="size[<?php echo $i; ?>]"]').attr('disabled', true);
      $('input[name="uom[<?php echo $i; ?>]"]').attr('disabled', true);
      $('input[name="qty[<?php echo $i; ?>]"]').attr('disabled', true);
      $('input[name="expected_cost[<?php echo $i; ?>]"]').attr('disabled', true);
      $('input[name="total_amount[<?php echo $i; ?>]"]').attr('disabled', true);
      $('input[name="reason_purpose[<?php echo $i; ?>]"]').attr('disabled', true);
      $('input[name="remarks[<?php echo $i; ?>]"]').attr('disabled', true);
      $('input[name="nsi_no[<?php echo $i; ?>]"]').attr('disabled', true);
      $('select[name="currency[<?php echo $i; ?>]"]').attr('disabled', true);

    }
    
  });

 <?php } ?> 

  function _alert(){
    
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
     }).then((result) => {
      if(result.value) {
          $("#form-mr").submit();
      }
    });
    
  }

  </script>

  <script type="text/javascript">   
  $('#mto_detail').DataTable({
    "language": { 
      "infoFiltered": "" },
      "paging": true,
      "order": [],    
  });
</script>

<script>
function validateForm() {
  <?php for($i=1;$i<=$src_total_mto_detail;$i++){ ?>

  var id_checkbox<?php echo $i; ?>    = document.forms["form-mr"]["id_checkbox[<?php echo $i; ?>]"].value;

  var size<?php echo $i; ?>           = document.forms["form-mr"]["size[<?php echo $i; ?>]"].value;
  var qty<?php echo $i; ?>            = document.forms["form-mr"]["qty[<?php echo $i; ?>]"].value;
  var expected_cost<?php echo $i; ?>  = document.forms["form-mr"]["expected_cost[<?php echo $i; ?>]"].value;
  var total_amount<?php echo $i; ?>   = document.forms["form-mr"]["total_amount[<?php echo $i; ?>]"].value;
  var nsi_no<?php echo $i; ?>         = document.forms["form-mr"]["nsi_no[<?php echo $i; ?>]"].value;
  var mat_code<?php echo $i; ?>       = document.forms["form-mr"]["mat_code[<?php echo $i; ?>]"].value;

  if(size<?php echo $i; ?> == "") {   
    if(id_checkbox<?php echo $i; ?> == 1){
    Swal.fire({
      type: 'warning',
      title: 'Oops...',
      text: 'Size on Material Code '+ mat_code<?php echo $i; ?> + ' must be filled up..',
    })
    return false; 
    } 

  } else if(qty<?php echo $i; ?> == "" || qty<?php echo $i; ?> == "0"){
    if(id_checkbox<?php echo $i; ?> == 1){
    Swal.fire({
      type: 'warning',
      title: 'Oops...',
      text: 'Qty on Material Code'+ mat_code<?php echo $i; ?> + ' must be filled up..',
    })
    return false;
    }  

  } else if(nsi_no<?php echo $i; ?> == ""){
    if(id_checkbox<?php echo $i; ?> == 1){
    Swal.fire({
      type: 'warning',
      title: 'Oops...',
      text: 'NSI Number on Material Code'+ mat_code<?php echo $i; ?> + ' must be filled up..',
    })
    return false;
    }  

  }
  <?php } ?>
}

function autoCalAmmount(param,value){

  var expected_cost   = $(value).val();
  var qty             = $('input[name="qty['+param+']"]').val();
  
  var valTotalAmount   = parseInt(qty) * parseInt(expected_cost);

  $('input[name="total_amount['+param+']"]').val(valTotalAmount);

}
</script>