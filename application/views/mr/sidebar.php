<div class="wrapper" style="min-height: 79vh">
  <nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
    <ul class="list-unstyled components">
     
           
      <li>
        <?php if($read_permission[19] == 1){ ?>
        <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fas fa-home"></i> &nbsp; MR ( Material Requisition )
        </a>
        <?php } ?>
        <ul class="list-unstyled" id="homeSubmenu2">
          <?php if($read_permission[20] == 1){ ?>
            <li>
              <a href="<?php echo base_url();?>mr/mr_new"><i class="fas fa-plus"></i> &nbsp; Create New</a>
            </li>
          <?php } ?>
          <?php if($read_permission[19] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>mr/mr_list/open"><i class="far fa-clock"></i> &nbsp; Waiting List</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>mr/mr_list/rejected"><i class="fas fa-times-circle"></i> &nbsp; Rejected List</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>mr/mr_list/approved"><i class="fas fa-check-circle"></i> &nbsp; Approved List</a>
        </li>
        <?php } ?>
        </ul>
      </li> 

      <li>
        
        <a href="<?php echo base_url();?>mr/mr_list_tab/approved"  class="dropdown-toggle">
          <i class="fas fa-list-alt"></i> &nbsp; Quotation & Tabulation
        </a>
      
      </li> 
   

    </ul>
  </nav>