<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=MRIR_Detail.xls");
?>
<table border="2">
    <thead>
        <tr>
          <th colspan="2" style="vertical-align:middle;">MRIR Status<br/>( QTY )</th>
          <th colspan="3" style="vertical-align:middle;">OSD Status<br/>( QTY )</th>
         
          <th rowspan="2" style="vertical-align:middle;">UOM</th>
          <th colspan="3" style="vertical-align:middle;">MATERIAL</th>
          <th rowspan="2" style="vertical-align:middle;width: 200px;">DEPARTMENT</th>

          <th rowspan="2" style="vertical-align:middle;">BRAND</th>
          <th rowspan="2" style="vertical-align:middle;">COLOR CODE</th>
          <th rowspan="2" style="vertical-align:middle;">DATE RECEIVING</th>
          <th rowspan="2" style="vertical-align:middle;">REMARKS</th>
        </tr>
        <tr>
          <th style="vertical-align:middle;">Submitted</th>
          <th style="vertical-align:middle;">Approved</th>

          <th style="vertical-align:middle;">Damage</th>
          <th style="vertical-align:middle;">Shortage</th>
          <th style="vertical-align:middle;">Over</th>

          <th style="vertical-align:middle;">Code</th>
          <th style="vertical-align:middle;">Category</th>
          <th style="vertical-align:middle;">Name</th>

        </tr>
    </thead>
    <tbody align="center">
        <?php foreach ($mrir_detail_list as $key => $value) { ?>
		<tr>
			<td>
                <?php if($mrir_detail['status'] == 3): ?>
                    Approve
                <?php elseif($mrir_detail['status'] == 2): ?>
                    Reject
                <?php else: ?>
                    Request
                <?php endif ?>         
            </td>
			<td><?= $mrir_detail['qty'] ?></td>
			<td><?= ($mrir_detail['qty_approve'] == 0 && $mrir_detail['qty_demage'] == 0 ? $mrir_detail['qty'] : $mrir_detail['qty_approve']) ?></td>
			<td><?= ($mrir_detail['qty_approve'] == 0 && $mrir_detail['qty_demage'] == 0 ? 0 : $mrir_detail['qty_demage']) ?></td>
            <td><?= $mrir_detail['qty_shortage'] ?></td>
            <td><?= $mrir_detail['qty_over'] ?></td>
            <td><?= $mrir_detail['uom'] ?></td>
            <td><?= $mrir_detail['code_material'] ?></td>
            <td><?= $mrir_detail['catalog_category'] ?></td>
            <td><?= $mrir_detail['material'] ?></td>
            <td><?= $mrir_detail['name_of_department'] ?></td>
            <td><?= $mrir_detail['brand'] ?></td>
            <td><?= $mrir_detail['color_code'] ?></td>
            <td><?= $mrir_detail['date_manufacturing'] ?></td>
            <td><?= $mrir_detail['remarks'] ?></td>
		</tr>
        <?php } ?>
    </tbody>
</table>