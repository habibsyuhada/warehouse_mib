<?php 
  
?>
<!DOCTYPE html>
<html><head>
  <title></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 3.8cm;
      margin-left: 2.5cm;
      margin-right: 1.5cm;
      margin-bottom: 2cm;
      font-family: "helvetica";
      font-size: 50% !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      padding-top: 15px;
      padding-left: 1.5cm;
      padding-right: 1.5cm;
    
    }

    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table tr {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }
   
  </style>
</head><body>
  <header>
  	<img src="img/logo.png" style="width: 100px;"><br><br>
  	<table>
  		<tr>
  			<td style="font-size: 10px; width: 100px;">Client</td>
  			<td style="font-size: 10px;">:</td>
  			<td style="font-size: 10px; width: 250px;"></td>
  			<td style="font-size: 10px; width: 350px;"><b><u>MATERIAL RECEIVING AND INSPECTION REPORT</u></b></td>
  			<td style="font-size: 10px; width: 150px;">Report No.</td>
  			<td style="font-size: 10px; width: 5px;"></td>
  			<td style="font-size: 10px; width: 10px; border: 1px solid; padding: 2px;"></td>
  		</tr>
  		<tr>
  			<td style="font-size: 10px; width: 100px;">Project Title</td>
  			<td style="font-size: 10px;">:</td>
  			<td style="font-size: 10px; width: 250px;"></td>
  			<td style="font-size: 10px; width: 350px;"></td>
  		 <td style="font-size: 10px; width: 150px;">&nbsp;</td>
        <td style="font-size: 10px; width: 5px;">&nbsp;</td>
        <td style="font-size: 10px; width: 10px;">&nbsp;</td>
  		</tr>
  		<tr>
  			<td style="font-size: 10px; width: 100px;">Project Ref</td>
  			<td style="font-size: 10px;">:</td>
  			<td style="font-size: 10px; width: 250px;"></td>
        <td style="font-size: 10px; width: 350px;"></td>
        <td style="font-size: 10px; width: 150px;" colspan="3">
            <table>
              <tr>
                  <td></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                  <td></td>
              </tr>
               <tr>
                  <td>Vendor Name</td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                  <td></td>
              </tr>
               <!-- <tr>
                  <td>Date Of Receiveing</td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                  <td><?= $str_date_receiving ?></td>
              </tr> -->
            </table>
        </td>
  		</tr> 
       
  	</table>
  </header>
  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-left: -25px;">
	<thead><tr>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 20px;"><b>S/No</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Unique Ident No.</b></th>
        <!-- <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Categories</b></th>     -->
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 70px;"><b>Material Code</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 70px;"><b>Description</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Qty</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>UoM</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Plate / Tag No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Heat / Series No.</b></th>
        <th bgcolor="#a6ffa6" colspan="5" style="font-size: 9px; width: 150px; max-height: 25;"><b>Size</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Spec / Grade</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Mill Cert No.</b></th>        
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Brand</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>DO No. / PL No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Vendor</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Color Code</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Remarks</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Status</b></th>
    </tr></thead>
    <thead><tr>
    	<th bgcolor="#a6ffa6" style="font-size: 9px;">Length</th>
    	<th bgcolor="#a6ffa6" style="font-size: 9px;">Width</th>
      <th bgcolor="#a6ffa6" style="font-size: 9px;">Thnk</th>
      <th bgcolor="#a6ffa6" style="font-size: 9px;">OD</th>
      <th bgcolor="#a6ffa6" style="font-size: 9px;">Sch</th>
    </tr></thead>    
    <tbody><?php $no=1; foreach($q_material as $dt_material){ ?><tr>
    	<td><?= $no ?></td>
    	<!-- <td><?= $dt_material->category ?></td> -->
      <td><?= $dt_material->unique_ident_no ?></td>
      <td><?= $catalog_list[$dt_material->catalog_id]['code_material'] ?></td>
      <td><?= $dt_material->description ?></td>
      <td><?= $dt_material->qty ?></td>
      <td><?= $dt_material->uom ?></td>
      <td><?= $dt_material->plate_or_tag_no ?></td>
      <td><?= $dt_material->heat_or_series_no ?></td>
      <td><?= $catalog_list[$dt_material->catalog_id]['length_m'] ?></td>
      <td><?= $catalog_list[$dt_material->catalog_id]['width_m'] ?></td>
      <td><?= $catalog_list[$dt_material->catalog_id]['thk_mm'] ?></td>
      <td><?= $catalog_list[$dt_material->catalog_id]['od'] ?></td>
      <td><?= $catalog_list[$dt_material->catalog_id]['sch'] ?></td>
    	<td><?= $dt_material->spec ?></td>
    	<td><?= $dt_material->mill_cert_no ?></td>    	
    	<td><?= $dt_material->brand ?></td>
      <?php if($category == 'SS'): ?>
      <td><?= $dt_material->do_or_pl_no ?></td>
      <?php endif; ?>
      <?php if($category == 'CS'): ?>
      <td><?= @$vendor_list[$dt_material->vendor]; ?></td>
      <?php endif; ?>
    	<td><?= $dt_material->color_code ?></td>
      <td><?= $dt_material->remarks ?></td>
      <td><?php if($dt_material->status == '3'){ echo "Approved"; } else if($dt_material->status == '2'){ echo "Rejected"; } else { echo "Submited"; } ?></td>
    </tr><?php $no++; } ?></tbody>  
  </table>
<br>
  <table width='100%'>
      <tr>
          <td><img src="data:image/png;base64, <?php echo $str_createdby[0]['sign_approval']; ?>" width='100px;'/><br><?php echo $str_createdby[0]['full_name'] ?></td>
          <td>
            <?php if($dt_mrir->approved_by != '0'){ ?>
              <img src="data:image/png;base64, <?php if($dt_mrir->sign_approved != ""){ echo $dt_mrir->sign_approved; } else { echo $str_approvedby[0]["sign_approval"]; } ?>" width='100px;'/><br><?php echo $str_approvedby[0]['full_name'] ?>
            <?php } else {
              echo "<b><div>This Document Still Not </div><div>Approved By QC Inspector</div></b>";
            } ?>
          </td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td>Lead Material / Material Control</td>
          <td>SMOE QC Inspector</td>
          <td>Client Representative</td>
          <td>3rd Party</td>
      </tr>
      <tr>
          <td><!-- Sign / Date : <?php //echo date("d-M-y",strtotime($dt_mrir->created_date)); ?> --></td>
          <td><!-- Sign / Date : <?php //echo date("d-M-y",strtotime($dt_mrir->approved_date)); ?> --></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td><!-- Approval No : 001-LOGISTIC-MRIR-<?php //echo $str_report_no ?> --></td>
          <td><!-- <?php //echo $approval_log[0]["approved_category"]; ?>/<?php //echo $approval_log[0]["id"] ?>/<?php //echo $approval_log[0]["approved_request_no"] ?> --></td>
          <td></td>
          <td></td>
      </tr>
  </table>
  <footer>  	
  </footer>
</body></html>