<?php 
  foreach($q_mrir as $dt_mrir){
    $str_report_no      = $dt_mrir->report_no;
    $str_rev            = $dt_mrir->total_submit;
    $str_po_no          = $dt_mrir->po_no;
    $str_vendor         = $dt_mrir->vendor;
    $str_project        = $this->Mrir_mod->search_project_data($dt_mrir->project_id);
    $str_date_receiving =  date('d-M-y', strtotime($dt_mrir->date_receiving));
    $str_createdby      = $this->Mrir_mod->search_sign_data($dt_mrir->created_by);    
    $str_approvedby     = $this->Mrir_mod->search_sign_data($dt_mrir->approved_by);

   // print_r($str_approvedby);
  }
?>
<!DOCTYPE html>
<html><head>
  <title><?php echo $str_report_no." - ".$str_project[0]['project_name']; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 3cm;
      margin-left: 1.5cm;
      margin-right: 1.5cm;
      margin-bottom: 1cm;
      font-family: "helvetica";
      font-size: 50% !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      padding-top: 15px;
      padding-left: 1.5cm;
      padding-right: 1.5cm;
    
    }


    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;
     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table tr {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }
    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }

   
  </style>
</head><body>
  <header>
  	<img src="img/logo.png" style="width: 100px;"><br><br>
  	<table>
  		<tr>
  			<td style="font-size: 10px; width: 100px;">Client</td>
  			<td style="font-size: 10px;">:</td>
  			<td style="font-size: 10px; width: 250px;"><?php echo $str_project[0]['client']; ?></td>
  			<td style="font-size: 10px; width: 350px;"><b><u>MATERIAL RECEIVING AND INSPECTION REPORT</u></b></td>
  			<td style="font-size: 10px; width: 150px;">Report No.</td>
  			<td style="font-size: 10px; width: 5px;"> : </td>
  			<td style="font-size: 10px; width: 10px; border: 1px solid; padding: 2px;"><?= $str_report_no ?></td>
  		</tr>
  		<tr>
  			<td style="font-size: 10px; width: 100px;">Project Title</td>
  			<td style="font-size: 10px;">:</td>
  			<td style="font-size: 10px; width: 250px;"><?php echo $str_project[0]['project_name']; ?></td>
  			<td style="font-size: 10px; width: 350px;"></td>
  			<td style="font-size: 10px; width: 150px;">Rev.</td>
  			<td style="font-size: 10px; width: 5px;">:</td>
  			<td style="font-size: 10px; width: 10px;"><?= $str_rev ?></td>
  		</tr>
  		<tr>
  			<td style="font-size: 10px; width: 100px;">Project Ref</td>
  			<td style="font-size: 10px;">:</td>
  			<td style="font-size: 10px; width: 250px;"><?php echo $str_project[0]['project_ref']; ?></td>
        <td style="font-size: 10px; width: 350px;"></td>
        <td style="font-size: 10px; width: 150px;">Date of Receiving</td>
        <td style="font-size: 10px; width: 5px;">:</td>
        <td style="font-size: 10px; width: 10px;"><?= $str_date_receiving ?></td>
  		</tr>  		
  	</table>
  </header>
  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-left: -25px;">
	<thead><tr>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 20px;"><b>S/No</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Category</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 60px;"><b>PO No</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 60px;"><b>Vendor</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 70px;"><b>Description / Delivery Condition</b></th>
        <th bgcolor="#a6ffa6" colspan="3" style="font-size: 9px; width: 125px; max-height: 20px;"><b>Size (In MM)</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Spec / Grade</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Spec Category</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 35px;"><b>Type</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Plate / Tag No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Heat / Lot No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>ceq</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Mill Cert No.</b></th>        
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Primary Mill / Country of Origin</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>DO No. / PL No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Unique Ident No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Qty</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>UoM</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Color Code</b></th>
    </tr></thead>
    <thead><tr>
    	<th bgcolor="#a6ffa6" style="font-size: 5px;">Length</th>
    	<th bgcolor="#a6ffa6" style="font-size: 5px;">Width/OD</th>
    	<th bgcolor="#a6ffa6" style="font-size: 5px;">Sch/Thk</th>
    </tr></thead>    
    <tbody><?php $no=1; foreach($q_material as $dt_material){ ?><tr>    
    	<td><?= $no ?></td>
    	<td><?= $dt_material->category ?></td>
    	<td><?= $dt_material->po_number ?></td>
      <td><?= $dt_material->vendor ?></td>
      <td><?= $dt_material->description ?></td>
    	<td><?= $dt_material->length ?></td>
    	<td><?= $dt_material->width_or_od ?></td>
    	<td><?= $dt_material->sch_or_thk ?></td>
    	<td><?= $dt_material->spec ?></td>
      <td><?= (isset($spec_category_list[$dt_material->spec_category]) ? $spec_category_list[$dt_material->spec_category] : '-') ?></td>
    	<td><?= $dt_material->type ?></td>
    	<td><?= $dt_material->plate_or_tag_no ?></td>
    	<td><?= $dt_material->heat_or_series_no ?></td>
    	<td><?= $dt_material->ceq ?></td>
    	<td><?= $dt_material->mill_cert_no ?></td>    	
    	<td><?= $dt_material->supplier_name ?></td>
    	<td><?= $dt_material->do_or_pl_no ?></td>
    	<td><?= $dt_material->unique_ident_no ?></td>
    	<td><?= $dt_material->qty ?></td>
    	<td><?= $dt_material->uom ?></td>
    	<td><?= $dt_material->color_code ?></td>
    </tr><?php $no++; } ?></tbody>  
  </table>
<br>
  <table width='100%' >
      <tr>
          <td><img src="data:image/png;base64, <?php echo $str_createdby[0]['sign_approval']; ?>" width='100px;'/><br><?php echo $str_createdby[0]['full_name'] ?></td>
          <td><img src="data:image/png;base64, <?php if($dt_mrir->sign_approved != ""){ echo $dt_mrir->sign_approved; } else { echo $str_approvedby[0]["sign_approval"]; } ?>" width='100px;'/><br><?php echo $str_approvedby[0]['full_name'] ?></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td>Lead Material / Material Control</td>
          <td>SMOE QC Inspector</td>
          <td>Client Representative</td>
          <td>3rd Party</td>
      </tr>
      <tr>
          <td><!-- Sign / Date : <?php //echo date("d-M-y",strtotime($dt_mrir->created_date)); ?> --></td>
          <td><!-- Sign / Date : <?php //echo date("d-M-y",strtotime($dt_mrir->approved_date)); ?> --></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td><!-- Approval No : 001-LOGISTIC-MRIR-<?php //echo $str_report_no ?> --></td>
          <td><!-- <?php //echo $approval_log[0]["approved_category"]; ?>/<?php //echo $approval_log[0]["id"] ?>/<?php //echo $approval_log[0]["approved_request_no"] ?> --></td>
          <td></td>
          <td></td>
      </tr>
  </table>
  <footer>  	
  </footer>
</body></html>