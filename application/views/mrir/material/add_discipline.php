<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrir/add_discipline_process">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Discipline Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="discipline_name" placeholder="Discipline Name" required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Initial Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="initial_name" placeholder="Initial Name" required>                
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>mrir/discipline" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
</script>