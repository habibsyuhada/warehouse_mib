<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrir/add_material_proccess">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

<?php 
  foreach($q_mrir as $dt_mrir){
?>
              <input type="hidden" name="project_id" value="<?= $dt_mrir->project_id ?>">
<?php
  }
?>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="report_no">Report No</label>
                  <input type="text" class="form-control" name="report_no" placeholder="Report No" value="<?= $report_number ?>" readonly>
                </div>
                <div class="form-group col-md-6">
                  <label>Category</label>
                  <input type="text" class="form-control" id="category" name="category" placeholder="Category" autocomplete="off" required>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Discipline</label>
                  <select class="form-control select2" name="discipline">
                    <?php 

                      $str_discipline = $discipline;

                      foreach($q_discipline as $dt_discipline){
                    ?>
                    <option <?php echo ($str_discipline == $dt_discipline->initial) ? "selected": "" ?> value="<?= $dt_discipline->id ?>"><?= $dt_discipline->discipline_name ?></option>
                    <?php
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-12">
                  <label>Description / Delivery Condition</label>
                  <textarea class="form-control" name="description" placeholder="Description / Delivery Condition" required autocomplete="off"></textarea>  
                </div>
              </div>
            </div>
          </div>


          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-gray">
            <div class="container-fluid">
              <h6 class="pb-2 mb-0" style="color: black">Size (in MM)</h6>

              <div class="form-row">
                <div class="form-group col-md-4">
                  <label>Length</label>
                  <input type="text" class="form-control" name="length" placeholder="Length" autocomplete="off" required>
                </div>
                <div class="form-group col-md-4">
                  <label>Width / OD</label>
                  <input type="text" class="form-control" name="width_or_od" placeholder="Width / OD" required autocomplete="off">    
                </div>
                <div class="form-group col-md-4">
                  <label>SCH/THK</label>
                  <input type="text" class="form-control" name="sch_or_thk" placeholder="SCH/THK" required autocomplete="off"> 
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Spec / Grade</label>
                  <input type="text" class="form-control" name="spec" placeholder="Spec / Grade" required autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                  <label>Spec Category</label>
                  <select class="form-control select2" required name="spec_category">
                  <?php foreach($q_spec_category as $spec_category){ ?>
                    <option value="<?= $spec_category->id ?>"><?= $spec_category->spec_desc ?></option>
                  <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Type</label>
                  <input type="text" class="form-control" name="type" placeholder="Type" required autocomplete="off"> 
                </div>
                <div class="form-group col-md-6">
                  <label>Plate/Tag No</label>
                  <input type="text" class="form-control" name="plate_or_tag_no" placeholder="Plate/Tag No" required autocomplete="off">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Heat/Series No</label>
                  <input type="text" class="form-control" name="heat_or_series_no" placeholder="Heat/Series No" required autocomplete="off">
                </div>
                <div class="form-group col-md-6">
                  <label>CEQ</label>
                  <input type="text" class="form-control" name="ceq" placeholder="CEQ" required autocomplete="off">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Mill Cert No</label>
                  <input type="text" class="form-control" name="mill_cert_no" placeholder="Mill Cert No" required autocomplete="off"> 
                </div>
                <div class="form-group col-md-6">
                  <label>Date Manufacturing</label>
                  <input type="text" class="form-control datepicker" name="date_manufacturing" placeholder="Date Manufacturing" required autocomplete="off">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Primary Mill / Country of Origin</label>
                  <input type="text" class="form-control" name="supplier_name" placeholder="Primary Mill / Country of Origin" required autocomplete="off"> 
                </div>
                <div class="form-group col-md-6">
                  <label>DO.PL No</label>
                  <input type="text" class="form-control" name="do_or_pl_no" placeholder="DO.PL No" required autocomplete="off">
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Unique Ident No</label>
                  <input type="text" class="form-control" name="unique_ident_no" id="unique_ident_no" placeholder="Unique Ident No" required autocomplete="off" onblur="check_unique_ident()">
                  <span id="text_alert" hidden></span>
                </div>
                <div class="form-group col-md-6">
                  <label>Qty</label>
                  <input type="number" class="form-control" name="qty" placeholder="Qty" required autocomplete="off">   
                </div>
              </div>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Uom</label>
                  <select name="uom" class="form-control select2">
                    <option value="Length">Length</option>
                    <option value="Meter">Meter</option>
                    <option value="Mili Meter">Mili Meter</option>
                    <option value="Square Meter">Square Meter</option>
                    <option value="Pcs">Pcs</option>
                    <option value="Each">Each</option>
                    <option value="Unit">Unit</option>
                    <option value="Sets">Sets</option>
                  </select> 
                </div>
                <div class="form-group col-md-6">
                  <label>Color Code</label>
                  <input type="text" class="form-control" name="color_code" placeholder="Color Code" required autocomplete="off"> 
                </div>
              </div>
            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <button class="btn btn-secondary" type="button" onclick="window.history.back();" title="Cancel"><i class="fa fa-close"></i> Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function check_unique_ident(){
    $("#text_alert").removeAttr("hidden");
    var uin = $('#unique_ident_no').val();

    if(uin == ""){
      document.getElementById("text_alert").style.color = "red";
      $('#text_alert').text('Unique Number is Required');
      $("#submitBtn").attr("disabled", true);
    }

    $.ajax({
      url: "<?= base_url() ?>mrir/check_unique_ident/" + uin,
      type: "post",
      success: function(data){
        if(data == 0){
          document.getElementById("text_alert").style.color = "green";
          $('#text_alert').text('This report number is available');
          $('#submitBtn').removeAttr("disabled");
        } else {
          document.getElementById("text_alert").style.color = "red";
          $('#text_alert').text('This unique number already exists in the database');
          $("#submitBtn").attr("disabled", true);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });
  }
</script>