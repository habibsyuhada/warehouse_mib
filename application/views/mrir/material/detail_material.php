<div id="content" class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar justify-content-between">
          <h5 class="pb-2 mb-0"><?php echo $meta_title ?></h5>
        </nav>
        <div class="my-3 p-3 bg-white rounded shadow-sm">

          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Detail Material</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Document</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <?php 
              foreach($q_material as $dt_material){ //FOREACH 1
                $unique = $dt_material->unique_ident_no;
            ?>
            <!-- CONTENT -->
            <form method="POST" action="<?php echo base_url();?>mrir/update_material_proccess">
              <div class="overflow-auto media text-muted py-3 mt-1 border-gray">
                <div class="container-fluid">
                  <input type="hidden" name="id" value="<?= $dt_material->id ?>">
                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label for="report_no">Report No</label>
                      <input type="text" class="form-control" name="report_no" placeholder="Report No" value="<?= $dt_material->report_no ?>" readonly>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Category</label>
                      <input type="text" class="form-control" id="category" name="category" placeholder="Category" value="<?= $dt_material->category ?>" autocomplete="off" required>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Discipline</label>
                      <select class="form-control select2" name="discipline">
                      <?php 
                        $str_discipline = $dt_material->discipline;
                        $str_spec_category = $dt_material->spec_category;

                        foreach($q_discipline as $dt_discipline){ //FOEACH 2
                      ?>
                        <option <?php echo ($str_discipline == $dt_discipline->id) ? "selected": "disabled" ?> value="<?= $dt_discipline->id ?>"><?= $dt_discipline->discipline_name ?></option>
                      <?php
                        }
                       ?>  
                      </select>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label>Description / Delivery Condition</label>
                      <textarea class="form-control" name="description" placeholder="Description / Delivery Condition" required autocomplete="off"><?= $dt_material->description ?></textarea>  
                    </div>
                  </div>
                </div>
              </div>

              <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-gray">
                <div class="container-fluid">
                  <h6 class="pb-2 mb-0" style="color: black">Size (in MM)</h6>

                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label>Length</label>
                      <input type="text" class="form-control" name="length" placeholder="Length" autocomplete="off" required value="<?= $dt_material->length ?>">
                    </div>
                    <div class="form-group col-md-4">
                      <label>Width / OD</label>
                      <input type="text" class="form-control" name="width_or_od" placeholder="Width / OD" required autocomplete="off" value="<?= $dt_material->width_or_od ?>">    
                    </div>
                    <div class="form-group col-md-4">
                      <label>SCH/THK</label>
                      <input type="text" class="form-control" name="sch_or_thk" placeholder="SCH/THK" required autocomplete="off" value="<?= $dt_material->sch_or_thk ?>"> 
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Spec / Grade</label>
                      <input type="text" class="form-control" name="spec" placeholder="Spec / Grade" required autocomplete="off" value="<?= $dt_material->spec ?>">
                    </div>

                   <div class="form-group col-md-6">
                  <label>Spec Category</label>
                  <select class="form-control select2" required name="spec_category">
                  <?php foreach($q_spec_category as $spec_category){ ?>
                    <option <?php echo ($str_spec_category == $spec_category->id) ? "selected": "disabled" ?> value="<?= $spec_category->id ?>"><?= $spec_category->spec_desc ?></option>
                  <?php } ?>
                  </select>
                </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Type</label>
                      <input type="text" class="form-control" name="type" placeholder="Type" required autocomplete="off" value="<?= $dt_material->type ?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Plate/Tag No</label>
                      <input type="text" class="form-control" name="plate_or_tag_no" placeholder="Plate/Tag No" required autocomplete="off" value="<?= $dt_material->plate_or_tag_no ?>">    
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Heat/Series No</label>
                      <input type="text" class="form-control" name="heat_or_series_no" placeholder="Heat/Series No" required autocomplete="off" value="<?= $dt_material->heat_or_series_no ?>">
                    </div>
                    <div class="form-group col-md-6">
                      <label>CEQ</label>
                      <input type="text" class="form-control" name="ceq" placeholder="CEQ" required autocomplete="off" value="<?= $dt_material->ceq ?>">
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Mill Cert No</label>
                      <input type="text" class="form-control" name="mill_cert_no" placeholder="Mill Cert No" required autocomplete="off" value="<?= $dt_material->mill_cert_no ?>"> 
                    </div>
                    <div class="form-group col-md-6">
                      <label>Date Manufacturing</label>
                      <input type="text" class="form-control datepicker" name="date_manufacturing" placeholder="Date Manufacturing" required autocomplete="off" value="<?= date('d-m-Y', strtotime($dt_material->date_manufacturing)) ?>">
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Primary Mill / Country of Origin</label>
                      <input type="text" class="form-control" name="supplier_name" placeholder="Primary Mill / Country of Origin" value="<?= $dt_material->supplier_name ?>" required autocomplete="off"> 
                    </div>
                    <div class="form-group col-md-6">
                      <label>DO.PL No</label>
                      <input type="text" class="form-control" name="do_or_pl_no" placeholder="DO.PL No" required autocomplete="off" value="<?= $dt_material->do_or_pl_no ?>">
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Unique Ident No</label>
                      <input type="text" class="form-control" name="unique_ident_no" id="unique_ident_no" placeholder="Unique Ident No" required autocomplete="off" value="<?= $dt_material->unique_ident_no ?>" readonly>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Qty</label>
                      <input type="number" class="form-control" name="qty" placeholder="Qty" required autocomplete="off" value="<?= $dt_material->qty ?>">   
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-6">
                      <label>Uom</label>
                      <?php 
                          $str_uom = $dt_material->uom;
                      ?>
                      <select name="uom" class="form-control select2">
                        <option <?php echo ($str_uom == 'Length') ? "selected": "" ?> value="Length">Length</option>
                        <option <?php echo ($str_uom == 'Meter') ? "selected": "" ?> value="Meter">Meter</option>
                        <option <?php echo ($str_uom == 'Mili Meter') ? "selected": "" ?> value="Mili Meter">Mili Meter</option>
                        <option <?php echo ($str_uom == 'Square Meter') ? "selected": "" ?> value="Square Meter">Square Meter</option>
                        <option <?php echo ($str_uom == 'Pcs') ? "selected": "" ?> value="Pcs">Pcs</option>
                        <option <?php echo ($str_uom == 'Each') ? "selected": "" ?> value="Each">Each</option>
                        <option <?php echo ($str_uom == 'Unit') ? "selected": "" ?> value="Unit">Unit</option>
                        <option <?php echo ($str_uom == 'Sets') ? "selected": "" ?> value="Sets">Sets</option>
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                      <label>Color Code</label>
                      <input type="text" class="form-control" name="color_code" placeholder="Color Code" required autocomplete="off" value="<?= $dt_material->color_code ?>"> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="text-right mt-3">
                <?php if($read_permission[23] == 1){ ?>
                <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-pen"></i> Update</button>
                <?php } ?>
                <a href="<?php echo base_url();?>mrir/detail_mrr/?report_number=<?= $dt_material->report_no ?>&discipline=<?= $dt_material->discipline ?>" class="btn btn-secondary" title="Submit"><i class="fa fa-close"></i> Cancel</a>
              </div>
            </form>
              <!-- TUTUP CONTENT -->
            <?php
              } //TUTUP FOREACH 1
            ?>
            </div>

            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <div class="overflow-auto media text-muted py-3 mt-1 border-gray">
                <div class="container-fluid">
                  <h6 style="color: black;">Add New Document</h6>
                  <form method="POST" action="<?= base_url() ?>mrir/add_file_material_proccess" enctype="multipart/form-data">
                    <div class="form-row">
                      <div class="form-group col-md-5">
                        <input type="hidden" name="document_unique" value="<?= $unique ?>">
                        <input type="text" class="form-control" name="document_name" id="document_name" placeholder="File Name" required autocomplete="off">
                      </div>
                      <div class="form-group col-md-5">
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file" id="file" required>
                            <label class="custom-file-label" for="file_1">Choose file</label>
                          </div>
                        </div>
                      </div>
                      <!-- <div class="form-group col-md-2">
                        <button class="btn btn-primary" type="button" onclick="add_more()"> Add More File</button>
                      </div> -->
                    </div>

                    <div id="more_file">

                    </div>
                     <?php if($read_permission[23] == 1){ ?>
                    <button class="btn btn-success">Submit</button>
                      <?php } ?>
                  </form>
                </div>
              </div>

              <?php  
                foreach($q_document_material as $document_material){
              ?>
              <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
                <div class="container-fluid">
                  
                  <div class="form-row">
                    <div class="form-group col-md-1">
                      <img src="<?= base_url() ?>img/pdf.svg" style="width: 35px;">
                    </div>
                    <div class="form-group col-md-10">
                      <label><?= $document_material->document_name ?></label>
                    </div>
                    <div class="form-group col-md-1">
                      <label><a target="_blank" href="<?= base_url() ?>upload/material_document/<?= $document_material->document_file ?>"><i class="fa fa-download"></i></a></label>
                    </div>
                  </div>

                </div>
              </div>
              <?php 
              }
              ?>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  var count = 1;

  $('#file').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      //replace the "Choose a file" label
      $(this).next('.custom-file-label').html(fileName);
  });

  function add_more(){
    count++;

    var html = "";

    html += '<div class="form-row" id=div_add_' + count + '>' +
            '<div class="form-group col-md-5">' +
            '<input type="text" class="form-control" name="document_name[]" id="file_name' + count +'" placeholder="File Name" required autocomplete="off">' +
            '</div>' +
            '<div class="form-group col-md-5">' +
            '<div class="input-group">' +
            '<div class="custom-file">' +
            '<input type="file" class="custom-file-input" name="file_material[]" id="file_1">' +
            '<label class="custom-file-label" for="file_1">Choose file</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="form-group col-md-2">' +
            '<button class="btn btn-danger" type="button" onclick="remove(' + count +')"> Remove Fields</button>' +
            '</div>' +
            '</div>';

    $('#more_file').append(html);

    console.log(count);
  }

  function remove(param){
    $('#div_add_' + param).remove();
  }

</script>