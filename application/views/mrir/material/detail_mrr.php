<?php 
  /*
    Title : Halaman MRIR DISCIPLINE
    Date  : 31 Juli 2019
  */
?>

  <div id="content" class="container-fluid overflow-auto">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm overflow-auto">
      	<nav class="navbar justify-content-between">
          <?php 
            if($this->input->get('discipline') == "1"){
              $str_discipline = "Piping";
            } else {
              $str_discipline = "Structure";
            }
          ?>
    			<h5 class="pb-2 mb-0"><?php echo $meta_title." | ".$str_discipline ?></h5>
        
    		</nav>
        
          <div class="overflow-auto media text-muted py-3 mt-1">
            <div class="container-fluid overflow-auto">

              <table class="table table-bordered datatable">
                <thead>
                  <tr bgcolor="#008060" style="color: white;text-align: center">
                    <th rowspan="2">Report Number</th>
                    <th rowspan="2">Unique Ident No.</th>
                    <th rowspan="2">PO No.</th>
                    <th rowspan="2">Vendor</th>
                    <th rowspan="2">Description</th>
                    <th colspan="3">Size</th>
                    <th rowspan="2">Spec</th>
                    <th rowspan="2">Spec Category</th>
                    <th rowspan="2">Type</th>
                    <th rowspan="2">Plate</th>
                    <th rowspan="2">Heat</th>
                    <th rowspan="2">CEQ</th>
                    <th rowspan="2">Mill Cert No.</th>
                    <th rowspan="2" style="width: 100px;">DO</th>
                    <th rowspan="2">Qty</th>
                    <th rowspan="2">Uom</th>
                    <th rowspan="2">Color Code</th>
                    <th rowspan="2">Action</th>
                  </tr>
                  <tr bgcolor="#008060" style="color: white; text-align: center;">
                  	<th>Length</th>
                  	<th>Width</th>
                  	<th>SCH/THK</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($query as $detail_mrr){ ?>
                	<tr>
                		<td><?= $detail_mrr->report_no ?></td>
                		<td><?= $detail_mrr->unique_ident_no ?></td>
                    <td><?php if($detail_mrr->po_number != ""){ echo $detail_mrr->po_number; } else { echo "-"; } ?></td>
                    <td><?php if($detail_mrr->vendor != ""){ echo $detail_mrr->vendor; } else { echo "-"; } ?></td>
                		<td><?= $detail_mrr->description ?></td>
                    
                		<td><?= $detail_mrr->length ?></td>
                		<td><?= $detail_mrr->width_or_od ?></td>
                		<td><?= $detail_mrr->sch_or_thk ?></td>
                    <td><?= $detail_mrr->spec ?></td>
                    <td><?= (isset($spec_category_list[$detail_mrr->spec_category]) ? $spec_category_list[$detail_mrr->spec_category] : '-') ?></td>
                		<td><?= $detail_mrr->type ?></td>
                    <td><?= $detail_mrr->plate_or_tag_no ?></td>
                    <td><?= $detail_mrr->heat_or_series_no ?></td>
                    <td><?= $detail_mrr->ceq ?></td>
                    <td><?= $detail_mrr->mill_cert_no ?></td>
                    <td><?= $detail_mrr->do_or_pl_no ?></td>
                    <td><?= $detail_mrr->qty ?></td>
                    <td><?= $detail_mrr->uom ?></td>
                    <td><?= $detail_mrr->color_code ?></td>
                    <td>
                      <button onclick="detail_material('<?= strtr($this->encryption->encrypt($detail_mrr->unique_ident_no),'+=/', '.-~') ?>')" class="btn btn-primary"> Detail</button>
                    </td>
                	</tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
         </div>  
        <div class="text-left mt-3">
          <a class='btn btn-warning' href='<?php echo base_url()."mrir/mrir_discipline/".strtr($this->encryption->encrypt($discipline),'+=/', '.-~'); ?>'><i class="fas fa-chevron-circle-left"></i> Back</a>
          <a href="<?= base_url() ?>mrir/export_mrir/<?= strtr($this->encryption->encrypt($this->input->get('report_no')),'+=/', '.-~') ?>" target="_blank"><button type="button" class="btn btn-danger"><i class="fa fa-file-pdf"></i> PDF</button></a>
       </div>
     </div>
   </div>
  </div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $('.datatable').dataTable({
    "lengthChange": false,
    "order": []
  });

  function detail_material(param){
    window.location = '<?= base_url() ?>mrir/detail_material/' + param;
  }
</script>