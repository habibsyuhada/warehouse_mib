  <div id="content" class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <center><a href="<?= base_url() ?>mrir/add_discipline"><button class="btn btn-success"><i class="fa fa-plus"></i> Add New</button></a></center>
              <br>
              <table class="table table-hover datatables">
                <thead>
                  <tr bgcolor="#008060" style="color: white">
                    <th>Discipline</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
<?php 
  foreach($query as $dt_discipline){
    if($dt_discipline->status == 1){
      $id = $dt_discipline->id;
      $status = "Active";
    }
?>
                  <tr>
                    <td><?= $dt_discipline->discipline_name.' ('.$dt_discipline->initial.') ' ?></td>
                    <td><?= $status ?></td>
                    <td>
                      <a href="<?= base_url() ?>mrir/edit_discipline/?id=<?= $id ?>"><button class="btn btn-primary"> Modify</button></a>
                    </td>
                  </tr>
<?php 
  }
?>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $('.datatables').dataTable();
</script>