<?php 
  /*
    Title : Halaman MRIR DISCIPLINE
    Date  : 31 Juli 2019
  */
?>

  <div id="content" class="container-fluid overflow-auto">
    <div class="row">
      <div class="col-md-12">
      	<nav class="navbar justify-content-between">
    			<h5 class="pb-2 mb-0"><?php echo $meta_title ?></h5>
    			<form class="form-inline">
            <a href="<?= base_url() ?>file/material/Template_import_material.xlsx" class='btn btn-primary'><i class="fas fa-cloud-download-alt" style="font-size:13px"></i> Template</a>
    			</form>
    		</nav>
        <div class="my-3 p-3 bg-white rounded shadow-sm overflow-auto">
          <div class="overflow-auto media text-muted py-3 mt-1">
            <div class="container-fluid overflow-auto">
              <form method="POST" action="<?= base_url() ?>mrir/import_material_proccess/<?= $report_no ?>" enctype="multipart/form-data">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="file"  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" class="custom-file-input" id="file_1" required>
                        <label class="custom-file-label" for="file_1">Choose file </label>
                      </div>
                    </div>
                    <input type="hidden" name="discipline" class="form-control" value="<?= $discipline ?>">
                  </div>
                  <div class="form-group col-md-6">
                    <button class="btn btn-success">Import</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function detail_material(param){
    window.open('<?= base_url() ?>mrir/detail_material/' + param);
  }

  $('#file_1').on('change',function(){
      //get the file name
      var fileName = $(this).val();
      //replace the "Choose a file" label
      $(this).next('.custom-file-label').html(fileName);
  });
</script>