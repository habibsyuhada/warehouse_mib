<?php 
  /*
    Title : Halaman MRIR DISCIPLINE
    Date  : 31 Juli 2019
  */
?>

  <div id="content" class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table table-hover datatable">
                <thead>
                  <tr bgcolor="#008060" style="color: white">
                    <th>#</th>
                    <th style="width: 500px;">Report Number</th>
                   
                    <th>Date Receiving</th>
                    <th>Status</th>
                    
                    <th>Action</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <?php  
                    $no = 1;
                    foreach($query as $dt_mrir){
                      if($dt_mrir->status == 0){
                        $status = "-";
                      } else {
                        $status = "Already Disposition Qc";
                      }
                  ?>
                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $dt_mrir->report_no ?></td>
                    
                    <td><?= date('d-m-Y', strtotime($dt_mrir->date_receiving)) ?></td>
                    <td><?= $discipline_name ?></td>
                    
                    <td>

                      <a href="<?= base_url() ?>mrir/detail_mrr/?report_number=<?= $dt_mrir->report_no ?>&discipline=<?= $dt_mrir->discipline ?>"><button class="btn btn-primary"> Detail</button></a>
                      <?php if($read_permission[23] == 1){ ?>
                          <?php if($dt_mrir->status == 0){ ?>
                              <a href="<?= base_url() ?>mrir/disposition/<?= strtr($this->encryption->encrypt($dt_mrir->report_no),'+=/', '.-~') ?>"><button class="btn btn-warning"> Disposition</button></a>
                          <?php } else { ?>
                              <button class="btn btn-warning" type="button" disabled> Disposition</button></a>
                          <?php } ?>
                      <?php } ?>
                    </td>
                  
                  </tr>
                  <?php 
                      $no++; 
                    }
                  ?>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $('.datatable').dataTable();
</script>