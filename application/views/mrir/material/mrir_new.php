<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrir/act_add_mrir">
    <input type="hidden" class="form-control" name="po_number" placeholder="PO Number" autocomplete="off" required>
    <input type="hidden" class="form-control" name="vendor" placeholder="Vendor" autocomplete="off" required>

    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <?php  echo $this->session->flashdata('message');?>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Report Number</label>
                  <input type="text" class="form-control" name="report_number" id="report_number" placeholder="Report Number" autocomplete="off" onblur="check_report_no()" required>
                  <span id="text_alert"></span>
                </div>
                <div class="form-group col-md-6">
                 <label>Discipline</label>
                  <select class="custom-select select2" name="discipline">
                  <?php foreach($q_discipline as $dt_discipline){ ?>
                    <option value="<?= $dt_discipline->id ?>"><?= $dt_discipline->discipline_name ?></option>
                  <?php } ?>
                  </select>
                  
                </div>
              </div>


              <div class="form-row">
                <div class="form-group col-md-12">
                  <label>Date Receiving</label>
                  <input type="text" class="form-control datepicker" data-zdp_readonly_element="false" name="date_receiving" autocomplete="off" required placeholder="Date : dd-mm-yyyy" value='<?php echo date("d-m-Y"); ?>'>
                </div>
              </div>
              
            </div>
          </div>
          <div class="text-right mt-3">

            <?php if($read_permission[21] == 1){ ?>
                <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <?php } ?>

            <a href="<?php echo base_url();?>mrir/mrir_discipline/p" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  function check_report_no(){
    
    $("#text_alert").removeAttr("hidden");
    var r_no = $('#report_number').val();

    if(r_no == ""){
      document.getElementById("text_alert").style.color = "red";
      $('#text_alert').text('Report Number is Required');
      $("#submitBtn").attr("disabled", true);
    } else {
      $.ajax({
        url: "<?= base_url() ?>mrir/check_report_no/",
        type: "post",
        data: {
          'r_no': r_no
        },
        success: function(data){
          if(data == 0){
            document.getElementById("text_alert").style.color = "green";
            $('#text_alert').text('This report number is available');
            $('#submitBtn').removeAttr("disabled");
          } else {
            document.getElementById("text_alert").style.color = "red";
            $('#text_alert').text('This report number already exists in the database');
            $("#submitBtn").attr("disabled", true);
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });
    }
  }
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
</script>