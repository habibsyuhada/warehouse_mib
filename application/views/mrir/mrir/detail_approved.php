<?php $mrir_list = $mrir_list[0]; ?>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>mrir/update_mrir_proccess">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">
              <input type="hidden" name="id" value="<?= $mrir_list['id'] ?>">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Report Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="report_no" value="<?= $mrir_list['report_no'] ?>" readonly required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">PO Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="po_number" value="<?= $mrir_list['po_number'] ?>" readonly required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Date Receiving</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="date_receiving" value="<?= date('d M Y', strtotime($mrir_list['created_date'])) ?>" readonly required>                
                </div>
              </div>

              <!-- <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="status" value="Open" readonly required>                
                </div>
              </div> -->

            </div>
          </div>

          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-gray">
            
            <table class="table table-bordered datatable text-center w-100">
              <thead class="bg-green-smoe" style="color: white; text-align: center;">
                <tr>
                  <th rowspan="2" style="vertical-align:middle;">Action</th>
                  <th colspan="2" style="vertical-align:middle;">MRIR Status<br/>( QTY )</th>
                  <th colspan="3" style="vertical-align:middle;">OSD Status<br/>( QTY )</th>
                 
                  <th rowspan="2" style="vertical-align:middle;">UOM</th>
                  <th colspan="3" style="vertical-align:middle;">MATERIAL</th>
                  <th rowspan="2" style="vertical-align:middle;width: 200px;">DEPARTMENT</th>

                  <th rowspan="2" style="vertical-align:middle;">BRAND</th>
                  <th rowspan="2" style="vertical-align:middle;">COLOR CODE</th>
                  <th rowspan="2" style="vertical-align:middle;">DATE RECEIVING</th>
                  <th rowspan="2" style="vertical-align:middle;">REMARKS</th>
                </tr>
                <tr>
                  <th style="vertical-align:middle;">Submitted</th>
                  <th style="vertical-align:middle;">Approved</th>

                  <th style="vertical-align:middle;">Damage</th>
                  <th style="vertical-align:middle;">Shortage</th>
                  <th style="vertical-align:middle;">Over</th>

                  <th style="vertical-align:middle;">Code</th>
                  <th style="vertical-align:middle;">Category</th>
                  <th style="vertical-align:middle;">Name</th>

                </tr>
              </thead>
              <tbody>
                <?php  
                  $no = 1;
                  $count = 0;
                  foreach($mrir_detail_list as $mrir_detail){
                ?>
                <input type="hidden" name="unique_mrir_detail[]" value="<?= $mrir_detail['unique_no'] ?>">
                <tr>
                  
                  <td style="vertical-align:middle;">
                    <center><b>
                    <?php if($mrir_detail['status'] == 3): ?>
                      <!-- <div class="form-check form-check-inline text-success"><label class="form-check-label">Approve</label></div> -->
                      <label class="form-check-label text-success">Approve</label>
                      <input type="hidden" class="form-control" name="approve[<?php echo $count; ?>]" value="ignored">
                    <?php elseif($mrir_detail['status'] == 2): ?>
                      <div class="form-check form-check-inline text-danger"><label class="form-check-label">Reject</label></div>
                    <?php else: ?>
                      <label class="form-check-label">Request</label>
                      <!-- <input style="height: 22px; width: 22px;" type="checkbox" name="approve[<?php echo $count; ?>]" value="A <?= $mrir_detail['id'] ?>"> -->
                    <?php endif ?>
                    </b><center>
                  </td>
                  <td style="vertical-align:middle;" class="qty_total"><?= $mrir_detail['qty'] ?></td>
                  <td style="vertical-align:middle;">
                    <input type="number" class="form-control qty_approve" name="qty_approve[<?php echo $count; ?>]" value="<?= ($mrir_detail['qty_approve'] == 0 && $mrir_detail['qty_demage'] == 0 ? $mrir_detail['qty'] : $mrir_detail['qty_approve']) ?>" min="0" max="<?= $mrir_detail['qty'] ?>" readonly oninput="change_qty(this)" <?php echo ($mrir_detail['status'] != 3 && $mrir_detail['status'] != 2 ? '' : 'readonly')  ?>>
                  </td>

                  <td style="vertical-align:middle;">
                    <input type="number" class="form-control qty_demage" name="qty_demage[<?php echo $count; ?>]" value="<?= ($mrir_detail['qty_approve'] == 0 && $mrir_detail['qty_demage'] == 0 ? 0 : $mrir_detail['qty_demage']) ?>" max="<?= $mrir_detail['qty'] ?>" readonly>
                    <input type="hidden" name="qty_over[<?php echo $count; ?>]" value="<?= $mrir_detail['qty_over'] ?>">
                    <input type="hidden" name="qty_shortage[<?php echo $count; ?>]" value="<?= $mrir_detail['qty_shortage'] ?>">
                    <input type="hidden" name="catalog_id[<?php echo $count; ?>]" value="<?= $mrir_detail['catalog_id'] ?>">
                  </td>
                  <td style="vertical-align:middle;" class="qty_shortage_v"><?= $mrir_detail['qty_shortage'] ?></td>
                  <td style="vertical-align:middle;" class="qty_over_v"><?= $mrir_detail['qty_over'] ?></td>
                  
                  <td style="vertical-align:middle;"><?= $mrir_detail['uom'] ?></td>
                  <td style="vertical-align:middle;">
                    <input type="hidden" class="form-control" name="id_material[<?php echo $count; ?>]" value="<?= $mrir_detail['id'] ?>">
                    <?= $mrir_detail['code_material'] ?>
                  </td>
                  <td><?= $mrir_detail['catalog_category'] ?></td>
                  <td><?= $mrir_detail['material'] ?></td>
                  <td><?= $mrir_detail['name_of_department'] ?></td>
                  <td><?= $mrir_detail['brand'] ?></td>
                  <td><?= $mrir_detail['color_code'] ?></td>
                  <td><?= $mrir_detail['date_manufacturing'] ?></td>
                  <td style="vertical-align:middle;">
                    <?= $mrir_detail['remarks'] ?>
                    <input type="hidden" class="form-control" name="unique_ident_no[<?php echo $count; ?>]" value="<?= $mrir_detail['unique_ident_no'] ?>">
                    <input type="hidden" class="form-control" name="qty[<?php echo $count; ?>]" value="<?= $mrir_detail['qty'] ?>">
                  </td>
                </tr>
                <?php 
                    $no++;
                    $count++;
                  }
                ?>
              </tbody>
            </table>

          </div>
          <div class="mt-3">
            <?php if($read_permission[42] == 1){ ?>
              <!-- <a href="<?= base_url() ?>mrir/export_mrir_excel/<?= strtr($this->encryption->encrypt($mrir_list['id']),'+=/', '.-~') ?>" target="_blank"><button type="button" class="btn btn-success"><i class="fa fa-file-excel"></i> Excel</button></a> -->
            <!-- <a href="<?= base_url() ?>mrir/export_mrir/<?= strtr($this->encryption->encrypt($mrir_list['id']),'+=/', '.-~') ?>" target="_blank"><button type="button" class="btn btn-danger"><i class="fa fa-file-pdf"></i> PDF</button></a> -->
            <?php } ?>
            <button type="button" onclick="window.history.go(-1)" class="btn btn-secondary"><i class="fa fa-close"></i> Close</button>
            <!-- <a href="<?php echo base_url();?>mrir/mrir_data/approved" class="btn btn-secondary" title="Submit"><i class="fa fa-close"></i> Back</a> -->
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function change_qty(input) {
    var qty_demage = $(input).closest('tr').find('.qty_demage');
    var qty_approve = $(input).closest('tr').find('.qty_approve');
    var qty_total = $(input).closest('tr').find('.qty_total');

    var qty_osd = $(qty_total).text() - $(qty_approve).val();
   

    var qty_submitted = $(qty_total).text();
    var qty_approved = Number($(qty_approve).val());

    console.log(qty_submitted + " V/S " +qty_approved);

    if(qty_approved >= 0 && qty_approved <= qty_submitted){
      $(qty_demage).val(qty_osd);
    } else {
        Swal.fire(
          'Warning!',
          'Please check your Approved Qty',
          'warning'
        );
         $(qty_approve).val($(qty_total).text());
         $(qty_demage).val(0);
    }
  }
</script>