<div id="content" class="container-fluid">  

  <div class="row">
    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-gray">
          <div class="container-fluid">
            <h6 style="color: black;">Add New Document</h6>
            <form method="POST" action="<?= base_url() ?>mrir/add_file_material_proccess" enctype="multipart/form-data">
              
              <div class="form-row">
                <div class="form-group col-md-5">
                  <input type="hidden" name="document_unique" value="<?= $material->unique_ident_no ?>">
                  <input type="text" class="form-control" name="document_name" id="document_name" placeholder="File Name" required autocomplete="off">
                </div>
                <?php if($read_permission[38] == 1){ ?>
                <div class="form-group col-md-5">
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="file" id="file" required>
                      <label class="custom-file-label" for="file_1">Choose file</label>
                    </div>
                  </div>
                </div>
                <?php } ?>
                <!-- <div class="form-group col-md-2">
                  <button class="btn btn-primary" type="button" onclick="add_more()"> Add More File</button>
                </div> -->
              </div>
              

              <div id="more_file">

              </div>
              <?php if($read_permission[38] == 1){ ?>
                <button class="btn btn-success">Submit</button>
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-gray">
          <div class="container-fluid">
            
            <table class="table table-hover datatables text-center align-middle">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Document Name</th>
                  <th>Upload Date</th>
                  <th>Attachment</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($document_list as $key => $document): ?>
                  <tr>
                    <td><?php echo $key+1 ?></td>
                    <td><?php echo $document->document_name ?></td>
                    <td><?php echo date('N, F j Y - H:i:s', strtotime($document->timestamp)) ?></td>
                    <td><a target="_blank" href="<?= base_url() ?>upload/material_document/<?php echo $document->document_file ?>" class="btn btn-dark btn-flat"><i class="fas fa-file-pdf"></i></a></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>

</div> 

</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $(document).ready(function() {
    $('.datatables').DataTable({
      order:[]
    });
  });
</script>