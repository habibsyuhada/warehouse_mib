<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrir/update_mrir_proccess">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">
              <?php 
                foreach($query as $dt_mrir){
                  $str_discipline = $dt_mrir->discipline;

                  if($str_discipline == "p"){
                    $str = "Piping";
                  } else {
                    $str = "Structure";
                  }

                  if($dt_mrir->status == 1){
                    $str_status = "Submitted";
                  } else if($dt_mrir->status == 2){
                    $str_status = "Pending";
                  } else {
                    $str_status = "Approved";
                  }
              ?>
              <input type="hidden" name="id" value="<?= $dt_mrir->id ?>">
              <input type="hidden" name="discipline" value="$dt_mrir->discipline">
              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Project ID</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="project_id" value="<?= $dt_mrir->project_id ?>" readonly required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Report Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="report_no" value="<?= $dt_mrir->report_no ?>" readonly required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Discipline</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="discipline" value="<?= $dt_mrir->report_no ?>" readonly required>                
                </div>
              </div>

              <!-- <div class="form-group row">
                <label class="col-sm-2 col-form-label">PO Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="po_no" value="<?= $dt_mrir->po_no ?>" readonly required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Vendor</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="vendor" value="<?= $dt_mrir->vendor ?>" readonly required>                
                </div>
              </div> -->

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Date Receiving</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="date_receiving" value="<?= $dt_mrir->date_receiving ?>" readonly required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Discipline</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="discipline" value="<?= $str ?>" readonly required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                  <input type="text" name="status" class="form-control" readonly value="<?= $str_status ?>">         
                </div>
              </div>   
            <?php
              } //TUTUP FOREACH
            ?>
            </div>
          </div>
          <div class="mt-3">
            <a href="<?php echo base_url();?>mrir/mrir_data/approved" class="btn btn-secondary" title="Submit"><i class="fa fa-close"></i> Back</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
</script>