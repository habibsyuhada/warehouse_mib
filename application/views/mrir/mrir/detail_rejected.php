<div id="content" class="container-fluid">
  <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">
            <?php 
              foreach($query as $dt_mrir){
                $category = $dt_mrir->category;
                $str_report_no = $dt_mrir->report_no;
                $str_discipline = $dt_mrir->discipline;
                $str_project = $this->Mrir_mod->search_project_data($dt_mrir->project_id);

                if($str_discipline == "p"){
                  $str = "Piping";
                } else {
                  $str = "Structure";
                }

                if($dt_mrir->status == 1){
                  $str_status = "Submitted";
                } else if($dt_mrir->status == 2){
                  $str_status = "Pending";
                } else {
                  $str_status = "Approved";
                }
            ?>
            <input type="hidden" name="id" value="<?= $dt_mrir->id ?>">
            <input type="hidden" name="discipline" value="$dt_mrir->discipline">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Project ID</label>
              <div class="col-sm-10">
                <input type="hidden" class="form-control" name="project_id" value="<?= $dt_mrir->project_id ?>" readonly required> 
                   <input type="text" class="form-control" value="<?php echo $project[0]['project_name']; ?>" readonly required>               
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Report Number</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="report_no" value="<?= $dt_mrir->report_no ?>" readonly required>                
              </div>
            </div>

            <?php if($category == 'SS'): ?>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Vendor</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="vendor" value="<?= $vendor_list[$dt_mrir->vendor] ?>"" readonly required>                
              </div>
            </div>
            <?php endif; ?>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">PO Number</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="po_number" value="<?= $dt_mrir->po_number ?>" readonly required>                
              </div>
            </div>
              
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Status</label>
              <div class="col-sm-10">
                <input type="text" name="status" class="form-control" readonly value="<?= $str_status ?>">         
              </div>
            </div>   
            <?php
              } //TUTUP FOREACH
            ?>
          </div>
        </div>

        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-gray">
          
          <table class="table table-bordered datatable">
              <thead>
                <tr bgcolor="#008060" style="color: white; text-align: center;">
                  <!-- <th rowspan="2" style="vertical-align:middle;">Doc</br>Mat</th> -->
                  <th rowspan="2" style="vertical-align:middle;">Status</th>
                  <?php if($category == 'SS'): ?>
                  <th rowspan="2" style="vertical-align:middle;">DO No./ PL No.</th>
                  <?php endif; ?>
                  <th rowspan="2" style="vertical-align:middle;">Unique Ident No.</th>
                  
                  <!-- <th rowspan="2" style="vertical-align:middle;">Cat</th> -->
                  <th rowspan="2" style="vertical-align:middle;">Material Code</th>
                  <th rowspan="2" style="vertical-align:middle;">Description</th>
                  <th rowspan="2" style="vertical-align:middle;">Qty</th>
                  <th rowspan="2" style="vertical-align:middle;">UoM</th>
                  <?php if($category == 'CS'): ?>
                  <th rowspan="2" style="vertical-align:middle;">Vendor</th>
                  <?php endif; ?>
                  <th rowspan="2" style="vertical-align:middle;">Plate / Tag No.</th>
                  <th rowspan="2" style="vertical-align:middle;">Heat / Lot No.</th>
                  <th colspan="3" style="vertical-align:middle;">Size (In MM)</th>
                  <th rowspan="2" style="vertical-align:middle;">Material Grade</th>
                  <th rowspan="2" style="vertical-align:middle;">Spec Category</th>
                  <th rowspan="2" style="vertical-align:middle;">Type</th>
                  <!-- <th rowspan="2" style="vertical-align:middle; width: 150px;">ceq</th> -->
                  <th rowspan="2" style="vertical-align:middle;">Mill Cert No</th>
                  <th rowspan="2" style="vertical-align:middle;">Date Manufacturing</th>
                  <th rowspan="2" style="vertical-align:middle;">Primary Mill / Country of Origin</th>
                  <th rowspan="2" style="vertical-align:middle;">Color Code</th>                  
                  <th rowspan="2" style="vertical-align:middle;"></th>                  
                </tr>
                <tr bgcolor="#008060" style="color: white; text-align: center;">
                  <th style="vertical-align:middle;">Length</th>
                  <th style="vertical-align:middle;">Width</th>
                  <th style="vertical-align:middle;">SCH/THK</th>
                </tr>
              </thead>
              <tbody>
                <?php  
                  $no = 1;
                  foreach($q_mrr as $detail_mrr){
                     if($detail_mrr->status == 3) {
                      $str_approval = "<span style='color:green;'>Approved</span>";
                    } else {
                     if($detail_mrr->remarks != ''){
                        $str_approval = "<span style='color:red;'>Pending </span></br> ( ".$detail_mrr->remarks." )";
                      } else {
                         $str_approval = "<span style='color:red;'>Pending </span>";
                      }
                    }
                ?>
                <tr>
                  <!-- <td style="vertical-align:middle;">
                     <center>
                      <?php if($detail_mrr->document_file != ""){ ?>
                      <a target="_blank" href="<?= base_url() ?>upload/material_document/<?= $detail_mrr->document_file ?>">
                        <img src="<?= base_url() ?>img/pdf.svg" style="width: 35px;">
                      </a>
                      <?php } else { echo "-"; } ?>
                     </center>
                  </td> -->
                  <td style="vertical-align:middle;font-weight: bold;width: 200px;text-align: center;">
                   <?= $str_approval ?>
                  </td>
                  <?php if($category == 'SS'): ?>
                  <td style="vertical-align:middle;"><?= $detail_mrr->do_or_pl_no ?></td>
                  <?php endif; ?>
                  <td style="vertical-align:middle;"><?= $detail_mrr->unique_ident_no ?></td>
                  <!-- <td style="vertical-align:middle;"><?= $detail_mrr->category ?></td> -->
                  <td style="vertical-align:middle;"><?= $catalog_list[$detail_mrr->catalog_id]['code_material'] ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->description ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->qty ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->uom ?></td>
                  <?php if($category == 'CS'): ?>
                    <td style="vertical-align:middle;"><?= $vendor_list[$detail_mrr->vendor] ?></td>
                  <?php endif; ?>
                  <td style="vertical-align:middle;"><?= $detail_mrr->plate_or_tag_no ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->heat_or_series_no ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->length ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->width_or_od ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->sch_or_thk ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->spec ?></td>
                      <td style="vertical-align:middle;"><?= $detail_mrr->spec_category ?></td>
                  <!-- TYPE -->
                  <td style="vertical-align:middle;"><?= $detail_mrr->type ?></td>

                  <!-- ceq -->
                  <!-- <td style="vertical-align:middle;"><?= $detail_mrr->ceq ?></td> -->
                  <td style="vertical-align:middle;"><?= $detail_mrr->mill_cert_no ?></td>

                  <!-- DATE MANUFACTURING -->
                  <td style="vertical-align:middle;"><?PHP if($detail_mrr->date_manufacturing !== '0000-00-00'){ echo date('d-m-Y', strtotime($detail_mrr->date_manufacturing)); } else { echo "-"; } ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->supplier_name ?></td>
                  <td style="vertical-align:middle;"><?= $detail_mrr->color_code ?></td>
                  <td style="vertical-align:middle;"><a href="<?php echo base_url() ?>mrir/detail_material/<?= strtr($this->encryption->encrypt($detail_mrr->unique_ident_no),'+=/', '.-~') ?>" class="btn btn-info text-nowrap"><i class="fa fa-search"></i> Detail</a></td>
                  
                  
                </tr>
                <?php 
                    $no++;
                  }
                ?>
              </tbody>
            </table>

        </div>
        <div class="mt-3">
              <?php if($read_permission[21] == 1 AND $read_permission[23] == 1){ ?>
                <a href="<?= base_url() ?>mrir/re_submit/<?= $str_report_no ?>"><button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit">Re-Submit</button></a>
              <?php } ?>
               <?php if($read_permission[27] == 1){ ?>
            <a href="<?= base_url() ?>mrir/export_mrir/<?= strtr($this->encryption->encrypt($this->input->get('report_no')),'+=/', '.-~') ?>" target="_blank"><button type="button" class="btn btn-danger"><i class="fa fa-file-pdf"></i> PDF</button></a>
            <?php } ?>
              <a href="<?php echo base_url();?>mrir/mrir_data/rejected" class="btn btn-secondary" title="Submit"><i class="fa fa-close"></i> Back</a>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
</script>