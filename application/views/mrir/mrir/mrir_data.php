  <div id="content" class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
              <table class="table table-hover text-center" id="mrir_dt">
              <thead class="bg-green-smoe text-white">
                  <tr >
                    <th>Report Number</th>
                    <th>PO Number</th>
                    <th>Date Receiving</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $('#mrir_dt').DataTable({
    "language": { 
      "infoFiltered": "" 
    },
    "paging": true,
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax": {
      "url": "<?php echo base_url();?>mrir/mrir_list_json/<?= $param ?>",
      "type": "POST",
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": true,
    }, ],
  });  
</script>