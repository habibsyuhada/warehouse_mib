<?php 
  // print_r($sheet);exit;
?>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrir/insert_mrir_data">
    <div class="row">
      <div class="col-md-10">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

               <input type='hidden' name='project_id' class='form_control' value='<?php echo $get_drawing_data[0]["project_id"]; ?>' readonly>
               <input type='hidden' name='report_no' class='form_control' value='<?php echo $get_mrir_no; ?>' readonly>
               <input type='hidden' name='do_no' class='form_control' value='<?php echo $material[0]["do_pl"]; ?>' readonly>
               <input type='hidden' name='vendor' class='form_control' value='<?php echo $material[0]["vendor"]; ?>' readonly>
               <input type='hidden' name='date_receiving' class='form_control' value='<?php echo $material[0]["date_received"]; ?>' readonly>
               <input type='hidden' name='discipline' class='form_control' value='<?php echo $get_drawing_data[0]["discipline"]; ?>' readonly>
               <input type='hidden' name='comments' class='form_control' value='Disposition To QC Inspector' readonly>
               <input type='hidden' name='created_date' class='form_control' value='<?php echo date("Y-m-d"); ?>' readonly>
               <input type='hidden' name='status' class='form_control' value='0' readonly>



              <table class="table dataTable table-hover text-center">
                <thead class="bg-success text-white">
                  <tr>
                    <th rowspan="2" style="vertical-align:middle;">DO No./ PL No.</th>
                    <th rowspan="2" style="vertical-align:middle;">Unique Ident No.</th>
                    <th rowspan="2" style="vertical-align:middle;">Cat</th>                  
                    <th rowspan="2" style="vertical-align:middle;">PO No</th>
                    <th rowspan="2" style="vertical-align:middle;">Vendor Name</th>
                    <th rowspan="2" style="vertical-align:middle;">Description / Delivery Condition</th>
                    <th colspan="3" style="vertical-align:middle;">Size (In MM)</th>
                    <th rowspan="2" style="vertical-align:middle;">Material Grade</th>
                    <th rowspan="2" style="vertical-align:middle;">Material Class</th>
                    <th rowspan="2" style="vertical-align:middle;">Type</th>
                    <th rowspan="2" style="vertical-align:middle;">Plate / Tag No.</th>
                    <th rowspan="2" style="vertical-align:middle;">Heat / Lot No.</th>
                    <!-- <th rowspan="2" style="vertical-align:middle; width: 150px;">Ceq</th> -->
                    <th rowspan="2" style="vertical-align:middle;">Mill Cert No</th>
                    <th rowspan="2" style="vertical-align:middle;">Date Manufacturing</th>
                    <th rowspan="2" style="vertical-align:middle;">Primary Mill / Country of Origin</th>                 
                    <th rowspan="2" style="vertical-align:middle;">Qty</th>
                    <th rowspan="2" style="vertical-align:middle;">UoM</th>
                    <th rowspan="2" style="vertical-align:middle;">Color Code</th>                  
                  </tr>
                  <tr style="color: white; text-align: center;">
                    <th style="vertical-align:middle;">Length</th>
                    <th style="vertical-align:middle;">Width</th>
                    <th style="vertical-align:middle;">Sch/Thk</th>                    
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    foreach ($material as $key => $value) { 

                      $status   = '';
                      $disabled = 0;
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td style="vertical-align:middle;">
                      <input type='text' name='do_pl[]' class='form_control' value='<?php echo $value["do_pl"]; ?>' readonly>
                      <input type='hidden' name='brand[]' class='form_control' value='<?php echo $value["brand"]; ?>' readonly>
                      <input type='hidden' name='supplier_name[]' class='form_control' value='<?php echo $value["supplier_name"]; ?>' readonly>
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='uniq_no[]' class='form_control' value='<?php echo $value["uniq_no"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='category[]' class='form_control' value='<?php echo $value["category"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='po_item[]' class='form_control' value='<?php echo $value["po_item"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='vendorx[]' class='form_control' value='<?php echo $value["vendor"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='material[]' class='form_control' value='<?php echo $value["material"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='length[]' class='form_control' value='<?php echo $value["length"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='width_od[]' class='form_control' value='<?php echo $value["width_od"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='sch_thk[]' class='form_control' value='<?php echo $value["sch_thk"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='spec[]' class='form_control' value='<?php echo $value["spec"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='spec_category[]' class='form_control' value='<?php echo $value["spec_category"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='type[]' class='form_control' value='<?php echo $value["type"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='plate_or_tag_no[]' class='form_control' value='<?php echo $value["plate_or_tag_no"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='heat_no' class='form_control' value='<?php echo $value["heat_no"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <!-- <input type='text' name='ceq[]' class='form_control' value='<?php //echo $value["ceq"]; ?>' readonly style="background-color: #e9ecef"> -->
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='mill_certificate[]' class='form_control' value='<?php echo $value["mill_certificate"]; ?>' readonly style="background-color: #e9ecef">
                    </td>   
                    <td style="vertical-align:middle;">
                      <input type='text' name='date_manufacturing[]' class='form_control' value='<?php echo $value["date_manufacturing"]; ?>' readonly style="background-color: #e9ecef">
                    </td>               
                    <td style="vertical-align:middle;">
                      <input type='text' name='country_origin[]' class='form_control' value='<?php echo $value["country_origin"]; ?>' readonly style="background-color: #e9ecef">
                    </td>  

                    <td style="vertical-align:middle;">
                      <input type='text' name='qty[]' class='form_control' value='<?php echo $value["qty"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='uom[]' class='form_control' value='<?php echo $value["uom"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                    <td style="vertical-align:middle;">
                      <input type='text' name='color_code[]' class='form_control' value='<?php echo $value["color_code"]; ?>' readonly style="background-color: #e9ecef">
                    </td>                  
                      
                  </tr> 
                  <?php } ?>               
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-left mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit">
              <i class="fa fa-check"></i> Submit
            </button>
            <a href="<?php echo base_url();?>receiving_detail/<?= strtr($this->encryption->encrypt($material[0]['receiving_id']), '+=/', '.-~') ?>" class="btn btn-secondary " title="Submit">
              <i class="fa fa-close"></i> Cancel
            </a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->