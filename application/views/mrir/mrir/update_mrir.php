<?php $mrir_list = $mrir_list[0]; ?>
<style>
.table_ss {
  font-size: 15px !important;
  width: 1000 !important;
}

/* The container */
.containerx {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.containerx input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.containerx:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.containerx input:checked ~ .checkmark {
  background-color: #007339;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.containerx input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.containerx .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>

<style>

.radio-toolbar input[type="radio"] {
  display: none;
}

.radio-toolbar label {
  display: inline-block;
  background-color: #e3e3e3;
  padding: 4px 11px;
  font-family: Arial;
  font-size: 16px;
  cursor: pointer;
}

.radio-toolbar input[type="radio"]:checked+label {
  background-color: #e3e3e3;
}

</style>

<div id="content" class="container-fluid">  
  <div class="row">
    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
          <div class="container-fluid">

          <form method="POST" action="<?php echo base_url();?>mrir/update_status_mrir_proccess">

            <input type="hidden" name="mrir_id" value="<?= $mrir_list['id'] ?>">

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Report Number</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="report_no" value="<?= $mrir_list['report_no'] ?>" readonly required>                
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">PO Number</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="po_number" value="<?= $mrir_list['po_number'] ?>" readonly required>                
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Date Receiving</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="date_receiving" value="<?= date('d M Y', strtotime($mrir_list['created_date'])) ?>" readonly required>                
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Status</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="status" value="Open" readonly required>                
              </div>
            </div>

            <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
              <div class="container-fluid">

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <div class="radio-toolbar">

                  <div class="form-check form-check-inline text-success" onclick="check_all_btn()">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="approval" value="0" style="width: 17px; height: 17px">
                    <b>Check All</b></label>
                  </div>
                  <!-- <div class="form-check form-check-inline text-danger">
                    <label class="form-check-label">
                      <input class="form-check-input" type="radio" name="approval" value="1" style="width: 17px; height: 17px">
                    <b>Reject All</b></label>
                  </div> -->
                  <div class="form-check form-check-inline text-secondary" onclick="uncheck_all_btn()">
                    <label class="form-check-label"><input class="form-check-input" type="radio" name="approval" value="3" style="width: 17px; height: 17px">
                   <b>Uncheck All</b></label>
                  </div>
                </div>
                </br>

                <table class="table table-bordered datatable text-center w-100">
                  <thead class="bg-green-smoe" style="color: white; text-align: center;">
                    <tr>
                      <th rowspan="2" style="vertical-align:middle;">Action</th>
                      <th colspan="2" style="vertical-align:middle;">MRIR Status<br/>( QTY )</th>
                      <th colspan="3" style="vertical-align:middle;">OSD Status<br/>( QTY )</th>
                     
                      <th rowspan="2" style="vertical-align:middle;">UOM</th>
                      <th colspan="3" style="vertical-align:middle;">MATERIAL</th>
                      <th rowspan="2" style="vertical-align:middle;width: 200px;">DEPARTMENT</th>

                      <th rowspan="2" style="vertical-align:middle;">BRAND</th>
                      <th rowspan="2" style="vertical-align:middle;">COLOR CODE</th>
                      <th rowspan="2" style="vertical-align:middle;">DATE RECEIVING</th>
                      <th rowspan="2" style="vertical-align:middle;">REMARKS</th>
                    </tr>
                    <tr>
                      <th style="vertical-align:middle;">Submitted</th>
                      <th style="vertical-align:middle;">Approved</th>

                      <th style="vertical-align:middle;">Damage</th>
                      <th style="vertical-align:middle;">Shortage</th>
                      <th style="vertical-align:middle;">Over</th>

                      <th style="vertical-align:middle;">Code</th>
                      <th style="vertical-align:middle;">Category</th>
                      <th style="vertical-align:middle;">Name</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php  
                      $no = 1;
                      $count = 0;
                      foreach($mrir_detail_list as $mrir_detail){
                    ?>
                    <input type="hidden" name="unique_mrir_detail[]" value="<?= $mrir_detail['unique_no'] ?>">
                    <tr>
                      
                      <td style="vertical-align:middle;">
                        <center><b>
                        <?php if($mrir_detail['status'] == 3): ?>
                          <!-- <div class="form-check form-check-inline text-success"><label class="form-check-label">Approve</label></div> -->
                          <label class="form-check-label text-success">Approve</label>
                          <input type="hidden" class="form-control" name="approve[<?php echo $count; ?>]" value="ignored">
                        <?php elseif($mrir_detail['status'] == 2): ?>
                          <div class="form-check form-check-inline text-danger"><label class="form-check-label">Reject</label></div>
                        <?php else: ?>
                          <input style="height: 22px; width: 22px;" type="checkbox" name="approve[<?php echo $count; ?>]" value="A <?= $mrir_detail['id'] ?>">
                        <?php endif ?>
                        </b><center>
                      </td>
                      <td style="vertical-align:middle;" class="qty_total"><?= $mrir_detail['qty'] ?></td>
                      <td style="vertical-align:middle;">
                        <input type="number" class="form-control qty_approve" name="qty_approve[<?php echo $count; ?>]" value="<?= ($mrir_detail['qty_approve'] == 0 && $mrir_detail['qty_demage'] == 0 ? $mrir_detail['qty'] : $mrir_detail['qty_approve']) ?>" min="0" max="<?= $mrir_detail['qty'] ?>" oninput="change_qty(this)" <?php echo ($mrir_detail['status'] != 3 && $mrir_detail['status'] != 2 ? '' : 'readonly')  ?>>
                      </td>

                      <td style="vertical-align:middle;">
                        <input type="number" class="form-control qty_demage" name="qty_demage[<?php echo $count; ?>]" value="<?= ($mrir_detail['qty_approve'] == 0 && $mrir_detail['qty_demage'] == 0 ? 0 : $mrir_detail['qty_demage']) ?>" max="<?= $mrir_detail['qty'] ?>" readonly>
                        <input type="hidden" name="qty_over[<?php echo $count; ?>]" value="<?= $mrir_detail['qty_over'] ?>">
                        <input type="hidden" name="qty_shortage[<?php echo $count; ?>]" value="<?= $mrir_detail['qty_shortage'] ?>">
                        <input type="hidden" name="catalog_id[<?php echo $count; ?>]" value="<?= $mrir_detail['catalog_id'] ?>">
                      </td>
                      <td style="vertical-align:middle;" class="qty_shortage_v"><?= $mrir_detail['qty_shortage'] ?></td>
                      <td style="vertical-align:middle;" class="qty_over_v"><?= $mrir_detail['qty_over'] ?></td>
                      
                      <td style="vertical-align:middle;"><?= $mrir_detail['uom'] ?></td>
                      <td style="vertical-align:middle;">
                        <input type="hidden" class="form-control" name="id_material[<?php echo $count; ?>]" value="<?= $mrir_detail['id'] ?>">
                        <?= $mrir_detail['code_material'] ?>
                      </td>
                      <td><?= $mrir_detail['catalog_category'] ?></td>
                      <td><?= $mrir_detail['material'] ?></td>
                      <td><?= $mrir_detail['name_of_department'] ?></td>
                      <td><?= $mrir_detail['brand'] ?></td>
                      <td><?= $mrir_detail['color_code'] ?></td>
                      <td><?= $mrir_detail['date_manufacturing'] ?></td>
                      <td style="vertical-align:middle;">
                        <textarea class="form-control" name="remarks[]"></textarea>
                        <!-- <?= $mrir_detail['remarks'] ?> -->
                        <input type="hidden" class="form-control" name="unique_ident_no[<?php echo $count; ?>]" value="<?= $mrir_detail['unique_ident_no'] ?>">
                        <input type="hidden" class="form-control" name="qty[<?php echo $count; ?>]" value="<?= $mrir_detail['qty'] ?>">
                      </td>
                    </tr>
                    <?php 
                        $no++;
                        $count++;
                      }
                    ?>
                  </tbody>
                </table>

              </div>
            </div>
            <div class="mt-3">
              <input type="hidden" name="module" value="mrir">
              <input type="hidden" name="user_id" value="<?php echo $read_cookies['0'] ?>">



              <?php if($read_permission[38] == 1){ ?>

              <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>

              <?php } ?>
              <!-- <a href="<?= base_url() ?>mrir/export_mrir_inspection/<?= strtr($this->encryption->encrypt($mrir_detail->report_no),'+=/', '.-~') ?>" target="_blank"><button type="button" class="btn btn-danger"><i class="fa fa-file-pdf"></i> Export PDF</button></a> -->

              <button type="button" onclick="window.history.go(-1)" class="btn btn-secondary"><i class="fa fa-close"></i> Close</button>
              <!-- <a href="<?php echo base_url();?>mrir/mrir_data/request" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Close</a> -->
            </div>

          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 

</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });


  

  $(document).ready(function(){

    // $('input[name="approval"]').click(function(){
    //   var approve_val = $(this).val();
      
    //   if(approve_val == 0){

    //     $('.reject').removeAttr('checked');
    //     $('.approve').prop('checked', true);

    //   } else if(approve_val == 1){

    //     $('.approve').removeAttr('checked');
    //     $('.reject').prop('checked', true);

    //   } else {
    //     console.log(approve_val);
    //    $('.approve').prop('checked', false);
    //    $('.reject').prop('checked', false);
    //   }
    // });

    // $('input[type="checkbox"]').click(function(){
    //     var id = $(this).val();

    //     if($(this).prop("checked") == true){
    //       var url = '<?= base_url() ?>mrir/approve_unapprove_material/';
    //       var _val = 3; //untuk deteksi valuenya checked atau tidak

    //     } else if($(this).prop("checked") == false){
    //       var url = '<?= base_url() ?>mrir/approve_unapprove_material/';
    //       var _val = 1; //untuk deteksi valuenya checked atau tidak
    //     }

    //     action_material(url,id,_val);
    // });
  });

  function action_material(url,id,_val){
    $.ajax({
      url: url,
      type: "post",
      data: {
        'id' : id,
        'val' : _val
      },
      success: function(data){

      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });
  }

  function change_demage(input, qty) {
    var input_demage = $(input).closest('table').closest('tr').find('.qty_demage');
    input_demage.val(qty);    
  }

  function change_qty(input) {
    var qty_demage = $(input).closest('tr').find('.qty_demage');
    var qty_approve = $(input).closest('tr').find('.qty_approve');
    var qty_total = $(input).closest('tr').find('.qty_total');

    var qty_osd = $(qty_total).text() - $(qty_approve).val();
   

    var qty_submitted = $(qty_total).text();
    var qty_approved = Number($(qty_approve).val());

    console.log(qty_submitted + " V/S " +qty_approved);

    if(qty_approved >= 0 && qty_approved <= qty_submitted){
      $(qty_demage).val(qty_osd);
    } else {
        Swal.fire(
          'Warning!',
          'Please check your Approved Qty',
          'warning'
        );
         $(qty_approve).val($(qty_total).text());
         $(qty_demage).val(0);
    }
  }


  function check_all_btn() {
    $('input[type=checkbox]').prop('checked', true);
  }

  function uncheck_all_btn() {
    $('input[type=checkbox]').prop('checked', false);
  }

</script>