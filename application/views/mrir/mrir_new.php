<?php
  $po_list = $po_list[0];
?>
<style type="text/css">
  .table-hover tbody tr:hover{
    background-color: #eee;
  }
</style>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrir/mrir_new_process">

    <div class="row">
      <input type="hidden" name="receiving_id" class="form-control" value="<?php echo $po_list['id'] ?>">
      <input type="hidden" name="category" class="form-control" value="<?php echo $category ?>">
      <!-- <input type="hidden" name="f" class="form-control" value="<?php //echo $po_list['id'] ?>"> -->

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <?php  echo $this->session->flashdata('message');?>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">PO No.</label>
                <div class="col-sm-10">
                  <input type="text" name="po_number" class="form-control" value="<?php echo $po_list['po_number'] ?>" readonly required="">                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Create Date</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" value="<?php echo date('Y-m-d H:i:s') ?>" readonly required="">                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Create By</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" value="<?php echo $this->user_cookie[1] ?>" readonly required="">                
                </div>
              </div>
              
            </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-gray">
            <div class="container-fluid">

              <table class="table table-bordered table-hover text-center text-nowrap" id='mrir_list_data'>
                <thead class="bg-green-smoe text-white">
                  <tr>
                    <th rowspan="2" class="align-middle">PL No.</th>
                    <th colspan="3" class="align-middle">Material</th>
                    <th rowspan="2" class="align-middle">Department</th>
                    <th colspan="3" class="align-middle">Qty</th>
                    <th rowspan="2" class="align-middle">UOM</th>
                    <th rowspan="2" class="align-middle">Brand</th>
                    <th rowspan="2" class="align-middle">Color Code</th>
                    <th rowspan="2" class="align-middle">Date Receiving</th>
                    <th rowspan="2" class="align-middle">Remarks</th>
                  </tr>
                  <tr>
                    <th>Code</th>
                    <th>Category</th>
                    <th>Name</th>

                    <th>Received</th>
                    <th>Over</th>
                    <th>Shortage</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($receiving_detail_list as $key => $detail): ?>
                    <input type="hidden" name="receiving_detail_id[]" value="<?= $detail['rd_id'] ?>">
                    <input type="hidden" name="id_material[]" value="<?= $detail['id_material'] ?>">
                    <input type="hidden" name="dept_id[]" value="<?= $detail['dept_id'] ?>">

                    <tr>
                      <td><?= $detail['do_pl'] ?></td>
                      <td><?= $detail['code_material'] ?></td>
                      <td><?= $detail['catalog_category'] ?></td>
                      <td><?= $detail['material'] ?></td>
                      <td><?= $detail['name_of_department'] ?></td>
                      <td><input style="min-width: 100px" type="number" name="qty[]" class="form-control main_input" value="<?php echo $detail['qty']-$detail['qty_over'] ?>" min="1" onfocus="save_qty($(this).val())" oninput="calculate_qty_main(this)" required></td>
                      <td><input style="min-width: 100px" type="number" name="qty_over[]" class="form-control" value="<?php echo $detail['qty_over'] ?>" min="0" required></td>
                      <td><input style="min-width: 100px" type="number" name="qty_shortage[]" class="form-control" value="<?php echo $detail['qty_shortage'] ?>" min="0" required></td>
                      <td><?= $detail['uom'] ?></td>
                      <td><?php echo $detail['brand'] ?></td>
                      <td><?php echo $detail['color_code'] ?></td>
                      <td><?= $detail['date_manufacturing'] ?></td>
                      <td><textarea name='remarks[]' class="form-control" placeholder="Fill Up Remarks"></textarea></td>
                      
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              
            </div>
          </div>
          <div class="text-right mt-3">

            <?php if($read_permission[38] == 1){ ?>

              <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>

            <?php } ?>

            <a href="<?php echo base_url();?>mrir/mrir_new_choose_po" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  // $('#mrir_list_data').DataTable({
  //   // "scrollX": true,
  //   "order": [],
  //   // "fixedColumns":   {
  //   //   "leftColumns": 2,
  //   // }
  // });

  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function add_heat(btn) {
    var row = $(btn).closest('tr');
    $(row).after('<tr>'+row.html()+'</tr>');
    var next = $(row).next();
    var next_btn = $(next).find('button');
    var next_qty = $(next).find('input[name="qty[]"');
    var next_qty_over = $(next).find('input[name="qty_over[]"');
    var next_qty_shortage = $(next).find('input[name="qty_shortage[]"');
    $(next_btn).replaceWith('<button type="button" onclick="delete_heat(this);" class="btn btn-danger" title="Delete Heat Number"><i class="fa fa-times"></i></button>');
    $(next_qty).removeClass('main_input');
    $(next_qty).val(0);
    $(next_qty_over).val(0);
    $(next_qty_shortage).val(0);
  }

  function delete_heat(btn) {
    var row = $(btn).closest('tr');
    var input = $(row).find('input[name="qty[]"');
    var target = $(".main_input[title="+$(input).attr('title')+"]");
    var new_qty = parseInt($(target).val()) + parseInt($(input).val());
    $(target).val(new_qty);
    $(row).remove();
  }

  var qty_default;
  function save_qty(input) {
    qty_default = input;
    // console.log('Save : '+input);
  }

  function calculate_qty_main(input) {

    if($(input).hasClass("main_input") == false){
      var target = $(".main_input[title="+$(input).attr('title')+"]");
      var target_qty = parseInt($(target).val());
      var input_qty_default = parseInt(qty_default);
      var input_qty = parseInt($(input).val());
      var operation;
      // console.log('target_qty : '+target_qty);
      // console.log('input_qty_default : '+input_qty_default);
      // console.log('input_qty : '+input_qty);
      if(input_qty_default > input_qty){
        var dif_qty = parseInt(input_qty_default - input_qty);
        // console.log('dif_qty : '+dif_qty);
        if(input_qty > 0){
          $(target).val(target_qty + dif_qty);
          qty_default = input_qty;
        }
        else{
          //input data 0
          sweetalert('error', 'Minimal Input Qty is 1');
          $(input).val(input_qty_default);
        }
      }
      else{
        var dif_qty = parseInt(input_qty - input_qty_default);
        // console.log('dif_qty : '+dif_qty);
        if(target_qty - dif_qty > 0){
          $(target).val(target_qty - dif_qty);
          qty_default = input_qty;
        }
        else{
          //qty input is over from qty main
          sweetalert('error', 'Your input data is over from qty that you have.');
          $(input).val(input_qty_default);
        }        
      }
    }
  }

  // function calculate(input) {
  //   console.log($(input).closest('tr').find('.width_m').val());
  //   var qty = $(input).val();

  //   var cell_length = $(input).closest('tr').find('.length_m').val() * qty;
  //   $(input).closest('tr').find('.cell-length').text(cell_length);
  //   $(input).closest('tr').find('input[name="length[]"]').val(cell_length);

  //   var cell_width = $(input).closest('tr').find('.width_m').val() * qty;
  //   $(input).closest('tr').find('.cell-width').text(cell_width);
  //   $(input).closest('tr').find('input[name="width_or_od[]"]').val(cell_width);

  //   var cell_thk = $(input).closest('tr').find('.thk_mm').val() * qty;
  //   $(input).closest('tr').find('.cell-thk').text(cell_thk);
  //   $(input).closest('tr').find('input[name="sch_or_thk[]"]').val(cell_thk);
  // }
</script>