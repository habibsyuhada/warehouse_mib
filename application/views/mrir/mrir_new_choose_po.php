<?php
  $receiving_list = $receiving_list[0];
?>
<style type="text/css">
  .table-hover tbody tr:hover{
    background-color: #eee;
  }
</style>
<div id="content" class="container-fluid">

  <form method="POST" action="<?php echo base_url();?>mrir/mrir_new">

    <div class="row">
      <input type="hidden" name="receiving_id" class="form-control" value="<?php echo $receiving_list['id'] ?>">
      <input type="hidden" name="category" class="form-control" value="<?php echo $category ?>">
      <!-- <input type="hidden" name="f" class="form-control" value="<?php //echo $receiving_list['id'] ?>"> -->

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <?php  echo $this->session->flashdata('message');?>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">PO Number</label>
                <div class="col-sm-10">
                  <select name="po_number" class="select2 form-control">
                    <?php foreach ($po_list as $key => $value) {
                      echo "<option value='".$value['po_number']."'>".$value['po_number']."</option>";
                    } ?>
                  </select>
                </div>
              </div>
              
            </div>
          </div>
          <div class="text-right mt-3">
            <?php if(sizeof($po_list) > 0){ ?>
            <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <?php } ?>
            <a href="<?php echo base_url();?>receiving_detail/<?php echo strtr($this->encryption->encrypt($receiving_list['id']), '+=/', '.-~'); ?>" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
</script>