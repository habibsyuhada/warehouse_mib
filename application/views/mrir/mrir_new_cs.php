<style type="text/css">
  .table-hover tbody tr:hover{
    background-color: #eee;
  }
</style>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrir/mrir_new_process">

    <div class="row">
      <input type="hidden" name="receiving_id" class="form-control" value="<?php echo $receiving_list['id'] ?>">
      <input type="hidden" name="category" class="form-control" value="<?php echo $category ?>">
      <!-- <input type="hidden" name="f" class="form-control" value="<?php //echo $receiving_list['id'] ?>"> -->

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <?php  echo $this->session->flashdata('message');?>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">PO No.</label>
                <div class="col-sm-10">
                  <input type="text" name="po_number" class="form-control" value="<?php echo $receiving_list['shipment_no'] ?>" readonly="" required="">                
                </div>
              </div>

              <!-- <div class="form-group row">
                <label class="col-sm-2 col-form-label">Vendor</label>
                <div class="col-sm-10">
                  <input type="text" name="vendor" class="form-control" value="<?php echo $receiving_list['vendor'] ?>" readonly="" required="">                
                </div>
              </div> -->

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Date Receiving</label>
                <div class="col-sm-10">
                  <input type="text" name="date_receiving" class="form-control datepicker" value="<?php echo $receiving_list['date_shipment'] ?>" readonly="" required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Create Date</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" value="<?php echo date('Y-m-d H:i:s') ?>" readonly="" required="">                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Create By</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" value="<?php echo $this->user_cookie[1] ?>" readonly="" required="">                
                </div>
              </div>
              
            </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-gray">
            <div class="container-fluid">

              <table class="table table-bordered table-hover text-center text-nowrap" id='mrir_list_data'>
                <thead class="bg-green-smoe text-white">
                  <tr>
                    <!-- <th>DO No.</th> -->
                    <th rowspan="2">Material Code</th>
                    <th rowspan="2">Heat/Series No.</th>
                    <th rowspan="2">Plate/Tag No.</th>
                    <th rowspan="2">Qty</th>
                    <th rowspan="2">Qty Over</th>
                    <th rowspan="2">Qty Shortage</th>
                    <th rowspan="2">Description</th>
                    <th rowspan="2">Vendor</th>
                    <th colspan="5">Size</th>
                    <th rowspan="2">Material Class</th>
                    <th rowspan="2">Mill Cert. No.</th>
                    <th rowspan="2">Brand</th>
                    <th rowspan="2">UoM</th>
                    <th rowspan="2">Color Code</th>
                    <!-- <th>Date Receiving</th> -->
                    <th rowspan="2">Remarks</th>
                  </tr>
                  <tr>
                    <th>Length</th>
                    <th>Width</th>
                    <th>Thk</th>
                    <th>Sch</th>
                    <th>OD</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($receiving_detail_list as $key => $detail): ?>
                    <tr>
                      <!-- <td>
                        <?php echo $receiving_list[$detail['receiving_id']]['do_pl'] ?>
                        <input type="hidden" class="form-control" name="do_no[]" value="<?php echo $receiving_list[$detail['receiving_id']]['do_pl'] ?>">
                      </td> -->
                      <td><?php echo $catalog_list[$detail['catalog_id']]['code_material'] ?></td>
                      <td>
                        <div class="row">
                          <div class="col-sm pr-0">
                            <input type="text" name="heat_no[]" class="form-control" required>
                            <input type="hidden" name="receive_detail_id[]" class="form-control" value="<?php echo $detail['id'] ?>">
                          </div>
                          <div class="col-sm-auto pl-0">
                            <?php if($read_permission[38] == 1){ ?>
                            <button type="button" onclick="add_heat(this);" class="btn btn-success" title="Add Heat Number"><i class="fa fa-plus"></i></button>
                            <?php } ?>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="row">
                          <div class="col-sm pr-0">
                            <input type="text" name="plate_or_tag_no[]" class="form-control" required>
                          </div>
                          <div class="col-sm-auto pl-0">
                            <?php if($read_permission[38] == 1){ ?>
                            <button type="button" onclick="add_heat(this);" class="btn btn-success" title="Add Plate Number"><i class="fa fa-plus"></i></button>
                            <?php } ?>
                          </div>
                        </div>
                      </td>
                      <td>
                        <input style="min-width: 100px" type="number" name="qty[]" class="form-control main_input" title="qty_<?php echo $detail['id'] ?>" value="<?php echo $detail['qty']-$detail['qty_over'] ?>" min="1" onfocus="save_qty($(this).val())" oninput="calculate_qty_main(this)" required>
                        <input type="hidden" class="form-control length_m" value="<?php echo ($catalog_list[$detail['catalog_id']]['length_m'] == '' ? 0 : $catalog_list[$detail['catalog_id']]['length_m']) ?>">
                        <input type="hidden" class="form-control width_m" value="<?php echo ($catalog_list[$detail['catalog_id']]['width_m'] == '' ? 0 : $catalog_list[$detail['catalog_id']]['width_m'] ) ?>">
                        <input type="hidden" class="form-control thk_mm" value="<?php echo ($catalog_list[$detail['catalog_id']]['thk_mm'] == '' ? 0 : $catalog_list[$detail['catalog_id']]['thk_mm']) ?>">
                      </td>
                      <td>
                        <input style="min-width: 100px" type="number" name="qty_over[]" class="form-control" value="<?php echo $detail['qty_over'] ?>" min="0" required>
                      </td>
                      <td>
                        <input style="min-width: 100px" type="number" name="qty_shortage[]" class="form-control" value="<?php echo $detail['qty_shortage'] ?>" min="0" required>
                      </td>
                      <td>
                        <?php echo $catalog_list[$detail['catalog_id']]['material'] ?>
                        <input type="hidden" name="description[]" class="form-control" value="<?php echo $catalog_list[$detail['catalog_id']]['material'] ?>">
                      </td>
                      <td>
                        <?php echo $vendor_list[$detail['vendor_id']] ?>
                        <input type="hidden" name="vendor[]" class="form-control" value="<?php echo $detail['vendor_id'] ?>">
                      </td>
                      <td class="cell-length"><?php echo ($catalog_list[$detail['catalog_id']]['length_m'] == '' ? '' : $catalog_list[$detail['catalog_id']]['length_m']) ?></td>
                      <td class="cell-width"><?php echo ($catalog_list[$detail['catalog_id']]['width_m'] == '' ? '' : $catalog_list[$detail['catalog_id']]['width_m']) ?></td>
                      <td class="cell-thk"><?php echo ($catalog_list[$detail['catalog_id']]['thk_mm'] == '' ? '' : $catalog_list[$detail['catalog_id']]['thk_mm']) ?></td>
                      <td class="cell-thk"><?php echo ($catalog_list[$detail['catalog_id']]['sch'] == '' ? '' : $catalog_list[$detail['catalog_id']]['sch']) ?></td>
                      <td class="cell-thk"><?php echo ($catalog_list[$detail['catalog_id']]['od'] == '' ? '' : $catalog_list[$detail['catalog_id']]['od']) ?></td>
                      <td>
                        <?php echo $detail['spec_category'] ?>
                        <input type="hidden" name="spec_category[]" value="<?php echo $detail['spec_category'] ?>">
                      </td>
                      <td>
                        <?php echo $detail['mill_cert_no'] ?>
                        <input type="hidden" name="mill_cert_no[]" value="<?php echo $detail['mill_cert_no'] ?>">
                      </td>
                      <td>
                        <?php echo $detail['brand'] ?>
                        <input type="hidden" name="brand[]" value="<?php echo $detail['brand'] ?>">
                      </td>
                      <td>
                        <input type="hidden" name="uom[]" value="PCS">
                        PCS
                      </td>
                      <td><?php echo $detail['color_code'] ?></td>
                      <!-- <td>
                        <?php echo $receiving_list[$detail['receiving_id']]['date_created'] ?>
                        <input type="hidden" class="form-control-plaintext" readonly name="date_receiving[]" value="<?php echo $receiving_list[$detail['receiving_id']]['date_created'] ?>">
                      </td> -->
                      <td><?php echo $detail['remark'] ?></td>
                      
                      <input type="hidden" name="country_origin[]" value="<?php echo $detail['country_origin'] ?>">
                      <input type="hidden" name="date_manufacturing[]" value="<?php echo $detail['date_manufacturing'] ?>">
                      <input type="hidden" name="ceq[]" value="<?php echo $detail['ceq'] ?>">
                      <input type="hidden" name="project_id[]" value="<?php echo $detail['project_id'] ?>">
                      <input type="hidden" name="discipline[]" value="<?php echo $detail['discipline'] ?>">
                      <input type="hidden" name="spec_grade[]" value="<?php echo $detail['spec_grade'] ?>">
                      <input type="hidden" name="receiving_detail_id[]" value="<?php echo $detail['id'] ?>">
                      <input type="hidden" name="catalog_id[]" value="<?php echo $detail['catalog_id'] ?>">
                      <input type="hidden" name="length[]" value="<?php echo ($catalog_list[$detail['catalog_id']]['length_m'] == '' ? 0 : $catalog_list[$detail['catalog_id']]['length_m']) ?>">
                      <input type="hidden" name="width_or_od[]" value="<?php echo ($catalog_list[$detail['catalog_id']]['width_m'] == '' ? 0 : $catalog_list[$detail['catalog_id']]['width_m'] ) ?>">
                      <input type="hidden" name="sch_or_thk[]" value="<?php echo ($catalog_list[$detail['catalog_id']]['thk_mm'] == '' ? 0 : $catalog_list[$detail['catalog_id']]['thk_mm']) ?>">
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              
            </div>
          </div>
          <div class="text-right mt-3">

            <?php if($read_permission[38] == 1){ ?>

              <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>

            <?php } ?>

            <a href="<?php echo base_url();?>mrir/mrir_new_choose_po" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  // $('#mrir_list_data').DataTable({
  //   // "scrollX": true,
  //   "order": [],
  //   // "fixedColumns":   {
  //   //   "leftColumns": 2,
  //   // }
  // });

  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function add_heat(btn) {
    var row = $(btn).closest('tr');
    $(row).after('<tr>'+row.html()+'</tr>');
    var next = $(row).next();
    var next_btn = $(next).find('button');
    var next_qty = $(next).find('input[name="qty[]"');
    var next_qty_over = $(next).find('input[name="qty_over[]"');
    var next_qty_shortage = $(next).find('input[name="qty_shortage[]"');
    $(next_btn).replaceWith('<button type="button" onclick="delete_heat(this);" class="btn btn-danger" title="Delete Heat Number"><i class="fa fa-times"></i></button>');
    $(next_qty).removeClass('main_input');
    $(next_qty).val(0);
    $(next_qty_over).val(0);
    $(next_qty_shortage).val(0);
  }

  function delete_heat(btn) {
    var row = $(btn).closest('tr');
    var input = $(row).find('input[name="qty[]"');
    var target = $(".main_input[title="+$(input).attr('title')+"]");
    var new_qty = parseInt($(target).val()) + parseInt($(input).val());
    $(target).val(new_qty);
    $(row).remove();
  }

  var qty_default;
  function save_qty(input) {
    qty_default = input;
    // console.log('Save : '+input);
  }

  function calculate_qty_main(input) {

    if($(input).hasClass("main_input") == false){
      var target = $(".main_input[title="+$(input).attr('title')+"]");
      var target_qty = parseInt($(target).val());
      var input_qty_default = parseInt(qty_default);
      var input_qty = parseInt($(input).val());
      var operation;
      // console.log('target_qty : '+target_qty);
      // console.log('input_qty_default : '+input_qty_default);
      // console.log('input_qty : '+input_qty);
      if(input_qty_default > input_qty){
        var dif_qty = parseInt(input_qty_default - input_qty);
        // console.log('dif_qty : '+dif_qty);
        if(input_qty > 0){
          $(target).val(target_qty + dif_qty);
          qty_default = input_qty;
        }
        else{
          //input data 0
          sweetalert('error', 'Minimal Input Qty is 1');
          $(input).val(input_qty_default);
        }
      }
      else{
        var dif_qty = parseInt(input_qty - input_qty_default);
        // console.log('dif_qty : '+dif_qty);
        if(target_qty - dif_qty > 0){
          $(target).val(target_qty - dif_qty);
          qty_default = input_qty;
        }
        else{
          //qty input is over from qty main
          sweetalert('error', 'Your input data is over from qty that you have.');
          $(input).val(input_qty_default);
        }        
      }
    }
  }

  // function calculate(input) {
  //   console.log($(input).closest('tr').find('.width_m').val());
  //   var qty = $(input).val();

  //   var cell_length = $(input).closest('tr').find('.length_m').val() * qty;
  //   $(input).closest('tr').find('.cell-length').text(cell_length);
  //   $(input).closest('tr').find('input[name="length[]"]').val(cell_length);

  //   var cell_width = $(input).closest('tr').find('.width_m').val() * qty;
  //   $(input).closest('tr').find('.cell-width').text(cell_width);
  //   $(input).closest('tr').find('input[name="width_or_od[]"]').val(cell_width);

  //   var cell_thk = $(input).closest('tr').find('.thk_mm').val() * qty;
  //   $(input).closest('tr').find('.cell-thk').text(cell_thk);
  //   $(input).closest('tr').find('input[name="sch_or_thk[]"]').val(cell_thk);
  // }
</script>