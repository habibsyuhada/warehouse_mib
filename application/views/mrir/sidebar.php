<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
  <ul class="list-unstyled components">
  <?php if($read_permission[37] == 1){ ?>
    <?php if($read_permission[38] == 1){ ?>
    <li>
      <a href="<?= base_url();?>mrir/mrir_new_choose_po">
        <i class="fas fa-plus"></i> New MRIR Request
      </a>
    </li>
    <?php } ?>
    <li>
      <a href="#homeSubmenu3" data-parent="#sidebar" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">
        <i class="fas fa-spell-check"></i> MRIR Console
      </a>
      <ul class="list-unstyled collapse show" id="homeSubmenu3">
        <li>
          <a href="<?= base_url();?>mrir/mrir_data/request"><i class="fas fa-caret-right"></i> Request</a>
        </li>
        <li>
          <a href="<?= base_url();?>mrir/mrir_data/approved"><i class="fas fa-caret-right"></i> Completed</a>
        </li>
         <!-- <li>
         <a href="<?= base_url();?>mrir/mrir_data/rejected/SS"><i class="fas fa-caret-right"></i> Pending</a> 
        </li> -->
      </ul>
    </li>
  <?php } ?>
  </ul>
</nav>