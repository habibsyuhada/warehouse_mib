<script type='text/javascript'>
  $(window).on('load', function() { add_element(); });

  var count_element = 1;
  function add_element(){

    var html_element =  '<tr id="tr_element_' + count_element + '">' +
                          '<td><input type="text" id="unique_no'+count_element+'" onfocus="unique_no_func(' + count_element + ');" name="unique_no[' + count_element + ']" class="form-control" onblur="checkunique(this,' + count_element + ');"></td>' +
                          '<td><input type="text" id="description[' + count_element + ']" name="description[' + count_element + ']" class="form-control" readonly></td>' +

                          '<td><input type="text" id="thk_mm[' + count_element + ']" name="thk_mm[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="width_m[' + count_element + ']" name="width_m[' + count_element + ']" class="form-control" readonly></td>' + 
                          '<td><input type="text" id="length_m[' + count_element + ']" name="length_m[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="weight[' + count_element + ']" name="weight[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="od[' + count_element + ']" name="od[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="sch[' + count_element + ']" name="sch[' + count_element + ']" class="form-control" readonly></td>' +

                          '<td><input type="text" id="tag_heat_no[' + count_element + ']" name="tag_heat_no[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="balance[' + count_element + ']" name="balance[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="balance_length[' + count_element + ']" name="balance_length[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="number" name="issued_qty[' + count_element + ']" class="form-control" placeholder="*" required   oninput="auto_length(this,' + count_element + ');"  ></td>' +
                          '<td><input type="number" name="issued_length[' + count_element + ']" class="form-control" placeholder="*" required  step="any" oninput="auto_length_qty(this,' + count_element + ');"></td>' +
                          '<td><input type="text" id="unit[' + count_element + ']" name="unit[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" name="location[' + count_element + ']" class="form-control" placeholder="*" required></td>' +
                          '<td><textarea type="text" name="remarks[' + count_element + ']" class="form-control" placeholder="*" ></textarea></td>' +
                          '<td><button class="btn btn-danger" type="button" onclick="delete_element(' + count_element + ')"><i class="fa fa-trash"></i></button></td>' + 
                        '</tr>';

    $('#table_element').append(html_element);

    count_element++;
  }
</script>

<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mrs/mrs_new_form" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">


              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                   <label>Project</label>
                      <input type="hidden" class="form-control" name="project_id" id="project_id" placeholder='---' readonly>         
                      <input type="text" class="form-control" name="project_name" id="project_name" placeholder='---' readonly>         
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                    <label>mrs No</label>
                        <input type="text" class="form-control" name="mrs_no" id="mrs_no" value='<?php echo $get_mrs_no; ?>' readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Sub Contractor Name</label>
                        <input type="text" class="form-control" name="sub_contractor_name" id="sub_contractor_name" placeholder="---" required>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                     <label>Date</label>
                        <input type="date" class="form-control" name="created_date" id="created_date" value="<?php echo date('Y-m-d'); ?>" required> 
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Line No.</label>
                        <input type="text" class="form-control" name="line_no" id="line_no" placeholder="---" required>
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Material Class</label>
                     <select  class="custom-select select2" name="material_class" required>
                          <option value="">---</option>
                          <?php foreach($material_class as $material_class): ?>
                            <option value="<?php echo $material_class['material_class'] ?>"><?php echo $material_class['material_class'] ?></option>
                          <?php endforeach; ?>
                      </select>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Drawing Number</label>
                        <input type="text" class="form-control" name="drawing_no" id="drawing_no" placeholder="Drawing Number" onblur="check_mrs(this)" required>                
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Equipment Tag No.</label>
                        <input type="text" class="form-control" name="equipment_tag_no" id="equipment_tag_no" placeholder="---" required>
                  </div>
                </div>
              </div>
            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" >
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th rowspan="2">UNIQUE NO.</th>
                          <th rowspan="2">DESCRIPTION</th>
                          <th  colspan="6">SIZE</th>
                        
                          
                          <th rowspan="2">TAG/HEAT NO</th>
                          <th colspan="2">BALANCE</th>
                          <th colspan="2">ISSUED</th>
                          <th rowspan="2">UOM</th>
                         
                          <th rowspan="2">LOCATION</th>
                          <th rowspan="2">REMARKS</th>
                          <th rowspan="2" >
                             <?php if($read_permission[52] == 1){ ?>
                            <button class="btn btn-success float-right" style='background-color: #004cc2;' type="button" onclick="add_element()"><i class="fa fa-plus"></i></button>
                            <?php } ?>
                          </th>
                          
                        </tr>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th>THK<br/>(MM)</th>
                          <th>WIDTH<br/>(M)</th>
                          <th>LENGTH<br/>(M)</th>
                          <th>WEIGHT<br/>(KG)</th>
                          <th>OD<br/>(M)</th>
                          <th>SCH<br/>(M)</th>

                          <th>QTY<br/>(PCS)</th>
                          <th>LENGTH<br/>(M)</th>

                          <th>QTY<br/>(PCS)</th>
                          <th>LENGTH<br/>(M)</th>
                        </tr> 

                      </thead>
                      
                      <tbody id="table_element" class="table-border">
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-right mt-3">   

            <?php if($read_permission[52] == 1){ ?>

              <button type='submit' name='submitBtn' id='submitBtn' value='submit' class="btn btn-success" title="Submit" disabled ><i class="fa fa-check"></i> Submit</button>

            <?php } ?>

            <a href="<?php echo base_url();?>mrs" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>

</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">

  var no=1;
  

  var delayTimer;
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $("#drawing_no").autocomplete({
     source: "<?php echo base_url(); ?>mrs/drawing_autocomplete",
     autoFocus: true,
     classes: {
         "ui-autocomplete": "highlight"
     }
  });

  function check_mrs(input) {
    var drawing_no = $(input).val();

    if(drawing_no == ''){

        $(input).addClass('is-invalid');
        $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
        $('button[name=submit]').prop("disabled", true);

        $('input[name="discipline"]').val('');
        $('input[name="module"]').val('');
    
    } else {

      $.ajax({
        url: "<?php echo base_url();?>mrs/drawing_no_check/",
        type: "post",
        data: {
          drawing_no: drawing_no
        },
        success: function(data) {
          var data = JSON.parse(data);
       
          if(data.hasil == 0){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Drawing Not Found')" );
            $(input).after('<div class="invalid-feedback">Drawing Not Found.</div>');
            $('button[name=submit]').prop("disabled", true);

            $('input[name="discipline"]').val('');
            $('input[name="module"]').val('');
          
          } else {
          
            $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            $('input[name="discipline"]').val(data.discipline);
            $('input[name="module"]').val(data.module);
            $('input[name="project_id"]').val(data.project_id);
            $('input[name="project_name"]').val(data.project_name);
          
          }
          
          if(!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }

        }
      });

    }
  }
</script>

<script type="text/javascript">

  function unique_no_func(nox){
    
    var project_selected = $("input[id='project_id']").val();
    var drawing_no       = $("input[id='drawing_no']").val();

    $("input[name='unique_no["+nox+"]']").autocomplete({

      source: function(request,response){
        $.post('<?php echo base_url(); ?>mrs/uniqno_autocomplete',{term: request.term, project_id: project_selected, drawing_no: drawing_no }, response, 'json');
      },
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });

  }


  function delete_element(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_process(count);

      }
    })
  }

  function delete_element_process(count){
    $('#tr_element_' + count).remove();
  }

   function checkunique(input, num) {

    var text             = $(input).val();
    var empty_val        = "-";
    var project_selected = $("input[id='project_id']").val();

      $.ajax({

        url: "<?php echo base_url(); ?>mrs/unique_no_check",
        type: "post",
        data: {
          'unique_no': text,
          'project_id': project_selected,
        },

        success: function(data) {
          
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='unique_no"+i+"']").val() == text){
                data = 'Error : Duplicate Unique No on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');

            $("input[name='description["+num+"]']").val(empty_val);

            $("input[name='thk_mm["+num+"]']").val(empty_val);
            $("input[name='width_m["+num+"]']").val(empty_val);
            $("input[name='length_m["+num+"]']").val(empty_val);
            $("input[name='weight["+num+"]']").val(empty_val);
            $("input[name='od["+num+"]']").val(empty_val);
            $("input[name='sch["+num+"]']").val(empty_val);

            $("input[name='unit["+num+"]']").val(empty_val);
            $("input[name='tag_heat_no["+num+"]']").val(empty_val);

            $("input[name='balance["+num+"]']").val(empty_val);
            $("input[name='balance_length["+num+"]']").val(empty_val);
                                 
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            var res = data.split("; ");

            $("input[name='description["+num+"]']").val(res[0]);

            $("input[name='thk_mm["+num+"]']").val(res[1]);
            $("input[name='width_m["+num+"]']").val(res[2]);
            $("input[name='length_m["+num+"]']").val(res[3]);
            $("input[name='weight["+num+"]']").val(res[4]);
            $("input[name='od["+num+"]']").val(res[5]);
            $("input[name='sch["+num+"]']").val(res[6]);

            $("input[name='unit["+num+"]']").val(res[7]);
            $("input[name='tag_heat_no["+num+"]']").val(res[8]);

            $("input[name='balance["+num+"]']").val(res[9]);
            $("input[name='balance_length["+num+"]']").val(res[10]);
            
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  }


  function checkbalance(input, num) {

    var text      = $(input).val();
    var unique_no = $('input[id="unique_no'+num+'"').val();
    var empty_val = "-";
  
      $.ajax({
        url: "<?php echo base_url(); ?>mrs/balance_checking",
        type: "post",
        data: {
          'issued_qty': text,
          'unique_no': unique_no,
        },
        success: function(data) {
        
          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $('.valid-feedback').remove( ":contains('Available')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
            $('.valid-feedback').remove( ":contains('Available')" );
            $(input).after('<div class="valid-feedback">'+data+'</div>');
            $('button[name=submitBtn]').prop("disabled", false);
           
          }

            
         
        }
      });
   }

  </script>

  <script type="text/javascript">

  function validate_approve() {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
         //$('form').submit();
      }
    })

  }

  function auto_length(input,num) {

    var text = $(input).val();

    var bal_qty = $("input[name='balance["+num+"]']").val();
    var length = $("input[name='balance_length["+num+"]']").val();

    var total_length_by_unt = Number(length) / Number(bal_qty);

    var actual_length = Number(text) * total_length_by_unt;
    var actual_length_dsp = actual_length.toFixed(2);

    $("input[name='issued_length["+num+"]']").val(actual_length_dsp);      

  }

  function auto_length_qty(input,num) {

    var text    = $(input).val();

    var bal_qty = $("input[name='balance["+num+"]']").val();
    var length  = $("input[name='balance_length["+num+"]']").val();

    var total_length_by_unt = Number(length) / Number(bal_qty);

    var actual_length     = Number(text) / total_length_by_unt;
    var actual_length_dsp =  Math.ceil(actual_length);

    $("input[name='issued_qty["+num+"]']").val(actual_length_dsp);      

   }

  
 $(function(){
   
       $('.select_unique').select2({
           minimumInputLength: 1,
           allowClear: true,
           placeholder: 'Search Unique No',
           ajax: {
              dataType: 'json',
              type: "post",
               url: "<?php echo base_url(); ?>mrs/uniqno_autocomplete_select2",
              delay: 800,
              data: function(params) {
                return {
                  search: params.term,
                  project_id:  $("input[id='project_id']").val(),
                }
              },
              processResults: function (data, page) {
              return {
                results: data
              };
            },
          }
      }).on('select2:select', function (evt) {
         var data = $(".select2 option:selected").text();
      });
 });

</script>