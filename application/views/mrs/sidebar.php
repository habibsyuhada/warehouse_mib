<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
  <ul class="list-unstyled components">
   
    <?php if($read_permission[51] == 1){ ?>
    
    <li>
      <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-home"></i> MRS ( Material Return Slip )
      </a>
      <ul class="list-unstyled" id="homeSubmenu2">
        
        <?php if($read_permission[52] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>mrs_add"><i class="fas fa-plus"></i> &nbsp; Create New</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>mrs/mrs_import"><i class="far fa-file-excel"></i> &nbsp; Import</a>
        </li>
        <?php } ?>

        <li>
          <a href="<?php echo base_url();?>mrs/mrs_list/pending"><i class="far fa-clock"></i> &nbsp; Pending List</a>
        </li>
         <li>
          <a href="<?php echo base_url();?>mrs/mrs_list/rejected"><i class="fas fa-times-circle"></i> &nbsp; Rejected List</a>
        </li>
         <li>
          <a href="<?php echo base_url();?>mrs/mrs_list/approved"><i class="fas fa-check-circle"></i> &nbsp; Approved List</a>
        </li>
      </ul>
    </li> 

    <?php } ?>
 

  </ul>
</nav>