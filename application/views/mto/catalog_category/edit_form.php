  <?php 
    $mto_category_list = $mto_category_list[0];
  ?>
  <div class="container" style="background-color: whitesmoke">
    <div class="row">
      <div class="container-fluid">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <?php if ($this->session->flashdata('success')): ?>
            <div class="alert alert-success" role="alert">
              <?php echo $this->session->flashdata('success'); ?>
            </div>
          <?php endif; ?>

          <a class='btn btn-warning' href="<?php echo site_url('mto_category') ?>">
            <i class="fas fa-arrow-left"></i> Back
          </a>
        
          <div class="card-body">
            <form action="<?php echo base_url() ?>mto_category/edit_process" method="post" enctype="multipart/form-data" autocomplete='off'>
              <input type="hidden" name="id" value="<?= $mto_category_list['id'] ?>"/>
                <div class="form-group">
                  <label for="initial">Initial*</label>
                  <input class="form-control" type="text" name="initial" value='<?= $mto_category_list['initial'] ?>' required />
                </div>
                
                <div class="invalid-feedback">
                  <?php echo form_error('area_name') ?>
                </div>

                <div class="form-group">
                  <label for="description">Description*</label>
                  <input class="form-control" type="text" name="description" value='<?= $mto_category_list['description'] ?>' required />
                </div>

              <button class="btn btn-success" type="submit"><i class="fa fa-edit"></i> Edit</button>
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
