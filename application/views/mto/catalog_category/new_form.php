<div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">  

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('mto_category') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>

        
            <div class="card-body">

                <form action="<?php base_url() ?>add_process" method="post" enctype="multipart/form-data"  autocomplete='off'>
                  <div class="form-group">
                    <label for="initial">Initial*</label>
                    <input class="form-control" type="text" name="initial" placeholder="Fill Up Initial" required />
                  </div>
                  <div class="invalid-feedback">
                    <?php echo form_error('initial') ?>
                  </div>

                  <div class="form-group">
                    <label for="description">Description*</label>
                    <input class="form-control" type="text" name="description" placeholder="Fill Up Description" required />
                  </div>

                  <div class="invalid-feedback">
                    <?php echo form_error('description') ?>
                  </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

            </div>
          </div>
        </div>
      </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
