<?php
  $draw = $draw_list[0];
  $material_class_draw = explode("; ",$draw['material_class']);
  // print_r($material_class_draw);exit;
?>
<style type="text/css">
  .nav-link{
    color: #000;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #007bff;
    background: #fff;
    border-bottom: 2px solid #007bff;
    border-radius: 0px;
  }
</style>
<div id="content" class="container-fluid">
 
  <div class="row">

    <div class="col-md-12">
       <div class="overflow-auto text-muted  ">
          <h6><?php $this->load->view('_partial/breadcump.php');?></h6>
       </div>
    </div>

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">
          <?php if($read_permission[2] == 1){ ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == '' ? 'active' : '') ?>" data-toggle="pill" href="#pills-detail">Detail</a>
          </li>

          <?php } ?>

          <?php if($read_permission[9] == 1){ ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'pc' ? 'active' : '') ?>" data-toggle="pill" href="#pills-piecemark">Piece Mark</a>
          </li>

          <?php } ?>

          <?php if($read_permission[14] == 1){ ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'j' ? 'active' : '') ?>" data-toggle="pill" href="#pills-joint">Joint</a>
          </li>

          <?php } ?>

          <?php if($read_permission[2] == 1){ ?>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'r' ? 'active' : '') ?>" data-toggle="pill" href="#pills-rev">Revision</a>
          </li>

          <?php }?>

        </ul>
        <div class="overflow-auto media text-muted py-3 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="tab-content">

              <!-- Drawing Detail -->
              <div class="tab-pane fade <?php echo ($t == '' ? 'show active' : '') ?>" id="pills-detail">
                <form method="POST" action="<?php echo base_url();?>engineering/draw_edit_process">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Drawing Number :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="drawing_no" placeholder="Drawing Number" value="<?php echo $draw['drawing_no'] ?>" required readonly>                
                          <input type="hidden" class="form-control" name="id" value="<?php echo $draw['id'] ?>" required>                
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Sheet Number :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="sheet_no" placeholder="Sheet Number" value="<?php echo $draw['sheet_no'] ?>" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Module :</label>
                        <div class="col-xl">
                          <select class="custom-select" name="module">
                            <?php foreach($module_list as $module): ?>
                            <option value="<?php echo $module['mod_id'] ?>" <?php echo($module['mod_id'] == $draw['module'] ? 'selected' : ''); ?>><?php echo $module['mod_desc'] ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Discipline :</label>
                        <div class="col-xl">
                          <select class="custom-select" name="discipline">
                            <?php foreach($discipline_list as $discipline): ?>
                            <option value="<?php echo $discipline['id'] ?>" <?php echo($discipline['id'] == $draw['discipline'] ? 'selected' : ''); ?>><?php echo $discipline['discipline_name'] ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Received Date :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control datepicker" data-zdp_readonly_element="false" name="received_date" value="<?php echo date("d-m-Y", strtotime($draw['received_date'])) ?>" required placeholder="Date">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Description :</label>
                        <div class="col-xl">
                          <!-- <input type="text" class="form-control" name="description" placeholder="Description" value="<?php echo $draw['descriptions'] ?>" required> -->
                          <textarea class="form-control" name="description" placeholder="Description" required><?php echo $draw['descriptions'] ?></textarea>
                        </div>
                      </div>
                    </div>
                    
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <!--
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Description :</label>
                        <div class="col-xl">
                          <!-- <input type="text" class="form-control" name="description" placeholder="Description" value="<?php //echo $draw['descriptions'] ?>" required> -->
                        <!--  <textarea class="form-control" name="description" placeholder="Description" required><?php //echo $draw['descriptions'] ?></textarea>
                        </div>
                      </div>
                       -->
                    </div>
                    <div class="col-md">
                      <!--
                        <div class="form-group row">
                          <label class="col-xl-3 col-form-label">Total Weight :</label>
                          <div class="col-xl">
                            <input type="text" class="form-control" name="total_weight" placeholder="Total Weight" value="<?php //echo $draw['area'] ?>" required>
                          </div>
                        </div> 
                      -->
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <?php if($read_permission[5] == 1){ ?>
                            <!-- <button type="submit" name='submit' value='submit' class="btn btn-primary" title="Update"><i class="fa fa-check"></i> Update</button> -->
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
              <!-- Drawing Detail END -->

              <!-- Drawing Peace Mark -->
              <div class="tab-pane fade <?php echo ($t == 'pc' ? 'show active' : '') ?>" id="pills-piecemark">
                
                <?php if($read_permission[10] == 1){ ?>

                <form method="POST" action="<?php echo base_url();?>engineering/piecemark_new_process">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Piece Mark No. :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="piece_mark_no" required>                
                          <input type="hidden" class="form-control" name="drawing_no" value="<?php echo $draw['drawing_no'] ?>" required>                
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Material Grade :</label>
                        <div class="col-xl">
                          <select class="form-control" name="material_grade">
                            <option value="">-</option>
                              <?php foreach($material_grade as $mg): ?>
                              <option value="<?php echo $mg['id'] ?>" ><?php echo $mg['material_grade'] ?></option>
                              <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label"><?php echo ($draw['discipline'] == '2' ? 'Thk(mm) :' : 'Dia (inch) :') ?></label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="diameter">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <?php if($draw['discipline'] == 1): ?>
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Sch :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="sch">
                        </div>
                      </div>
                      <?php endif; ?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Length :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="length">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Weight :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="weight">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <?php  if($draw['discipline'] == 1): ?>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Test Pack No. :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="test_pack_no">
                        </div>
                      </div>
                    </div>
                    <?php endif; ?>
                    <div class="col-md<?php echo ($draw['discipline'] == '2' ? '-6' : '') ?>">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Remarks :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="remarks">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Can Number :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="can_number">
                        </div>
                      </div>
                    </div>
                    <div class="col-md"></div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <!-- <button type="submit" name='submit' value='submit' class="btn btn-primary" title="Submit"><i class="fa fa-check"></i> Submit</button> -->
                        </div>
                      </div>
                    </div>
                  </div>

                </form>

              <?php } ?>

                <table class="table dataTable table-hover text-center">
                  <thead class="bg-success text-white">
                    <tr>
                      <th>#</th>
                      <th>Piece Mark No.</th>
                      <th>Material Grade</th>
                      <th><?php echo ($draw['discipline'] == '2' ? 'Thk(mm) :' : 'Dia (inch) :') ?></th>
                       <?php if($draw['discipline'] == 1): ?>
                      <th>Sch</th>
                       <?php endif; ?>
                      <th>Length</th>
                      <th>Can Number</th>
                      <th>Weight</th>
                      <th>Test Pack No.</th>
                      <th>Remarks</th>
                      <?php //if($read_permission[9] == 1){ ?>
                      <th>Action</th>
                      <?php //} ?>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach($peacemark_list as $peacemark): ?>
                    <tr>
                      <td class="align-middle"><?php echo $no++; ?></td>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="piece_mark_no" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['piece_mark_no'] ?></div>
                      </td>
                      <td class="align-middle">
                        <?php 
                          if($peacemark['material_grade']){
                            $material_grade =  $this->general_mod->material_grade_get_db($peacemark['material_grade']);
                              foreach ($material_grade as $material_grade) {
                                echo $material_grade['material_grade'];
                              }
                          }
                        ?>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="diameter" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['diameter'] ?></div>
                      </td>
                       <?php if($draw['discipline'] == 1): ?>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="sch" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['sch'] ?></div>
                      </td>
                       <?php endif; ?>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="length" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['length'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="can_number" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['can_number'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="weight" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['weight'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="test_pack_no" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['test_pack_no'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[12] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="peacemark_edit(this);" <?php } ?> data-col="remarks" data-id="<?php echo $peacemark['id'] ?>"><?php echo $peacemark['remarks'] ?></div>
                      </td>
                      <?php if($read_permission[13] == 1){ ?>
                      <td class="align-middle">
                        <!-- <button type="button" onClick="peacemark_delete('<?php echo $peacemark['id'] ?>')" class="btn btn-danger text-white" title="Detail">
                          <i class="fas fa-trash"></i>
                        </button> -->
                      </td>
                      <?php } ?>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <!-- Drawing Peace Mark END -->

              <!-- Drawing Joint Mark -->
              <div class="tab-pane fade <?php echo ($t == 'j' ? 'show active' : '') ?>" id="pills-joint">
                
                <?php if($read_permission[15] == 1){ ?>

                <form method="POST" action="<?php echo base_url();?>engineering/joint_new_process">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Weld Map :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="weld_map" required>        
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Class :</label>
                        <div class="col-xl">
                          <select type="text" class="form-control" name="class" required>
                            <option value=''>~ Choice ~</option>
                            <?php foreach ($class_list as $value) { ?>
                               <option value='<?php echo $value['id']; ?>'><?php echo $value['class_name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Joint No. :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="joint_no" required>
                          <input type="hidden" class="form-control" name="drawing_no" value="<?php echo $draw['drawing_no'] ?>" required>                
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Weld Type :</label>
                        <div class="col-xl">
                          <select type="text" class="form-control" name="weld_type" required>
                            <option value=''>~ Choice ~</option>

                            <?php foreach ($weld_type as $value) { ?>
                               <option value='<?php echo $value->weld_type_code; ?>'><?php echo $value->weld_type; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label"><?php echo ($draw['discipline'] == '2' ? 'Thk(mm) :' : 'Dia (inch) :') ?></label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="diameter">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <?php if($draw['discipline'] == 1): ?>
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Sch :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="sch">
                        </div>
                      </div>
                      <?php endif; ?>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Length :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="length">
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Joint Type :</label>
                        <div class="col-xl">
                          <select type="text" class="form-control" name="joint_type" required>
                            <option value=''>~ Choice ~</option>
                            <?php foreach ($joint_type as $value) { ?>
                               <option value='<?php echo $value->joint_type_code; ?>'><?php echo $value->joint_type; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <?php if($draw['discipline'] == 1): ?>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Test Pack No. :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="test_pack_no">
                        </div>
                      </div>
                    </div>
                    <?php endif; ?>
                    <div class="col-md<?php echo ($draw['discipline'] == '2' ? '-6' : '') ?>">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Remarks :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="remarks">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">WPS Group :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="wps_group" required>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <?php if($draw['discipline'] == 1): ?>
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Spool No :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="spool_no" required>
                        </div>
                      </div>
                      <?php endif; ?>
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Can Number :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="can_number" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <!-- <button type="submit" name='submit' value='submit' class="btn btn-primary" title="Submit"><i class="fa fa-check"></i> Submit</button> -->
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              <?php } ?>

                <table class="table dataTable table-hover text-center">
                  <thead class="bg-success text-white">
                    <tr>
                      <th>#</th>
                      <th>Weld Map</th>
                      <th>Joint No.</th>
                      <th>Weld Type</th>
                      <th><?php echo ($draw['discipline'] == '2' ? 'Thk(mm) :' : 'Dia (inch) :') ?></th>
                        <?php if($draw['discipline'] == 1): ?>
                      <th>Sch</th>
                       <?php endif; ?>
                      <th>Length</th>
                      <th>Joint Type</th>
                      <th>Test Pack No.</th>
                      <th>Remarks</th>
                      <th>WPS Group</th>
                      <th>Spool No</th>
                      <th>Class</th>
                      <th>Can Number</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach($joint_list as $joint): ?>
                    <tr>
                      <td class="align-middle"><?php echo $no++; ?></td>
                      <td class="align-middle">
                        <?php echo $joint['weld_map'] ?>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="joint_no" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['joint_no'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="weld_type" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['weld_type'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="diameter" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['diameter'] ?></div>
                      </td>
                      <?php if($draw['discipline'] == 1): ?>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="sch" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['sch'] ?></div>
                      </td>
                      <?php endif; ?>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="length" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['length'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="joint_type" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['joint_type'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="test_pack_no" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['test_pack_no'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="remarks" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['remarks'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="wps_group" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['wps_group'] ?></div>
                      </td>
                      <td class="align-middle">
                        <div <?php if($read_permission[17] == 1){ ?> contenteditable onclick="save_first_value(this);" onblur="joint_edit(this);" <?php } ?> data-col="spool_no" data-id="<?php echo $joint['id'] ?>"><?php echo $joint['spool_no'] ?></div>
                      </td>
                      <td class="align-middle">
                        <?php 
                          foreach ($class_list as $value) {
                            if($value['id']==$joint['class']){
                              echo $value['class_name'];
                            }
                          }
                        ?>
                      </td>
                      <td class="align-middle">
                        <div><?= @$joint['can_number']; ?></div>
                      </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
              <!-- Drawing Joint Mark END -->

              <!-- Drawing Revision -->
              <div class="tab-pane fade <?php echo ($t == 'r' ? 'show active' : '') ?>" id="pills-rev">
                
                <?php if($read_permission[3] == '1'){ ?>

                <form method="POST" action="<?php echo base_url();?>engineering/revision_new_process" enctype="multipart/form-data">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Revision Number :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="rev_no" required>                
                          <input type="hidden" class="form-control" name="drawing_no" value="<?php echo strtr($this->encryption->encrypt($draw['drawing_no']), '+=/', '.-~') ?>" required>                
                          <input type="hidden" class="form-control" name="id" value="<?php echo $draw['id'] ?>" required>                
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Attachment :</label>
                        <div class="col-xl">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input" onChange="$('.custom-file-label').html($(this).val())" name="file" >
                            <label class="custom-file-label">Choose file</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Remarks :</label>
                        <div class="col-xl">
                          <textarea class="form-control" name="remarks"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <!-- <button type="submit" name='submit' value='submit' class="btn btn-primary" title="Submit"><i class="fa fa-check"></i> Submit</button> -->
                        </div>
                      </div>
                    </div>
                  </div>

                </form>

              <?php } ?>

              <?php if($read_permission[2] == '1'){ ?>

                <table class="table dataTable table-hover text-center">
                  <thead class="bg-success text-white">
                    <tr>
                      <th>Revision Number</th>
                      <th>Date</th>
                      <th>Attachment</th>
                      <th>Remarks</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; foreach($revision_list as $revision): ?>
                    <tr>
                      <td class="align-middle"><?php echo $revision['rev_no'] ?></td>
                      <td class="align-middle"><?php echo $revision['rev_date'] ?></td>

                     <td class="align-middle"><a target="_blank" href="<?php echo base_url();?>upload/rev/<?php echo $revision['attachment'] ?>" class="btn btn-flat btn-dark"><i class="fas fa-file-pdf"></i></a></td>

                       <!--  <td class="align-middle"><a target="_blank" href="http://10.5.252.88/content/public/document/drawing/<?php //echo $revision['attachment'] ?>" class="btn btn-flat btn-dark"><i class="fas fa-file-pdf"></i></a></td> -->

                      <td class="align-middle"><?php echo $revision['remarks'] ?></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>

               <?php } ?>  
              </div>
              <!-- Drawing Revision END -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  var first_value;
  function save_first_value(cell){
    var value = $(cell).html();
    first_value = value;
  }

  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function peacemark_delete(id){
    Swal.fire({
      title: 'Are you sure to <b class="text-danger">&nbsp;Delete&nbsp;</b> this?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete it it!'
    }).then((result) => {
      if (result.value) {
        window.location = '<?php echo base_url();?>engineering/piecemark_delete_process/'+id+'/<?php echo strtr($this->encryption->encrypt($draw['drawing_no']), '+=/', '.-~'); ?>';
      }
    })
  }

  function peacemark_edit(cell){
    var col = $(cell).data("col");
    var id = $(cell).data("id");
    var value = $(cell).html();
    if(value != first_value){
      Swal.fire({
        title: 'Are you sure to <b class="text-warning">&nbsp;Edit&nbsp;</b> this?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?php echo base_url();?>engineering/peacemark_edit_process",
            type: "post",
            data: {
              col: col,
              id: id,
              value: value
            }
          });
        }
        else{
          $(cell).html(first_value);
        }
      })
    }
  }

  function joint_delete(id){
    Swal.fire({
      title: 'Are you sure to <b class="text-danger">&nbsp;Delete&nbsp;</b> this?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete it it!'
    }).then((result) => {
      if (result.value) {
        window.location = '<?php echo base_url();?>engineering/joint_delete_process/'+id+'/<?php echo strtr($this->encryption->encrypt($draw['drawing_no']), '+=/', '.-~'); ?>';
      }
    })
  }

  function joint_edit(cell){
    var col = $(cell).data("col");
    var id = $(cell).data("id");
    var value = $(cell).html();
    if(value != first_value){
      Swal.fire({
        title: 'Are you sure to <b class="text-warning">&nbsp;Edit&nbsp;</b> this?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?php echo base_url();?>engineering/joint_edit_process",
            type: "post",
            data: {
              col: col,
              id: id,
              value: value
            }
          });
        }
        else{
          $(cell).html(first_value);
        }
      })
    }
  }
</script>