<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=Drawing-".date('YmdHis').".xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
  $datadb = $this->general_mod->eng_module_get_db();
  foreach ($datadb as $value) {
    $module_list[$value['mod_id']] = $value['mod_desc'];
  }
  $datadb  = $this->general_mod->eng_discipline_get_db();
  foreach ($datadb as $value) {
    $discipline_list[$value['id']] = $value['discipline_name'];
  }
  $datadb  = $this->general_mod->material_class_get_db($this->user_cookie[10]);
  foreach ($datadb as $value) {
    $material_class_list[$value['id']] = $value['material_class'];
  }

 ?>
 <table border="1">
 
      <thead>
 
           <tr>
 
                <th style="background: #28a745">Drawing No</th>
                <th style="background: #28a745">Sheet No</th>
                <th style="background: #28a745">Module</th>
                <th style="background: #28a745">Discipline</th>
                <th style="background: #28a745">Material Class</th>
                <th style="background: #28a745">Date Receive</th>
        
 
           </tr>
 
      </thead>
 
      <tbody>
 
           <?php
              if($draw_list):
              foreach($draw_list as $key => $item):
            ?>
            <tr>
              <td align="center" style="vertical-align:middle"><?php echo $item['drawing_no'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $item['sheet_no'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $module_list[$item['module']] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $discipline_list[$item['discipline']] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $material_class_list[$item['material_class']] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $item['received_date'] ?></td>
            </tr>
            <?php endforeach; endif; ?>
 
      </tbody>
 
 </table>