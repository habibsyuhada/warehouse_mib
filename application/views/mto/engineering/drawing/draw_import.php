<div id="content" class="container-fluid">
  
  <div class="row">
    <div class="col-md-12">

      <?php //if($read_permission[2] == 1){ ?>

      <form method="POST" action="<?php echo base_url();?>engineering/draw_preview" enctype="multipart/form-data">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?> </h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Template Excel</label>
              <div class="col-sm-10 col-form-label">
                <a href="<?php echo base_url(); ?>/file/draw/Template_Import_Drawing.xlsx">Template_Import_Drawing.xlsx</a>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Upload</label>
              <div class="col-sm-10">
                <div class="custom-file">
                  <input type="file" name="file" class="custom-file-input" onChange="$('#label1.custom-file-label').html($(this).val())">
                  <label id="label1" class="custom-file-label">Choose file</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="text-right mt-3">
          <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
        </div>
      </div>
      </form>

      <?php //} ?>

    </div>
  </div>

  <div class="row">

    <div class="col-md-6">

       <?php //if($read_permission[7] == 1){ ?>

      <form method="POST" action="<?php echo base_url();?>engineering/peacemark_preview" enctype="multipart/form-data">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0">Import Peace Mark</h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Template Excel</label>
              <div class="col-sm-10 col-form-label">
                <a href="<?php echo base_url(); ?>/file/draw/Template_Import_Piece_Mark.xlsx">Template_Import_Piece_Mark.xlsx</a>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Upload</label>
              <div class="col-sm-10">
                <div class="custom-file">
                  <input type="file" name='file' class="custom-file-input" onChange="$('#label2.custom-file-label').html($(this).val())">
                  <label id="label2" class="custom-file-label">Choose file</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="text-right mt-3">
          <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
        </div>
      </div>
      </form>

    <?php //}  ?>
    </div>

    <div class="col-md-6">
       <?php //if($read_permission[12] == 1){ ?>
      <form method="POST" action="<?php echo base_url();?>engineering/joint_preview" enctype="multipart/form-data">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0">Import Joint</h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Template Excel</label>
              <div class="col-sm-10 col-form-label">
                <a href="<?php echo base_url(); ?>/file/draw/Template_Import_Joint.xlsx">Template_Import_Joint.xlsx</a>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Upload</label>
              <div class="col-sm-10">
                <div class="custom-file">
                  <input type="file" name='file' class="custom-file-input" onChange="$('#label3.custom-file-label').html($(this).val())">
                  <label id="label3" class="custom-file-label">Choose file</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="text-right mt-3">
          <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
        </div>
      </div>
      </form>
       <?php //} ?>
    </div>   

  </div>
  
</div>
</div><!-- ini div dari sidebar yang class wrapper -->