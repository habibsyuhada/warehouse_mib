<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>engineering/draw_new_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Drawing Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="drawing_no" placeholder="Drawing Number" required oninput="drawing_no_check(this);">                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Sheet No</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="sheet_no" placeholder="Sheet No" required>                
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Module</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="module">
                    <?php foreach($module_list as $module): ?>
                    <option value="<?php echo $module['mod_id'] ?>"><?php echo $module['mod_desc'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Discipline</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="discipline">
                    <?php foreach($discipline_list as $discipline): ?>
                    <option value="<?php echo $discipline['id'] ?>"><?php echo $discipline['discipline_name'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Received Date</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control datepicker" data-zdp_readonly_element="false" name="received_date" required placeholder="Date : dd-mm-yyyy">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Description</label>
                <div class="col-sm-10">
                  <!-- <input type="text" class="form-control" name="description" placeholder="Description" required>-->
                  <textarea class="form-control" name="description" placeholder="Description" required></textarea>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>engineering/draw_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  var delayTimer;
  function drawing_no_check(input) {
    text = $(input).val();
    console.log(text);
    clearTimeout(delayTimer);
    delayTimer = setTimeout(function() {
      // Do the ajax stuff
      $.ajax({
        url: "<?php echo base_url();?>engineering/drawing_no_check/",
        type: "post",
        data: {
          drawing_no: text
        },
        success: function(data) {
          if(data != 0){
            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
            $(input).after('<div class="invalid-feedback">Duplicate Drawing Number.</div>');
            $('button[name=submit]').prop("disabled", true);
          }
          else{
            $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
          }
          if (!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }
        }
      });
    }, 1500); // Will do the ajax stuff after 1000 ms, or 1 s
  }
</script>