<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>engineering/draw_import_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-hover text-center">
                <thead class="bg-success text-white">
                  <tr>
                    <th>Drawing No</th>
                    <th>Sheet No</th>
                    <th>Module</th>
                    <th>Discipline</th>
                    <th>Material Class</th>
                    <th>Date Received</th>
                    <th>Description</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $drawing_no = array();
                    $no=1; 
                    foreach($sheet as $row):
                      if($no > 1): 

                        $status = '';
                        $disabled = 0;
                       
                        if(!isset($discipline_list[$row['D']])){
                          $status = 'Discipline Not Found';
                          $disabled = 1;
                        }
                        if(!isset($module_list[$row['C']])){
                          $status = 'Module Not Found';
                          $disabled = 1;
                        }
                        if(in_array($row['A'], $draw_duplicate) || in_array($row['A'], $drawing_no)){
                          $status = 'Duplicate Draw Number';
                          $disabled = 2;
                        }
                        $drawing_no[] = $row['A'];

                       if($row['A'] != "" AND $row['C'] != "" AND $row['D'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td class="align-middle"><input type="text" name="drawing_no[]" class="form-control" value="<?php echo $row['A'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="sheet_no[]" class="form-control" value="<?php echo $row['B'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="module[]" class="form-control" value="<?php echo (isset($module_list[$row['C']]) ? $row['C'] : '-') ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="discipline[]" class="form-control" value="<?php echo (isset($discipline_list[$row['D']]) ? $row['D'] : '-') ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    
                    <td class="align-middle"><input type="text" name="received_date[]" class="form-control datepicker" value="<?php echo date('d-m-Y',strtotime($row['E'])) ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="descriptions[]" class="form-control" value="<?php echo $row['F'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><?php echo $status ?></td>
                  </tr>
                  <?php } endif; ?>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>engineering/draw_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
</script>