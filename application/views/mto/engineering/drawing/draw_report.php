<div id="content" class="container-fluid">
  <form method="GET" action="<?php echo base_url();?>engineering/draw_report">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Date From</label>
              <div class="col-sm-10">
                <input type="text" class="form-control datepicker" data-zdp_readonly_element="false" name="df" value="<?php echo (isset($df) ? date('d-m-Y', strtotime($df)) : ''); ?>" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Date To</label>
              <div class="col-sm-10">
                <input type="text" class="form-control datepicker" data-zdp_readonly_element="false" name="dt" value="<?php echo (isset($dt) ? date('d-m-Y', strtotime($dt)) : ''); ?>" required>
              </div>
            </div>


          </div>
        </div>
        <div class="text-right mt-3">
          <button type="submit" name='submit' class="btn btn-primary " title="Submit"><i class="fas fa-search"></i> Find</button>
          <button class="btn btn-success dropdown-toggle" type="button" title="Export" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-file-excel"></i> Download to Excel
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
            <button class="dropdown-item" type="submit"  name='draw_excel'>Drawing Master</button>
            <button class="dropdown-item" type="submit"  name='peacemark_excel'>Peace Mark</button>
            <button class="dropdown-item" type="submit"  name='joint_excel'>Joint</button>
          </div>
        </div>
      </div>
    </div>

  </div>
  </form>

  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <table class="table dataTable table-hover text-center">
              <thead class="bg-success text-white">
                <tr>
                  <th>Drawing No</th>
                  <th>Sheet No</th>
                  <th>Module</th>
                  <th>Discipline</th>
                  <th>Material Class</th>
                  <th>Received Date</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($draw_list as $draw): ?>
                <tr>
                  <td class="align-middle"><?php echo $draw['drawing_no'] ?></td>
                  <td class="align-middle"><?php echo $draw['sheet_no'] ?></td>
                  <td class="align-middle"><?php echo (isset($module_list[$draw['module']]) ? $module_list[$draw['module']] : '-') ?></td>
                  <td class="align-middle"><?php echo (isset($discipline_list[$draw['discipline']]) ? $discipline_list[$draw['discipline']] : '-') ?></td>
                  <td class="align-middle"><?php echo (isset($material_class[$draw['material_class']]) ? $material_class[$draw['material_class']] : '-') ?></td>
                  <td class="align-middle"><?php echo date("d-m-Y", strtotime($draw['received_date'])) ?></td>
                </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
  $('.dataTable').DataTable({
    "lengthChange": false,
    "order": []
  });
</script>