<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>engineering/joint_import_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-hover text-center">
                <thead class="bg-success text-white">
                  <tr>
                    <th>Drawing No</th>
                    <th>Weld Map</th>
                    <th>Joint Number</th>
                    <th>Weld Type</th>
                    <th>Diameter</th>
                    <th>SCH</th>
                    <th>Length</th>
                    <th>Joint Type</th>
                    <th>Test Pack Number</th>
                    <th>WPS Group</th>
                    <th>Spoll Number</th>
                    <th>Remarks</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no=1; 
                    foreach($sheet as $row):
                      if($no > 1): 

                        $status = '';
                        $disabled = 0;
                        if(!in_array($row['A'], $draw_duplicate)){
                          $status = 'Drawing Number Not Found';
                          $disabled = 2;
                        }
                        if(in_array($row['C'], $joint_duplicate[$row['A']])){
                          $status = 'Duplicate Joint Number';
                          $disabled = 2;
                        }

                        if($row['C'] == ''){

                          $status = 'Joint Number empty';
                          $disabled = 2;

                        }

                        if($row['D'] == ''){

                          $status = 'Welding Type Empty';
                          $disabled = 2;

                        }

                        if($row['H'] == ''){

                          $status = 'Joint Type Empty';
                          $disabled = 2;

                        }
                        



                        if($row['A'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">

                    <td class="align-middle"><input type="text" name="drawing_no[]" class="form-control" value="<?php echo $row['A'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="weld_map[]" class="form-control" value="<?php echo $row['B'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="joint_no[]" class="form-control" value="<?php echo $row['C'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="weld_type[]" class="form-control" value="<?php echo $row['D'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="diameter[]" class="form-control" value="<?php echo $row['E'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="sch[]" class="form-control" value="<?php echo $row['F'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="length[]" class="form-control" value="<?php echo $row['G'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="joint_type[]" class="form-control" value="<?php echo $row['H'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="test_pack_no[]" class="form-control" value="<?php echo $row['I'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="wps_group[]" class="form-control" value="<?php echo $row['J'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="spool_no[]" class="form-control" value="<?php echo $row['K'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="remarks[]" class="form-control" value="<?php echo $row['L'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><?php echo $status ?></td>

                  </tr>
                  <?php } endif; ?>
                  
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-right mt-3">
            
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>engineering/draw_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->