<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=Drawing-".date('YmdHis').".xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>

 <table border="1">
 
      <thead>
 
           <tr>
 
                <th style="background: #28a745">Drawing No</th>
                <th style="background: #28a745">Sheet No</th>
                <th style="background: #28a745">Module</th>
                <th style="background: #28a745">Discipline</th>
                <th style="background: #28a745">Material Class</th>
                <th style="background: #28a745">Date Receive</th>

                <th style="background: #28a745">Piece Mark No</th>
                <th style="background: #28a745">Material Grade</th>
                <th style="background: #28a745">Diameter</th>
                <th style="background: #28a745">Sch</th>
                <th style="background: #28a745">Length</th>
                <th style="background: #28a745">Witdh</th>
                <th style="background: #28a745">Test Pack No</th>
                <th style="background: #28a745">Remarks</th>
        
 
           </tr>
 
      </thead>
 
      <tbody>
 
           <?php
              if($draw_list):
              foreach($draw_list as $key => $item):
            ?>
            <tr>
              <td align="center" style="vertical-align:middle"><?php echo $item['drawing_no'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $item['sheet_no'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo (isset($module_list[$item['module']]) ? $module_list[$item['module']] : '-') ?></td>
              <td align="center" style="vertical-align:middle"><?php echo (isset($discipline_list[$item['discipline']]) ? $discipline_list[$item['discipline']] : '-') ?></td>
              <td align="center" style="vertical-align:middle"><?php echo (isset($material_class[$item['material_class']]) ? $material_class[$item['material_class']] : '-') ?></td>
              <td align="center" style="vertical-align:middle"><?php echo (string) $item['received_date'] ?></td>

              <td align="center" style="vertical-align:middle"><?php echo $item['piece_mark_no'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo (isset($material_grade[$item['material_grade']]) ? $material_grade[$item['material_grade']] : '-') ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $item['diameter'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo ($item['sch'] == 'NULL' ? '' : $item['sch']) ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $item['length'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $item['weight'] ?></td>
              <td align="center" style="vertical-align:middle"><?php echo ($item['test_pack_no'] == 'NULL' ? '' : $item['test_pack_no']) ?></td>
              <td align="center" style="vertical-align:middle"><?php echo $item['remarks'] ?></td>
            </tr>
            <?php endforeach; endif; ?>
 
      </tbody>
 
 </table>