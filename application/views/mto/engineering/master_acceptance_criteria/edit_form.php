<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">



              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_acceptance_criteria') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('master_acceptance_criteria/master_acceptance_criteria/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_acceptance_criteria->id ?>'/>
                
               
                <div class="form-group">
                    <label for="acceptance_criteria">Acceptance Criteria*</label>
                    <input class="form-control" type="text" name="acceptance_criteria" value='<?php echo $master_acceptance_criteria->acceptance_criteria ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('acceptance_criteria') ?>
                </div>

                         
                <div class="form-group">
                    <label for="project_id">Project Name*</label>
                    <select name='project_id' class='form-control' required>
                      <?php foreach ($project as $project) { ?>
                         <option value='<?php echo $project['id'] ?>' <?php if($read_cookies[10] == $project['id']){ echo "selected";} else { echo "disabled"; } ?>><?php echo $project['project_name'] ?></option>
                      <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('project_id') ?>
                    </div>
                </div>

                

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
