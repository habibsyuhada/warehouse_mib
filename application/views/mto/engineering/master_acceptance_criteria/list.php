    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_acceptance_criteria/master_acceptance_criteria/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Acceptance Criteria</th>
                    <th>Project Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_acceptance_criteria as $master_acceptance_criteria): ?>
                  <tr>
                      <td width="20">
                      <?php echo $no++; ?>
                    </td>
                    
                    <td width="150">
                      <?php echo $master_acceptance_criteria->acceptance_criteria ?>
                    </td>
                     <td width="150">
                     <?php  $show_project_name = $this->master_acceptance_criteria_model->getProjectById($master_acceptance_criteria->project_id); ?>
                     <?php echo $show_project_name->project_name ?>
                    </td>
                   
                    
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_acceptance_criteria/master_acceptance_criteria/edit/'.$master_acceptance_criteria->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_acceptance_criteria/master_acceptance_criteria/delete/'.$master_acceptance_criteria->id.'/'.$master_acceptance_criteria->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> <?php if($master_acceptance_criteria->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            