<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_area') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('Master_area/Master_area/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_area->id ?>'/>
                
                <div class="form-group">
                    <label for="area_name">Area Name*</label>
                    <input class="form-control" type="text" name="area_name"  value='<?php echo $master_area->area_name ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('area_name') ?>
                  </div>

                         
                <div class="form-group">
                    <label for="status">Area Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='1' <?php if($master_area->status == 1){ echo 'selected'; } ?>>Actived</option>
                      <option value='2' <?php if($master_area->status == 2){ echo 'selected'; } ?>>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
