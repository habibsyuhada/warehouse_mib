<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_class') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('master_class/master_class/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_class->id ?>'/>
                
                  <div class="form-group">
                    <label for="class_code">Class Code*</label>
                    <input class="form-control" type="text" name="class_code" value='<?php echo $master_class->class_code ?>' required />
                  </div>
                  <div class="invalid-feedback">
                      <?php echo form_error('class_code') ?>
                  </div>                 

                  <div class="form-group">
                    <label for="class_name">NDT Percentage Name*</label>
                    <input class="form-control" type="text" name="class_name" value='<?php echo $master_class->class_name ?>' required />
                  </div>
                  <div class="invalid-feedback">
                      <?php echo form_error('class_name') ?>
                  </div>                 
                
                <input class="btn btn-success" type="submit" name="btn" value="Save" />

            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
