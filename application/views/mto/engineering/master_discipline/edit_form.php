<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_discipline') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('Master_discipline/Master_discipline/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_discipline->id ?>'/>
                
                 <div class="form-group">
                    <label for="initial">discipline Initial*</label>
                    <input class="form-control" type="text" name="initial" value='<?php echo $master_discipline->initial ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('initial') ?>
                  </div>

                  <div class="form-group">
                    <label for="discipline_name">discipline Name*</label>
                    <input class="form-control" type="text" name="discipline_name" value='<?php echo $master_discipline->discipline_name ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('discipline_name') ?>
                  </div>

                         
                <div class="form-group">
                    <label for="status">discipline Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='<?php echo $master_discipline->status ?>'><?php if($master_discipline->status == 1){ echo 'Actived'; } else { echo "In-Actived"; } ?></option>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
