    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_discipline/master_discipline/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>discipline Initial</th>
                    <th>discipline Name</th>
                    <th>discipline Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_discipline as $master_discipline): ?>
                  <tr>
                      <td width="150">
                      <?php echo $no++; ?>
                    </td>
                    <td width="150">
                      <?php echo $master_discipline->initial ?>
                    </td>
                      <td width="150">
                      <?php echo $master_discipline->discipline_name ?>
                    </td>
                    <td>
                      <?php if($master_discipline->status == 1){ ?>
                       <span class='btn btn-success'><?php echo "Actived"; ?></span>
                     <?php } else { ?>
                       <span class='btn btn-warning'><?php echo "In-Actived"; ?></span>
                     <?php } ?>
                    </td>
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_discipline/master_discipline/edit/'.$master_discipline->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_discipline/master_discipline/delete/'.$master_discipline->id.'/'.$master_discipline->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> <?php if($master_discipline->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            