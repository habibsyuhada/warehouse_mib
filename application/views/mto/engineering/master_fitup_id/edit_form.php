<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_fitup_id') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('Master_fitup_id/Master_fitup_id/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_fitup_id->id ?>'/>
                
                 <div class="form-group">
                    <label for="fit_up_badge">Fit Up Badge*</label>
                    <input class="form-control" type="text" name="fit_up_badge" value='<?php echo $master_fitup_id->fit_up_badge ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('fit_up_badge') ?>
                  </div>

                  <div class="form-group">
                    <label for="fitup_name">Fit Up Name*</label>
                    <input class="form-control" type="text" name="fitup_name" value='<?php echo $master_fitup_id->fitup_name ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('fitup_name') ?>
                  </div>

                    <div class="form-group">
                    <label for="vsd">Validation Start Date*</label>
                    <input class="form-control" type="date" name="vsd" value='<?php echo $master_fitup_id->vsd ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('vsd') ?>
                  </div>

                      <div class="form-group">
                    <label for="ved">Validation End Date*</label>
                    <input class="form-control" type="date" name="ved" value='<?php echo $master_fitup_id->ved ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('ved') ?>
                  </div>

                         
                <div class="form-group">
                    <label for="status">discipline Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='<?php echo $master_fitup_id->status ?>'><?php if($master_fitup_id->status == 1){ echo 'Actived'; } else { echo "In-Actived"; } ?></option>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
