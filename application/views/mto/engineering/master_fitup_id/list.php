    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_fitup_id/master_fitup_id/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Fitup Badge</th>
                    <th>Fitup Description</th>
                    <th>Validation Start Date</th>
                     <th>Validation End Date</th>
                    <th>Fitup Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_fitup_id as $master_fitup_id): ?>
                  <tr>
                      <td>
                      <?php echo $no++; ?>
                    </td>
                    <td width="100">
                      <?php echo $master_fitup_id->fit_up_badge ?>
                    </td>
                      <td width="150">
                      <?php echo $master_fitup_id->fitup_name ?>
                    </td>
                    </td>
                      <td width="150">
                      <?php echo $master_fitup_id->vsd ?>
                    </td>
                    </td>
                      <td width="150">
                      <?php echo $master_fitup_id->ved ?>
                    </td>
                    <td>
                      <?php if($master_fitup_id->status == 1){ ?>
                       <span class='btn btn-success'><?php echo "Actived"; ?></span>
                     <?php } else { ?>
                       <span class='btn btn-warning'><?php echo "In-Actived"; ?></span>
                     <?php } ?>
                    </td>
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_fitup_id/master_fitup_id/edit/'.$master_fitup_id->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_fitup_id/master_fitup_id/delete/'.$master_fitup_id->id.'/'.$master_fitup_id->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> <?php if($master_fitup_id->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            