<div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">  

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_fitup_id') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>

        
            <div class="card-body">

                <form action="<?php base_url('master_fitup_id/master_fitup_id/add') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
                              
                 <div class="form-group">
                    <label for="fit_up_badge">Fit Up Badge*</label>
                    <input class="form-control" type="text" name="fit_up_badge" placeholder="Fill Up Fitup Badge" required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('fit_up_badge') ?>
                  </div>

                  <div class="form-group">
                    <label for="fitup_name">Fit Up Name*</label>
                    <input class="form-control" type="text" name="fitup_name" placeholder="Fill Up Fitup Name" required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('fitup_name') ?>
                  </div>


                  <div class="form-group">
                    <label for="vsd">Validation Start Date*</label>
                    <input class="form-control" type="date" name="vsd" placeholder="Validation Start Date" required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('vsd') ?>
                  </div>

                      <div class="form-group">
                    <label for="ved">Validation End Date*</label>
                    <input class="form-control" type="date" name="ved" placeholder="Validation End Date" required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('ved') ?>
                  </div>  

                         
                <div class="form-group">
                    <label for="status">Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

            </div>
          </div>
        </div>
      </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
