    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_flaw_type/master_flaw_type/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Film size</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_flaw_type as $master_flaw_type): ?>
                  <tr>
                    <td width="10">
                      <?php echo $no++; ?>
                    </td>
                    <td width="150">
                      <?php echo $master_flaw_type->flaw_type_name; ?>
                    </td>    
                    
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_flaw_type/master_flaw_type/edit/'.$master_flaw_type->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_flaw_type/master_flaw_type/delete/'.$master_flaw_type->id.'/'.$master_flaw_type->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> <?php if($master_flaw_type->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            