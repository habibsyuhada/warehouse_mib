<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_joint_type') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('master_joint_type/master_joint_type/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_joint_type->id ?>'/>
                
                  <div class="form-group">
                    <label for="joint_type_code">Joint Type Code*</label>
                    <input class="form-control" type="text" name="joint_type_code" value='<?php echo $master_joint_type->joint_type_code ?>' required />
                  </div>
                  <div class="invalid-feedback">
                      <?php echo form_error('joint_type_code') ?>
                  </div> 


                  <div class="form-group">
                    <label for="joint_type">Joint Type*</label>
                    <input class="form-control" type="text" name="joint_type" value='<?php echo $master_joint_type->joint_type ?>' required />
                  </div>
                  <div class="invalid-feedback">
                      <?php echo form_error('joint_type') ?>
                  </div>                
                
                <input class="btn btn-success" type="submit" name="btn" value="Save" />

            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
