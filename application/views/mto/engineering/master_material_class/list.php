    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_material_class/master_material_class/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Material Initial</th>
                    <th>Material Class</th>
                    <th>Project Name</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_material_class as $master_material_class): ?>
                  <tr>
                      <td width="20">
                      <?php echo $no++; ?>
                    </td>
                    <td width="150">
                      <?php echo $master_material_class->material_initial ?>
                    </td>
                    <td width="150">
                      <?php echo $master_material_class->material_class ?>
                    </td>
                     <td width="150">
                     <?php  $show_project_name = $this->master_material_class_model->getProjectById($master_material_class->project_id); ?>
                     <?php echo $show_project_name->project_name ?>
                    </td>
                    <td>
                      <?php if($master_material_class->status == 1){ ?>
                       <span class='btn btn-success'><?php echo "Actived"; ?></span>
                     <?php } else { ?>
                       <span class='btn btn-warning'><?php echo "In-Actived"; ?></span>
                     <?php } ?>
                    </td>
                    
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_material_class/master_material_class/edit/'.$master_material_class->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_material_class/master_material_class/delete/'.$master_material_class->id.'/'.$master_material_class->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> <?php if($master_material_class->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            