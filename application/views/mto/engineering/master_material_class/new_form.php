<div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">  

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_material_class') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>

        
            <div class="card-body">

                <form action="<?php base_url('master_material_class/master_material_class/add') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
                              
                <div class="form-group">
                    <label for="material_initial">Material Initial*</label>
                    <input class="form-control" type="text" name="material_initial" placeholder="Fill Up Material Initial" required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('material_initial') ?>
                </div>

                <div class="form-group">
                    <label for="material_class">Material Class*</label>
                    <input class="form-control" type="text" name="material_class" placeholder="Fill Up Material Class"  required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('material_class') ?>
                </div>

                         
                <div class="form-group">
                    <label for="project_id">Project Name*</label>
                    <select name='project_id' class='form-control' required>
                      <option value=''>~ Choice Project ~</option>
                      <?php foreach ($project as $project) { ?>
                        <option value='<?php echo $project['id'] ?>' <?php if($master_material_class->project_id == $project['id']){ echo "selected";} ?>><?php echo $project['project_name'] ?></option>
                      <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('project_id') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="status">Material Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

            </div>
          </div>
        </div>
      </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
