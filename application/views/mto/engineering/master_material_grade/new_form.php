<div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">  

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_material_grade') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>

        
            <div class="card-body">

                <form action="<?php base_url('master_material_class/master_material_class/add') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
                              
                <input  type="hidden" name="id" value='<?php echo $master_material_grade->id ?>'/>
                
                <div class="form-group">
                    <label for="material_grade">Material Grade*</label>
                    <input class="form-control" type="text" name="material_grade" placeholder="Fill Up Material Grade" required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('material_grade') ?>
                </div>
                                       
                <div class="form-group">
                    <label for="material_class">Material Class*</label>
                    <select name='material_class' class='form-control' required>
                      <?php foreach ($material_class as $material_class) { ?>
                        <option value='<?php echo $material_class['id'] ?>' <?php if($master_material_grade->material_class == $material_class['id']){ echo "selected";} ?>><?php echo $material_class['material_class'] ?></option>
                      <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('material_class') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="status">Material Grade*</label>
                    <select name='status' class='form-control' required>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

            </div>
          </div>
        </div>
      </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
