<div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">  

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a href="<?php echo site_url('master_module') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>

        
            <div class="card-body">

                <form action="<?php base_url('master_module/master_module/add') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
                              
                 <div class="form-group">
                    <label for="mod_desc">Module Desciption*</label>
                    <input class="form-control" type="text" name='mod_desc' value='<?php echo $master_module->mod_desc ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('mod_desc') ?>
                </div>
                         
                <div class="form-group">
                    <label for="details">Status Module*</label>
                    <select name='status' class='form-control' required>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

            </div>
          </div>
        </div>
      </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
