    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_ndt_percentage/master_ndt_percentage/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NDT Percentage Code</th>
                    <th>NDT Percentage Name</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_ndt_percentage as $master_ndt_percentage): ?>
                  <tr>
                    <td width="10">
                      <?php echo $no++; ?>
                    </td>
                    <td width="150">
                      <?php echo $master_ndt_percentage->ndt_percentage_code; ?>
                    </td>                    
                    <td width="150">
                      <?php echo $master_ndt_percentage->ndt_percentage_name; ?>
                    </td>
                    
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_ndt_percentage/master_ndt_percentage/edit/'.$master_ndt_percentage->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_ndt_percentage/master_ndt_percentage/delete/'.$master_ndt_percentage->id.'/'.$master_ndt_percentage->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i>   <?php if($master_ndt_percentage->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a> 
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            