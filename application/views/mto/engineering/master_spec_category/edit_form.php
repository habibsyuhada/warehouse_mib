<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_spec_category') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('Master_spec_category/Master_spec_category/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_spec_category->id ?>'/>
                
                 <div class="form-group">
                    <label for="spec_code">Spec Code*</label>
                    <input class="form-control" type="text" name="spec_code" value='<?php echo $master_spec_category->spec_code ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('spec_code') ?>
                  </div>

                  <div class="form-group">
                    <label for="spec_desc">Spec Description*</label>
                    <input class="form-control" type="text" name="spec_desc" value='<?php echo $master_spec_category->spec_desc ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('spec_desc') ?>
                  </div>

                         
                <div class="form-group">
                    <label for="status">Spec Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='<?php echo $master_spec_category->status ?>'><?php if($master_spec_category->status == 1){ echo 'Actived'; } else { echo "In-Actived"; } ?></option>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
