<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_surface_condition') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('master_surface_condition/master_surface_condition/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $master_surface_condition->id ?>'/>
              
                  <div class="form-group">
                    <label for="surface_condition">Surface Condition *</label>
                    <input class="form-control" type="text" name="surface_condition" value='<?php echo $master_surface_condition->surface_condition ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('surface_condition') ?>
                  </div>

                     
              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
