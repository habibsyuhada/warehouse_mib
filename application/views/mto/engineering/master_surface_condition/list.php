    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_surface_condition/master_surface_condition/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Surface Condition</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_surface_condition as $master_surface_condition): ?>
                  <tr>
                      <td width="20">
                      <?php echo $no++; ?>
                    </td>
                  
                      <td width="150">
                      <?php echo $master_surface_condition->surface_condition ?>
                    </td>
                   
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_surface_condition/master_surface_condition/edit/'.$master_surface_condition->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_surface_condition/master_surface_condition/delete/'.$master_surface_condition->id.'/'.$master_surface_condition->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Delete</a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            