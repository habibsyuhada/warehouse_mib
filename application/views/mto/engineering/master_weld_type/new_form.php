<div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">  

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_weld_type') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>

        
            <div class="card-body">

                <form action="<?php base_url('master_weld_type/master_weld_type/add') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>

                  <div class="form-group">
                    <label for="weld_type_code">Welding Type Code*</label>
                    <input class="form-control" type="text" name="weld_type_code" placeholder="Fill Up Welding Code Type" required />
                  </div>
                  <div class="invalid-feedback">
                      <?php echo form_error('weld_type_code') ?>
                  </div>  
                              
                 <div class="form-group">
                    <label for="weld_type">Welding Type*</label>
                    <input class="form-control" type="text" name="weld_type" placeholder="Fill Up Welding Type" required />
                  </div>
                  <div class="invalid-feedback">
                      <?php echo form_error('weld_type') ?>
                  </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

            </div>
          </div>
        </div>
      </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
