<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('master_wps') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('master_wps/master_wps/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="wps_id" value='<?php echo $master_wps->wps_id ?>'/>
                
                <div class="form-group">
                    <label for="wps_code">WPS Code*</label>
                    <input class="form-control" type="text" name="wps_code" value='<?php echo $master_wps->wps_code ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('wps_code') ?>
                </div>

                <div class="form-group">
                    <label for="wps_desc">WPS Description*</label>
                    <input class="form-control" type="text" name="wps_desc" value='<?php echo $master_wps->wps_desc ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('wps_desc') ?>
                </div>

                <div class="form-group">
                    <label for="wps_discipline">WPS Discipline*</label>
                    <select name='wps_discipline' class='form-control' required>
                    <?php foreach ($master_discipline as $value) { ?>
                      <option value='<?php echo  $value->discipline_name; ?>' <?php if($master_wps->wps_discipline == $value->discipline_name){ echo "selected";} ?>><?php echo  $value->discipline_name; ?></option>
                    <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('wps_discipline') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="wps_module">WPS Module*</label>
                    <select name='wps_module' class='form-control' required>
                    <?php foreach ($master_module as $value) { ?>
                      <option value='<?php echo  $value->mod_desc; ?>' <?php if($master_wps->wps_module == $value->wps_module){ echo "selected";} ?>><?php echo  $value->mod_desc; ?></option>
                    <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('wps_module') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="wel_code">Assign Process*</label>
                    <table>
                      <tr>
                        <td>
                            <input type='checkbox' name='assign1' value='GTAW' <?php if($master_wps->assign1 == 'GTAW'){ echo 'checked'; } ?>>
                        </td>
                        <td> 
                          &nbsp;&nbsp;GTAW 
                        </td>
                      </tr> 
                      <tr>
                        <td> 
                        <input type='checkbox' name='assign2' value='GMAW' <?php if($master_wps->assign2 == 'GMAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;GMAW</td>
                      
                      </tr>
                      <tr>
                        <td> 
                        <input type='checkbox' name='assign3' value='SMAW' <?php if($master_wps->assign3 == 'SMAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;SMAW</td>
                      
                      </tr>
                      <tr>
                        <td> 
                        <input type='checkbox' name='assign4' value='FCAW' <?php if($master_wps->assign4 == 'FCAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;FCAW</td>
                       
                      </tr>
                      <tr>
                        <td> 
                        <input type='checkbox'  name='assign5' value='SAW' <?php if($master_wps->assign5 == 'SAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;SAW</td>
                        
                      </tr>
                      
                      
                    </table>

                </div>
                                    
                <div class="form-group">
                    <label for="status">WPS Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='<?php echo $master_wps->status ?>'><?php if($master_wps->status == 1){ echo 'Actived'; } else { echo "In-Actived"; } ?></option>
                      <option value='1'>Actived</option>
                      <option value='2'>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
