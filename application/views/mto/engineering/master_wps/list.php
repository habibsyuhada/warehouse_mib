    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('master_wps/master_wps/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>WPS Code</th>
                    <th>WPS Description</th>
                    <th>WPS Discipline</th>
                    <th>WPS Module</th>
                     <th>Assign Process</th>
                    <th>Status</th>
                   
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($master_wps as $master_wps): ?>
                  <tr>
                      <td width="10">
                      <?php echo $no++; ?>
                    </td>
                    <td width="200">
                      <?php echo $master_wps->wps_code; ?>
                    </td>
                     <td width="200">
                      <?php echo $master_wps->wps_desc; ?>
                    </td>
                    <td width="200">
                      <?php echo $master_wps->wps_discipline; ?>
                    </td>
                     <td width="200">
                      <?php echo $master_wps->wps_module; ?>
                    </td>
                    <td>
                      <?php echo $master_wps->assign1; ?>
                      <?php echo $master_wps->assign2; ?>
                      <?php echo $master_wps->assign3; ?>
                      <?php echo $master_wps->assign4; ?>
                      <?php echo $master_wps->assign5; ?>
                    </td>
                     <td>
                      <?php if($master_wps->status == 1){ ?>
                       <span class='btn btn-success'><?php echo "Actived"; ?></span>
                     <?php } else { ?>
                       <span class='btn btn-warning'><?php echo "In-Actived"; ?></span>
                     <?php } ?>
                    </td>
                    
                   
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('master_wps/master_wps/edit/'.$master_wps->wps_id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                     <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('master_wps/master_wps/delete/'.$master_wps->wps_id.'/'.$master_wps->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i>  <?php if($master_wps->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            