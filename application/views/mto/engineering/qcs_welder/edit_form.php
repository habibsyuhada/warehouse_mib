<div class="container" style="background-color: whitesmoke">
     
    <div class="row">
      <div class="container-fluid">
            <div class="my-3 p-3 bg-white rounded shadow-sm">

              <?php if ($this->session->flashdata('success')): ?>
                 <div class="alert alert-success" role="alert">
                  <?php echo $this->session->flashdata('success'); ?>
                </div>
              <?php endif; ?>

              <a class='btn btn-warning' href="<?php echo site_url('qcs_welder') ?>">
                  <i class="fas fa-arrow-left"></i> Back
              </a>
        
          <div class="card-body">

            <form action="<?php base_url('qcs_welder/qcs_welder/edit') ?>" method="post" enctype="multipart/form-data"  autocomplete='off'>
              
                <input  type="hidden" name="id" value='<?php echo $qcs_welder->id ?>'/>
                
                <div class="form-group">
                    <label for="wel_code">Welder Code*</label>
                    <input class="form-control" type="text" name="wel_code" id="wel_code" value='<?php echo $qcs_welder->wel_code ?>' readonly />
                     <span id="text_alert"></span>
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('wel_code') ?>
                </div>

                <div class="form-group">
                    <label for="welder_badge">Welder Badge*</label>
                    <input class="form-control" type="text" name="welder_badge"  id="wel_code" value='<?php echo $qcs_welder->welder_badge ?>' required />
                     <span id="text_alert_welder"></span>
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('welder_badge') ?>
                </div>

                <div class="form-group">
                    <label for="project_id">Project Name*</label>
                    <select name='project_id' class='form-control' required>
                      <?php foreach ($project as $project) { ?>
                        <option value='<?php echo $project['id'] ?>' <?php if($master_material_class->project_id == $project['id']){ echo "selected";} ?>><?php echo $project['project_name'] ?></option>
                      <?php } ?>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('project_id') ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="wel_name">Welder Name*</label>
                    <input class="form-control" type="text" name="wel_name"  value='<?php echo $qcs_welder->wel_name ?>' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('wel_name') ?>
                </div>

                <div class="form-group">
                    <label for="cwm">Class Welder Material*</label>
                    

                    <select name='cwm' class='form-control' required>
                      <?php foreach ($material_class as $material_class) { ?>
                        <option value='<?php echo $material_class->id ?>' <?php if($qcs_welder->cwm == $material_class->id){ echo "selected";} ?>><?php echo $material_class->material_class ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('cwm') ?>
                </div>

                <div class="form-group">
                    <label for="wel_code">Discipline*</label>
                    <select name='discipline' class='form-control'>
                       <?php foreach ($master_discipline as $key) { ?>
                    
                        <option value='<?php echo $key['id']; ?>' <?php if($key['id'] == $qcs_welder->discipline){ echo 'selected';} ?> ><?php echo $key['discipline_name']; ?></option>
                    
                      <?php }  ?>
                    </select>
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('discipline') ?>
                </div>
                <div class="form-group">
                    <label for="wel_code">Assign Process*</label>
                    <table>
                      <tr>
                        <td>
                            <input type='checkbox' name='assign1' value='GTAW' <?php if($qcs_welder->assign1 == 'GTAW'){ echo 'checked'; } ?>>
                        </td>
                        <td> 
                          &nbsp;&nbsp;GTAW 
                        </td>
                      </tr> 
                      <tr>
                        <td> 
                        <input type='checkbox' name='assign2' value='GMAW' <?php if($qcs_welder->assign2 == 'GMAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;GMAW</td>
                      
                      </tr>
                      <tr>
                        <td> 
                        <input type='checkbox' name='assign3' value='SMAW' <?php if($qcs_welder->assign3 == 'SMAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;SMAW</td>
                      
                      </tr>
                      <tr>
                        <td> 
                        <input type='checkbox' name='assign4' value='FCAW' <?php if($qcs_welder->assign4 == 'FCAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;FCAW</td>
                       
                      </tr>
                      <tr>
                        <td> 
                        <input type='checkbox'  name='assign5' value='SAW' <?php if($qcs_welder->assign5 == 'SAW'){ echo 'checked'; } ?>>
                        </td>
                        <td>&nbsp;&nbsp;SAW</td>
                        
                      </tr>
                      
                      
                    </table>

                </div>
               
                
                <div class="form-group">
                    <label for="vsd">Validity Start Date*</label>
                    <input class="form-control" type="date" name="vsd"  value='<?php echo $qcs_welder->vsd ?>' id='datepicker' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('vsd') ?>
                </div>
                
                <div class="form-group">
                    <label for="ved">Validity End Date*</label>
                    <input class="form-control" type="date" name="ved"  value='<?php echo $qcs_welder->ved ?>' id='datepicker' required />
                </div>
                <div class="invalid-feedback">
                      <?php echo form_error('ved') ?>
                </div>

                <div class="form-group">
                    <label for="disqua">Welder Qualified Status*</label>
                    <select name='disqua' class='form-control' required>
                      <option value='1' <?php if($qcs_welder->disqua == 1){ echo 'selected'; } ?>>1</option>
                      <option value='2' <?php if($qcs_welder->disqua == 2){ echo 'selected'; } ?>>2</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('disqua') ?>
                    </div>
                </div>

                         
                <div class="form-group">
                    <label for="status">Welder Status*</label>
                    <select name='status' class='form-control' required>
                      <option value='1' <?php if($qcs_welder->status == 1){ echo 'selected'; } ?>>Actived</option>
                      <option value='2' <?php if($qcs_welder->status == 2){ echo 'selected'; } ?>>In-Actived</option>
                    </select>
                    <div class="invalid-feedback">
                      <?php echo form_error('status') ?>
                    </div>
                </div>

              <input class="btn btn-success" type="submit" name="btn" value="Save" />
            </form>

      </div>

            </div>
      </div>
    </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
