    <div class="container-fluid" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">


       <div class="card-header">
            <a class='btn btn-primary' href="<?php echo site_url('qcs_welder/qcs_welder/add') ?>"><i class="fas fa-plus"></i> Add New</a>
       </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Welder Code</th>
                    <th>Welder Badge</th>
                    <th>Project ID</th>
                    <th>Welder Name</th>
                    <th>Class Welder Material</th>
                    <th>Discipline</th>
                    <th>Assign Process</th>
                    <th>Validity Start Date</th>
                    <th>Validity End Date</th>
                    <th>Qualify</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($qcs_welder as $qcs_welder): ?>
                  <tr>
                    <td width="20">
                      <?php echo $no++; ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->wel_code ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->welder_badge ?>
                    </td>
                    <td>
                      <?php echo (isset($project_list[$qcs_welder->project_id]) ? $project_list[$qcs_welder->project_id] : '-') ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->wel_name ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->cwm ?>
                    </td>
                    <td>
                      <?php echo (isset($discipline_list[$qcs_welder->discipline]) ? $discipline_list[$qcs_welder->discipline] : '-') ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->assign1 ?>
                      <?php echo $qcs_welder->assign2 ?>
                      <?php echo $qcs_welder->assign3 ?>
                      <?php echo $qcs_welder->assign4 ?>
                      <?php echo $qcs_welder->assign5 ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->vsd ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->ved ?>
                    </td>
                    <td>
                      <?php echo $qcs_welder->disqua ?>
                    </td>
                    <td>
                      <?php if($qcs_welder->status == 1){ ?>
                       <span class='btn btn-success'><?php echo "Actived"; ?></span>
                     <?php } else { ?>
                       <span class='btn btn-warning'><?php echo "In-Actived"; ?></span>
                     <?php } ?>
                    </td>

                    
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo site_url('qcs_welder/qcs_welder/edit/'.$qcs_welder->id) ?>"
                       class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo site_url('qcs_welder/qcs_welder/delete/'.$qcs_welder->id.'/'.$qcs_welder->status_delete) ?>')"
                       href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i> <?php if($qcs_welder->status_delete == 1){ ?>Delete <?php } else { echo "Actived"; } ?></a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            