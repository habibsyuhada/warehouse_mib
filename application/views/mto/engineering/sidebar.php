<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
  <ul class="list-unstyled components">
    
    <li>
      <center>
        <?php $str_project = $this->mrir_mod->search_project_data($read_cookies[10]); ?>
        <span style="text-shadow: 2px 2px #454545; padding: 15px; border-radius: 50px 20px;background-color:#0017ed;font-size: 15px;font-family: arial;font-weight: bold;">Project <?php echo $str_project[0]['project_name']; ?></span>
      </center>  
      <br/>
      
      <li>
        <a href="<?php echo base_url();?>engineering/draw_list"><i class="fas fa-file-alt"></i> &nbsp; Drawing List</a>
      </li>
    </li>

    <!--
    <li>
      <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle collapsed">
        <i class="fas fa-home"></i> Drawing
      </a>
      <ul class="collapse list-unstyled" id="homeSubmenu2">
        <li>
          <a href="<?php //echo base_url();?>engineering/draw_list"><i class="fas fa-caret-right"></i> Drawing List</a>
        </li>
        <li>
          <a href="<?php// echo base_url();?>engineering/draw_new"><i class="fas fa-caret-right"></i> Add New Drawing</a>
        </li>
        <li>
          <a href="<?php// echo base_url();?>engineering/draw_import"><i class="fas fa-caret-right"></i> Import Drawing</a>
        </li>
      </ul>
    </li> -->
    <!--
    <li>
      <a href="#">
        <i class="fas fa-chart-line"></i> MTO
      </a>
    </li>-->

    <li>
      <a href="<?php echo base_url();?>mto_add"><i class="fas fa-plus"></i> &nbsp; Add New MTO</a>
    </li>

    <li>
      <a href="<?php echo base_url();?>mto_list"><i class="fas fa-file-alt"></i> &nbsp; MTO List</a>
    </li>

    <br>

    <li>
      <a href="<?php echo base_url();?>mto_category_list"><i class="fas fa-minus-square"></i> &nbsp; MTO Category</a>
    </li>

    <br>

    <li>
      <a href="<?php echo base_url();?>material_catalog_list"><i class="fas fa-minus-square"></i> &nbsp; Material Catalog</a>
    </li>

  </ul>
</nav>