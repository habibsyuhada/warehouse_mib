<?php 
  $mto = $mto_list[0];
?>

<style>

.table_ss {
  font-size: 15px !important;
  width: 1000 !important;
}

/* The container */
.containerx {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.containerx input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
}

/* On mouse-over, add a grey background color */
.containerx:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.containerx input:checked ~ .checkmark {
  background-color: #007339;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.containerx input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.containerx .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>

<style>

  .radio-toolbar input[type="radio"] {
  display: none;
}

.radio-toolbar label {
  display: inline-block;
  background-color: #e3e3e3;
  padding: 4px 11px;
  font-family: Arial;
  font-size: 16px;
  cursor: pointer;
}

.radio-toolbar input[type="radio"]:checked+label {
  background-color: #e3e3e3;
}

</style>

<div id="content" class="container-fluid">  
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">
              <form method="POST" action="<?php echo base_url();?>mto/mto_approval_proccess">
                <input type="hidden" name="mto_id" value="<?= $mto['id'] ?>">

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">MTO Number</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?= $mto['mto_number'] ?>" readonly required>              
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Project</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?= $name_project ?>" readonly required>              
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Module</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?= $module_name ?>" readonly required>              
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Priority</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?= $priority_name ?>" readonly required>              
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-sm-2 col-form-label">Remarks</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" disabled><?= $mto['remarks'] ?></textarea>
                  </div>
                </div>

            </div>
          </div>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <div class="radio-toolbar">
                <div class="form-check form-check-inline text-success">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="approval" value="0" style="width: 17px; height: 17px">
                  <b>Approve All</b></label>
                </div>
                <div class="form-check form-check-inline text-danger">
                  <label class="form-check-label">
                    <input class="form-check-input" type="radio" name="approval" value="1" style="width: 17px; height: 17px">
                  <b>Reject All</b></label>
                </div>
                <div class="form-check form-check-inline text-secondary">
                  <label class="form-check-label"><input class="form-check-input" type="radio" name="approval" value="3" style="width: 17px; height: 17px">
                 <b>Uncheck All</b></label>
                </div>
              </div>
              </br>

              <table class="table table-bordered datatable table_ss">
                <thead>
                  <tr bgcolor="#008060" style="color: white; text-align: center;">
                    <th rowspan="2" style="vertical-align:middle;width:100px;">Approval</th>
                    <th rowspan="2" style="vertical-align:middle;">Reject Remarks</th>
                    <th rowspan="2" style="vertical-align:middle;">Material Code</th>
                    <th rowspan="2" style="vertical-align:middle;">Description</th>
                    <th colspan="2" style="vertical-align:middle;">Material</th>
                    <th colspan="6" style="vertical-align:middle;">Size</th>
                    <th rowspan="2" style="vertical-align:middle;">Discipline</th>
                    <th rowspan="2" style="vertical-align:middle;">Nett Area (M2)</th>
                    <th rowspan="2" style="vertical-align:middle;">Area per Plate (M2)</th>
                    <th rowspan="2" style="vertical-align:middle;">Nett Length (M)</th>
                    <th rowspan="2" style="vertical-align:middle;">Unit Wt kg/m</th>
                    <th rowspan="2" style="vertical-align:middle;">Cont</th>
                    <th rowspan="2" style="vertical-align:middle;">Total (pcs)</th>
                    <th rowspan="2" style="vertical-align:middle;">Certification (3.1/3.2)</th>
                    <th colspan="2" style="vertical-align:middle;">Weight (MT)</th>
                    <th rowspan="2" style="vertical-align:middle;">Remarks</th>
                  </tr>
                  <tr bgcolor="#008060" style="color: white; text-align: center;">
                    <th style="vertical-align:middle;">Grade</th>
                    <th style="vertical-align:middle;">Class</th>

                    <th style="vertical-align:middle;">Thk (mm)</th>
                    <th style="vertical-align:middle;">Width (M)</th>
                    <th style="vertical-align:middle;">Length (M)</th>
                    <th style="vertical-align:middle;">Weight</th>
                    <th style="vertical-align:middle;">OD</th>
                    <th style="vertical-align:middle;">Schedule</th>

                    <th style="vertical-align:middle;">Per Piece</th>
                    <th style="vertical-align:middle;">Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                  <?php 
                    $count = 0;
                    foreach($mto_detail_list as $mto_detail){
                  ?>
                    <td>
                      <?php if($mto_detail['status'] == 3){
                        echo '<span class="badge badge-success">Approved</span>';
                      } else if($mto_detail['status'] == 2){
                        echo '<span class="badge badge-danger">Rejected</span>';
                      } else { ?>
                      <div class="form-check form-check-inline text-success">
                        <input class="form-check-input approve" type="radio" name="approve[<?php echo $count; ?>]" value="A <?= $mto_detail['id_mto_detail'] ?>" style="width: 17px; height: 17px">
                        <label class="form-check-label">Approve</label>
                      </div>
                      <div class="form-check form-check-inline text-danger">
                        <input class="form-check-input reject" type="radio" name="approve[<?php echo $count; ?>]" value="R <?= $mto_detail['id_mto_detail'] ?>" style="width: 17px; height: 17px">
                        <label class="form-check-label">Reject</label>
                      </div>
                      <?php } ?>
                    </td>
                    <td><textarea class="form-control" name="reject_remarks[]"></textarea></td>
                    <td><?= $mto_detail['code_material'] ?></td>
                    
                    <td><?= $mto_detail['material'] ?></td>
                    <td><?= $mto_detail['material_grade'] ?></td>
                    <td><?= $mto_detail['name_material_class'] ?></td>
                    
                    <td><?= $mto_detail['thk_mm'] ?></td>
                    <td><?= $mto_detail['width_m'] ?></td>
                    <td><?= $mto_detail['length_m'] ?></td>
                    <td><?= $mto_detail['weight'] ?></td>
                    <td><?= $mto_detail['od'] ?></td>
                    <td><?= $mto_detail['sch'] ?></td>
                    
                    <td><?= $mto_detail['discipline_name'] ?></td>
                    
                    <td><?= $mto_detail['nett_area'] ?></td>
                    <td><?= $mto_detail['area_per_plate'] ?></td>
                    <td><?= $mto_detail['nett_length'] ?></td>
                    <td><?= $mto_detail['unit_wt'] ?></td>
                    <td><?= $mto_detail['cont'] ?></td>
                    <td><?= $mto_detail['total_pcs'] ?></td>
                    <td><?= $mto_detail['certification'] ?></td>
                    <td><?= $mto_detail['weight_per_piece'] ?></td>
                    <td><?= $mto_detail['weight_total'] ?></td>
                    <td><?= $mto_detail['remarks'] ?></td>
                  </tr>
                  <?php $count++; } ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="mt-3">
            <?php if($read_permission[9] == 1){ ?>
              <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
            <?php } ?>
            <button type="button" class="btn btn-secondary" onclick="goBack()"><i class="fa fa-close"></i> Close</button>
          </div>

          </form>
     </div>
      </div>
       </div>
        </div> 

</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $(document).ready(function(){

    $('input[name="approval"]').click(function(){
      var approve_val = $(this).val();
      
      if(approve_val == 0){

        $('.reject').removeAttr('checked');
        $('.approve').prop('checked', true);

      } else if(approve_val == 1){

        $('.approve').removeAttr('checked');
        $('.reject').prop('checked', true);

      } else {
        $('.approve').prop('checked', false);
        $('.reject').prop('checked', false);
      }
    });
  });

  function action_material(url,id,_val){
    $.ajax({
      url: url,
      type: "post",
      data: {
        'id' : id,
        'val' : _val
      },
      success: function(data){

      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });
  }

</script>