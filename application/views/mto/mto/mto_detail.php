<?php
  $mto = $mto_list[0];
?>
<style type="text/css">
  .nav-link{
    color: #000;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #007bff;
    background: #fff;
    border-bottom: 2px solid #007bff;
    border-radius: 0px;
  }
</style>
<div id="content" class="container-fluid" style="overflow: auto;">

  <input type="hidden" name="mto_id" value="<?= $mto['id'] ?>">
 
  <div class="row">

    <div class="col-md-12">
       <div class="overflow-auto text-muted  ">
          <h6>
            <ol class="breadcrumb bg-white">MTO Number : <?= $mto_number ?></ol>
          </h6>
       </div>
    </div>

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == '' ? 'active' : '') ?>" data-toggle="pill" href="#pills-detail">Material Draft</a>
          </li>

          <!-- <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'pc' ? 'active' : '') ?>" data-toggle="pill" href="#pills-piecemark">Import Material Draft</a>
          </li> -->
        </ul>
        <div class="overflow-auto media text-muted py-3 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="tab-content">

              <!-- Drawing Detail -->
              <div class="tab-pane fade <?php echo ($t == '' ? 'show active' : '') ?>" id="pills-detail">

                <form method="POST" id="material_draft">

                  <input type="hidden" name="mto_id" value="<?= $mto['id'] ?>">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Project :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="project" placeholder="Project" value="<?php echo $project_name ?>" required disabled>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Module :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="module" placeholder="Module" value="<?php echo $module_name ?>" required disabled>
                        </div>
                      </div>
                    </div>                    
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Priority :</label>
                        <div class="col-xl">
                          <input type="text" class="form-control" name="priority" value="<?php echo $priority_name ?>" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Material Code :</label>
                        <div class="col-xl">
                          <input type="hidden" name="id_material_cat">
                          <input type="text" class="form-control" name="material_cat" onblur="check_material_catalog(this)" onfocus="$(this).removeClass('is-valid is-invalid'); $('#text_alert').addClass('d-none')">
                          <span id="text_alert" class="d-none text-danger">Error: Material Not Found</span>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Discipline :</label>
                        <div class="col-xl">
                          <select class="form-control" name="discipline">
                            <option value=""> - </option>
                            <?php foreach($discipline_list as $discipline){ ?>
                              <option value="<?= $discipline['id'] ?>"><?= $discipline['discipline_name'] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Description :</label>
                        <div class="col-xl">
                          <input type="text" name="description" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Material Grade :</label>
                        <div class="col-xl">
                          <input type="text" name="material_grade" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Material Class :</label>
                        <div class="col-xl">
                          <input type="text" name="material_class" class="form-control" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Thk (mm):</label>
                        <div class="col-xl">
                          <input type="text" name="thk_mm" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Width (M) :</label>
                        <div class="col-xl">
                          <input type="text" name="width_m" class="form-control" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Length (M) :</label>
                        <div class="col-xl">
                          <input type="text" name="length_m" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Weight (M) :</label>
                        <div class="col-xl">
                          <input type="text" name="weight" class="form-control" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">OD (M):</label>
                        <div class="col-xl">
                          <input type="text" name="od" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Sch (M):</label>
                        <div class="col-xl">
                          <input type="text" name="sch" class="form-control" disabled>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Nett Area (M2) :</label>
                        <div class="col-xl">
                          <input type="number" name="nett_area" class="form-control">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Area per Plate (M2) :</label>
                        <div class="col-xl">
                          <input type="text" name="area_per_plate" class="form-control" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Nett Length (M) :</label>
                        <div class="col-xl">
                          <input type="number" name="nett_length" class="form-control">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Unit Wt (kg/m) :</label>
                        <div class="col-xl">
                          <input type="number" name="unit_wt" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Cont (in %) :</label>
                        <div class="col-xl">
                          <input type="number" name="cont" class="form-control" oninput="autofill_total_pcs()">
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Total (pcs) :</label>
                        <div class="col-xl">
                          <input type="number" name="total_pcs" class="form-control" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Certification :</label>
                        <div class="col-xl">
                          <select class="form-control" name="certification">
                            <option value=""> - </option>
                            <?php foreach($certification_list as $certification){ ?>
                              <option value="<?= $certification['id'] ?>"><?= $certification['certification'] ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Weight (MT) Per Piece :</label>
                        <div class="col-xl">
                          <input type="number" name="weight_per_piece" class="form-control" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Weight (MT) Total :</label>
                        <div class="col-xl">
                          <input type="number" name="weight_total" class="form-control" readonly>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">Remarks :</label>
                        <div class="col-xl">
                          <textarea class="form-control" name="remarks"></textarea>
                        </div>
                      </div>
                    </div>

                    <div class="col-md">
                      <div class="form-group row">
                        
                      </div>
                    </div>
                  </div>

                  <div class="col-xl text-right">
                    <?php if($read_permission[3] == 1){ ?>
                      <button type="button" name='submit' value='submit' class="btn btn-success" title="Submit" onclick="submit_detail()"><i class="fa fa-check"></i> Submit</button>
                    <?php } ?>
                  </div>

                </form>

                <br>

                <!-- TABLE  -->
                <table class="table dataTable table-hover table-bordered text-center" id="mto_material_detail">
                  <thead class="bg-green-smoe text-white">
                    <tr>
                      <th rowspan="2" style="vertical-align: middle;">Material Code</th>
                      <th rowspan="2" style="vertical-align: middle;">Description</th>
                      <th colspan="2" style="vertical-align: middle;">Material</th>
                      <th colspan="6" style="vertical-align: middle;">Size</th>
                      <th rowspan="2" style="vertical-align: middle;">Discipline</th>
                      <th rowspan="2" style="vertical-align: middle;">Nett Area (M2)</th>
                      <th rowspan="2" style="vertical-align: middle;">Area per Plate (M2)</th>
                      <th rowspan="2" style="vertical-align: middle;">Nett Length (M)</th>
                      <th rowspan="2" style="vertical-align: middle;">Unit Wt kg/m</th>
                      <th rowspan="2" style="vertical-align: middle;">Cont (in %)</th>
                      <th rowspan="2" style="vertical-align: middle;">Total (pcs)</th>
                      <th rowspan="2" style="vertical-align: middle;">Certification (3.1 / 3.2)</th>
                      <th colspan="2" style="vertical-align: middle;">Weight (MT)</th>
                      <th rowspan="2" style="vertical-align: middle;">Remarks</th>
                      <th rowspan="2" style="vertical-align: middle;">Action</th>
                    </tr>
                    <tr>
                      <th style="vertical-align: middle;">Grade</th>
                      <th style="vertical-align: middle;">Class</th>

                      <th style="vertical-align: middle;">Thk (mm)</th>
                      <th style="vertical-align: middle;">Width (M)</th>
                      <th style="vertical-align: middle;">Length (M)</th>
                      <th style="vertical-align: middle;">Weight (M)</th>
                      <th style="vertical-align: middle;">OD (M)</th>
                      <th style="vertical-align: middle;">Sch (M)</th>

                      <th style="vertical-align: middle;">Per Piece</th>
                      <th style="vertical-align: middle;">Total</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <!-- Drawing Detail END -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">

  $('#mto_material_detail').DataTable({
    "scrollX": true,
    "language": { 
      "infoFiltered": "" 
    },
    "paging": true,
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax": {
        "url": "<?php echo base_url();?>mto/mto_detail_list_json/<?= $mto['id'] ?>",
        "type": "POST",
    },
    "columnDefs": [{
      "targets": [0],
      "orderable": true,
    },],
  });

  $('input[name="material_cat"]').autocomplete({
     source: "<?php echo base_url(); ?>mto/material_catalog_autocomplete",
     autoFocus: true,
     classes: {
         "ui-autocomplete": "highlight"
     }
   });

  function check_material_catalog(input){

    var string_input = $(input).val().split(" ");
    var val_material_catalog = string_input[0];
    var val_mto_id = $('input[name="mto_id"]').val()

    if(val_material_catalog == "" || val_material_catalog.includes('Not Found')){
      $('#text_alert').removeClass('d-none').text('Error: Material Not Found');
      $('input[name="material_cat"]').addClass('is-invalid');
      $('button[name="submit"]').attr('disabled', true);
      $('input[name="id_material_cat"]').val(0);

      $('input[name="description"]').val('');
      $('input[name="material_grade"]').val('');
      $('input[name="material_class"]').val('');
      $('input[name="thk_mm"]').val('');
      $('input[name="width_m"]').val('');
      $('input[name="length_m"]').val('');
      $('input[name="weight"]').val('');
      $('input[name="od"]').val('');
      $('input[name="sch"]').val('');
            
    } else {
      $.ajax({
        url: "<?= base_url() ?>mto/mto_check_material_catalog",
        type: "POST",
        data: {
          'material': val_material_catalog,
          'mto_id'  : val_mto_id
        },
        success: function(data){
          var hasil = JSON.parse(data);
          if(hasil.id == '0'){
            $('input[name="material_cat"]').addClass('is-invalid').removeClass("is-valid");
            $('#text_alert').removeClass('d-none').text('Error: Duplicate Material');
            $('button[name="submit"]').attr('disabled', true);

            $('input[name="id_material_cat"]').val(0);

            // VALUE
            $('input[name="description"]').val('');
            $('input[name="material_grade"]').val('');
            $('input[name="material_class"]').val('');
            $('input[name="thk_mm"]').val('');
            $('input[name="width_m"]').val('');
            $('input[name="length_m"]').val('');
            $('input[name="weight"]').val('');
            $('input[name="od"]').val('');
            $('input[name="sch"]').val('');

          } else {
            $('input[name="material_cat"]').addClass('is-valid').removeClass("is-invalid");
            $('#text_alert').addClass('d-none');
            $('button[name="submit"]').removeAttr('disabled');

            $('input[name="id_material_cat"]').val(hasil.id);

            // VALUE
            $('input[name="description"]').val(hasil.description);
            $('input[name="material_grade"]').val(hasil.material_grade);
            $('input[name="material_class"]').val(hasil.material_class);
            $('input[name="thk_mm"]').val(hasil.thk_mm);
            $('input[name="width_m"]').val(hasil.width_m);
            $('input[name="length_m"]').val(hasil.length_m);
            $('input[name="weight"]').val(hasil.weight);
            $('input[name="od"]').val(hasil.od);
            $('input[name="sch"]').val(hasil.sch);

            autofill_area_per_plate();

          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });
    }
  }

  function autofill_area_per_plate(){
    var width_m   = $('input[name="width_m"]').val();
    var length_m  = $('input[name="length_m"]').val();

    if(width_m && length_m){
      var area_per_plate = parseFloat(width_m) * parseFloat(length_m);
      $('input[name="area_per_plate"]').val(area_per_plate);
    } else {
      $('input[name="area_per_plate"]').val('');
    }
  }

  function autofill_total_pcs(){
    var nett_area       = $('input[name="nett_area"]').val();
    var area_per_plate  = $('input[name="area_per_plate"]').val();

    var width_m         = $('input[name="width_m"]').val();
    var nett_length     = $('input[name="nett_length"]').val();
    var length          = $('input[name="length_m"]').val();
    var cont            = $('input[name="cont"]').val();

    if(width_m){
      var total_pcs = parseInt(((nett_area * (cont / 100 )) + parseFloat(nett_area)) / area_per_plate) + 1;
    } else{
      var total_pcs = parseInt(((nett_length * (cont / 100)) + parseFloat(nett_length)) / length) + 1;
    }

    $('input[name="total_pcs"]').val(total_pcs);

    autofill_weight_per_piece();
    autofill_weight_total();
  }

  function autofill_weight_per_piece(){
    var thk_mm = $('input[name="thk_mm"]').val();
    var width_m = $('input[name="width_m"]').val();
    var length_m = $('input[name="length_m"]').val();

    var unit_wt = $('input[name="unit_wt"]').val();

    if(width_m){
      var weight_per_piece = (parseFloat(thk_mm) * parseFloat(width_m) * parseFloat(length_m) * 7.85 / 1000);
    } else if(unit_wt){
      var weight_per_piece = (parseFloat(length_m) * parseFloat(unit_wt) / 1000);
    }

    $('input[name="weight_per_piece"]').val(weight_per_piece.toFixed(2));
  }

  function autofill_weight_total(){
    var total_pcs = $('input[name="total_pcs"]').val();
    var weight_per_piece = $('input[name="weight_per_piece"]').val();

    var weight_total = parseFloat(total_pcs) * parseFloat(weight_per_piece);

    $('input[name="weight_total"]').val(weight_total);
  }
</script>

<script type="text/javascript">
  var first_value;
  function save_first_value(cell){
    var value = $(cell).html();
    first_value = value;
  }

  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $('select[name="material_cat"]').change(function(){
    var val_material_category = $('select[name="material_cat"]');
    var val_size = $('input[name="size"]');
    var val_type = $('input[name="type"]');
    var val_weight = $('input[name="weight"]');
    var val_length = $('input[name="length"]');

    if(val_material_category.val() != ""){
      $.ajax({
        url: "<?php echo base_url();?>pcms/mto/get_material_category",
        type: "post",
        data: {
          'material_cat': val_material_category.val(),
        },
        success: function(data) {
          var hasil = JSON.parse(data);
              hasil = hasil[0];
          
          val_size.val(hasil['size']);
          val_type.val(hasil['type']);
          val_weight.val(hasil['weight']);
          val_length.val(hasil['length']);
          // val_size.val();
        }
      });
    } else {
      val_size.val('');
      val_type.val('');
      val_weight.val('');
      val_length.val('');
    }
  });
</script>

<script>

  //MTO DETAIL EDIT ====================================================================
  function material_edit(cell){
    var col = $(cell).data("col");
    var id = $(cell).data("id");
    var value = $(cell).html();
    if(value != first_value){
      Swal.fire({
        title: 'Are you sure to <b class="text-warning">&nbsp;Edit&nbsp;</b> this?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Update it!'
      }).then((result) => {
        if (result.value) {
          $.ajax({
            url: "<?php echo base_url();?>mto/mto_material_edit_process",
            type: "post",
            data: {
              col: col,
              id: id,
              value: value
            }, success(data){
              Swal.fire(
                'Success!',
                'Your data has been changed.',
                'success'
              )
            }
          });
        }
        else{
          $(cell).html(first_value);
        }
      })
    }
  }
  //=============================================================================

  function delete_material(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          data: {
            'id' : id,
            'mto_number' : $('input[name="mto_number"]').val()
          },
          url: '<?php echo base_url()?>mto/mto_material_draft_delete_process',
          type: "post",
          success : function(data){
            $('#material_draft').trigger('reset');
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            ).then(function() {
              location.reload();
            });

          }
        });
      }
    })
  }

  function submit_detail(){
    var mat_cat = $('input[name="material_cat"]').val();

    if(mat_cat == "" ){
      Swal.fire(
        'Error!',
        'Please complete the form',
        'error'
      )
    } else {

      $.ajax({
        data: $('#material_draft').serialize(),
        url: '<?php echo base_url()?>mto/mto_material_draft_add_process',
        type: "post",
        success : function(data){
          $('#material_draft').trigger('reset');
          $('input[name="material_cat"]').removeClass('is-valid');

          Swal.fire(
            'Success!',
            'Data Successful Added',
            'success'
          ).then(function() {
            location.reload();
          });

        }
      });
    }
  }

</script>