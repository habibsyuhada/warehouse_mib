<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=Report-".date('YmdHis').".xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");

 ?>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
      th{
        background: #55efc4;
      }
      td{
        text-align: center;
      }
    </style>
  </head>
  <body>
    <table border="1">
     
      <thead>
        <tr>
          <th style="background: #55efc4;" rowspan="2">Item Line</th>
          <th style="background: #55efc4;" rowspan="2">Description</th>
          <th style="background: #55efc4;" colspan="2">Material</th>
          <th style="background: #55efc4;" colspan="6">Size</th>
          <th style="background: #55efc4;" rowspan="2">Discipline</th>
          <th style="background: #55efc4;" rowspan="2">Nett Area (M2)</th>
          <th style="background: #55efc4;" rowspan="2">Area per Plate (M2)</th>
          <th style="background: #55efc4;" rowspan="2">Nett Length (M)</th>
          <th style="background: #55efc4;" rowspan="2">Unit Wt (kg/m)</th>
          <th style="background: #55efc4;" rowspan="2">Cont (%)</th>
          <th style="background: #55efc4;" rowspan="2">Total (pcs)</th>
          <th style="background: #55efc4;" rowspan="2">Certification</th>
          <th style="background: #55efc4;" colspan="2">Weight (MT)</th>
          <th style="background: #55efc4;" rowspan="2">Remarks</th>
        </tr>
        <tr>
          <th style="background: #55efc4;">Grade</th>
          <th style="background: #55efc4;">Class</th>

          <th style="background: #55efc4;">Thk (mm)</th>
          <th style="background: #55efc4;">Width (M)</th>
          <th style="background: #55efc4;">Length (M)</th>
          <th style="background: #55efc4;">Weight</th>
          <th style="background: #55efc4;">OD</th>
          <th style="background: #55efc4;">Schedule</th>

          <th style="background: #55efc4;">Per Piece</th>
          <th style="background: #55efc4;">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          $no = 1;
          $total = 0;
          foreach($mto_detail_list as $detail): 
        ?>
        <tr>
          <td><?php echo $no++ ?></td>
          <td><?php echo $detail['material'] ?></td>
          <td><?php echo $detail['material_grade'] ?></td>
          <td><?php echo $detail['name_material_class'] ?></td>
          <td><?php echo $detail['thk_mm'] ?></td>
          <td><?php echo $detail['width_m'] ?></td>
          <td><?php echo $detail['length_m'] ?></td>
          <td><?php echo $detail['nett_area'] ?></td>
          <td><?php echo @round($detail['width_m']*$detail['length_m'], 2) ?></td>
          <td><?php echo $detail['nett_length'] ?></td>
          <td><?php echo $detail['unit_wt'] ?></td>
          <td><?php echo $detail['cont'] ?>%</td>
          <td><?php echo $detail['total_pcs'] ?></td>
          <td><?php echo $detail['certification'] ?></td>
          <?php  
            if($detail['thk_mm'] == "" || $detail['width_m'] == "" || $detail['length_m'] == ""){
              $per_piece = round($detail['length_m']*$detail['unit_wt']/1000, 2);
            }
            else{
              $per_piece = round($detail['thk_mm']*$detail['width_m']*$detail['length_m']/1000, 2);
            }
          ?>
          <td><?php echo $per_piece ?></td>
          <td><?php echo $total_tmp = round($detail['total_pcs']*$per_piece, 2) ?></td>
          <td><?php echo $detail['remarks'] ?></td>
        </tr>
        <?php 
            $total = $total + $total_tmp;
          endforeach; 
        ?>
        <tr>
          <td></td>
          <td style="background: #dfe6e9;" colspan="14" style="text-align: left"><b>Total</b></td>
          <td style="background: #dfe6e9;"><?php echo $total ?></td>
          <td></td>
        </tr>
      </tbody> 
    </table>
  </body>
</html>
    