<?php 
  foreach($mto_list as $mto){
    $mto_number      = $mto['mto_number'];
    $mto_rev         = $mto['mto_rev'];
    // $str_vendor         = $dt_mrir->vendor;
    // $str_project        = $this->Mrir_mod->search_project_data($dt_mrir->project_id);
    // $str_date_receiving =  date('d-M-y', strtotime($dt_mrir->date_receiving));
    $str_createdby      = $this->mto_mod->search_sign_data($mto['created_by']);    
    $str_approvedby     = $this->mto_mod->search_sign_data($mto['approved_by']);
  }
?>

<!DOCTYPE html>
<html><head>
  <title>MTO Number : <?php echo $mto_number; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 4.5cm;
      margin-left: 2.3cm;
      margin-right: 1.5cm;
      margin-bottom: 3cm;
      font-family: "helvetica";
      font-size: 9px !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 5cm;
      padding-top: 1px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    
    }

    footer {
      position: fixed;
      bottom: 1cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      padding-bottom: 2.5px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    
    }


    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;
     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table>thead>tr>td,table>tbody>tr>td{
      vertical-align: top;
    }

    .br_break{
      line-height: 15px;
    }

    .br_break_no_bold{
      line-height: 18px;
    }

    .br{
      border-right: 1px #000 solid;
    }
    .bl{
      border-left: 1px #000 solid;
    }
    .bt{
      border-top: 1px #000 solid;
    }
    .bb{
      border-bottom:  1px #000 solid;
    }
    .bx{
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .by{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
    }
    .ball{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .tab{
      display: inline-block; 
      width: 100px;
    }
    .tab2{
      display: inline-block; 
      width: 100px;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }

  </style>
</head><body>
  <header>
    <table width="100%" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-top: 50px;">
      <tr>
        <td rowspan="4" style="padding-bottom: 4px;font-size: 15px !important; width: 30px;"><img src="img/logo.png" style="width: 100px; padding-top: 25px;"></td>
        <td rowspan="2" style="padding-bottom: 4px;font-size: 15px !important;"><h3>Horsea Top Jacket</h3></td>
        <td style="padding-bottom: 4px;font-size: 10px !important; width: 60px; text-align: right;">By :</td>
        <td style="padding-bottom: 4px;font-size: 10px !important; width: 60px;">LN</td>
      </tr>
      <tr>
        <td style="padding-bottom: 4px;font-size: 10px !important; text-align: right;">Checked :</td>
        <td style="padding-bottom: 4px;font-size: 10px !important;">WN</td>
      </tr>
      <tr>
        <td style="padding-bottom: 4px;font-size: 13px !important;">Material Take-Off Primary Steel</td>

        <td style="padding-bottom: 4px;font-size: 10px !important; text-align: right;">Rev :</td>
        <td style="padding-bottom: 4px;font-size: 10px !important;">01</td>
      </tr>
      <tr>
        <td style="padding-bottom: 4px;font-size: 15px !important;">-</td>

        <td style="padding-bottom: 4px;font-size: 10px !important; text-align: right;">Date :</td>
        <td style="padding-bottom: 4px;font-size: 10px !important;">11/26/2019</td>
      </tr>
    </table>
  </header>

  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-left: -25px; ">
       <thead><tr bgcolor="#008060" style="color: white !important; text-align: center;">
          <th rowspan="2">DESCRIPTION</th>
          <th rowspan="2">STEEL TYPE</th>
          <th rowspan="2">GRADE</th>
          <th colspan='3'>PLATE / PROFILE SIZE</th>
          <th rowspan="2">NET AREA<br/>(M2)</th>
          <th rowspan="2">AREA PER<br/>PLATE<br/>(M2)</th>
          <th rowspan="2">NET<br/>LENGTH<br/>(M)</th>
          <th rowspan="2">UNIT WT<br/>(KG/M)</th>
          <th rowspan="2">CONT<br/>(%)</th>
          <th rowspan="2">TOTAL<br/>(Pcs)</th>
          <th rowspan="2">CERT<br/>(3.1/3.2)</th>
          <th colspan="2">WEIGTH<br/>(MT)</th>
          <th rowspan="2">REMARKS</th>
        </tr><tr bgcolor="#008060" style="color: white !important; text-align: center;">
          <th>THK<br/>(mm)</th>
          <th>WIDTH<br/>(M)</th>
          <th>LENGTH<br/>(M)</th>

          <th>PER PIECE</th>
          <th>TOTAL</th>
        </tr></thead>
        <tbody><?php $no = 1; foreach ($mto_detail_list as $key => $mto_detail) { ?><tr>
          <td><?= $mto_detail['catalog_name']; ?></td>
          <td><?= $mto_detail['steel_type'] ?></td>
          <td><?= $mto_detail['grade'] ?></td>
          <td><?= $mto_detail['thk_mm'] ?></td>
          <td><?= $mto_detail['width_m'] ?></td>
          <td><?= $mto_detail['length_m'] ?></td>
          <td><?= $mto_detail['nett_area'] ?></td>
          <td><?= $mto_detail['area_per_plate'] ?></td>
          <td><?= $mto_detail['nett_length'] ?></td>
          <td><?= $mto_detail['unit_wt'] ?></td>
          <td><?= $mto_detail['cont'] ?></td>
          <td><?= $mto_detail['total_pcs'] ?></td>
          <td><?= $mto_detail['certification'] ?></td>
          <td><?= $mto_detail['weight_per_piece'] ?></td>
          <td><?= $mto_detail['weight_total'] ?></td>
          <td><?= $mto_detail['remarks'] ?></td>
        </tr><?php $no++; } ?></tbody>  
  </table>

</body></html>