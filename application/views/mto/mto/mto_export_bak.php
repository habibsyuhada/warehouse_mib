<?php 
  //print_r($mto_detail_list);exit;
?>
<?php 
  foreach($mto_list as $mto){
    $mto_number      = $mto['mto_number'];
    $mto_rev         = $mto['mto_rev'];
    // $str_vendor         = $dt_mrir->vendor;
    // $str_project        = $this->Mrir_mod->search_project_data($dt_mrir->project_id);
    // $str_date_receiving =  date('d-M-y', strtotime($dt_mrir->date_receiving));
    $str_createdby      = $this->mto_mod->search_sign_data($mto['created_by']);    
    $str_approvedby     = $this->mto_mod->search_sign_data($mto['approved_by']);

   // print_r($str_approvedby);
  }
?>
<!DOCTYPE html>
<html><head>
  <title>MTO Number : <?php echo $mto_number; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 3.8cm;
      margin-left: 2.5cm;
      margin-right: 1.5cm;
      margin-bottom: 2cm;
      font-family: "helvetica";
      font-size: 50% !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      padding-top: 15px;
      padding-left: 1.5cm;
      padding-right: 1.5cm;
    
    }

    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table tr {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }
   
  </style>
</head><body>
  <header>
    <br><br>
  	<table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important;">
  		<tr>
        <td rowspan="4" style="font-size: 10px; width: 75px;"><img src="img/logo.png" style="width: 100px;"></td>
        <td rowspan="2" style="font-size: 15px;">HORNSEA-2</td>
        <td style="font-size: 10px; width: 50px;">By :</td>
  			<td style="font-size: 10px; width: 50px;">LN</td>
  		</tr>
      <tr>
        <td>Checked :</td>
        <td>WH</td>
      </tr>
      <tr>
        <td>Material Tale-Off Primary Steel</td>
        <td>Rev :</td>
        <td><?= str_pad($mto_rev , 2, '0', STR_PAD_LEFT); ?></td>
      </tr>
      <tr>
        <td> - </td>
        <td>Date :</td>
        <td>11/26/2019</td>
      </tr>
  	</table>
  </header>
  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-left: -25px;">
	<thead><tr>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 20px;"><b>Item Line</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 150px;"><b>Description</b></th>    
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 30px;"><b>Steel Type</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 150px;"><b>Grade</b></th>
        <th bgcolor="#a6ffa6" colspan="3" style="font-size: 9px; width: 100px; max-height: 25;"><b>Plate / Profile Size</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Nett Area (M2)</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Area per Plate (M2)</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Nett Length (M)</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 40px;"><b>Unit Wt kg/m</b></th>        
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Cont</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Total (pcs)</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Certification (3.1 / 3.2)</b></th>
        <th bgcolor="#a6ffa6" colspan="2" style="font-size: 9px; width: 50px;"><b>Weight (MT)</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Remarks</b></th>
        <!-- <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Status</b></th> -->
    </tr></thead>
    <thead><tr>
    	<th bgcolor="#a6ffa6" style="font-size: 9px;">Thk (mm)</th>
    	<th bgcolor="#a6ffa6" style="font-size: 9px;">Width (M)</th>
    	<th bgcolor="#a6ffa6" style="font-size: 9px;">Length (M)</th>

      <th bgcolor="#a6ffa6" style="font-size: 9px;">Per Piece</th>
      <th bgcolor="#a6ffa6" style="font-size: 9px;">Total</th>
    </tr></thead>
    <tbody><?php $no=1; foreach($mto_detail_list as $mto_detail){ ?><tr>    
    	<td><?= $no ?></td>
    	<td><?= $mto_detail['catalog_name'] ?></td>
      <td><?= $mto_detail['steel_type'] ?></td>
    	<td><?= $mto_detail['grade'] ?></td>
    	<td><?= $mto_detail['thk_mm'] ?></td>
    	<td><?= $mto_detail['width_m'] ?></td>
    	<td><?= $mto_detail['length_m'] ?></td>
    	<td><?= $mto_detail['nett_area'] ?></td>
    	<td><?= $mto_detail['area_per_plate'] ?></td>
    	<td><?= $mto_detail['nett_length'] ?></td>    	
    	<td><?= $mto_detail['unit_wt'] ?></td>
    	<td><?= $mto_detail['cont'] ?></td>
    	<td><?= $mto_detail['total_pcs'] ?></td>
    	<td><?= $mto_detail['certification'] ?></td>
    	<td><?= $mto_detail['weight_per_piece'] ?></td>
    	<td><?= $mto_detail['weight_total'] ?></td>
      <td><?= $mto_detail['remarks'] ?></td>
    </tr><?php $no++; } ?></tbody>  
  </table>
</body></html>