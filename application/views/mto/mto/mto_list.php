<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center" id='mto_list_dt'>
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>MTO Number</th>
                  <th>Project</th>
                  <th>Module</th>
                  <th>Created By</th>
                  <th>Created Date</th>
                  <th>Priority</th>
                  <?php if($_parameter != 'draft'){ ?><th>Remarks</th><?php } ?>
                  <th>Action</th>
                </tr>
              </thead>
              
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('#mto_list_dt').DataTable({
    // "scrollX": true,
    "language": { 
      "infoFiltered": "" },
      "paging": true,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
          "url": "<?php echo base_url();?>mto/mto_list_json/<?= $_parameter ?>",
          "type": "POST",
      },
      "columnDefs": [{
        "targets": [0],
        "orderable": true,
      }, ],
  });
</script>