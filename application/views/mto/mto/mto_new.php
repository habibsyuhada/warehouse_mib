<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mto/mto_add_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Project</label>
                <div class="col-sm-10">
                  <select class="form-control" name="project" id="projectx" required>
                    <option value="">-</option>
                    <?php foreach($project_chain as $project){ ?>
                      <option <?php echo $project_chain_selected == $project['id'] ? 'selected="selected"' : '' ?> value="<?= $project['id'] ?>"><?= $project['project_name'] ?></option>
                    <?php } ?>
                  </select>        
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Module</label>
                <div class="col-sm-10">
                  <select class="form-control" name="module" id="modulex" required>
                    <option value="">-</option>
                    <?php foreach($module_chain as $module){ ?>
                      <option <?php echo $module_chain_selected == $module['mod_id'] ? 'selected="selected"' : '' ?> 
                                    class="<?php echo $module['project_id'] ?>" value="<?= $module['mod_id'] ?>"><?= $module['mod_desc'] ?></option>
                    <?php } ?>
                  </select>        
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Priority</label>
                <div class="col-sm-10">
                  <select class="form-control" name="priority">
                    <?php foreach($priority_list as $priority){ ?>
                      <option value="<?= $priority['id'] ?>"><?= $priority['priority_name'] ?></option>
                    <?php } ?>
                  </select>                
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <?php if($read_permission[3] == 1){ ?>
              <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <?php } ?>
            <a href="<?php echo base_url();?>mto" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function check_mto(input) {
    var mto_number = $(input).val();

    if(mto_number == ''){
        $(input).addClass('is-invalid');
        $('.invalid-feedback').remove( ":contains('Duplicate MTO Number')" );
        $('button[name=submit]').prop("disabled", true);    
    } else {
      $.ajax({
        url: "<?php echo base_url();?>mto/mto_no_check/",
        type: "post",
        data: {
          mto_number: mto_number
        },
        success: function(data) {
          var data = JSON.parse(data);
          
          if(data.hasil == 0){
            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Duplicate MTO Number')" );
            $(input).after('<div class="invalid-feedback">Duplicate MTO Number.</div>');
            $('button[name=submit]').prop("disabled", true);
          }
          else{
            $('.invalid-feedback').remove( ":contains('Duplicate MTO Number')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');
          }
          if (!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }
        }
      });

    }
  }
</script>

<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.chained.min.js"></script>
<script>
    $("#modulex").chained("#projectx"); // disini kita hubungkan kota dengan provinsi
</script>