<?php 
  // print_r($material_catalog_list);exit;
?>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mto/mto_detail_import_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-hover text-center">
                <thead class="bg-success text-white">
                  <tr>
                    <th rowspan="2" style="width: 1000px;">Category</th>
                    <th rowspan="2">Steel Type</th>
                    <th rowspan="2" style="width: 1000px;">Grade</th>
                    <th colspan="3">Plate / Profile Size</th>
                    <th rowspan="2">Nett Area (M2)</th>
                    <th rowspan="2">Area per Plate (M2)</th>
                    <th rowspan="2">Nett Length (M)</th>
                    <th rowspan="2">Unit Wt kg/m</th>
                    <th rowspan="2">Cont (in %)</th>
                    <th rowspan="2">Total (pcs)</th>
                    <th rowspan="2" style="width: 10px;">Certification (3.1 / 3.2)</th>
                    <th colspan="2">Weight (MT)</th>
                    <th rowspan="2">Remarks</th>
                    <th rowspan="2">Action</th>
                  </tr>
                  <tr>
                    <th>Thk (mm)</th>
                    <th style="width: 1000px;">Width (M)</th>
                    <th>Length (M)</th>

                    <th>Per Piece</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no=1; 
                    
                    foreach($sheet as $row):
                      if($no > 1): 

                        $status = '';
                        $disabled = 0;

                        $id_mc = @$row['id_material_catalog'];
                        $id_mtoc = @$row['id_mto_category'];

                        $material_catalog = @$row['material_catalog'];
                        $mto_category     = @$row['mto_category'];

                        if(!in_array($row['A'], $material_catalog_list)){
                          $status = 'Material Catalog Not Found';
                          $disabled = 2;
                          $material_catalog = $row['A'];
                        }

                        if($row['A'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td class="align-middle"><input type="text" name="mto_number" class="form-control" value="<?php echo $mto_number ?>" readonly></td>
                    <td class="align-middle">
                      <input type="hidden" name="material_catalog[]" value="<?= $id_mc ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="text" class="form-control" value="<?= $material_catalog ?>" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="steel_type" value="<?= $row['steel_type'] ?>" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="grade" value="<?= $row['grade'] ?>" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="thk_mm" value="<?= $row['thk_mm'] ?>" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="width_m" value="<?= $row['width_m'] ?>" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="length_m" value="<?= $row['length_m'] ?>" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="number" class="form-control" name="nett_area[]" value="<?= $row['B'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="area_per_plate" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="number" class="form-control" name="nett_length[]" value="<?= $row['C'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="number" class="form-control" name="unit_wt[]" value="<?= $row['D'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="number" class="form-control" name="cont[]" value="<?= $row['E'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="total_pcs" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="number" class="form-control" name="certification[]" value="<?= $row['F'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="number" class="form-control" name="weight_per_piece" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="number" class="form-control" name="weight_total" disabled>
                    </td>
                    <td class="align-middle">
                      <textarea class="form-control" name="remarks[]"><?= $row['G'] ?></textarea>
                    </td>
                    <td class="align-middle"><?php echo $status ?></td>

                  </tr>
                  <?php  } endif; ?>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>mto_detail/<?= strtr($this->encryption->encrypt($mto_number), '+=/', '.-~') ?>" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">

  autofill_area_per_plate();
  autofill_total_pcs();
  autofill_weight_per_piece();
  autofill_weight_total();

  function autofill_area_per_plate(){
    var width_m   = $('input[name="width_m"]').val();
    var length_m  = $('input[name="length_m"]').val();

    if(width_m && length_m){
      var area_per_plate = width_m * length_m;
      $('input[name="area_per_plate"]').val(area_per_plate);
    } else {
      $('input[name="area_per_plate"]').val('');
    }
  }

  function autofill_total_pcs(){
    var nett_area       = $('input[name="nett_area"]').val();
    var area_per_plate  = $('input[name="area_per_plate"]').val();

    var nett_length     = $('input[name="nett_length"]').val();
    var length          = $('input[name="length_m"]').val();
    var cont            = $('input[name="cont"]').val();

    if(nett_area){
      var total_pcs = parseInt(((nett_area * (cont / 100 )) + parseFloat(nett_area)) / area_per_plate) + 1;
    } else if(nett_length){
      var total_pcs = parseInt(((nett_length * (cont / 100)) + parseFloat(nett_length)) / length) + 1;
    }

    $('input[name="total_pcs"]').val(total_pcs);
  }

  function autofill_weight_per_piece(){
    var thk_mm = $('input[name="thk_mm"]').val();
    var width_m = $('input[name="width_m"]').val();
    var length_m = $('input[name="length_m"]').val();

    var unit_wt = $('input[name="unit_wt"]').val();

    if(width_m){
      var weight_per_piece = (thk_mm * width_m * length_m * 7.85 / 1000);
    } else if(unit_wt){
      var weight_per_piece = (length_m * unit_wt / 1000);
    }

    $('input[name="weight_per_piece"]').val(weight_per_piece.toFixed(2));
  }

  function autofill_weight_total(){
    var total_pcs = $('input[name="total_pcs"]').val();
    var weight_per_piece = $('input[name="weight_per_piece"]').val();

    var weight_total = total_pcs * weight_per_piece;

    $('input[name="weight_total"]').val(weight_total);
  }
</script>