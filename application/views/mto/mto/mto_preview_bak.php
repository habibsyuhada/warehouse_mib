<?php 
  // print_r($sheet[4]);exit;
?>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>mto/mto_detail_import_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-hover text-center">
                <thead class="bg-success text-white">
                  <tr>
                    <th>MTO Number</th>
                    <th>Material Catalog</th>
                    <th>MTO Category</th>
                    <th>Qty</th>
                    <th>Total Weight</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no=1; 
                    
                    foreach($sheet as $row):
                      if($no > 1): 

                        $status = '';
                        $disabled = 0;

                        $id_mc = $id_material_catalog[$no];
                        $id_mtoc = $id_mto_category[$no];

                        $material_catalog = $sheet['material_catalog'][$no];
                        $mto_category     = $sheet['mto_category'][$no];

                        if(!in_array($row['A'], $material_catalog_list)){
                          $status = 'Material Catalog Not Found';
                          $disabled = 2;
                          $material_catalog = $row['A'];
                        }

                        if(!in_array($row['B'], $mto_category_list)){
                          $status = 'MTO Category Not Found';
                          $disabled = 2;
                          $mto_category = $row['B'];
                        }

                        if($row['A'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td class="align-middle"><input type="text" name="mto_number" class="form-control" value="<?php echo $mto_number ?>" readonly></td>
                    <td class="align-middle">
                      <input type="hidden" name="material_catalog[]" value="<?= $id_mc ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="text" class="form-control" value="<?= $material_catalog ?>" disabled>
                    </td>
                    <td class="align-middle">
                      <input type="hidden" name="mto_category[]" value="<?= $id_mtoc ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="text" class="form-control" value="<?= $mto_category ?>" disabled>
                    </td>
                    <td class="align-middle"><input type="text" name="qty[]" class="form-control" value="<?php echo $row['C'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="total_weight[]" class="form-control" value="<?php echo $row['D'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><?php echo $status ?></td>
                  </tr>
                  <?php  } endif; ?>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>mto_detail/<?= strtr($this->encryption->encrypt($mto_number), '+=/', '.-~') ?>" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->