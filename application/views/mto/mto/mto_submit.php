<div id="content" class="container">
  <form method="POST" action="<?php echo base_url();?>mto/mto_submit_process">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
              <?php if($count_mto_detail == 0){ ?>
                <center><h4 style="padding-bottom: 15px; color: red">No Material Found on this MTO</h4></center>
              <?php } ?>
              <div class="form-group row">
                <input type="hidden" name="mto_id" value="<?= $mto_list[0]['id'] ?>">

                <label class="col-sm-2 col-form-label">Disposition To</label>
                <div class="col-sm-10">
                  <select class="form-control" name="disposition_to">
                    <option value="Engineering">Engineering</option>
                  </select>             
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Remarks</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="remarks" placeholder="Remarks"></textarea>            
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success" title="Submit" <?php echo ($count_mto_detail == 0 ? 'disabled' : ''); ?>><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>mto_list/draft" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });
</script>