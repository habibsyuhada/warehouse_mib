    <div class="container" style="background-color: whitesmoke">
     
      <div class="row">
        <div class="container-fluid">
          <div class="my-3 p-3 bg-white rounded shadow-sm">
            <div class="card-header">
              <a class='btn btn-primary' href="<?php echo site_url('mto_category/add') ?>"><i class="fas fa-plus"></i> Add New</a>
            </div>

       <div class="card-body">

          <div class="table-responsive">
              <table class="table table-hover text-center" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Initial</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($mto_category_list as $mto_category): ?>
                  <tr>
                    <td width="20"><?= $no++; ?></td>
                    <td width="20"><?= $mto_category['initial'] ?></td>
                    <td width="500"><?= $mto_category['description'] ?></td>
                    <td width="250">
                      <a class='btn btn-warning' href="<?php echo base_url() ?>mto_category/edit/<?= strtr($this->encryption->encrypt($mto_category['id']),'+=/', '.-~') ?>" class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
                      <a class='btn btn-danger' onclick="deleteConfirm('<?php echo base_url() ?>mto_category/delete/<?= strtr($this->encryption->encrypt($mto_category['id']),'+=/', '.-~') ?>')"
                       href="#" class="btn btn-small text-danger"><i class="fas fa-trash"></i> Delete </a>
                    </td>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
          </div>
        </div>

        </div>
      </div>
   </div>   

    </div>
    </div><!-- ini div dari sidebar yang class wrapper -->
            