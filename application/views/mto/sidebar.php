<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
  <ul class="list-unstyled components">
    <?php if($read_permission[2] == 1){ ?>   

    <li>
      <a href="#homeSubmenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-home"></i> MTO ( Material Take Off )
      </a>
      <ul class="list-unstyled" id="homeSubmenu1">
        <?php if($read_permission[3] == 1){ ?>    
          <li>
            <a href="<?php echo base_url();?>mto_add"><i class="fas fa-plus"></i> &nbsp; Add New MTO</a>
          </li>
        <?php } ?> 
        <?php if($read_permission[2] == 1){ ?> 
          <li>
            <a href="<?php echo base_url();?>mto_list/draft"><i class="fas fa-file-alt"></i> &nbsp; MTO List</a>
          </li>
        <?php } ?> 
      </ul>
    </li> 
    
    <li>
      <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-home"></i> MTO Approval
      </a>
      <ul class="list-unstyled" id="homeSubmenu2">
        <li>
          <a href="<?php echo base_url();?>mto_list/waiting"><i class="far fa-clock"></i> &nbsp; Waiting List</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>mto_list/rejected"><i class="fas fa-times-circle"></i> &nbsp; Rejected List</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>mto_list/approved"><i class="fas fa-check-circle"></i> &nbsp; Approved List</a>
        </li>
      </ul>
    </li> 

    <?php } ?> 

    <?php if($read_permission[10] == 1){ ?>  

    <li>
      <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-home"></i> Material Catalog
      </a>
      <ul class="list-unstyled" id="homeSubmenu3">
        <?php if($read_permission[11] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>material_catalog_add"><i class="fas fa-plus"></i> &nbsp; Add Material Catalog</a>
        </li>
        <?php } ?> 
        <li>
          <a href="<?php echo base_url();?>material_catalog_list"><i class="fas fa-minus-square"></i> &nbsp; Material Catalog</a>
        </li>
        <?php if($read_permission[11] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>import_material_catalog"><i class="fas fa-file-excel"></i> &nbsp; Import Material Catalog</a>
        </li>
        <?php } ?> 
      </ul>
    </li>
     
    <?php } ?>
    <!-- <li>
      <a href="#homeSubmenu4" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-home"></i> Vendor Catalog
      </a>
      <ul class=" list-unstyled">
        <li>
          <a href="<?php //echo base_url();?>catalog_category_list"><i class="fas fa-minus-square"></i> &nbsp; Catalog Category</a>
        </li>
      </ul>
    </li> 

    <!-- <li>
      <a href="#homeSubmenu5" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-home"></i> Priority
      </a>
      <ul class="list-unstyled" id="homeSubmenu5">
        <li>
          <a href="<?php// echo base_url();?>priority"><i class="fas fa-minus-square"></i> &nbsp; Priority</a>
        </li>
      </ul>
    </li>  -->

  </ul>
</nav>