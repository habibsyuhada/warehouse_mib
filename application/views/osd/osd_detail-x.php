<?php $osd_list = $osd_list[0]; ?>

<script>

  <?php for($i=1;$i<=sizeof($osd_list_detail);$i++){ ?>

    var no=1;

    function checkunique<?php echo $i; ?>(input, num){

      var text<?php echo $i; ?> = input;
      var empty_val = "-";
      var po_number = $("input[id='po_number']").val();

      $.ajax({
        url: "<?php echo base_url(); ?>osd/unique_no_check_det",
        type: "post",
        data: {
          'osd_det_id': text<?php echo $i; ?>,
          'po_number': po_number,
        },
        success: function(data){
          var dup = 0;

          for(var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='unique_no"+i+"']").val() == text<?php echo $i; ?>){
                data = 'Error : Duplicate Unique No on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

              $(input).addClass('is-invalid');
              $('.invalid-feedback').remove( ":contains('Error')" );
              $(input).after('<div class="invalid-feedback">'+data+'</div>');

              $("textarea[name='description["+num+"]']").val(empty_val);
              $("input[name='length["+num+"]']").val(empty_val);
              $("input[name='width["+num+"]']").val(empty_val);
              $("input[name='thk["+num+"]']").val(empty_val);
              $("input[name='spec["+num+"]']").val(empty_val);
              $("input[name='plate_tag_no["+num+"]']").val(empty_val);
              $("input[name='series_heat_no["+num+"]']").val(empty_val);
              $("input[name='brand["+num+"]']").val(empty_val);
              $("input[name='do_pl["+num+"]']").val(empty_val);
              $("input[name='mrir_no["+num+"]']").val(empty_val);
              $("input[name='uom["+num+"]']").val(empty_val);
                     
              $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

              $('.invalid-feedback').remove( ":contains('Error')" );
              $(input).removeClass('is-invalid');
              $(input).addClass('is-valid');

              var res = data.split("; ");

              $("textarea[name='description["+num+"]']").val(res[0]);
              $("input[name='length["+num+"]']").val(res[3]);
              $("input[name='width["+num+"]']").val(res[1]);
              $("input[name='thk["+num+"]']").val(res[2]);
              $("input[name='spec["+num+"]']").val(res[6]);
              $("input[name='plate_tag_no["+num+"]']").val(res[9]);
              $("input[name='series_heat_no["+num+"]']").val(res[5]);
              $("input[name='brand["+num+"]']").val(res[7]);
              $("input[name='do_pl["+num+"]']").val(res[8]);
              $("input[name='mrir_no["+num+"]']").val(res[10]);           
              $("input[name='uom["+num+"]']").val(res[4]);
            
              $('button[name=submitBtn]').prop("disabled", false);
          }
        }
      });
  }

<?php } ?>

</script>

<script type="text/javascript">

  function check_po(input) {
    
    var po_number = input;

    if(po_number == ''){

        $(input).addClass('is-invalid');
        $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
        $('button[name=submit]').prop("disabled", true);

        $('input[name="client"]').val('');
        $('input[name="project_title"]').val('');
        $('input[name="date_of_receiving"]').val('');
        $('input[name="project_ref"]').val('');
    
    } else {

      $.ajax({
        url: "<?php echo base_url();?>osd/check_po_number/",
        type: "post",
        data: {
          po_number: po_number
        },
        success: function(data) {

          var data = JSON.parse(data);
       
          if(data.hasil == 0){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('DO / PL Number not found..')" );
            $(input).after('<div class="invalid-feedback">DO / PL Number not found..</div>');
            $('button[name=submit]').prop("disabled", true);

            $('input[name="client"]').val('');
            $('input[name="project_title"]').val('');
            $('input[name="date_of_receiving"]').val('');
            $('input[name="project_ref"]').val('');
            $('input[name="project_id"]').val('');

          } else {

            $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            $('input[name="client"]').val(data.client);
            $('input[name="project_title"]').val(data.project_title);
            $('input[name="date_of_receiving"]').val(data.date_of_receiving);
            $('input[name="project_ref"]').val(data.project_ref);
            $('input[name="project_id"]').val(data.project_id);

          }

          if(!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }
        }
      });
    }
  }
</script>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>osd/approve_osd_form" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

                <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Client</label>
                      <input type="text" class="form-control" name="client" id="client" value='<?php  ?>'  readonly>                
                                    
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                     <label>Purchase Order Number</label>
                     <script> check_po('<?php echo $osd_list["po_number"]; ?>');</script>
                      <input type="text" class="form-control" name="po_number" id="po_number" value='<?php echo $osd_list["po_number"]; ?>' readonly>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Project Title</label>
                      <input type="text" class="form-control" name="project_title" id="project_title" placeholder="---" readonly>                
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Project Ref</label>
                      <input type="text" class="form-control" name="project_ref" id="project_ref" placeholder="---" readonly>                
                  </div>
                </div>
              </div>

            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" >
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th>UNIQUE NO.</th>
                          <th>DESCRIPTION</th>
                          <th>LENGTH</th>
                          <th>OD/WIDTH</th>
                          <th>THK</th>
                          <th>SPEC</th>
                          <th>PLATE / TAG NO</th>
                          <th>HEAT /SERIES NO</th>
                          <th>BRAND</th>
                          <th>DO / PL NO</th>
                          <th>MRIR NO</th>                         
                          <th>OVER</th>
                          <th>SHORTAGE</th>
                          <th>DAMAGE</th>
                          <th>UOM</th>
                          <th>REMARKS</th>                          
                        </tr>
                        
                      </thead>
                      <tbody id="table_element" class="table-border">

                      <?php $no=1; foreach ($osd_list_detail as $key) { ?>
                        <tr>

                          <td>
                            <input type="text" class='form-control' id='unique_no[<?php echo $no; ?>]' value='<?php echo $key['unique_no']; ?>' readonly>

                             <script> checkunique<?php echo $no; ?>(<?php echo $key["osd_det_id"]; ?>,<?php echo $no; ?>);</script>
                          </td>                          
                          <td>
                              <textarea type="text" id="description[<?php echo $no; ?>]" name="description[<?php echo $no; ?>]" class="form-control" readonly></textarea>
                          </td>
                          <td>
                              <input type="text" id="length[<?php echo $no; ?>]" name="length[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                              <input type="text" id="width[<?php echo $no; ?>]" name="width[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                              <input type="text" id="thk[<?php echo $no; ?>]" name="thk[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                              <input type="text" id="spec[<?php echo $no; ?>]" name="spec[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                            <div class="row">
                            <div class="col-sm pr-0">
                              <input type="text" id="plate_tag_no[<?php echo $no; ?>]" name="plate_tag_no[<?php echo $no; ?>]" class="form-control" >
                            </div>
                            <div class="col-sm-auto pl-0">  
                             <button type="button" onclick="add_heat(this,'<?php echo $no; ?>');" class="btn btn-success" title="Add Heat Number"><i class="fa fa-plus"></i></button>
                            </div>
                            </div>
                          </td>
                          <td>
                            <div class="row">
                              <div class="col-sm pr-0">
                                <input type="text" id="series_heat_no[<?php echo $no; ?>]" name="series_heat_no[<?php echo $no; ?>]" class="form-control" >
                              </div>
                              <div class="col-sm-auto pl-0">
                                <button type="button" onclick="add_heat(this,'<?php echo $no; ?>');" class="btn btn-success" title="Add Plate Number"><i class="fa fa-plus"></i></button>
                              </div>
                            </div>
                          </td>
                          <td>
                              <input type="text" id="brand[<?php echo $no; ?>]" name="brand[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                            <input type="text" id="do_pl[<?php echo $no; ?>]" name="do_pl[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                            <input type="text" id="mrir_no[<?php echo $no; ?>]" name="mrir_no[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                            <input type="number" name="over[<?php echo $no; ?>]" class="form-control  main_input_over" value='<?php echo $key['over']; ?>'  title="qty_<?php echo $no; ?>" onfocus="save_qty_over($(this).val(),'<?php echo $no; ?>')" oninput="calculate_qty_main_over(this,'<?php echo $no; ?>')">
                          </td>
                          <td>
                            <input type="number" name="shortage[<?php echo $no; ?>]" class="form-control  main_input_shortage" value='<?php echo $key['shortage']; ?>'  title="qty_<?php echo $no; ?>" onfocus="save_qty_shortage($(this).val(),'<?php echo $no; ?>')" oninput="calculate_qty_main_shortage(this,'<?php echo $no; ?>')">
                          </td>
                          <td>
                            <input type="number" name="damage[<?php echo $no; ?>]" class="form-control  main_input_damage" value='<?php echo $key['damage']; ?>'  title="qty_<?php echo $no; ?>" onfocus="save_qty_damage($(this).val(),'<?php echo $no; ?>')" oninput="calculate_qty_main_damage(this,'<?php echo $no; ?>')">
                          </td>
                          <td>
                            <input type="text" id="uom[<?php echo $no; ?>]" name="uom[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                            <textarea type="text" id="remarks[<?php echo $no; ?>]" name="remarks[<?php echo $no; ?>]" class="form-control" readonly><?php echo $key['remarks']; ?></textarea>
                          </td>
                           </tr>

                      <?php $no++;} ?>
                        
                      </tbody>
                    </table>
                    
                  </div>
                </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <a href="<?php echo base_url();?>osd/osd_list" class="btn btn-secondary " title="Submit">
              <i class="fa fa-close"></i> Cancel
            </a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->





 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "<?php echo base_url();?>osd/approve_osd_form/<?php echo $osd_list['osd_no']; ?>/"+param;
      }
    })

  }

  function add_heat(btn,no) {

    var row = $(btn).closest('tr');
    $(row).after('<tr>'+row.html()+'</tr>');
    var next = $(row).next();
    var next_btn = $(next).find('button');

    var next_qty_over     = $(next).find('input[name="over['+no+']"');
    var next_qty_shortage = $(next).find('input[name="shortage['+no+']"');
    var next_qty_damage   = $(next).find('input[name="damage['+no+']"');

    $(next_btn).replaceWith('<button type="button" onclick="delete_heat(this);" class="btn btn-danger" title="Delete Heat Number"><i class="fa fa-times"></i></button>');

    $(next_qty_over).removeClass('main_input_over');
    $(next_qty_shortage).removeClass('main_input_shortage');
    $(next_qty_damage).removeClass('main_input_damage');

    $(next_qty_over).val(0);
    $(next_qty_shortage).val(0);
    $(next_qty_damage).val(0);

  }

   function delete_heat(btn,no) {
    var row = $(btn).closest('tr');

    var input_over     = $(row).find('input[name="over['+no+']"');
    var input_shortage = $(row).find('input[name="shortage['+no+']"');
    var input_damage   = $(row).find('input[name="damage['+no+']"');

    var target_over     = $(".main_input_over[title="+$(input_over).attr('title')+"]");
    var target_shortage = $(".main_input_shortage[title="+$(input_shortage).attr('title')+"]");
    var target_damage   = $(".main_input_damage[title="+$(input_damage).attr('title')+"]");

    var new_qty_over      = parseInt($(target_over).val()) + parseInt($(input_over).val());
    var new_qty_shortage  = parseInt($(target_shortage).val()) + parseInt($(input_shortage).val());
    var new_qty_damage    = parseInt($(target_damage).val()) + parseInt($(input_damage).val());

    $(target_over).val(new_qty_over);
    $(target_shortage).val(new_qty_shortage);
    $(target_damage).val(new_qty_damage);

    $(row).remove();
  }

  var qty_def_over;
  var qty_def_shortage;
  var qty_def_damage;

  function save_qty_over(input) {
    qty_def_over = input;
  }

  function save_qty_shortage(input) {
    qty_def_shortage = input;
  }

  function save_qty_damage(input) {
    qty_def_damage = input;
  }

  function calculate_qty_main_over(input) {

    if($(input).hasClass("main_input_over") == false){

      var target            = $(".main_input_over[title="+$(input).attr('title')+"]");
      var target_qty        = parseInt($(target).val());
      var input_qty_default = parseInt(qty_def_over);
      var input_qty         = parseInt($(input).val());
      var operation;
     
      if(input_qty_default > input_qty){

        var dif_qty = parseInt(input_qty_default - input_qty);
      
        if(input_qty > 0){
        
          $(target).val(target_qty + dif_qty);
          qty_def_over = input_qty;
        
        } else {

          sweetalert('error', 'Minimal Input Qty is 1');
          $(input).val(input_qty_default);

        }

      } else {

        var dif_qty = parseInt(input_qty - input_qty_default);

        if(target_qty - dif_qty > 0){
        
          $(target).val(target_qty - dif_qty);
          qty_def_over = input_qty;
        
        } else {

          sweetalert('error', 'Your input data is over from qty that you have.');
          $(input).val(input_qty_default);

        }        
      }
    }

  }

  function calculate_qty_main_shortage(input) {

    if($(input).hasClass("main_input_shortage") == false){

      var target            = $(".main_input_shortage[title="+$(input).attr('title')+"]");
      var target_qty        = parseInt($(target).val());
      var input_qty_default = parseInt(qty_def_shortage);
      var input_qty         = parseInt($(input).val());
      var operation;
     
      if(input_qty_default > input_qty){

        var dif_qty = parseInt(input_qty_default - input_qty);
      
        if(input_qty > 0){
        
          $(target).val(target_qty + dif_qty);
          qty_def_shortage = input_qty;
        
        } else {

          sweetalert('error', 'Minimal Input Qty is 1');
          $(input).val(input_qty_default);

        }

      } else {

        var dif_qty = parseInt(input_qty - input_qty_default);

        if(target_qty - dif_qty > 0){
        
          $(target).val(target_qty - dif_qty);
          qty_def_shortage = input_qty;
        
        } else {

          sweetalert('error', 'Your input data is over from qty that you have.');
          $(input).val(input_qty_default);

        }        
      }
    }

  }

  function calculate_qty_main_damage(input) {

    if($(input).hasClass("main_input_damage") == false){

      var target            = $(".main_input_damage[title="+$(input).attr('title')+"]");
      var target_qty        = parseInt($(target).val());
      var input_qty_default = parseInt(qty_def_damage);
      var input_qty         = parseInt($(input).val());
      var operation;
     
      if(input_qty_default > input_qty){

        var dif_qty = parseInt(input_qty_default - input_qty);
      
        if(input_qty > 0){
        
          $(target).val(target_qty + dif_qty);
          qty_def_damage = input_qty;
        
        } else {

          sweetalert('error', 'Minimal Input Qty is 1');
          $(input).val(input_qty_default);

        }

      } else {

        var dif_qty = parseInt(input_qty - input_qty_default);

        if(target_qty - dif_qty > 0){
        
          $(target).val(target_qty - dif_qty);
          qty_def_over = input_qty;
        
        } else {

          sweetalert('error', 'Your input data is over from qty that you have.');
          $(input).val(input_qty_default);

        }        
      }
    }

  }

</script>