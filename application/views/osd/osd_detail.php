<?php $osd_list = $osd_list[0]; ?>

<script>

  <?php for($i=1;$i<=sizeof($osd_list_detail);$i++){ ?>

    var no=1;

    function checkunique<?php echo $i; ?>(input, num) {

      var text<?php echo $i; ?> = input;
      var empty_val = "-";
      var po_number = $("input[id='po_number']").val();

      $.ajax({
        url: "<?php echo base_url(); ?>osd/unique_no_check_det",
        type: "post",
        data: {
          'osd_det_id': text<?php echo $i; ?>,
          'po_number': po_number,
        },
        success: function(data) {
          var dup = 0;

          for(var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='unique_no"+i+"']").val() == text<?php echo $i; ?>){
                data = 'Error : Duplicate Unique No on the list!';
              }
            }
          }
          
          if(data.includes("Error")){

              $(input).addClass('is-invalid');
              $('.invalid-feedback').remove( ":contains('Error')" );
              $(input).after('<div class="invalid-feedback">'+data+'</div>');

              $("input[name='mrir_no["+num+"]']").val(empty_val);
                     
              $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

              $('.invalid-feedback').remove( ":contains('Error')" );
              $(input).removeClass('is-invalid');
              $(input).addClass('is-valid');

              var res = data.split("; ");

              $("input[name='mrir_no["+num+"]']").val(res[0]);
           
            
              $('button[name=submitBtn]').prop("disabled", false);
          }
        }
      });
  }

<?php } ?>

</script>

<script type="text/javascript">

  function check_po(input) {
    
    var po_number = input;

    if(po_number == ''){

        $(input).addClass('is-invalid');
        $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
        $('button[name=submit]').prop("disabled", true);

        $('input[name="client"]').val('');
        $('input[name="project_title"]').val('');
        $('input[name="date_of_receiving"]').val('');
        $('input[name="project_ref"]').val('');
    
    } else {

      $.ajax({
        url: "<?php echo base_url();?>osd/check_po_number/",
        type: "post",
        data: {
          po_number: po_number
        },
        success: function(data) {

          var data = JSON.parse(data);
       
          if(data.hasil == 0){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('DO / PL Number not found..')" );
            $(input).after('<div class="invalid-feedback">DO / PL Number not found..</div>');
            $('button[name=submit]').prop("disabled", true);

            $('input[name="client"]').val('');
            $('input[name="project_title"]').val('');
            $('input[name="date_of_receiving"]').val('');
            $('input[name="project_ref"]').val('');
            $('input[name="project_id"]').val('');

          } else {

            $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            $('input[name="client"]').val(data.client);
            $('input[name="project_title"]').val(data.project_title);
            $('input[name="date_of_receiving"]').val(data.date_of_receiving);
            $('input[name="project_ref"]').val(data.project_ref);
            $('input[name="project_id"]').val(data.project_id);

          }

          if(!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }
        }
      });
    }
  }
</script>


<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>osd/approve_osd_form" id='form-osd'  onsubmit="return validateForm()" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
                <div class="row">
                 <div class="col-md">
                  <div class="form-group">
                     <label>Purchase Order Number</label>
                      <input type="text" class="form-control" name="po_number" id="po_number" value='<?php echo $osd_list["po_number"]; ?>' readonly>
                  </div>
                </div>
              </div>

            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" >
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th><input type="checkbox" id="selectall_omission" style="width: 15px; height: 15px; text-align: center;"></th>
                         
                          <th>MATERIAL CODE</th>
                          <th>DESCRIPTION</th>
                          <th>MRIR NO</th>                         
                          <th>OVER</th>
                          <th>SHORTAGE</th>
                          <th>DAMAGE</th>
                          <th>UOM</th>
                         
                            
                        </tr>
                        
                      </thead>
                      <tbody id="table_element" class="table-border">

                      <?php $no=1; foreach ($osd_list_detail as $key) { ?>
                        <tr>
                          <td>
                              <input type="hidden" name="id[<?php echo $no; ?>]" value="<?= $key['osd_det_id'] ?>">
                              <input type="hidden" name="osd_no[<?php echo $no; ?>]" value="<?= $key['osd_no'] ?>">
                              <input type="hidden" id='id_checkbox' name="id_checkbox[<?php echo $no; ?>]" value="0">
                              <input type="hidden" id='no' name="no[<?php echo $no; ?>]" value="<?php echo $no; ?>" disabled>
                              <input type="checkbox" id='cb_add_mr' name="cb_add_mr[<?php echo $no; ?>]" style="width: 15px; height: 15px; text-align: center;" data-toggle="collapse" data-target="#accordion<?php echo $no; ?>" class="clickable">     
                          </td>
                          
                          <td>
                            <input type="text" class='form-control' id='code_material[<?php echo $no; ?>]' name='code_material[<?php echo $no; ?>]' value='<?php echo $key['code_material']; ?>' readonly>

                             <script> checkunique<?php echo $no;  ?>(<?php echo $key["osd_det_id"]; ?>,<?php echo $no; ?>);</script>
                          </td>                          
                          <td>
                              <textarea type="text" id="description[<?php echo $no; ?>]" name="description[<?php echo $no; ?>]" class="form-control" readonly><?php echo $key['material']; ?></textarea>
                          </td>
                        
                          <td>
                            <input type="text" id="mrir_no[<?php echo $no; ?>]" name="mrir_no[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                          <td>
                            <input type="number" name="over[<?php echo $no; ?>]" class="form-control" value='<?php echo $key['over']; ?>' readonly>
                          </td>
                          <td>
                            <input type="number" name="shortage[<?php echo $no; ?>]" class="form-control" value='<?php echo $key['shortage']; ?>' readonly>
                          </td>
                          <td>
                            <input type="number" name="damage[<?php echo $no; ?>]" class="form-control" value='<?php echo $key['damage']; ?>' readonly>
                          </td>
                          <td>
                            <input type="text" id="uom[<?php echo $no; ?>]" name="uom[<?php echo $no; ?>]" class="form-control" readonly>
                          </td>
                                                   
                        </tr>

                           <tr class="table table-borderless text-left" name="tr_detail_<?php echo $no; ?>">
                              <td colspan="17">
                                  <div id="accordion<?php echo $no; ?>" class="<?php if($osd_list['status'] != '3'){ ?>collapse<?php } ?> targets">
                                      <div class="form-row">
                          
                                        <div class="form-group col-md-6">
                                          <label>OSD ACTION</label>
                                             <select name='action_osd[<?php echo $no; ?>]'  class="form-control">
                                                <option value=''>---</option>
                                                <option value='acceptable' <?php if($key['action_osd'] == "acceptable"){ echo "selected"; } ?>>Acceptable</option>
                                                <option value='quarantine' <?php if($key['action_osd'] == "quarantine"){ echo "selected"; } ?>>Quarantine</option>
                                                <option value='others' <?php if($key['action_osd'] == "others"){ echo "selected"; } ?>>Others</option>
                                              </select>
                                        </div>

                                        <div class="form-group col-md-6">
                                          <label>SOLUTION OS&D</label>
                                           <textarea type="text" id="remarks[<?php echo $no; ?>]" name="remarks[<?php echo $no; ?>]" class="form-control" placeholder='Fill Up Solution Of OSD'><?php echo $key['remarks']; ?></textarea>
                                        </div>

                                      </div>  
                                  </div>
                              </td>
                            </tr>

                      <?php $no++;} ?>
                        
                      </tbody>
                    </table>


                  </div>
                </div>

            </div>
          </div>
          <div class="text-right mt-3">

            <?php if($read_permission[60] == 1){ ?>

            <!-- <a href="<?php echo base_url(); ?>osd/osd_pdf/<?php echo strtr($this->encryption->encrypt($osd_list['osd_no']), '+=/', '.-~'); ?>" class="btn btn-primary" title="Print Out Document"><i class="fas fa-file-pdf"></i> Print Out</a> -->

            <?php } ?>

            <?php if($read_permission[59] == 1){ ?>

              <?php if($osd_list['status'] != 3){ ?>
                <button name='btnsubmit_osd' class='btn btn-primary' disabled>Submit</button>
              <?php } ?>
            <?php } ?>

            <a href="<?php echo base_url();?>osd/osd_list" class="btn btn-secondary " title="Cancel">
              <i class="fa fa-close"></i> Cancel
            </a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->





 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "<?php echo base_url();?>osd/approve_osd_form/<?php echo $osd_list['osd_no']; ?>/"+param;
      }
    })

  }


  <?php for($i=1;$i<=sizeof($osd_list_detail);$i++){ ?>

  $('input[name="cb_add_mr[<?php echo $i; ?>]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[<?php echo $i; ?>]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1);
    } else if($(this).prop("checked") == false){
      $(id_array).val(0);
    }

    if($('input[name="cb_add_mr[<?php echo $i; ?>]"]:checked').length > 0){
      $('button[name="btnsubmit_osd"]').removeAttr('disabled');
    } else {
      $('button[name="btnsubmit_osd"]').attr('disabled', true);
    }
    
  });

 <?php } ?> 

 function validateForm() {

  <?php for($i=1;$i<=sizeof($osd_list_detail);$i++){ ?>

  var id_checkbox<?php echo $i; ?>  = document.forms["form-osd"]["id_checkbox[<?php echo $i; ?>]"].value;
  var action_osd<?php echo $i; ?>   = document.forms["form-osd"]["action_osd[<?php echo $i; ?>]"].value;
  var remarks<?php echo $i; ?>      = document.forms["form-osd"]["remarks[<?php echo $i; ?>]"].value;
  var unique_no<?php echo $i; ?>    = document.forms["form-osd"]["unique_no[<?php echo $i; ?>]"].value;
  
 
  if(action_osd<?php echo $i; ?> == ""){   
    if(id_checkbox<?php echo $i; ?> == 1){
      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'Choice Action OS & D on Unique No '+ unique_no<?php echo $i; ?>,
      });
      return false;
    }
  } else if(remarks<?php echo $i; ?> == ""){
    if(id_checkbox<?php echo $i; ?> == 1){
      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'Fill Up Solution OS & D on Unique No '+ unique_no<?php echo $i; ?>,
      });
      return false;
    }
  } 

  <?php } ?>

}


</script>