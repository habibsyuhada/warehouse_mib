<script type='text/javascript'>
  $(window).on('load', function() { add_element(); });

  var count_element = 1;
  function add_element(){

    var html_element =  '<tr id="tr_element_' + count_element + '">' +
                          '<td><input type="text" id="unique_no'+count_element+'" onfocus="unique_no_func(' + count_element + ');" name="unique_no[' + count_element + ']" class="form-control" onblur="checkunique(this,' + count_element + ');"></td>' +
                          '<td><textarea type="text" id="description[' + count_element + ']" name="description[' + count_element + ']" class="form-control" readonly></textarea></td>' +
                          '<td><input type="text" id="length[' + count_element + ']" name="length[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="width[' + count_element + ']" name="width[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="thk[' + count_element + ']" name="thk[' + count_element + ']" class="form-control" readonly></td>' + 
                          '<td><input type="text" id="spec[' + count_element + ']" name="spec[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="plate_tag_no[' + count_element + ']" name="plate_tag_no[' + count_element + ']" class="form-control" readonly></td>' +  
                          '<td><input type="text" id="series_heat_no[' + count_element + ']" name="series_heat_no[' + count_element + ']" class="form-control" readonly></td>' +  
                          '<td><input type="text" id="brand[' + count_element + ']" name="brand[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="do_pl[' + count_element + ']" name="do_pl[' + count_element + ']" class="form-control" readonly></td>' +
                          '<td><input type="text" id="mrir_no[' + count_element + ']" name="mrir_no[' + count_element + ']" class="form-control" readonly></td>' +

                          '<td><input type="number" name="over[' + count_element + ']" class="form-control" placeholder="*"></td>' +
                          '<td><input type="number" name="shortage[' + count_element + ']" class="form-control" placeholder="*"></td>' +
                          '<td><input type="number" name="damage[' + count_element + ']" class="form-control" placeholder="*"></td>' +
                          
                          '<td><input type="text" id="uom[' + count_element + ']" name="uom[' + count_element + ']" class="form-control" readonly></td>' +

                          '<td><textarea type="text" id="remarks[' + count_element + ']" name="remarks[' + count_element + ']" class="form-control" required></textarea></td>' +
                          
                          '<td><button class="btn btn-danger" type="button" onclick="delete_element(' + count_element + ')"><i class="fa fa-trash"></i></button></td>' + 

                        '</tr>';

    $('#table_element').append(html_element);

    count_element++;
  }
</script>
<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" action="<?php echo base_url();?>osd/osd_new_form" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Client</label>
                      <input type="text" class="form-control" name="client" id="client" placeholder="---"  readonly>                
                      <input type="hidden" class="form-control" name="osd_no" id="osd_no" value='<?php echo $get_osd_no; ?>' readonly>
                      <input type="hidden" class="form-control" name="project_id" id="project_id" readonly>                
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                     <label>Purchase Order Number</label>
                      <input type="text" class="form-control" name="po_number" id="po_number" placeholder="---" onblur="check_po(this)" required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md">
                  <div class="form-group">
                      <label>Project Title</label>
                      <input type="text" class="form-control" name="project_title" id="project_title" placeholder="---" readonly>                
                  </div>
                </div>
                <div class="col-md">
                  <div class="form-group">
                      <label>Project Ref</label>
                      <input type="text" class="form-control" name="project_ref" id="project_ref" placeholder="---" readonly>                
                  </div>
                </div>
              </div>


            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" >
                      <thead>
                        <tr bgcolor="#008060" style="color: white !important; text-align: center;">
                          <th>UNIQUE NO.</th>
                          <th>DESCRIPTION</th>
                          <th>LENGTH</th>
                          <th>OD/WIDTH</th>
                          <th>THK</th>
                          <th>SPEC</th>
                          <th>PLATE / TAG NO</th>
                          <th>HEAT /SERIES NO</th>
                          <th>BRAND</th>
                          <th>DO / PL NO</th>
                          <th>MRIR NO</th>
                         
                          <th>OVER</th>
                          <th>SHORTAGE</th>
                          <th>DAMAGE</th>

                          <th>UOM</th>
                          <th>REMARKS</th>
                          
                          <th width="180">
                            <button class="btn btn-success float-right" style='background-color: #004cc2;' type="button" onclick="add_element()"><i class="fa fa-plus"></i> Add Material</button>
                          </th>
                        </tr>
                        
                      </thead>
                      <tbody id="table_element" class="table-border">
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type='submit' name='submitBtn' id='submitBtn' value='submit' class="btn btn-success" title="Submit" disabled ><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>pcms/mto" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>

</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">

  var no=1;
  

  var delayTimer;
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $("#po_number").autocomplete({
     source: "<?php echo base_url(); ?>osd/po_number_list",
     autoFocus: true,
     classes: {
         "ui-autocomplete": "highlight"
     }
  });

  function check_po(input) {
    var po_number = $(input).val();

    if(po_number == ''){

        $(input).addClass('is-invalid');
        $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
        $('button[name=submit]').prop("disabled", true);

        $('input[name="client"]').val('');
        $('input[name="project_title"]').val('');
        $('input[name="date_of_receiving"]').val('');
        $('input[name="project_ref"]').val('');
    
    } else {

      $.ajax({
        url: "<?php echo base_url();?>osd/check_po_number/",
        type: "post",
        data: {
          po_number: po_number
        },
        success: function(data) {

          var data = JSON.parse(data);
         // console.log(data.hasil);

          if(data.hasil == 0){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('DO / PL Number not found..')" );
            $(input).after('<div class="invalid-feedback">DO / PL Number not found..</div>');
            $('button[name=submit]').prop("disabled", true);

            $('input[name="client"]').val('');
            $('input[name="project_title"]').val('');
            $('input[name="date_of_receiving"]').val('');
            $('input[name="project_ref"]').val('');
            $('input[name="project_id"]').val('');

          } else {

            $('.invalid-feedback').remove( ":contains('Duplicate Drawing Number')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            $('input[name="client"]').val(data.client);
            $('input[name="project_title"]').val(data.project_title);
            $('input[name="date_of_receiving"]').val(data.date_of_receiving);
            $('input[name="project_ref"]').val(data.project_ref);
            $('input[name="project_id"]').val(data.project_id);

          }

          if (!$('.is-invalid').length) {
            $('button[name=submit]').prop("disabled", false);
          }

        }
      });

    }
  }
</script>

<script type="text/javascript">

  function unique_no_func(nox){

   var po_number = $("input[id='po_number']").val();
   
    $("input[name='unique_no["+nox+"]']").autocomplete({
      source: function(request,response){
        $.post('<?php echo base_url(); ?>osd/uniqno_autocomplete',{term: request.term,po_number: po_number}, response, 'json');
      },
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });

  }


  function delete_element(count){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        Swal.fire(
          'Deleted!',
          'Your data has been deleted.',
          'success'
        )

        delete_element_process(count);

      }
    })
  }

  function delete_element_process(count){
    $('#tr_element_' + count).remove();
  }

   function checkunique(input, num){

    var text = $(input).val();
    var empty_val = "-";
    var po_number = $("input[id='po_number']").val();
 
      // Do the ajax stuff
      $.ajax({
        url: "<?php echo base_url(); ?>osd/unique_no_check",
        type: "post",
        data: {
          'unique_no': text,
          'po_number': po_number,
        },
        success: function(data) {
          var dup = 0;

          for( var i = 1; i < num; i++){
            if(i != num){
              if($("input[id='unique_no"+i+"']").val() == text){
                console.log($("input[id='unique_no"+num+"']").val()+"--"+text);
                data = 'Error : Duplicate Unique No on the list!';
              }
            }
          }
          

          if(data.includes("Error")){

            $(input).addClass('is-invalid');
            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).after('<div class="invalid-feedback">'+data+'</div>');
            
            $("textarea[name='description["+num+"]']").val(empty_val);
            $("input[name='length["+num+"]']").val(empty_val);
            $("input[name='width["+num+"]']").val(empty_val);
            $("input[name='thk["+num+"]']").val(empty_val);
            $("input[name='spec["+num+"]']").val(empty_val);
            $("input[name='plate_tag_no["+num+"]']").val(empty_val);
            $("input[name='series_heat_no["+num+"]']").val(empty_val);
            $("input[name='brand["+num+"]']").val(empty_val);
            $("input[name='do_pl["+num+"]']").val(empty_val);
            $("input[name='mrir_no["+num+"]']").val(empty_val);
            $("input[name='uom["+num+"]']").val(empty_val);
            
            $('button[name=submitBtn]').prop("disabled", true);
            
          } else {

            $('.invalid-feedback').remove( ":contains('Error')" );
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            var res = data.split("; ");

            $("textarea[name='description["+num+"]']").val(res[0]);
            $("input[name='length["+num+"]']").val(res[3]);
            $("input[name='width["+num+"]']").val(res[1]);
            $("input[name='thk["+num+"]']").val(res[2]);
            $("input[name='spec["+num+"]']").val(res[6]);
            $("input[name='plate_tag_no["+num+"]']").val(res[9]);
            $("input[name='series_heat_no["+num+"]']").val(res[5]);
            $("input[name='brand["+num+"]']").val(res[7]);
            $("input[name='do_pl["+num+"]']").val(res[8]);
            $("input[name='mrir_no["+num+"]']").val(res[10]);
            $("input[name='uom["+num+"]']").val(res[4]);
           
            $('button[name=submitBtn]').prop("disabled", false);
           
          }
        }
      });
  }

  </script>

  <script type="text/javascript">

  function validate_approve() {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
         //$('form').submit();
      }
    })

  }
</script>