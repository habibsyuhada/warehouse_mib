<?php 



  foreach($osd_list as $dt_mrir){
    
    $str_report_no  = $dt_mrir["osd_no"];
    $str_po_number  = $dt_mrir["po_number"];


    
  }

    $created_date   = $this->osd_mod->get_receiving_data($osd_list_detail[0]["receive_det_id"]);


?>
<!DOCTYPE html>
<html><head>
  <title><?php echo $str_report_no." - ".$str_project[0]['project_name']; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 3.8cm;
      margin-left: 2.5cm;
      margin-right: 1.5cm;
      margin-bottom: 2cm;
      font-family: "helvetica";
      font-size: 50% !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      padding-top: 15px;
      padding-left: 1.5cm;
      padding-right: 1.5cm;
    
    }

    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table tr {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }
   
  </style>
</head><body>
  <header>
    <img src="img/logo.png" style="width: 100px;"><br><br>
    <table>
      <tr>
        <td style="font-size: 10px; width: 100px;">Client</td>
        <td style="font-size: 10px;">:</td>
        <td style="font-size: 10px; width: 250px;"></td>
        <td style="font-size: 10px; width: 350px;"><b><u>MATERIAL RECEIVING AND INSPECTION REPORT</u></b></td>
        <td style="font-size: 10px; width: 150px;">Report No.</td>
        <td style="font-size: 10px; width: 5px;">  </td>
        <td style="font-size: 10px; width: 10px; border: 1px solid; padding: 2px;"><?= $str_report_no ?></td>
      </tr>
      <tr>
        <td style="font-size: 10px; width: 100px;">Project Title</td>
        <td style="font-size: 10px;">:</td>
        <td style="font-size: 10px; width: 250px;"><?php echo $str_project[0]['project_name']; ?></td>
        <td style="font-size: 10px; width: 350px;"></td>
       <td style="font-size: 10px; width: 150px;">&nbsp;</td>
        <td style="font-size: 10px; width: 5px;">&nbsp;</td>
        <td style="font-size: 10px; width: 10px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-size: 10px; width: 100px;">Project Ref</td>
        <td style="font-size: 10px;">:</td>
        <td style="font-size: 10px; width: 250px;"></td>
        <td style="font-size: 10px; width: 350px;"></td>
        <td style="font-size: 10px; width: 150px;" colspan="3">
            <table>
              <tr>
                  <td>PO. No</td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                  <td width='100px'><?php  echo $str_po_number; ?></td>
              </tr>
               <tr>
                  <td>Date Of Receiving</td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</td>
                  <td><?php echo date("d-M-y",strtotime($created_date[0]["date_created"])) ?></td>
              </tr>
              
            </table>
        </td>
      </tr> 
       
    </table>
  </header>
  <table class="table-body" width='100%' border="1" style="text-align: center;border-collapse: collapse !important; margin-left: -25px;">
  <thead><tr>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 20px;"><b>S/No</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 70px;"><b>Description / Delivery Condition</b></th>
        <th bgcolor="#a6ffa6" colspan="3" style="font-size: 9px; width: 150px; max-height: 25;"><b>Size (In MM)</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Spec / Grade</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Plate / Tag No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Heat / Series No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Mill Cert No.</b></th>        
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Brand</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>DO No. / PL No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Unique Ident No.</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Qty</b></th>        
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Over</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Shortage</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Damage</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>UOM</b></th>
        <th bgcolor="#a6ffa6" rowspan="2" style="font-size: 9px; width: 50px;"><b>Remarks</b></th>
    </tr></thead>
    <thead><tr>
      <th bgcolor="#a6ffa6" style="font-size: 9px;">Length</th>
      <th bgcolor="#a6ffa6" style="font-size: 9px;">Width/OD</th>
      <th bgcolor="#a6ffa6" style="font-size: 9px;">Sch/Thk</th>
    </tr></thead>    
    <tbody><?php $no=1; foreach($osd_list_detail as $dt_material){ $data_detail = $this->osd_mod->get_material_unique_no($dt_material["unique_no"]); $data_detail = $data_detail[0]; ?><tr>    
      <td><?= $no ?></td>

      <td><?= $data_detail["description"] ?></td>
      <td><?= $data_detail["length"] ?></td>
      <td><?= $data_detail["width_or_od"] ?></td>
      <td><?= $data_detail["sch_or_thk"] ?></td>
      <td><?= $data_detail["spec"]?></td>
      <td><?= $data_detail["plate_or_tag_no"] ?></td>
      <td><?= $data_detail["heat_or_series_no"] ?></td>
      <td><?= $data_detail["mill_cert_no"] ?></td>      
      <td><?= $data_detail["supplier_name"] ?></td>
      <td><?= $data_detail["do_or_pl_no"] ?></td>
      <td><?= $data_detail["unique_ident_no"] ?></td>
      <td><?= $data_detail["qty"] ?></td>
      <td><?= $dt_material["over"] ?></td>
      <td><?= $dt_material["shortage"] ?></td>
      <td><?= $dt_material["damage"] ?></td>
      <td><?= $data_detail["uom"] ?></td>      
      <td><?= $dt_material["remarks"] ?></td>      
      
    </tr><?php $no++; } ?></tbody>  
  </table>
<br>
  <table width='100%'>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td>Lead Material / Material Control</td>
          <td>SMOE QC Inspector</td>
          <td>Client Representative</td>
          <td>3rd Party</td>
      </tr>
      <tr>
          <td><!-- Sign / Date : <?php //echo date("d-M-y",strtotime($dt_mrir->created_date)); ?> --></td>
          <td><!-- Sign / Date : <?php //echo date("d-M-y",strtotime($dt_mrir->approved_date)); ?> --></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
          <td><!-- Approval No : 001-LOGISTIC-MRIR-<?php //echo $str_report_no ?> --></td>
          <td><!-- <?php //echo $approval_log[0]["approved_category"]; ?>/<?php //echo $approval_log[0]["id"] ?>/<?php //echo $approval_log[0]["approved_request_no"] ?> --></td>
          <td></td>
          <td></td>
      </tr>
  </table>
  <footer>    
  </footer>
</body></html>