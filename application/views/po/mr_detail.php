<?php 
  $mr_list = $mr_list[0];
  $mr_list_detail_sat = $mr_list_detail[0];

  //print_r($mr_list_detail_sat);
?>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" id='form-mr' action="<?php echo base_url();?>po/mr_detail_after" enctype="multipart/form-data">

    <input type="hidden" class="form-control" name="mr_numberx" id="mr_numberx"  value='<?php echo $mr_list['mr_number']; ?>' readonly>  
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?> </h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col-md">
                    <div class="form-group">                      
                      <label>Date</label>
                        <input type="text" class="form-control" name="po_create_date" id="po_create_date"  value='<?php echo date("Y-m-d"); ?>' readonly> 
                        <input type="hidden" class="form-control" name="cat_id" id="cat_id"  value='<?php echo $mr_list['budget_cat_id']; ?>' readonly> 
                        <input type="hidden" class="form-control" name="dept_id" id="dept_id"  value='<?php echo $mr_list['budget_dept_id']; ?>' readonly> 
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    <label>Quotation</label>
                       <input type="text" class="form-control" name="vendor" id="vendor"  value='<?php echo $vendor_data[$mr_list_detail_sat['vendor_winner']] ?>' readonly>  
                      
                  </div>
                </div>                
              </div>

              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                     <label>Alamat</label>
                        <textarea rows="4"  type="text" class="form-control" name="created_by" id="created_by" readonly><?php echo $mr_list_detail_sat["name"];  ?>&#13;&#10;<?php echo $mr_list_detail_sat["address"];  ?>&#13;&#10;Telp. <?php if(!empty($mr_list_detail_sat["phone_no"])){ echo $mr_list_detail_sat["phone_no"]; } else { echo "-"; }  ?>&#13;&#10;Email : <?php echo $mr_list_detail_sat["email"];  ?></textarea>
                      
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    <label>Reference</label>
                       <input type="text" class="form-control" name="reference_no" id="reference_no"  value='<?php echo  $mr_list_detail_sat["reference_no"] ?>' readonly>  
                  </div>
                </div>                
              </div>
       
          
           <div class="col-md-12">
                    <table class="table text-muted text-center" id='mr_detail'>
                       <thead class="bg-green-smoe text-white">
                        <tr>                          
                          <th>#</th>                                                
                          <th>MR NO</th>                                                
                          <th>DESCRIPTION</th>
                          <th>QTY</th>
                          <th>UOM</th> 
                          <th>PRICE PER UNIT</th>
                          <th>TOTAL AMOUNT</th>
                          <th>CURRENCY</th>
                          <th style="width: 200px;overflow: hidden;">PO REMARKS</th>                      
                        </tr>                        
                      </thead>
                      <tbody>

                      <?php $no=1; foreach($mr_list_detail as $key) { ?>
                        <tr>
                          <td>
                            <input type="hidden" name="id[]" value="<?= $key['id'] ?>" >
                            <input type="hidden" id='id_checkbox' name="id_checkbox[]" value="0" >
                            <input type="hidden" id='no' name="no[]" value="<?php echo $no; ?>" >
                            <input type="checkbox" id='cb_add_mr' name="cb_add_mr[]" style="width: 15px; height: 15px; text-align: center;" >                         
                          </td>

                          <td>
                              <?php echo $key['mr_number']; ?>
                               <input type="hidden" id='mr_number' name="mr_number[]" value='<?php echo $mr_list['mr_number'] ?>'>      
                          </td>
                          
                          <td>
                              <?php echo $key['tec_spec']; ?>
                              <input type="hidden" id='tec_spec' name="tec_spec[]" value='<?php echo $key['tec_spec'] ?>'>
                          </td>

                         
                          <td>
                            <?php echo $key['qty_req']; ?>  
                            <input type="hidden" id='req_qty' name="req_qty[]" value='<?php echo $key['qty_req']; ?>' style='width: 50px;' >                                                     
                          </td>

                          <td>
                             <?php echo $uom[$key['uom_req']]; ?>    
                             <input type="hidden" id='req_qty' name="uom_req[]" value='<?php echo $key['uom_req']; ?>' style='width: 50px;' >                                                     
                          </td>

                          <td>
                            <?php if($key['price_per_unit'] <= 0){ ?>
                              <input type="number" id='price_per_unit_<?php echo $key["id"]; ?>'  class='form-control' title="price_per_unit_<?php echo $key['id']; ?>" name="price_per_unit[]"  min='1' oninput="calculate_total_amount(this);" placeholder='Actual Price' disabled> 
                              <?php } else { ?>
                                <?php echo $key['price_per_unit']; ?>
                              <?php } ?>                                                                       
                          </td>


                          <td>
                             <?php if($key['total_amount'] <= 0){ ?>
                              <input type="number" id='total_amount_<?php echo $key["id"]; ?>'  class='form-control' title="total_amount_<?php echo $key['id']; ?>" name="total_amount[]"  min='1' placeholder='Total Amount' readonly> 
                              <?php } else { ?>
                                <?php echo $key['total_amount']; ?>
                              <?php } ?>                                                                   
                          </td>


                          <td>
                            <select id="currency_<?php echo $key["id"]; ?>" name="currency[]"  class="form-control" required  disabled>
                                <?php 
                                    foreach ($currency_list as $value) { 
                                      if($value['id_cur'] == $key['currency']){
                                        echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; 
                                      }
                                    } 
                                ?>
                              </select>                                                   
                          </td>

                         
                       

                          <td>
                            <?php if($key['remarks_po'] == ""){ ?>
                              <input type="text" id="remarks_po_<?php echo $key["id"]; ?>" name="remarks_po[]" class="form-control" disabled>
                            <?php } else { ?>
                              <?php echo $key['remarks_po'] ?>
                            <?php } ?>
                          </td>

                        
                         
                        </tr>
                      <?php $no++; } ?>
                        
                      </tbody>
                      
                    </table>

            </div>


                  </div>
                </div>


            </div>
          </div>

          <div class="col-md-12">
           <div class="my-3 p-3 bg-white rounded shadow-sm">
            
                <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
                  <div class="container-fluid">

                    
                  <div class="text-right mt-3">

                    <button type='submit' class="btn btn-success" value="Next"><i class="fas fa-arrow-circle-right"></i> Next </button>

                    <a href="<?php echo base_url();?>po/po_list" class="btn btn-secondary " title="Cancel"><i class="fas fa-arrow-circle-left"></i>&nbsp; Back</a>
                                            
                    

                  </div>


               </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mr_list['mrs_no']; ?>/"+param;
      }
    })

  }

  function delete_data(param,mr,po) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>po/delete_data/"+param+"/"+mr+"/"+po;
      }
    })

  }

</script>

<script type="text/javascript">

   $('input[name="cb_add_mr[]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[]"]');
    var id_data = $(this).closest('td').find('input[name="id[]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1); 

      $('input[id="price_per_unit_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="price_per_unit_'+id_data.val()+'"]').prop('required',true);

      $('input[id="total_amount_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="total_amount_'+id_data.val()+'"]').prop('required',true);
      
      $('select[id="currency_'+id_data.val()+'"]').removeAttr('disabled');
      $('select[id="currency_'+id_data.val()+'"]').prop('required',true);

      $('input[id="remarks_po_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="remarks_po_'+id_data.val()+'"]').prop('required',true);    
    }

    else if($(this).prop("checked") == false){
      $(id_array).val(0);   

       $('input[id="price_per_unit_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="total_amount_'+id_data.val()+'"]').attr('disabled', true);
      $('select[id="currency_'+id_data.val()+'"]').attr('disabled', true);
       $('input[id="remarks_po_'+id_data.val()+'"]').attr('disabled', true);  
    }
   
    
  });


    function calculate_total_amount(input) {

    var row   = $(input).closest('tr');
    var po_qty   = $(row).find('input[name="req_qty[]"]').val();
    var data_ppu   = $(row).find('input[name="price_per_unit[]"]').val();

    var total_price = Number(data_ppu) * Number(po_qty);

    console.log(total_price);

    $(row).find('input[name="total_amount[]"]').val(total_price);

  }


  function _alert(){    
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
     }).then((result) => {
      if(result.value){
          $("#form-mr").submit();
      }
    });
  }

  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  });

  </script>

  <script type="text/javascript">

    $('#mr_detail').DataTable({
      "language": { 
      "infoFiltered": "" },
      "paging": false,
      "order": [],    
    });

  </script>