<?php 
  $mr_list = $mr_list[0];
  $mr_list_detail_sat = $mr_list_detail[0];

  //print_r($mr_list_detail_sat);
?>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" id='form-mr' action="<?php echo base_url();?>po/process_update_po" enctype="multipart/form-data">

    <input type="hidden" class="form-control" name="po_number" id="po_number"  value='<?php echo $po_nos; ?>' readonly>  
    <input type="hidden" class="form-control" name="mr_numberx" id="mr_numberx"  value='<?php echo $mr_list['mr_number']; ?>' readonly>  
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?> </h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                      
                      <?php if($mr_list["request_type"] == "Service Request"){ ?>
                        <label>Service Order No.</label>
                        <input type="text" class="form-control" name="po_number" id="po_number"  placeholder="Input PO NUmber" required=""> 
                       <?php } else if($mr_list["request_type"] == "Material Request"){ ?>
                        <label>Purchase Order No.</label>
                         <input type="text" class="form-control" name="po_number" id="po_number"  placeholder="Input PO NUmber" required="" > 
                       <?php } ?>
                       
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    <label>Quotation</label>
                       <input type="text" class="form-control" name="vendor" id="vendor"  value='<?php echo $vendor_data[$mr_list_detail_sat['vendor_winner']] ?>' readonly>  
                       <input type="hidden" class="form-control" name="cat_id" id="cat_id"  value='<?php echo $cat_id; ?>' readonly>  
                       <input type="hidden" class="form-control" name="dept_id" id="dept_id"  value='<?php echo $dept_id; ?>' readonly>  
                      
                  </div>
                </div>                
              </div>

              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                      <label>Date</label>
                        <input type="text" class="form-control" name="po_create_date" id="po_create_date"  value='<?php echo date("Y-m-d"); ?>' readonly> 
                      
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    <label>Reference</label>
                       <input type="text" class="form-control" name="reference_no" id="reference_no"  value='<?php echo  $mr_list_detail_sat["reference_no"] ?>' readonly>  
                  </div>
                </div>                
              </div>
              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                      <label>Alamat</label>
                        <textarea rows="4"  type="text" class="form-control" name="created_by" id="created_by" readonly><?php echo $mr_list_detail_sat["name"];  ?>&#13;&#10;<?php echo $mr_list_detail_sat["address"];  ?>&#13;&#10;Telp. <?php if(!empty($mr_list_detail_sat["phone_no"])){ echo $mr_list_detail_sat["phone_no"]; } else { echo "-"; }  ?>&#13;&#10;Email : <?php echo $mr_list_detail_sat["email"];  ?></textarea>
                      
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    &nbsp;
                      
                  </div>
                </div>                
              </div>
          
           <div class="col-md-12">
                    <table class="table text-muted text-center" id='mr_detail'>
                       <thead class="bg-green-smoe text-white">
                        <tr>
                          
                          <th>#</th>                                                
                          <th>MR NO</th>                                                
                          <th>DESCRIPTION</th>
                          <th>QTY</th>
                          <th>UOM</th> 
                          <th>PRICE PER UNIT</th>
                          <th>TOTAL AMOUNT</th>
                          <th>CURRENCY</th>                           
                         
                          <th style="width: 200px;overflow: hidden;">PO REMARKS</th>
                      
                        </tr>                        
                      </thead>
                      <tbody>

                      <?php $no=1; foreach($id as $kunci => $key) { ?>

                        <?php if($id_checkbox[$kunci] == 1){ ?>

                        <tr>

                          <td>
                            <input type="hidden" name="id[]" value="<?= $id[$kunci] ?>" >
                            <input type="hidden" id='id_checkbox' name="id_checkbox[]" value="<?= $id_checkbox[$kunci] ?>" >
                            <input type="hidden" id='no' name="no[]" value="<?php echo $no; ?>" >
                            <input type="checkbox" id='cb_add_mr' name="cb_add_mr[]" style="width: 15px; height: 15px; text-align: center;" checked="" >                         
                          </td>

                          <td>
                              <?php echo $mr_numberx[$kunci] ?>
                               <input type="hidden" id='mr_number' name="mr_number[]" value='<?php echo $mr_numberx[$kunci] ?>'>      
                          </td>
                          
                          <td>
                              <?php echo $tec_spec[$kunci] ?>
                              <input type="hidden" id='tec_spec' name="tec_spec[]" value='<?php echo $tec_spec[$kunci] ?>'> 
                          </td>

                         
                          <td>
                            <?php echo $req_qty[$kunci]; ?>  
                            <input type="hidden" id='req_qty' name="req_qty[]" value='<?php echo $req_qty[$kunci] ?>' style='width: 50px;' >                                                     
                          </td>

                          <td>
                             <?php echo $uom[$uom_req[$kunci]]; ?>                                                         
                          </td>

                          <td>
                           
                              <input type="hidden" class='form-control' name="price_per_unit[]" value='<?php echo $price_per_unit[$kunci] ?>'> 
                              
                               <?php echo number_format($price_per_unit[$kunci]); ?>
                                                                                            
                          </td>


                          <td>
                            
                              <input type="hidden"  class='form-control' name="total_amount[]"  value='<?php echo $total_amount[$kunci]; ?>' readonly> 
                            
                                <?php echo number_format($total_amount[$kunci]); ?>
                                                                                           
                          </td>


                          <td>
                            <select  name="currency[]"  class="form-control" required >
                                <?php 
                                    foreach ($currency_list as $value) { 
                                      if($value['id_cur'] == $currency[$kunci]){
                                        echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; 
                                      }
                                    } 
                                ?>
                              </select>                                                   
                          </td>

                         
                       

                          <td>
                              <input type="hidden"  name="remarks_po[]" class="form-control" value='<?php echo $remarks_po[$kunci]; ?>'>
                              <?php echo number_format($remarks_po[$kunci]) ?>
                            
                          </td>

                        
                         
                        </tr>
                      <?php } ?>
                      <?php $no++; } ?>
                        
                      </tbody>
                      <tfoot class="bg-green-smoe text-white">
                        <tr>
                          <th colspan="6">TOTAL</th>
                          <th><input type="text" id='total_all_amount' name='total_all_amount' value="<?php echo $total_all_amount; ?>" class="form-control" readonly=""></th>
                          <th colspan="5">&nbsp;</th>
                        </tr>
                         <tr>
                          <th colspan="6">DISKON</th>
                          <th><input type="number" id='diskon' name='diskon' value='0' class="form-control" oninput="count_grand_total_diskon($(this).val())"></th>
                          <th colspan="5">&nbsp;</th>
                        </tr> 
                         <tr>
                          <th colspan="6">PPN</th>
                          <th><input type="number" id='diskon' name='ppn' value='0' class="form-control" oninput="count_grand_total_ppn($(this).val())"></th>
                          <th colspan="5">&nbsp;</th>
                        </tr>
                         <tr>
                          <th colspan="6">GRAND TOTAL</th>
                          <th><input type="number" id='grand_total' name='grand_total' value='0' class="form-control" readonly></th>
                          <th colspan="5">&nbsp;</th>
                        </tr>                          
                      </tfoot>
                    </table>

                    <br><br>
              <label>Syarat dan Ketentuan / <i>Term and Condition</i></label>
              
              <ol type="a">
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kepada</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name='kepada_to' placeholder="Penerima PO" required="" value="<?php echo $term[0]["kepada_to"] ?>">
                    </div>  
                  </div>
                </li>
                <li>
                  <div class="form-group row">
                    <?PHP if($mr_list["service_request"] == 1){ ?>
                      <label class="col-sm-2 col-form-label">Lokasi Kerja</label>
                      <?php $var_so_po = "Lokasi Kerja"; ?>
                    <?PHP } else { ?>
                    <label class="col-sm-2 col-form-label">Waktu Pengiriman</label>
                     <?php $var_so_po = "Waktu Pengiriman"; ?>
                    <?PHP }  ?>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name='jadwal_pekerjaan' placeholder="<?php echo  $var_so_po; ?>" value="<?php echo $term[0]["jadwal_pekerjaan"] ?>">
                    </div>  
                  </div>
                </li>
                
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kondisi Pengiriman</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name='lokasi_kerja' value="<?php echo $term[0]["lokasi_kerja"] ?>" placeholder="Kondisi Pengiriman">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lokasi Pengiriman</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" rows="3" name='lokasi_pengiriman'><?php if(empty($term[0]["lokasi_pengiriman"])){ ?>PT. Mitra Energi Batam "MEB"&#13;&#10;JL. Lintas Gas Negara, Trans-Barelang, KM. 3,5 Tembesi, Sagulung, Batam, Kep. Riau-INDONESIA<?php } else { echo $term[0]["lokasi_pengiriman"]; } ?></textarea>
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pembayaran</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name='pembayaran' value="<?php if(empty($term[0]["pembayaran"])){ ?>14 Hari setelah hari setelah barang dan dokumen penagihan di terima oleh MEB.<?php } else { echo $term[0]["pembayaran"]; } ?>">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lain-lain</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" rows="12" name='lain_lain'><?php if(empty($term[0]["pembayaran"])){ ?>1. Semua pembayaran akan dilakukan setelah MEB menerima semua dokumen pendukung (invoice, PO konfirmasi dan dokumen pendukung lainnya). &#13;&#10;2. Dokumen penagihan dikirimkan ke MEB dengan alamat: Jl. Lintas Gas Negara, Trans Barelang, KM. 3,5 Tembesi, Sagulung, Batam. &#13;&#10;3. PO Konfirmasi harus dikirim kembali dalam waktu 3 hari kerja setelah PO dikirimkan. apabila tidak ada konfirmasi maka ... dianggap telah menyetujui semua &#13;&#10;4. Jangka waktu pengiriman terhitung sejak PO konfirmasi dari  ... . &#13;&#10;5. Pengurusan dokumen endorsemen PPFTZ 03 apabila di perlukan berikut biaya-biaya yang timbul menjadi tanggung jawab  .... &#13;&#10;6. Denda keterlambatan akan dikenakan sebesar  ... % per hari keterlambatan apabila barang diterima melebihi waktu yang telah di sepakati kedua belah pihak ( merefer  point a. syarat dan ketentuan dalam PO ini).&#13;&#10;7. ... akan memberikan Certificate of Origin dan Certificate of Manufacture.&#13;&#10;8. Garansi selama 24 bulan setelah barang di terima MEB atau 12 bulan setelah pemasangan di unit MEB.&#13;&#10;9. Pemasok wajib mematuhi semua peraturan yang berlaku di lingkungan MEB berikut peraturan mengenai K3L.<?php } else { echo $term[0]["lain_lain"]; } ?></textarea>
                    </div>  
                  </div>
                </li>

              </ol>
                
            </div>


                  </div>
                </div>


            </div>
          </div>

          <div class="col-md-12">
           <div class="my-3 p-3 bg-white rounded shadow-sm">
                <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
                  <div class="container-fluid">
                    
                    <button type="submit" name='submitBtn' id='save_term' value='save_term' class="btn btn-info" title="Submit PO Number"><i class="fa fa-check"></i> Submit</button>
                    

                  </div>
               </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mr_list['mrs_no']; ?>/"+param;
      }
    })

  }

  function delete_data(param,mr,po) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>po/delete_data/"+param+"/"+mr+"/"+po;
      }
    })

  }

</script>

<script type="text/javascript">

   $('input[name="cb_add_mr[]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[]"]');
    var id_data = $(this).closest('td').find('input[name="id[]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1); 

      $('input[id="price_per_unit_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="price_per_unit_'+id_data.val()+'"]').prop('required',true);

      $('input[id="total_amount_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="total_amount_'+id_data.val()+'"]').prop('required',true);
      
      $('select[id="currency_'+id_data.val()+'"]').removeAttr('disabled');
      $('select[id="currency_'+id_data.val()+'"]').prop('required',true);

      $('input[id="remarks_po_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="remarks_po_'+id_data.val()+'"]').prop('required',true);    
    }

    else if($(this).prop("checked") == false){
      $(id_array).val(0);   

       $('input[id="price_per_unit_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="total_amount_'+id_data.val()+'"]').attr('disabled', true);
      $('select[id="currency_'+id_data.val()+'"]').attr('disabled', true);
       $('input[id="remarks_po_'+id_data.val()+'"]').attr('disabled', true);  
    }
   
    
  });


    function calculate_total_amount(input) {

    var row   = $(input).closest('tr');
    var po_qty   = $(row).find('input[name="req_qty[]"]').val();
    var data_ppu   = $(row).find('input[name="price_per_unit[]"]').val();

    var total_price = Number(data_ppu) * Number(po_qty);

    console.log(total_price);

    $(row).find('input[name="total_amount[]"]').val(total_price);

  }

  function count_grand_total_diskon(input) {

    var total   = $('input[name="total_all_amount"]').val();
    var diskon  = input;
    var ppn     = $('input[name="ppn"]').val();

    var grand_total = (Number(total) - Number(diskon)) + Number(ppn);
    
    $('input[name="grand_total"]').val(grand_total);

  }

  function count_grand_total_ppn(input) {

    var total   = $('input[name="total_all_amount"]').val();
    var diskon  = $('input[name="diskon"]').val();
    var ppn     = input;

    var grand_total = (Number(total) - Number(diskon)) + Number(ppn);
    
    $('input[name="grand_total"]').val(grand_total);

  }


  function _alert(){    
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
     }).then((result) => {
      if(result.value){
          $("#form-mr").submit();
      }
    });
  }

  $(document).ready(function() {
    count_grand_total_ppn("0");
  });

  </script>

  <script type="text/javascript">

    $('#mr_detail').DataTable({
      "language": { 
      "infoFiltered": "" },
      "paging": false,
      "order": [],    
    });

  </script>