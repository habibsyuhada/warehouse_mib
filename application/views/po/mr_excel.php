<?php 

  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=PO_Report-".date('YmdHis').".xls");
  header("Pragma: no-cache");
  header("Expires: 0");

  $mr_list       = $mr_list[0];
  $year          = date("Y",strtotime($mr_list["created_date"]));
  $created_by    = (isset($user_data[$mr_list['created_by']]) ? $user_data[$mr_list['created_by']] : '-');
  $created_date  = $mr_list["created_date"];
 

?>

<!DOCTYPE html>
<html><head>
  <title>MR Number :  MR-<?php echo $year; ?>-<?php echo $mr_list['mr_number']; ?></title>
  
</head><body>  

  <table  border="1">
       <thead><tr>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">MR REFERENCE</th>
                <th bgcolor="#ff3700" style="color: white !important; text-align: center;">PO NUMBER</th>
                <th bgcolor="#ff3700" style="color: white !important; text-align: center;">PO REMARKS</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">DESCRIPTION</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">QTY</th>
                <th bgcolor="#008060" style="color: white !important; text-align: center;">UOM</th>                   
                <th bgcolor="#008060" style="color: white !important; text-align: center;">PRICE PER UNIT</th>                   
                <th bgcolor="#008060" style="color: white !important; text-align: center;">TOTAL AMOUNT</th>                   
                <th bgcolor="#008060" style="color: white !important; text-align: center;">CURRENCY</th>                   
                <th bgcolor="#008060" style="color: white !important; text-align: center;">VENDOR NAME</th>                
                <th bgcolor="#008060" style="color: white !important; text-align: center;">ETA DATE</th>                
              </tr></thead>
        <tbody><?php $no=1; foreach ($mr_list_detail as $key) { ?>
        <tr>
          <td><?php echo "MR-".date("Y",strtotime($key["timestamp"]))."-".$key['mr_number']; ?></td>
          <td><?php echo $key['po_number']; ?></td>
          <td><?php echo $key['remarks_po']; ?></td>
          <td>
            <center>
                  <b>
                    <?php echo $key['tec_spec'];; ?>
                  </b>                           
            </center>
          </td>         
          <td><?php echo $key['qty_req']; ?></td>
          <td><?php echo $uom[$key['uom_req']]; ?></td>         
          <td><?php echo $key['price_per_unit']; ?></td>
          <td><?php echo $key['total_amount']; ?></td>
          <td><?php echo $cur[$key['id_cur']]; ?></td>
          <td><?php echo $key['name']; ?></td>
          <td><?php echo $key['eta_date']; ?></td>
        </tr><?php $no++;} ?></tbody>  
  </table>

</body></html>