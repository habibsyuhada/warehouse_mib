<div id="content" class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <?php echo $this->session->flashdata('message');?>
            
            <table class="table table-hover text-center" id='mr_list_data'>
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>Budget Category</th>
                  <th>Department</th>
                  <th>Material Requisition No.</th>                  
                  <th>Vendor Selected</th>                  
                                                     
                  <th>Action</th>                                                                 
                </tr>
              </thead>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">   
  $('#mr_list_data').DataTable({
    "language": { 
      "infoFiltered": "" },
      "paging": true,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
          "url": "<?php echo base_url();?>po/mr_list_json/<?php echo $status; ?>/<?php echo $type_req; ?>",
          "type": "POST",
      },
      "columnDefs": [{
        "targets": [0],
        "orderable": true,
      }, ],
  });
</script>