
<?php 
  $mr_list = $mr_list[0];
  $mr_list_detail_sat = $mr_list_detail[0];

  //print_r($mr_list_detail_sat);
?>



<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" id='form-mr' action="<?php echo base_url();?>po/process_update_po" enctype="multipart/form-data">

    <input type="hidden" class="form-control" name="po_number" id="po_number"  value='<?php echo $po_nos; ?>' readonly>  
    <input type="hidden" class="form-control" name="mr_numberx" id="mr_numberx"  value='<?php echo $mr_list['mr_number']; ?>' readonly>  
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                      
                      <?php if($mr_list["request_type"] == "Service Request"){ ?>
                        <label>Service Order No.</label>
                        <input type="text" class="form-control" name="created_by" id="created_by"  value='<?php echo $mr_list_detail[0]["po_number"] ?>' readonly> 
                       <?php } else if($mr_list["request_type"] == "Material Request"){ ?>
                        <label>Purchase Order No.</label>
                         <input type="text" class="form-control" name="created_by" id="created_by"  value='<?php echo $mr_list_detail[0]["po_number"] ?>' readonly> 
                       <?php } ?>
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    <label>Quotation</label>
                       <input type="text" class="form-control" name="vendor" id="vendor"  value='<?php echo $data_vendor[$mr_list_detail_sat['vendor_winner']] ?>' readonly>  
                      
                  </div>
                </div>                
              </div>

              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                       <?php if($mr_list["request_type"] == "Service Request"){ ?>
                        <label>Service Order Date</label>
                       
                       <?php } else if($mr_list["request_type"] == "Material Request"){ ?>
                        <label>Purchase Order Date</label>
                         
                       <?php } ?>
                        <input type="text" class="form-control" name="created_by" id="created_by"  value='<?php echo date("Y-m-d",strtotime($mr_list_detail_sat['created_date_po'])); ?>' readonly> 
                      
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                   <label>Quotation Reference No</label>
                       <input type="text" class="form-control" name="reference_no" id="reference_no"  value='<?php echo  $mr_list_detail_sat["reference_no"] ?>' readonly>  
                      
                  </div>
                </div>                
              </div>
              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                      <label>Alamat</label>
                        <textarea rows="4"  type="text" class="form-control" name="created_by" id="created_by" readonly><?php echo $mr_list_detail_sat["name"];  ?>&#13;&#10;<?php echo $mr_list_detail_sat["address"];  ?>&#13;&#10;Telp. <?php if(!empty($mr_list_detail_sat["phone_no"])){ echo $mr_list_detail_sat["phone_no"]; } else { echo "-"; }  ?>&#13;&#10;Email : <?php echo $mr_list_detail_sat["email"];  ?></textarea>
                      
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    
                      
                  </div>
                </div>                
              </div>
          
           <div class="col-md-12">
                    <table class="table text-muted text-center" id='mr_detail'>
                       <thead class="bg-green-smoe text-white">
                        <tr>
                         
                          <th>MR NO</th>                                                
                          <th>DESCRIPTION</th>
                          <th>QTY</th>
                          <th>UOM</th> 
                          <th>PRICE PER UNIT</th>
                          <th>TOTAL AMOUNT</th>
                          <th>CURRENCY</th>                           
                       
                          <th style="width: 200px;overflow: hidden;">PO REMARKS</th>
                          
                        </tr>                        
                      </thead>
                      <tbody>

                      <?php $no=1; foreach($mr_list_detail as $key) { ?>

                        <tr>

                         

                          <td>
                              <?php echo $key['mr_number']; ?>
                          </td>
                          
                          <td>
                              <?php echo $key['tec_spec']; ?>
                          </td>

                         
                          <td>
                            <?php echo $key['qty_req']; ?>                                                       
                          </td>

                          <td>
                             <?php echo $uom[$key['uom_req']]; ?>                                                         
                          </td>

                          <td>
                            <?php echo number_format($key['price_per_unit']); ?>                                                       
                          </td>


                          <td>
                            <?php echo number_format($key['total_amount']); ?>                                                       
                          </td>


                          <td>
                            <?php echo $cur[$key['id_cur']]; ?>                                                       
                          </td>

                          
                       
                          <td>
                            <?php echo $key['remarks_po'] ?>
                          </td>

                         
                        </tr>
                      <?php $no++; } ?>
                        
                      </tbody>
                      <tfoot class="bg-green-smoe text-white">
                        <tr>
                          <th colspan="6">TOTAL AMOUNT</th>
                          <th>IDR <?php echo $mr_list_detail_sat['total_all_amount']; ?></th>
                          <th colspan="5">#</th>
                        </tr> 
                         <tr>
                          <th colspan="6">DISKON</th>
                          <th>IDR <?php echo $mr_list_detail_sat['diskon']; ?></th>
                          <th colspan="5">#</th>
                        </tr> 
                         <tr>
                          <th colspan="6">PPN</th>
                          <th>IDR <?php echo $mr_list_detail_sat['ppn']; ?></th>
                          <th colspan="5">#</th>
                        </tr>
                         <tr>
                          <th colspan="6">GRANT TOTAL</th>
                          <th>IDR <?php echo $mr_list_detail_sat['grand_total']; ?></th>
                          <th colspan="5">#</th>
                        </tr>                        
                      </tfoot>
                    </table>

                    <br><br>
              <label>Syarat dan Ketentuan / <i>Term and Condition</i></label>
              
              <ol type="a">
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kepada</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name='kepada_to' placeholder="Penerima PO" required="" value="<?php echo $term[0]["kepada_to"] ?>">
                    </div>  
                  </div>
                </li>
                <li>
                  <div class="form-group row">
                    <?PHP if($mr_list["service_request"] == 1){ ?>
                      <label class="col-sm-2 col-form-label">Lokasi Kerja</label>
                      <?php $var_so_po = "Lokasi Kerja"; ?>
                    <?PHP } else { ?>
                    <label class="col-sm-2 col-form-label">Waktu Pengiriman</label>
                     <?php $var_so_po = "Waktu Pengiriman"; ?>
                    <?PHP }  ?>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name='jadwal_pekerjaan' placeholder="<?php echo  $var_so_po; ?>" value="<?php echo $term[0]["jadwal_pekerjaan"] ?>">
                    </div>  
                  </div>
                </li>
                
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kondisi Pengiriman</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name='lokasi_kerja' value="<?php echo $term[0]["lokasi_kerja"] ?>" placeholder="Kondisi Pengiriman">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lokasi Pengiriman</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" rows="3" name='lokasi_pengiriman'><?php if(empty($term[0]["lokasi_pengiriman"])){ ?>PT. Mitra Energi Batam "MEB"&#13;&#10;JL. Lintas Gas Negara, Trans-Barelang, KM. 3,5 Tembesi, Sagulung, Batam, Kep. Riau-INDONESIA<?php } else { echo $term[0]["lokasi_pengiriman"]; } ?></textarea>
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pembayaran</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control"  name='pembayaran' value="<?php if(empty($term[0]["pembayaran"])){ ?>14 Hari setelah hari setelah barang dan dokumen penagihan di terima oleh MEB.<?php } else { echo $term[0]["pembayaran"]; } ?>">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lain-lain</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" rows="12" name='lain_lain'><?php if(empty($term[0]["pembayaran"])){ ?>1. Semua pembayaran akan dilakukan setelah MEB menerima semua dokumen pendukung (invoice, PO konfirmasi dan dokumen pendukung lainnya). &#13;&#10;2. Dokumen penagihan dikirimkan ke MEB dengan alamat: Jl. Lintas Gas Negara, Trans Barelang, KM. 3,5 Tembesi, Sagulung, Batam. &#13;&#10;3. PO Konfirmasi harus dikirim kembali dalam waktu 3 hari kerja setelah PO dikirimkan. apabila tidak ada konfirmasi maka ... dianggap telah menyetujui semua &#13;&#10;4. Jangka waktu pengiriman terhitung sejak PO konfirmasi dari  ... . &#13;&#10;5. Pengurusan dokumen endorsemen PPFTZ 03 apabila di perlukan berikut biaya-biaya yang timbul menjadi tanggung jawab  .... &#13;&#10;6. Denda keterlambatan akan dikenakan sebesar  ... % per hari keterlambatan apabila barang diterima melebihi waktu yang telah di sepakati kedua belah pihak ( merefer  point a. syarat dan ketentuan dalam PO ini).&#13;&#10;7. ... akan memberikan Certificate of Origin dan Certificate of Manufacture.&#13;&#10;8. Garansi selama 24 bulan setelah barang di terima MEB atau 12 bulan setelah pemasangan di unit MEB.&#13;&#10;9. Pemasok wajib mematuhi semua peraturan yang berlaku di lingkungan MEB berikut peraturan mengenai K3L.<?php } else { echo $term[0]["lain_lain"]; } ?></textarea>
                    </div>  
                  </div>
                </li>

              </ol>
            </div>


                  </div>
                </div>


            </div>
          </div>

          <div class="col-md-12">
           <div class="my-3 p-3 bg-white rounded shadow-sm">
              <h6 class="pb-2 mb-0">Approval & Report</h6>
                <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
                  <div class="container-fluid">

                    <?php

                      if($mr_list_detail_sat['grand_total'] <= 50000000 ){
                        $title_of_sign = "Manager";
                      } else if($mr_list_detail_sat['grand_total'] > 50000000 AND $mr_list_detail_sat['grand_total'] <= 100000000){
                        $title_of_sign = "General Manager (GM)";
                      } else if($mr_list_detail_sat['grand_total'] > 100000000 AND $mr_list_detail_sat['grand_total'] <= 300000000){
                        $title_of_sign = "Direktur Oprational / Direktur Finance"; 
                      } else if($mr_list_detail_sat['grand_total'] > 300000000 AND $mr_list_detail_sat['grand_total'] <= 700000000){
                        $title_of_sign = "Direktur Utama";
                      } else if($mr_list_detail_sat['grand_total'] > 700000000){
                        $title_of_sign = "Direktur Utama Approvement MPI";       
                      }

                     ?>

                      <?php
                        $get_user_manager    = $this->mr_mod->get_user_data_sign($mr_list_detail_sat['manager_approval_by']);
                        $get_bod             = $this->mr_mod->get_user_data_sign($mr_list_detail_sat['bod_approval_by']);
                        $get_procurement     = $this->mr_mod->get_user_data_sign($mr_list_detail_sat['created_by_po']);
                        $get_user_current    = $this->mr_mod->get_user_data_sign($read_cookies[0]);


                      ?>

                     <table class="table" width="100%">
                      <tr>
                          <td> <b><?php echo $title_of_sign; ?></b> </td>
                          <td> <b>Boat Of Director (BOD)</b> </td>
                          <td> <b>Procurement</b> </td>
                          <td> <b>EXPORT</b> </td>    
                      </tr>
                      <tr>
                          <td>    
                          <?php if($mr_list_detail_sat['manager_approval_status'] != "0"){ ?> 

                            <img src="data:image/png;base64, <?php echo $get_user_manager[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' /><br/><?php echo $get_user_manager[0]["full_name"]; ?>

                          <?php } else { ?>

                            <?php if($read_cookies[7] == 6){ ?>

                            <button type="submit" name='submitBtn' id='approve_po_manager' value='approve_po_manager' class="btn btn-success" title="Approve PO"><i class="fa fa-check"></i> Approve</button>

                            <button type="submit" name='submitBtn' id='reject_po_manager' value='reject_po_manager' class="btn btn-danger" title="Reject PO"><i class="fa fa-check"></i> Reject</button>

                           <?php } else { ?>

                              Waiting Approval <?php echo $title_of_sign ?>

                           <?php }?>

                          <?php }  ?> 
                          </td>
                          <td>   
  
                            <?php if($mr_list_detail_sat['bod_approval_status'] != "0"){ ?> 

                            <img src="data:image/png;base64, <?php echo $get_bod[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' /><br/><?php echo $get_bod[0]["full_name"]; ?>

                          <?php } else { ?>

                            <?php if($read_cookies[7] == 7){ ?>

                            <button type="submit" name='submitBtn' id='approve_po_bod' value='approve_po_bod' class="btn btn-success" title="Approve PO"><i class="fa fa-check"></i> Approve</button>

                            <button type="submit" name='submitBtn' id='reject_po_bod' value='reject_po_bod' class="btn btn-danger" title="Reject PO"><i class="fa fa-check"></i> Reject</button>

                            <?php } else { ?>
                              Waiting Approval<br/>Boat Of Director (BOD)
                           <?php }?>

                          <?php }  ?> 
                          </td>
                          <td>            
                          
                            <img src="data:image/png;base64, <?php echo $get_procurement[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' /><br/><?php echo $get_procurement[0]["full_name"]; ?>

                          </td>
                          <td>
                           <?php  if($mr_list_detail_sat['status_approval'] == 3){ ?>
                                  <a target='_blank' href="<?php echo base_url(); ?>po/mr_excel_2/<?php echo strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~'); ?>/<?php echo strtr($this->encryption->encrypt($po_nos), '+=/', '.-~'); ?>" class="btn btn-success" title="Export Excel"><i class="fas fa-file-excel"></i> Excel</a>              

                                  <a target='_blank' href="<?php echo base_url();?>po/po_pdf_format/<?php echo strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~'); ?>/<?php echo strtr($this->encryption->encrypt($mr_list_detail_sat['po_number']), '+=/', '.-~'); ?>" class="btn btn-danger " title="Submit"><i class="fas fa-file-pdf"></i> PDF</a>
                          <?php } ?>
                          </td>
                      </tr>
                      <tr>
                          <td> 
                            <?php if($mr_list_detail_sat['manager_approval_status'] == 3){ ?>
                                Date : <?php echo date("Y-m-d",strtotime($mr_list_detail_sat["manager_approval_date"])) ?>
                            <?php } ?>
                            </td>
                          <td>
                             <?php if($mr_list_detail_sat['bod_approval_status'] == 3){ ?>
                                Date : <?php echo date("Y-m-d",strtotime($mr_list_detail_sat["bod_approval_date"])) ?>
                             <?php } ?>
                          </td>
                          <td>Date : <?php echo date("Y-m-d",strtotime($mr_list_detail_sat["created_date_po"])) ?></td>
                          <td>&nbsp;</td>
                      </tr>
                      </table>

                   

                  <div class="text-right mt-3">
                    <a href="<?php echo base_url();?>po/po_list" class="btn btn-secondary " title="Cancel"><i class="fas fa-arrow-circle-left"></i>&nbsp; Back</a>

                    

                  </div>
               </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mr_list['mrs_no']; ?>/"+param;
      }
    })

  }

  function delete_data(param,mr,po) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>po/delete_data/"+param+"/"+mr+"/"+po;
      }
    })

  }

</script>

<script type="text/javascript">


  $('#selectall_omission').click(function (e) {

    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);

    if($(this).prop("checked") == true){

      $('button[name="btnsubmit_omission"]').removeAttr('disabled');

      $('input[id="id_checkbox"]').val(1);
      $('input[id="po_number"]').removeAttr('disabled');
      $('input[id="remarks_po"]').removeAttr('disabled');
      $('select[id="vendor"]').removeAttr('disabled');
      $('input[id="eta_date"]').removeAttr('disabled');
      $('input[id="etd_date"]').removeAttr('disabled');
      
    } else if($(this).prop("checked") == false){

      $('button[name="btnsubmit_omission"]').attr('disabled', true);

      $('input[id="id_checkbox"]').val(0);
      $('input[id="po_number"]').attr('disabled', true);
      $('input[id="remarks_po"]').attr('disabled', true);
      $('select[id="vendor"]').attr('disabled', true);
      $('input[id="eta_date"]').attr('disabled', true);
      $('input[id="etd_date"]').attr('disabled', true);
     
    }

  });


<?php for($i=1;$i<=$src_total_mr_detail;$i++){ ?>

  $('input[name="cb_add_mr[<?php echo $i; ?>]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[<?php echo $i; ?>]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1);
    }

    else if($(this).prop("checked") == false){
      $(id_array).val(0);
    }

    if($('input[name="cb_add_mr[<?php echo $i; ?>]"]:checked').length > 0){

      $('button[name="btnsubmit_omission"]').removeAttr('disabled');
      
      $('input[name="po_number[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="remarks_po[<?php echo $i; ?>]"]').removeAttr('disabled');

      $('select[name="vendor[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="eta_date[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="etd_date[<?php echo $i; ?>]"]').removeAttr('disabled');
      

    } else {

      $('button[name="btnsubmit_omission"]').attr('disabled', true);

      $('input[name="po_number[<?php echo $i; ?>]"]').attr('disabled', true);     
      $('input[name="remarks_po[<?php echo $i; ?>]"]').attr('disabled', true); 

      $('select[name="vendor[<?php echo $i; ?>]"]').attr('disabled', true); 
      $('input[name="eta_date[<?php echo $i; ?>]"]').attr('disabled', true); 
      $('input[name="etd_date[<?php echo $i; ?>]"]').attr('disabled', true); 

    }
    
  });

 <?php } ?> 

  function _alert(){
    
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
     }).then((result) => {
      if(result.value){
          $("#form-mr").submit();
      }
    });
    
  }

  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  });

  </script>

  <script type="text/javascript">

    $('#mr_detail').DataTable({
      "language": { 
      "infoFiltered": "" },
      "paging": false,
      "order": [],    
    });

  </script>