
<?php 
  $mr_list = $mr_list[0];
?>

<div id="content" class="container-fluid" style="overflow: auto;">
  <form method="POST" id='form-mr' action="<?php echo base_url();?>po/process_update_po" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?> </h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col-md">
                    <div class="form-group">
                      <label>PO Created By</label>
                        <input type="text" class="form-control" name="created_by" id="created_by"  value='<?php echo $user_data[$mr_list_detail[0]['created_by_po']] ?>' readonly> 
                      
                    </div>
                </div>
                  
                <div class="col-md">
                  <div class="form-group">
                    <label>PO Created Date</label>
                       <input type="text" class="form-control" name="created_date" id="created_date_po"  value='<?php echo $mr_list_detail[0]['created_date_po'] ?>' readonly>  
                      
                  </div>
                </div>                
              </div>
          
           <div class="col-md-12">
                    <table class="table text-muted text-center" id='mr_detail'>
                       <thead class="bg-green-smoe text-white">
                        <tr>
                          <th>PO NO</th>
                          <th>MR NO</th>                                                
                          <th>DESCRIPTION</th>
                          <th>PO QTY</th>
                          <th>UOM</th> 
                          <th>PRICE PER UNIT</th>
                          <th>TOTAL AMOUNT</th>
                          <th>CURRENCY</th>                           
                          <th>VENDOR</th>
                          <th>ETA DATE</th>
                          <th style="width: 200px;overflow: hidden;">PO REMARKS</th>
                          <th>ACTION</th>
                        </tr>                        
                      </thead>
                      <tbody>

                      <?php $no=1; foreach($mr_list_detail as $key) { ?>

                        <tr>

                          <td>
                            <b> <?php echo $key['po_number'] ?> </b>                                                         
                             <input type="hidden" id='mr_number' name="mr_number[<?php echo $no; ?>]" value='<?php echo $mr_list['mr_number'] ?>'>             
                          </td>

                          <td>
                              <?php echo $key['mr_number']; ?>
                          </td>
                          
                          <td>
                              <?php echo $key['tec_spec']; ?>
                          </td>

                         
                          <td>
                            <?php echo $key['qty_req']; ?>                                                       
                          </td>

                          <td>
                             <?php echo $uom[$key['uom_req']]; ?>                                                         
                          </td>

                          <td>
                            <?php echo $key['price_per_unit']; ?>                                                       
                          </td>


                          <td>
                            <?php echo $key['total_amount']; ?>                                                       
                          </td>


                          <td>
                            <?php echo $cur[$key['id_cur']]; ?>                                                       
                          </td>

                          
                         
                          <td>
                           <?php echo (isset($data_vendor[$key['vendor']]) ? $data_vendor[$key['vendor']] : '-'); ?>
                          </td>

                          <td>
                            <?php echo  $key['eta_date']; ?>
                          </td>

                          <td>
                            <?php echo $key['remarks_po'] ?>
                          </td>

                          <td>    

                              <?php if($read_permission[26] == 1){ ?>

                                <?php if($read_cookies[0] == $key['created_by_po']){ ?>                      
                                  <a href="#" class='btn btn-danger' onclick="delete_data('<?php echo $key['id_po']; ?>','<?php echo $mr_list['mr_number']; ?>','<?php echo $key['po_number'] ?>')"><i class="fas fa-trash-alt"></i></a>
                                <?php } ?>

                              <?php } ?>

                          </td>
                         
                        </tr>
                      <?php $no++;} ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-left mt-3">

            <a href="<?php echo base_url();?>po/po_list" class="btn btn-secondary " title="Cancel"><i class="fas fa-arrow-circle-left"></i>&nbsp; Back</a>

            <?php if($read_permission[27] == 1){ ?>
                        
            <a target='_blank' href="<?php echo base_url(); ?>po/mr_excel_2/<?php echo strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~'); ?>/<?php echo strtr($this->encryption->encrypt($po_nos), '+=/', '.-~'); ?>" class="btn btn-success" title="Export Excel"><i class="fas fa-file-excel"></i> Excel</a>
          
            <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mr_list['mrs_no']; ?>/"+param;
      }
    })

  }

  function delete_data(param,mr,po) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if(result.value) {
        window.location.href = "<?php echo base_url();?>po/delete_data/"+param+"/"+mr+"/"+po;
      }
    })

  }

</script>

<script type="text/javascript">


  $('#selectall_omission').click(function (e) {

    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);

    if($(this).prop("checked") == true){

      $('button[name="btnsubmit_omission"]').removeAttr('disabled');

      $('input[id="id_checkbox"]').val(1);
      $('input[id="po_number"]').removeAttr('disabled');
      $('input[id="remarks_po"]').removeAttr('disabled');
      $('select[id="vendor"]').removeAttr('disabled');
      $('input[id="eta_date"]').removeAttr('disabled');
      $('input[id="etd_date"]').removeAttr('disabled');
      
    } else if($(this).prop("checked") == false){

      $('button[name="btnsubmit_omission"]').attr('disabled', true);

      $('input[id="id_checkbox"]').val(0);
      $('input[id="po_number"]').attr('disabled', true);
      $('input[id="remarks_po"]').attr('disabled', true);
      $('select[id="vendor"]').attr('disabled', true);
      $('input[id="eta_date"]').attr('disabled', true);
      $('input[id="etd_date"]').attr('disabled', true);
     
    }

  });


<?php for($i=1;$i<=$src_total_mr_detail;$i++){ ?>

  $('input[name="cb_add_mr[<?php echo $i; ?>]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[<?php echo $i; ?>]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1);
    }

    else if($(this).prop("checked") == false){
      $(id_array).val(0);
    }

    if($('input[name="cb_add_mr[<?php echo $i; ?>]"]:checked').length > 0){

      $('button[name="btnsubmit_omission"]').removeAttr('disabled');
      
      $('input[name="po_number[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="remarks_po[<?php echo $i; ?>]"]').removeAttr('disabled');

      $('select[name="vendor[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="eta_date[<?php echo $i; ?>]"]').removeAttr('disabled');
      $('input[name="etd_date[<?php echo $i; ?>]"]').removeAttr('disabled');
      

    } else {

      $('button[name="btnsubmit_omission"]').attr('disabled', true);

      $('input[name="po_number[<?php echo $i; ?>]"]').attr('disabled', true);     
      $('input[name="remarks_po[<?php echo $i; ?>]"]').attr('disabled', true); 

      $('select[name="vendor[<?php echo $i; ?>]"]').attr('disabled', true); 
      $('input[name="eta_date[<?php echo $i; ?>]"]').attr('disabled', true); 
      $('input[name="etd_date[<?php echo $i; ?>]"]').attr('disabled', true); 

    }
    
  });

 <?php } ?> 

  function _alert(){
    
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
     }).then((result) => {
      if(result.value){
          $("#form-mr").submit();
      }
    });
    
  }

  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  });

  </script>

  <script type="text/javascript">

    $('#mr_detail').DataTable({
      "language": { 
      "infoFiltered": "" },
      "paging": false,
      "order": [],    
    });

  </script>