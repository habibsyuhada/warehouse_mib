<?php 
  $mr_list            = $mr_list[0];
  $year          = date("Y",strtotime($mr_list["created_date"]));

  $mr_list_detail_sat = $mr_list_detail[0];
?>
<!DOCTYPE html>
<html><head>
  <title>MR Number :  MR-<?php echo $year; ?>-<?php echo $mr_list['mr_number']; ?></title>
  <style type="text/css">
   
    @page {
      margin: 0cm 0cm;
    }

    body {
      top: 0cm;
      left: 0cm;
      right: 0cm;
      margin-top: 3cm;
      margin-left: 1.4cm;
      margin-right: 1.5cm;
      margin-bottom: 3cm;
      font-family: "helvetica";
      font-size: 12px !important;
    }

    header {
      position: fixed;
      top: 0cm;
      left: 0cm;
      right: 0cm;
      height: 5cm;
      padding-top: 1px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    
    }

    footer {
      position: fixed;
      bottom: 1cm;
      left: 0cm;
      right: 0cm;
      height: 3cm;
      padding-bottom: 2.5px;
      padding-left: 1.4cm;
      padding-right: 1.5cm;
    }


    .titleHead {
      border:1px #000 solid;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      background-color: #a6ffa6;
      font-weight: bold;
     
    }

    .titleHeadMain {
      text-align: center;
      border-collapse: collapse;
      text-align: center;
      vertical-align: middle;
      font-size: 25px;
      font-weight: bold;
    }

    table.table td {
      font-size: 90%;
      border:1px #000 solid;
      font-weight: bold;
      max-width: 150px;
      word-wrap: break-word;
    }

    table>thead>tr>td,table>tbody>tr>td{
      vertical-align: top;
    }

    .br_break{
      line-height: 15px;
    }

    .br_break_no_bold{
      line-height: 18px;
    }

    .br{
      border-right: 1px #000 solid;
    }
    .bl{
      border-left: 1px #000 solid;
    }
    .bt{
      border-top: 1px #000 solid;
    }
    .bb{
      border-bottom:  1px #000 solid;
    }
    .bx{
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .by{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
    }
    .ball{
      border-top: 1px #000 solid;
      border-bottom: 1px #000 solid;
      border-left: 1px #000 solid;
      border-right: 1px #000 solid;
    }
    .tab{
      display: inline-block; 
      width: 100px;
    }
    .tab2{
      display: inline-block; 
      width: 100px;
    }

    table.table-body td {
      font-size: 70%;
      border:1px #000 solid;
      max-width: 150px;
      word-wrap: break-word;
    }

    table.table-body th {
      font-size: 90%;
      border:1px #000 solid;
    }

    table.table-body tr {
      font-size: 90%;
      border:1px #000 solid;
    }

  </style>

  <style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size: 11px !important;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>

  <style>
#customers2 {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
  font-size: 11px !important;
  width: 300px;
}

#customers2 td, #customers2 th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers2 tr:nth-child(even){background-color: #f2f2f2;}

#customers2 tr:hover {background-color: #ddd;}

#customers2 th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head><body>
  <header>
   
    <table width="100%">
      <tr>
        <td>
           <img src="img/logo_MEB3.png" style="width: 50px; padding-top: 25px;">
           <img src="img/logo_ompi.png" style="width: 50px; height: 70px; padding-top: 25px;">
        </td>
        <td width="300px"><br/>
          <u><h1 style="font-size: 20px !important;"><?php if($mr_list["request_type"] == "Service Request"){ echo "SERVICE ORDER"; } else { echo "PURCHASE ORDER"; } ?></h1></u>
        </td>
        <td><br/><br/>
         <table>
           <tr>
              <td>No. Form</td>
              <td>:</td>
              <td>FM-PROC-05</td>
           </tr>
           <tr>
              <td>Revisi</td>
              <td>:</td>
              <td>0</td>
           </tr>
           <tr>
              <td>Tanggal</td>
              <td>:</td>
              <td>1 Feb 2013</td>
           </tr>
         </table>
        </td>
      </tr>     
    </table>
    
  </header>
  <table width="100%">
     <tr>
        <td width="60%">
         <table width="100%">
           <tr>
              <td><?php if($mr_list["request_type"] == "Service Request"){ echo "Service Order Number"; } else { echo "Purchase Order Number"; } ?></td>
              <td>:</td>
              <td><?php echo $mr_list_detail[0]["po_number"] ?></td>
            </tr>
            <tr>
             <td>Tanggal</td>
              <td>:</td>
              <td><?php echo date("Y-m-d",strtotime($mr_list_detail_sat['created_date_po'])); ?></td>
            </tr>
            <tr>
             <td>Alamat</td>
              <td>:</td>
              <td><?php echo $mr_list_detail_sat["name"];  ?><br/><?php echo $mr_list_detail_sat["address"];  ?><br/>Telp. <?php if(!empty($mr_list_detail_sat["phone_no"])){ echo $mr_list_detail_sat["phone_no"]; } else { echo "-"; }  ?><br/>Email : <?php echo $mr_list_detail_sat["email"];  ?></td>
              
            </tr>
            <tr>
              <td>Kepada</td>
              <td>:</td>
              <td><?php echo $term[0]["kepada_to"] ?></td>
            </tr>
         </table>
        </td>
        <td width="40%">
          <table width="100%">
           <tr>
              <td>Quotation</td>
              <td>:</td>
              <td><?php echo $data_vendor[$mr_list_detail_sat['vendor_winner']] ?></td>
            </tr>
             <tr>
              <td>Date</td>
              <td>:</td>
              <td><?php echo date("Y-m-d",strtotime($mr_list_detail_sat["timestamp_quo"])); ?></td>
            </tr>
            <tr>
              <td>Reference</td>
              <td>:</td>
              <td><?php echo  $mr_list_detail_sat["reference_no"] ?></td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
            </tr>
         </table>
        </td>
      </tr> 
      
  </table>
  <br/>
  <table width="100%" id='customers'><tr><th>DESC</th><th>QTY</th><th>UOM</th><th>PRICE PER UNIT</th><th>TOTAL AMOUNT</th><th>CURRENCY</th><th style="width: 200px;overflow: hidden;">PO REMARKS</th></tr><?php $no=1; foreach($mr_list_detail as $key) { ?><tr><td><?php echo $key['tec_spec']; ?></td><td><?php echo $key['qty_req']; ?></td><td><?php echo $uom[$key['uom_req']]; ?></td><td><?php echo number_format($key['price_per_unit']); ?></td><td><?php echo number_format($key['total_amount']); ?></td><td><?php echo $cur[$key['id_cur']]; ?></td><td><?php echo $key['remarks_po'] ?></td></tr><?php $no++; } ?>
      <tr><td colspan="4">TOTAL </td><td><?php echo number_format($mr_list_detail_sat['total_all_amount']); ?></td><td colspan="2">IDR</td></tr>
      <tr><td colspan="4">DISKON</td><td><?php echo number_format($mr_list_detail_sat['diskon']); ?></td><td colspan="2">IDR</td></tr>
      <tr><td colspan="4">PPN</td><td><?php echo number_format($mr_list_detail_sat['ppn']); ?></td><td colspan="2">IDR</td></tr>
      <tr><td colspan="4">GRAND TOTAL</td><td><?php echo number_format($mr_list_detail_sat['grand_total']); ?></td><td colspan="2">IDR</td></tr>
    </table>

  <br/>

  <ol type="a">
             
                <li>
                  <div class="form-group row">
                    <?PHP if($mr_list["request_type"] == "Service Request"){ ?>
                      <label class="col-sm-2 col-form-label">Lokasi Kerja</label>
                      <?php $var_so_po = "Lokasi Kerja"; ?>
                    <?PHP } else { ?>
                    <label class="col-sm-2 col-form-label">Waktu Pengiriman</label>
                     <?php $var_so_po = "Waktu Pengiriman"; ?>
                    <?PHP }  ?>
                    <div class="col-sm-10">
                      <?php echo $term[0]["jadwal_pekerjaan"] ?>
                    </div>  
                  </div>
                </li>
                
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kondisi Pengiriman</label>
                    <div class="col-sm-10">
                      <?php echo $term[0]["lokasi_kerja"] ?>
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lokasi Pengiriman</label>
                    <div class="col-sm-10">
                      <?php  echo $term[0]["lokasi_pengiriman"]; ?>
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pembayaran</label>
                    <div class="col-sm-10">
                     <?php echo $term[0]["pembayaran"]; ?>
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lain-lain</label>
                    <div class="col-sm-10">
                     <?php  echo $term[0]["lain_lain"]; ?>
                    </div>  
                  </div>
                </li>

              </ol>
              <br/>

              <?php

                      if($mr_list_detail_sat['grand_total'] <= 50000000 ){
                        $title_of_sign = "Manager";
                      } else if($mr_list_detail_sat['grand_total'] > 50000000 AND $mr_list_detail_sat['grand_total'] <= 100000000){
                        $title_of_sign = "General Manager (GM)";
                      } else if($mr_list_detail_sat['grand_total'] > 100000000 AND $mr_list_detail_sat['grand_total'] <= 300000000){
                        $title_of_sign = "Direktur Oprational / Direktur Finance"; 
                      } else if($mr_list_detail_sat['grand_total'] > 300000000 AND $mr_list_detail_sat['grand_total'] <= 700000000){
                        $title_of_sign = "Direktur Utama";
                      } else if($mr_list_detail_sat['grand_total'] > 700000000){
                        $title_of_sign = "Direktur Utama Approvement MPI";       
                      }

                     ?>
              
              <table>
                <tr>
                  <td width="70%">
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                  </td>
                  <td width="30%">
                    <table id='customers2'>
          <tr>
            <td><?php echo $title_of_sign; ?></td>
            <td>Boat Of Director ( BOD )</td>
          </tr>
          <tr>
            <td><?php echo $user_data[$mr_list_detail_sat['manager_approval_by']] ?></td>
            <td><?php echo $user_data[$mr_list_detail_sat['created_by_po']] ?></td>
          </tr>
          <tr>
            <td><?php echo $mr_list_detail_sat['manager_approval_date'] ?></td>
            <td><?php echo $mr_list_detail_sat['created_date_po'] ?></td>
          </tr>

        </table>
                  </td>
                </tr>
              </table>
        
       

  </body></html>