<?php 
  $mr_list = $mr_list[0];
?>

<div id="content" class="container-fluid" style="overflow: auto;">

  <form method="POST" id='form-mr' action="<?php echo base_url();?>po/process_insert_po" onsubmit="return validateForm()" enctype="multipart/form-data">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?> </h6>
          <?php  echo $this->session->flashdata('message');?>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                    <label>Material Requitision No.</label>
                    <input type="hidden" name='mr_number'  value='<?php echo $mr_list['mr_number']; ?>' readonly>
                    <input type="text" class="form-control" name="form_no" id="form_no" placeholder="Type Material Requitision No." value='<?php echo $mr_list['form_number']; ?>' required readonly>
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label>Material Requitision Date</label>
                    <input type="text" class="form-control datepickerx" name="created_date" id="created_date"  value='<?php echo $mr_list['created_date']; ?>' disabled>
                  </div>
                </div> 

              </div>

              <div class="row">

                <div class="col-md">
                  <div class="form-group">
                    <label>Budget Category</label>
                    <input type="hidden" class="form-control" name="cat_id" id="created_date"  value='<?php echo $mr_list['budget_cat_id']; ?>' readonly>
                    <select id='budget' name='budget' class="form-control" onchange="fnc();">
                      <?php foreach ($budget as $value) { ?>
                         <option value='<?php echo $value['id']; ?>' <?php if($mr_list['budget_cat_id'] == $value['id']){ echo "selected"; } else { echo "disabled";} ?>><?php echo $value['category_name']; ?> </option>
                      <?php  } ?>
                    </select>         
                  </div>
                </div>

                <div class="col-md">
                  <div class="form-group">
                    <label>Department</label>
                    <input type="hidden" class="form-control" name="dept_id" id="dept_id" value='<?php echo $mr_list["budget_dept_id"]; ?>' readonly>
                    <input type="text" class="form-control" name="dept_id_show" id="dept_id_show" value='<?php echo $dept[$mr_list["budget_dept_id"]]; ?>' readonly>
                  </div>
                </div> 

              </div>

            </br>
           <div class="col-md-12">
                    <table class="table text-muted text-center" id='mr_detail'>
                       <thead class="bg-green-smoe text-white">
                        <tr>
                        
                          <th><input type="checkbox" id="selectall_omission" style="width: 15px; height: 15px; text-align: center;"></th>
                          <th>MATERIAL CODE</th>
                          <th>PO NO</th>
                          <th>DESCRIPTION</th>
                          <th>PO QTY</th>
                           <th>UOM</th>
                          <th>PRICE PER UNIT</th>
                          <th>TOTAL AMOUNT</th>
                          <th>CURRENCY</th>                         
                          <th>VENDOR</th>
                          <th>ETA DATE</th>                         
                          <th>PO REMARKS</th>
                          <th>STATUS</th>                         
                         
                        </tr>                        
                      </thead>
                      <tbody>

                      <?php 
                        $no=1; foreach($mr_list_detail as $key){
                        $search_total_po_partial = $this->po_mod->po_total_by_item_call($key['id']);
                        $waiting_qty =  $key['qty'] - $search_total_po_partial[0]['total_po'];
                      ?>

                        <tr>

                          <td>
                            <?php if($waiting_qty > 0){ ?>
                              <input type="hidden" name="id[]" value="<?= $key['id'] ?>" >
                              <input type="hidden" id='id_checkbox' name="id_checkbox[]" value="0" >
                              <input type="hidden" id='no' name="no[]" value="<?php echo $no; ?>" >
                              <input type="checkbox" id='cb_add_mr' name="cb_add_mr[]" style="width: 15px; height: 15px; text-align: center;" >
                            <?php } else { ?>
                              <input type="hidden" name="id[]" value="<?= $key['id'] ?>" >
                              <input type="hidden" id='id_checkbox' name="id_checkbox[]" value="0" >
                              <input type="hidden" id='no' name="no[]" value="<?php echo $no; ?>" >
                              <input type="hidden" id='cb_add_mr' name="cb_add_mr[]" style="width: 15px; height: 15px; text-align: center;" >                              
                            <?php } ?>
                          </td>
                         
                          <td>
                            <?php echo $key['code_material']; ?>                              
                          </td>

                          <td>
                            <?php if($waiting_qty > 0){ ?>
                                <div class="row">
                                  <div class="col-sm pr-0">
                                    <input type="text" class='form-control' id='po_number_<?php echo $key["id"]; ?>' name="po_number[]" placeholder='PO Number' onblur="checkpodata(this,'<?php echo $key['id']; ?>')"  maxlength="10" disabled > 
                                  </div>
                                  <div class="col-sm-auto pl-0">
                                    <?php if($waiting_qty > 1){ ?>
                                     <button type="button" onclick="add_newpo(this);" class="btn btn-success" title="Add New PO Number" id='button_add_<?php echo $key['id']; ?>' name="button_add[]" disabled><i class="fa fa-plus"></i></button>
                                   <?php } ?>
                                  </div>
                                </div>  
                            <?php } else { ?>
                               <input type="hidden" class='form-control' id='po_number' name="po_number[]" placeholder='PO Number' onblur="checkpodata(this,)"  maxlength="10" > 
                                <?php
                              if(!empty($key['po_number'])){

                                $datadb = $this->po_mod->po_list_detail_call($key['id']);
                                foreach ($datadb as $data_po) {

                                  $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~')."/".strtr($this->encryption->encrypt($data_po['po_number']), '+=/', '.-~');

                                  echo $data_mr_status = "<a href=".$links." target='_blank'><span class='badge badge-primary'>".$data_po['po_number']." ( ".$data_po['po_qty']." Pcs )</span></a><br/>";

                                }

                              } else {

                                 echo $data_mr_status = '<span class="badge badge-danger">Waiting</span>';

                              }

                            ?> 
                            <?php } ?>

                             <input type="hidden" id='mr_number' name="mr_number[]" value='<?php echo $mr_list['mr_number'] ?>' >   

                             <input type="hidden" id='mat_code' name="mat_code[]" value='<?php echo (isset($code_material[$key['catalog_id']]) ? $code_material[$key['catalog_id']] : '-'); ?>' >
                          </td>

                          <td>
                            <center>

                            <b>
                              <?php echo (isset($material[$key['code_material']]) ? $material[$key['code_material']] : '-'); ?>
                            </b>
                              
                            </center>

                          </td>


                           <td>
                            <center>  

                              <?php if($waiting_qty > "0"){  ?>

                              <input type="Number" id='po_qty_<?php echo $key["id"]; ?>'  class='form-control main_input' title="po_qty_<?php echo $key['id']; ?>" name="po_qty[]" value='<?php echo $waiting_qty; ?>' style='width: 75px;' max='<?php echo $waiting_qty; ?>' min='0' onfocus="save_qty($(this).val())"  oninput="calculate_qty_main(this,'<?php echo $waiting_qty; ?>')"   disabled>  

                              <input type="hidden" id='req_qty' name="req_qty[]" value='<?php echo $key['qty']; ?>' style='width: 50px;' >
                            
                              <input type="hidden" id='waiting_qty' name="waiting_qty[]" value='<?php echo $waiting_qty; ?>'   style='width: 50px;' >  

                              <?php } else {  ?>

                                   <input type="hidden" id='po_qty' name="po_qty[]">

                              <?php  
                                    echo $key['qty']; 
                                
                              } ?>
                               
                            </center>     
                          </td>
                           <td>
                             <?php echo $uom[$key['uom']]; ?>                                                         
                          </td>

                          <td>
                            <center>  

                              <input type="number" id='price_per_unit_<?php echo $key["id"]; ?>'  class='form-control' title="price_per_unit_<?php echo $key['id']; ?>" name="price_per_unit[]"  style='width: 75px;' min='1' oninput="calculate_total_amount(this);" placeholder='Actual Price' disabled> 
                               
                            </center>     
                          </td>
                          <td>
                            <center>  

                              <input type="number" id='total_amount_<?php echo $key["id"]; ?>'  class='form-control' title="total_amount_<?php echo $key['id']; ?>" name="total_amount[]"  style='width: 75px;'  min='1' placeholder='Total Amount' readonly> 
                               
                            </center>     
                          </td>
                          <td>
                            <center>  

                              <select id="currency_<?php echo $key["id"]; ?>" name="currency[]"  class="form-control" required disabled>
                                <?php foreach ($currency_list as $value) { 
                                  echo "<option value=".$value["id_cur"].">".$value["currency"]."</option>"; 
                                } ?>
                              </select>
                               
                            </center>     
                          </td>
                         
                         

                          <td>
                            <center>
                            <?php
                              if($waiting_qty <= 0){
                            ?>    
                             <input type="hidden" id='vendor' name="vendor[]">
                              
                            <?php  
                                //$datadb = $this->po_mod->po_list_detail_call($key['id']);
                                foreach ($datadb as $data_po) {

                                  $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~')."/".strtr($this->encryption->encrypt($data_po['po_number']), '+=/', '.-~');

                                  echo $data_mr_status = (isset($vendor_data[$data_po['vendor']]) ? $vendor_data[$data_po['vendor']] : '-')."<br/>";

                                }


                              } else {

                            ?>

                           <select id="vendor_<?php echo $key['id']; ?>" class="form-control" name="vendor[]" style='width:250px;'  disabled>
                                <option value="">------------------------------</option>                                   
                                <?php foreach ($vendor_list as $value) { ?>                                 
                                <option value="<?php echo $value['id_vendor']; ?>" <?php if($value['id_vendor'] == $key['vendor']){ echo "selected"; } ?>><?php echo $value['name']; ?></option> 
                                <?php } ?>                                
                              </select>

                            <?php
                              }
                            ?>
                             
                            </center>                              
                          </td>

                          <td>
                            <?php
                              if($waiting_qty <= 0){

                                echo '<input type="hidden" id="eta_date" name="eta_date[]">';

                                //$datadb = $this->po_mod->po_list_detail_call($key['id']);
                                foreach ($datadb as $data_po) {

                                  $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~')."/".strtr($this->encryption->encrypt($data_po['po_number']), '+=/', '.-~');

                                  echo $data_mr_status = date("d-M-Y",strtotime($data_po['eta_date']))."<br/>";

                                }


                              } else {

                            ?>
                             <?php if(!isset($key['eta_date']) OR empty($key['eta_date'])){ ?>
                                <input type="date" id='eta_date_<?php echo $key['id']; ?>' class='form-control' name="eta_date[]" placeholder='ETA Date'   disabled> 
                             <?php } else { ?> 
                                <input type="date" id='eta_date_<?php echo $key['id']; ?>' class='form-control' name="eta_date[]" value='<?php echo $key['eta_date'] ?>'   disabled> 
                             <?php } ?>

                            <?php
                              }
                            ?>
                          </td>
                          <td>
                            <?php
                              if($waiting_qty <= 0){

                                echo '<input type="hidden" id="remarks_po_" name="remarks_po[]">';

                                //$datadb = $this->po_mod->po_list_detail_call($key['id']);
                                foreach ($datadb as $data_po) {

                                  $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~')."/".strtr($this->encryption->encrypt($data_po['po_number']), '+=/', '.-~');

                                  echo $data_mr_status = $data_po['remarks_po']."<br/>";

                                }


                              } else {

                            ?>

                             <?php if(!isset($key['remarks_po']) OR empty($key['remarks_po'])){ ?>
                                <input type="text" class='form-control' id='remarks_po_<?php echo $key['id']; ?>' name="remarks_po[]" placeholder='PO Remarks'   disabled> 
                             <?php } else { ?> 
                                <input type="text" class='form-control' id='remarks_po_<?php echo $key['id']; ?>' name="remarks_po[]" value='<?php echo $key['remarks_po'] ?>'   disabled> 
                             <?php } ?>

                            <?php
                              }
                            ?>
                          </td>                     

                          <td>                          
                            <?php
                              if(!empty($key['po_number'])){

                                if($waiting_qty > 0){ 

                                $datadb = $this->po_mod->po_list_detail_call($key['id']);
                                foreach ($datadb as $data_po) {

                                  $links = base_url()."po/po_detail/".strtr($this->encryption->encrypt($mr_list['mr_number']), '+=/', '.-~')."/".strtr($this->encryption->encrypt($data_po['po_number']), '+=/', '.-~');

                                  echo $data_mr_status = "<a href=".$links." target='_blank'><span class='badge badge-primary'>".$data_po['po_number']." ( ".$data_po['po_qty']." Pcs )</span></a><br/>";

                                }

                              } else {
                                 echo $data_mr_status = "<span class='badge badge-success'>Completed</span></a><br/>";
                              }

                              } else {

                                 echo $data_mr_status = '<span class="badge badge-danger">OPEN</span>';

                              }
                            ?>       
                          </td>
                          

                        </tr>
                      <?php $no++;} ?>
                        
                      </tbody>
                    </table>
                  </div>
                </div>

            </div>
          </div>
          <div class="text-left mt-3">

            <a href="<?php echo base_url();?>po/mr_list" class="btn btn-secondary " title="Cancel"><i class="fas fa-arrow-circle-left"></i>&nbsp; Back </a>

            <?php if($read_permission[24] == 1){ ?>
              <?php if($status_po == "open" OR $status_po == ""){ ?>
                <button type="submit"  class="btn btn-primary"  name="btnsubmit_omission" disabled>
                    <i class="fas fa-save"></i> Update PO
                </button>
                <!-- <input class="btn btn-primary" type="submit" name="btnsubmit_omission" value="Update PO" disabled /> -->
              <?php } ?>
            <?php } ?>
        
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

 <script type="text/javascript">


  $('#selectall_omission').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);

    if($(this).prop("checked") == true){

      $('button[name="btnsubmit_omission"]').removeAttr('disabled');

      $('input[id="id_checkbox"]').val(1);
     
      
    } else if($(this).prop("checked") == false){

      $('button[name="btnsubmit_omission"]').attr('disabled', true);

      $('input[id="id_checkbox"]').val(0);
     
     
    }
    
  });



  $('input[name="cb_add_mr[]"]').click(function (e) {

    var id_array = $(this).closest('td').find('input[name="id_checkbox[]"]');
    var id_data = $(this).closest('td').find('input[name="id[]"]');

    if($(this).prop("checked") == true){
      $(id_array).val(1);
      $('button[id="button_add_'+id_data.val()+'"]').removeAttr('disabled');
      $('button[id="button_add_'+id_data.val()+'"]').prop('required',true);

      $('input[id="po_qty_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="po_qty_'+id_data.val()+'"]').prop('required',true);

      $('input[id="price_per_unit_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="price_per_unit_'+id_data.val()+'"]').prop('required',true);

      $('input[id="total_amount_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="total_amount_'+id_data.val()+'"]').prop('required',true);

      $('select[id="currency_'+id_data.val()+'"]').removeAttr('disabled');
      $('select[id="currency_'+id_data.val()+'"]').prop('required',true);

      $('input[id="po_number_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="po_number_'+id_data.val()+'"]').prop('required',true);

      $('select[id="vendor_'+id_data.val()+'"]').removeAttr('disabled');
      $('select[id="vendor_'+id_data.val()+'"]').prop('required',true);

      $('input[id="eta_date_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="eta_date_'+id_data.val()+'"]').prop('required',true);

      $('input[id="remarks_po_'+id_data.val()+'"]').removeAttr('disabled');
      $('input[id="remarks_po_'+id_data.val()+'"]').prop('required',true);
    }

    else if($(this).prop("checked") == false){
      $(id_array).val(0);
      $('button[id="button_add_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="po_qty_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="price_per_unit_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="total_amount_'+id_data.val()+'"]').attr('disabled', true);
      $('select[id="currency_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="po_number_'+id_data.val()+'"]').attr('disabled', true);
      $('select[id="vendor_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="eta_date_'+id_data.val()+'"]').attr('disabled', true);
      $('input[id="remarks_po_'+id_data.val()+'"]').attr('disabled', true);
    }

   
    if($('input[id="cb_add_mr"]:checked').length > 0){
      $('button[name="btnsubmit_omission"]').removeAttr('disabled');
    } else {
      $('button[name="btnsubmit_omission"]').attr('disabled', true);
    }
    
  });


  function add_newpo(btn) {

    var row = $(btn).closest('tr');
    $(row).after('<tr>'+row.html()+'</tr>');
    var next          = $(row).next();
    var next_btn      = $(next).find('button');
    var next_checkbox = $(next).find('input:checkbox:first');
    var next_po_qty   = $(next).find('input[name="po_qty[]"');   

    $(next_btn).replaceWith('<button type="button" onclick="delete_po(this);" class="btn btn-danger" id="button_add" title="Delete PO Number"><i class="fa fa-times"></i></button>');
    $(next_checkbox).replaceWith('<input type="checkbox" id="cb_add_mr" name="cb_add_mr[]" style="width: 15px; height: 15px; text-align: center;" checked>');
    $(next_po_qty).removeClass('main_input');
    $(next_po_qty).val(0);

  }

  function delete_po(btn) {
    var row     = $(btn).closest('tr');
    var input   = $(row).find('input[name="po_qty[]"');
    var target  = $(".main_input[title="+$(input).attr('title')+"]");
    var new_qty = parseInt($(target).val()) + parseInt($(input).val());
    $(target).val(new_qty);
    $(row).remove();
  }

  function validate_approve(param) {

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "<?php echo base_url();?>mis/approve_mis_form/<?php echo $mr_list['mrs_no']; ?>/"+param;
      }
    })

  }


  var qty_default;
  function save_qty(input) {
    qty_default = input;

    console.log('Save : '+input);

  }

  function calculate_qty_main(input,max) {

    var row   = $(input).closest('tr');
    var po_qty   = $(row).find('input[name="po_qty[]"]').val();
    var data_ppu   = $(row).find('input[name="price_per_unit[]"]').val();

    var total_price = Number(data_ppu) * Number(po_qty);

    console.log(total_price);

    $(row).find('input[name="total_amount[]"]').val(total_price);

    var max_Data = max;
    var value_Data = $(input).val();

    if(Number(value_Data) > Number(max_Data) || Number(value_Data) < 0){

        $(input).val("0").change();

       if($(input).hasClass("main_input") == false){
        $(".main_input[title="+$(input).attr('title')+"]").val(max_Data).change();
       }

      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'OPEN QTY only have '+ max + ' PCS must be filled up..',
      });
       
     return false;

    } else { 

    if($(input).hasClass("main_input") == false){

      var target            = $(".main_input[title="+$(input).attr('title')+"]");
      var target_qty        = parseInt($(target).val());
      var input_qty_default = parseInt(qty_default);
      var input_qty         = parseInt($(input).val());
      var operation;
     
      if(input_qty_default > input_qty){
        var dif_qty = parseInt(input_qty_default - input_qty);
       
        if(input_qty > 0){

          $(target).val(target_qty + dif_qty);
          qty_default = input_qty;
        
        } 
      } else {

        console.log(input_qty);

        var dif_qty = parseInt(input_qty - input_qty_default);
       
        if(target_qty - dif_qty > 0){
          $(target).val(target_qty - dif_qty);
          qty_default = input_qty;

        }       
      }
    }
  }

}


</script>

<script type="text/javascript">



  function _alert(){
    
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Submit it!'
     }).then((result) => {
      if (result.value) {
        $("#form-mr").submit();
      }
    });
    
  }

  </script>

  <script type="text/javascript">   
  $('#mr_detail').DataTable({
    "language": { 
      "infoFiltered": "" },
      "paging": false,
      "order": [],    
  });

$(document).ready(function() {
    $('.vendor_sel2').select2();
});

</script>

<script>

function checkpodata(input,id) {
    var text = $(input).val();
    $.ajax({
      url: "<?php echo base_url();?>po/check_po_number/",
      type: "post",
      data: {
        'pos': text
      },
      success: function(data) {
      
      $('select[id="vendor_'+id+'"]').val(data).change();

      
      }
    });
  }


  function checkQtyInput(max,value) {
    
    var max_Data = max;
    var value_Data = $(value).val();


    if(Number(value_Data) > Number(max_Data) || Number(value_Data) < 0){

      $('input[name="po_qty[]"]').val("0").change();

      Swal.fire({
        type: 'warning',
        title: 'Oops...',
        text: 'OPEN QTY only have '+ max + ' PCS must be filled up..',
      });
       
     return false;

    } 

    
    
  }


  function calculate_total_amount(input) {

    var row   = $(input).closest('tr');
    var po_qty   = $(row).find('input[name="po_qty[]"]').val();
    var data_ppu   = $(row).find('input[name="price_per_unit[]"]').val();

    var total_price = Number(data_ppu) * Number(po_qty);

    console.log(total_price);

    $(row).find('input[name="total_amount[]"]').val(total_price);

  }

 
</script>