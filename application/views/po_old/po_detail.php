<?php 
  $po_list = $po_list[0]; 
  $po_form = substr($po_list['po_number'], 3).'/MEB-M&R-BTM/II/'.date('Y', strtotime($po_list['date_po']));
?>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>po/process_insert_po">
    <input type="hidden" name="id_po" value="<?= $po_list['id'] ?>">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Nomor PO / <i>PO Number</i></label>
                  <input type="text" class="form-control" name="po_no" value="<?= $po_form ?>" readonly>
                </div>
                <div class="col">
                  <label class="col-form-label">Penawaran / <i>Quotation</i></label>
                  <input type="text" class="form-control" name="quotation" placeholder="Penawaran" value="<?= $po_list['quotation'] ?>" required>
                </div>
              </div>

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Tanggal / <i>Date</i></label>
                  <input type="text" class="form-control datepicker" name="date_po" value="<?= date('d-m-Y', strtotime($po_list['date_po'])) ?>" placeholder="dd-mm-yyyy" required>
                </div>
                <div class="col">
                  <label class="col-form-label">Tanggal / <i>Date</i></label>
                  <input type="text" class="form-control datepicker" name="date_quotation" value="<?= date('d-m-Y', strtotime($po_list['date_quotation'])) ?>" placeholder="dd-mm-yyyy" required>
                </div>
              </div>

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Alamat / <i>Address</i></label>
                  <textarea class="form-control" name="address" placeholder="Alamat" required><?= $po_list['address'] ?></textarea>
                </div>
                <div class="col">
                  <label class="col-form-label">Referensi / <i>Reference</i></label>
                  <input type="text" class="form-control" name="reference" placeholder="Referensi" value="<?= $po_list['reference'] ?>" required>
                </div>
              </div>

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Kepada / <i>Attention</i></label>
                  <input type="text" class="form-control" name="assign_to" placeholder="Kepada" value="<?= $po_list['assign_to'] ?>" required>
                </div>
                <div class="col">
                </div>
              </div>

            </div>
          </div>
          <br>

          <h6 class="pb-2 mb-0">Material List</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table" name="table_mr_list">
                <thead>
                  <tr>
                    <th>Deskripsi / <i>Description</i></th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Harga Per-Unit / <i>Price Per-Unit</i></th>
                    <th>Total Harga / <i>Total Price</i></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($po_detail_list as $key => $value) { ?>
                  <tr>
                    <td><?= $value['material'] ?></td>
                    <td><input type="number" name="qty[]" class="form-control" value="<?= $value['qty'] ?>"></td>
                    <td><?= $value['uom'] ?></td>
                    <td><input type="number" name="price_per_unit[]" class="form-control" value="<?= $value['price_per_unit'] ?>"></td>
                    <td><input type="number" name="total_price[]" class="form-control" value="<?= $value['qty'] * $value['price_per_unit'] ?>" readonly></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>

              <br><br>
              <label>Syarat dan Ketentuan / <i>Term and Condition</i></label>
              
              <ol type="a">
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Waktu Pengiriman</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control-plaintext" value="12 Minggu setelah konfirmasi">
                    </div>  
                  </div>
                </li>
                
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kondisi Pengiriman</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" value="" placeholder="Kondisi Pengiriman">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lokasi Pengiriman</label>
                    <div class="col-sm-10">
                      <textarea class="form-control-plaintext" rows="3">PT. Mitra Energi Batam "MEB"&#13;&#10;JL. Lintas Gas Negara, Trans-Barelang, KM. 3,5 Tembesi, Sagulung, Batam, Kep. Riau-INDONESIA
                      </textarea>
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pembayaran</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control-plaintext" value="14 Hari setelah hari setelah barang dan dokumen penagihan di terima oleh MEB.">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lain-lain</label>
                    <div class="col-sm-10">
                      <textarea class="form-control-plaintext" rows="12">1. Semua pembayaran akan dilakukan setelah MEB menerima semua dokumen pendukung (invoice, PO konfirmasi dan dokumen pendukung lainnya). &#13;&#10;2. Dokumen penagihan dikirimkan ke MEB dengan alamat: Jl. Lintas Gas Negara, Trans Barelang, KM. 3,5 Tembesi, Sagulung, Batam. &#13;&#10;3. PO Konfirmasi harus dikirim kembali dalam waktu 3 hari kerja setelah PO dikirimkan. apabila tidak ada konfirmasi maka PT. Raya Filter Konsep dianggap telah menyetujui semua &#13;&#10;4. Jangka waktu pengiriman terhitung sejak PO konfirmasi dari PT. Raya Filter Konsep. &#13;&#10;5. Pengurusan dokumen endorsemen PPFTZ 03 apabila di perlukan berikut biaya-biaya yang timbul menjadi tanggung jawab PT. Raya Filter Konsep. &#13;&#10;6. Denda keterlambatan akan dikenakan sebesar 0,5 % per hari keterlambatan apabila barang diterima melebihi waktu yang telah di sepakati kedua belah pihak ( merefer  point a. syarat dan ketentuan dalam PO ini).&#13;&#10;7. PT. Raya Filter Konsep akan memberikan Certificate of Origin dan Certificate of Manufacture.&#13;&#10;8. Garansi selama 24 bulan setelah barang di terima MEB atau 12 bulan setelah pemasangan di unit MEB.&#13;&#10;9. Pemasok wajib mematuhi semua peraturan yang berlaku di lingkungan MEB berikut peraturan mengenai K3L.
                      </textarea>
                    </div>  
                  </div>
                </li>

              </ol>

            </div>
          </div>

          <div class="row">
            <div class="col-md-2 offset-md-8 text-center">
              <label><b>PT. Mitra Energi Batam</b></label>
              <br><br>
              <button type="button" class="btn btn-success"> Sign </button>
              <br><br>
              <label><b>Bambang Urip Setiawan Adi</b></label>
              <label><b>Direktur Operasi</b></label>
            </div>

            <div class="col-md-2 text-center">
              <label><b>Diakui & Diterima</b></label>
              <br><br>
              <div class="text-center">
                <button type="button" class="btn btn-success"> Sign </button>
              </div>
              <br>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label"><b>Nama</b></label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="nama_sign" placeholder="Nama">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label"><b>Tanggal</b></label>
                <div class="col-sm-8">
                  <input type="text" class="form-control datepicker" name="tanggal_sign" value="<?= date('Y-m-d') ?>" readonly>
                </div>
              </div>
            </div>
          </div>

          <div class="text-right mt-3">
            
            <?php if($read_permission[60] == 1){ ?>
                <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <?php } ?>

            <a href="<?php echo base_url();?>" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function get_total_price(input){
    var price_per_unit = $(input).val();
    var qty = $(input).closest('tr').find('input[name="qty[]"]').val();

    var total_price = parseInt(price_per_unit) * parseInt(qty);

    $(input).closest('tr').find('input[name="total_price[]"]').val(total_price);
  }

</script>