<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>po/process_insert_po">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Nomor PO / <i>PO Number</i></label>
                  <input type="text" class="form-control" name="po_no" placeholder="Auto Generate" readonly>
                </div>
                <div class="col">
                  <label class="col-form-label">Penawaran / <i>Quotation</i></label>
                  <input type="text" class="form-control" name="quotation" placeholder="Penawaran" required>
                </div>
              </div>

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Tanggal / <i>Date</i></label>
                  <input type="text" class="form-control datepicker" name="date_po" placeholder="dd-mm-yyyy" required>
                </div>
                <div class="col">
                  <label class="col-form-label">Tanggal / <i>Date</i></label>
                  <input type="text" class="form-control datepicker" name="date_quotation" placeholder="dd-mm-yyyy" required>
                </div>
              </div>

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Alamat / <i>Address</i></label>
                  <textarea class="form-control" name="address" placeholder="Alamat" required></textarea>
                </div>
                <div class="col">
                  <label class="col-form-label">Referensi / <i>Reference</i></label>
                  <input type="text" class="form-control" name="reference" placeholder="Referensi" required>
                </div>
              </div>

              <div class="row">
                <div class="col">
                  <label class="col-form-label">Kepada / <i>Attention</i></label>
                  <input type="text" class="form-control" name="assign_to" placeholder="Kepada" required>
                </div>
                <div class="col">
                </div>
              </div>

            </div>
          </div>
          <br>

          <h6 class="pb-2 mb-0">Material List</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-row">
                <div class="form-group col-md-12">
                  <label>MR Number</label>
                  <input type="text" class="form-control" placeholder="MR Number" name="mr_number" required onfocus="$(this).removeClass('is-valid is-invalid'); mr_autocomplete(this)" onblur="check_mr(this)">
                </div>
              </div>

              <table class="table d-none" name="table_mr_list">
                <thead>
                  <tr>
                    <th><input type="checkbox" name="check_all"></th>
                    <th>Deskripsi / <i>Description</i></th>
                    <th>Qty</th>
                    <th>Unit</th>
                    <th>Harga Per-Unit / <i>Price Per-Unit</i></th>
                    <th>Total Harga / <i>Total Price</i></th>
                  </tr>
                </thead>
                <tbody id="mr_list">
                  <tr>
                    <td colspan="5"></td>
                    <td>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label"><b>Total : </b></label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" placeholder="" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label"><b>PPN : </b></label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" value="0">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label"><b>Grand Total : </b></label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" placeholder="" readonly>
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>

              <br><br>
              <label>Syarat dan Ketentuan / <i>Term and Condition</i></label>
              
              <ol type="a">
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Waktu Pengiriman</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control-plaintext" value="12 Minggu setelah konfirmasi">
                    </div>  
                  </div>
                </li>
                
                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Kondisi Pengiriman</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" value="" placeholder="Kondisi Pengiriman">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lokasi Pengiriman</label>
                    <div class="col-sm-10">
                      <textarea class="form-control-plaintext" rows="3">PT. Mitra Energi Batam "MEB"&#13;&#10;JL. Lintas Gas Negara, Trans-Barelang, KM. 3,5 Tembesi, Sagulung, Batam, Kep. Riau-INDONESIA
                      </textarea>
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Pembayaran</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control-plaintext" value="14 Hari setelah hari setelah barang dan dokumen penagihan di terima oleh MEB.">
                    </div>  
                  </div>
                </li>

                <li>
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Lain-lain</label>
                    <div class="col-sm-10">
                      <textarea class="form-control-plaintext" rows="12">1. Semua pembayaran akan dilakukan setelah MEB menerima semua dokumen pendukung (invoice, PO konfirmasi dan dokumen pendukung lainnya). &#13;&#10;2. Dokumen penagihan dikirimkan ke MEB dengan alamat: Jl. Lintas Gas Negara, Trans Barelang, KM. 3,5 Tembesi, Sagulung, Batam. &#13;&#10;3. PO Konfirmasi harus dikirim kembali dalam waktu 3 hari kerja setelah PO dikirimkan. apabila tidak ada konfirmasi maka PT. Raya Filter Konsep dianggap telah menyetujui semua &#13;&#10;4. Jangka waktu pengiriman terhitung sejak PO konfirmasi dari PT. Raya Filter Konsep. &#13;&#10;5. Pengurusan dokumen endorsemen PPFTZ 03 apabila di perlukan berikut biaya-biaya yang timbul menjadi tanggung jawab PT. Raya Filter Konsep. &#13;&#10;6. Denda keterlambatan akan dikenakan sebesar 0,5 % per hari keterlambatan apabila barang diterima melebihi waktu yang telah di sepakati kedua belah pihak ( merefer  point a. syarat dan ketentuan dalam PO ini).&#13;&#10;7. PT. Raya Filter Konsep akan memberikan Certificate of Origin dan Certificate of Manufacture.&#13;&#10;8. Garansi selama 24 bulan setelah barang di terima MEB atau 12 bulan setelah pemasangan di unit MEB.&#13;&#10;9. Pemasok wajib mematuhi semua peraturan yang berlaku di lingkungan MEB berikut peraturan mengenai K3L.
                      </textarea>
                    </div>  
                  </div>
                </li>

              </ol>

            </div>
          </div>

          <div class="row">
            <div class="col-md-2 offset-md-8 text-center">
              <label><b>PT. Mitra Energi Batam</b></label>
              <br><br>
              <button type="button" class="btn btn-success"> Sign </button>
              <br><br>
              <label><b>Bambang Urip Setiawan Adi</b></label>
              <label><b>Direktur Operasi</b></label>
            </div>

            <div class="col-md-2 text-center">
              <label><b>Diakui & Diterima</b></label>
              <br><br>
              <div class="text-center">
                <button type="button" class="btn btn-success"> Sign </button>
              </div>
              <br>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label"><b>Nama</b></label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="nama_sign" placeholder="Nama">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label"><b>Tanggal</b></label>
                <div class="col-sm-8">
                  <input type="text" class="form-control datepicker" name="tanggal_sign" value="<?= date('Y-m-d') ?>" readonly>
                </div>
              </div>
            </div>
          </div>

          <div class="text-right mt-3">
            
            <?php if($read_permission[60] == 1){ ?>
                <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <?php } ?>

            <a href="<?php echo base_url();?>" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function mr_autocomplete(input){
    $(input).autocomplete({
      source: "<?php echo base_url(); ?>po/mr_autocomplete",
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });
  }

  function check_mr(input){
    var mr_number = $(input).val();

    if(mr_number == '' || mr_number.includes('Found') == true){
      $(input).addClass('is-invalid');
      // $('button[name="submit_detail"]').attr('disabled', true);
      $('#submit_btn_po').attr('hidden', true);
    } else {
      $.ajax({
        url: "<?= base_url() ?>po/check_mr",
        type: "POST",
        data: {
          'mr_number': mr_number
        },
        success: function(data){
          var hasil = JSON.parse(data);
          if(hasil.hasil == 0){
            $(input).addClass('is-invalid');

            // $('button[name="submit_detail"]').attr('disabled', true);
            $('table[name="table_mr_list"]').addClass('d-none');

            $('#submit_btn_po').attr('hidden', true);

          } else {
            $(input).addClass('is-valid');
            $(input).attr('readonly', true);
            $('table[name="table_mr_list"]').removeClass('d-none');

            $('#submit_btn_po').removeAttr('hidden');

            mr_list();
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });

    }

    function mr_list(){
      var mr_number = $('input[name="mr_number"]').val();

      $.ajax({
        url: "<?= base_url() ?>po/po_mr_list",
        type: "POST",
        data: {
          'mr_number': mr_number
        },
        success: function(data){
          var hasil = JSON.parse(data);
          var html = '';
          var i;
          for(i=0; i<hasil.length; i++){
              html += '<tr>'+
                        '<td name="checkbox_material"><input type="checkbox" onclick="check_material(this)" name="select_material[]" value="'+hasil[i].id_material+'"></td>'+
                        '<td>' + 
                          '<input type="hidden" name="mr_detail_id[]" value="' +hasil[i].id+ '" disabled required>' +
                          '<input type="hidden" name="id_material[]" value="' +hasil[i].id_material+ '" disabled required>'+hasil[i].material+'</td>'+
                        '<td><input type="number" name="qty[]" min="0" max="' +hasil[i].qty_order+ '" class="form-control" value="' +hasil[i].qty_order+ '" disabled required></td>'+
                        '<td><input type="hidden" name="id_uom[]" value="' +hasil[i].id_uom+ '" disabled required>'+hasil[i].uom+'</td>'+
                        '<td><input type="number" name="price_per_unit[]" oninput="get_total_price(this)" class="form-control" disabled required></td>'+
                        '<td><input type="number" name="total_price[]" class="form-control" disabled readonly required></td>'+
                      '</tr>';
          }

          html += '<tr>' + 
                    '<td colspan="5"></td>' +
                    '<td>' +
                      '<div class="form-group row">' + 
                        '<label class="col-sm-3 col-form-label"><b>Total : </b></label>' +
                        '<div class="col-sm-9">' + 
                          '<input type="number" class="form-control" placeholder="" readonly>' +
                        '</div>' + 
                      '</div>' + 
                      '<div class="form-group row">' + 
                        '<label class="col-sm-3 col-form-label"><b>PPN : </b></label>' +
                        '<div class="col-sm-9">' + 
                          '<input type="number" class="form-control" value="0">' +
                        '</div>' + 
                      '</div>' + 
                      '<div class="form-group row">' +
                        '<label class="col-sm-3 col-form-label"><b>Grand Total : </b></label>' +
                        '<div class="col-sm-9">' + 
                          '<input type="number" class="form-control" placeholder="" readonly>' +
                        '</div>' +
                      '</div>' +
                    '</td>' +
                  '</tr>';

          $('#mr_list').html(html);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });

    }
  }

  function get_total_price(input){
    var price_per_unit = $(input).val();
    var qty = $(input).closest('tr').find('input[name="qty[]"]').val();

    var total_price = parseInt(price_per_unit) * parseInt(qty);

    $(input).closest('tr').find('input[name="total_price[]"]').val(total_price);
  }

  function check_material(input){
    if($(input).is(":checked")){
      $(input).closest('tr').find('input').removeAttr('disabled');
    }
    else if($(input).is(":not(:checked)")){
      $(input).closest('tr').find('input').attr('disabled', true);
      $(input).closest('tr').find('input[name="select_material[]"]').removeAttr('disabled');
    }
  }

</script>