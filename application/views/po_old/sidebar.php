<div class="wrapper" style="min-height: 79vh">
  <nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
    <ul class="list-unstyled components">
           
       <?php if($read_permission[23] == 1){ ?>
      <li>
        <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fas fa-home"></i> PO ( Purchase Order )
        </a>
        <ul class="list-unstyled" id="homeSubmenu2">
          <?php if($read_permission[24] == 1){ ?>
          <li>
            <a href="<?php echo base_url();?>po/po_new"><i class="fas fa-plus"></i> &nbsp; Create New</a>
          </li>
          <?php } ?>
          
          <li>
            <a href="<?php echo base_url();?>po/po_list/waiting"><i class="fas fa-list"></i> &nbsp; PO Waiting List</a>
          </li>

          <li>
            <a href="<?php echo base_url();?>po/po_list/complete"><i class="fas fa-list"></i> &nbsp; PO Complete List</a>
          </li>
        </ul>
      </li> 

      <li>
        <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fas fa-home"></i> SO ( Service Order )
        </a>
        <ul class="list-unstyled" id="homeSubmenu2">
          <?php if($read_permission[24] == 1){ ?>
          <li>
            <a href="<?php echo base_url();?>po/so_new"><i class="fas fa-plus"></i> &nbsp; Create New</a>
          </li>
          <?php } ?>
          
          <li>
            <a href="<?php echo base_url();?>po/po_list"><i class="fas fa-list"></i> &nbsp; SO List</a>
          </li>         
        </ul>
      </li> 
   <?php } ?>

    </ul>
  </nav>