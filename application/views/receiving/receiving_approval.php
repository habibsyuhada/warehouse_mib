  <?php $receiving_master = $receiving_master[0] ?>
  <div id="content" class="container-fluid" style="overflow: auto;">
    <form method="POST" id='form-mr' action="<?php echo base_url();?>receiving/receiving_approval_process" enctype="multipart/form-data">
      <input type="hidden" name="receiving_id" value="<?= $receiving_master['id'] ?>">
      <div class="row">

        <div class="col-md-12">
          <div class="my-3 p-3 bg-white rounded shadow-sm">
            <h6 class="pb-2 mb-0"><?php echo $meta_title ?> </h6>
            <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
              <div class="container-fluid">

                <div class="row">
                  <div class="col-md">
                    <div class="form-group">
                      <label>DO/PL</label>
                      <input type="text" class="form-control" name="do_pl" value='<?= $receiving_master['do_pl'] ?>' readonly> 
                    </div>
                  </div>
                    
                  <div class="col-md">
                    <div class="form-group">
                      <label>ATA</label>
                      <input type="text" class="form-control" name="ata" value='<?= $receiving_master['ata'] ?>' readonly>  
                    </div>
                  </div>
                </div>
            
                <div class="col-md-12">
                  <table class="table text-muted text-center" id='mr_detail'>
                    <thead class="bg-green-smoe text-white">
                      <tr>
                        <th rowspan="2">PO NUMBER</th>
                        <th rowspan="2">MATERIAL</th>
                        <th colspan="2">PO</th>
                        <th rowspan="2">UNIT</th> 
                        <th rowspan="2">COUNTRY ORIGIN</th>
                        <th rowspan="2">BRAND</th>
                        <th rowspan="2">COLOR CODE</th>                           
                        <th rowspan="2">STATUS</th>
                      </tr>
                      <tr>
                        <th>QTY</th>
                        <th>RECEIVING</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($receiving_detail_list as $receiving_detail){ ?>
                      <tr>
                        <td><?= $receiving_detail['po_number'] ?></td>
                        <td><?= $receiving_detail['material'] ?></td>
                        <td><?= $receiving_detail['qty_request'] ?></td>
                        <td><?= $receiving_detail['qty'] ?></td>
                        <td><?= $receiving_detail['uom_name'] ?></td>
                        <td><?= $receiving_detail['country_origin'] ?></td>
                        <td><?= $receiving_detail['brand'] ?></td>
                        <td><?= $receiving_detail['color_code'] ?></td>
                        <td><?= $receiving_detail['status_material'] ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>

            </div>

            <?php 
              $get_user_current    = $this->mr_mod->get_user_data_sign($read_cookies[0]);
              $get_warehouse       = $this->mr_mod->get_user_data_sign($receiving_master['warehouse_approved']);
              $get_procurement     = $this->mr_mod->get_user_data_sign($receiving_master['procurement_approved']);
            ?>

            <label></label>
            <table class="table table-bordered text-center">
              <thead>
                <tr>
                  <th>User</th>
                  <th>Warehouse</th>
                  <th>Procurement</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div class="text-center">
                      <?php if($receiving_master['user_approved'] == 0){ ?>
                        <button type="button" class="btn btn-success" onclick="approval_button('user')">Approve</button>
                      <?php } else { ?>
                        <img src="data:image/png;base64, <?php echo $get_user_current[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' />
                        <br/>
                        <?php echo $get_user_current[0]["full_name"]; ?>
                      <?php } ?>
                    </div>
                  </td>
                  <td>
                    <div class="text-center">
                      <?php if($receiving_master['warehouse_approved'] == 0){ ?>
                      <button type="button" class="btn btn-success" onclick="approval_button('warehouse')">Approve</button>
                      <?php } else { ?>
                        <img src="data:image/png;base64, <?php echo $get_warehouse[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' />
                        <br/>
                        <?php echo $get_warehouse[0]["full_name"]; ?>
                      <?php } ?>
                    </div>
                  </td>
                  <td>
                    <div class="text-center">
                      <?php if($receiving_master['procurement_approved'] == 0){ ?>
                      <button type="button" class="btn btn-success" onclick="approval_button('procurement')">Approve</button>
                      <?php } else { ?>
                        <img src="data:image/png;base64, <?php echo $get_procurement[0]["sign_approval"]; ?>" style='width: 100px; height: 50px;' />
                        <br/>
                        <?php echo $get_procurement[0]["full_name"]; ?>
                      <?php } ?>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>

            <!-- <button class="btn btn-success" type="submit"><i class="fas fa-check"></i> Approve</button> -->
            <!-- <button type="button" class="btn btn-success" onclick="approval_button(<?= $receiving_master['id'] ?>)"><i class="fas fa-check"></i> Approve</button> -->
            <!-- <a href="<?php echo base_url();?>receiving_list/waiting" class="btn btn-secondary" title="Cancel"><i class="fas fa-arrow-circle-left"></i>&nbsp; Back</a>               -->

          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<script type="text/javascript">
  function approval_button(param){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Approve it!'
    }).then((result) => {
      if(result.value){
        $.ajax({
          data: {
            'receiving_id' : <?= $receiving_master['id'] ?>,
            'access' : param
          },
          url: '<?= base_url() ?>receiving/receiving_approval_process',
          type: "post",
          success : function(data){
            Swal.fire(
              'Approved!',
              'Data has been approved.',
              'success'
            ).then(function() {
              location.reload();
            });
          }
        });
      }
    })
  }
</script>