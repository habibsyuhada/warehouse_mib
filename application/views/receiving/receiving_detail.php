<style type="text/css">
  .nav-link{
    color: #000;
  }
  .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    color: #007bff;
    background: #fff;
    border-bottom: 2px solid #007bff;
    border-radius: 0px;
  }

  .valign-middle{
    vertical-align: middle;
  }
</style>

<!-- <input type="hidden" name="mto_id" value="<?= $receiving_list['mto_id'] ?>"> -->
<div id="content" class="container-fluid" style="overflow: auto;">
 
  <div class="row">

    <div class="col-md-12">
       <div class="overflow-auto text-muted  ">
          <h6>
            <ol class="breadcrumb bg-white">DO / PL No : <?= $receiving_list['do_pl'] ?></ol>
          </h6>
       </div>
    </div>

    <?php echo $this->session->flashdata('message');?>
    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <ul class="nav nav-pills" id="pills-tab" role="tablist">

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == '' ? 'active' : '') ?>" data-toggle="pill" href="#pills-receiving">Receiving</a>
          </li>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'rd' ? 'active' : '') ?>" data-toggle="pill" href="#pills-detail">Receiving Detail</a>
          </li>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'ml' ? 'active' : '') ?>" data-toggle="pill" href="#pills-material">Material List</a>
          </li>

          <li class="nav-item">
            <a class="nav-link <?php echo ($t == 'at' ? 'active' : '') ?>" data-toggle="pill" href="#pills-attachment">Documents</a>
          </li>

        </ul>
        <div class="overflow-auto media text-muted py-3 border-bottom border-top border-gray">
          <div class="container-fluid">
            <div class="tab-content">

              <!-- RECEIVING MASTER -->
              <div class="tab-pane fade <?php echo ($t == '' ? 'show active' : '') ?>" id="pills-receiving">
                <form method="POST" id="receiving_master" action="<?php echo base_url();?>receiving/update_receiving">

                  <input type="hidden" name="id_receiving_master" value="<?= $receiving_list['id'] ?>">

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">DO / PL :</label>
                        <div class="col-xl">
                          <input type="text" name="do_pl" class="form-control" value="<?= $receiving_list['do_pl'] ?>" readonly>
                          <p name="alert_do_pl" class="d-none text-danger">* Duplicate DO / PL</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <label class="col-xl-3 col-form-label">ATA :</label>
                        <div class="col-xl">
                          <input type="text" name="ata" class="form-control datepicker" value="<?= date('d-m-Y', strtotime($receiving_list['ata'])) ?>" required>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <?php if($read_permission[61] == 1){ ?>
                            <button type="submit" name='submit_receiving' value='submit' class="btn btn-success" title="Update"><i class="fa fa-edit"></i> Update</button>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
              <!-- RECEIVING MASTER END -->

              <!-- RECEIVING DETAIL -->
              <div class="tab-pane fade <?php echo ($t == 'rd' ? 'show active' : '') ?>" id="pills-detail">
                <form method="POST" id="receiving_detail" action="<?= base_url() ?>receiving/receiving_detail_add_process">
                  <input type="hidden" name="master_receiving_id" value="<?= $master_receiving_id ?>">
                  <input type="hidden" name="do_pl" value="<?= $receiving_list['do_pl'] ?>">

                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label>PO Number</label>
                      <input type="text" class="form-control" placeholder="PO Number" name="po_number" onfocus="$(this).removeClass('is-valid is-invalid')" onblur="check_po(this)">
                    </div>
                  </div>

                  <table class="table table-hover text-center d-none" name="table_catalog_by_po">
                    <thead class="bg-green-smoe text-white">
                      <tr>
                        <th rowspan="2" style="vertical-align: middle;">
                          <input type="checkbox" name="selectall_material" onclick="selectall_material_click(this)" data-toggle="collapse" data-target="targets" class="clickable">
                        </th>
                        <th rowspan="2" style="vertical-align: middle;">PO NUMBER</th>
                        <th rowspan="2" style="vertical-align: middle;">DESCRIPTION</th>
                        <th rowspan="2" style="vertical-align: middle;">VENDOR</th>
                        <th colspan="2" style="vertical-align: middle;">QTY</th>
                        <th rowspan="2" style="vertical-align: middle;">UNIT</th>
                        <th rowspan="2" style="vertical-align: middle;">STATUS</th>
                      </tr>
                      <tr>
                        <th style="vertical-align: middle;">REQUEST</th>
                        <th style="vertical-align: middle;">RECEIVING</th>
                      </tr>
                    </thead>
                    <tbody id="catalog_by_po">
                      
                    </tbody>
                  </table>

                  <div class="row">
                    <div class="col-md">
                    </div>
                    <div class="col-md">
                      <div class="form-group row">
                        <div class="col-xl text-right">
                          <?php if($read_permission[60] == 1){ ?>
                              <button type="button" id='submit_btn_po' name='submit_detail' class="btn btn-success" title="Submit" onclick="submit_action('receiving_detail')" hidden="true" disabled><i class="fa fa-check"></i> Submit</button>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
              <!-- RECEIVING DETAIL END -->

              <!-- MATERIAL LIST -->
              <div class="tab-pane fade <?php echo ($t == 'ml' ? 'show active' : '') ?>" id="pills-material">
                <!-- TABLE  -->
                <table class="table table-hover text-center" id="table_receiving_list">
                  <thead class="bg-green-smoe text-white text-center">
                    <tr>
                      <th class="valign-middle" rowspan="2">PO NUMBER</th>
                      <th class="valign-middle" rowspan="2">DESCRIPTION</th>
                      <th class="valign-middle" colspan="2">QTY</th>
                      <th class="valign-middle" rowspan="2">UNIT</th>
                      <th class="valign-middle" rowspan="2">COUNTRY ORIGIN</th>
                      <th class="valign-middle" rowspan="2">BRAND</th>
                      <th class="valign-middle" rowspan="2">COLOR CODE</th>
                      <th class="valign-middle" rowspan="2">STATUS</th>
                    </tr>
                    <tr>
                      <th>REQUEST</th>
                      <th>RECEIVING</th>
                    </tr>
                  </thead>
                </table>

                <br>

              </div>
              <!-- MATERIAL LIST END -->

              <!-- ATTACHMENT RECEIVING -->
              <div class="tab-pane fade <?php echo ($t == 'ra' ? 'show active' : '') ?>" id="pills-attachment">
                <?php if($read_permission[63] == 1){ ?>
                <form method="POST" id="receiving_attachment" class="dropzone dz-clickable" action="<?php echo base_url();?>receiving/receiving_attachment_upload" enctype="multipart/form-data">
                  <input type="hidden" name="id_receiving_master" value="<?= $receiving_list['id'] ?>">
                  <input type="hidden" name="name_file">
                  <!-- <input type="file" name="file_attachment"> -->
                  <!-- <div class="dz-default dz-message"><span>Drop files here to upload</span></div> -->
                </form>
                <?php } ?>

                <br><br>
                <table class="table table-hover">
                  <thead class="bg-green-smoe text-white">
                    <tr>
                      <th width="5"></th>
                      <th>FILENAME</th>
                      <th class="text-center">ACTION</th>
                    </tr>
                  </thead>
                  <tbody id="receiving_attachment_body">
                    
                  </tbody>
                </table>

              </div>
              <!-- ATTACHMENT RECEIVING END -->

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->

<!-- GENERAL -->
<script type="text/javascript">

  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });

  $('#table_receiving_list').DataTable({
    // "scrollX": true,
    "language": { 
      "infoFiltered": "" 
    },
    "paging": true,
    "processing": true,
    "serverSide": true,
    "order": [],
    "ajax": {
        "url": "<?php echo base_url();?>receiving/receiving_detail_list_json/<?= $master_receiving_id ?>",
        "type": "POST",
    },
    "columnDefs": [{
      "targets": 'no-sort',
      "orderable": false,
    }, ],
  });


  $('.datepicker').datepicker({
    startDate: new Date(),
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  $('input[name="po_number"]').autocomplete({
    source: "<?php echo base_url(); ?>receiving/po_autocomplete",
    autoFocus: true,
    classes: {
      "ui-autocomplete": "highlight"
    }
  });

  function submit_action(param){

    if(param == 'receiving_detail'){
      var _url = '<?= base_url() ?>receiving/receiving_detail_add_process';
      var _status =  $('.detail_required').filter(function(){ return this.value==''}).length;

      if(_status > 0){

        Swal.fire(
          'Error!',
          'Please Complete the form',
          'error'
        );
        return false;
      }

    } else if(param == 'receiving_attachment'){
      var _url = '<?= base_url() ?>receiving/receiving_attachment_add_process';
      $('#'+param).removeAttr('action').attr('action', _url);
      // return false;
    }

    $.ajax({
      url: _url,
      type: "post",
      success : function(data){
        $('#'+param).submit();

        // if(param == 'receiving_detail'){
        //   show_receiving_detail();
        // }
      }
    });
  }

  function delete_action(param, id){

    if(param == 'receiving_detail'){
      var _url = '<?= base_url() ?>receiving/receiving_detail_delete_process';
    } else if(param == 'receiving_attachment'){
      var val_receiving_id = $('input[name="id_receiving_master"]').val();
      var _url = '<?= base_url() ?>receiving/receiving_attachment_delete_process/' + val_receiving_id;
    }

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          data: {
            'id' : id,
          },
          url: _url,
          type: "post",
          success : function(data){

            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            ).then(function() {
              location.reload();
            });

            if(param == 'receiving_detail'){
              show_receiving_detail();
            }
          }
        });
      }
    })
  }

</script>
<!--  -->

<!-- RECEIVING -->
<script type="text/javascript">
  function check_do_pl(input){
    var val_do_pl = $(input).val();
    var val_receiving_id = $('input[name="id_receiving_master"]').val();

    $.ajax({
      url: "<?= base_url() ?>receiving/receiving_check_do_pl",
      type: "POST",
      data: {
        'receiving_id' : val_receiving_id,
        'do_pl': val_do_pl
      },
      success: function(data){
        var hasil = JSON.parse(data);
        if(hasil.hasil == 0){
          $(input).addClass('is-valid');
          $(input).removeClass('is-invalid');

          $('button[name="submit_receiving"]').removeAttr('disabled');
          $('p[name="alert_do_pl"]').addClass('d-none');

        } else {
          $(input).addClass('is-invalid');
          $(input).removeClass('is-valid');

          $('button[name="submit_receiving"]').attr('disabled', true);
          $('p[name="alert_do_pl"]').removeClass('d-none');
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });

  }
</script>
<!--  -->

<!-- RECEIVING DETAIL -->
<script type="text/javascript">

  function check_unique_number(input){
    var val_unique_number = $(input).val();

    if(val_unique_number == ""){
      $('p[name="alert_unique_number"]').removeClass('d-none');
      $('p[name="alert_unique_number"]').html('* Unique Number is Required');

      $(input).addClass('is-invalid');
      $(input).removeClass('is-valid');

      $('button[name="submit_detail"]').attr('disabled', true);
            
    } else {
      $.ajax({
        url: "<?= base_url() ?>receiving/receiving_check_unique_no",
        type: "POST",
        data: {
          'unique_no': val_unique_number
        },
        success: function(data){
          if(data == '0'){

            $('p[name="alert_unique_number"]').addClass('d-none');
            $(input).addClass('is-valid');
            $(input).removeClass('is-invalid');

            $('button[name="submit_detail"]').removeAttr('disabled');

          } else {

            $('p[name="alert_unique_number"]').removeClass('d-none');
            $('p[name="alert_unique_number"]').html('* Duplicate Unique Number');

            $('button[name="submit_detail"]').attr('disabled', true);

            $(input).removeClass('is-valid');
            $(input).addClass('is-invalid');
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });
    }

  }

  function check_po(input){
    var po_number = $(input).val();

    if(po_number == '' || po_number.includes('Found') == true){
      $(input).addClass('is-invalid');
      // $('button[name="submit_detail"]').attr('disabled', true);
      $('#submit_btn_po').attr('hidden', true);
    } else {
      $.ajax({
        url: "<?= base_url() ?>receiving/receiving_check_po",
        type: "POST",
        data: {
          'po': po_number
        },
        success: function(data){
          var hasil = JSON.parse(data);
          if(hasil.hasil == 0){
            $(input).addClass('is-invalid');

            // $('button[name="submit_detail"]').attr('disabled', true);
            $('table[name="table_catalog_by_po"]').addClass('d-none');

            $('#submit_btn_po').attr('hidden', true);

          } else {
            $(input).addClass('is-valid');
            $('table[name="table_catalog_by_po"]').removeClass('d-none');

            $('#submit_btn_po').removeAttr('hidden');

            catalog_list_by_po();
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });

    }
  }

  function catalog_list_by_po(){
    var po_number = $('input[name="po_number"]').val();

    $.ajax({
      url: "<?= base_url() ?>receiving/catalog_list_by_po",
      type: "POST",
      data: {
        'po': po_number
      },
      success: function(data){
        var hasil = JSON.parse(data);
        var html = '';
        var i;
        for(i=0; i<hasil.length; i++){
            html += '<tr>'+
                      '<td name="checkbox_material"><input type="hidden" name="id_checkbox[]" value="0"><input type="checkbox" name="select_material[]" value="'+hasil[i].material_catalog_id+'" onclick="select_material_click(this, '+ i +')" data-toggle="collapse" data-target="#accordion' + i + '" class="clickable"></td>'+
                      '<td><input type="hidden" name="po_number" value="' +hasil[i].po_number+ '">'+hasil[i].po_number+'</td>'+
                      '<td>'+hasil[i].description+'</td>'+
                      '<td>'+hasil[i].vendor_name+'</td>'+
                      '<td>'+hasil[i].qty_request+'</td>'+
                      '<td>'+hasil[i].qty_receiving +'</td>'+
                      '<td>'+hasil[i].uom_name +'</td>'+
                      '<td>'+hasil[i].status_receiving+'</td>'+
                    '</tr>'+

                    '<tr class="table table-borderless text-left" name="tr_detail_' + i + '">' +
                      '<td colspan="20">' +
                        '<div id="accordion' + i + '" class="collapse targets">' +
                        
                          '<div class="form-row">' + 
                            '<input type="hidden" name="total_qty_po[]" value="' + hasil[i].qty_request +'" disabled>' + 
                            '<input type="hidden" name="total_receiving[]" value="' + hasil[i].qty_receiving +'" disabled>' + 
                            '<input type="hidden" name="vendor_id[]" value="' + hasil[i].vendor_id +  '" disabled>' +
                            '<input type="hidden" name="po_id[]" value="' + hasil[i].id_po +  '" disabled>' +
                            
                            '<div class="form-group col-md-6">' +
                              '<label>QTY</label>' + 
                              '<input type="number" class="form-control" name="qty[]" oninput="get_val_qty(this, ' + i + ')" placeholder="Fill Up Qty" disabled>' + 
                              '<input type="hidden" name="length_m[]" value="' + hasil[i].length +'" disabled>' + 
                              '<label class="text-danger d-none" id="qty_alert_' + i + '"></label><br>' + 

                              '<div class="form-check form-check-inline p-1 d-none">' +
                                '<input class="form-check-input" type="radio" name="shortage_categories['+ hasil[i].material_catalog_id +']" value="0" style="width: 17px; height: 17px" checked>' +
                              '</div>' +

                              '<div class="form-check form-check-inline p-1 shortage_categories_' + i  +' d-none">' +
                                '<input class="form-check-input" type="radio" name="shortage_categories['+ hasil[i].material_catalog_id +']" value="1" style="width: 17px; height: 17px">' +
                                '<label class="form-check-label">OS&D Shortage</label>' +
                              '</div>' +

                              '<div class="form-check form-check-inline p-1 shortage_categories_' + i  +' d-none">' + 
                                '<input class="form-check-input" type="radio" name="shortage_categories['+ hasil[i].material_catalog_id +']" value="2" style="width: 17px; height: 17px">' +
                                '<label class="form-check-label">Partial Delivery</label>' +
                              '</div>' +
                            '</div>' + 

                            '<div class="form-group col-md-6">' + 
                              '<label>BRAND</label>' + 
                              '<input type="text" class="form-control" name="brand[]" placeholder="Fill Up Brand" disabled>' +
                            '</div>' + 

                          '</div>' +    

                          '<div class="form-row">' + 
                            '<div class="form-group col-md-6">' +
                              '<label>COUNTRY ORIGIN</label>' + 
                              '<input type="text" class="form-control" name="country_origin[]" placeholder="Fill Up Country Origin" disabled>' + 
                            '</div>' +

                            '<div class="form-group col-md-6">' + 
                              '<label>COLOR CODE</label>' + 
                              '<input type="text" class="form-control" name="color_code[]" placeholder="Fill Up Color Code" disabled>' + 
                            '</div>' + 
                          '</div>' +                    

                        '</div>' +
                      '</td>' +
                    '</tr>';
        }

        $('#catalog_by_po').html(html);
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });

  }

  // VENDOR --------------------------------------------------------------
  function vendor_autocomplete(input){
    $(input).autocomplete({
      source: "<?php echo base_url(); ?>receiving/vendor_autocomplete",
      autoFocus: true,
      classes: {
        "ui-autocomplete": "highlight"
      }
    });
  }

  function check_vendor(input){
    var vendor = $(input).val();

    if(vendor == '' || vendor.includes('Found')){
      $(input).addClass('is-invalid');
    } else {

      $.ajax({
        url: "<?= base_url() ?>receiving/vendor_check",
        type: "POST",
        data: { 'vendor': vendor },
        success: function(data){
          $(input).addClass('is-alid');
        },
        error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
        }
      });

      $(input).addClass('is-valid');
    }
  }
  //-----------------------------------------------------------------------

  function get_val_qty(input, param){
    var val_qty = $(input).val();
    var length_m = $('#accordion' +param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="length_m[]"]').val();
    var input_uom_length = $('#accordion' +param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="uom_length[]"]');

    var total_qty_po = $('#accordion' + param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="total_qty_po[]"]').val();
    var total_receiving = $('#accordion' + param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="total_receiving[]"]').val();

    if(val_qty){
      var uom_length = parseInt(val_qty) * parseInt(length_m);
      $(input_uom_length).val(uom_length);

      var total_all = parseInt(val_qty) + parseInt(total_receiving);

      //CHECK QTY IS SHORTAGE OR OVER ---------
      if(parseInt(total_all) > parseInt(total_qty_po)){
        $('#qty_alert_' + param).removeClass('d-none');
        $('#qty_alert_' + param).text('* Qty is Over');
        $('.shortage_categories_' + param).addClass('d-none');
      } else if(parseInt(total_all) < parseInt(total_qty_po)){
        $('#qty_alert_' + param).removeClass('d-none');
        $('#qty_alert_' + param).text('* Qty is Shortage');
        $('.shortage_categories_' + param).removeClass('d-none');
      } else{
        $('#qty_alert_' + param).addClass('d-none');
        $('.shortage_categories_' + param).addClass('d-none');
      }
      //---------------------------------------

    } else {
      $(input_uom_length).val('');
      $('#qty_alert_' + param).addClass('d-none');
      $('.shortage_categories_' + param).addClass('d-none');
    }

  }

  function selectall_material_click(input){
    $(input).closest('table').find('td input:checkbox').prop('checked', input.checked);


    if($(input).prop("checked") == true){
      // $('button[name="submit_detail"]').removeAttr('disabled');      
      $('.targets').show();

      $('input[name="project_id[]"]').removeAttr('disabled');
      $('input[name="certification[]"]').removeAttr('disabled');
      $('input[name="total_qty_po[]"]').removeAttr('disabled');
      $('input[name="total_receiving[]"]').removeAttr('disabled');
      $('input[name="vendor_id[]"]').removeAttr('disabled');
      $('input[name="ceq[]"]').removeAttr('disabled');
      $('input[name="discipline[]"]').removeAttr('disabled');

      $('input[name="delivery_condition[]"]').removeAttr('disabled');
      $('input[name="spec_grade[]"]').removeAttr('disabled');
      $('input[name="spec_category[]"]').removeAttr('disabled');
      $('input[name="plate_or_tag_no[]"]').removeAttr('disabled');
      $('input[name="uom_length[]"]').removeAttr('disabled');
      $('input[name="country_origin[]"]').removeAttr('disabled');
      $('input[name="brand[]"]').removeAttr('disabled');
      $('input[name="heat_or_lot_no[]"]').removeAttr('disabled');
      $('input[name="mill_cert_no[]"]').removeAttr('disabled');
      $('input[name="date_manufacturing[]"]').removeAttr('disabled');
      $('input[name="color_code[]"]').removeAttr('disabled');
      $('input[name="qty[]"]').removeAttr('disabled');
      $('input[name="po_id[]"]').removeAttr('disabled');
      
    }
    else if($(input).prop("checked") == false){

      $('.targets').hide();
      // $('.targets')addClass('collapse');

      $('input[name="project_id[]"]').attr('disabled', true);
      $('input[name="certification[]"]').attr('disabled', true);
      $('input[name="total_qty_po[]"]').attr('disabled', true);
      $('input[name="total_receiving[]"]').attr('disabled', true);
      $('input[name="vendor_id[]"]').attr('disabled', true);
      $('input[name="ceq[]"]').attr('disabled', true);
      $('input[name="discipline[]"]').attr('disabled', true);

      $('input[name="delivery_condition[]"]').attr('disabled', true);
      $('input[name="spec_grade[]"]').attr('disabled', true);
      $('input[name="spec_category[]"]').attr('disabled', true);
      $('input[name="plate_or_tag_no[]"]').attr('disabled', true);
      $('input[name="uom_length[]"]').attr('disabled', true);
      $('input[name="country_origin[]"]').attr('disabled', true);
      $('input[name="brand[]"]').attr('disabled', true);
      $('input[name="heat_or_lot_no[]"]').attr('disabled', true);
      $('input[name="mill_cert_no[]"]').attr('disabled', true);
      $('input[name="date_manufacturing[]"]').attr('disabled', true);
      $('input[name="color_code[]"]').attr('disabled', true);
      $('input[name="qty[]"]').attr('disabled', true);
      $('input[name="po_id[]"]').attr('disabled', true);
    }

    checkbox_check();
  }

  function checkbox_check(){
    var checkbox_check = $('input[name="select_material[]"]:checked').length;
    
    if(checkbox_check > 0){
      $('#submit_btn_po').removeAttr('disabled');
    } else {
      $('#submit_btn_po').attr('disabled', true);
    }
    
  }

  function select_material_click(input, param){
    
    var checkbox_material   = $(input).closest('tr').find('td[name="checkbox_material"]').find('input[name="id_checkbox[]"]');

    var project_id          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="project_id[]"]');
    var certification       = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="certification[]"]');
    var total_qty_po        = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="total_qty_po[]"]');
    var total_receiving     = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="total_receiving[]"]');
    var vendor_name         = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="vendor_id[]"]');
    var ceq                 = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="ceq[]"]');
    var discipline          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="discipline[]"]');
    var delivery_condition  = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="delivery_condition[]"]');
    var spec_grade          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="spec_grade[]"]');
    var spec_category       = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="spec_category[]"]');
    var plate_or_tag_no     = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="plate_or_tag_no[]"]');
    var uom_length          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="uom_length[]"]');
    var country_origin      = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="country_origin[]"]');
    var brand               = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="brand[]"]');
    var heat_or_lot_no      = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="heat_or_lot_no[]"]');
    var mill_cert_no        = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="mill_cert_no[]"]');
    var date_manufacturing  = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="date_manufacturing[]"]');
    var color_code          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="color_code[]"]');
    var qty                 = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="qty[]"]');
    var po_id               = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="po_id[]"]');

    if($(input).prop("checked") == true){

      $('#accordion' + param).show();

      $(project_id).removeAttr('disabled').addClass('detail_required');
      $(certification).removeAttr('disabled').addClass('detail_required');
      $(total_qty_po).removeAttr('disabled').addClass('detail_required');
      $(total_receiving).removeAttr('disabled').addClass('detail_required');
      $(vendor_name).removeAttr('disabled').addClass('detail_required');
      $(ceq).removeAttr('disabled').addClass('detail_required');
      $(discipline).removeAttr('disabled').addClass('detail_required');

      $(delivery_condition).removeAttr('disabled').addClass('detail_required');
      $(spec_grade).removeAttr('disabled').addClass('detail_required');
      $(spec_category).removeAttr('disabled').addClass('detail_required');
      $(plate_or_tag_no).removeAttr('disabled').addClass('detail_required');
      $(uom_length).removeAttr('disabled').addClass('detail_required');
      $(country_origin).removeAttr('disabled').addClass('detail_required');
      $(brand).removeAttr('disabled').addClass('detail_required');
      $(heat_or_lot_no).removeAttr('disabled').addClass('detail_required');
      $(mill_cert_no).removeAttr('disabled').addClass('detail_required');
      $(date_manufacturing).removeAttr('disabled').addClass('detail_required');
      $(color_code).removeAttr('disabled').addClass('detail_required');
      $(qty).removeAttr('disabled').addClass('detail_required');
      $(po_id).removeAttr('disabled').addClass('detail_required');

      $(checkbox_material).val(1);
      // $('button[id="submit_btn_po"]').removeAttr('hidden');

    } 
    else if($(input).prop("checked") == false){

      $('#accordion' + param).hide();

      $(project_id).attr('disabled', true).removeClass('detail_required');
      $(certification).attr('disabled', true).removeClass('detail_required');
      $(total_qty_po).attr('disabled', true).removeClass('detail_required');
      $(total_receiving).attr('disabled', true).removeClass('detail_required');
      $(vendor_name).attr('disabled', true).removeClass('detail_required');
      $(ceq).attr('disabled', true).removeClass('detail_required');
      $(discipline).attr('disabled', true).removeClass('detail_required');

      $(delivery_condition).attr('disabled', true).removeClass('detail_required');
      $(spec_grade).attr('disabled', true).removeClass('detail_required');
      $(spec_category).attr('disabled', true).removeClass('detail_required');
      $(plate_or_tag_no).attr('disabled', true).removeClass('detail_required');
      $(uom_length).attr('disabled', true).removeClass('detail_required');
      $(country_origin).attr('disabled', true).removeClass('detail_required');
      $(brand).attr('disabled', true).removeClass('detail_required');
      $(heat_or_lot_no).attr('disabled', true).removeClass('detail_required');
      $(mill_cert_no).attr('disabled', true).removeClass('detail_required');
      $(date_manufacturing).attr('disabled', true).removeClass('detail_required');
      $(color_code).attr('disabled', true).removeClass('detail_required');
      $(qty).attr('disabled', true).removeClass('detail_required');
      $(po_id).attr('disabled', true).removeClass('detail_required');

      $(checkbox_material).val(0);
      // $('button[id="submit_btn_po"]').attr('hidden', true);
    }

    checkbox_check();

  }


</script>


<!-- RECEIVING ATTACHMENT -->
<script type="text/javascript">

  Dropzone.autoDiscover = false;
  $(".dropzone").dropzone({
    acceptedFiles: ".pdf",
    complete: function(file){
      this.removeFile(file);
      table_receiving_attachment();
    }
  });

  table_receiving_attachment();

  function table_receiving_attachment(){
    var receiving_id = $('input[name="id_receiving_master"]').val();

    $.ajax({
      url: "<?= base_url() ?>receiving/receiving_attachment_list",
      type: "POST",
      data: {
        'receiving_id': receiving_id
      },
      success: function(data){
        var hasil = JSON.parse(data);
        var html = '';
        var i;
        for(i=0; i<hasil.length; i++){
            html += '<tr>' +
                      '<td><img src="<?= base_url() ?>img/pdf.svg" style="width: 25px;"></td>' +
                      '<td><a href="<?= base_url() ?>file/receiving/'+receiving_id+'/'+hasil[i].attachment+'" target="_blank">'+hasil[i].attachment+'</a></td>' +
                      '<td class="text-center"><button type="button" class="btn btn-danger" onclick="delete_action(\'receiving_attachment\', ' +hasil[i].id+ ')"><i class="fas fa-trash"></i></button></td>' +
                    '</tr>';
        }

        $('#receiving_attachment_body').html(html);
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });
  }

</script>