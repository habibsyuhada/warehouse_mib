<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>receiving/receiving_add_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Packing List Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="do_pl" placeholder="Packing List Number" onblur="check_do_pl(this)" required>
                  <label class="d-none" id="label_alert_do_pl" style="color: red;"></label>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">ATA</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control datepicker" name="ata" placeholder="ATA" required>                
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">PO Number</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">PO Number</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" placeholder="PO Number" name="po_number" onfocus="$(this).removeClass('is-valid is-invalid')" onblur="check_po(this)" required>
                </div>
              </div>

              <table class="table table-hover text-center d-none" name="table_catalog_by_po">
                <thead class="bg-green-smoe text-white">
                  <tr>
                    <th rowspan="2" style="vertical-align: middle;">
                      <input type="checkbox" name="selectall_material" onclick="selectall_material_click(this)" data-toggle="collapse" data-target="targets" class="clickable">
                    </th>
                    <th rowspan="2" style="vertical-align: middle;">PO NUMBER</th>
                    <th rowspan="2" style="vertical-align: middle;">DESCRIPTION</th>
                    <th rowspan="2" style="vertical-align: middle;">VENDOR</th>
                    <th colspan="2" style="vertical-align: middle;">QTY</th>
                    <th rowspan="2" style="vertical-align: middle;">STATUS</th>
                  </tr>
                  <tr>
                    <th style="vertical-align: middle;">REQUEST</th>
                    <th style="vertical-align: middle;">RECEIVING</th>
                  </tr>
                </thead>
                <tbody id="catalog_by_po">
                  
                </tbody>
              </table>

            </div>
          </div>

          <div class="text-right mt-3">
            
            <?php if($read_permission[60] == 1){ ?>
                <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <?php } ?>

            <a href="<?php echo base_url();?>receiving" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>

          </div>
        </div>
      </div>

    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function check_do_pl(input) {
    var do_pl = $(input).val();

    if(do_pl == ''){
        $(input).addClass('is-invalid');
        $('#label_alert_do_pl').removeClass('d-none');
        $('#label_alert_do_pl').text('Packing List Number is Required');
        $('button[name=submit]').prop("disabled", true);    
    } else {
      $.ajax({
        url: "<?php echo base_url();?>receiving/receiving_check_do_pl/",
        type: "post",
        data: {
          do_pl: do_pl,
          receiving_id: ''
        },
        success: function(data) {
          var data = JSON.parse(data);

          if(data.hasil > 0){ // KALAU DUPLICATE ----
            $(input).addClass('is-invalid');
            $(input).removeClass('is-valid');

            $('#label_alert_do_pl').removeClass('d-none');
            $('#label_alert_do_pl').text('Duplicate Packing List Number');

            $('button[name=submit]').attr("disabled", true);
            
          } else{ // KALAU GAK DUPLICATE -----
            $(input).removeClass('is-invalid');
            $(input).addClass('is-valid');

            $('#label_alert_do_pl').addClass('d-none');

            $('button[name=submit]').removeAttr("disabled");
          }

        }
      });

    }
  }

  $('input[name="po_number"]').autocomplete({
    source: "<?php echo base_url(); ?>receiving/po_autocomplete",
    autoFocus: true,
    classes: {
      "ui-autocomplete": "highlight"
    }
  });

  function check_po(input){
    var po_number = $(input).val();

    if(po_number == '' || po_number.includes('Found') == true){
      $(input).addClass('is-invalid');
      // $('button[name="submit_detail"]').attr('disabled', true);
      $('#submit_btn_po').attr('hidden', true);
    } else {
      $.ajax({
        url: "<?= base_url() ?>receiving/receiving_check_po",
        type: "POST",
        data: {
          'po': po_number
        },
        success: function(data){
          var hasil = JSON.parse(data);
          if(hasil.hasil == 0){
            $(input).addClass('is-invalid');

            // $('button[name="submit_detail"]').attr('disabled', true);
            $('table[name="table_catalog_by_po"]').addClass('d-none');

            $('#submit_btn_po').attr('hidden', true);

          } else {
            $(input).addClass('is-valid');
            $('table[name="table_catalog_by_po"]').removeClass('d-none');

            $('#submit_btn_po').removeAttr('hidden');

            catalog_list_by_po();
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
      });

    }
  }

  function catalog_list_by_po(){
    var po_number = $('input[name="po_number"]').val();

    $.ajax({
      url: "<?= base_url() ?>receiving/catalog_list_by_po",
      type: "POST",
      data: {
        'po': po_number
      },
      success: function(data){
        var hasil = JSON.parse(data);
        var html = '';
        var i;
        for(i=0; i<hasil.length; i++){
            html += '<tr>'+
                      '<td name="checkbox_material"><input type="hidden" name="id_checkbox[]" value="0"><input type="checkbox" name="select_material[]" value="'+hasil[i].id_po+'" onclick="select_material_click(this, '+ i +')" data-toggle="collapse" data-target="#accordion' + i + '" class="clickable"></td>'+
                      '<td><input type="hidden" name="po_number" value="' +hasil[i].po_number+ '">'+hasil[i].po_number+'</td>'+
                      '<td>'+hasil[i].description+'</td>'+
                      '<td>'+hasil[i].vendor_name+'</td>'+
                      '<td>'+hasil[i].qty_request+'</td>'+
                      '<td>'+hasil[i].qty_receiving +'</td>'+
                      // '<td>'+hasil[i].uom_name +'</td>'+
                      '<td>'+hasil[i].status_receiving+'</td>'+
                    '</tr>'+

                    '<tr class="table table-borderless text-left" name="tr_detail_' + i + '">' +
                      '<td colspan="20">' +
                        '<div id="accordion' + i + '" class="collapse targets">' +
                        
                          '<div class="form-row">' + 
                            '<input type="hidden" name="total_qty_po[]" value="' + hasil[i].qty_request +'" disabled>' + 
                            '<input type="hidden" name="total_receiving[]" value="' + hasil[i].qty_receiving +'" disabled>' + 
                            '<input type="hidden" name="vendor_id[]" value="' + hasil[i].vendor_id +  '" disabled>' +
                            '<input type="hidden" name="po_id[]" value="' + hasil[i].id_po +  '" disabled>' +
                            
                            '<div class="form-group col-md-6">' +
                              '<label>QTY</label>' + 
                              '<input type="number" class="form-control" name="qty[]" oninput="get_val_qty(this, ' + i + ')" placeholder="Fill Up Qty" disabled>' + 
                              '<input type="hidden" name="length_m[]" value="' + hasil[i].length +'" disabled>' + 
                              '<label class="text-danger d-none" id="qty_alert_' + i + '"></label><br>' + 

                              '<div class="form-check form-check-inline p-1 d-none">' +
                                '<input class="form-check-input" type="radio" name="shortage_categories['+ hasil[i].material_catalog_id +']" value="0" style="width: 17px; height: 17px" checked>' +
                              '</div>' +

                              '<div class="form-check form-check-inline p-1 shortage_categories_' + i  +' d-none">' +
                                '<input class="form-check-input" type="radio" name="shortage_categories['+ hasil[i].material_catalog_id +']" value="1" style="width: 17px; height: 17px">' +
                                '<label class="form-check-label">OS&D Shortage</label>' +
                              '</div>' +

                              '<div class="form-check form-check-inline p-1 shortage_categories_' + i  +' d-none">' + 
                                '<input class="form-check-input" type="radio" name="shortage_categories['+ hasil[i].material_catalog_id +']" value="2" style="width: 17px; height: 17px">' +
                                '<label class="form-check-label">Partial Delivery</label>' +
                              '</div>' +
                            '</div>' + 

                            '<div class="form-group col-md-6">' + 
                              '<label>BRAND</label>' + 
                              '<input type="text" class="form-control" name="brand[]" placeholder="Fill Up Brand" disabled>' +
                            '</div>' + 

                          '</div>' +    

                          '<div class="form-row">' + 
                            '<div class="form-group col-md-6">' +
                              '<label>COUNTRY ORIGIN</label>' + 
                              '<input type="text" class="form-control" name="country_origin[]" placeholder="Fill Up Country Origin" disabled>' + 
                            '</div>' +

                            '<div class="form-group col-md-6">' + 
                              '<label>COLOR CODE</label>' + 
                              '<input type="text" class="form-control" name="color_code[]" placeholder="Fill Up Color Code" disabled>' + 
                            '</div>' + 
                          '</div>' +                    

                        '</div>' +
                      '</td>' +
                    '</tr>';
        }

        $('#catalog_by_po').html(html);
      },
      error: function(jqXHR, textStatus, errorThrown) {
         console.log(textStatus, errorThrown);
      }
    });

  }

  function get_val_qty(input, param){
    var val_qty = $(input).val();
    var length_m = $('#accordion' +param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="length_m[]"]').val();
    var input_uom_length = $('#accordion' +param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="uom_length[]"]');

    var total_qty_po = $('#accordion' + param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="total_qty_po[]"]').val();
    var total_receiving = $('#accordion' + param).closest('tr[name="tr_detail_' + param + '"]').find('input[name="total_receiving[]"]').val();

    if(val_qty){
      var uom_length = parseInt(val_qty) * parseInt(length_m);
      $(input_uom_length).val(uom_length);

      var total_all = parseInt(val_qty) + parseInt(total_receiving);

      //CHECK QTY IS SHORTAGE OR OVER ---------
      if(parseInt(total_all) > parseInt(total_qty_po)){
        $('#qty_alert_' + param).removeClass('d-none');
        $('#qty_alert_' + param).text('* Qty is Over');
        $('.shortage_categories_' + param).addClass('d-none');
      } else if(parseInt(total_all) < parseInt(total_qty_po)){
        $('#qty_alert_' + param).removeClass('d-none');
        $('#qty_alert_' + param).text('* Qty is Shortage');
        $('.shortage_categories_' + param).removeClass('d-none');
      } else{
        $('#qty_alert_' + param).addClass('d-none');
        $('.shortage_categories_' + param).addClass('d-none');
      }
      //---------------------------------------

    } else {
      $(input_uom_length).val('');
      $('#qty_alert_' + param).addClass('d-none');
      $('.shortage_categories_' + param).addClass('d-none');
    }

  }

  function selectall_material_click(input){
    $(input).closest('table').find('td input:checkbox').prop('checked', input.checked);


    if($(input).prop("checked") == true){
      // $('button[name="submit_detail"]').removeAttr('disabled');      
      $('.targets').show();

      $('input[name="project_id[]"]').removeAttr('disabled');
      $('input[name="certification[]"]').removeAttr('disabled');
      $('input[name="total_qty_po[]"]').removeAttr('disabled');
      $('input[name="total_receiving[]"]').removeAttr('disabled');
      $('input[name="vendor_id[]"]').removeAttr('disabled');
      $('input[name="ceq[]"]').removeAttr('disabled');
      $('input[name="discipline[]"]').removeAttr('disabled');

      $('input[name="delivery_condition[]"]').removeAttr('disabled');
      $('input[name="spec_grade[]"]').removeAttr('disabled');
      $('input[name="spec_category[]"]').removeAttr('disabled');
      $('input[name="plate_or_tag_no[]"]').removeAttr('disabled');
      $('input[name="uom_length[]"]').removeAttr('disabled');
      $('input[name="country_origin[]"]').removeAttr('disabled');
      $('input[name="brand[]"]').removeAttr('disabled');
      $('input[name="heat_or_lot_no[]"]').removeAttr('disabled');
      $('input[name="mill_cert_no[]"]').removeAttr('disabled');
      $('input[name="date_manufacturing[]"]').removeAttr('disabled');
      $('input[name="color_code[]"]').removeAttr('disabled');
      $('input[name="qty[]"]').removeAttr('disabled');
      $('input[name="po_id[]"]').removeAttr('disabled');
      
    }
    else if($(input).prop("checked") == false){

      $('.targets').hide();
      // $('.targets')addClass('collapse');

      $('input[name="project_id[]"]').attr('disabled', true);
      $('input[name="certification[]"]').attr('disabled', true);
      $('input[name="total_qty_po[]"]').attr('disabled', true);
      $('input[name="total_receiving[]"]').attr('disabled', true);
      $('input[name="vendor_id[]"]').attr('disabled', true);
      $('input[name="ceq[]"]').attr('disabled', true);
      $('input[name="discipline[]"]').attr('disabled', true);

      $('input[name="delivery_condition[]"]').attr('disabled', true);
      $('input[name="spec_grade[]"]').attr('disabled', true);
      $('input[name="spec_category[]"]').attr('disabled', true);
      $('input[name="plate_or_tag_no[]"]').attr('disabled', true);
      $('input[name="uom_length[]"]').attr('disabled', true);
      $('input[name="country_origin[]"]').attr('disabled', true);
      $('input[name="brand[]"]').attr('disabled', true);
      $('input[name="heat_or_lot_no[]"]').attr('disabled', true);
      $('input[name="mill_cert_no[]"]').attr('disabled', true);
      $('input[name="date_manufacturing[]"]').attr('disabled', true);
      $('input[name="color_code[]"]').attr('disabled', true);
      $('input[name="qty[]"]').attr('disabled', true);
      $('input[name="po_id[]"]').attr('disabled', true);
    }

    checkbox_check();
  }

  function checkbox_check(){
    var checkbox_check = $('input[name="select_material[]"]:checked').length;
    
    if(checkbox_check > 0){
      $('#submit_btn_po').removeAttr('disabled');
    } else {
      $('#submit_btn_po').attr('disabled', true);
    }
    
  }

  function select_material_click(input, param){
    
    var checkbox_material   = $(input).closest('tr').find('td[name="checkbox_material"]').find('input[name="id_checkbox[]"]');

    var project_id          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="project_id[]"]');
    var certification       = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="certification[]"]');
    var total_qty_po        = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="total_qty_po[]"]');
    var total_receiving     = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="total_receiving[]"]');
    var vendor_name         = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="vendor_id[]"]');
    var ceq                 = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="ceq[]"]');
    var discipline          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="discipline[]"]');
    var delivery_condition  = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="delivery_condition[]"]');
    var spec_grade          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="spec_grade[]"]');
    var spec_category       = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="spec_category[]"]');
    var plate_or_tag_no     = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="plate_or_tag_no[]"]');
    var uom_length          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="uom_length[]"]');
    var country_origin      = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="country_origin[]"]');
    var brand               = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="brand[]"]');
    var heat_or_lot_no      = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="heat_or_lot_no[]"]');
    var mill_cert_no        = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="mill_cert_no[]"]');
    var date_manufacturing  = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="date_manufacturing[]"]');
    var color_code          = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="color_code[]"]');
    var qty                 = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="qty[]"]');
    var po_id               = $('#accordion' +param).closest('tr[name="tr_detail_' + param +'"]').find('input[name="po_id[]"]');

    if($(input).prop("checked") == true){

      $('#accordion' + param).show();

      $(project_id).removeAttr('disabled').addClass('detail_required');
      $(certification).removeAttr('disabled').addClass('detail_required');
      $(total_qty_po).removeAttr('disabled').addClass('detail_required');
      $(total_receiving).removeAttr('disabled').addClass('detail_required');
      $(vendor_name).removeAttr('disabled').addClass('detail_required');
      $(ceq).removeAttr('disabled').addClass('detail_required');
      $(discipline).removeAttr('disabled').addClass('detail_required');

      $(delivery_condition).removeAttr('disabled').addClass('detail_required');
      $(spec_grade).removeAttr('disabled').addClass('detail_required');
      $(spec_category).removeAttr('disabled').addClass('detail_required');
      $(plate_or_tag_no).removeAttr('disabled').addClass('detail_required');
      $(uom_length).removeAttr('disabled').addClass('detail_required');
      $(country_origin).removeAttr('disabled').addClass('detail_required');
      $(brand).removeAttr('disabled').addClass('detail_required');
      $(heat_or_lot_no).removeAttr('disabled').addClass('detail_required');
      $(mill_cert_no).removeAttr('disabled').addClass('detail_required');
      $(date_manufacturing).removeAttr('disabled').addClass('detail_required');
      $(color_code).removeAttr('disabled').addClass('detail_required');
      $(qty).removeAttr('disabled').addClass('detail_required');
      $(po_id).removeAttr('disabled').addClass('detail_required');

      $(checkbox_material).val(1);
      // $('button[id="submit_btn_po"]').removeAttr('hidden');

    } 
    else if($(input).prop("checked") == false){

      $('#accordion' + param).hide();

      $(project_id).attr('disabled', true).removeClass('detail_required');
      $(certification).attr('disabled', true).removeClass('detail_required');
      $(total_qty_po).attr('disabled', true).removeClass('detail_required');
      $(total_receiving).attr('disabled', true).removeClass('detail_required');
      $(vendor_name).attr('disabled', true).removeClass('detail_required');
      $(ceq).attr('disabled', true).removeClass('detail_required');
      $(discipline).attr('disabled', true).removeClass('detail_required');

      $(delivery_condition).attr('disabled', true).removeClass('detail_required');
      $(spec_grade).attr('disabled', true).removeClass('detail_required');
      $(spec_category).attr('disabled', true).removeClass('detail_required');
      $(plate_or_tag_no).attr('disabled', true).removeClass('detail_required');
      $(uom_length).attr('disabled', true).removeClass('detail_required');
      $(country_origin).attr('disabled', true).removeClass('detail_required');
      $(brand).attr('disabled', true).removeClass('detail_required');
      $(heat_or_lot_no).attr('disabled', true).removeClass('detail_required');
      $(mill_cert_no).attr('disabled', true).removeClass('detail_required');
      $(date_manufacturing).attr('disabled', true).removeClass('detail_required');
      $(color_code).attr('disabled', true).removeClass('detail_required');
      $(qty).attr('disabled', true).removeClass('detail_required');
      $(po_id).attr('disabled', true).removeClass('detail_required');

      $(checkbox_material).val(0);
      // $('button[id="submit_btn_po"]').attr('hidden', true);
    }

    checkbox_check();

  }
</script>