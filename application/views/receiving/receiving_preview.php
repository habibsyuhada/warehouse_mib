<?php 
  // print_r($spec_category_list);exit;
?>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>receiving/receiving_detail_import_process">
    
    <input type="hidden" name="master_receiving_id" value="<?= $master_receiving_id ?>">

    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-hover text-center">
                <thead class="bg-success text-white">
                  <tr>
                    <th>Material Catalog</th>
                    <th>Unique Number</th>
                    <th>PO Item</th>
                    <th>Heat Number</th>
                    <th>Mill Certificate</th>
                    <th>Country Origin</th>
                    <th>Brand</th>
                    <th>UoM</th>
                    <th>Color Code</th>
                    <th>Valuta</th>
                    <th>Area</th>
                    <th>Location</th>
                    <th>Type</th>
                    <th>Spec</th>
                    <th>Spec Category</th>
                    <th>Plate/Tag No</th>
                    <th>Supplier Name</th>
                    <th>CEQ</th>
                    <th>Date Manufacturing</th>
                    <th>Category</th>
                    <th>Status Receiving</th>
                    <th>Qty</th>
                    <th>Unit Weight (Length, Sheet)</th>
                    <th>Unit Weight (mm, sqm)</th>
                    <th>Total Weight (Length, Sheet)</th>
                    <th>Total Weight (mm, sqm)</th>
                    <th>Value (mm, sqm)</th>
                    <th>Total Value (Length, Sheet)</th>
                    <th>Total Value (mm,sqm)</th>
                    <th>Remarks</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $no=1; 

                    foreach($sheet as $row):

                      if($no > 1): 

                        $status = '';
                        $disabled = 0;

                        //MATERIAL CATALOG ------------------------------------------------------------
                        $material_catalog = $row['material_catalog'];
                        $id_material_catalog = $row['id_material_catalog'];

                        if(!in_array($row['A'], $material_catalog_list)){
                          $status = 'Material Catalog Not Found';
                          $disabled = 2;
                          $material_catalog = $row['A'];
                          $id_material_catalog = '';
                        }
                        //-----------------------------------------------------------------------------

                        //UNIQUE NUMBER ---------------------------------------------------------------
                        if(in_array($row['B'], $unique_no_list)){
                          $status = 'Unique Number Already Exist';
                          $disabled = 2;
                          $mto_category = $row['B'];
                        }
                        //-----------------------------------------------------------------------------

                        //AREA ------------------------------------------------------------------------
                        $name_area = $row['name_area'];
                        $id_area = $row['id_area'];

                        if(!in_array($row['K'], $area_list)){
                          $status = 'Area Not Found';
                          $disabled = 2;
                          
                          $name_area = $row['K'];
                          $id_area = '';

                        }
                        //-----------------------------------------------------------------------------

                        //SPEC CATEGORY ---------------------------------------------------------------
                        $name_spec_category = $row['name_spec_category'];
                        $id_spec_category = $row['id_spec_category'];

                        if(!in_array($row['O'], $spec_category_list)){
                          $status = 'Spec Category Not Found';
                          $disabled = 2;

                          $name_spec_category = $row['O'];
                          $id_spec_category = '';
                        }
                        //-----------------------------------------------------------------------------

                        if($row['A'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td class="align-middle">
                      <input type="text" class="form-control" value="<?= $material_catalog ?>" disabled>
                      <input type="hidden" name="material_catalog[]" class="form-control" value="<?= $id_material_catalog ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle"><input type="text" name="unique_number[]" class="form-control" value="<?= $row['B'] ?>" readonly <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="po_item[]" class="form-control" value="<?= $row['C'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="heat_no[]" class="form-control" value="<?= $row['D'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="mill_certificate[]" class="form-control" value="<?= $row['E'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="country_origin[]" class="form-control" value="<?= $row['F'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="brand[]" class="form-control" value="<?= $row['G'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="uom[]" class="form-control" value="<?= $row['H'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="color_code[]" class="form-control" value="<?= $row['I'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="valuta[]" class="form-control" value="<?= $row['J'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle">
                      <input type="text" class="form-control" value="<?= $name_area ?>" readonly <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="hidden" name="area[]" class="form-control" value="<?= $id_area ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle"><input type="text" name="location[]" class="form-control" value="<?= $row['L'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="type[]" class="form-control" value="<?= $row['M'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="spec[]" class="form-control" value="<?= $row['N'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle">
                      <input type="hidden" name="spec_category[]" value="<?= $id_spec_category ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                      <input type="text" class="form-control" value="<?= $name_spec_category ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle"><input type="text" name="plate_or_tag_no[]" class="form-control" value="<?= $row['P'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="supplier_name[]" class="form-control" value="<?= $row['Q'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="ceq[]" class="form-control" value="<?= $row['R'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="date_manufacturing[]" class="form-control" value="<?= $row['S'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="category[]" class="form-control" value="<?= $row['T'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>

                    <td class="align-middle"><input type="text" name="status_receiving[]" class="form-control" value="<?= $row['U'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="qty[]" class="form-control" value="<?= $row['V'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="unit_weight_length_sheet[]" class="form-control" value="<?= $row['W'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="unit_weight_mm_sqm[]" class="form-control" value="<?= $row['X'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="total_weight_length_sheet[]" class="form-control" value="<?= $row['Y'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="total_weight_mm_sqm[]" class="form-control" value="<?= $row['Z'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="value_mm_sqm[]" class="form-control" value="<?= $row['AA'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="total_value_length_sheet[]" class="form-control" value="<?= $row['AB'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="total_value_mm_sqm[]" class="form-control" value="<?= $row['AC'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><input type="text" name="remarks[]" class="form-control" value="<?= $row['AD'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>></td>
                    <td class="align-middle"><?php echo $status ?></td>
                  </tr>
                  <?php  } endif; ?>
                  <?php $no++; endforeach; ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>receiving_detail/<?= strtr($this->encryption->encrypt($master_receiving_id), '+=/', '.-~') ?>" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->