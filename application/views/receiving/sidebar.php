<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
  <ul class="list-unstyled components">
    <?php if($read_permission[59] == 1){ ?>
    <li>
      <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-truck"></i> Receiving
      </a>
      <ul class="list-unstyled" id="homeSubmenu2">
        <?php if($read_permission[60] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>receiving_add"><i class="fas fa-plus"></i> &nbsp; Add Receiving</a>
        </li>
        <?php } ?>
        <li>
          <a href="<?php echo base_url();?>receiving_list/waiting"><i class="fas fa-minus-square"></i> &nbsp; Receiving Waiting</a>
        </li>
        <li>
          <a href="<?php echo base_url();?>receiving_list/complete"><i class="fas fa-minus-square"></i> &nbsp; Receiving Complete</a>
        </li>
      </ul>
    </li>
    <?php } ?>

  </ul>
</nav>