<div class="wrapper" style="min-height: 79vh">
<nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) || $sidebar_close == 1 ? 'active' : '') ?>">
  <ul class="list-unstyled components">

    <!-- <li>
      <a href="<?php //echo base_url();?>mb/mb_list">
        <i class="fas fa-pallet"></i> &nbsp; Material Balance
      </a>
    </li> -->
    <?php if($this->permission_cookie[88] == 1){ ?>
    <li>
      <a href="#menuuser" data-parent="#sidebar" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-user"></i> &nbsp; User
      </a>
      <ul class="list-unstyled collapse show" id="menuuser">
        <?php if($this->permission_cookie[88] == 1){ ?>
        <li>
          <a href="<?= base_url();?>user/userList"><i class="fas fa-list"></i> &nbsp; User List</a>
        </li>
        <?php } ?>
        <?php if($this->permission_cookie[89] == 1){ ?>
        <li>
          <a href="<?= base_url();?>user/userDetailNew"><i class="fa fa-plus"></i> &nbsp; Add New User</a>
        </li>
        <?php } ?>
      </ul>
    </li>
    <?php } ?>

    <?php if($this->permission_cookie[95] == 1 || $this->permission_cookie[96] == 1){ ?>
    <li>
      <a href="#menudept" data-parent="#sidebar" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-users"></i> &nbsp; Department
      </a>
      <ul class="list-unstyled collapse show" id="menudept">
        <?php if($this->permission_cookie[95] == 1){ ?>
        <li>
          <a href="<?= base_url();?>department/departmentList"><i class="fas fa-list"></i> &nbsp; Department List</a>
        </li>
        <?php } ?>
        <?php if($this->permission_cookie[96] == 1){ ?>
        <li>
          <a href="<?= base_url();?>department/departmentDetailNew"><i class="fa fa-plus"></i> &nbsp; Add New Department</a>
        </li>
        <?php } ?>
      </ul>
    </li>
    <?php } ?>

    <?php if($this->permission_cookie[7] == 1 || $this->permission_cookie[8] == 1){ ?>
    <li>
      <a href="#menuaccount" data-parent="#sidebar" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-book"></i> &nbsp; Account
      </a>
      <ul class="list-unstyled collapse show" id="menuaccount">
        <?php if($this->permission_cookie[7] == 1){ ?>
        <li>
          <a href="<?= base_url();?>budget/budgetCategoryList"><i class="fas fa-list"></i> &nbsp; Account List</a>
        </li>
        <?php } ?>
        <?php if($this->permission_cookie[8] == 1){ ?>
        <li>
          <a href="<?= base_url();?>budget/budgetCategoryNew"><i class="fa fa-plus"></i> &nbsp; Add New Account</a>
        </li>
        <?php } ?>
      </ul>
    </li>
    <?php } ?>

    <?php if($this->permission_cookie[32] == 1 || $this->permission_cookie[33] == 1){ ?>
    <li>
      <a href="#menuvendor" data-parent="#sidebar" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-certificate"></i> Vendor
      </a>
      <ul class="list-unstyled collapse show" id="menuvendor">
        <?php if($this->permission_cookie[32] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>vendor_new"><i class="fas fa-plus"></i> &nbsp; Add New Vendor</a>
        </li>
        <?php } ?>
        <?php if($this->permission_cookie[33] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>vendor_list"><i class="fas fa-list"></i> &nbsp; Vendor List</a>
        </li>
        <?php } ?>
        <?php if($this->permission_cookie[33] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>vendor_import"><i class="fas fa-plus"></i> &nbsp; Import Vendor</a>
        </li> 
        <?php } ?>        
      </ul>
    </li> 
    <?php } ?>

    <?php if($this->permission_cookie[32] == 1 || $this->permission_cookie[33] == 1){ ?>
    <li>
      <a href="#menuvendor" data-parent="#sidebar" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
        <i class="fas fa-certificate"></i> Vendor Category
      </a>
      <ul class="list-unstyled collapse show" id="menuvendor">
        <?php if($this->permission_cookie[32] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>vendor_verification_new"><i class="fas fa-plus"></i> &nbsp; Add Vendor Category</a>
        </li>
        <?php } ?>
        <?php if($this->permission_cookie[33] == 1){ ?>
        <li>
          <a href="<?php echo base_url();?>vendor_verification_list"><i class="fas fa-list"></i> &nbsp; Vendor Category List</a>
        </li>
        <?php } ?>        
      </ul>
    </li> 
    <?php } ?>

  </ul>
</nav>