<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>user/userDetailEditProcess">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Badge</label>
                <div class="col-sm-10">
                  <input type="text" name="badge_no" class="form-control" value="<?php echo $user['badge_no'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Full Name</label>
                <div class="col-sm-10">
                  <input type="text" name="full_name" class="form-control" value="<?php echo $user['full_name'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" name="username" class="form-control" value="<?php echo $user['username'] ?>" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Department</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="department">
                    <?php foreach($dept_list as $dept): ?>
                    <option value="<?php echo $dept['id_department'] ?>" <?php echo ($dept['id_department'] == $user['department'] ? 'selected' : '') ?>><?php echo $dept['name_of_department'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">E-mail</label>
                <div class="col-sm-10">
                  <input type="text" name="email" class="form-control" value="<?php echo $user['email'] ?>" required>
                </div>
              </div>

             <div class="form-group row">
                <label class="col-sm-2 col-form-label">Level</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="id_role">
                    <?php foreach($role_list as $role): ?>
                    <option value="<?php echo $role['id'] ?>" <?php echo ($role['id'] == $user['id_role'] ? 'selected' : '') ?>><?php echo $role['name_of_role'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Status</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="status_user">
                    <option value="1" <?php echo (1 == $user['status_user'] ? 'selected' : '') ?>>Actived</option>
                    <option value="0" <?php echo (0 == $user['status_user'] ? 'selected' : '') ?>>Disactived</option>
                  </select>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <input type="hidden" name="id_user" value="<?php echo $user['id_user'] ?>" required>
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url() ?>user/userList" class="btn btn-danger" title="Submit"><i class="fa fa-times"></i> Cancel</a>
          </div>
        </div>
      </div>

    </div>
  </form>

  <?php if($this->permission_cookie[101] == 1){ ?>
  <form method="POST" action="<?php echo base_url();?>user/userUpdatePermissionProcess">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
              <div class="w-100 bg-dark">
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-danger" onclick="change_menu(this)">Administrator</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">User</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">Budget</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">MR</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">Quotation</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">Tabulation</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">PO</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">Receiving</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">MRIR</button>
                <button type="button" class="btn btn-flat p-3 font-weight-bold btn-dark" onclick="change_menu(this)">OSD</button>
              </div>
              <div class="content-permission d-none p-3" name='Administrator'>
                <h2 class="mb-3">Administrator</h2>
                <hr>
                <label class="container-checkbox font-weight-bold">All
                  <input type="checkbox" onchange="checkbox_all();" value="1" id="check_all">
                  <span class="checkmark"></span>
                </label>
                <!-- <label class="container-checkbox font-weight-bold">Administrator(User And Department) - 0
                  <input type="checkbox" value="1" name="0" <?php echo ($permission_wh[0] == 1 ? 'checked' : '') ?>>
                  <span class="checkmark"></span>
                </label> -->
              </div>

              <div class="content-permission d-none p-3" name='User'>
                <h2 class="mb-3">User</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View User - 88
                      <input type="checkbox" value="1" name="88" <?php echo ($permission_wh[88] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New User - 89
                      <input type="checkbox" value="1" name="89" <?php echo ($permission_wh[89] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update User - 90
                      <input type="checkbox" value="1" name="90" <?php echo ($permission_wh[90] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete User - 91
                      <input type="checkbox" value="1" name="91" <?php echo ($permission_wh[91] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import User - 92
                      <input type="checkbox" value="1" name="92" <?php echo ($permission_wh[92] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export User - 93
                      <input type="checkbox" value="1" name="93" <?php echo ($permission_wh[93] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Permission User - 101
                      <input type="checkbox" value="1" name="101" <?php echo ($permission_wh[101] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department User - 94
                      <input type="checkbox" value="1" name="94" <?php echo ($permission_wh[94] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Department  - 95
                      <input type="checkbox" value="1" name="95" <?php echo ($permission_wh[95] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Department - 96
                      <input type="checkbox" value="1" name="96" <?php echo ($permission_wh[96] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Department - 97
                      <input type="checkbox" value="1" name="97" <?php echo ($permission_wh[97] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Department - 98
                      <input type="checkbox" value="1" name="98" <?php echo ($permission_wh[98] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Department - 99
                      <input type="checkbox" value="1" name="99" <?php echo ($permission_wh[99] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Department - 100
                      <input type="checkbox" value="1" name="100" <?php echo ($permission_wh[100] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>

              <div class="content-permission d-none p-3" name='Budget'>
                <h2 class="mb-3">Budget</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Budget - 1
                      <input type="checkbox" value="1" name="1" <?php echo ($permission_wh[1] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Budget - 2
                      <input type="checkbox" value="1" name="2" <?php echo ($permission_wh[2] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Budget - 3
                      <input type="checkbox" value="1" name="3" <?php echo ($permission_wh[3] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Budget - 4
                      <input type="checkbox" value="1" name="4" <?php echo ($permission_wh[4] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Budget - 5
                      <input type="checkbox" value="1" name="5" <?php echo ($permission_wh[5] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Budget - 6
                      <input type="checkbox" value="1" name="6" <?php echo ($permission_wh[6] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department Budget - 80
                      <input type="checkbox" value="1" name="80" <?php echo ($permission_wh[80] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Budget Category  - 7
                      <input type="checkbox" value="1" name="7" <?php echo ($permission_wh[7] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Budget Category - 8
                      <input type="checkbox" value="1" name="8" <?php echo ($permission_wh[8] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Budget Category - 9
                      <input type="checkbox" value="1" name="9" <?php echo ($permission_wh[9] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Budget Category - 10
                      <input type="checkbox" value="1" name="10" <?php echo ($permission_wh[10] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Budget Category - 11
                      <input type="checkbox" value="1" name="11" <?php echo ($permission_wh[11] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Budget Category - 12
                      <input type="checkbox" value="1" name="12" <?php echo ($permission_wh[12] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Budget Transfer  - 13
                      <input type="checkbox" value="1" name="13" <?php echo ($permission_wh[13] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Budget Transfer - 14
                      <input type="checkbox" value="1" name="14" <?php echo ($permission_wh[14] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Budget Transfer - 15
                      <input type="checkbox" value="1" name="15" <?php echo ($permission_wh[15] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Budget Transfer - 16
                      <input type="checkbox" value="1" name="16" <?php echo ($permission_wh[16] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Budget Transfer - 17
                      <input type="checkbox" value="1" name="17" <?php echo ($permission_wh[17] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Budget Transfer - 18
                      <input type="checkbox" value="1" name="18" <?php echo ($permission_wh[18] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>

              <div class="content-permission d-none p-3" name='MR'>
                <h2 class="mb-3">MR</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Material Request (MR) - 19
                      <input type="checkbox" value="1" name="19" <?php echo ($permission_wh[19] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Material Request (MR) - 20
                      <input type="checkbox" value="1" name="20" <?php echo ($permission_wh[20] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Material Request (MR) - 21
                      <input type="checkbox" value="1" name="21" <?php echo ($permission_wh[21] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Material Request (MR) - 22
                      <input type="checkbox" value="1" name="22" <?php echo ($permission_wh[22] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Material Request (MR) - 23
                      <input type="checkbox" value="1" name="23" <?php echo ($permission_wh[23] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Material Request (MR) - 24
                      <input type="checkbox" value="1" name="24" <?php echo ($permission_wh[24] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Aproval Material Request (MR) - 25
                      <input type="checkbox" value="1" name="25" <?php echo ($permission_wh[25] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department Material Request (MR) - 81
                      <input type="checkbox" value="1" name="81" <?php echo ($permission_wh[81] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Material Catalog - 26
                      <input type="checkbox" value="1" name="26" <?php echo ($permission_wh[26] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Material Catalog - 27
                      <input type="checkbox" value="1" name="27" <?php echo ($permission_wh[27] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Material Catalog - 28
                      <input type="checkbox" value="1" name="28" <?php echo ($permission_wh[28] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Material Catalog - 29
                      <input type="checkbox" value="1" name="29" <?php echo ($permission_wh[29] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Material Catalog - 30
                      <input type="checkbox" value="1" name="30" <?php echo ($permission_wh[30] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Material Catalog - 31
                      <input type="checkbox" value="1" name="31" <?php echo ($permission_wh[31] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Vendor - 32
                      <input type="checkbox" value="1" name="32" <?php echo ($permission_wh[32] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Vendor - 33
                      <input type="checkbox" value="1" name="33" <?php echo ($permission_wh[33] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Vendor - 34
                      <input type="checkbox" value="1" name="34" <?php echo ($permission_wh[34] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Vendor - 35
                      <input type="checkbox" value="1" name="35" <?php echo ($permission_wh[35] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Vendor - 36
                      <input type="checkbox" value="1" name="36" <?php echo ($permission_wh[36] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Vendor - 37
                      <input type="checkbox" value="1" name="37" <?php echo ($permission_wh[37] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>                
              </div>

              <div class="content-permission d-none p-3" name='Quotation'>
                <h2 class="mb-3">Quotation</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Quotation - 38
                      <input type="checkbox" value="1" name="38" <?php echo ($permission_wh[38] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Quotation - 39
                      <input type="checkbox" value="1" name="39" <?php echo ($permission_wh[39] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Quotation - 40
                      <input type="checkbox" value="1" name="40" <?php echo ($permission_wh[40] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Quotation - 41
                      <input type="checkbox" value="1" name="41" <?php echo ($permission_wh[41] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Quotation - 42
                      <input type="checkbox" value="1" name="42" <?php echo ($permission_wh[42] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Quotation - 43
                      <input type="checkbox" value="1" name="43" <?php echo ($permission_wh[43] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Aproval Quotation - 44
                      <input type="checkbox" value="1" name="44" <?php echo ($permission_wh[44] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department Quotation - 82
                      <input type="checkbox" value="1" name="82" <?php echo ($permission_wh[82] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>                
              </div>

              <div class="content-permission d-none p-3" name='Tabulation'>
                <h2 class="mb-3">Tabulation</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Tabulation - 45
                      <input type="checkbox" value="1" name="45" <?php echo ($permission_wh[45] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Tabulation - 46
                      <input type="checkbox" value="1" name="46" <?php echo ($permission_wh[46] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Tabulation - 47
                      <input type="checkbox" value="1" name="47" <?php echo ($permission_wh[47] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Tabulation - 48
                      <input type="checkbox" value="1" name="48" <?php echo ($permission_wh[48] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Tabulation - 49
                      <input type="checkbox" value="1" name="49" <?php echo ($permission_wh[49] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Tabulation - 50
                      <input type="checkbox" value="1" name="50" <?php echo ($permission_wh[50] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Aproval Tabulation - 51
                      <input type="checkbox" value="1" name="51" <?php echo ($permission_wh[51] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department Tabulation - 83
                      <input type="checkbox" value="1" name="83" <?php echo ($permission_wh[83] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>                
              </div>

              <div class="content-permission d-none p-3" name='PO'>
                <h2 class="mb-3">PO</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Purchase Order (PO) - 52
                      <input type="checkbox" value="1" name="52" <?php echo ($permission_wh[52] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Purchase Order (PO) - 53
                      <input type="checkbox" value="1" name="53" <?php echo ($permission_wh[53] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Purchase Order (PO) - 54
                      <input type="checkbox" value="1" name="54" <?php echo ($permission_wh[54] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Purchase Order (PO) - 55
                      <input type="checkbox" value="1" name="55" <?php echo ($permission_wh[55] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Purchase Order (PO) - 56
                      <input type="checkbox" value="1" name="56" <?php echo ($permission_wh[56] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Purchase Order (PO) - 57
                      <input type="checkbox" value="1" name="57" <?php echo ($permission_wh[57] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Aproval Purchase Order (PO) - 58
                      <input type="checkbox" value="1" name="58" <?php echo ($permission_wh[58] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department Purchase Order (PO) - 84
                      <input type="checkbox" value="1" name="84" <?php echo ($permission_wh[84] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>                
              </div>

              <div class="content-permission d-none p-3" name='Receiving'>
                <h2 class="mb-3">Receiving</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View Receiving - 59
                      <input type="checkbox" value="1" name="59" <?php echo ($permission_wh[59] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New Receiving - 60
                      <input type="checkbox" value="1" name="60" <?php echo ($permission_wh[60] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update Receiving - 61
                      <input type="checkbox" value="1" name="61" <?php echo ($permission_wh[61] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete Receiving - 62
                      <input type="checkbox" value="1" name="62" <?php echo ($permission_wh[62] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import Receiving - 63
                      <input type="checkbox" value="1" name="63" <?php echo ($permission_wh[63] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export Receiving - 64
                      <input type="checkbox" value="1" name="64" <?php echo ($permission_wh[64] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Aproval Receiving - 65
                      <input type="checkbox" value="1" name="65" <?php echo ($permission_wh[65] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department Receiving - 85
                      <input type="checkbox" value="1" name="85" <?php echo ($permission_wh[85] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>                
              </div>

              <div class="content-permission d-none p-3" name='MRIR'>
                <h2 class="mb-3">MRIR</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View MRIR - 66
                      <input type="checkbox" value="1" name="66" <?php echo ($permission_wh[66] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New MRIR - 67
                      <input type="checkbox" value="1" name="67" <?php echo ($permission_wh[67] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update MRIR - 68
                      <input type="checkbox" value="1" name="68" <?php echo ($permission_wh[68] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete MRIR - 69
                      <input type="checkbox" value="1" name="69" <?php echo ($permission_wh[69] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import MRIR - 70
                      <input type="checkbox" value="1" name="70" <?php echo ($permission_wh[70] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export MRIR - 71
                      <input type="checkbox" value="1" name="71" <?php echo ($permission_wh[71] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Aproval MRIR - 72
                      <input type="checkbox" value="1" name="72" <?php echo ($permission_wh[72] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department MRIR - 86
                      <input type="checkbox" value="1" name="86" <?php echo ($permission_wh[86] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>                
              </div>

              <div class="content-permission d-none p-3" name='OSD'>
                <h2 class="mb-3">OSD</h2>
                <hr>
                <div class="row">
                  <div class="col-auto">
                    <label class="container-checkbox font-weight-bold">View OSD - 73
                      <input type="checkbox" value="1" name="73" <?php echo ($permission_wh[73] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Create New OSD - 74
                      <input type="checkbox" value="1" name="74" <?php echo ($permission_wh[74] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Update OSD - 75
                      <input type="checkbox" value="1" name="75" <?php echo ($permission_wh[75] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Delete OSD - 76
                      <input type="checkbox" value="1" name="76" <?php echo ($permission_wh[76] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Import OSD - 77
                      <input type="checkbox" value="1" name="77" <?php echo ($permission_wh[77] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Export OSD - 78
                      <input type="checkbox" value="1" name="78" <?php echo ($permission_wh[78] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">Aproval OSD - 79
                      <input type="checkbox" value="1" name="79" <?php echo ($permission_wh[79] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                    <label class="container-checkbox font-weight-bold">View All Department OSD - 87
                      <input type="checkbox" value="1" name="87" <?php echo ($permission_wh[87] == 1 ? 'checked' : '') ?>>
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>                
              </div>
            </div>
          </div>
          <div class="text-right mt-3">
            <input type="hidden" name="id_user" value="<?php echo $user['id_user'] ?>" required>
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
  <?php } ?>

</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $('div.content-permission[name=Administrator]').removeClass('d-none');
  check_checkbox();
  
  function change_menu(btn) {
    var text = $(btn).text();
    $('.content-permission').addClass('d-none');
    $('.content-permission[name='+text+']').removeClass('d-none');
  }

  function checkbox_all() {
    if($('#check_all').prop('checked') == true){
      $('input[type=checkbox]').prop('checked', true);
    }
    else{
      $('input[type=checkbox]').prop('checked', false);
    }
    
  }

  $('input[type=checkbox]').not('#check_all').change(function() {
    check_checkbox();
  });

  function check_checkbox() {
    var number_checked = $('input:checkbox:checked').not('#check_all').length;
    var number_checkbox = $('input:checkbox').not('#check_all').length;
    console.log(number_checkbox - number_checked);
    if((number_checkbox - number_checked) > 0){
      console.log('not all');
      $('#check_all').prop('checked', false);
    }
    else{
      console.log('all');
      $('#check_all').prop('checked', true);
    }
  }
</script>