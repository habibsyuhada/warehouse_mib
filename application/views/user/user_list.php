<?php 
  $f_dept = $this->input->get('dept');
?>
<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">
    
    <!-- <div class="col-md-12" id="container-filter">
      <form method="get">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Filter</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Department</label>
                <div class="col-sm-10">
                  <select onchange="change_filter();" class="custom-select" name="dept">
                    <?php foreach($dept_list as $dept): ?>
                    <option value="<?php echo $dept['id_department'] ?>"><?php echo $dept['name_of_department'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" class="btn btn-flat btn-info" title="Submit"><i class="fa fa-search"></i> Filter</button>
          </div>
        </div>
      </form>
    </div> -->

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <!-- <div class="p-3 my-3 bg-light border">
              <div class="row">
                <div class="col-sm-12 col-md">
                  <button type="button" class="btn btn-flat btn-info">Total User : 1 User</button>
                </div>
                <div class="col-sm-12 col-md-auto">
                  <?php if($this->permission_cookie[89] == 1){ ?>
                  <a name="add_data" href="<?php echo base_url();?>user/userDetailNew/<?php echo $f_dept ?>" class="btn btn-flat btn-info"><i class="fa fa-plus"></i> Add Data</a>
                  <?php } ?>
                </div>
              </div>
            </div> -->

            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center dataTable">
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>ID</th>
                  <th>Badge</th>
                  <th>Full Name</th>
                  <th>Username</th>
                  <th>Department</th>
                  <th>E-mail</th> 
                  <th>Level</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($all_list as $key => $all): ?>
                  <tr>
                    <td><?php echo $all['id_user'] ?></td>
                    <td><?php echo $all['badge_no'] ?></td>
                    <td><?php echo $all['full_name'] ?></td>
                    <td><?php echo $all['username'] ?></td>
                    <td><?php echo $all['name_of_department'] ?></td>
                    <td><?php echo $all['email'] ?></td>
                    <td><?php echo $all['name_of_role'] ?></td>
                    <td><?php echo ($all['status_user'] == 1 ? '<span class="badge badge-success">Actived</span>' : '<span class="badge badge-danger">Disactived</span>') ?></td>
                    <td nowrap>
                      <?php if($this->permission_cookie[90] == 1){ ?>
                      <a href="<?php echo base_url() ?>user/userDetailEdit/<?php echo strtr($this->encryption->encrypt($all['id_user']), '+=/', '.-~') ?>" class="btn btn-flat btn-warning"><i class="fa fa-edit"></i> Edit</a>
                      <?php } ?>
                      <?php if($this->permission_cookie[91] == 1){ ?>
                      <a href="<?php echo base_url() ?>user/userDetailDeleteProcess/<?php echo strtr($this->encryption->encrypt($all['id_user']), '+=/', '.-~') ?>" onclick="return confirm('Are you sure?')" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i> Delete</a>
                      <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('.dataTable').DataTable({
    
  });

  function change_filter() {
    var dept = $("#container-filter select[name=dept]").val();

    $('a[name=add_data]').attr("href", "<?php echo base_url();?>user/userDetailNew/"+dept);
  }
</script>