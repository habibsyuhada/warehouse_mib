<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>user/userDetailNewProcess">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Badge</label>
                <div class="col-sm-10">
                  <input type="text" name="badge_no" class="form-control" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Full Name</label>
                <div class="col-sm-10">
                  <input type="text" name="full_name" class="form-control" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" name="username" class="form-control" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Department</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="department">
                    <?php foreach($dept_list as $dept): ?>
                    <option value="<?php echo $dept['id_department'] ?>"><?php echo $dept['name_of_department'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">E-mail</label>
                <div class="col-sm-10">
                  <input type="text" name="email" class="form-control" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Level</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="id_role">
                    <?php foreach($role_list as $role): ?>
                    <option value="<?php echo $role['id'] ?>"><?php echo $role['name_of_role'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Submit</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  $(".yearpicker").datepicker({
    format: "yyyy",
    viewMode: "years", 
    minViewMode: "years",
    orientation: "bottom left",
    autoclose: true
  });
</script>