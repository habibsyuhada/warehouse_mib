<div id="content" class="container-fluid">
  <form method="POST" action="">
    <div class="row">
      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Badge</label>
                <div class="col-sm-10">
                  <input type="text" name="badge_no" class="form-control" value="<?php echo $user['badge_no'] ?>" disabled>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Full Name</label>
                <div class="col-sm-10">
                  <input type="text" name="full_name" class="form-control" value="<?php echo $user['full_name'] ?>" disabled>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" name="username" class="form-control" value="<?php echo $user['username'] ?>" disabled>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Department</label>
                <div class="col-sm-10">
                  <select class="custom-select" name="department" disabled>
                    <?php foreach($dept_list as $dept): ?>
                    <option value="<?php echo $dept['id_department'] ?>" <?php echo ($dept['id_department'] == $user['department'] ? 'selected' : '') ?>><?php echo $dept['name_of_department'] ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">E-mail</label>
                <div class="col-sm-10">
                  <input type="text" name="email" class="form-control" value="<?php echo $user['email'] ?>" disabled>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </form>

  <form method="POST" action="<?php echo base_url();?>user/userPasswordEditProcess">
    <div class="row">

      <div class="col-md-6">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Digital Signature</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
              <img src="data:image/png;base64, <?php echo $user['sign_approval']; ?>" width='100%;'/>
            </div>
          </div>
          <div class="text-right mt-3">
            <a href="<?php echo base_url() ?>user/user_signature" class="btn btn-warning" title="Submit"><i class="fa fa-edit"></i> Update Digital Signature</a>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0">Digital Signature</h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">
              
              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Old Password</label>
                <div class="col-sm-9">
                  <input type="password" name="old_password" class="form-control" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-3 col-form-label">New Password</label>
                <div class="col-sm-9">
                  <input type="password" name="new_password" class="form-control" required>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-sm-3 col-form-label">Confirm Password</label>
                <div class="col-sm-9">
                  <input type="password" name="confirm_password" class="form-control" required>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' class="btn btn-success" title="Submit"><i class="fa fa-check"></i> Change Password</button>
          </div>
        </div>
      </div>
    </div>
  </form>

</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">

</script>