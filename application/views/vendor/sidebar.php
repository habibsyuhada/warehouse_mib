<div class="wrapper" style="min-height: 79vh">
  <nav id="sidebar" class="<?php echo (($this->input->cookie('sidebarCollapse') !== NULL && $this->input->cookie('sidebarCollapse') == 1) ? 'active' : '') ?>">
    <ul class="list-unstyled components">
           
      
      <li>
        <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
          <i class="fas fa-home"></i> Vendor
        </a>
        <ul class="list-unstyled" id="homeSubmenu2">
          <li>
            <a href="<?php echo base_url();?>vendor_new"><i class="fas fa-plus"></i> &nbsp; Create New</a>
          </li>
          
          <li>
            <a href="<?php echo base_url();?>vendor_list"><i class="fas fa-list"></i> &nbsp; Vendor List</a>
          </li>         
        </ul>
      </li> 
   

    </ul>
  </nav>