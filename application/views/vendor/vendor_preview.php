<?php 
  // print_r($sheet);exit;
?>
<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>vendor/vendor_import_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <table class="table dataTable table-borderless table-hover text-center">
                <thead class="bg-green-smoe text-white">
                  <tr>
                    <th rowspan="2">Vendor Name</th>
                    <th rowspan="2">Remarks</th>
                    <th rowspan="2">Address</th>
                    <!-- <th rowspan="2">PIC</th> -->
                    <th rowspan="2">Phone Number</th>
                    <th rowspan="2">E-mail</th>
                    <th rowspan="2">FAX</th>
                    <th colspan="2">Contact</th>
                    <th rowspan="2">Status</th>
                  </tr>
                  <tr>
                    <th>Name</th>
                    <th>Phone</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    foreach($sheet as $key => $row){
                      if($key > 1){

                        $status = '';
                        $disabled = 0;

                        //CATALOG CODE ---------------------------
                        // if(!$row['id_catalog_code'] == ''){
                        //   $status = 'Duplicate Vendor';
                        //   $disabled = 2;
                        // }
                        //----------------------------------------

                        if($row['A'] != ""){
                  ?>
                  <tr style="background: <?php echo ($disabled == 2 ? '#f8d7da' : ($disabled == 1 ? '#fff3cd' : '')); ?>">
                    <td class="align-middle">
                      <input type="text" class="form-control" name="vendor_name[]" value="<?= $row['A'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <textarea class="form-control" name="remarks[]" <?php echo ($disabled > 0 ? 'disabled' : ''); ?> ><?php echo $row['B'] ?></textarea>
                    </td>
                    <td class="align-middle">
                      <textarea class="form-control" name="address[]" <?php echo ($disabled > 0 ? 'disabled' : ''); ?> ><?php echo $row['C'] ?></textarea>
                    </td>
                    <!-- <td class="align-middle">
                      <input type="text" class="form-control" name="pic[]" value="<?= $row['D'] ?>" readonly <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td> -->
                    <td class="align-middle">
                      <input type="text" class="form-control" name="phone_no[]" value="<?= $row['E'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="email[]" value="<?= $row['F'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="fax[]" value="<?= $row['G'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="contact_name[]" value="<?= $row['H'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle">
                      <input type="text" class="form-control" name="contact_phone[]" value="<?= $row['I'] ?>" <?php echo ($disabled > 0 ? 'disabled' : ''); ?>>
                    </td>
                    <td class="align-middle"><?php echo $status ?></td>
                  </tr>
                  <?php } } } ?>
                </tbody>
              </table>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn'  value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>vendor_list" class="btn btn-secondary " title="Submit"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->