<?php 
  $vendor_list = $vendor_list[0]; 
  // print_r($vendor_list);exit;
?>

<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>vendor_verification/vendor_verification_edit_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">

                <input type="hidden" name="id" value="<?= $vendor_list['id'] ?>">

                <label class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="verification" placeholder="Name" onfocus="$(this).removeClass('is-valid is-invalid')" value="<?= $vendor_list['verification'] ?>" required>
                  <label class="d-none" id="label_alert_vendor_number" style="color: red;"></label>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='update' id='submitBtn' value='submit' class="btn btn-success " title="Update"><i class="fa fa-check"></i> Update</button>
            <a href="<?php echo base_url();?>vendor_verification_list" class="btn btn-secondary " title="Cancel"><i class="fa fa-arrow-left"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">

  function check_vendor_number(param, input) {
    var real_vendor_number = <?= $vendor_list['vendor_no'] ?>;
    var vendor_number = $(input).val();

    if(vendor_number == real_vendor_number){
      $(input).addClass('is-valid').removeClass('is-invalid');
      $('#label_alert_vendor_number').addClass('d-none');
      $('button[name=submit]').removeAttr("disabled");
    } else {

      if(vendor_number == ''){
          $(input).addClass('is-invalid');
          $('#label_alert_vendor_number').removeClass('d-none').text('Vendor Number is Required');
          $('button[name=submit]').prop("disabled", true);    
      } else {
        $.ajax({
          url: "<?php echo base_url();?>vendor/vendor_check/" + param,
          type: "post",
          data: {
            vendor_number: vendor_number,
          },
          success: function(data) {
            var data = JSON.parse(data);

            if(data.hasil > 0){ // KALAU DUPLICATE ----
              $(input).addClass('is-invalid').removeClass('is-valid');
              $('#label_alert_vendor_number').removeClass('d-none').text('Duplicate Vendor Number');
              $('button[name=submit]').attr("disabled", true);

            } else{ // KALAU GAK DUPLICATE -----
              $(input).removeClass('is-invalid').addClass('is-valid');
              $('#label_alert_vendor_number').addClass('d-none');
              $('button[name=submit]').removeAttr("disabled");
            
            }
          }
        });
      }

    }

  }
</script>