<div id="content" class="container-fluid" style="overflow: auto;">
  <div class="row">

    <div class="col-md-12">
      <div class="my-3 p-3 bg-white rounded shadow-sm">
        <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
        <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
          <div class="container-fluid">
            <?php  echo $this->session->flashdata('message');?>
            <table class="table table-hover text-center" id='vendor_verification_list_dt'>
              <thead class="bg-green-smoe text-white">
                <tr>
                  <th>Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">   
  $('#vendor_verification_list_dt').DataTable({
    // "scrollX": true,
    "language": { 
      "infoFiltered": "" },
      "paging": true,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
          "url": "<?php echo base_url();?>vendor_verification/vendor_verification_list_json",
          "type": "POST",
      },
      "columnDefs": [{
        "targets": [0],
        "orderable": true,
      }, ],
  });

  function delete_action(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if(result.value) {
        $.ajax({
          data: {
            'id' : id,
          },
          url: '<?= base_url() ?>vendor_verification/vendor_verification_delete_process',
          type: "post",
          success : function(data){

            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            ).then(function() {
              location.reload();
            });

          }
        });
      }
    })
  }
</script>