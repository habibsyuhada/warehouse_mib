<div id="content" class="container-fluid">
  <form method="POST" action="<?php echo base_url();?>vendor_verification/vendor_verification_add_process">
    <div class="row">

      <div class="col-md-12">
        <div class="my-3 p-3 bg-white rounded shadow-sm">
          <h6 class="pb-2 mb-0"><?php echo $meta_title ?></h6>
          <div class="overflow-auto media text-muted py-3 mt-1 border-bottom border-top border-gray">
            <div class="container-fluid">

              <div class="form-group row">
                <label class="col-sm-2 col-form-label">Name </label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="verification" placeholder="Name" onfocus="$(this).removeClass('is-valid is-invalid')" required>
                  <label class="d-none" id="label_alert_verification" style="color: red;"></label>
                </div>
              </div>

            </div>
          </div>
          <div class="text-right mt-3">
            <button type="submit" name='submit' id='submitBtn' value='submit' class="btn btn-success " title="Submit"><i class="fa fa-check"></i> Submit</button>
            <a href="<?php echo base_url();?>vendor_list" class="btn btn-secondary " title="Cancel"><i class="fa fa-arrow-left"></i> Cancel</a>
          </div>
        </div>
      </div>
    </div>
  </form>
</div>
</div><!-- ini div dari sidebar yang class wrapper -->
<script type="text/javascript">
  
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: "bottom auto",
    autoclose: true,
    todayHighlight: true
  });

  function check_vendor_number(param, input) {
    var vendor_number = $(input).val();

    if(vendor_number == ''){
        $(input).addClass('is-invalid');
        $('#label_alert_verification').removeClass('d-none').text('Name is Required');
        $('button[name=submit]').prop("disabled", true);    
    } else {
      $.ajax({
        url: "<?php echo base_url();?>vendor_verification/vendor_verification_check/" + param,
        type: "post",
        data: {
          vendor_number: vendor_number,
        },
        success: function(data) {
          var data = JSON.parse(data);

          if(data.hasil > 0){ // KALAU DUPLICATE ----
            $(input).addClass('is-invalid').removeClass('is-valid');
            $('#label_alert_verification').removeClass('d-none').text('Duplicate Vendor Name');
            $('button[name=submit]').attr("disabled", true);

          } else{ // KALAU GAK DUPLICATE -----
            $(input).removeClass('is-invalid').addClass('is-valid');
            $('#label_alert_verification').addClass('d-none');
            $('button[name=submit]').removeAttr("disabled");
          
          }
        }
      });

    }
  }

</script>