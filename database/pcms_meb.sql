-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2020 at 06:38 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pcms_meb`
--

-- --------------------------------------------------------

--
-- Table structure for table `eproc_budgeting`
--

CREATE TABLE `eproc_budgeting` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `dept` int(11) NOT NULL,
  `nama_pt` int(11) NOT NULL DEFAULT '1' COMMENT '1 MEB; 2 DEB',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_delete` int(11) NOT NULL DEFAULT '1' COMMENT '0 = deleted; 1 = actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_budgeting`
--

INSERT INTO `eproc_budgeting` (`id`, `year`, `dept`, `nama_pt`, `created_by`, `created_date`, `status_delete`) VALUES
(1, 2020, 32, 1, 76, '2020-02-08 10:09:59', 1),
(2, 2020, 32, 1, 76, '2020-02-11 09:29:54', 1),
(3, 2021, 32, 1, 76, '2020-02-11 09:44:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_budgeting_category`
--

CREATE TABLE `eproc_budgeting_category` (
  `id` int(11) NOT NULL,
  `account_no` varchar(50) NOT NULL,
  `assetable` varchar(10) NOT NULL DEFAULT 'Capex' COMMENT 'CAPEX ? OPEX',
  `category_name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_delete` int(11) NOT NULL DEFAULT '1' COMMENT '0 = Deleted; 1 = Actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_budgeting_category`
--

INSERT INTO `eproc_budgeting_category` (`id`, `account_no`, `assetable`, `category_name`, `created_by`, `created_date`, `status_delete`) VALUES
(1, '1800400001', 'Capex', 'Computer & Peripherals', 0, '0000-00-00 00:00:00', 1),
(2, '1800400005', 'Capex', 'Office & Factory Equipment', 0, '0000-00-00 00:00:00', 1),
(3, '1800303001', 'Capex', 'Power Generator Equipment & Machinery', 0, '0000-00-00 00:00:00', 1),
(4, '1800303002', 'Capex', 'Auxiliary Support Equipment & Machinery', 0, '0000-00-00 00:00:00', 1),
(5, '1800400099', 'Capex', 'Other Office Equipments', 0, '0000-00-00 00:00:00', 1),
(6, '6000202004', 'Opex', 'Technical and Engineering Fees Local', 0, '0000-00-00 00:00:00', 1),
(7, '6000202005', 'Opex', 'Technical and Engineering Fees Overseas', 0, '0000-00-00 00:00:00', 1),
(8, '6000203101', 'Opex', 'Maintenance Building Cost', 0, '0000-00-00 00:00:00', 1),
(9, '6000203105', 'Opex', 'Maintenance Production Equipment', 0, '0000-00-00 00:00:00', 1),
(10, '6000400003', 'Opex', 'Freight Charges', 0, '0000-00-00 00:00:00', 1),
(11, '6000700003', 'Opex', 'Inspection & Certification', 0, '0000-00-00 00:00:00', 1),
(12, '6000800004', 'Opex', 'Publication, Printing and Advertising', 0, '0000-00-00 00:00:00', 1),
(13, '6000800009', 'Opex', 'Entertainment & Corporate Relation', 0, '0000-00-00 00:00:00', 1),
(14, '6000902001', 'Opex', 'Computer & Peripherals', 0, '0000-00-00 00:00:00', 1),
(15, '6010100031', 'Opex', 'Lubricant, fuel, and water', 0, '0000-00-00 00:00:00', 1),
(16, '6000203102', 'Opex', 'Maintenance Building Equipment', 0, '0000-00-00 00:00:00', 1),
(17, '6000204001', 'Opex', 'Rental-Vehicles', 0, '0000-00-00 00:00:00', 1),
(18, '6000204002', 'Opex', 'Rental-Machinery & Equipments', 0, '0000-00-00 00:00:00', 1),
(19, '6000700004', 'Opex', 'Calibration', 0, '0000-00-00 00:00:00', 1),
(20, '6010100032', 'Opex', 'Spare Parts', 0, '0000-00-00 00:00:00', 1),
(21, '6000205003', 'Opex', 'Environment, Health & Safety Services', 0, '0000-00-00 00:00:00', 1),
(22, '6000205007', 'Opex', 'Waste (Hazardous & Domestic) - Services', 0, '0000-00-00 00:00:00', 1),
(23, '6000800006', 'Opex', 'Community Development', 0, '0000-00-00 00:00:00', 1),
(24, '6000800007', 'Opex', 'Government Relation ', 0, '0000-00-00 00:00:00', 1),
(25, '6000800008', 'Opex', 'Employee Relationship ', 0, '0000-00-00 00:00:00', 1),
(26, '6000901002', 'Opex', 'Medicine Supplies', 0, '0000-00-00 00:00:00', 1),
(27, '6000902002', 'Opex', 'Safety & Environment Conservation Equipments', 0, '0000-00-00 00:00:00', 1),
(28, '6000902099', 'Opex', 'Other Office Equipment', 0, '0000-00-00 00:00:00', 1),
(29, '6001400003', 'Opex', 'Professional Training and Seminar', 0, '0000-00-00 00:00:00', 1),
(30, '6000203103', 'Opex', 'Maintenance Office Equipment', 0, '0000-00-00 00:00:00', 1),
(31, '6000203104', 'Opex', 'Maintenance Vehicles', 0, '0000-00-00 00:00:00', 1),
(32, '6000204003', 'Opex', 'Rental- Facilities', 0, '0000-00-00 00:00:00', 1),
(33, '6000205004', 'Opex', 'Cleaning Services', 0, '0000-00-00 00:00:00', 1),
(34, '6000205099', 'Opex', 'Other Services', 0, '0000-00-00 00:00:00', 1),
(35, '6000300001', 'Opex', 'Fuel (for Operational & Transportation)', 0, '0000-00-00 00:00:00', 1),
(36, '6000500001', 'Opex', 'Ticket Expenses', 0, '0000-00-00 00:00:00', 1),
(37, '6000500002', 'Opex', 'Accomodation Expenses', 0, '0000-00-00 00:00:00', 1),
(38, '6000600001', 'Opex', 'Insurance Premium', 0, '0000-00-00 00:00:00', 1),
(39, '6000700001', 'Opex', 'Licenses, Permits & Registration', 0, '0000-00-00 00:00:00', 1),
(40, '6000800005', 'Opex', 'Entertainment and Representation Expenses', 0, '0000-00-00 00:00:00', 1),
(41, '6000901001', 'Opex', 'Stamp Duty and Stationeries', 0, '0000-00-00 00:00:00', 1),
(42, '6000902004', 'Opex', 'Clothing & Uniform', 0, '0000-00-00 00:00:00', 1),
(43, '6009000001', 'Opex', 'Food & Beverages', 0, '0000-00-00 00:00:00', 1),
(44, '6009000002', 'Opex', 'Telecommunications Expenses', 0, '0000-00-00 00:00:00', 1),
(45, '6009000003', 'Opex', 'Reading Materials & Subscription (Books,News,Mgzn', 0, '0000-00-00 00:00:00', 1),
(46, '6009000004', 'Opex', 'Utilities (Water, Electricity)', 0, '0000-00-00 00:00:00', 1),
(47, '123', 'Capex', 'Habib', 2020, '2020-02-16 07:06:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_budgeting_detail`
--

CREATE TABLE `eproc_budgeting_detail` (
  `id` int(11) NOT NULL,
  `id_budget` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `assetable` varchar(10) NOT NULL DEFAULT 'Capex' COMMENT 'Capex ? Opex',
  `description` varchar(50) NOT NULL,
  `budget` int(11) NOT NULL,
  `remarks` varchar(50) NOT NULL DEFAULT '-',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_delete` int(11) NOT NULL DEFAULT '1' COMMENT '0 = Deleted; 1 = Actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_budgeting_detail`
--

INSERT INTO `eproc_budgeting_detail` (`id`, `id_budget`, `id_category`, `assetable`, `description`, `budget`, `remarks`, `created_by`, `created_date`, `status_delete`) VALUES
(1, 1, 1, 'Capex', 'Item A', 10000, '-', 76, '2020-02-08 10:11:06', 1),
(2, 1, 2, 'Capex', 'Item B', 50000, '-', 1, '2020-02-08 14:35:24', 1),
(3, 2, 1, 'Capex', 'Item A', 10000, '-', 76, '2020-02-11 09:29:54', 1),
(4, 1, 1, 'Capex', 'Item A', 10000, '-', 76, '2020-02-11 09:48:31', 1),
(5, 1, 2, 'Capex', 'Item A', 10001, '-', 76, '2020-02-11 09:48:31', 1),
(6, 1, 1, 'Capex', 'Item A', 10007, '', 76, '2020-02-11 09:48:31', 1),
(7, 3, 1, 'Capex', 'Item A', 10008, '', 76, '2020-02-11 09:48:31', 1),
(8, 1, 1, 'Capex', '1', 1, '1', 1000051, '2020-02-11 10:57:48', 1),
(9, 1, 1, 'Capex', 'P', 5000, '1', 1000049, '2020-02-24 10:55:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_budgeting_transfer`
--

CREATE TABLE `eproc_budgeting_transfer` (
  `id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `from_category` int(11) NOT NULL,
  `from_dept` int(11) NOT NULL,
  `to_category` int(11) NOT NULL,
  `to_dept` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `remarks` varchar(50) NOT NULL,
  `nama_pt` int(11) NOT NULL DEFAULT '1' COMMENT '1 MEB; 2 DEB',
  `assetable` varchar(10) NOT NULL DEFAULT 'Capex' COMMENT 'Capex? Opex',
  `created_by` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_delete` int(11) NOT NULL DEFAULT '1' COMMENT '0 = Deleted; 1 = Actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_budgeting_transfer`
--

INSERT INTO `eproc_budgeting_transfer` (`id`, `year`, `from_category`, `from_dept`, `to_category`, `to_dept`, `budget`, `remarks`, `nama_pt`, `assetable`, `created_by`, `created_date`, `status_delete`) VALUES
(1, 2020, 1, 32, 2, 32, 1000, 'test aja', 1, 'Capex', 76, '2020-02-10 06:20:27', 1),
(2, 2020, 1, 1, 2, 1, 1, '1', 1, 'Capex', 76, '2020-02-10 06:33:14', 0);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_catalog_category`
--

CREATE TABLE `eproc_catalog_category` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `catalog_category` varchar(50) NOT NULL,
  `status_delete` int(11) NOT NULL COMMENT '0 = deleted, 1 = actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_catalog_category`
--

INSERT INTO `eproc_catalog_category` (`id`, `code`, `catalog_category`, `status_delete`) VALUES
(2, 'pipe', 'PIPE', 1),
(3, 'plate', 'PLATE', 1),
(4, 'anglebar', 'ANGLE BAR', 1),
(5, 'h-beam', 'H-BEAM', 1),
(6, 't-beam', 'T-BEAM', 1),
(7, 'channel', 'CHANNEL', 1),
(8, 'flatbar', 'FLAT BAR', 1),
(9, 'roundbar', 'ROUND BAR', 1),
(10, 'lippedchannel', 'LIPPED CHANNEL', 1),
(11, 'hexbolt', 'HEX BOLT', 1),
(12, 'hexnut', 'HEX NUT', 1),
(13, 'flatwasher', 'FLAT WASHER', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_currency`
--

CREATE TABLE `eproc_currency` (
  `id_cur` int(250) NOT NULL,
  `currency` varchar(250) NOT NULL,
  `status` int(250) NOT NULL DEFAULT '1' COMMENT '1 : enable 0 : disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_currency`
--

INSERT INTO `eproc_currency` (`id_cur`, `currency`, `status`) VALUES
(1, 'IDR', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_logistic`
--

CREATE TABLE `eproc_logistic` (
  `receiving_id` int(11) NOT NULL,
  `supply_number` varchar(20) NOT NULL,
  `shipping_number` varchar(20) NOT NULL,
  `date_psa` date NOT NULL,
  `date_fsa` date NOT NULL,
  `vendor` varchar(20) NOT NULL,
  `material_package` varchar(20) NOT NULL,
  `cipl` varchar(20) NOT NULL,
  `srn` varchar(20) NOT NULL,
  `shipping_line` varchar(20) NOT NULL,
  `vessel_or_flight` varchar(20) NOT NULL,
  `pol` varchar(20) NOT NULL,
  `pod` varchar(20) NOT NULL,
  `package_total` int(11) NOT NULL,
  `weight` float NOT NULL,
  `measurements` decimal(10,4) NOT NULL,
  `po_list` longtext NOT NULL,
  `gp_20` smallint(6) NOT NULL,
  `gp_40` smallint(6) NOT NULL,
  `gp_45` smallint(6) NOT NULL,
  `ot_20` smallint(6) NOT NULL,
  `ot_40` smallint(6) NOT NULL,
  `fr_20` smallint(6) NOT NULL,
  `fr_40` smallint(6) NOT NULL,
  `other` varchar(50) NOT NULL,
  `etd` date NOT NULL,
  `atd` date NOT NULL,
  `eta` date NOT NULL,
  `ata` date NOT NULL,
  `shipping_complete` datetime NOT NULL,
  `remarks` varchar(250) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_logistic`
--

INSERT INTO `eproc_logistic` (`receiving_id`, `supply_number`, `shipping_number`, `date_psa`, `date_fsa`, `vendor`, `material_package`, `cipl`, `srn`, `shipping_line`, `vessel_or_flight`, `pol`, `pod`, `package_total`, `weight`, `measurements`, `po_list`, `gp_20`, `gp_40`, `gp_45`, `ot_20`, `ot_40`, `fr_20`, `fr_40`, `other`, `etd`, `atd`, `eta`, `ata`, `shipping_complete`, `remarks`, `status`) VALUES
(35, 'SUPPLY_01', 'SHIPMENT_01', '2019-12-10', '2019-12-10', 'PT SMOE ', '', 'CIPL_01', 'BL/AWB_01', 'SHIPPING_01', 'VESSEL_01', 'POL_01', 'POD_01', 1555, 123, '13.0000', 'PO-00001;PO-00002;', 20, 40, 45, 20, 40, 20, 40, 'OTHER', '2019-12-10', '2019-12-10', '2019-12-10', '2019-12-10', '0000-00-00 00:00:00', '', 1),
(39, 'SUPPLY_01', 'SHIPPING_01', '2019-12-10', '2019-12-10', 'PT SMOE INDONESIA', '', '', '', '', '', '', '', 0, 0, '0.0000', '123;321;234;432;', 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '', 1),
(50, '123', '321', '2019-12-10', '2019-12-10', 'PT SMOE INDONESIA', '', '', '', '', '', '', '', 0, 0, '0.0000', '123;', 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '', 1),
(51, 'SUPPLY_03', 'SHIPPING_03', '2019-12-10', '2019-12-10', 'PT SMOE INDONESIA', '', '', '', '', '', '', '', 0, 0, '0.0000', 'BBBBB;AAAAA;', 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_logistic_detail`
--

CREATE TABLE `eproc_logistic_detail` (
  `rec_det_id` int(200) NOT NULL,
  `receiving_id` int(200) NOT NULL,
  `catalog_id` int(200) NOT NULL,
  `mrir` varchar(200) NOT NULL,
  `uniq_no` varchar(200) NOT NULL,
  `po_item` varchar(200) NOT NULL,
  `heat_no` varchar(200) NOT NULL,
  `mill_certificate` varchar(200) NOT NULL,
  `country_origin` varchar(200) NOT NULL,
  `brand` varchar(200) NOT NULL,
  `uom` varchar(200) NOT NULL,
  `color_code` varchar(200) NOT NULL,
  `valuta` varchar(200) NOT NULL,
  `area` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `type` varchar(250) NOT NULL,
  `spec` varchar(150) NOT NULL,
  `spec_category` int(11) NOT NULL,
  `plate_or_tag_no` varchar(250) NOT NULL,
  `supplier_name` varchar(250) NOT NULL,
  `ceq` float NOT NULL,
  `date_manufacturing` date NOT NULL,
  `category` varchar(25) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `status_receiving` varchar(200) NOT NULL,
  `created_by` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `update_by` varchar(200) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` varchar(200) NOT NULL,
  `delete_date` datetime NOT NULL,
  `status` varchar(200) NOT NULL COMMENT '0 = delete, 1 = active',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rec_uniq_id` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_logistic_detail`
--

INSERT INTO `eproc_logistic_detail` (`rec_det_id`, `receiving_id`, `catalog_id`, `mrir`, `uniq_no`, `po_item`, `heat_no`, `mill_certificate`, `country_origin`, `brand`, `uom`, `color_code`, `valuta`, `area`, `location`, `type`, `spec`, `spec_category`, `plate_or_tag_no`, `supplier_name`, `ceq`, `date_manufacturing`, `category`, `remarks`, `status_receiving`, `created_by`, `created_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `status`, `timestamp`, `rec_uniq_id`) VALUES
(151, 24, 40, '', 'S-0001', 'P-0001', '25', '80', 'INDONESIA', 'WINSTAR SHIPPING PTE LTD', 'LENGTH', 'RED', '1', '1', 'BATAM', '3.1', 'EN102555S355K2+M Z35', 17, '6-8548', 'PT SMOE', 0.35, '2019-10-30', 'PLATE', '-', 'COMPLETE', '76', '2019-11-25 16:11:40', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '1', '2019-12-06 09:30:31', '5ddb9acca5fe4'),
(152, 24, 41, '', 'S-0002', 'P-0002', '30', '35', 'INDONESIA', 'WINSTAR SHIPPING PTE LTD', 'LENGTH', 'BLUE', '2', '2', 'BATAM', '3.2', 'EN102555S355K2+M Z36', 1, '6-8549', 'PT SMOE', 0.35, '2019-10-30', 'PLATE', '-', 'COMPLETE', '76', '2019-11-25 16:11:40', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '1', '2019-12-06 09:30:33', '5ddb9acca5ffb');

-- --------------------------------------------------------

--
-- Table structure for table `eproc_logistic_doc`
--

CREATE TABLE `eproc_logistic_doc` (
  `id` int(11) NOT NULL,
  `receiving_id` int(11) NOT NULL,
  `remarks` varchar(50) NOT NULL,
  `file_name` text NOT NULL,
  `upload_by` int(11) NOT NULL,
  `upload_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_master_certification`
--

CREATE TABLE `eproc_master_certification` (
  `id` int(11) NOT NULL,
  `certification` varchar(50) NOT NULL,
  `status_delete` int(11) NOT NULL COMMENT '0 = deleted, 1 = actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_master_certification`
--

INSERT INTO `eproc_master_certification` (`id`, `certification`, `status_delete`) VALUES
(1, '3.1', 1),
(2, '3.2', 1),
(3, 'N/A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_master_vendor`
--

CREATE TABLE `eproc_master_vendor` (
  `id_vendor` int(200) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pic` int(11) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `contact_name` varchar(50) NOT NULL,
  `contact_number` varchar(50) NOT NULL,
  `verification` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `remarks` text NOT NULL,
  `status` int(200) NOT NULL COMMENT '0: Deleted 1: Actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_master_vendor`
--

INSERT INTO `eproc_master_vendor` (`id_vendor`, `name`, `address`, `pic`, `phone_no`, `email`, `fax`, `contact_name`, `contact_number`, `verification`, `created_by`, `created_date`, `remarks`, `status`) VALUES
(1, 'PT CAHAYA BAKTI UNGGUL', 'INDONESIA', 0, '', 'smoe@gmail.com', '', '', '', '1;1;1', 146, '2020-02-08 09:08:08', '', 1),
(2, 'INTERNATIONAL PAINT SINGAPORE', 'BATAM', 0, '', 'smoe@gmail.com', '', '', '', '1;1;1', 146, '2020-02-08 09:08:33', '', 1),
(3, 'ALARM SUPPLY PTE.LTD', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR CHEMTEC', 1),
(4, 'ALBA POWER - UK', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'ASP (Authorized Service Provider)', 1),
(5, 'ANAMORPHIC PICTURE', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA LAIN-LAIN', 1),
(6, 'AZ TECHNOLOGY', 'Keprimall - Rukomall Lt.2 No. 61-62 Batam - Indonesia', 0, '0778 7490625', 'niko_21635@yahoo.com', '', 'Edianto', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(7, 'B & E TECHNOLOGIES', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'General Supplier', 1),
(8, 'BPJS', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Kesehatan', 1),
(9, 'CFE FAST ENGLISH COURSE', 'Education and QMS Consultant English and Autocad Drafter Trainer Batu Ampar - Batam.', 0, '81328821009', 'diannursalihbudi@yahoo.co.id', '', 'Dian Budi Nursalih', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(10, 'CONTROL CARE', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA PERBAIKAN CONTROL TURBINE', 1),
(11, 'CV. ABS RADIATOR', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA FABRIKASI RADIATOR', 1),
(12, 'CV. ALAMANDA UTAMA', 'Bengkong Baru Blok D No. 03', 0, '0778-450159', '', '', 'Maryono', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(13, 'CV. ASEAN MARINE SAFETY', 'Komp. Ruko Buana Impian', 0, '0778-7074391', '\'asean.marinesafety@yahoo.com\'', '0778-395616', 'Syafril ', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER SAFETY', 1),
(14, 'CV. BINTAN SARANA ORBIT', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONSULTAN', 1),
(15, 'CV. BINTANG KARYA ALAMI', 'KOMPL. BENGKONG BARU BLOK B NO.31 BATAM 29457', 0, '0778-423013', '\'bintangi_cv@yahoo.com\'', '0778-423013', 'Rominda Gultom', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(16, 'CV. ELANG ATK', 'Jl. Raden Patah, Komp.Lucky Permai No. 22 Nagoya Batam', 0, '0778-459246, 0778-459245', 'atk@elangfleksindo.co.id', '0778-459246, 0778-459245', 'Dian', '', '', 0, '0000-00-00 00:00:00', 'General Supplier', 1),
(17, 'CV. ISMAYA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA LAIN-LAIN', 1),
(18, 'CV. LINTAS SARANA MAJU', 'JL. H SALEH RT.05/02 KEL. BANDA BARU PAMULANG TANGERANG SELATAN', 0, '082114918037', ' Agus.sign@gmail.com', '', 'AGUS', '', '', 0, '0000-00-00 00:00:00', 'JASA PERCETAKAN', 1),
(19, 'CV. MANDIRI JAYA LAKSANA', 'Jl. Demak No. 10 - 11 Cimanggu Permai Bogor - Jawa Batam', 0, '0251-8312231', 'mandirijayalaksana@gmail.com', 'Fax. 0251-8312231', 'Dicky Jayalaksana', '', '', 0, '0000-00-00 00:00:00', 'JASA KONVEKSI', 1),
(20, 'CV. MARIANA JAYA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Pengangkutan Limbah', 1),
(21, 'CV. MIKA SARANA TEKNIK', 'Komp. Perum Kopkas PLN Blok C No. 10 Batam Centre', 0, '0778-5136555, 81364000490', 'ferryjonizar@yahoo.com', '', 'Ferijhon', '', '', 0, '0000-00-00 00:00:00', 'JASA PURIFIKASI OLI', 1),
(22, 'CV. MITRA BORNEO JAYA', 'Jln. Balao Lusuma Indah Blok A No. 2-3 Nagoya', 0, '0778-7803077', 'mitraborneojaya@gmail.com', '0778-433222', 'Unian tong', '', '', 0, '0000-00-00 00:00:00', 'General Supplier ', 1),
(23, 'CV. MITRA DINAMIS', 'Komp. Pertokoan Seruni Blok B No. 12 Sei Panas', 0, '0778-427933, 0778-427934, 0778-427935', 'ridhwanidrus26@gmail.com', '0778-431140', 'Ridwhan Idrus', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER SAFETY', 1),
(24, 'CV. MULTI PRIMA DAYA PERKASA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Training', 1),
(25, 'CV. MURACOM', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR EPSON', 1),
(26, 'CV. RAGIL PANCA PERKASA', 'Komp. Bintang Raya Blok C No. 18', 0, '0778-7283636', 'ragil_ppcs@yahoo.com', '', 'Pahrojim', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(27, 'CV. SURO DADI MAKMUR', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Kontraktor', 1),
(28, 'CV. TUMANTIIS BUDI MANDIRI', 'Puri Nuirwana 1 blok O No.12 RT.003 RW016 Pabuaran Cibinong Bogor', 0, '021-87910848', 'ab@tumantiisbudimandiri.co.id', '', 'Adnan', '8128128559', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR DAIKIN', 1),
(29, 'CV. WAHANA ARTHA EXPRESS', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA FORWARDING', 1),
(30, 'DANNY PRODUCTION', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Konveksi', 1),
(31, 'DEKOR ANTIK', 'Jl. Sultan Abdul Rachman, Komp. Sulaiman Plaza Blok A No. 1-2-3', 0, '0821-71588021', 'Yeny_koh@yahoo.com', '', 'Viny', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER INTERIOR', 1),
(32, 'DEWAN PIMPINAN PUSAT SERIKAT PEKERJA PT. PLN', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(33, 'DIAN NURSALIH BUDI', 'Education and QMS Consultant English and Autocad Drafter Trainer Batu Ampar - Batam.', 0, 'Telp.\'81328821009', 'diannursalihbudi@yahoo.co.id', '', 'Dian Budi Nursalih', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(34, 'DIGI VALVES', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR SAMYANG ARCA', 1),
(35, 'DINAS PEMADAM KEBAKARAN KOTA BATAM', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(36, 'DONALDSON FITRATION', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(37, 'ELBRIS ALLIANCE PTE LTD', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'General Supplier', 1),
(38, 'ELESKA HAKIT', 'Lembaga Sertifikasi Kopetensi Teknik Kelistrikan Indonesia Komplek PLTD Senayan, Jl Asia Afrika Senayan Jakarta Selatan', 0, 'Telp. 021 57851780', '', '', 'Sudibyanto', 'Hp. 08159884222', '', 0, '0000-00-00 00:00:00', 'JASA SERTIFIKASI', 1),
(39, 'FOSTER AND BRIDGE INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA SERTIFIKASI ', 1),
(40, 'GALERI WARNA', 'Komplek Nagoya Gateway Blok D No. 1 Lubuk Baja Batam', 0, 'Telp. 0778-433409', 'galeriwarna9188@yahoo.com', '', 'Budiman', 'Telp. 082383279188', '', 0, '0000-00-00 00:00:00', 'General Supplier pengecatan', 1),
(41, 'GAS TURBINE REPAIR OPERATIONS', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONSULTAN', 1),
(42, 'GHANDUR PONTIANAK', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Konsultan psikotest', 1),
(43, 'HJ KREASINDO', 'Marakash Square Blok B2 No. 8-9 Perum Pondok Ungu Permai Bahagia, Babelan Bekasi Jawa Barat 17610', 0, 'Telp. 021-889-747-33', 'iqbalkresnabitama@gmail.com', 'Fax. 021-889-742-68', 'Iqbal Kresna Bitama', '', '', 0, '0000-00-00 00:00:00', 'Jasa Fabrikasi ', 1),
(44, 'IAS TRAINING CENTRE', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Training', 1),
(45, 'IKATAN AKUNTAN INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(46, 'INDUSTRIAL TURBINE COMPANY - SIEMENS', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'jasa Rental Mesin', 1),
(47, 'INFORMA INOVATIVE FURNISHING', 'Nagoya Hill Superblock Ground F1 Jl. Teuku Umar Lubuk Baja Batam - Indonesia', 0, 'Phone 0778 - 1500382', '', '', 'Bpk. Rojak', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR  FURNITURE', 1),
(48, 'INSTITUT TEKNOLOGI BANDUNG', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(49, 'JAYA TEKNIK SARANA BATAM', 'Ruko Tiban Palapa AA No. 09 Tiban Batam - Indonesia', 0, 'Telp. 0778-7496137', 'jts.interior@gmail.com', '', 'Ira', 'Hp. 082170087373', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(50, 'KAWASAKI TRADING', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(51, 'KINGSINE TECHNOLOGY', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(52, 'KLINIK MEDILAB BATAM', 'Pusat Pemeriksaan Kesehatan Tenaga Kerja Komp. Taman Niaga Sukajadi Blok J No. 3A, 5, 6. Jl. Ahmad Yani-Batam 29433', 0, 'Telp. 0778-7372022, 7372023', 'cusomercare@medilab-clinic.com', 'Fax. 0778-7372024', 'Dr. Mariaman Tjendera, M.Kes', '', '', 0, '0000-00-00 00:00:00', 'Jasa Kesehatan', 1),
(53, 'KOMIGAS', 'Jalan Gas Lintas Negara KM. 3,5 Panaran, Tembesi, Kepulauan riau', 0, '082187836641', 'koperasikaryawanmitragemilangsejahtera@gmail.com', '', 'Trisnawati Siagian', '', '', 0, '0000-00-00 00:00:00', 'JASA LAIN LAIN', 1),
(54, 'KOSO CONTROL ASIA PTE LTD', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Original Equipment Manufacturer', 1),
(55, 'LAMBERT CONSULTING AND TRAINING', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(56, 'LEMBAGA AHLI TEKNIK MEKANIKAL ELEKTRIKAL INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Training', 1),
(57, 'MARKSHARE TRAINING', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(58, 'METZIN TECHNOLOGIES', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA ALLIGNMENT', 1),
(59, 'MUC REGISTERED TAX CONSULTANS', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Konsultan', 1),
(60, 'MULTIMATIC CONSULTANCY TRAINING AND SERTIFICATION', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(61, 'PLN LITBANG', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA SERTIFIKASI ALAT UKUR', 1),
(62, 'PPM MANAJEMEN', 'Jl. Menteng Raya No. 9 - 19 RT-01/RW-10 Kb Sirih Menteng Kota Jakarta Pusat - Indonesia.', 0, 'Telp. 021- 2300314', 'Email.daftar@ppm-manajemen.ac.id', '', 'Supriyanto', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(63, 'PRODIA OCCUPATIONAL HEALTH CENTRE', 'Menara Palma, Mezzanine Floor Jl. HR. Rasuna Said Blok X2 Kav. 6 Jakarta Selatan 12950', 0, 'Telp. 021-57957353', 'marketing@prodiaohi.co.id', 'Fax. 021-57957352', 'DR. Bertha Pangaribuan, M.SI', '', '', 0, '0000-00-00 00:00:00', 'JASA KESEHATAN', 1),
(64, 'PT PUTRA TIDAR PERKASA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA SECURITY', 1),
(65, 'PT. ADVENT NIAGA PRAKARSA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Kontraktor Boiler & Condenser', 1),
(66, 'PT. AGUNG AUTOMALL', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR VEHICLE', 1),
(67, 'PT. AHZA JAYA MULIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(68, 'PT. AIR INDONESIA', 'Air Indonesia Bulding Jl. Kelapa Kuning Raya CC-11, Rt.014/007 Kel. Pondok Kelapa Kec. Duren Sawit Jakarta Timur.', 0, 'Phone 021-54201777, 22223265.', ' arya@air-indonesia.com', '', 'Arya Maulana', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR OLI SHELL', 1),
(69, 'PT. APASES', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(70, 'PT. ARIESTA LOGISTINDO INTERNASIONAL', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA FORWARDING', 1),
(71, 'PT. ARTEKNOLOGI ORIENTASI INDONESIA', 'Jl. Raya Kranggan 99/B-25 Jatisampurna Bekasi 17433', 0, 'Telp/Fax. 021-21872769', 'corporate@arindo-pt.com', '', 'Rusdiyanto', '', '', 0, '0000-00-00 00:00:00', 'Jasa perbaikan PSV', 1),
(72, 'PT. ASTAR TESTING & INSPECTION', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA LAIN-LAIN', 1),
(73, 'PT. ASURANSI RAMAYANA', 'KOMP.MAHKOTA RAYA BLOK D NO.1', 0, '0778-7483375', 'm9nasution@gmail.com', '0778-7483376', 'Tri Wahyudi', '', '', 0, '0000-00-00 00:00:00', 'Jasa Asuransi', 1),
(74, 'PT. ASURANSI SINARMAS', 'Jl. Raden Patah Komplek Nagasakti No.2', 0, 'Telp. 0778-459218, 459214', 'mkt.btm@sinarmas.co.id', 'Fax. 0778-459213', 'Warni S Silitonga', '', '', 0, '0000-00-00 00:00:00', 'Jasa Asuransi', 1),
(75, 'PT. ATAMORA TEHNIK MAKMUR', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Perbaikan Mesin', 1),
(76, 'PT. AYIN ABADI', 'Komp. Mediterania Town House Blok G3A No.', 0, '0778-7067621', 'ayinabadi@gmail.com', '', 'Lamhot Parulian Purba, ', '081372229544', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(77, 'PT. AZTECH KARYA MANDIRI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER IT & PC', 1),
(78, 'PT. BAGUS CAKRAWALA NUSANTARA', 'Ruko Permata Niaga Blok B No.02 Kelurahan Sukajadi Kec.Batam Kota', 0, '082261170170', 'bcn.batam@gmail.com', '', 'Bayu', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR', 1),
(79, 'PT. BARATA SETYA SEJAHTERA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR', 1),
(80, 'PT. BARDA MULTI SOLUTION', 'RUKO PERUM. BATU AJI BLOK M.3 NO 1B BATU AJI BATAM', 0, '', '', '', 'DADANG KURNIAWAN', '', '', 0, '0000-00-00 00:00:00', 'JASA PENGETESAN TRAFO & ELECTRICAL', 1),
(81, 'PT. BATAM KONEKTRA JAYA', 'Kpmp. Century Park Blok F No. 8 Sadai Kec.Bengkong Batam', 0, '0778-7483212', 'my.riff@gmail.com', '0778-7483209', 'Muhammad Syarif', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR ELECTRICAL', 1),
(82, 'PT. BENWIN INDONESIA', 'KOMP. WORKSHOP TANAH MAS - 13', 0, '0778-463067', 'harismaw@benwinindo.com', '0778-463080', 'Hartono Isnawan', '', '', 0, '0000-00-00 00:00:00', 'Distributor', 1),
(83, 'PT. BOILERTECH INDONESIA', 'Kawasan Bintang Industri II, Lot D1 No. 612 Jl. Brigjen Katamso, Tanjung Uncang-Pulau Batam', 0, 'Telp. 0778-736 7322', '', 'Fax. 0778 736 7323', 'Yon Yon Ng', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR BOILER & CONDENSER', 1),
(84, 'PT. BRAVO ENGINERING', 'Blok D No 1 A, Puri Industrial Park 2000-Batam Centre', 0, '\'0778 748-2233', 'eli.ptbravoengineeringbatam@gmail.com', '', 'Eli/Alek', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR COBELCO & REWINDING', 1),
(85, 'PT. BUANA ELANG PERKASA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA SECURITY', 1),
(86, 'PT. BUKIT MAS JAYA', 'Komp. Baloi Point Blok B2 No 6-9 Baloi-Batam', 0, '0778-431261', 'marketing3@bukitmasjaya.com', '', 'Hertaty', '', '', 0, '0000-00-00 00:00:00', 'JASA KONVEKSI', 1),
(87, 'PT. CG POWER SYSTEM INDONESIA (POWELL)', 'Alamanda Tower 18th floor, Jl Tb Simatupang Kav.23-24 Cilandak Barat Jakarta 12430', 0, '021-29660055', 'azis.setiawan@cggloba.com', '021-29660054', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR  TRAFO', 1),
(88, 'PT. CHUCK INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA FABRIKASI', 1),
(89, 'PT. CITRA ALAM CAHAYA ABADI', 'Komplek Mega Jaya Industrial Park Blok B No. 18 Batam Centre, Batam - Indonesia', 0, ' 0778-4804972', 'pt.citraalam1@gmail.com', ' 0778-4804972', 'Donny', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(90, 'PT. CITRA CIPTA AVIGRA', 'Jl. Letda Natsir No. 37C Kp. Cikeas Nagrak Nagrak - Gunung Putri, Bogor 16967', 0, 'Telp. 021 22950101', 'marketing_cca@cbn.net.id, asepmuchtar71@yahoo.com', '', 'Asep', '', '', 0, '0000-00-00 00:00:00', 'Distributor ZOK', 1),
(91, 'PT. COMPACT TOTAL UTAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa sewa alat berat di pontianak', 1),
(92, 'PT. CONTROL SYSTEM ARENA PARA NUSA', 'Komp. Ruko Taman Niaga Sukajadi Blok H1 No.1 Suka Jadi Baram 29400', 0, '0778-7372221, \'081364575057', 'batam@pmcontrol.com', '0778-7372216', 'Lukman Simbolon', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR EMERSON', 1),
(93, 'PT. DARMAWAN RAHMADANTI', 'Cahaya Garden Blok E No. 3 Batam Centre - Indonesia', 0, '0778-7024422', 'Email. Ptdarmawan_rahmadanti@yahoo.com', '', 'Rodi Sonari', 'HP. 0811697749', '', 0, '0000-00-00 00:00:00', 'JASA PURIFIKASI OLI', 1),
(94, 'PT. DATAKU GLOBAL AKSES', 'Ruko Citra Indah Blok A1 No. 1 Batam Centre - Indonesia', 0, 'Phone 085364716555', 'burhan@dataku.co.id, sales@dataku.co.id', '', 'Burhan', '', '', 0, '0000-00-00 00:00:00', 'General Supplier IT & PC ', 1),
(95, 'PT. DESA AIR CARGO BATAM', 'Jl. Pahlawan Desa Karang Asem Timur Citeureup Bogor 16810', 0, '021-8753175', 'dacb@gjgroups.com', '021-8753167', 'Dian', '', '', 0, '0000-00-00 00:00:00', 'Jasa Pengangkutan Limbah', 1),
(96, 'PT. DHARAYA', 'Graha Dharaya Jl. Rawasari Barat E 277 Cempaka Putih Timur Jakarta Pusat 10510', 0, '021-42885887', '', '021-4280261', 'Dina', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(97, 'PT. DIELEKTRIKA PERSADA TEKNIK', 'Ruko Golden Boulever Blok E No. 28. Jl. Pahlawan Seribu BSD City Tangerang Selatan 15322', 0, '021-53154460', 'dielektrika@dielektrika.com', '021-5382765', 'Better Pasaribu. ', '\'0812800003869', '', 0, '0000-00-00 00:00:00', 'JASA PENGETESAN RELAY', 1),
(98, 'PT. DINAMIKA LUBSINDO UTAMA', 'Jl. Laksamana Bintan, Komp. Tanjung Tri Sakti Blok H No.2 Batam', 0, 'Telp. 0778-430040', 'zulkifli_ptdlu@yahoo.com', '', 'Zulkifli', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR OLI SHELL', 1),
(99, 'PT. DWI PRIMA ENGINEERING', 'Jl. Pangeran Jaya Karta 141 Blok II No. 141, Komp. Gedung Selamat Mangga Dua Selatan Jakarta 11730', 0, '021-6266272, 021-6266273', '', '021-6392804', 'Heri Suryanto, ', '\'081519833638', '', 0, '0000-00-00 00:00:00', 'Distributor Pump & Valve', 1),
(100, 'PT. ECOLAB INTERNATIONAL INDONESIA', 'Jl. Pahlawan Desa Karang Asem Timur Citeureup Bogor 16810', 0, '021-8753175', 'dsetyawan@ecolab.com', '021-8753167', 'Danang', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR CHEMICAL', 1),
(101, 'PT. EKASURYA MANDIRI', 'Komp. Limindo Trade Blok B No. 8-9 Batam Centre', 0, '0778-478600', 'antoni_ng@em.co.ic', '0778-478686', 'Antoni Ng', '', '', 0, '0000-00-00 00:00:00', 'Distributor ABB', 1),
(102, 'PT. ELANG DWI MITRA', 'jJl. Raden Patah Komp. Ruko Lucky Permai No. 22-23 Batam Indonesia  29432 ', 0, '0778-450193', 'yenny@elangfleksindo.co.id', '0778-45089', 'Yenny', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(103, 'PT. ENERGI MANDIRI PERDANA', 'RUKO KINTAMANI BLOK B NO. 19 BATAM', 0, '0778 - 765256', 'energimandiriperdana@yahoo.co.id', '', 'Isdeftiono', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(104, 'PT. EONCHEMICALS PUTRA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR CHEMICAL', 1),
(105, 'PT. ETERNITY PRIMA MANDIRI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Kontraktor', 1),
(106, 'PT. FAJAR MAS MURNI', 'JL. RAJA ALI HAJI KOMP. INTISAKTI BLOK G NO. 7-8 BATAM 29432', 0, '0778-454486', 'www.fajarmasmurni.com', '0778-454485', 'Yusuf', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR COMPRESSOR', 1),
(107, 'PT. FORMASI SISTEM INTERNASIONAL', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA FORWARDING', 1),
(108, 'PT. FRESH GM CONSULTANT', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(109, 'PT. FSR CAHAYA MANDIRI', 'Komp. Laguna Blok C5 No. 01 Batuaja', 0, '0778-7079523', 'fsrcahayamandiri@yahoo.com', '0778-7054476', 'Danny Silaban', '', '', 0, '0000-00-00 00:00:00', 'JASA FABRIKASI', 1),
(110, 'PT. FTS INDONESIA', 'Komp.Perkantoran Anggrek Mas 2 Blok A2 No.99 Batam Centre - Indonesia 29413', 0, 'T. 0778-4881918', 'Email. adminbth@ptfts.com', '', 'Teddy', '', '', 0, '0000-00-00 00:00:00', 'Distributor Micfil filter', 1),
(111, 'PT. GANDANG GARANTUNG INDAH', 'KOMPLEKS GADING REGENSI BLOK B2-16 LN.SOEKARNO HATTA BANDUNG-INDONESIA 40292', 0, '62-022-7316355', '', '', 'SUGANDA', '', '', 0, '0000-00-00 00:00:00', 'JASA KONSULTAN LIGHTNING', 1),
(112, 'PT. GE OPERATIONS INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(113, 'PT. GENTA RAYA CEMERLANG', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(114, 'PT. GLOBAL PETRO CEMERLANG', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa penyedia Solar', 1),
(115, 'PT. GOMAL SUKSES', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Pengangkutan Limbah', 1),
(116, 'PT. GUNA ELEKTRO', 'Jl. MI Ridwan Ra\'is No. 5 Jakarta 10110', 0, '021-3858183, 021-3858193', 'Novyanti.Siregar@gae.id', '021-3857338', 'Novyanti Siregar', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR', 1),
(117, 'PT. HAMON INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(118, 'PT. HAN INDO MILENIUM', 'Ruko Mahkota Raya Blok G No. 1, 2 & 3', 0, '0778-7496688', 'iis.susanti@han-indo.com', '0778-7497788', 'Iis Susanti/Feri', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR', 1),
(119, 'PT. HEMSINDO', 'Komp. Taman Laguna Blok B3 No. 12B Batu Aji Batam.', 0, '0778-3582219', 'dikky@hemsindo.com', '', 'Dikky08137200316', '', '', 0, '0000-00-00 00:00:00', 'Kontraktor ', 1),
(120, 'PT. HYDROMART UTAMA INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR WATER TREATMENT & FILTER', 1),
(121, 'PT. HYPROWIRA ADHITAMA', 'Jl.Raya Kebayoran lama No. 17D, Jakarta Selatan 12210', 0, '021-5361104, 021-5361105', 'masniaty@hyprowira.com', '021-5361517', 'Hendra Priyana, 8119502451', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR PALL FILTER', 1),
(122, 'PT. IKM INDONESIA', 'PURI INDUSTRIAL PARK 2000 BLOK C NO.10 A BATAM CNTRE', 0, '0778-469211', 'jennyann.oligodayag@sg.ikm.com', '0778-469233', '', 'ANDREY GUNATAMA', '', 0, '0000-00-00 00:00:00', 'Kontraktor Boiler & Condenser', 1),
(123, 'PT. INDI KRAN HYDRAULIK', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Sewa Tools balikpapan', 1),
(124, 'PT. INDOCLEAN DYNAMIC', 'Komp Lytech Industrial Park Blok F No. 3A', 0, '0778-7494880, 0778-7494881, 0778-7494882', '', '0778-7494883', '', 'Susie \'08127034281', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR OLI CASTROL', 1),
(125, 'PT. INFORSYS INDONESIA', 'KOMPLEKS EXECUTIVE CENTRE BLOK 2  JL. LAKSAMANA BINTAN KOMPLEKS EXECUTIVE CENTRE BLOK 2 ', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA IT & PC', 1),
(126, 'PT. INTERTEK UTAMA SERVICES', 'SINGAPORE TECHNICAL CENTER 1 SERAYA AVENUE SINGAPORE 628208', 0, '(65) 68967428', '', '(65) 68967429', '', 'MR. MARK CLONTS', '', 0, '0000-00-00 00:00:00', 'Jasa Lab', 1),
(127, 'PT. JATIM ASPEK NUSANTARA', 'Jl. Comal No.33, Surabaya ', 0, '(031) 5677063', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA SERTIFIKASI', 1),
(128, 'PT. KALIANDRA SETYATAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR BETWITH', 1),
(129, 'PT. KEVALINDO MEGAH PERKASA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(130, 'PT. KHARISMA MANDIRI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(131, 'PT. LAMBERT PERFORMA INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(132, 'PT. LANCANG KUNING SUKSES', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(133, 'PT. LESHACO LOGISTIC INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA FORWARDING', 1),
(134, 'PT. LUXINDO RAYA CABANG BATAM', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Distributor Vakuum', 1),
(135, 'PT. MAHARDIKA ANUGRAH UTAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA FORWARDING', 1),
(136, 'PT. MARDOHAR CATUR TUNGGAL GAYA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Konveksi', 1),
(137, 'PT. MCQUAY TRITUNGGAL PRATAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Distributor', 1),
(138, 'PT. MEGAMART INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Percetakan', 1),
(139, 'PT. MITRA BUMINDO PUTRA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(140, 'PT. MITRA MULTIDAYA UTAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Kontraktor', 1),
(141, 'PT. MOTOR GENERATOR SPESIALIS INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA PERBAIKAN GENERATOR', 1),
(142, 'PT. NETSOLUTION', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA IT & PC', 1),
(143, 'PT. NILAM SERVOTAMA BATAM', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(144, 'PT. NUSANTARA TURBIN DAN PROPULSI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Perbaikan Part Engine', 1),
(145, 'PT. OSC PERKASA INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(146, 'PT. OTOMASI BATAM SEJAHTERA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Fabrikasi', 1),
(147, 'PT. PATRA KUMBARA PERDANA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR OLI NYNAS', 1),
(148, 'PT. PENTRACO KARYA ADIPRESTASI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(149, 'PT. PEOPLE MEC', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR', 1),
(150, 'PT. PETROLAB SERVICE', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA LAB', 1),
(151, 'PT. PHITAGORAS GLOBAL DUTA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(152, 'PT. PM CONTROL SYSTEM', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Distributor emerson', 1),
(153, 'PT. POLY ARRAD PUSAKA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR CHEMICAL', 1),
(154, 'PT. PRAKARSA DAYA LESTARI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA RENTAL LIFT BUMB SKYLIFT', 1),
(155, 'PT. PRIMA KARYA NUSA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR OLI MOBIL', 1),
(156, 'PT. PRIME SAFETY INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING SAFETY', 1),
(157, 'PT. PROCHEM TRITAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR CHEMICAL', 1),
(158, 'PT. PROOPTEKNIK ALIGMENT INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Allignment', 1),
(159, 'PT. PROTEKNIK MEGA PERSADA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(160, 'PT. PUTRA BATAM JASA MANDIRI UTAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA RENTAL ALAT BERAT', 1),
(161, 'PT. PUTRANATA ADI MANDIRI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA ALLIGNMENT', 1),
(162, 'PT. RIRANA PRATAMA MANDIRI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR CAT COATING', 1),
(163, 'PT. SA ARY INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(164, 'PT. SEALINDO HARAPAN UTAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(165, 'PT. SELARAS DAYA UTAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Distributor Nynas Trafo', 1),
(166, 'PT. SIEMENS INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Original Equioment Manufacturer', 1),
(167, 'PT. SINERGI INTI MEGATAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR FILTER', 1),
(168, 'PT. SINERGI SAN PERKASA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Distributor AAF filter', 1),
(169, 'PT. SISTEM MULTI OTOMATIS', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Perbaikan /Install IT', 1),
(170, 'PT. SLS BEARING', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR NACHI', 1),
(171, 'PT. SUCOFINDO', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA SERTITIFIKASI', 1),
(172, 'PT. SULZER INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA PERBAIKAN ENGINE', 1),
(173, 'PT. SURVEYOR INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Konsultasi & Sertifikasi', 1),
(174, 'PT. TACHI TRAININDO', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(175, 'PT. TATA MURDAYA BERSAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA KONSULTAN', 1),
(176, 'PT. TATA WIGUNA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(177, 'PT. TIRTA PUTRA MALINDO SEJATI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR GRUNFOSS', 1),
(178, 'PT. TJOKRO BERSAUDARA SAMARINDAINDO', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Fabrikasi', 1),
(179, 'PT. TOSAN ALAM SEMSTA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Konsultan Sipil', 1),
(180, 'PT. TOTAL FLUID CONTROL', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(181, 'PT. TPE INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA PERBAIKAN PSV', 1),
(182, 'PT. TRAKINDO UTAMA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Distributor Cartepilar', 1),
(183, 'PT. TRANE INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(184, 'PT. TRIMITRA WISESA ABADI', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR BOILER', 1),
(185, 'PT. TUNAS KARYA INDOSWASTA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TENAGA KERJA', 1),
(186, 'PT. TURBO TEKNOLOGI PROFESSIONAL', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA INSPEKSI MESIN', 1),
(187, 'PT. TUV NORD INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA SERTIFIKASI', 1),
(188, 'PT. ULD LOGISTIC', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Forwarding', 1),
(189, 'PT. USAHA BARU BERSAMA JAYA', 'Komp Ruko Taman Carina III No. 12A-12B', 0, '0778-3581016', 'ubbj.pt@outlook.com', '0778-3581020', 'Densi/Icha', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(190, 'PT. VIONA DUTA SAMUDRA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA PENGURUSAN VISA TENAGA ASING', 1),
(191, 'PT. VIZTA MEGA JAYA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'General Supplier', 1),
(192, 'PT. WELDBRO INTERNATIONAL', 'Kompl. Bintang Raya Blok C No.11 Batam Centre, Batam - Indonesia', 0, 'Telp. 0778-4082812', 'Email.peter@welbro.intl.com', 'Fax. 0778-4080733', 'Pieter', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(193, 'PT. WILO PUMP INDONESIA', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR ', 1),
(194, 'PT.OTOMASI BATAM SEJAHTERA', 'Grand Niaga Mas Blok B No. 76 Batam Centre - Indonesia ', 0, 'Telp. 0778-4618594', 'otomasibatamsejahtera@ptobs.com', 'Fax. 0778- 4618594', 'Agus Sugiyanto', '', '', 0, '0000-00-00 00:00:00', 'Jasa Fabrikasi', 1),
(195, 'Rolls Wood Group', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'ASP (Authorized Service Provider)', 1),
(196, 'RUMAH SAKIT BUDI KEMULIAAN', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Kesehatan', 1),
(197, 'RUMAH SAKIT GRAHA HERMINE', 'Jl Jendral Suprapto, Ruko Asih Raya Blok B No.9-15 Batu Aji Pulau Batam-Indonesia', 0, '0778-363318, 0778-363127', '', '0778-363164', 'Dr. Dewi Kurniawaty, Pjs Direktur', '', '', 0, '0000-00-00 00:00:00', 'Jasa Kesehatan', 1),
(198, 'RUMAH SAKIT ST. ELISABETH', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Kesehatan', 1),
(199, 'RYAN PRINTING STICKER & ADVERTISING', 'Kios Griya Pratama Blok AA No.14 Kel. Buliang Kec. Batuaji', 0, 'Hp. 081365205588', 'Ryansticker,cutting@gmail.com', '', 'Ryan', '', '', 0, '0000-00-00 00:00:00', 'Jasa Percetakan', 1),
(200, 'SC ENGINEERING CO. PTE. LTD', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(201, 'SD TECHMOLOGY', 'Ruko Mega Legenda B2 No. 29, Batam Centre 29463', 0, 'Telp. 0778-7498075', 'marketing@batamonlineshop.com', '', 'Syam Jafar', '', '', 0, '0000-00-00 00:00:00', 'GENERAL SUPPLIER', 1),
(202, 'SEWOONG PLANT CO, LTD', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Distributor CCPP', 1),
(203, 'SHANGHAI SCIYEE ', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(204, 'SIEMENS PTE LTD', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'ASP (Authorized Service Provider)', 1),
(205, 'SOHARD EMBEDDED SYSTEMS', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(206, 'TAS ENERGY INC', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR', 1),
(207, 'TRAINER GLOBAL CONSULTANT', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING', 1),
(208, 'TRANSCANADA TURBINE', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'ASP (Authorized Service Provider)', 1),
(209, 'TURBINE TECHNOLOGY SERVICES', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Konsultan Engine & PT', 1),
(210, 'UNICON GLOBAL CO,LTD', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'OEM', 1),
(211, 'UNION SERVICE', 'KOMP. Pergudangan Nagoya Indah Blok A no.8', 0, '0778-452268', 'union_service89@yahoo.com', '0778-426841', 'Vanny 081267571705', '', '', 0, '0000-00-00 00:00:00', 'JASA REWINDING', 1),
(212, 'UNIVERSITAS INTERNATIONAL BATAM', 'Jalan Gajah Mada, Sei Ladi, Tiban Indah, Sekupang, Kota Batam, Kepulauan Riau 29442', 0, '0778 7437111', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA TRAINING FINANCE', 1),
(213, 'VBR TURBINE PARTNER', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Original Equioment Manufacturer', 1),
(214, 'VISUAL ART', 'Ruko Mahkota Niaga Blok A No.5 Batam Centre - Indonesia', 0, 'Telp. 0778-4165382', 'Visualart.digitalprint@gmail.com', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Percetakan', 1),
(215, 'PT RAYA FILTER', 'Komplek Bayangkara Paku Jaya No.11, Serpong Utara, Tangerang Selatan', 0, '021-40770090', '', '', 'Ikrar', '', '', 0, '0000-00-00 00:00:00', 'DISTRIBUTOR FILTER NORDIC', 1),
(216, 'PT. INPESCO', '', 0, '', 'Hp. 0811701887', '', '', '', '', 0, '0000-00-00 00:00:00', 'JASA PEMBASMI HAMA', 1),
(217, 'PT. MOMENTA', 'Komp. Bintang Raya Blok B8  Batam Centre Indonesia', 0, '0778-7483788', 'momenta.prima@yahoo.com', '0778-7483568', 'H.Reynold Febrian ', '081270898881', '', 0, '0000-00-00 00:00:00', 'KONTRAKTOR SIPIL', 1),
(218, 'PT HASKON', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'KONSULTANT', 1),
(219, 'DHL', '', 0, '', '', '', '', '', '', 0, '0000-00-00 00:00:00', 'Jasa Kurir Forwarding', 1),
(220, 'PT. International Tools & Equipment', 'Komp. Ruko Taman Carina Blok III No. 15 Kecamatan Batuaji ', 0, 'Telp. 081213158889', 'intertools@gmail.com', '', 'Tiwan', '', '', 0, '0000-00-00 00:00:00', 'General Supplier', 1),
(221, 'PT. Prima Agung Prakarsa', 'Palm Regency Blok B No.03 Batam', 0, '', 'primaagungprakarsa@gmail.com', '', 'Cristina', '', '', 0, '0000-00-00 00:00:00', 'General Supplier', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_master_vendor_verification`
--

CREATE TABLE `eproc_master_vendor_verification` (
  `id` int(11) NOT NULL,
  `verification` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_master_vendor_verification`
--

INSERT INTO `eproc_master_vendor_verification` (`id`, `verification`, `status`) VALUES
(1, 'NPWP', 1),
(2, 'SIUP', 1),
(3, 'SITU', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_material_catalog`
--

CREATE TABLE `eproc_material_catalog` (
  `id` int(11) NOT NULL,
  `code_material` varchar(150) NOT NULL,
  `material` varchar(100) NOT NULL,
  `catalog_category_id` int(11) NOT NULL,
  `status_delete` int(11) NOT NULL COMMENT '1= deleted, 0 = actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_material_catalog`
--

INSERT INTO `eproc_material_catalog` (`id`, `code_material`, `material`, `catalog_category_id`, `status_delete`) VALUES
(1, '000001', 'Welding Machine', 9, 0),
(2, '000002', 'Laptop', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mr`
--

CREATE TABLE `eproc_mr` (
  `id_mr` int(11) NOT NULL,
  `pt_id` int(11) NOT NULL COMMENT '1= meb 2=deb',
  `budget_cat_id` int(11) NOT NULL,
  `budget_dept_id` int(11) NOT NULL,
  `mr_number` varchar(250) NOT NULL,
  `request_sche` varchar(250) NOT NULL COMMENT '1 = schedule, 2 = unschedule',
  `request_type` varchar(250) NOT NULL COMMENT '1 = Material Req, 2 = Service Req',
  `request_checklist` varchar(250) NOT NULL COMMENT '1 = BOP, 2=CCPP,  3=Chiller, 4=DEB1, 5=DEB2, 6=MEB1, 7=MEB2, 8=TM2500',
  `budget_type` varchar(250) NOT NULL COMMENT '1 = Capex, 2 = Opex',
  `mr_date` varchar(250) NOT NULL,
  `project` varchar(250) NOT NULL,
  `posting_budget` varchar(250) NOT NULL,
  `drawing` varchar(250) DEFAULT NULL,
  `justification_drawing` longtext,
  `term_of_reference` varchar(250) DEFAULT NULL,
  `justification_of_reference` longtext,
  `picture` varchar(250) DEFAULT NULL,
  `justification_picture` longtext,
  `email_correspondences` varchar(250) DEFAULT NULL,
  `justification_email` longtext,
  `drawing_attach` varchar(250) DEFAULT NULL,
  `term_attach` varchar(250) DEFAULT NULL,
  `pic_attach` varchar(250) DEFAULT NULL,
  `email_attach` varchar(250) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1= open 2=rejected, 3=approved',
  `manager_on_duty` int(11) NOT NULL,
  `manager_app_status` int(11) NOT NULL COMMENT '1 : open, 2 : reject, 3 : approved',
  `manager_app_date` datetime NOT NULL,
  `warehouse_id` datetime NOT NULL,
  `warehouse_app_status` int(11) NOT NULL COMMENT '1 : open, 2 : reject, 3 : approved',
  `warehouse_app_date` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_mr`
--

INSERT INTO `eproc_mr` (`id_mr`, `pt_id`, `budget_cat_id`, `budget_dept_id`, `mr_number`, `request_sche`, `request_type`, `request_checklist`, `budget_type`, `mr_date`, `project`, `posting_budget`, `drawing`, `justification_drawing`, `term_of_reference`, `justification_of_reference`, `picture`, `justification_picture`, `email_correspondences`, `justification_email`, `drawing_attach`, `term_attach`, `pic_attach`, `email_attach`, `created_by`, `created_date`, `status`, `manager_on_duty`, `manager_app_status`, `manager_app_date`, `warehouse_id`, `warehouse_app_status`, `warehouse_app_date`, `timestamp`) VALUES
(38, 1, 1, 32, '000001', 'Schedule', 'Service Request', 'Chiller', 'Capex', '', 'TEST 1', '1', NULL, '', NULL, '', NULL, '', '1', 'TEST', '-', '-', '-', '000001-email-20200225_095749.docx', 1, '2020-02-25 00:00:00', 3, 1, 3, '2020-02-25 09:59:31', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2020-02-25 02:59:31'),
(39, 1, 2, 32, '000002', 'Schedule', 'Material Request', 'CCPP', 'Capex', '', '-', '2', NULL, '', NULL, '', NULL, '', '1', 'justification', '-', '-', '-', '000002-email-20200225_170548.pdf', 1000050, '2020-02-25 00:00:00', 3, 1000050, 3, '2020-02-25 17:05:54', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', '2020-02-25 10:05:54');

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mrir`
--

CREATE TABLE `eproc_mrir` (
  `id` int(11) NOT NULL,
  `report_no` varchar(250) NOT NULL,
  `po_number` varchar(250) NOT NULL,
  `comments` varchar(254) NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `approved_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=draft,1=disposition,2=reject disposition,3=production',
  `total_submit` int(11) NOT NULL,
  `sign_approved` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mrir_material`
--

CREATE TABLE `eproc_mrir_material` (
  `id` int(11) NOT NULL,
  `mrir_id` int(11) NOT NULL,
  `receiving_detail_id` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `unique_no` varchar(200) NOT NULL,
  `date_receiving` date NOT NULL,
  `qty` int(11) NOT NULL,
  `qty_approve` int(11) NOT NULL DEFAULT '0',
  `qty_demage` int(11) NOT NULL,
  `qty_shortage` int(11) NOT NULL,
  `qty_over` int(11) NOT NULL,
  `remarks` longtext NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=draft,1=disposition,2=reject disposition,3=production',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_submit` int(11) NOT NULL,
  `checked_by` int(11) NOT NULL,
  `checked_date` datetime NOT NULL,
  `checked_sign` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mrir_material_document`
--

CREATE TABLE `eproc_mrir_material_document` (
  `doc_id` int(11) NOT NULL,
  `material_id` varchar(250) NOT NULL,
  `upload_by` int(11) NOT NULL,
  `document_name` varchar(250) NOT NULL,
  `document_file` varchar(250) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mr_detail`
--

CREATE TABLE `eproc_mr_detail` (
  `id` int(11) NOT NULL,
  `mr_number` varchar(250) NOT NULL,
  `code_material` varchar(250) DEFAULT NULL COMMENT 'id from material catalog',
  `tec_spec` longtext NOT NULL,
  `qty_req` decimal(11,2) NOT NULL,
  `uom_req` int(11) NOT NULL,
  `qty_stock` decimal(11,2) NOT NULL,
  `uom_stock` int(11) NOT NULL,
  `qty_order` decimal(11,2) NOT NULL,
  `uom_order` int(11) NOT NULL,
  `currency` int(11) NOT NULL,
  `estimate_price` decimal(11,2) NOT NULL,
  `vendor_winner` int(250) NOT NULL,
  `vendor_winner_update_by` int(250) NOT NULL,
  `vendor_winner_update_date` datetime NOT NULL,
  `status_approval` int(250) NOT NULL COMMENT '1 = waiting, 2 = rejected, 3 = approved',
  `status` varchar(250) NOT NULL COMMENT '1= enable 2=disabled',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_mr_detail`
--

INSERT INTO `eproc_mr_detail` (`id`, `mr_number`, `code_material`, `tec_spec`, `qty_req`, `uom_req`, `qty_stock`, `uom_stock`, `qty_order`, `uom_order`, `currency`, `estimate_price`, `vendor_winner`, `vendor_winner_update_by`, `vendor_winner_update_date`, `status_approval`, `status`, `timestamp`) VALUES
(141, '000001', '0', 'TEST 1', '1.00', 1, '0.00', 0, '1.00', 1, 1, '1500.00', 1, 1, '2020-02-25 16:48:55', 0, '1', '2020-02-25 09:48:55'),
(142, '000001', '0', 'TEST 2', '2.00', 1, '0.00', 0, '2.00', 1, 1, '2500.00', 16, 1, '2020-02-25 16:48:55', 0, '1', '2020-02-25 09:48:55'),
(143, '000001', '0', 'TEST 3', '3.00', 1, '0.00', 0, '3.00', 1, 1, '3500.00', 1, 1, '2020-02-25 16:48:55', 0, '1', '2020-02-25 09:48:55'),
(144, '000002', '0', 'test', '2.00', 1, '0.00', 0, '3.00', 1, 1, '25000.00', 1, 1000050, '2020-02-25 17:08:03', 0, '1', '2020-02-25 10:08:03');

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mr_quotation`
--

CREATE TABLE `eproc_mr_quotation` (
  `id_quotation` int(11) NOT NULL,
  `mr_number` varchar(250) NOT NULL,
  `id_vendor` int(250) NOT NULL,
  `reference_no` varchar(250) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `currency` int(11) NOT NULL,
  `attachment` varchar(250) NOT NULL,
  `timestamp_quo` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(250) NOT NULL COMMENT '1 = enable 0=disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_mr_quotation`
--

INSERT INTO `eproc_mr_quotation` (`id_quotation`, `mr_number`, `id_vendor`, `reference_no`, `amount`, `currency`, `attachment`, `timestamp_quo`, `status`) VALUES
(40, '000001', 1, 'TEST-002', '12334.00', 1, '000001-1-20200225_100048.docx', '2020-02-25 10:00:48', 1),
(43, '000002', 1, '1225', '5000.00', 1, '000002-1-20200225_170803.pdf', '2020-02-25 17:08:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mr_tabulation`
--

CREATE TABLE `eproc_mr_tabulation` (
  `id_tabulation` int(11) NOT NULL,
  `mr_number` varchar(250) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(250) NOT NULL COMMENT '1= enable 0=disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_mr_tabulation`
--

INSERT INTO `eproc_mr_tabulation` (`id_tabulation`, `mr_number`, `filename`, `created_by`, `status`) VALUES
(46, '000001', '000001-2-20200225_100048.docx', 1, 1),
(48, '000001', '000001-2-20200225_164342.docx', 1, 1),
(49, '000001', '000001-3-20200225_164342.docx', 1, 1),
(50, '000001', '000001-4-20200225_164342.docx', 1, 1),
(51, '000002', '000002-1-20200225_170803.pdf', 1000050, 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mto`
--

CREATE TABLE `eproc_mto` (
  `id` int(11) NOT NULL,
  `mto_number` varchar(50) NOT NULL,
  `project_id` int(11) NOT NULL,
  `module` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `mto_rev` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_by` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = draft, 1 = pending, 2 = rejected, 3 = approved',
  `remarks` varchar(250) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sign_approved` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_mto`
--

INSERT INTO `eproc_mto` (`id`, `mto_number`, `project_id`, `module`, `priority`, `mto_rev`, `created_date`, `created_by`, `approved_by`, `modify_date`, `modify_by`, `status`, `remarks`, `timestamp`, `sign_approved`) VALUES
(48, '000001', 6, 4, 1, 0, '2019-12-30 13:21:02', 1, 0, '0000-00-00 00:00:00', 0, 3, '', '2019-12-30 06:28:44', ''),
(49, '000002', 7, 3, 1, 0, '2019-12-30 13:22:54', 76, 0, '0000-00-00 00:00:00', 0, 1, '', '2019-12-30 06:39:37', ''),
(50, '000003', 6, 4, 1, 0, '2019-12-30 13:59:29', 1, 0, '0000-00-00 00:00:00', 0, 3, 'test', '2020-01-03 03:04:01', ''),
(51, '000004', 7, 1, 1, 0, '2019-12-30 14:07:02', 76, 0, '0000-00-00 00:00:00', 0, 3, '', '2019-12-30 07:32:11', ''),
(52, '000005', 2, 1, 2, 0, '2019-12-31 08:59:04', 76, 0, '0000-00-00 00:00:00', 0, 3, '', '2020-01-03 01:17:11', ''),
(53, '000006', 1, 2, 1, 0, '2020-01-07 09:17:14', 76, 0, '0000-00-00 00:00:00', 0, 3, '', '2020-01-07 02:22:16', ''),
(54, '000007', 1, 2, 1, 0, '2020-01-08 13:15:59', 1, 0, '0000-00-00 00:00:00', 0, 1, '', '2020-01-08 06:25:46', ''),
(55, '000008', 2, 3, 1, 0, '2020-01-13 08:49:13', 1, 0, '0000-00-00 00:00:00', 0, 2, '', '2020-02-07 01:55:45', ''),
(56, '000009', 6, 3, 1, 0, '2020-01-13 14:24:30', 76, 0, '0000-00-00 00:00:00', 0, 3, 'test', '2020-01-14 06:03:30', ''),
(57, '000010', 2, 3, 2, 0, '2020-01-14 08:57:41', 76, 0, '0000-00-00 00:00:00', 0, 3, '-', '2020-01-14 02:02:20', ''),
(58, '000011', 6, 3, 2, 0, '2020-01-15 09:55:33', 1, 0, '0000-00-00 00:00:00', 0, 1, 'asdasdasd', '2020-02-04 01:42:16', ''),
(59, '000012', 2, 2, 2, 0, '2020-01-17 15:07:26', 1, 0, '0000-00-00 00:00:00', 0, 3, '----', '2020-02-04 02:22:30', ''),
(61, '000013', 2, 2, 2, 1, '2020-02-07 08:49:01', 146, 0, '0000-00-00 00:00:00', 0, 2, '', '2020-02-07 01:58:06', ''),
(62, '000014', 1, 1, 2, 0, '2020-02-07 09:10:10', 146, 0, '0000-00-00 00:00:00', 0, 0, '', '2020-02-07 02:10:10', ''),
(63, '000015', 7, 6, 2, 0, '2020-02-07 09:12:38', 146, 0, '0000-00-00 00:00:00', 0, 0, '', '2020-02-07 02:12:38', '');

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mto_cat`
--

CREATE TABLE `eproc_mto_cat` (
  `id` int(11) NOT NULL,
  `initial` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = delete, 1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_mto_cat`
--

INSERT INTO `eproc_mto_cat` (`id`, `initial`, `description`, `status`) VALUES
(1, 'SS', 'SMOE Supply', 1),
(2, 'CS', 'Client Supply', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mto_detail`
--

CREATE TABLE `eproc_mto_detail` (
  `id` int(11) NOT NULL,
  `mto_id` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL COMMENT 'id from material catalog',
  `discipline` int(11) NOT NULL,
  `nett_area` varchar(10) NOT NULL,
  `nett_length` varchar(10) NOT NULL,
  `unit_wt` varchar(10) NOT NULL,
  `cont` int(220) NOT NULL COMMENT 'in %',
  `total_qty` int(250) NOT NULL,
  `certification` varchar(10) NOT NULL,
  `remarks` varchar(250) NOT NULL,
  `reject_remarks` varchar(250) NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_by` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = delete, 1 = active/pending, 2 = rejected, 3 = approved',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_mto_detail`
--

INSERT INTO `eproc_mto_detail` (`id`, `mto_id`, `catalog_id`, `discipline`, `nett_area`, `nett_length`, `unit_wt`, `cont`, `total_qty`, `certification`, `remarks`, `reject_remarks`, `modify_date`, `modify_by`, `status`, `timestamp`) VALUES
(140, 48, 46, 2, '1000', '1000', '1000', 9, 31, '3.1', '1000', '', '0000-00-00 00:00:00', 0, 3, '2019-12-30 06:32:28'),
(141, 48, 35, 2, '3.2', '252', '15', 7, 1, '3.1', 'TEST', '', '0000-00-00 00:00:00', 0, 3, '2019-12-30 06:28:43'),
(142, 48, 36, 2, '1500', '1500', '1500', 8, 45, '3.1', '15', '', '0000-00-00 00:00:00', 0, 3, '2019-12-30 06:32:44'),
(144, 49, 46, 1, '32', '23', '53', 16, 2, '3.1', '-', '', '0000-00-00 00:00:00', 0, 1, '2019-12-30 06:39:28'),
(149, 50, 35, 2, '1000', '152', '25', 9, 29, '3.2', '', '', '0000-00-00 00:00:00', 0, 3, '2020-01-03 03:04:01'),
(150, 51, 31, 2, '1000', '200', '30', 18, 1, '3.1', '-', '', '0000-00-00 00:00:00', 0, 3, '2019-12-30 07:32:11'),
(151, 50, 31, 1, '55', '22', '33', 44, 1, '3.1', '---', '', '0000-00-00 00:00:00', 0, 3, '2020-01-03 03:04:00'),
(152, 50, 36, 2, '3.8', '8.4', '9.4', 864, 2, '3.2', 'test untuk 03', '', '0000-00-00 00:00:00', 0, 3, '2020-01-03 03:04:00'),
(153, 52, 31, 1, '3.2', '2.3', '4.3', 15, 1, '3.1', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-01-03 01:17:11'),
(154, 52, 35, 2, '50', '30', '40', 15, 2, '3.1', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-01-03 01:17:11'),
(155, 52, 36, 2, '60', '20', '40', 15, 2, '3.2', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-01-03 02:23:06'),
(156, 53, 35, 2, '3.2', '2.5', '5', 15, 1, '3.1', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-01-07 02:22:16'),
(157, 53, 37, 2, '4', '5', '6', 17, 1, '3.1', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-01-07 02:22:16'),
(158, 54, 36, 2, '100', '100', '100', 100, 6, '3.1', '100', '', '0000-00-00 00:00:00', 0, 1, '2020-01-08 06:18:40'),
(159, 54, 38, 2, '500', '200', '5200', 15, 16, '3.1', '', '', '0000-00-00 00:00:00', 0, 1, '2020-01-08 06:23:52'),
(160, 54, 46, 2, '7821', '25', '7821', 10, 239, '3.1', 'TEST', '', '0000-00-00 00:00:00', 0, 1, '2020-01-08 06:25:29'),
(161, 56, 31, 2, '15', '15', '5', 1, 1, '3.1', '-', '', '0000-00-00 00:00:00', 0, 3, '2020-01-15 04:44:41'),
(162, 57, 40, 3, '15', '34', '24', 15, 4, '3.1', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-01-14 02:02:20'),
(163, 57, 45, 5, '32', '12', '5', 15, 2, '3.1', '---', '', '0000-00-00 00:00:00', 0, 3, '2020-01-14 02:02:20'),
(164, 56, 36, 2, '34', '33', '25', 15, 2, '3.1', '', '', '0000-00-00 00:00:00', 0, 3, '2020-01-14 06:03:30'),
(165, 56, 35, 2, '45', '45', '25', 10, 2, '3.1', 'test', '', '0000-00-00 00:00:00', 0, 3, '2020-01-14 06:03:29'),
(166, 56, 39, 2, '25', '25', '25', 10, 3, '3.1', 'test', '', '0000-00-00 00:00:00', 0, 3, '2020-01-14 06:03:29'),
(168, 55, 65, 3, '2', '3', '1', 4, 1, '1', '--', '', '2020-01-15 10:16:26', 1, 3, '2020-02-07 01:55:45'),
(169, 55, 64, 1, '300', '200', '400', 15, 29, '1', '--', '', '0000-00-00 00:00:00', 0, 2, '2020-02-07 01:55:45'),
(170, 55, 37, 2, '8', '9', '15', 30, 1, '3', '----', '', '0000-00-00 00:00:00', 0, 3, '2020-02-07 01:55:45'),
(171, 58, 64, 1, '32', '25', '13', 10, 3, '2', '--', '', '0000-00-00 00:00:00', 0, 1, '2020-01-17 08:06:45'),
(172, 59, 31, 6, '2', '3', '1', 4, 1, '2', '2', '', '0000-00-00 00:00:00', 0, 3, '2020-02-04 02:22:17'),
(174, 59, 65, 3, '1', '5', '3', 4, 1, '2', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-02-04 02:22:17'),
(175, 59, 64, 3, '1', '2', '3', 4, 1, '2', '--', '', '0000-00-00 00:00:00', 0, 3, '2020-02-04 02:22:17'),
(176, 61, 31, 15, '1', '2', '3', 4, 1, '3', '5', '', '0000-00-00 00:00:00', 0, 3, '2020-02-07 01:58:06'),
(177, 61, 64, 2, '10', '20', '30', 40, 2, '2', '50', '', '0000-00-00 00:00:00', 0, 2, '2020-02-07 01:58:06'),
(178, 63, 31, 3, '1', '2', '3', 4, 1, '3', '5', '', '0000-00-00 00:00:00', 0, 1, '2020-02-07 02:20:30'),
(179, 63, 35, 10, '1', '2', '3', 4, 1, '1', '----', '', '0000-00-00 00:00:00', 0, 1, '2020-02-07 02:23:28'),
(180, 63, 36, 10, '2', '3', '4', 5, 1, '2', '-', '', '0000-00-00 00:00:00', 0, 1, '2020-02-07 02:23:57'),
(181, 55, 31, 2, '2', '3', '4', 5, 1, '2', '--', '', '0000-00-00 00:00:00', 0, 1, '2020-02-07 03:05:13');

-- --------------------------------------------------------

--
-- Table structure for table `eproc_mto_rev`
--

CREATE TABLE `eproc_mto_rev` (
  `id` int(11) NOT NULL,
  `mto_number` varchar(150) NOT NULL,
  `rev` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_osd`
--

CREATE TABLE `eproc_osd` (
  `osd_id` int(200) NOT NULL,
  `osd_no` varchar(200) NOT NULL,
  `po_number` varchar(200) NOT NULL,
  `project_id` varchar(200) NOT NULL,
  `create_by` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_by` varchar(200) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` varchar(200) NOT NULL,
  `delete_date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_osd_detail`
--

CREATE TABLE `eproc_osd_detail` (
  `osd_det_id` int(200) NOT NULL,
  `osd_no` varchar(200) NOT NULL,
  `project_id` int(250) NOT NULL,
  `discipline` int(250) NOT NULL,
  `receive_det_id` int(250) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `unique_no` varchar(200) NOT NULL,
  `over` int(200) NOT NULL DEFAULT '0',
  `shortage` int(200) NOT NULL DEFAULT '0',
  `damage` int(200) NOT NULL DEFAULT '0',
  `action_osd` varchar(250) NOT NULL,
  `remarks` varchar(200) NOT NULL,
  `date_action` datetime NOT NULL,
  `status_osd` int(200) NOT NULL COMMENT '0 Open / 1 : Close',
  `status_report` int(200) NOT NULL,
  `create_by` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_by` varchar(200) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_by` varchar(200) NOT NULL,
  `delete_date` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_osd_detail`
--

INSERT INTO `eproc_osd_detail` (`osd_det_id`, `osd_no`, `project_id`, `discipline`, `receive_det_id`, `catalog_id`, `unique_no`, `over`, `shortage`, `damage`, `action_osd`, `remarks`, `date_action`, `status_osd`, `status_report`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_by`, `delete_date`, `timestamp`) VALUES
(95, '000001', 0, 0, 0, 2, '5e426145b9473', 0, 0, 1, 'quarantine', 'asdasd', '0000-00-00 00:00:00', 1, 0, '1000050', '2020-02-11 16:44:44', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '2020-02-11 10:33:12'),
(96, '000002', 0, 0, 0, 1, '5e428a84d4a90', 20, 0, 0, '', '', '0000-00-00 00:00:00', 1, 0, '1000049', '2020-02-11 18:05:52', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '2020-02-11 11:05:52');

-- --------------------------------------------------------

--
-- Table structure for table `eproc_po`
--

CREATE TABLE `eproc_po` (
  `id_po` int(11) NOT NULL,
  `cat_id` int(250) NOT NULL,
  `dept_id` int(250) NOT NULL,
  `mr_number` varchar(250) NOT NULL,
  `po_number` varchar(250) NOT NULL,
  `description` longtext NOT NULL,
  `po_qty` int(250) NOT NULL,
  `price_per_unit` decimal(11,2) NOT NULL,
  `total_amount` decimal(11,2) NOT NULL,
  `id_cur` int(11) NOT NULL,
  `remarks_po` varchar(250) NOT NULL,
  `total_all_amount` int(250) NOT NULL,
  `diskon` int(250) NOT NULL,
  `ppn` int(250) NOT NULL,
  `grand_total` int(250) NOT NULL,
  `vendor` varchar(250) NOT NULL,
  `eta_date` date NOT NULL,
  `mr_detail_id` int(11) NOT NULL,
  `created_by_po` int(200) NOT NULL,
  `created_date_po` datetime NOT NULL,
  `modified_by_po` int(200) NOT NULL DEFAULT '0',
  `modified_date_po` datetime NOT NULL,
  `approved_by` int(250) NOT NULL,
  `approved_date` datetime NOT NULL,
  `status_approval` int(250) NOT NULL COMMENT '1=open,2=rejected',
  `manager_approval_status` int(250) NOT NULL COMMENT '1 = waiting, 2 = rejected, 3 = approved',
  `manager_approval_by` int(250) NOT NULL,
  `manager_approval_date` datetime NOT NULL,
  `bod_approval_status` int(250) NOT NULL COMMENT '1 = waiting, 2 = rejected, 3 = approved',
  `bod_approval_by` int(250) NOT NULL,
  `bod_approval_date` datetime NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = delete, 1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_po`
--

INSERT INTO `eproc_po` (`id_po`, `cat_id`, `dept_id`, `mr_number`, `po_number`, `description`, `po_qty`, `price_per_unit`, `total_amount`, `id_cur`, `remarks_po`, `total_all_amount`, `diskon`, `ppn`, `grand_total`, `vendor`, `eta_date`, `mr_detail_id`, `created_by_po`, `created_date_po`, `modified_by_po`, `modified_date_po`, `approved_by`, `approved_date`, `status_approval`, `manager_approval_status`, `manager_approval_by`, `manager_approval_date`, `bod_approval_status`, `bod_approval_by`, `bod_approval_date`, `status`) VALUES
(39, 0, 0, '000001', '2020/MEB/2020/0001', 'TEST 1', 1, '1500.00', '1500.00', 1, '', 3500, 1200, 200, 2500, '', '0000-00-00', 141, 1, '2020-02-25 12:56:26', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 3, 3, 1, '2020-02-25 13:43:44', 3, 1000077, '2020-02-25 14:01:35', 1),
(40, 0, 0, '000001', '2020/MEB/2020/0001', 'TEST 2', 2, '1000.00', '2000.00', 1, '', 3500, 1200, 200, 2500, '', '0000-00-00', 142, 1, '2020-02-25 12:56:26', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 3, 3, 1, '2020-02-25 13:43:44', 3, 1000077, '2020-02-25 14:01:35', 1),
(41, 0, 0, '000002', '2020/MEB/2020/0002', 'test', 2, '5000.00', '10000.00', 1, '', 10000, 5000, 10000, 15000, '', '0000-00-00', 144, 1000050, '2020-02-25 17:08:57', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 3, 3, 1000050, '2020-02-25 17:09:05', 3, 1000077, '2020-02-25 14:01:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_po_detail_old`
--

CREATE TABLE `eproc_po_detail_old` (
  `id` int(11) NOT NULL,
  `po_number` varchar(50) NOT NULL,
  `mr_detail_id` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `qty` decimal(11,2) NOT NULL,
  `price_per_unit` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_po_old`
--

CREATE TABLE `eproc_po_old` (
  `id_po` int(11) NOT NULL,
  `po_number` varchar(50) NOT NULL,
  `quotation` varchar(50) NOT NULL,
  `date_po` date NOT NULL,
  `date_quotation` date NOT NULL,
  `address` text NOT NULL,
  `reference` varchar(50) NOT NULL,
  `assign_to` varchar(50) NOT NULL,
  `mr_detail_id` int(11) NOT NULL,
  `created_by_po` int(200) NOT NULL,
  `created_date_po` datetime NOT NULL,
  `modified_by_po` int(200) NOT NULL,
  `modified_date_po` datetime NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = delete, 1 = active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_priority`
--

CREATE TABLE `eproc_priority` (
  `id` int(11) NOT NULL,
  `priority_name` varchar(50) NOT NULL,
  `status_delete` int(11) NOT NULL COMMENT '0 = deleted, 1 = actived'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_priority`
--

INSERT INTO `eproc_priority` (`id`, `priority_name`, `status_delete`) VALUES
(1, 'Primary', 1),
(2, 'Secondary', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_receiving`
--

CREATE TABLE `eproc_receiving` (
  `id` int(11) NOT NULL,
  `do_pl` varchar(50) NOT NULL,
  `ata` date NOT NULL,
  `atd` date NOT NULL,
  `date_created` date NOT NULL,
  `user_created` int(11) NOT NULL,
  `user_approved` int(11) NOT NULL,
  `user_approved_date` datetime NOT NULL,
  `warehouse_approved` int(11) NOT NULL,
  `warehouse_approved_date` datetime NOT NULL,
  `procurement_approved` int(11) NOT NULL,
  `procurement_approved_date` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_receiving`
--

INSERT INTO `eproc_receiving` (`id`, `do_pl`, `ata`, `atd`, `date_created`, `user_created`, `user_approved`, `user_approved_date`, `warehouse_approved`, `warehouse_approved_date`, `procurement_approved`, `procurement_approved_date`, `status`) VALUES
(67, 'DO_001', '2020-02-25', '0000-00-00', '2020-02-25', 1000050, 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_receiving_detail`
--

CREATE TABLE `eproc_receiving_detail` (
  `id` int(11) NOT NULL,
  `receiving_id` int(11) NOT NULL,
  `po_number` varchar(250) NOT NULL,
  `po_id` int(11) NOT NULL,
  `catalog_id` int(11) NOT NULL,
  `mrir` varchar(250) NOT NULL,
  `country_origin` varchar(250) NOT NULL,
  `brand` varchar(250) NOT NULL,
  `delivery_condition` varchar(250) NOT NULL,
  `date_manufacturing` date NOT NULL,
  `color_code` varchar(250) NOT NULL,
  `qty` int(11) NOT NULL,
  `qty_over` int(250) NOT NULL,
  `shortage_categories` varchar(250) NOT NULL,
  `qty_shortage` int(200) NOT NULL,
  `osd_id` varchar(250) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = delete, 1 = active',
  `mrir_created` int(11) NOT NULL COMMENT '0 = Not Yet; 1 = Already Created'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_receiving_detail`
--

INSERT INTO `eproc_receiving_detail` (`id`, `receiving_id`, `po_number`, `po_id`, `catalog_id`, `mrir`, `country_origin`, `brand`, `delivery_condition`, `date_manufacturing`, `color_code`, `qty`, `qty_over`, `shortage_categories`, `qty_shortage`, `osd_id`, `status`, `mrir_created`) VALUES
(3, 67, '2020/MEB/2020/0002', 41, 0, '', 'COUNTRY_01', 'BRAND_01', '', '0000-00-00', 'COLOR_01', 2, 0, '', 0, '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `eproc_receiving_document`
--

CREATE TABLE `eproc_receiving_document` (
  `id` int(11) NOT NULL,
  `receiving_id` int(11) NOT NULL,
  `attachment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `eproc_term`
--

CREATE TABLE `eproc_term` (
  `id_term` int(250) NOT NULL,
  `kepada_to` varchar(250) NOT NULL,
  `mr_number` varchar(250) NOT NULL,
  `po_number` varchar(250) NOT NULL,
  `jadwal_pekerjaan` longtext NOT NULL,
  `lokasi_kerja` longtext NOT NULL,
  `lokasi_pengiriman` longtext NOT NULL,
  `pembayaran` longtext NOT NULL,
  `lain_lain` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `eproc_term`
--

INSERT INTO `eproc_term` (`id_term`, `kepada_to`, `mr_number`, `po_number`, `jadwal_pekerjaan`, `lokasi_kerja`, `lokasi_pengiriman`, `pembayaran`, `lain_lain`) VALUES
(23, 'MAHMUD AMMA RIZKI', '000001', '2020/MEB/2020/0001', 'TEST', 'TEST', 'PT. Mitra Energi Batam \"MEB\"\r\nJL. Lintas Gas Negara, Trans-Barelang, KM. 3,5 Tembesi, Sagulung, Batam, Kep. Riau-INDONESIA', '14 Hari setelah hari setelah barang dan dokumen penagihan di terima oleh MEB.', '1. Semua pembayaran akan dilakukan setelah MEB menerima semua dokumen pendukung (invoice, PO konfirmasi dan dokumen pendukung lainnya). &#13;\r\n2. Dokumen penagihan dikirimkan ke MEB dengan alamat: Jl. Lintas Gas Negara, Trans Barelang, KM. 3,5 Tembesi, Sagulung, Batam. \r\n3. PO Konfirmasi harus dikirim kembali dalam waktu 3 hari kerja setelah PO dikirimkan. apabila tidak ada konfirmasi maka ... dianggap telah menyetujui semua \r\n4. Jangka waktu pengiriman terhitung sejak PO konfirmasi dari  ... . \r\n5. Pengurusan dokumen endorsemen PPFTZ 03 apabila di perlukan berikut biaya-biaya yang timbul menjadi tanggung jawab  .... \r\n6. Denda keterlambatan akan dikenakan sebesar  ... % per hari keterlambatan apabila barang diterima melebihi waktu yang telah di sepakati kedua belah pihak ( merefer  point a. syarat dan ketentuan dalam PO ini).\r\n7. ... akan memberikan Certificate of Origin dan Certificate of Manufacture.\r\n8. Garansi selama 24 bulan setelah barang di terima MEB atau 12 bulan setelah pemasangan di unit MEB.\r\n9. Pemasok wajib mematuhi semua peraturan yang berlaku di lingkungan MEB berikut peraturan mengenai K3L.'),
(24, '-', '000002', '', '-', '-', '-PT. Mitra Energi Batam \"MEB\"\r\nJL. Lintas Gas Negara, Trans-Barelang, KM. 3,5 Tembesi, Sagulung, Batam, Kep. Riau-INDONESIA', '-', '-1. Semua pembayaran akan dilakukan setelah MEB menerima semua dokumen pendukung (invoice, PO konfirmasi dan dokumen pendukung lainnya). \r\n2. Dokumen penagihan dikirimkan ke MEB dengan alamat: Jl. Lintas Gas Negara, Trans Barelang, KM. 3,5 Tembesi, Sagulung, Batam. \r\n3. PO Konfirmasi harus dikirim kembali dalam waktu 3 hari kerja setelah PO dikirimkan. apabila tidak ada konfirmasi maka ... dianggap telah menyetujui semua \r\n4. Jangka waktu pengiriman terhitung sejak PO konfirmasi dari  ... . \r\n5. Pengurusan dokumen endorsemen PPFTZ 03 apabila di perlukan berikut biaya-biaya yang timbul menjadi tanggung jawab  .... \r\n6. Denda keterlambatan akan dikenakan sebesar  ... % per hari keterlambatan apabila barang diterima melebihi waktu yang telah di sepakati kedua belah pihak ( merefer  point a. syarat dan ketentuan dalam PO ini).\r\n7. ... akan memberikan Certificate of Origin dan Certificate of Manufacture.\r\n8. Garansi selama 24 bulan setelah barang di terima MEB atau 12 bulan setelah pemasangan di unit MEB.\r\n9. Pemasok wajib mematuhi semua peraturan yang berlaku di lingkungan MEB berikut peraturan mengenai K3L.');

-- --------------------------------------------------------

--
-- Table structure for table `eproc_uom`
--

CREATE TABLE `eproc_uom` (
  `id_uom` int(250) NOT NULL,
  `uom` varchar(250) NOT NULL,
  `status` int(250) NOT NULL DEFAULT '1' COMMENT '1 : enable 0 : disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `eproc_uom`
--

INSERT INTO `eproc_uom` (`id_uom`, `uom`, `status`) VALUES
(1, 'PC', 1),
(2, 'PCS', 1),
(3, 'BOX', 1),
(4, 'LOT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `portal_department`
--

CREATE TABLE `portal_department` (
  `id_department` int(200) NOT NULL,
  `name_of_department` varchar(200) NOT NULL,
  `status` int(250) NOT NULL DEFAULT '1' COMMENT '1 : enable 0 : disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portal_department`
--

INSERT INTO `portal_department` (`id_department`, `name_of_department`, `status`) VALUES
(2, 'HR & Admin', 1),
(7, 'Finance', 1),
(11, 'Procurement', 1),
(19, 'Health, Safety & Environment', 1),
(30, 'Engineer + Maintenance Rehability (MR)', 1),
(31, 'Oprational (OPS)', 1),
(32, 'General Servics (GS) + IT', 1),
(33, 'None', 1),
(34, 'Warehouse (WS)', 1),
(35, 'Boat Of Director (BOD)', 1);

-- --------------------------------------------------------

--
-- Table structure for table `portal_login_slider`
--

CREATE TABLE `portal_login_slider` (
  `id_slider` int(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `filename` varchar(250) NOT NULL,
  `status` int(250) NOT NULL COMMENT '1 : aktiv, 2 : deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portal_login_slider`
--

INSERT INTO `portal_login_slider` (`id_slider`, `title`, `description`, `filename`, `status`) VALUES
(14, 'SAFETY START WITH ME', 'Best gift you can give your family is you please be safe', 'image_202001071354331.jpg', 1),
(15, 'QUALITY IS EVERYBODY\'S RESPONSIBILITY', 'Quality is not a program or a project; it isn’t the responsibility of one individual or even those assigned to the Quality Department.', 'image_202001071412071.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `portal_role`
--

CREATE TABLE `portal_role` (
  `id` int(200) NOT NULL,
  `name_of_role` varchar(200) NOT NULL,
  `status` int(250) NOT NULL DEFAULT '1' COMMENT '1 : enable 0 : disable'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `portal_role`
--

INSERT INTO `portal_role` (`id`, `name_of_role`, `status`) VALUES
(1, 'Manager', 1),
(2, 'General Manager (GM)', 1),
(3, 'Direktur Oprational / Direktur Finance', 1),
(4, 'Direktur Utama', 1),
(5, 'Direktur Utama Approvement MPI', 1),
(6, 'Staff', 1),
(7, 'Boat Of Director (BOD)', 1);

-- --------------------------------------------------------

--
-- Table structure for table `portal_user_db`
--

CREATE TABLE `portal_user_db` (
  `id_user` int(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `badge_no` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `department` int(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `sign_id` varchar(200) NOT NULL DEFAULT 'default.png',
  `id_role` varchar(200) NOT NULL,
  `status_user` int(1) NOT NULL,
  `last_update` datetime NOT NULL,
  `update_by` int(64) NOT NULL,
  `sign_approval` longtext NOT NULL,
  `warehouse_permission` varchar(250) NOT NULL DEFAULT '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portal_user_db`
--

INSERT INTO `portal_user_db` (`id_user`, `created_date`, `full_name`, `badge_no`, `username`, `password`, `department`, `project_id`, `email`, `sign_id`, `id_role`, `status_user`, `last_update`, `update_by`, `sign_approval`, `warehouse_permission`) VALUES
(1, '2019-07-30 09:37:23', 'Mahmud Amma Rizki', '10039372', 'mahmud', '827ccb0eea8a706c4c34a16891f84e7b', 32, 4, 'mahmud.ammarizki@smoe.com', 'default.png', '6', 1, '2020-02-10 13:17:13', 1, 'iVBORw0KGgoAAAANSUhEUgAAArwAAAD6CAYAAABDJJQbAAAgAElEQVR4Xu3dCXRcx3kn+v9Xt7FyBy1SoiiyG+gG0E2JI0dP3iRb1GLFsuVd3mbijP2SE4+TjCeeJPMyOZMZZ2aSzJkkdpJ5L2NnsnmeZ55jbZZs7bYla7MsWbFMiWgA3UA3F5HiIpAASKx963unbneTEAkQDaAB9PK/5zR7q1u36leX6K+r61YJuFGAAhSgAAUoQAEKUKCGBaSG68aqUYACFKAABShAAQpQAAx4eRJQgAIUoAAFKEABCtS0AAPemm5eVo4CFKAABShAAQpQgAEvzwEKUIACFKAABShAgZoWYMBb083LylGAAhSgAAUoQAEKMODlOUABClCAAhSgAAUoUNMCDHhrunlZOQpQgAIUoAAFKEABBrw8ByhAAQpQgAIUoAAFalqAAW9NNy8rRwEKUIACFKAABSjAgJfnAAUoQAEKUIACFKBATQsw4K3p5mXlKEABClCAAhSgAAUY8PIcoAAFKEABClCAAhSoaQEGvDXdvKwcBShAAQpQgAIUoAADXp4DFKAABShAAQpQgAI1LcCAt6abl5WjAAUoQAEKUIACFGDAy3OAAhSgAAUoQAEKUKCmBRjw1nTzsnIUoAAFKEABClCAAgx4eQ5QgAIUoAAFKEABCtS0AAPemm5eVo4CFKAABShAAQpQgAEvzwEKUIACFKAABShAgZoWYMBb083LylGAAhSgAAUoQAEKMODlOUABClCAAhSgAAUoUNMCDHhrunlZOQpQgAIUoAAFKEABBrw8ByhAAQpQgAIUoAAFalqAAW9NNy8rRwEKUIACFKAABSjAgJfnAAUoQAEKUIACFKBATQsw4K3p5mXlKEABClCAAhSgAAUY8PIcoAAFKEABClCAAhSoaQEGvDXdvKwcBShAAQpQgAIUoAADXp4DFKAABShAAQpQgAI1LcCAt6abl5WjAAUoQAEKUIACFGDAy3OAAhSgAAUoQAEKUKCmBRjw1nTzsnIUoAAFKEABClCAAgx4eQ5QgAIUoAAFKEABCtS0AAPemm5eVo4CFKAABShAAQpQgAEvzwEKUIACFKAABShAgZoWYMBb083LylGAAhSgAAUoQAEKMODlOUABClCAAhSgAAUoUNMCDHhrunlZOQpQgAIUoAAFKEABBrw8ByhAAQpQgAIUoAAFalqAAW9NNy8rRwEKUIACFKAABSjAgJfnAAUoQAEKUIACFKBATQsw4K3p5mXlKEABClCAAhSgAAUY8PIcoAAFKEABClCAAhSoaQEGvDXdvKwcBShAAQpQgAIUoAADXp4DFKAABShAAQpQgAI1LcCAt6abl5WjAAUoQAEKUIACFGDAy3OAAhSgAAUoQAEKUKCmBRjw1nTzsnIUoAAFKEABClCAAgx4eQ5QgAIUoAAFKEABCtS0AAPemm5eVq4SBbbu3r2mZWrqEuN711ixDUZ1ykKiorJFRTcZYJMCmyA4BAtVo9/P9vd+vRLrwjJRgAIUoAAFqkGAAW81tBLLWBEC11xzTcPR0dEtIQ1dItBLBOYSq3aLCN4EwSZVbBLRTVDZBA0C1k1wgSsQWmIFplsaZG1PT8/UEvPh7hSgAAUoQIG6FGDAW5fNzkrPJxCJdt0BI5fCmghgE4DEIdg5334lvq8AFvR/Tz2NZHt7syXmz2QUoAAFKEABCswQWNCHLuUoUKsC27dvb2lsWXubqtwGwa0AdqxQXccBpAAchOA1WD0KIxm1mhaR7wMwrhxW9R37070/WqEy8TAUoAAFKECBmhJgwFtTzcnKLESgo2NXVMXeCsEtCrwbwNrS9pdDgGagyKi7h4xDZFzEjsOaccCfEDHjYsy47/t2Zp7GeGes8Y+PNzYeP7p375mLHS8SSyQB7c4HvPJz+9M9Py2tfExFAQpQgAIUoMBMAQa8PB/qSsAFudbobaJ6mwpuu0jlLaAvCsw+hfaIYJ+KHWjxvMxKjaUNd8afEcU7XBmN9XYMDLxysK4ai5WlAAUoQAEKlEmAAW+ZIJlN5QoUg1yofS9E3jNrSQUnYfEkBM8Ya58dGOh7ZrVrFInF+wHEXDkmz7SuOXz4xbHVLhOPTwEKUIACFKhGAQa81dhqLPO8Aud6cu2HVOSmOXZ4EsD3RORZOzX2TDabnZg34xVMEInFTwHYAGAkk0q6e24UoAAFKEABCixCgAHvItC4S2UKzOjJ/QhE9swV5IrofSr6QKavr68yawKEw+FmaWhxF7QBipcz6eTuSi0ry0UBClCAAhSodAEGvJXeQizfRQXOBrnQ9yN/4dmFm8ozYuwDhSB3bzWQhjs7u0W9ZKGsD2RSydurodwsIwUoQAEKUKASBRjwVmKrsEwXFdjZ1RUx1rt9ngvPXlLV+4yxDwz2979QbaThjl3vEWMfynfw4r9nU8lfrbY6sLwUoAAFKECBShFgwFspLcFyXFTArXI2NDz+YRH9sAIfBdAwyw4HBbgHIt8Z7O9xc9hW7RaOJf6FQP97oQL/JpNK/nHVVoYFpwAFKEABCqyyAAPeVW4AHv7iAjuj3W83Ih+G4OPQWVc6G4Hi2yJ6f3OD+c5KTRm23O0WicX/K4DfdscR6PsHU73fXe5jMn8KUIACFKBArQow4K3Vlq3ienV0XHmFb+yHBfoRADfMXhW5H6rfMdpw/8DA3mNVXN1Zix6OxR8Q4L3uTWNNbGBgX7rW6sj6UIACFKAABVZKgAHvSknzOPMK7OyMf9gAH1ard4hIyyw7/BQid1k19+5PvVK8oGvefKsxQSQWPwDgCgC5TCo52/CNaqwWy0wBClCAAhRYFQEGvKvCzoMWBa7o6trmWfMLAvkMoPFZZE4ocKcA92ZSycfqQW7r7t1rWsenTwd15ZRk9dDkrCMFKEABCiyzAAPeZQZm9rMLhGOJtxropxT4TGFxhfMTfldU7s1NefceOPDyyXpybO/cda2qfb4Q8P5DJp38ZD3Vn3WlAAUoQAEKlFuAAW+5RZnfnAJuyIKofkAgrif3rbMkfEVV7zQh3DvY2/tyvVKGY92fEcjfBR28qr+fTfd+qV4tWG8KUIACFKBAOQQY8JZDkXnMHeTGrowb+O7iMzeV2JtnSXhEgW+ryHf29/cE887W+xaJdf85IF8IAl7I27Kpnh/XuwnrTwEKUIACFFiKAAPepehx31kFEolE48S0/ajCfBRQF+iev+Ug+CasfGdyrOW7hw+/OEbKcwKRWHwQQESA0cFUcj1tKEABClCAAhRYmgAD3qX5ce8ZApHOxFtE9SMq+OQcc+ZOK/DbnvXuGRh45SDxLhRwU7JZ47sZGqCCb2b7k5+iEwUoQAEKUIACSxNgwLs0v7rfu6Nj9xbf5D5SmDP33bOATCn0LgPcPZjqvafuwS4C4HrGx6fxBUDzq6qp/N/W6A8EaBSgCdAmQBoluEeTtWgSt+KcoBFAo7r3JFiBrhEWjSrBfu69/GsiDVDbCEiDAg1BWoV77PJsyL8fpM2nd2teLGpTH5AcgGk3rVpwU7he/XPP869PC5BTdxPkRM++77vXBJpTSE4EOVX4oi6d5kSlkF784LlLJyYnqjlVd+wFb9YAU2owDTVTCkwZhftyNgUPU1Z12lMEr6uqq8OSNoFcERzL6mvzZeR5nlG1zVBtBtAMNc2Ae6zNakyzWtees2/GYFKBUVWcNsAoVDdYY4Y837wyPW1er7eLQeez5vsUoEBtCyzyA622UVi7+QXCXbveI779SLACGrBhlj2eFMW3fc9+e39fX2b+HGs3RTh89UYbmt7mib1cRbcZYJsqLoXKVgi2AtgK91ywqXYVWLMKFLAAhgC8LoLXVfOPIfJ6cA89BivHFHrMh3e81Zs+1tfXN1qB9WCRKEABCswrwIB3XiImKArsLF6All/md/csMmkB7vZV79uf7v1RPci5Xtkz06bDg99uIR2i2Cmi2xTYhnO31hItdPG9qiUegcnqXcD1gLubARBaBMY4BMeg4lY3dD3UR1TtEQgOG+CwWBzJNcrh/cnkkUXkzV0oQAEKLJsAA95lo62RjPfsCYUPH71DVNwFaHfMUqtTCtwjKvdl0j3310it31CNHVddtSk0NdVh1XQIpAPQdig6gODmVkNbjm0KgjNQTAGYLN7cz+py7rl7L/jpXVw6E6SdUnVDBTR47PZXN3TAmCmoTrthA+7ndPfTvIGZdmnV6LTJ/1Q/ZRXTnudN+77vev8WvBljPGttyIiErKBBrA2pMSGxEnJDKFRs8FhFQqK2wbp7kZBa64IvF4QtaXN5uWEe+WNLMGzDQoN7CYZvSDBkQ1QbtDj8A4VhHVoY9hEM58gP/QiGi7jXzw3zWFL5LrLzGQDu4s0zCpwR6Bgg7rXFbi0KrBVgHYC1QPDrwWL+3rvzwA0/cduC2kiBfQLtUUWPwOzzIfsOpPf1LLZC3I8CFKDAUgQW8wdwKcfjvlUisDOaeLOBvQMi7qKpyIXF1vtFzX0hk7u/v7//RJVU66LFjEav7LDid6toN6x0Q9ANBLc3LbF+Q1AMwmBQFBmFnIDoKVgZBuwwjDkF8YdhvT8ozmrRIP4lteK6RDvuXiaBbduuaW1pmdzsG7vZCDarxWaBblZx93D3bxLr7uVNEN0CxRYAsy3xPXuJ3PhpEV9VPRHx5ip2MRAGpFdhDxvfHBgcTD5YpmoyGwpQgAKzCjDg5YnxBoFwZ/c/FxXXk3v7LDQ/Dnpzjb0v09fXV4107kO/oXWs2xPtVkg3oN3Qs8Gt6/1bzOZDMADoICCDUMkI7KCvJuPZicHBwcHh+TItLCd8CMBGQO/OpHpn602fLxu+T4GyCnR1da0b8xu2ePAvEcgWGN0Cd29xmYpeLueG7VxeYg/9RHDx3QWb/oWv3tfYA1zW5mNmFKDADAEGvDwdAoFwrPtX3GIHAuw6j+S0AN/wrf3W/oG+x6uJK9LV1QU/dBXE7hbFVSqyOxiOsLhtBAiC2gGBDKi4ezNojR3I9vZmF5flub3CnfFPiOKbwSsqH6zV4SFLdeL+lSsQTiQu9Sbt5b4nV8AibAQ7oQirIOz+xOS/zF0Y6RaHWrieX6vm4wx6K7eNWTIKVLMAA95qbr0ylD0f6JrfEKhb7vfs5j58oPhrbcA/VPoFKJ2dnW/ypWG379vdEFwl7oI6wVVu6q4FEyn6YZBUSFKgSVXpa5TcwHIPL4jEuh8G5OfdRUCZVNJd8MaNAjUlsL2z83IP3lvEyrUi+hZV3XN26IPgJBSbGPTWVJOzMhSoKAEGvBXVHCtXmEg0/ksq8psXBLqKb6uRv6rUZX4j0S43rvgyADsAcxWgbrYI93whm/tZtVeBpABJqEl60GQ63dNbuIJ9IXktOW1Hx66oFb8Xbtyjypcz6Z7fXHKmzIACVSAQjsX/jQD/KT9PNF5z0/MJ8M3BFBdcqYLmYxEpUFUCDHirqrmWXthwtNv1qvxHAO98Q26KbxjVPxsY6H1x6UcpTw7hzs5usd61EL0WkGsBuNucF8PMdlTXY2SAvRB9GSp7fWN7Km1e4Eis+48B+S1Xfqvyc/vTPT8tjyBzoUDlC4Q7dr1HjP4PQLcHc/9CtqjgU9n+ZH6IDzcKUIACZRBgwFsGxGrIIgge4X0Jik+8obyCvza+frUSAt0dsVi7gXeLALdA5Bb3E+cCbI8AshewL0Nkr6q8nE317HUx5ALyWPGke/bsCe1/9dirgLsYCM9nUsm3rngheEAKrLJAOJa4WqDuGoHCOF/5WSbVc/UqF4uHpwAFakiAAW8NNeZcVWmPdd+ukHvfONG8/i9j8ZXVDHTdCmRonNxjFDcosAfAvB9w+aVo9acKeRmKlz3P7PV0eu9yj7FdrtMk0hn/NBT/M8hf8cuZdPJvlutYzJcClSwQjsZ/QwRfKZYxk3IjjrhRgAIUKI8A/6CUx7FicwlHE38oov92RgG/Z4z50kDfvmdWodAm0pm4TmCvVzU3QHA9VNdcpBxu8YQfA/pjNxwBnv1Zpq/P9drWzBaJxf8RwJsBjOp0045s9qVTNVM5VoQCCxCIRqOX+NLgVnBz25lMKukWzOBGAQpQoCwCDHjLwlh5mbS3t29Qr/FbgNxaKN2Eqn4om+59ZCVLG/xUKfZ6UblOgRsBbJ37+G78nnlSgadU9en96aQLBmt2i3Qm3gnVJ4MKqv6vTLr3F2q2sqwYBeYRiETjn4bkf+1Q6Gezqd6/JxoFKECBcgkw4C2XZAXlsyO6K+EZ+yAUO12xRPHtwXTywytRxCu6urZ5Vm4wCIYp3IBgcYc5NsF+AE8B+qTCPpXt73ezJNTN1t4Zv1cVH8p/wMvbsqmeH9dN5VlRCpwnEInGX4bgSgCvZ1LJpa5uSF8KUIACbxBgwFtjJ0Qh2H367AVfKzAutL0zcb2q3oAgwA3G4s61YtkJ13sLyKO+mscOpl8ZqDH+kqvT3h6PqYeewrjqJzOppLPjRoG6FIhEu2+FSOHXJ/ntTKrnT+oSgpWmAAWWTYAB77LRrnzGQbAr9kcA1rujL9cUV+Hu7jAsboDiBgkCXInMUdvTEH0aKk9bq4/vH+h9duVVKvOI4Vj8LwX4vCudAO8eTCW/V5klZakosPwCkVjcDe1xUyWOTDeayKF9+4aW/6g8AgUoUE8CDHhrpLXPC3ZtJpVc0Hy18zGEO+MfEov3QeCC25svkj6tgu9AzKNnPH36eE/P6fnyrrf3d+y4apPXlHPLEbsvJj/NpJI/V28GrC8FigKFucELy5brn2RSvb9NHQpQgALlFmDAW27RVcjvvGB3enp8dMOhQ4fGl1oUNx43ZL1/CtiPFxZ+mD1LxU8geAgij2X6e55a6nFrff9IZ/fvQuUPXD2t4CP7+5NuyjhuFKhLgfZYfJ8CCbfSWqY/eXmlz51dl43ESlOgBgQY8FZ5I14RvbIjJL6bzcD1Fk6IL12Dgz0HFlut7bt2tTVO6m1q9P3IX1DVNEde31fFYwp5lCuDLUw7Eus+CMh2CPoz/cmuhe3N1BSoHYFwNPE7IvpHrkYC+/nBVN9Xa6d2rAkFKFBJAgx4K6k1FlGWcCx+SADXKzKukBsXc6X/2SBX7G0C+aACF8x/6RZ8AOQbbrowsfrU4GBv/yKKW/e7tEcT/0xFvxFAGPxypo8LTdT9SVGnAF1dXeum1OwPLrAV/CTTn3RLh3OjAAUosCwCDHiXhXVlMg1Hu58RkXcA6kO9T2bS++5ayJGDOXIVvwCjn4Fi84X7yiBgH7BiHtrf3/PQQvJm2tkFwrH4KwLsAuRQJtVzBZ0oUK8CkVi3myf8YwB8iLkm07/vZ/VqwXpTgALLL8CAd/mNl+UI4Vj8zwT4V4XM/3Mmlfy9Ug+0szNxm1H8EqAfnWWfEQjuFOCewf7kg6XmyXTzC7R3xt+rigdcSrf63WB/73+Zfy+moEDtCUSiu94PsfflpwnX/y+b6v2ntVdL1ogCFKgkAQa8ldQaJZZlZyz+PgO9DxBPFA8NppPvnW/Xjo74J9XgbRb6QYGEZ0n/mCjuVd+7J5N55eh8+fH9hQtEYonnAb0WgpONYnf29fWNLjwX7kGB6heIxLqPArIFgiOZ/uS26q8Ra0ABClS6AAPeSm+hWcpX/LBQ6GA21dtRShUisbjOki4rwD+oyD2Z/p7nS8mHaRYn0NG16yZr7ffd3gr8eTaV/I3F5cS9KFDdAueGYrn/DPbjmXTfndVdI5aeAhSoBgEGvNXQSjPKGInFHwNwCxRj02r+yaGBfen5qnBesOtDcTc8uXvnZVvueeKJJ3Lz7c/3ly4wY2L9SfGlcykzaSy9NMyBAqsjEIl1/wUg/zI4uuJbmXTyE6tTEh6VAhSoNwEGvFXU4uFY12cF5m+DzwrRf5stcQxoMeBVxe8Yi3sGB5OpKqp21Rd1Z0f3O4yRZwq9u9/MppKfqvpKsQIUWKBARyzxGQt1f7/c587BTCq5Y4FZMDkFKECBRQsw4F003Yrv6EVi8VcBbAXww0wquWfFS8ADLkogEos/AuDWfMArb86mel5aVEbciQJVKpBIJBrHp9WtutgAYMpT3JBOJ5+r0uqw2BSgQBUKMOCtkkYLR+MPiuA2QId1ujmczb50qkqKXtfF7OjovsYa+UmAIPh+pj95S12DsPJ1KRCJxs9A0OoqbxW/tj+d/Mu6hGClKUCBVRNgwLtq9KUfONKZeAtUfxz0EAr+dbY/+ZXS92bK1RSIdMb/AYqPB2VQ/flMuvfR1SwPj02BlRaIRONDEGwqHHdBUyiudFl5PApQoHYFGPBWQdtGYvFjAC4R4MRgKnlJFRSZRQQQje7e7sv0wULv7suZ/uRuwlCgXgSi0eh6XxrcFIfN+S988qeZdM9v1Uv9WU8KUKCyBBjwVlZ7XFCacHd3WHzJ5N/Q/5ZJ9X6hwovM4hUEwrHEVwX6uXzT4Rcz6eT/SxwK1INApDP+a1D8RbCAdnD+659m0r0Mduuh8VlHClSoAAPeCm2YYrEind2/DpX/lv/M0N/Ppnu/VOFFZvEAdHTs3mLNdHEBj6lMKtlEGArUukB7Z+JmVfwhoG8p1NWHyI2Z/p6nar3urB8FKFDZAgx4K7t9EI52f0lE/gMD3gpvqPOK194Z/6Iqvhy8bOW3MgM9f1pdNWBpKVC6wPbOzssb1Px7QH5lxl5DxjbEBwb2uiFZ3ChAAQqsqgAD3lXln//gDHjnN6rEFO2xeFoBtwrecKOxV3AZ4UpsJZapHALt0fjnIfiPCrypkN8oRH8z09/7P8qRP/OgAAUoUA4BBrzlUFzGPN4Q8AJfzaaSn1/GwzHrMgi0x7pvV8h3XFZq5avZgR62WRlcmUVlCYSj3XsE8kcQvO1syRTfmDb+7xzq73dzhnOjAAUoUDECDHgrpilmL0h7NP6rKvh/3LsC3DuYSn6kwotc98WLRBNPQ/Q6F+/6aq48kN7XU/coBKgZgXAicank9Peg+NVipRQYgDW/nh3Y93DNVJQVoQAFakqAAW+FN2d7LH6LAo8FxVSMQfChTCqZf86t4gQisfi73Ep4+YLJI5lUz3sqrpAsEAUWKRCOdf+KAP8JkC2FLKYA/LtMKvnHi8ySu1GAAhRYEQEGvCvCvPiD7IjF2j2EBgBMF5blzInIn1jkvp7t7+9dfM7cczkEIp3xb0PxwULet/LLyXIoM8+VFgi+yKn+IUTcLxfF7TFP5YvpdM++lS4Pj0cBClBgoQIMeBcqtgrpI7H4dwG877xDTwjw91bx9SzXpF+FVrnwkB0d8SutwcuFd17JpJJXVUTBWAgKLFIgGo1e4kvo9wD5l2ezELymwBez/clvLjJb7kYBClBgxQUY8K44+cIPGIlcuRUh3y0+0TLb3gJ801rzdY6fW7htOfeIdCb+FKr/2uWp0M9mU71/X878mRcFVlIgEo3/Egz+MxSXFo6rUPlKo+d/ibOOrGRL8FgUoEA5BBjwlkNxBfIoBL2/CMDdrpz1kKIPK+Tr7HlZgQY57xDbt29vaWhZdwDB1Ex6rG39mu0vvviiG4bCjQJVJdDR1XWdteYPANxwruD6gjH6xYG+vmeqqjIsLAUoQIGCAAPe6jsVvHCs+9NGzC+q6o3F4otITlVDhefPQfEN+N5dmcwrxdW+qq+mVVTi9ljiswr9W1dkAf7rYCr5f1VR8VlUCmBnNPFmz+ivqeKXznIoTqrI72ZTPV8lEQUoQIFqFmDAW8Wt1x6Lv08FvwjFx2dUYwJAc+H564DcDbV3Z9K9j1ZxVSu+6JFY/KcArgYwpZ52ZXt7sxVfaBaQAgAiXV27oeZzUPwLAOYsiuAbxvd+d2DglYOEogAFKFDtAgx4q70FAYRjibeK6KehwXCHdYUqnQawdkb1nnPBr3r2LgZj5W3086YiuzuT6rmjvEdgbhQov0BwkaXI5xT28yLizTjCSwL8u8FU8oHyH5U5UoACFFgdAQa8q+O+LEd1U5gZ9T4tYj4JaHf+IDoOSCOA4gfaOAR3qZW7s+me+5alIHWWaSTW/S1APhZUW+Rdmf6ep+qMgNWtIoGdsSvjHvzPKYKFIxrOFl3RryJfzqZ6vlZF1WFRKUABCpQkwIC3JKZqS/Qxrz2675Mq+ASg759R+tEZPcDu5b0A7vLV3M3VwBbXxtFodLsvDanCMJKfZlLJn3M5uaDCwN9urA4Vc7ai7WLNS4ODSZeeGwVWVKC9u7tTrRu6oL8GoGnGwdOi+MpgOukCXX9FC8WDUYACFFghAQa8KwS9WoeJdCbeAotPQPSTALYVyuFWR3Kb6/kNNgUeNIoHbEgf5JCH0lsrHO3+9yLy+8Eegn+e6U/+z3Cs+zMC+esZveozM8wZ23D5wMDeY6UfhSkpsHiBjo5dUWv8zwHy6zPG97sMMxD9ctu6NV/jjCKL9+WeFKBAdQgw4K2OdlpyKXfsuGpTqDFX6PWdOd0Qzu/1nRLFg2rwQE7sgwf7+g4v+eA1nEEkFndTkV0B4Lj4uE49/BWAPXNU2eYvCpL7M6me4mpsNazDqq2mwM6urohnjRu64BaNaD1bFsF+VXwF0+Nfy2az7iJXbhSgAAVqXoABb8038YUVbI/Fb1F1vb5uyMPZi9xcwvOD3zMQPKDQBxthH+jv7z9Rh1xzVrk91v1RhdyVT6DfBeSdADZczEiA0wqsFeg/G0z1/m96UqDcAu3tiR1q7OdE5AvuXJuR/0FR/bMzrY1fO7p375lyH5f5UYACFKhkAQa8ldw6y1y2SCS+EyH5gKjepoLbzjvcCID1M3qFTgJ4TBXPGmOfGezr+8kyF6/is4/E4j8E8K5CQTU/BW+wPZ9JJd86swIdHd3XWCMzzR7IpJK3V3wlWcCqESiMJ/8cgC+84f8ucBiQrzQa/2tcIa1qmpMFpQAFyizAgLfMoNWaXX6cn942e/Crw4Cc13MpgxB9FhZPGPUerbe5OsOdnd2iXvINwa7iZDCdUzr5l7OdB+FY998K5LPF90TknYP9PU9X6znDcleGQLi7O2ys+ZeHhuMAACAASURBVIyq/isAG8+WSvGaAn+GXNPXstmXTlVGaVkKClCAAqsjwIB3ddwr+qgXD34xVpjK6Nx0RvnaJAF9DCI/sVZemR5r6Tt8+EWXtia39s7EH6nq78yoXNrT6Xek0+njc1U4Eu2+FSKPACiM5cV3M6nkzFk0atKKlVoegUi0+/2A3FEYmjRj1gU5BsGfTzfIVw/t23d2lpDlKQVzLVUgGo1ekoP3Ditm7EAq+Vip+zEdBShQHgEGvOVxrNlcisGvQvdI/uf7N51X2XEALbMBKLDPAC9b1T5jZFgVLxrrZZqa7NGenp7iTBFVaReJxd30TflVqQT7M/3JcCkVOW8YBPzJUNuBAy+74SJv2Pbs2RM6cuRISy7X1GLtdIs2+C3QULOqbZE3TimVL4KRHaI6JBZHSikH06yCgCc7rMVad75ARAVQ8a3NCVQgGrzmHotvJSeaE1Gg8JrxrbjnvuwAzLsB+14FYjNroYpTMPg7k7P/2zbKCZl0+7qbUc+bsuMiaoxRGS++NmHd8zPuNc9TM2o0FDoTvOZ5XnB7PRRS70SDNja+bkOhkDY2NmpLS4u+uG6d4okn3Bc3N5SnOJzH7NmzR0ZHR2V4eNjkcjlxt+m2NrM5lxPf94NbLrfWWOuLXWeldXraWGtFtVV8P2e0RcU9b/J9o9osqlZsozWqKmqtNGqTWOsbNKq416xtCN5rRLCf0QYVaEisVeN4Q24/zxpFSFTh8jNQSCjkBfurqoGRHZrTHERfK3oaYzy1/hoVWSMqa1SwRkTWQLVVBa1G0aKKVoi2ANKirgIiLdCgfV0vu1vt0s2C476IuCXf3Tzoxc/bqUwqOXNauFU4GXlICtSfAAPe+mvzxdd4z55Q5NWjN0J1DyS4QMvdzt9yANytuLzx7MdTnITAfcAcheIoRI8CclyBUaMy6u5VZdQL5UYtENyaVEettaPpdHpy8ZUoaU+zdffulk1TU825XGMQcKJRm9WaFrW2RVR2wuDrhZxUFL+uRlug4urcLIUPOlu4F0EjVN0HXJNAOhTYNaMUBxVwwX9xX5fOPXYfktwoQIHaElAFBrOpZLS2qsXaUKDyBRjwVn4bVWwJt+7evWbN+NSNVuQmUb0ekPh5yxmfX3bXI+SCYTccgufexVu20HPmPh8DKvdP8Vax5wQLRgEKXFwgk0rybx9PEgqsggD/460Cei0fsqPjyius5OIqEjfQuOaD4Le/YQnT0gHcz76+5n8yhbjfffUNPw2WntPCUxZ/ri3u6f6v5IcwcKMABSgwh4Cq+mJkQCCvqtVX3RAWVf1ek6cvcpYMnjYUWD0BBryrZ19XR04kEmsnJ+VS9fRStf6lMHIpVLZCcCkUlwLYHMwJrFgHCeYGdrfzL4yrBDM3/OBcD7VIFqpuDO6EiE4oMAmIG3Lhbi5t8FgVU8Y9FveerlPFFwvvN0Lwl6L6A0DcIgCTYsw4cv7EtHjj4uXGxfPGzcTEuDFmYgWGc1SCcd2VIRxLXC3QOwD5FKDtFwLonVBzVybd4+Z9dl/Gqnbbvv3tLd66UxvF9zdAQxtE7QbAbIDRDYBugLoZYdxj2SCKDTCyQVU3Fl8rzHW90kN+3PzZRyGFIVjBUCw9CjGvqRuO5etRK6GjE4326PGentNV2zgsOAVqWIABbw03brVXLRwON4dCoXVT0rLOE7tOoOustcv6QSciViQfcIoLOGVyYjwUGs81Nk4c3bvXXaBnixesqeJkNp1sW4xzJBZ/vLgimwL3ZFPJjy4mH+5TvQLbt29vaWhedwckCHQ/MEtNDgL4KxX/rmx/f28l1NT9nxRZu0GachtgvQ0IglUXpJoNgN0IYzZA88Gqe926xyIbJL8gS/F2dknzJdSpGPQv9leX1wG45b3dtQMnBHpCgeBmVE4o7HEPcsJaOTEx0XKilmecWUIbcFcKVJUAA96qai4WthIECgEvMqmku/J6UVs42v0lEfkPxZ09bbgind57aFGZcaeqEtjZGf+wp/g4INcp1C1L/YbNfQESla9n0j33l7NiXV1d61R1/YRt2GBg17uAVPI9q8XH6/PBqq63rnfVBaiuh1VcoFoMYoNZB5a6zZzZoRyfQVaBIwK4IHZIgSFRDIm4x3IcosfU6lGFOWY9/+jByy47hieecNcScKMABepIoBx/bOqIi1WlQHkEwtHuPSLyuAA5dTMyqHw8k+65szy5M5dKE4h0dXXBmo8DLtDFleeXT4BXFfiqevqNbG9vdub711xzTcPo6OiGcdsYBKoC6wLVIEiFcashup/+XUCK9RZwgapbITF4HgSrLmjNv7bY3tCZxSkGq+61cuTnhv0Mu5u6e9VhiAwb6ClAgsewwcI3wxBbuPeHkTPDqqFh1dPD2WzWDQXiRgEKUOCiAgx4eYJQYJUEIrF4BkBx/l4uQrFK7bBch21vj8fUwycU8kmBzpyK7twhBX0Aet0cymd7VSH5nlYXzOaD1VnnuV5EuYvDAMo128e0C1SlELAWg1MVF8AWg1QdhpVhwJwKAlYXwIo/rJ43nBsZGT506JAbJsSNAhSgwLILMOBddmIegAKzC7THEn+n0M8U3z3dIOt4wUtlni1u7Krf1NTW5HubfIM2tWgzgjaI3aQqbQq0ibrnuFyAiOYXaCnHz/8OpNy9qi5Pd/Gk6zEdcT2rxvWsusf5YHUk6FEt9qpaM6zI966qeMPWTA9PNTUNH92798xytJZbKtnle35P93Ici3lSgAL1I8CAt37amjWtMIFINPExiH6rWCxr9br9A73PVlgxa6o40Wh0fS4UatOctHmCNlXZpOKCV9umQcAqm1zg6gJYFJ4D6i5MbK0gCDcEYCQYBiAYMTYYDjAS9J4GQax7zwWntnBvRkI5fxjw3G1EZGx4OWb7cEMvXn99fLMNSZtndLNCN4vFZhVshupGEaxVYK24m8padbOxCLaoYrPkZ2V5w8VsnK+2gs44FoUCNSDAgLcGGpFVqF6BSKw7B0hw8ZtCPp9N9Xy1emuzYiX3otFomw2FNtmctJmgx1Vc7+omVdsmQc+ruIB1E1zgWryputcXe6FhsZd1KeNWLaAnLt6rihF1QavIsEJGQn7QszriglXVsZHBwUEX0K7otn3XrrbQpL7TAC6IFRjZCrVbC9MKbgWwNZhaUALv8myqz2TSvdeXJzPmQgEKUICrXfEcoMCqCrRH4/eq4ENBIQR/k+lP/vKqFmgFDx6NRpumGxraGqalbeYwARXbBos2ESn0uuZ7XAVnA1g3rnWxWzkCV3eF/1zT4x0H9B9F8ayKPCG+npwK2SGMjQ1V4njVSOTKrWjQS9XHZcboZW5ebKvBzBE73U2AHYU5sUvxXupUYYX/BjgxmEpeUsoBmYYCFKBAqQLs4S1ViukosAwCM6cnU2BfNpW84Ar+ZThsWbN0S0yvPWPbrKdt1s+1eUba3DAB1+vqxrm6oQLuuZzrbS32vK5ZQkFccLWUi69c4DtUvLlprFR0CGpOisGQVQ2mtRIrbsqrBET/DwA3zrEYSp8A9/nWPrx/oM/Nr7zqm1voZSyX224QutzCbheYy1V1u0AuV+jlBrhUgcvKNNPCUuubVOAlqPYjhL/n2N2lcnJ/ClBgNgEGvDwvKLCKAjujiTcb0X8EcArARqj9WCbd51bTWvGtvb19g9/Q0BbKSZstBK3G5oNVqLSJG+caBK2yqdDjWhwusNiLs8rR2zqFs3OuugBWhwRmyIqeDOZiVQxZ4wJXHRJPh3K2cQiTOHngwMtudbxZt2h093bfTN+kKj8vULcgxNoLE0qvCr4Nax/Opnt/uJKN5YYYNLhgVr1iMLvdngtmg6AWcCuTlXXzAbhZRV4D4EGxFgKB4vIFDGVwK5C9BJWXAP2ZePalZs/b29PT46Ym40YBClBgWQUY8C4rLzOnwPwCkVj8UQDvLqTMCfDlnJqvH0jv65l/7zemcLMJoLV1I3K5jWJDu1Vsq1Ez7YYJuJ5WI/mLtIILsmaOb80/Xs3xre6K/3zAKmZIFUMo9LJaxUkxGgSvvtUh44WGjC9Dp9eYoXLNFNDR0X2NNXITIDcDevMcQxaScItCiDw82N/z9ELbppT07sKvkyfPRNQgrGIiEDesIOiZ3e56aAFcXuYL6E5DkYFoRiAZDeYD1uNG5BhgjlvjHx9vbDzeOpHbpcCHRd3wG+2ety6C16D4ST7AtT8zGnppYGBfet79mIACFKDAMgkw4F0mWGZLgVIF2qOJ/1NF/waA6+lyF0W58aEu8HU9hyloMFfrFIy4ZVs3KmSju4fA9eK58azuvnhbbG+rK+7Sx2AKTkL1JFSG4IYEuJWvxAWvhd7WoOfVDLlhAzkrQ6ZRh1qBoZXu5XNfDExjy01q5SZIEOBePVt7CdCjwN3W6sNLmkFjz57Q5a+9tsHz/Q2iDRvc4hEQEzaCiFp1gW0EkDAQBLXzba5nvLS/3Rr0yLoV/F519wocFE8yopIxdjKbTqePz3WwcCzxVoG+TwQfUsVVFynUqKq+YIx5HmpfEBt6YWDgFbcsMjcKUIACFSNQ2h/NiikuC0KByhZwy7fmcg0btSG3Ua3ZKGo3qJqNMHYjtBCoBoGrbAx+dlYUA9f2Mo2nXPIwgcLqb6cEckqhowBOQ/UMBGfc9FcCjAIyCmiTm/7KBU8Kf9TNKlBsHQUmRcw4JDch0964MQ3jodDk+GWXXTb+xCot6xqJxHdKSG5S0ZtgcYu7QGvWs0nwE7X6gCoeXdNk9uZyuWZrbbNqUzNgmzVkm2FDMQu/Kd977sYp283B7BDBfLzBGGY3Ttl9GSneSh2vXGow64YYvKqCQxIEtHJIrH1VYQ4Z9V+11hzKZK48BNzp0pW0ubHYrWOTN4uYmyxwuwAds++ovqh8x7ovZCJPZFM9L5V0ACaiAAUosIoCDHhXEZ+HrjyBcPjqjWie2CjW26DW3wgUg1WzAWo3whSCVshG41bDEsn3uObHTLrbUqatqjyQVSpRIeh2K3nlb4JpqHss04Cee929pzINCV67YFNgvQRfLGRToUd8tr95brqwyXzewRYqBPOLHeJxfjkWeoHdKBRHIDgM6KsCOaSihxRyyMB/1fe8Q/uTySPlaJqdXV0R45ubRXCTCt5TmMpttqzdEI7HrbWPV8qFeeWoP/OgAAXqR4ABb/20dU3WNJFINA5bu64BWGeAdTKNdb5gncCsd5PZW9F17j547O7dZPfu3uo6dUu4Fp4Xpl5y71XKNgWoV5yjd45CKaBWRFRVCzMWSDkCbhegubzdL+fFmRDq9W9FweJsCziHxRq/DuAYgKMKHBGVwzD2iCgOW8URE8LhBtUjfX19rld92bYrurq2eVbeL5DbAbjbbNspAR620MeNL48PDiZTy1YgZkwBClBgBQTq9UNsBWh5iLkE3JRJZ1TXeaozAtR8kGqDYFXzQSmwLliNKQhQzz5ePyM4de8tZczqcjRSCUu2lr64QHssfotVuUmg7mr4y+GmlVIbk8UvoLAcda7HPN10ZUcEcEHskCI/RtlNZaaQ4xA9plaPKswx6/lHD1522TGs0lAO1zhuBg6Y5tshertVfEBk1pXjTgH6gKh5aKpJHjq0b5+bto0bBShAgZoQYMBbE824vJW4JJFYu3bcrMl5WBvyptdoziTEQ4OojlmYIAB1QeocAWoQsOZ/Ws4HryVfcLO81Zot94pcsnW2grqe7dOet6ZhTNfmDNZ6ZnqN9WWW6bMWhijGjImx4yYXGpv0psdlvHG8sXF8rGxL0e7ZE9o6NNTU4PtNnrVNsLZZVJsEDU2q2izQJhFpsCIN4vsNIsg/tpJ/DNsAlVYj2m0VLvB3Y5/Dc9RyUiGDAjsAYFAVwVRkYoybysxdIDipsJPugkADuHs3rGFS8xcP5scgG38CUzIh4k2ITEw0NjaOb9myZWK1xiEvrDXhhaOJ28Xo7QhmV8CbZtmfQe4CUZmcAhSoTgEGvNXTbiaRSLROToZa/YapVpvzWhvUb7GeaUUhUMj3dnpN+fGHaLKqzfnXXECBJhjTDGizKpoBCR4DaA72F2kBtBXuXrUVKDwP7su+lXphTqkHdvN7ugumigHrSH75Vh027nVxF1PZYKlWWPe6e4yRBpgRVTMiMjGcTqfPXnBV6kGZbuUEwrHE1QK8E7DvgsrNc8/9KoOA/a6q+UGT5/9guYcHrJxA6Ufa2Zm4zai9TeGGLcz6ZeAEFA+JmIenGvEwe3JLt2VKClCgegUY8Ja17T7mhcOpdZ6XW+d63YzYfK+n9deoyBpRWaOCNSKyBtYG98FzxRqFrAHUXcm9phhsCtCq7qdHDX5+XM2f7ovjGJcyfnE26TEIRmCDYHUEgmGB+7nfPddhF7S6113g6oLWIHh1rxk7rJMy4vuhkQMHXnZBbnE6rbK2JjNbPYGd8fhl4uNdArwLilsBROcsjcozMHhQrf4gm04+t3qlXr0jnwty3ewKEimUZOYXy8MQPAQrjzR6/sP1+EVg9VqHR6YABSpBgAHvLK3gJn8/Ojq6JaShSwR6icBcYtVucY8BuUSAS1SwufBTfWHMafBzsgtMl3ObeQFNuYPPhZTblcONXTwXoEqhdzUIXjXoTT0boEq+dzW4GX84JzJy6Zo1Iy+++OKsV9YvpCBMWzMCXjja/U4Rt/ADbgHwtrkDXPSL4AeAeQq+Pj042HOgZhQWUJESgtysAg9B5ZEQph4u27CUBZSRSSlAAQpUikA9BrymvX3XNdbz32xUQirYptBtArMNqtvcc0EQzC52K/Y2Fq9uX2w+pe7ngsbC+MNz4xDzr9kpNyYRwET+phMQGYfFBEQnAAleD8biuteh46JmTMSO+2LGxNpx4+lp3zacCVmcnm6V02t9/8xKLxJQKgTTVZdAR0f8Sl9wi0gQ4Lpe3IY5ahDMGKCCpwH/6Ux//8+qq6blK+0cQe7MAxyE4n5r5IH9/T0P52fb4EYBClCAAjUb8G7fvr3Fa14X9yBxhU2IuHuJl7QsZv68KNc40zOAngHELZ1avJ12zwV6RgVnRHFG3cT+xpwR64JPHSsEnmPWeC7wdBcSjeX80JgJ+WPedONYU1NurKenZ4w/5/M/cbUIRKOJXT7U9dzeADfnK3DJRcr+BIDvqerT2XSvmwO25AUUqsWj1HLOH+TqMajcrwYPbF7X+gB/OSlVlukoQIF6EqiJgHf7rl1toUn/WhF5CyDXAnol4JbqXPJmITgGDebOPK7AMQMcU+CEKk4LcFoNRo3itAKjVnHasxj1m3B6jchoT0+Pu5iKGwXqTmDHjqs2SbP/Ng94m6q+TYHrJBifPsem6AfkUTX6/WmjT73a2+uGzNTlFg4nLkWj7jGKGyz052eMyZ3pccr15KrBg60heYB/a+ryVGGlKUCBBQhUXcDrpmOa9OVa9e1brOBaAa696AUts2KoD5UBt5LRbBPAW3jHms30sf7+/hMLsGRSCtStQKSra7da7+0mH+BeB0FsHgy3PPGjCvuoZ70fDAzsS9ctnpsnN9b9EV/xHmNwBVRunOMiVU4hVs8nCetOAQosSaDiA9729sQOG7I3ipXrIOp6cP/JAmp8HMA+CHrEYp+F9oSQ25dOp93r3ChAgUUIdHZ2vimn5m0KccHt20Xwjvw0dxfd3Kwbz6na5xR4ZH+690eLOHRN7dLembgZqu9X4H0X+dLOILemWp2VoQAFVkug4gJeNx2Rl9Mb1fVyCG4A5u0pKtjpz6DyvAAvMbBdrdOJx601ga27d69ZM+EnYO21aoLe2+vn+In9jVUXvAyL5wTynC/mR/tTryRrzWah9XF/28y0vQ4i74LIB6DYOUceJyC4T1S/P9XoPcJ5chcqzfQUoAAFLhRY9YDX9RZN29CNEHu9Qtw4v2tKaKi0Ai8YxQvimeebPH2BMweUoMYkFJhDwC0965vGhIEkENxsAuLusaMENLcEreuxfU6A50Yb5LnjHL8ON4451JR7B4B3KuQmQN3wq7m2HAR/Bav3ZdK93+PFqCWcdUxCAQpQYAECKx7wRqPRphxCe0TkegDudt1FpiNyVXEzGjwO6Auq+nyuyXuBPR4LaGEmpcAMAXeBZ+O0JqwGAW03FF0C6QbULdFb6vaSuuBW8CMx+txgb29/qTvWcrorurq2heBdD+v+ruk7AVx9kfqOA/IkBE8CeCrT3/NULduwbhSgAAVWW2BFAt5o9MoO3/jBMAVRvQmCS+euuPqAPKOq31fgMY71W+1ThMevRoFg5pJpe6v4sh1GtyAf1HaXPkTobK0PAvpTETzn+/qc+JM/ymazbv7mut92xq6MG8ldDyvXI/gCP8+XBrciHPBjEftMSOyTvCi27k8hAlCAAisosGwBbzjavccYc6Oq7oFbHvSim74gQZArj09PjDx26NCh8RU04KEoUJUCQVA7pTGjiFrYmIhbflfcErxuhoS2BVbKBbF7odgLkb2qdq+dath74MDLJxeYT80m3xHdlfCMf5NYuVkFrgd3ngVq9GcCeURVntEcns9me16rWRxWjAIUoECFC5Qt4O3s7LzcjcVV0RsFuAlA+CJ1f0Wgz6jIs5jGDzOZ5P4Kd2LxKLBsAuHw1RvRPLERvtkItRuNoE1F2lTRZoDN7l4FbZIPYtsU2Fx43LLIQmUgheBWzV4J+Xs5LOFCSffLlBX7LoW9GcEYXFw2j3dKge+r4FGZano8m33p1CLbh7tRgAIUoECZBZYU8EaiXXcA5lZIcLWx+0AIzVG+cVE8YoEfQuSJbKrnpTLXg9lRYCUETCKRaJ2cnGy1jY0tNue1hmyuVT3TotYG98bdq7TCaEtwr9pqRDYAshGqG1WwEbjgtlxl90Xw46DXVrFXPLM3hNzevr6+0eU6YDXnG+6Mf9JYjajgzQDeDsj2eeqTVeizgHnKV/PYwfQrA9Vc/xllD87zqampFt9vaNUGv0Wt1xoy6AyWu7N6QU+153lGrW1RtS2A1wzjHpsWqLYA7jbv5pZknzKQSQWmYIJl0qfcYw+YVM0/VlW3lPqiN1HZIUaHxOLIojNZhh3V5DuIxCK7mOx9z57J9vf3LmZf7kOBehFYWsAbi8+9TrvgNSh+osD3xNjHp0Re3WCMW31sag5cs23bNc2y/kyLWNvcbBubAdusaptdIGGtnSuYPpudeGa75jQHufAP8sUaVEQsRCYgvvtZdxJTZgLwJkTGJqy1k3U5ZnHPntDWoaGmBt9v8qxtEtXg5ibEFw01Qf2otaILsQ4+FNU2Q7XZfSgGH44GzcEcrqrNqtqwEv/xjEizBh/CphmihQ/lYB7ZFhW0GEWzAu5DugWCZmjhMdC4TOVzH/ZuMwvI3wUEB0X1gIoczD92C6nIq+L5h6eBwwf7+g4vIL+6S9rRsStqjd4mqrepBBfPrp8H4SigT4rKj6zIs9lUz48rDc1dFOzqMSUt6wz89QINbu41C7Ou+BgqG2F0I3Tmly/ZCKj7Qra20urF8swvMHmmdc3hwy+65ea5UYACswgsOuCNxOJubJ/748iNAvUk4ILT4hc99/+neCuXwVCw+h/wGqDuS+NrMOaIWHeP13JWjtgmvMaZShbO7a4rgJjdIrobFm+FwC1BPt/2KCCPWbXPrOQFtIlEYu246mY7jc2eG+Kikh/WImiD2k0KaTOFoS5umIsAmzQ/5KWU3tT56uzeP/88X8iXsVLyZ5qyCuj9mVTvB8uaJTOjQI0JLCXgPQOgtcY8WJ1zAi6oy99UATl7qrgH/PB745nifmYdh2AMinEFxkSD5+7iy2EITsHKKYieAmQY6u7tKRFzCsaeklzolOdNn9q2bdupJ554IseTsDwCkc74LwN4K9RdzAd38ex8m/vF/mFRfRrGPD3Y3/P0fDvM9X6xpxVoXj8Nf0O+p9UEPa35XlZdD5FNQRCr2OzGZRcuNHT37rbYXxOK/29d0cr9/9TlPQLA/e13PYnF20rN2uG5X1vcrzDiPnsELdDgM8gF+Svy69Biz4fl3i+TSi76s3y5y8b8KVApAkv6T+J6TMRIFFYjMLIVGkw3djkQ9JzMOwShUhBYjnIKqJtfdAzQcYiMQYvPgzF5q7+pTojIuIWMAzqRD0yDMk5AZFyse4wJ615zw1ysjAvsOIwZF/HHc35oTLzcuEx646HQ1NiGDRvGX3zxxSWNK1x9lNorgZvBomHKHr343yE5BrEpVbwCaI8Rb5+1Nhim6jaBXAFPVH2MGsmPzVaRNUatu1/rhgOo6CY3PtsINqpiU+FXL3c/31LL86EvZpjLbHm6oNSN2R5RYFSAEVGcgohbsviUVXVLPue/jLnXrJ4SY8aMr2PTJjQmody4Nzk5tmnTpjGe5/M1Gd+nAAUqWWBJAW8lV6ycZXO9NZ7nNapq07hIowGaTL4HpkmARgGafF9qpofBGJODu3hEci5InVRxj2XSN2Zy2vMmj7a1TYI9keU8xZhXmQUisfhwCWNyy3zUN2RXrp7WHKBDgBkS0SFVDEFxUgRDVnVIDIZEvSFf9CSsPRVSGVWVkVzOGz1woHsEuPNsAL+clWXeFKAABSpdgAFvpbcQy0cBCixYoAKvMRgBdEQhw8VeVjV4HRoEsa+LkdfVBbCK143i9VxIhxp8//V0Ou2GEHCjAAUoQIElCjDgXSIgd6cABSpTIBqNrs8h9HY3xVhwsZe7uEulzRpsEMVaBdZIfkaCNXPMTDDXmNXTbjgAVE668dkictINBbAGJ+GbUyr+sMIbboQ3AoyNMGitzPODpaIABepLgAFvfbU3a0sBClCAAhSgAAXqToABb901OStMAQpQgAIUoAAF6kuAAW99tTdrSwEKUIACFKAABepOgAFv3TU5K0wBClCAAhSgAAXqS4ABb321N2tLAQpQgAIUoAAF6k6AAW/dNTkrTAEKUIACFKAABepLgAFvfbU3a0sBClCAAhSgAAXqToABb901OStMAQpQUPCPLwAABb1JREFUgAIUoAAF6kuAAW99tTdrSwEKUIACFKAABepOgAFv3TU5K0wBClCAAhSgAAXqS4ABb321N2tLAQpQgAIUoAAF6k6AAW/dNTkrTAEKUIACFKAABepLgAFvfbU3a0sBClCAAhSgAAXqToABb901OStMAQpQgAIUoAAF6kuAAW99tTdrSwEKUIACFKAABepOgAFv3TU5K0wBClCAAhSgAAXqS4ABb321N2tLAQpQgAIUoAAF6k6AAW/dNTkrTAEKUIACFKAABepLgAFvfbU3a0sBClCAAhSgAAXqToABb901OStMAQpQgAIUoAAF6kuAAW99tTdrSwEKUIACFKAABepOgAFv3TU5K0wBClCAAhSgAAXqS4ABb321N2tLAQpQgAIUoAAF6k6AAW/dNTkrTAEKUIACFKAABepLgAFvfbU3a0sBClCAAhSgAAXqToABb901OStMAQpQgAIUoAAF6kuAAW99tTdrSwEKUIACFKAABepOgAFv3TU5K0wBClCAAhSgAAXqS4ABb321N2tLAQpQgAIUoAAF6k6AAW/dNTkrTAEKUIACFKAABepLgAFvie39sY997HlVvbbE5ExGAQpQgAIUoAAFllVARF64884737KsB6mRzBnwltiQDHhLhGIyClCAAhSgAAVWRIABb+nMDHhLt2JKClCAAhSgAAUoQIEqFGDAW4WNxiJTgAIUoAAFKEABCpQuwIC3dCumpAAFKEABClCAAhSoQgEGvFXYaCwyBShAAQpQgAIUoEDpAgx4S7diSgpQgAIUoAAFKECBKhRgwFuFjcYiU4ACFKAABShAAQqULsCAt3QrpqQABShAAQpQgAIUqEIBBrxV2GgsMgUoQAEKUIACFKBA6QIMeEu3YkoKUIACFKAABShAgSoUYMBbhY3GIlOAAhSgAAUoQAEKlC7AgLd0K6akAAUoQAEKUIACFKhCAQa8VdhoLDIFKEABClCAAhSgQOkCDHhLt2JKClCAAhSgAAUoQIEqFGDAW4WNxiJTgAIUoAAFKEABCpQuwIC3dCumpAAFKEABClCAAhSoQgEGvFXYaCwyBShAAQpQgAIUoEDpAgx4S7diSgpQgAIUoAAFKECBKhRgwFuFjcYiU4ACFKAABShAAQqULsCAt3QrpqQABShAAQpQgAIUqEIBBrxV2GgsMgUoQAEKUIACFKBA6QIMeEu3YkoKUIACFKAABShAgSoUYMBbhY3GIlOAAhSgAAUoQAEKlC7AgLd0K6akAAUoQAEKUIACFKhCAQa8VdhoLDIFKEABClCAAhSgQOkCDHhLt2JKClCAAhSgAAUoQIEqFGDAW4WNxiJTgAIUoAAFKEABCpQuwIC3dCumpAAFKEABClCAAhSoQgEGvFXYaCwyBShAAQpQgAIUoEDpAgx4S7diSgpQgAIUoAAFKECBKhRgwFuFjcYiU4ACFKAABShAAQqULsCAt3QrpqQABShAAQpQgAIUqEIBBrxV2GgsMgUoQAEKUIACFKBA6QIMeEu3YkoKUIACFKAABShAgSoUYMBbhY3GIlOAAhSgAAUoQAEKlC7AgLd0K6akAAUoQAEKUIACFKhCAQa8VdhoLDIFKEABClCAAhSgQOkCDHhLt2JKClCAAhSgAAUoQIEqFGDAW4WNxiJTgAIUoAAFKEABCpQuwIC3dCumpAAFKEABClCAAhSoQgEGvFXYaCwyBShAAQpQgAIUoEDpAgx4S7diSgpQgAIUoAAFKECBKhRgwFuFjcYiU4ACFKAABShAAQqULsCAt3QrpqQABShAAQpQgAIUqEIBBrxV2GgsMgUoQAEKUIACFKBA6QIMeEu3YkoKUIACFKAABShAgSoUYMBbhY3GIlOAAhSgAAUoQAEKlC7AgLd0K6akAAUoQAEKUIACFKhCAQa8VdhoLDIFKEABClCAAhSgQOkCDHhLt2JKClCAAhSgAAUoQIEqFPj/AeVmMFStUIyTAAAAAElFTkSuQmCC', '0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1'),
(1000049, '2019-07-30 10:15:15', 'Habib Syuhada', '10039315', 'habib', '827ccb0eea8a706c4c34a16891f84e7b', 32, 6, 'habib@gmail.com', 'default.png', '6', 1, '2020-01-24 09:57:53', 76, 'iVBORw0KGgoAAAANSUhEUgAAArwAAAD6CAYAAABDJJQbAAAgAElEQVR4Xu3dCZRkV13H8d//Vc+EgIGQkIDMJNNdXd1T3ZO4ZETDJoEYEcOmOBhAoxIJGhUBN/AoohD1yGYORiVAIsYFjGIi0RDZJCAhIBGj09XTVd1Vk0zMpmGJ2aa73t/zampmqjuzvO7U61fv1rfOmUMyfd+99/+5l8xvqt67ZeKFAAIIIIAAAggggEDAAhZwbZSGAAIIIIAAAggggIAIvGwCBBBAAAEEEEAAgaAFCLxBLy/FIYAAAggggAACCBB42QMIIIAAAggggAACQQsQeINeXopDAAEEEEAAAQQQIPCyBxBAAAEEEEAAAQSCFiDwBr28FIcAAggggAACCCBA4GUPIIAAAggggAACCAQtQOANenkpDgEEEEAAAQQQQIDAyx5AAAEEEEAAAQQQCFqAwBv08lIcAggggAACCCCAAIGXPYAAAggggAACCCAQtACBN+jlpTgEEEAAAQQQQAABAi97AAEEEEAAAQQQQCBoAQJv0MtLcQgggAACCCCAAAIEXvYAAggggAACCCCAQNACBN6gl5fiEEAAAQQQQAABBAi87AEEEEAAAQQQQACBoAUIvEEvL8UhgAACCCCAAAIIEHjZAwgggAACCCCAAAJBCxB4g15eikMAAQQQQAABBBAg8LIHEEAAAQQQQAABBIIWIPAGvbwUhwACCCCAAAIIIEDgZQ8ggAACCCCAAAIIBC1A4A16eSkOAQQQQAABBBBAgMDLHkAAAQQQQAABBBAIWoDAG/TyUhwCCCCAAAIIIIAAgZc9gAACCCCAAAIIIBC0AIE36OWlOAQQQAABBBBAAAECL3sAAQQQQAABBBBAIGgBAm/Qy0txCCCAAAIIIIAAAgRe9gACCCCAAAIIIIBA0AIE3qCXl+IQQAABBBBAAAEECLzsAQQQQAABBBBAAIGgBQi8QS8vxSGAAAIIIIAAAggQeNkDCCCAAAIIIIAAAkELEHiDXl6KQwABBBBAAAEEECDwsgcQQAABBBBAAAEEghYg8Aa9vBSHAAIIIIAAAgggQOBlDyCAAAIIIIAAAggELUDgDXp5KQ4BBBBAAAEEEECAwMseQAABBBBAAAEEEAhagMAb9PJSHAIIIIAAAggggACBlz2AAAIIIIAAAgggELQAgTfo5aU4BBBAAAEEEEAAAQIvewABBBBAAAEEEEAgaAECb9DLS3EIIIAAAggggAACBF72AAIIIIAAAggggEDQAgTeoJeX4hBAAAEEEEAAAQQIvOwBBBBAAAEEEEAAgaAFCLxBLy/FIYAAAggggAACCBB42QMIIIAAAggggAACQQsQeINeXopDAAEEEEAAAQQQIPCyBxBAAAEEEEAAAQSCFiDwBr28FIcAAggggAACCCBA4GUPIIAAAggggAACCAQtQOANenkpDgEEEEAAAQQQQIDAyx5AAAEEEEAAAQQQCFqAwBv08lIcAggggAACCCCAAIGXPYAAAggggAACCCAQtACBN+jlpTgEEEAAAQQQQAABAi97AAEEEEAAAQQQQCBoAQJv0MtLcQgggAACCCCAAAIEXvYAAggggAACCCCAQNACBN6gl5fiEEAAAQQQQAABBAi87AEEEEAAAQQQQACBoAUIvEEvL8UhgAACCCCAAAIIEHjZAwgggAACCCCAAAJBCxB4g15eikMAAQQQQAABBBAg8LIHEEAAAQQQQAABBIIWIPAGvbwUhwACCCCAAAIIIEDgZQ8ggAACCCCAAAIIBC1A4A16eSkOAQQQQAABBBBAgMDLHkAAAQQQQAABBBAIWoDAG/TyUhwCCCCAAAIIIIAAgZc9gAACCCCAAAIIIBC0AIE36OWlOAQQQAABBBBAAAECL3sAAQQQQAABBBBAIGgBAm/Qy0txCCCAAAIIIIAAAgRe9gACCCCAAAIIIIBA0AIE3qCXl+IQQACBMAXKk1PfF0tPUux3pq3Q5E8ys4cs1h3Lr4lO1kjbtWT39P6+Rxq1yO9f+ftJm+Rnyf9arNahxj/yz6OTPYof23utu7Z45JHL/udw9ZjsFC/F9yzdf/9n9+zZ82DaummHAAISgZddgAACCCAw8AKjE9PfIfezIuk5bnqmpJMGftLZTvAByf/PZfeZdJ+SX66WTHMe290l+d3z87Wrs50CvSNQHAECb3HWipkigAACQyEwVtm6w6x0YuzxU8yiM13+LJMeNxTFpyvSkzeXj9bUXNfF5le36rOXHa0tP0cgdIGj/h8mdADqQwABBBBYH4FTyqd9V2mkXUluQ4hM3+qmp5rbycmtCcntBi7bZNL2lLOJXbrDpP+VdK9Lx5nrOJm+RdJx3V8puwquWRKIk19RUplLX5H8MoJvcOtMQasQIPCuAoumCCCAAALS+Pi2Z8ZRXIpiv7/Xo3Mf6oid4K4TTL5Jbk+W6clS537Xzj2va3m5dLvJv+DSJ6XoS1rc2Gq1vvr1tfQVwjWTk5NPWtSGTaZ4k8s3eaxNkdlWl7ZK/u2SdYJu95XcE/yk7j/f5PJLWvXZvw7BgRoQWI0AgXc1WrRFAAEEhlhgy3j1GRbZBSa9OlsGb0u206RPy+zahbmZT2U7Xli9b5mc+qEo1ktlOv+Qwdf843Gs2yL5J5qNXVeFVT3VIHBoAQIvOwOBgAU2Vasnbli0Z6ukxxzpafZDP71+mCfXUzxNLtNoZFFjYW7m8wHzDk1poxPT32PyiyWd/SiLXpR0s6TOCQMu3RtJDTfVPfaG28hdx9jee+bm5g57UsGjHH+oLh8fr26Po+hCyS/sKfwbkp6w/99d2inpE+b2hcVj7FN7du68d6iQKHZoBAi8Q7PUFBqqwFhl649I0RNMflJsdqpJp8p1qkyn9v7BllP9d5nrZjfdLNMuj/3uVmP2+pzmwrCrFCiXp89ul+KXRLKflnTsUS93b8vsPyXdLtOdiv0ut+g2xdYqyXbOz//XbUftgwZ9FzhM8E3u8Y0llVYMeKO7/7NH0U3tDXYTAbjvy0GHOQkQeHOCZ1gE1iowPr6t0o7azzKPtsvi75HsaWvtK9vrOh9Lr/zDNBnySy67YqMtfWxubu72bOdA76sRGJuYOsfct7usLNP3qnNP6MGXSzea23uajRk+Bl8N7IC0TYJvu2SvMtfPSdq4YlrJu+8bDjHVhsk+H5tuLbX93znqbEAWk2msWoDAu2oyLkAgO4HRytSZpmhz5O3m/lFi87Ip2hTLj1dk55jrGdnNIPOek3eUDj5QY/obuf5rry9ecXujsSfz0RngkALj46edEpfav6Z9QehQr1vl+p1mo/ZBCMMQGKtUv1+mc1z23EOejLHv3fpH/IXVpT8sxaV38259GPtgmKog8A7TalPrQAmMjk+91My+ReZVc53hpjOkzhPtq30lIXKXS01zNRVZ0zxutj1q2tLGZh5Ps49Wq6NRW2e47Ixubcm9n73vKO1d8e9LMl3WjqNLb23snFktAO3XJrClWn16tGQ/s+LhpgOdeRJ6pLePaOndjUbjm2sbhauKINB59zfSdlO03eXbDxGCl5/9a7pSrg8067UbilAfc0SAwMseQCBjgbFK9UUmfWccRceafLNcmyU9XdIxaxz6G5J90s3/Nlb7S7fOzS2ssZ91vWyssu3FbvFLTHpxzzFJK+fwTXd/TxTbXy4s1OrrOsHABxutVp+vtlVNOmnfX0J8u2Qn95Zt0lIsXSXZDbHbDfzlI/BNcYTyNm/bdsLGxfhMj/VSl7/aDr7bu/Kht49G0uUL9do/Dq8WlRdBgMBbhFVijoUUKE9MnevJsUCulz/KApYk/4jcPmNRdMv9jynN3HXLLcvOP32U/a/75WOV6R2yeIdkO3oGv6vnHe6H5LrGYn/LwsLs3LpPMLABxypTF8j0gcOVZdK8ZBcv1GeuCKx0yumTQHli+qdc/kZJp3W7XP6Or2vOTNeNWPsd3JvfJ3S66asAgbevnHQ27ALJPbgynWeyF0lePorHQ+b6THKCgclvjku6uTU72xomw0qlclKsDTti00UmbevWfo+kk/b9c+cj9Q/Lo2t4UGptO6M8MfVXLr3iMFcvmexCgu7abIfxqm7w/ZMVn1Ddt+yb7VxXWmQf4vzkYdwhg1szgXdw14aZDbhAeXLq++LYn2FmGw/3EfHBd0L830z2r7Hps6W2GtKGu+fnb7l7wEtct+lVKpXHL2nkDRbZK+WaPNTAnfNC3f9WFl3Xqs/ctG6TK+hA4+PbnheX4svl2tItITbX9cP8F6yCLuVATrvzCZb8J1Z8SrN8ru7vijfYu3bXancMZBFMaqgECLxDtdwU2w+BLZPTLzD3nzfpB4/cn19lsg9xb1t69Uqlckw7GnmN3N697Igk0/1yPa6np5vM9C9xrKtbjdoX048QbstlR4pJz5H1/sXBPtaszyT3TvNCoK8ClUplczvaeL7cf1PSY7qd977j+9/u/n7fYO8j+PaVns5WKUDgXSUYzYdXYHRy6kcV+0+a2Q8cXiE5yid6Syne++cNjtla82YZHf2O47Xx4bNMOkeu5D7f7i0O2iv513sfthr2Y5KOfqSYva5Zn3nvmheDCxFIIdB5yO1hf4Gb/9SBb+Rz3SnTU7qX3yX5DbHsQ7t5wC2FKE36LUDg7bco/QUlcOrppz8xeqj94+b+0zKdvqK4JXN9go+Is13y/ff5Snqhm17QM9qyh2Zi05XR4oaLm81bdmU7o8HofcvExFTkI2863JFikhaj2J8+Pz/7lcGYMbMYFoGxyelfksevlyw5kSZ5Jd+wd0pP/V90040W2418nfGw7Ir86yTw5r8GzGAABUZHT6/ahsULJJ2/8ugmma5x6cOtudqHB3DqQU8p+ZY5j+LXuPSrPYUuOybJ5O9rx/rz3fOzXwgVI/m0wVzL9h9HioW62sWsa1O1euIxS3a+W+c+32/vVnGorzO+V/Lr3KJ5l9+0e672T8WsmFkPugCBd9BXiPmtq8CWrVvHorb9vEu/2HPuZDKH+yS/wiz+i4W5uS+v66QY7JACRzsmyaW/l+kjI/Hi1Y1G4+FQGEcnqpeY7HX76+FIsVBWNtw6ypWpN7jp9w5x9nhyvGLvvfku1y80G7VLw9WgsrwECLx5yTPuQAlsmThtKvKl5KzSn5Ns/4MXkmm3ZJdF7ehKvkpzoJbswGS6wTf5A/LYfb/pDyYLd/DfbUHuH49H/C93zxb7Xd+xialrJZ3bLf4hk13EkWKDuS+Z1SMFOg/8ypOvRz+z++U7vY16blHyf4ji0i/Nz+9s4IhAvwQIvP2SpJ9CCmypTH9nJL/AInutu4/0FLEo8zceOxJdNjMzk3wNLq8BFjj4wIx+VPIXHZyq3x3CA26d0ytsw79InaCQvD7frNeePcBLwtQQOKJApfJtm9taeroiP0uuCyV1//trD0j+WEkP++KDx7darYegRKAfAgTefijSR+EExsenTotL+lm5Llox+eS8yEvjEV3OETqFW9bOhMsT1Re6dJ5kyQNuJ3SrWP6tUKYrPfYPthqznx30KsvlqQkvKbmvsdKZq+kDzbnaawZ93swPgdUIjE5O/565v6l7TXJf76eb9dneb2JcTXe0ReARAgReNsVQCVQqp40vefsXLOoE3Q09xbdk9ke+d+MHW62vfn2oUAItNnnXd8Ni+5UrzvRd9oCbpLc067W3DSpBuTL9Ko/8vXI9sTvHtzfrteS8U14IBCcwWqmeZdY5g1vNeu2M4AqkoFwFCLy58jP4egmUy9PPUkkXuPvLZUo+Ltv/2iXTe33vgx/ko7P1Wo31H+fID7j5f8j1m83G7MfWf2aHH7Fcqb7Rzd61v4WbXsHJIIO0QswFAQSKJEDgLdJqMdc1CYxXqhfHZm/ufBh88FV3+Ttb9dnL1tQpFxVSoBt8//jAN0KZvnbw3VO/No78Pbt37fp03sWVJ6b/xOU/cyDslnysNTvbyntejI8AAggUVYDAW9SVY95HFRirVF8k0yWSjfU0vtflbyboHpUv6Ablyeqb3fU2yUqSYrlLZlFStEuvb9Vrl+QFMFaZulqml3TH31vyDeONxi178poP4yKAAAIhCBB4Q1hFalgmUJ6cPjuO9etm/rzlNPYrzfrMO+FCIBHonLkcWxJ6X9UVOfBgm5l/RrLfXZirfXI9tcYmpm6X9NTumJ9bfPD45+/Zc+OD6zkHxkIAAQRCFCDwhriqQ1xT9yPry3sIYjP/jYW52eTQc14IPEKg+0nARyTrnuN74Fik5C6YvzP5FQv12j9mSbfvZAn7O0kbk3FM+oOFeu3XshyTvhFAAIFhEiDwDtNqB15reWLqr1x6xYEyTdfEFr9h965dzcBLp7w+CKw4Fmn5MWauOTNdp7YuXVio1fsw3IEu9n2N9dLM/nvMTf6yhfrsR/s5Bn0hgAACwy5A4B32HRBA/eXJyae5SlfJtaVbzqLcXzZoT90HQB18Cd1jkT6+4itQ75N03P7iPdZ5rfnaR/qFMTYxtdg9dD+29sMnLCwsJEen8UIAAQQQ6KMAgbePmHS1/gLlieoPdz8K7g5uH2vWZ168/jNhxJAEyhNT57r8JyTrPfj+wLu+7v7brcbsWx9tzeWJ6T91+WuTfnhn99Fqcj0CCCBweAECL7ujsAKjE1PvMen1Bwuw1zXrM+8tbEFMfOAEKpXK5na08Xzt+wao42S6szNJ11NcunzEFy9qNBoPr2Xi+x6ai+aTrGvSRxfqtZetpR+uQQABBBA4ugCB9+hGtBhAgbHJ6ufl9szu1B6IYv/e+fnZrwzgVJlSIAKjlepbzey3uuUk38Z3vKRPWlsXreW+3rHJqVvkOl3SAyVfPGGtwTkQXspAAAEEMhUg8GbKS+f9FiiXq5Neiq6WfGrfG22aadVr2/o9Dv0hcCiBscr0Dpn/Tfdnyb23yddT36Y4eltzfuf706qNTk6dZ66/Ttqb+5sXGrO/n/Za2iGAAAIIrF6AwLt6M67ISaBcqb7JzXqPF7upWa+dmdN0GHZIBUYr1eebWfJgW/I3rge6X1Udx7E/e/f87BfSsPQ8qPZ/zXrtwANxh7p2y9atzzW3UxVr98qfJ79vkd9rse44+rjRyR7Fj7VYPd/YFp2skbZrye7pvb5Uiu+cm5tLzgTmhQACCAQhQOANYhnDLuKR35jmbVn0lubczO+GXTnVDapApVJ5fNs23Cbp8fs+aJDJ9LW47S88Wugdm6y+Rm6dr7Q207kLc7V/2l9nZ68reoLMq+Y6w01nSHpyDg73NOu1k3MYlyERQACBTAQIvJmw0mk/BMYq1e+XRb8q+dkH+nM94GbPbNVnvtqPMegDgbUKjI5OP8U2ekOux0m6V9IJkn/TY7vwSMeWjU5M/Y9JJ7pUa9Vr0+Pj33ay2+IrY/mPmdn2tc6nb9eZ7pf098252o/3rU86QgABBHIWIPDmvAAMf2iB8fHxU+Jo4609P+Ub09gsAyeQhNU4WrpW8qfJ9TWZnphM0l1vaDVqf7hywmMT078s+Ts6v292jVyRy59n6oTmAXnZl5v1me8ekMkwDQQQQKAvAgTevjDSSb8FxirVJZmV9gUDvjGt37701z+BZaF3/+0NnTzrn3G3tzbrtRtGK9XnSHammZJ70I/0390lc33CTTeb/Oa4pJtbs7M999z2b970hAACCAyTAIF3mFa7ILWOTUzdIOnZnXfKZK9t1Wc69zvyQmBQBTqh1xab3QfYkmkmZ/Me053vXkkbDzd3l3aa6YuK7fpmY+aqQa2ReSGAAAJFFiDwFnn1Apz76NZtP2BxfN2+N3Z1yUK91vPFEgEWTEnBCJTL06fGI/ozc39uiqLa7v52WXRdqz5zU4r2NEEAAQQQeBQCBN5Hgcel/RcYm5hKnnhPXg8167Vj+z8CPSLQP4Hx8anzYtOJbkrOgj4/xb24D7c9OuPWxs6Z/s2CnhBAAAEEjiZA4D2aED9fV4H9gbdZr7E311WewQ4nMFaZfrksfrJb9FSTb5Zrs6TR7q/DXZZ8KcU/u+wbkh9v0jlJw2a9dthbG1gBBBBAAIHsBAgV2dnSMwIIFFDgrLPOGtl9x90/rLafK7PnKQm5KV4ufcXcr7co+vTC3MynUlxCEwQQQACBdRIg8K4TNMMggMBgC4yPT78gNn+ZIp0t77yDm+a1aNKlMruWkJuGizYIIIBAPgIE3nzcGRUBBAZEYHz8tFPaUfuNJh3uAclY0g0ma8Ue7zbZbS7fE0dx/da5uYUBKYNpIIAAAggcQYDAy/ZAAIGhFCiXp35QJX+Vy175SAC7XqbkeLzPNedmPjeUQBSNAAIIBCRA4A1oMSkFAQTSCWzevPnYDcce90Bva5OW3Oy32rFdzSkK6RxphQACCBRFgMBblJVinggg0DeBnuPvkvOe5yW7eKE+c0XfBqAjBBBAAIGBEiDwDtRyMBkEEFgPgf2B12SvJuiuhzhjIIAAAvkKEHjz9Wd0BBBAAAEEEEAAgYwFCLwZA9M9AggggAACCCCAQL4CBN58/RkdAQQQQAABBBBAIGMBAm/GwHSPAAIIIIAAAgggkK8AgTdff0ZHAAEEEEAAAQQQyFiAwJsxMN0jgAACCCCAAAII5CtA4M3Xn9ERQAABBBBAAAEEMhYg8GYMTPcIIIAAAggggAAC+QoQePP1Z3QEEEAAAQQQQACBjAUIvBkD0z0CCCCAAAIIIIBAvgIE3nz9GR0BBBBAAAEEEEAgYwECb8bAdI8AAggggAACCCCQrwCBN19/RkcAAQQQQAABBBDIWIDAmzEw3SOAAAIIIIAAAgjkK0Dgzdef0RFAAAEEEEAAAQQyFiDwZgxM9wgggAACCCCAAAL5ChB48/VndAQQQAABBBBAAIGMBQi8GQPTPQIIIIAAAggggEC+AgTefP0ZHQEEEEAAAQQQQCBjAQJvxsB0jwACCCCAAAIIIJCvAIE3X39GRwABBBBAAAEEEMhYgMCbMTDdI4AAAggggAACCOQrQODN15/REUAAAQQQQAABBDIWIPBmDEz3CCCAAAIIIIAAAvkKEHjz9Wd0BBBAAAEEEEAAgYwFCLwZA9M9AggggAACCCCAQL4CBN58/RkdAQQQQAABBBBAIGMBAm/GwHSPAAIIIIAAAgggkK8AgTdff0ZHAAEEEEAAAQQQyFiAwJsxMN0jgAACCCCAAAII5CtA4M3Xn9ERQAABBBBAAAEEMhYg8GYMTPcIIIAAAggggAAC+QoQePP1Z3QEEEAAAQQQQACBjAUIvBkD0z0CCCCAAAIIIIBAvgIE3nz9GR0BBBBAAAEEEEAgYwECb8bAdI8AAggggAACCCCQrwCBN19/RkcAAQQQQAABBBDIWIDAmzEw3SOAAAIIIIAAAgjkK0Dgzdef0RFAAAEEEEAAAQQyFiDwZgxM9wgggAACCCCAAAL5ChB48/VndAQQQAABBBBAAIGMBQi8GQPTPQIIIIAAAggggEC+AgTefP0ZHQEEEEAAAQQQQCBjAQJvxsB0jwACCCCAAAIIIJCvAIE3X39GRwABBBBAAAEEEMhYgMCbMTDdI4AAAggggAACCOQrQODN15/REUAAAQQQQAABBDIWIPBmDEz3CCCAAAIIIIAAAvkKEHjz9Wd0BBBAAAEEEEAAgYwFCLwZA9M9AggggAACCCCAQL4CBN58/RkdAQQQQAABBBBAIGMBAm/GwHSPAAIIIIAAAgggkK8AgTdff0ZHAAEEEEAAAQQQyFiAwJsxMN0jgAACCCCAAAII5CtA4M3Xn9ERQAABBBBAAAEEMhYg8GYMTPcIIIAAAggggAAC+QoQePP1Z3QEEEAAAQQQQACBjAUIvBkD0z0CCCCAAAIIIIBAvgIE3nz9GR0BBBBAAAEEEEAgYwECb8bAdI8AAggggAACCCCQrwCBN19/RkcAAQQQQAABBBDIWIDAmzEw3SOAAAIIIIAAAgjkK0Dgzdef0RFAAAEEEEAAAQQyFiDwZgxM9wgggAACCCCAAAL5ChB48/VndAQQQAABBBBAAIGMBQi8GQPTPQIIIIAAAggggEC+AgTefP0ZHQEEEEAAAQQQQCBjAQJvSuAdO3Z8yd2flrI5zRBAAAEEEEAAgUwFzOzLV1111XdnOkggnRN4Uy4kgTclFM0QQAABBBBAYF0ECLzpmQm86a1oiQACCCCAAAIIIFBAAQJvAReNKSOAAAIIIIAAAgikFyDwpreiJQIIIIAAAggggEABBQi8BVw0powAAggggAACCCCQXoDAm96KlggggAACCCCAAAIFFCDwFnDRmDICCCCAAAIIIIBAegECb3orWiKAAAIIIIAAAggUUIDAW8BFY8oIIIAAAggggAAC6QUIvOmtaIkAAggggAACCCBQQAECbwEXjSkjgAACCCCAAAIIpBcg8Ka3oiUCCCCAAAIIIIBAAQUIvAVcNKaMAAIIIIAAAgggkF6AwJveipYIIIAAAggggAACBRQg8BZw0ZgyAggggAACCCCAQHoBAm96K1oigAACCCCAAAIIFFCAwFvARWPKCCCAAAIIIIAAAukFCLzprWiJAAIIIIAAAgggUEABAm8BF40pI4AAAggggAACCKQXIPCmt6IlAggggAACCCCAQAEFCLwFXDSmjAACCCCAAAIIIJBegMCb3oqWCCCAAAIIIIAAAgUUIPAWcNGYMgIIIIAAAggggEB6AQJveitaIoAAAggggAACCBRQgMBbwEVjyggggAACCCCAAALpBQi86a1oiQACCCCAAAIIIFBAAQJvAReNKSOAAAIIIIAAAgikFyDwpreiJQIIIIAAAggggEABBQi8BVw0powAAggggAACCCCQXoDAm96KlggggAACCCCAAAIFFCDwFnDRmDICCCCAAAIIIIBAegECb3orWiKAAAIIIIAAAggUUIDAW8BFY8oIIIAAAggggAAC6QUIvOmtaIkAAggggAACCCBQQAECbwEXjSkjgAACCCCAAAIIpBcg8Ka3oiUCCCCAAAIIIIBAAQUIvJc+aYwAAAECSURBVAVcNKaMAAIIIIAAAgggkF6AwJveipYIIIAAAggggAACBRQg8BZw0ZgyAggggAACCCCAQHoBAm96K1oigAACCCCAAAIIFFCAwFvARWPKCCCAAAIIIIAAAukFCLzprWiJAAIIIIAAAgggUEABAm8BF40pI4AAAggggAACCKQXIPCmt6IlAggggAACCCCAQAEFCLwFXDSmjAACCCCAAAIIIJBegMCb3oqWCCCAAAIIIIAAAgUUIPAWcNGYMgIIIIAAAggggEB6AQJveitaIoAAAggggAACCBRQgMBbwEVjyggggAACCCCAAALpBQi86a1oiQACCCCAAAIIIFBAgf8H6JJyVdtjuwMAAAAASUVORK5CYII=', '0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1');
INSERT INTO `portal_user_db` (`id_user`, `created_date`, `full_name`, `badge_no`, `username`, `password`, `department`, `project_id`, `email`, `sign_id`, `id_role`, `status_user`, `last_update`, `update_by`, `sign_approval`, `warehouse_permission`) VALUES
(1000050, '0000-00-00 00:00:00', 'Sulis', '10039730', 'sulis', '827ccb0eea8a706c4c34a16891f84e7b', 32, 4, 'sulis@gmail.com', 'default.png', '6', 1, '2020-02-10 17:02:15', 1000049, 'iVBORw0KGgoAAAANSUhEUgAAArwAAAD6CAYAAABDJJQbAAAgAElEQVR4Xu3de5wcZZ3v8e9TkwkECOGsAvoyMF093Z2uGUBdxXVd4ICLuoqLiOJdj7KiiILKrgfXdRXXw8pxvbAirng5rIrurlFBvKBwVA6uN5T1GDNTPV09Ux0IaIBVLhKSTLqe86qeHs4YAulkqi9V9el/lOSp3/P7vZ8Cvil6uo14IYAAAggggAACCCCQYQGT4dkYDQEEEEAAAQQQQAABEXi5CRBAAAEEEEAAAQQyLUDgzfTxMhwCCCCAAAIIIIAAgZd7AAEEEEAAAQQQQCDTAgTeTB8vwyGAAAIIIIAAAggQeLkHEEAAAQQQQAABBDItQODN9PEyHAIIIIAAAggggACBl3sAAQQQQAABBBBAINMCBN5MHy/DIYAAAggggAACCBB4uQcQQAABBBBAAAEEMi1A4M308TIcAggggAACCCCAAIGXewABBBBAAAEEEEAg0wIE3kwfL8MhgAACCCCAAAIIEHi5BxBAAAEEEEAAAQQyLUDgzfTxMhwCCCCAAAIIIIAAgZd7AAEEEEAAAQQQQCDTAgTeTB8vwyGAAAIIIIAAAggQeLkHEEAAAQQQQAABBDItQODN9PEyHAIIIIAAAggggACBl3sAAQQQQAABBBBAINMCBN5MHy/DIYAAAggggAACCBB4uQcQQAABBBBAAAEEMi1A4M308TIcAggggAACCCCAAIGXewABBBBAAAEEEEAg0wIE3kwfL8MhgAACCCCAAAIIEHi5BxBAAAEEEEAAAQQyLUDgzfTxMhwCyQgUK5U/tXZk3ons/YsVI+MU5ZhNYX3qpmR2oQoCCCCAAAK9ESDw9saVqgikXqBYrR4d7dQLjNFpknn8Iww0I6sbZUw9MpraVJ++NvXDMwACCCCAQKYECLyZOk6GQWB5AmPjE892HPs0ScdJOrGLapEkZ+k6O2/dZrPW7OJaliCAAAIIINAXAQJvX5jZBIHhFhgfP+qIltM630hvWUanVpKR0ZVh3X/lMupwKQIIIIAAAokKEHgT5aQYAukSKFYm/jSyeoWRffUunbcke5WMrndaunl2tnbzrpMdWZqccIw9QbInGOmFkkYltSSNOIrePBvMfCRdGnSLAAIIIJBVAQJvVk+WuRDYg0CxWFxjR/a7e5dl26zVhU6kr8zN+UG3iIVS9URjzPeWrI+2rho9eMuGDQ/+kFu3tViHAAIIIIBA0gIE3qRFqYdASgTcsjcvaUXcrpFmJXPRXDB9xb62X6hMvM9Y+/bF68PA558v+4rJdQgggAACiQrwL6REOSmGQDoE3Er1LFnziU7YvWQu8N+aROdu2Yvfx9t+EXiTEKUGAggggEASAgTeJBSpgUDKBNyKd5esHmUlvxn4E0m1T+BNSpI6CCCAAAJJChB4k9SkFgIpEBirTJ7v2OiD7ae7RqfM1f1vJtU2gTcpSeoggAACCCQpQOBNUpNaCAy5QKHwhEPM6PY7Op+osC0M/FVJtkzgTVKTWggggAACSQkQeJOSpA4CKRAoVLzTjNVV7ae7Mmcu54fUdjfuksB7Xxj4B6eAhBYRQAABBHIgQODNwSEzIgKLAm554nLJvk5WW+f3c47YPDX1m6R0ihXvZGt1/UI9e2kY1M5LqjZ1EEAAAQQQWI4AgXc5elyLQMoE3LL3a0mHy+irYd0/Lcn2ixXvYmt1wcLTYz13LvC/kWR9aiGAAAIIILCvAgTefZXjOgRSJlAse6dY6evtto3+W1j3P5vkCG7Z+6Wko9rlW9sPmZubuyfJ+tRCAAEEEEBgXwUIvPsqx3UIpEzALXvXSXqGpJ0jdt5tNBqbkxphfHyyFDnRwjezWXtD2KidlFRt6iCAAAIIILBcAQLvcgW5HoEUCBTXrXuyjZyfdlr9Thj4JyfZtlueOFeyH2k/3bX2r+catYuTrE8tBBBAAAEEliNA4F2OHtcikBIBtzTxORn7inYglX35XFD7QpKtFyrV7xhrnt6ub8zxc/Xpf0+yPrUQQAABBBBYjgCBdzl6XItACgTGx485LHLmt8StWun+ZuAflGTbrnvU4VrRin8YLn4l/tm+SfZKLQQQQACBfAoQePN57kydIwG3MvEOWXtR++mrNX8x15j+X0mO75a9v5P0t+2akf2rcLbW/hY3XggggAACCAyLAIF3WE6CPhDokYBb9jZJOlLSxjDwj056mwc/6ky6e8TOVxqNxp1J70E9BBBAAAEEliNA4F2OHtciMOQChYr3YmP1r+02rV4VNvzPJdmyW/beJun9nZp/Gwb+/0iyPrUQQAABBBBIQoDAm4QiNRAYUgG35G2QUfxUtxkGvpt0m27Zu13SYyWFYeAXk65PPQQQQAABBJIQIPAmoUgNBIZQoFCuvtTItD+Nwcqe3QxqlyfZplv23inpvQtPj82Lw8b0F5OsTy0EEEAAAQSSEiDwJiVJHQSGTGDJ01c/DPyJJNtbu3btqhWrVt9qpEfJ6uaw4T85yfrUQgABBBBAIEkBAm+SmtRCYEgE3LL3N5IW3k/bk68Rrn5EMufG5R3HOW52ZuoHQzI6bSCAAAIIIPAQAQIvNwUCGRMoFJ5wiFZsu8sYMyJpexj4+yc5Yql0zNqWma9LWiXpxjDw/2uS9amFAAIIIIBA0gIE3qRFqYfAgAUKFe80Y3VV3IaV3tIM/H9MsqVi2fsnK50d14yMTt9U99t78UIAAQQQQGBYBQi8w3oy9IXAPgq4Fe8yWZ0jq63z+zlHbJ6a+s0+lnrIZZVK5dHzdmTxc3bnw8BfmVRt6iCAAAIIINArAQJvr2Spi8CABNxydYtkDpPsNWFQe16SbRRLE2daYz8d17TG/F2zPv3uJOtTCwEEEEAAgV4IEHh7oUpNBAYkUKx4z7FW32hvb/XasOG3w2lSL7fs3Sjp+HY9J6qGMzMzSdWmDgIIIIAAAr0SIPD2Spa6CAxAwC15n5XRK+OtW9aZvKUxNZ1UG+66desUObVOvevDwH9mUrWpgwACCCCAQC8FCLy91KU2An0WKJSqOzufzvC7MPBXJ7m9W574K8n+Q1zTSM+dC/yFJ8m8EEAAAQQQGHIBAu+QHxDtIdCtwFjFe75j9ZX2emP+JqxP/32313azzi17v5R0lKyCsOFXurmGNQgggAACCAyDAIF3GE6BHhBIQMAtV9dL5oVxKSfS0bOz/sYEyrZLuJXJp8hGP2lnaUdvmZtJ9qPOkuqTOggggAACCOxOgMDLfYFABgTWTk7+weiOaJOkg4w0PRf4k0mOVSx7l1jpzZJ2zJtWcXO9fluS9amFAAIIIIBALwUIvL3UpTYCfRIoliZebo29srPdO8PAvyjJrd2K9ytZPUYyXwuD6VOTrE0tBBBAAAEEei1A4O21MPUR6IOAW65eKZmXx1sZ03rKXL3+06S2XfreYL5ZLSlV6iCAAAII9FOAwNtPbfZCoEcCxbI3b6UVcfkw8BP9+9qtVK+VNX8m2TvCoHZ4j0agLAIIIIAAAj0TSPRfjD3rksIIIPCIAm7Zs4sLkgy8R5YmJ0ZMNNWubaNPhY2ZszgKBBBAAAEE0iZA4E3bidEvArsR6FXgdSveZbI6J94yMuY5m+rT13IACCCAAAIIpE2AwJu2E6NfBPoZeB98cmzuCINp3s7A3YcAAgggkEoBAm8qj42mEfh9gV484S2Wqy+wMl+KdzIyZ84F01fgjgACCCCAQBoFCLxpPDV6RmAXgV4E3kLZ22ik+PN8wzDwi6AjgAACCCCQVgECb1pPjr4RWCKQdOAtlKonGmO+197COOeG9amPAo4AAggggEBaBQi8aT05+kagh4G3WKp+1xpzkqQ75x84ZGzz5h89ADgCCCCAAAJpFSDwpvXk6BuBHgVetzL5FNnoJ53yF4WB/06wEUAAAQQQSLMAgTfNp0fvCHQEknxLQ6FU/Y4x5ulW+s+VplWt1+t3AY0AAggggECaBQi8aT49ekcg4cBbrHgnW6vr47LG2L+cq9c+BDICCCCAAAJpFyDwpv0E6R8BSUk94XXL3i8kHSPJDwN/AlwEEEAAAQSyIEDgzcIpMkPuBZIIvIXyxOuN7McXnu6ak+fq09/JPSwACCCAAAKZECDwZuIYGSLvAksD7/xK51Gbp6Z+szcmhx9zzIEHPDAfSjpU0nVh4D9rb65nLQIIIIAAAsMsQOAd5tOhNwS6FHDL3lZJq+LlVuapzWB68VMWuqrglie+Jdk45G6z847bbE79uqsLWYQAAggggEAKBAi8KTgkWkRgTwJLvhVNxppXzDWmP7+naxZ/v1iuPtfKfK3z13wMWbdwrEMAAQQQSI0AgTc1R0WjCDy8gFueuEayf95+wmvte5qN2oXdernl6lbJxE+H7wkD/5Bur2MdAggggAACaREg8KblpOgTgUcQKJa9S6z05s6Sm8LA/6NuwNyyd6Ok4yXt1E6VwtDf1M11rEEAAQQQQCBNAgTeNJ0WvSLwMAJuyfsLGX3qwd+2elPY8C97JDC3XL1SMi9vr4ns68PZ2icARgABBBBAIIsCBN4snioz5U6gWJk81tropqWDG2O/G0Uj7282pr69K4hbmXiHrL2o/etGV4Z1/5W5Q2NgBBBAAIHcCBB4c3PUDJplgc7HisVfAbz/wpxmq2QPiP+ftfakZqN2w+L8xWq1YltmprPup2Ew/ZQs2zAbAggggAACBF7uAQQyJFCoTLzPWPv2zki2/fzW6LdRyz5302zth/GvL/nM3lvDwD8yQ+MzCgIIIIAAArsVIPByYyCQMYFCqXqiMeYbkuInvPEXUPyBZO+Vdc6Ssf/WGfc+Jxotzc5uuCNj4zMOAggggAACDxEg8HJTIJBBgfHxYw6LnJ1fl+yxsvqtjP7LkjF3OpF96uxs7eYMjs5ICCCAAAIIEHi5BxDIi8Dvhd5dho6MPufMj14Uhhs67+XNiwpzIoAAAgjkUYAnvHk8dWbOjcBC6J2Pvya4/fe6le430oGLAMbq/LmG/+GsgHTezjEaBv71WZmJORBAAAEEli9A4F2+IRUQGFqBQsX7gbF6WrtBo02yGus0G0ly2r9sdbV1nAvD+tQvhnaQPTQ2Pn7UES2ndb6R3iKptf3+Aw6+/fabt6Z1HvpGAAEEEEhWgMCbrCfVEBgagULJ+6YxevZC2LWvC+u1TxbLE6+xsvET3TXxLy954nuXjH1HvGZoBuiikWLRO9mO2NdL5oVLl4eBzz/buvBjCQIIIJAXAf6lkJeTZs5cCRTK1c8bmZd1hn5XGPjvXQpQKFUvNMa8u/NrnU9yaCfgLzqjeu+s728cVrD4bQtyzKmyOtVI47vrk8A7rKdHXwgggMBgBAi8g3FnVwR6JlAseRdbowsWnuzqY2Hdf+PuNusEx4s6b3nYHn9FhWT2l2zLynzGWOerYWPqmp412mXhUql06Lyz8smOjZ4tmfiJdWnXS400a5eEXwJvl7gsQwABBHIiQODNyUEzZj4Exserr44c8+n4/blW+koz8F/wSJMfOjl50IE7on8w0tm7X2c3y5gfyWrGGk3tHHWu2zw1FT8Rbr/GPO+xIztt/B7h7SbSr7pRtlZj1rGOY0d/sv/+dsvU1NSOxesKhcnHaLR1kmP0WBsZT46eIKsnShp5mNrbjMw5c8H0FUu+UEME3m5OgjUIIIBAfgQIvPk5aybNuED8JLRlRhe/SOLfw8A/vtuRC+Xqq43M5ZJWLrnmXkkHd1ujX+usNGWs+bYUfTts1K5b3JfA268TYB8EEEAgfQIE3vSdGR0jsFsBt+xtk7SfpHvDwG//UNrevOLAvNOsPN3Ini7pmbu5Nv5kh/jV/nSH/r7MTyUbh9vrwsC/cXd7E3j7eyLshgACCKRJgMCbptOiVwQeRsAtT3xLss+StS0TGW9uzg+Wg1WsTB5rbXSiZI6X7HMe4S0Fy9nm4a6dl8x3rY1+bB3ziyga2XBrY+PsnjZaEnhbYeCv2NN6fh8BBBBAID8CBN78nDWTZlTALVf/UTLnxeNZR29tzviXJD1qqXTM2nntOMIZsfdrx+iWMNy4JYk9isXimp0rDnzUiKJHR5GdN/uZXzWnpuIvytjr15LAe08Y+IfsdQEuQAABBBDIrACBN7NHy2B5ECiWJs60xsY/pBbH3WvCoPa8PMy964zr1q1bvSNy4vccx58tfFsz8Nfm0YGZEUAAAQR2L0Dg5c5AIKUClUrl0fN2JJR0kKw2hQ2/kNJRlt12/GkRzk7d3ik0EwZ+ddlFKYAAAgggkBkBAm9mjpJB8ibglr2fS3qCjd+364w8LaxP3ZQ3g8V5i9VqxbbMTPuvjX4W1v1j82rB3AgggAACDxUg8HJXIJBCgULZ+5iR3tDOd0Yfnqv756dwjMRaHh+vPilyzM86BW8IA/+kxIpTCAEEEEAg9QIE3tQfIQPkTcAte8+QdG3nkxN+Hgb+H+bNYNd542+NM8Z8r/OEd31Y91+UdxPmRwABBBD4/wIEXu4GBFImUCxP3Gpl1xrpdztMq7q5Xr8tZSMk3q5bmjhDxn6xE3gf9uuUE9+YgggggAACqRAg8KbimGgSgQWBQsm7yhidtvBX5m1hMP0BbKRiyTvHGl0WW1hr39Ns1C7EBQEEEEAAgUUBAi/3AgIpEShUvJcYq3/ptPu/w8CP39rAq/0HgeqFxph3t/8YYPXGuYb/MWAQQAABBBAg8HIPIJAyAbfk/UpGj5G0JQz8+H95dQTcineZrM5p/6U1Lwob0+vBQQABBBBAgMDLPYBAigSWvJUhskYvaNb9q1PUfs9bdSveF2V1RjvvWntSs1G7oeebsgECCCCAQGoEeEtDao6KRvMqUChXX2pkvtAOc7JXNIPamXm1eLi53bIXf0LDie3fd6JqODOz8Jm8vBBAAAEEEGh/RDsvBBAYagG3XL1DMocaaXou8CeHutkBNeeWvZqkdfH2prX9kLm5uXsG1ArbIoAAAggMoQCBdwgPhZYQWBRwy97/kXSCpAe0Ql7o+5vQeaiAW/bigHuwpG1h4K/CCAEEEEAAgaUCBF7uBwSGVKBQqv69Meav4/asdEEz8N8/pK0OtK21a9euGl21emuniTAM/OJAG2JzBBBAAIGhEyDwDt2R0BAC0lip+seOMT9ov+3I2uvCRu1ZuOxeYGzdOteJnLn4d43RD+fq/p9ghQACCCCAAE94uQcQGHIBt1zdKpn4P83fFwZ+/J/qeT2MwNh49WmO0/7DQfzHg0+Fdf8ssBBAAAEEECDwcg8gMMQCbsXbIKujJc070cj47OzGW4e43YG35pa818rokwuB154b1msfHXhTNIAAAgggMFQCvKVhqI6DZvIu4JYnviXZ+O0LVo55VTgzfWXeTfY0v1uauFTGvqmz7plh4F+/p2v4fQQQQACBfAkQePN13kw7xAJueeJyyb6u/aBS+vhc4L9hiNsdmtbcsvcjSU+NG3KikSN5Ij40R0MjCCCAwNAIEHiH5ihoJM8CxXXeBTbSxbGBNfphkx+86vp2cMteK866vN+5azIWIoAAArkTIPDm7sgZeNgECuPeacbRekkrJN0eBv7jhq3HYe1nrDLxbMfab7b/oMBHtw3rMdEXAgggMHABAu/Aj4AG8izgut6YVuiXklZL2u5EI0+fnd34wzyb7M3sS98GYuU8sRlM/d+9uZ61CCCAAAL5ECDw5uOcmXJIBdyS15TRWNyeMc75c/WpDw9pq0PZlluubpHMYZL8MPAnhrJJmkIAAQQQGLgAgXfgR0ADeRVY+sNWMrosrPuLnzSQV5K9mrtY9k6x0tcXLrLnhUHt0r0qwGIEEEAAgdwIEHhzc9QMOkwCxbL3BSu9dKEn8+UwmH7hMPWXhl7c8sT3JXucZO4esTvGGo3GvWnomx4RQAABBPovQODtvzk75lygUJp8lzHRezoM3w8D/4Sck+z1+IXyxB8Z2R93nu5eGga18/a6CBcggAACCORGgMCbm6Nm0GEQWPqJDFaaagb+UcPQV9p6cMsTX5LsCyTtiJyoumlmJkzbDPSLAAIIINA/AQJv/6zZKecC4+PjR0TOyo2SDpbM5jCYLEjr48+Q5bUXAkdWKsURO1KTNCpj1of16RftxeUsRQABBBDIoQCBN4eHzsiDEXDL3i2SjpB0jxONHM03gu3bObjl6uWSib+RLoqsjt3U8P9j3ypxFQIIIIBAXgQIvHk5aeYcqIBb8jbI6Oj4P8FLem4Y+NcPtKGUbl6YnHyM2RHFb1/Y31pd3Wz4z0/pKLSNAAIIINBHAQJvH7HZKp8ChVL1e8aYE9tfBmac88L61EfzKbH8qd2S9xkZvUrSNjtivWat1lx+VSoggAACCGRdgMCb9RNmvoEKFErevxqjF8dNWOmSZuC/daANpXjzycnJg+7f3rrbGDMi2S+FQe2MFI9D6wgggAACfRQg8PYRm63yJeBWJj4oa8/vhN2vNAM//lQBXvsoUBj3LjGO3hxfHhnznE316Wv3sRSXIYAAAgjkTIDAm7MDZ9z+CBRK3luM0Yfib5Ww0o+agf+0/uyczV1KpdKhLTN6R2e67WHg75/NSZkKAQQQQKAXAgTeXqhSM9cCbmndC2WcL8ZhV1IjDPxyrkESGL5Y8s6xRpe1n5Yb/c9m3X97AmUpgQACCCCQEwECb04OmjH7I1Aqlda2zGj88WPx31vbxh53+OobbrhhZ392z+4uxbI3ZaUJK/1mhZ13+Rrh7J41kyGAAAK9ECDw9kKVmrkUmJycXLl1R3SXpNWSWvMj9omba7Vf5hIjwaGLZe8UK329U/KiMPDfmWB5SiGAAAII5ECAwJuDQ2bE/ggs+azdSE70/HBm5pr+7JztXdyyF4fdU+LPMJ43reLmev22bE/MdAgggAACSQsQeJMWpV4uBdyyd52kZ8TDW2tPajZqN+QSIuGhi0WvbEfkSxqRzNfCYPrUhLegHAIIIIBADgQIvDk4ZEbsrYBb8T4nq1fEuxirN841/I/1dsf8VC9WJt5nrW3/gFpkdPqmun9VfqZnUgQQQACBpAQIvElJUieXAm7J+4SMzmqHXdl/ngtqr8klRI+GLpa9zVZ6nKx+HTb8x/ZoG8oigAACCGRcgMCb8QNmvN4JFNZ5F5hIFy882TXfm2tMP713u+WvsluaPFUm+urC5PbSMKidlz8FJkYAAQQQSEKAwJuEIjVyJ+BWJo6XtTcuZDFtDBv+0blD6PHAhbL3aSOdGW/jONFxszMzP+jxlpRHAAEEEMioAIE3owfLWL0TWLt27arRVau3LmRd3dYM/LW92y2/ld2yNy9pRSwQBj7/rMrvrcDkCCCAwLIF+JfIsgkpkDcBt+zZzsz3hoG/Jm/z92veJc4E3n6hsw8CCCCQUQECb0YPlrF6I7DkqeOOMPD3680uVI0FCLzcBwgggAACSQkQeJOSpE7mBdyy94Ck/eNB+U/svT9uAm/vjdkBAQQQyIsAgTcvJ82cyxIolL3fGenA+CuDO092W8sqyMV7FCDw7pGIBQgggAACXQoQeLuEYll+Bdyyd7ek+L26O+z8A2uazea2/Gr0b/KlgXfEzh/RaDQ29293dkIAAQQQyJIAgTdLp8ksiQu4Ze9eSasl+8BKxx4+MzNzX+KbUHC3AksDr2Q/EAa1t0GFAAIIIIDAvggQePdFjWsyLzC2bp3rRM505z27982vdAqbp6Z+k/nBh2hAt+zFf7g4qNPSjsjqjzc1/P8YohZpBQEEEEAgJQIE3pQcFG32T6CwbuI1JrKf6HwG7D123qk2m1O/7l8H7BQLFMvV063MlyXdE7+lxEpTkXVedEtjKv6DyNC8ihXvZGOiFdpp7vz9ppzDtKJld/1166hgHHv/Q9cvXB3/fvy/JlJzd0O2RqL7m/V6bWgAaAQBBBBIgQCBNwWHRIv9EyiUJt9lTPSe9o5Gv3VaI4+fnd14a/86YKelAoVy9fNG5mVSO/zFQbDRMubsW+rT3xmUVKFUPdE4epK15kmSTjDS4/rdy/b7Dzjw9ttvbn/5CS8EEEAAgT0LEHj3bMSKPAiceOKK4u1b1lur0zrj7miZlndLvT6Xh/GHdcax8lHeiFpXWukPJQWSyp1eZ2R1o4ypW6tghXb8cM2aNXdvue++w1bY0UNlW4cs/JnFPtoYs81E+tXijNZqzDrWsTJ3yYl+rR07mrv+IOLhxxxz4KodOw41LXOkZB9vrQ40xhwn6RmSVg7Wy14TBrXnDbYHdkcAAQTSJUDgTdd50W0PBI4sTU6MmOhLkrxO+Xk50dHhzMxMD7aj5F4KjHneY52d+qykkyVFncudvSyTmeV8BnRmjpJBEECgjwIE3j5is9XwCRQq3mnGtsPU6nZ3RjePqvVn9Xr9ruHrNr8dxU9cD3hgPn6P7Ko+KcRfH/1w/3zcIKvvW9kvOZEzNzc3fUufemIbBBBAAIF9FCDw7iMcl6VfoFDxLjZWFyyZ5DNh4L86/ZNld4L4abxj7AmSjd87e7qknn69s5F2RtKMkanJ2mtbcn40bD80l93TZjIEEEAgOQECb3KWVEqJwMLTwp1fluyzOi1HVuaNzWD64ykZgTYTFIjvh5Xbt69xotE1xrbWOI7TipzWnQ+sXHnnlg0b7k9wK0ohgAACCAxIgMA7IHi2HYzAWGniicbY9UYaX+jA3uE49vTZmZkfDKYjdkUAAQQQQACBXgsQeHstTP2hERgrV1/myHxS0gGdpm4aNa1TeL/u0BwRjSCAAAIIINATAQJvT1gpOmwCbql6qYx502JfVvaKZlA7c9j6pB8EEEAAAQQQSF6AwJu8KRWHSGB8/KgjIqd1ZfwFAZ225q3Muc1g+vIhapNWEEAAAQQQQKCHAgTeHuJSerAChYr3Ell91EiPijsx0m3GiV7M+3UHey7sjgACCCCAQL8FCLz9Fme/ngsUCoVDzMoDLpW1r1iy2fdHTet03q/bc342QAABBBBAYOgECLxDdyQ0tByBYsV7jpU+JquxxTpWerLxYfwAAAfLSURBVH8z8Jd+3u5ytuBaBBBAAAEEEEiZAIE3ZQdGuw8r4Ljl6iWSOXdJ0J0y0jlh4N+IGwIIIIAAAgjkV4DAm9+zz8zkxcrEcbL241aa7AwVGekDczzVzcwZMwgCCCCAAALLESDwLkePawcuUChV32WMebckp9PMRseJzuYH0wZ+NDSAAAIIIIDA0AgQeIfmKGhkbwTcSuXxsiPxVwE/tXNdS9KHwsD/73tTh7UIIIAAAgggkH0BAm/2zzhzExZL3hus0QclrVp8qmutzmo2/B9nblgGQgABBBBAAIFlCxB4l01IgX4JFIvVo63j/JOM/ZN4TyPttNKHearbrxNgHwQQQAABBNIpQOBN57nlquu1a9euWrFq9bmy9n3GmAffq2uc6DVzMzM/yxUGwyKAAAIIIIDAXgsQePeajAv6JTA+PlmKHPtsWfuXMg9+rm4k2Q+FQe1t/eqDfRBAAAEEEEAg3QIE3nSfX+a6Xwy5xkanWWOevsuA/xlZ84xNjemfZ25wBkIAAQQQQACBngkQeHtGS+FuBSqVyqN3RitOtSZ6nmRO3c11TWvshc167TPd1mQdAggggAACCCCwKEDg5V4YmMDa8cnSqIneboz5cyt72EMbMTVro387cL+RD0xNTf1uYI2yMQIIIIAAAgikWoDAm+rjS2fz7rp1x6hlXmelNyz5IbTFYbbLaL2Vvtys+1enc0K6RgABBBBAAIFhEiDwDtNpZLyX8fHqkyJjzpbRa3cz6g3G6prWSHT1ppmZMOMUjIcAAggggAACfRQg8PYRO69buaV1ZxjjvMRKp+9qYK15n3HM1WF96qa8+jA3AggggAACCPRWgMDbW1+qS3LLnt0F4sfG6nOO5tc3Go07QUIAAQQQQAABBHopQODtpS61dw27N0VGF2+q+1dBgwACCCCAAAII9EuAwNsv6Zzus/h018qc2Qymr8gpA2MjgAACCCCAwAAFCLwDxGdrBBBAAAEEEEAAgd4LEHh7b8wOCCCAAAIIIIAAAgMUIPAOEJ+tEUAAAQQQQAABBHovQODtvTE7IIAAAggggAACCAxQgMA7QHy2RgABBBBAAAEEEOi9AIG398bsgAACCCCAAAIIIDBAAQLvAPHZGgEEEEAAAQQQQKD3AgTe3huzAwIIIIAAAggggMAABQi8A8RnawQQQAABBBBAAIHeCxB4e2/MDggggAACCCCAAAIDFCDwDhCfrRFAAAEEEEAAAQR6L0Dg7b0xOyCAAAIIIIAAAggMUIDAO0B8tkYAAQQQQAABBBDovQCBt/fG7IAAAggggAACCCAwQAEC7wDx2RoBBBBAAAEEEECg9wIE3t4bswMCCCCAAAIIIIDAAAUIvAPEZ2sEEEAAAQQQQACB3gsQeLs0PuOMM26y1h7b5XKWIYAAAggggAACPRUwxvx0/fr1T+npJhkpTuDt8iAJvF1CsQwBBBBAAAEE+iJA4O2emcDbvRUrEUAAAQQQQAABBFIoQOBN4aHRMgIIIIAAAggggED3AgTe7q1YiQACCCCAAAIIIJBCAQJvCg+NlhFAAAEEEEAAAQS6FyDwdm/FSgQQQAABBBBAAIEUChB4U3hotIwAAggggAACCCDQvQCBt3srViKAAAIIIIAAAgikUIDAm8JDo2UEEEAAAQQQQACB7gUIvN1bsRIBBBBAAAEEEEAghQIE3hQeGi0jgAACCCCAAAIIdC9A4O3eipUIIIAAAggggAACKRQg8Kbw0GgZAQQQQAABBBBAoHsBAm/3VqxEAAEEEEAAAQQQSKEAgTeFh0bLCCCAAAIIIIAAAt0LEHi7t2IlAggggAACCCCAQAoFCLwpPDRaRgABBBBAAAEEEOhegMDbvRUrEUAAAQQQQAABBFIoQOBN4aHRMgIIIIAAAggggED3AgTe7q1YiQACCCCAAAIIIJBCAQJvCg+NlhFAAAEEEEAAAQS6FyDwdm/FSgQQQAABBBBAAIEUChB4U3hotIwAAggggAACCCDQvQCBt3srViKAAAIIIIAAAgikUIDAm8JDo2UEEEAAAQQQQACB7gUIvN1bsRIBBBBAAAEEEEAghQIE3hQeGi0jgAACCCCAAAIIdC9A4O3eipUIIIAAAggggAACKRQg8Kbw0GgZAQQQQAABBBBAoHsBAm/3VqxEAAEEEEAAAQQQSKEAgTeFh0bLCCCAAAIIIIAAAt0LEHi7t2IlAggggAACCCCAQAoFCLwpPDRaRgABBBBAAAEEEOhegMDbvRUrEUAAAQQQQAABBFIoQOBN4aHRMgIIIIAAAggggED3AgTe7q1YiQACCCCAAAIIIJBCAQJvCg+NlhFAAAEEEEAAAQS6FyDwdm/FSgQQQAABBBBAAIEUChB4U3hotIwAAggggAACCCDQvQCBt3srViKAAAIIIIAAAgikUIDAm8JDo2UEEEAAAQQQQACB7gUIvN1bsRIBBBBAAAEEEEAghQIE3hQeGi0jgAACCCCAAAIIdC9A4O3eipUIIIAAAggggAACKRQg8Kbw0GgZAQQQQAABBBBAoHsBAm/3VqxEAAEEEEAAAQQQSKEAgTeFh0bLCCCAAAIIIIAAAt0LEHi7t2IlAggggAACCCCAQAoFCLwpPDRaRgABBBBAAAEEEOhegMDbvRUrEUAAAQQQQAABBFIo8P8ArMaNc/R09PkAAAAASUVORK5CYII=', '0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1\r\n'),
(1000060, '0000-00-00 00:00:00', 'Staff Warehouse', '-', 'staff.warehouse', '827ccb0eea8a706c4c34a16891f84e7b', 34, 0, 'staff.warehouse@gmail.com', 'default.png', '6', 1, '2020-02-23 14:15:08', 1000049, '', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;0;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000061, '0000-00-00 00:00:00', 'Staff HR', '-', 'staff.hr', '827ccb0eea8a706c4c34a16891f84e7b', 2, 0, 'staff.hr@gmail.com', 'default.png', '6', 1, '2020-02-23 14:17:14', 1000049, '', '0;1;1;1;1;1;1;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000062, '0000-00-00 00:00:00', 'Staff MR', '-', 'staff.mr', '827ccb0eea8a706c4c34a16891f84e7b', 30, 0, 'staff.mr@gmail.com', 'default.png', '6', 1, '2020-02-23 14:18:03', 1000049, '', '0;1;1;1;1;1;1;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000063, '0000-00-00 00:00:00', 'Staff MR', '-', 'staff.mr2', '827ccb0eea8a706c4c34a16891f84e7b', 30, 0, 'staff.mr2@gmail.com', 'default.png', '6', 1, '2020-02-23 14:18:39', 1000049, '', '0;1;1;1;1;1;1;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000064, '0000-00-00 00:00:00', 'Staff OPS', '-', 'staff.ops', '827ccb0eea8a706c4c34a16891f84e7b', 31, 0, 'staff.ops@gmail.com', 'default.png', '6', 1, '2020-02-23 14:18:59', 1000049, '', '0;1;1;1;1;1;1;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000065, '0000-00-00 00:00:00', 'Staff GS', '-', 'staff.gs', '827ccb0eea8a706c4c34a16891f84e7b', 32, 0, 'staff.gs@gmail.com', 'default.png', '6', 1, '2020-02-23 14:19:36', 1000049, '', '0;1;1;1;1;1;1;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000066, '0000-00-00 00:00:00', 'Staff GS', '-', 'staff.gs2', '827ccb0eea8a706c4c34a16891f84e7b', 32, 0, 'staff.gs2@gmail.com', 'default.png', '6', 1, '2020-02-23 14:20:01', 1000049, '', '0;1;1;1;1;1;1;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000067, '0000-00-00 00:00:00', 'Staff HSE', '-', 'staff.hse', '827ccb0eea8a706c4c34a16891f84e7b', 19, 0, 'staff.hse@gmail.com', 'default.png', '6', 1, '2020-02-23 14:20:45', 1000049, '', '0;1;1;1;1;1;1;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000068, '0000-00-00 00:00:00', 'Staff Finance', '-', 'staff.finance', '827ccb0eea8a706c4c34a16891f84e7b', 7, 0, 'staff.finance@gmail.com', 'default.png', '6', 1, '2020-02-23 14:21:21', 1000049, '', '0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000069, '0000-00-00 00:00:00', 'Staff Proc', '-', 'staff.proc', '827ccb0eea8a706c4c34a16891f84e7b', 11, 0, 'staff.proc@gmail.com', 'default.png', '6', 1, '2020-02-23 14:52:57', 1000049, '', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000070, '0000-00-00 00:00:00', 'Staff Proc 2', '-', 'staff.proc2', '827ccb0eea8a706c4c34a16891f84e7b', 11, 0, 'staff.proc2@gmail.com', 'default.png', '6', 1, '2020-02-23 14:52:59', 1000049, '', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000071, '0000-00-00 00:00:00', 'Staff Proc 3', '-', 'staff.proc3', '827ccb0eea8a706c4c34a16891f84e7b', 11, 0, 'staff.proc3@gmail.com', 'default.png', '6', 1, '2020-02-23 14:53:01', 1000049, '', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000072, '0000-00-00 00:00:00', 'Staff Proc 4', '-', 'staff.proc4', '827ccb0eea8a706c4c34a16891f84e7b', 11, 0, 'staff.proc4@gmail.com', 'default.png', '6', 1, '2020-02-23 14:53:04', 1000049, '', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;1;1;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000073, '0000-00-00 00:00:00', 'Direktur Utama Approvement MPI', '-', 'dir.mpi', '827ccb0eea8a706c4c34a16891f84e7b', 33, 0, 'dir.mpi@gmail.com', 'default.png', '5', 1, '2020-02-23 15:01:33', 1000049, '', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0');
INSERT INTO `portal_user_db` (`id_user`, `created_date`, `full_name`, `badge_no`, `username`, `password`, `department`, `project_id`, `email`, `sign_id`, `id_role`, `status_user`, `last_update`, `update_by`, `sign_approval`, `warehouse_permission`) VALUES
(1000074, '0000-00-00 00:00:00', 'Direktur Utama', '-', 'director', '827ccb0eea8a706c4c34a16891f84e7b', 33, 0, 'director@gmail.com', 'default.png', '4', 1, '2020-02-23 15:01:39', 1000049, 'iVBORw0KGgoAAAANSUhEUgAAArwAAAD7CAYAAACIeEe+AAAgAElEQVR4XuydCZwjVbX/f6fSPTADgg9kdZhOpavSqTTgBrihMs8NBFRAcAdREVwQd0Hf+7uyuIHKIvIUXFFEcEHB7SlP1OdTR3GgU+lUdSoDA4ggKDAzzHSnzv9zK0lPOtPdqSRVWbrP/XxQ6Lr3nHO/t2b6l5t7zyFIEwJCYKAIjI+Pr9i8zd8CQJsncPYce76fD9QcOw12jTGeTZB/K4DhTm0tOp6x2XPtXWL1scSM66Z1N4D9APj+EFZvsO17WpliKp09HL5/OBMdDkD9s3swnrGZiB9g0OqqvYc9x96tFdvSVwgIgaVLgJbu1GRmQmBpEtBNa9uiQo60p3qFiT8szdmHm5VuWlsBrJin9yYAdwC4G4RHwPwIAY8w4xEGHtGIVjJoJYhXMdNKjXkVE60EWIlaJZ7UP/82K7JAV3hO7vRwUUmvpJH5LRE9Q5GYpvLqjYXCXZ1QWT0+vseKrXwUk38UQEcB2KNqr6z+33PsoU7sy1ghIASWDgERvEtnLWUmy4CAns6cBqYrFp8qvddzcp9eBjjmnaJuWr8G8Kxgz4/xHQA/90F/nhny77grn/9Hp1ySZvYMAn8hsMP0Ws/NfaNTm8thvG5kPw3id1ew0QWlQu6cKOedTqcfNw3tOIA+AsZ+DEyVHNuI0ofYEgJCYHAJiOAd3LWTyJchgaRp3U/AngzYJcfO1hAkTetqAl5Z/e+fe479gmWIB7qRPRHESuSCwK8uOvmro+agG9Y1IJwU+CjTSLGYUzvG0hYhkEynM8QJOxC7wI0lxz46LmC6af0CwHMB3Ow59tq4/IhdISAEBouACN7BWi+JdhkTGEmPv1Vj/xKFQCMcN1Wwv79d8NbtOgI8veXhXTZu3KjO+S6rppvWXwEcDMKDXsGufb0dKYP64xKeY8vfoSHo6qb1r+pxkH96jq2OhMTSUunxQ5n9ynEewslewf56LI7EqBAQAgNHQP6yHrglk4DjIJBKW8/TfNrFdXM/iMN+FDZ105oGoM4k7nAZp3pJa6LmZzkKsfpdRIC/6zn5E6Pg3mhDNy1ezpxbZaqb1o8AqB1dn6E9peRMqMuEsbSUaX2BgTOYuTyzU2LvjRMTD8TiSIwKASEwcARE8A7ckknAURIYHT3wAD/hvw/Mb1O/kLduWvWYu+9etzlKH1HYqj+7S4SjiwX7RhFicwnMOVtLfLpXyDc569zeyojgDc8taY6/kuAHx0oY/MWSkz8j/OjWeu5z8MG7rNoy/TcAuzLwvZJjH9+aBektBITAUiYggncpr67MbUECI6PjazXyX6e+9pzTiXCKV7C/1m/o9LR1P3jHs7v1cS53IVa3kwhofsabnJyMYx2XO+dWmOqm5QFIArjHc+z9Wxnbat+UkT2Hic8LxjGd5Lm5a1u1If2FgBBYugRE8C7dtZWZLUBgZHRsraZpv5zvMTP/suTm1YWXvmm6mX0PwJ9SAS20u6ueLWchlkw+8bE0vFXld10J4FbPsZ8U1wIuZ86tMNVN6z8AfCx4b5neUHRzV7YyvpW+hmHsVaZhlc83AWCr59g7tzJe+goBIbD0CYjgXfprLDOsIzCSso7WErghuNJS+Z8pAH9l4HiAywAloPlP8CYn1/cDuCDVEifUL3J1dneb59g7LRTXchZiSTPzOgJdFWzuMX+k5OY/HNf61XG+33PsveLyM8h2q8cLVPaKPYjoV8VC7t/jnE/SzJ5O4MsDH6Sd6RUmgsud0oSAEBACNQIieOVdWDYE9HTmTWC6LNgFYi4TaacVndxVhmGsLtPwndtB9E8xgblnd/ndxUL+wjCCd/PK4V3vXb9eFVlYFi1lWDcyQRUeUIJ3bcnN3xzHxJNG5ggl4Cq26e2ek7s4Dj+DblM3MzcAdAzA/+LpnZOl0q3/jHNOumn9GcCTAPpngrft67quKjwiTQgIASEwS0AEr7wMy4JAyrTOYuCzFZ2CB7TysDU1tf7vtcnraes7YNRu9ffN5bVaEQUCpv0Ep0v5fCmE4H3Ic+xKudVl0Kq74PdVp/qo59jqWEMsTTesr1bPfW/jBI8tth6xBDAARpPpzCnE9BUVqsZ03pSb+2CcYdenImP2P1tyJ98Zpz+xLQSEwGASEME7mOsmUbdAIGVaV/P2ogxbNX94Tb3YVaZSpnU0Ayp9UtD6Ia1XZed5qBQcswB+5Dn2sQtNe25asv7ZoW5hmdrumjKyr2fiLwcGGKd5rv2lto0tMlAfGxuDr92myjoT6Lqik3tZHH4G2eacI0Mx5kKuZ6SnMzeB6UgAm/whmBtsWx0BkiYEhIAQmENABK+8EEuWwOjo+Fpf89VFGXVLXLVtmj98QKPYrQHotzOweto6GYyvqvgWu6ymni/ncreptPVbZjwDoDs9J7cmrhc6ZVrXM3BcmPWIK4Z+tqvEbmII32duft48qnmMjmae4mv0p8pnHb6i5ORPj8q22BECQmBpERDBu7TWU2ZTJTCaHj/FZz/4WrXS6AbPyb14MUB9J3hnE/az5zn51KKx15e7TawYK+b/WlgOL0MqkzmIyxRcMGSmD5fc3EfimHcqZZmcQF59Sw/gL55jPzkOP4NqM5my3kwJvrj6bcQDmj/3yFBc85rd3WVs9odhyO5uXKTFrhAYfAIieAd/DWUGDQRSRvbDTPyhOrEb6nJRPwneNel0KsGao66ch8k6oJtWORBjzI96bj62M6z99rKlzOzlDD49zBnnTmJPpq0LiPH+wIbkeJ2D0jAyTy8T/a7CBg9qPJxZ6FuUTtagcazs7kZJU2wJgaVPQATv0l/jZTVD3bTUBZmPVybNZc3HU6em8uvCQKgXvFs3rdqllxXXRszMpzTQeyo7l4tnHUhmMkkqk0rwr3ayr/OWydnSVCq1Oyd22gBgdzD9wHNzLw2zzu30qSvrLDleGwDqpjVTzX+76JGhdrgvNqbu7O4jPK2ZpdKEqrImTQgIASEwLwERvPJiLBkCKcN6MxNU2jHVNiV4OOO66zeGnWA/7fDWCaymGRdSZuZ4Bl1X0bv0Qa+Qq1SbWuJNNzNnAvT5yrTnL7ccBYKRtHWcxrg+sMU42XPtr0dhdynY2J4ODOwjMb7Bud3uxrzqd3fBfInn5s/shl/xIQSEwOASEME7uGsnkdcR0I2xk0DaNdUfPew59m6tAuoXwasb2RNB/J2KwNJe4rkTP1xsLrppqR3tIPUTa9pRpcmJn7Q690Hsr5uWOqdsApT3nJwV1xxql9UIuL8ohSZmMafMzHcZdELwA5/P9KbyXSv2UHd2958JTI+4rvtQXOsvdoWAEFgaBETwLo11XNazSBrW04jwv1UIWzzHXtUOkH4RvLNZBxiO59rpZnPRTev/ABwW9JtB0vNs9TX/km7JsexLyOfvByIfdFbJyQU7vVG30dEDD/C1sgtgBRhf81z7lKh9DKK9Nens4QnmW1TsRPh+sWAH2Su60eac3WU6v+TmPtANv+JDCAiBwSYggnew12/ZR7///k9ZtdMum2sVxRYtvdsMVj8I3tTY2CHsa38MYg1RIrWa97SSP5job14ht1+zeS6F57ppqUpqzwHwiOYPj8Z1SSppWmdRrWCJXFabfXXqzu1u8hx7126+U9vP7vLf99htl9Xr1q2b7qZ/8SUEhMBgEhDBO5jrJlED0I3xY0F+7et+33NsVaCh7dYXgte0vsXAK0B48JFhbc19ExOPLDSh6pllVdpWzbvj+bcNrssD9fT4YWBf7WqDwF8pOvlT4wpBN60cAHVc4uHy1qGRO+647cG4fA2KXd20fgHguQDKCZ5Ouq4b+px8p3Occ3ZXw//zJu2PdWpTxgsBIbA8CIjgXR7rvORmqRvWa0H4WnVi//Ic+7GdTrLXgrf+63MGf77k5M9aUOymsms4wZWjC/OUSu6URT+PT5nZqxj8OhWjz/yMDW6+dpwl0rCTRuYIIvpVxSh/03Pyr4nUwQAa0w3rO6CgBDeDcUq3L/Clah8IgW3TVE5tLBTuGkCMErIQEAI9ICCCtwfQxWVnBHTT+k8AH61aedRz7EjyzvZa8CaNzIeJKMgfzL5/VGlqcsHLZ7ppbVMlbtUum+YP7x/XV/qdrVT0o6sfCiYBrATznzw3f2j0XioW9bT1JTDeUNG7zS8PxhVHv9hNGZlLmOit1Q9ZX/YK9hu7GdvjM5k9V5Rxb1DcgvmHnpt/STf9iy8hIAQGm4AI3sFev2UXfcq0zuLamUqg7Dn2UFQQep2Hd7Z4BHiL5+QXvHi3xhjPJsifqAgxfN1z7ZOjYtDvdnQz+x6AP1URXXy6V8hfEVfMupGZAZE6LhLZh6q4Yo3bbjKTOYLK1d1u5nWemz8kbp+N9nUjcy6IggtqPuH4DQX7e92OQfwJASEwuARE8A7u2i27yFNm9lQGX1md+LYya0+6w51QZywjab3c4Z0jYkFXeE7u9IUmlTSzZxD4CxXBS6/13Nw3IgEwAEZSpjXBQBbgLZgZ0j3v9nvjCHsknT1KY76xoqvx1qJr1/I7x+Gu723WfaPQM/Ffl5t62nPsFX0PTQIUAkKgrwiI4O2r5ZBgFiJQf0GNgBl/estjSqXSo1ES66XgbUXEpkxrHQNPDuauDWe8yfXqK/4l3+ZeVqPrijFWlEuZ2SsZfCoDm4Z4Wndd974lD3iBCdYVl/CHqbxPoVC4v9ss6j+AAPxpz8m/t9sxiD8hIAQGm4AI3sFev2URfSqVfS4n+OfB9SzwfZ6T3zuOifdS8OqGdQ0IJwU7imUaKRZzdzTOMTk6fiRp/ocBPLX67F7PsfeNg0U/2kylrUuZ8RYVG2v00tJk7gdxxJlMJnem4ZUq88CeYFzjufYr4vAzCDZ10/oKgEru4S4Xl6jnkzKtLzBwRvDng/D8YsFWmSKkCQEhIARCExDBGxqVdOwFgdXpJzx+mLepbASxn6XsqeA1LbVbvZNi7Dn2Dn8uV68e32N4pf+P2TVgLntuPrLzy71Y21Z91n2l3VG+5WZ+U+b4qxj+N4N+zC/03PzPmo1Zis+TaesVxPhWVWR2tbhEI8+kaW0k4PEAHvAce8+lyFvmJASEQLwERPDGy1esd0hAN62iujCvshHwtLa6VJr4W4cmFxzeY8HLtcDmE7y6aW0BsHOlD33ec3ILpiyLi08v7c6trsUfKbl5tdMdS9NN69cAnoWQle5iCaLHRlePj+8xvM2/J6gwB8x4jq0ygvSkzcm9S/QTr5A7qieBiFMhIAQGmoAI3oFevqUdvG5aamft+UrsQqNXeZO578Q5434VvKm09U5mXBhIXeIPFAv58+Pk0I+29XTmTWD6oopN07TDpyYnfhtHnKOj1oG+htuWM2s195SRfTUTB5chmeP9gNFsHevXvtexNItVngsBIdC/BETw9u/aLOvIdMO6FFQ5r0nAJ4uO/f64gfQyLdliYls3rbsB7MeE35UK9jPj5tCP9pOm9WUCXq9iK+88tMcdt8VT8Uw3sheD+G2B0KOyVSoU8v3II+6Y6ot7MPPakptXpZx70nQz+0WA31QV3z2NpScAxKkQEAKREBDBGwlGMRIlAT2dPgyc+H0lIxRuLjn22ijtL2SrH3d4U4b1FiZcGgh/zT+0ODn5p26w6DcfupmdAjgFxp2ea6+JI77Vq1evHF75mDvVZTUC/rvo2M+Lw88g2NRNSx0d2geMv3muvV8vY9ZN6y8AnqhimO+4Ty9jE99CQAgMDgERvIOzVssmUt20tlbPDm7xHHvBAgxRA+lHwaublsrWcMBy3t1Np9OPm+ZENS0Y/9Bz4qmwNSfPM+EUr2DXSldH/ar1tb36ksoE+krRyZ3aq4BHLGs/bQbqGw4w8PuSYz+9V7GIXyEgBAabgAjewV6/JRf97IUhdVbT18ypqQm3W5PsN8GbMjPHMOiG6vxf4Dm2Ss227Foybb2UGEFVLWI+p+jmL4gDgm5mbgXoCQD9c4VWXjM5OflwHH763WbSGLuASAuOEBG0Vxediat7FXP9nwEmuqBUyJ3Tq1jErxAQAoNNQATvYK/fkoo+OTZ+JPn+TZVftPhc0bHf0c0J9pvgna1uxdjguXaymyz6yVfKtC5iIHgX4jpPqpvWswH8T3XeX/Uc+3X9xKCbsdT9Objfc+y9uum70VfSyHyYiD4U/JzpJM/NXdvLeMS3EBACg0tABO/grt2Si7zuF21Pypf2k+BNmtbvCAi+vtV8PmRqKr9uyS14yAklDWsdUaWynD+E/TfYtkqXFWlLmZnLGVQp57yMc++mzOzlDA449EOBB93M/gTgF1b+HHT3G59IXzAxJgSEQM8JiODt+RJIADUCNcHZq4sp/SJ4QbgGjJcHoqNLGSr6+S3UTaus9A6Ahz3H3i2OWHXTUmeEHwfgTs+J51JcHHFHaXNkbEzXfG2qonVxfdGxT4jSfju2dNN6EMBjGWSXnFy2HRsyRggIASFQ/X0qIISAEFAE+kbwzi4H/8hz8scu59XR09lngVkVgogtH2wqbb2IGT+u7O7SZzw3957lyFw3MzZAGQCbEzy9h+u66vJoz5pujL8Y5Aflo4nwiWLBPrtnwYhjISAEBp6A7PAO/BLKBKIi0E+Cl4G/lBw7+Bp/OTc9nf0AmM9VDMqsjd/hTuSi5qGb1lcAnKLs+szP2ODm/zdqH/1uLzWWfTX7lUITAL/Xc/Kf7nXMSdP6FgGvqAre5xcL9i96HZP4FwJCYHAJiOAd3LWTyCMm0EeCtydnmCPGGYk53cj8BkTPZKBUcmxVYjryljKtaQaGAGz1HLtavjlyN31tUDesTSCoFIA9LSNcD6nuKEtX0xP29UJJcEJACLRNQARv2+hk4FIj0C+CVy7nVN4swzBWl2moBFAirpKyo2Njz/R97TfBLiLw7aJjv3KpvdfN5jM6Om74mu9U+93sdanQy2JxjZrW+3zgE0Efxsmea3+92TzkuRAQAkJgMQIieOX9EAJVAr0qLZwyrEu5WkZZhdKrS3v99iLoaetkML5a0Tz0tJKT+7+oY0ya1vuoJqyIz/QK+Uui9tHv9pJG5oVE9JOK6O9toYkaKz1t3QPGvgAVPSc32u8MJT4hIAT6n4AI3v5fI4mwSwS6vcObNMZfCOLzCfyk+imK4K3QSJnZqxgc5MOdXqHtuXFi4oGoXwXdzPwAoBcruz7Tkze4OVXGdlk13bDeAMKXgg8WzB8pufkP9xJAKm1dwIyg8IUGPnXKyasz1tKEgBAQAh0REMHbET4ZvJQIdFPw1pdvbWQogrdCRDetLQB2BrjsOXl1xjbyppvWPwDsAeYtnpvvWhnryCfSgcH64g4MPrXUQ4G5enx8j+FtvgdgNxDWeQX7kA6mJkOFgBAQArMERPDKyyAEqgS6JXhHUtbRWgKqZHD1zx99HuC31xZCBG9wfne3Mg3/SzFhYKLk2AdG/aIaxoGjZSpXS1fTTz0nd2TUPgbBXv1OelyV7MJySJrW1QS8kggzPmuHlpyJW8OOlX5CQAgIgcUIiOCV90MIdFHw6unMm8B0GYAEmMsMPK/k5m/ultgelMVeY4xnE+RPBPEy/dZzc4dHHbtuZF8D4tplqP/0HPvjUfsYBHt6OnsLmAO+cVWyC8NhzrcejC97rv3GMOOkjxAQAkIgDAERvGEoSZ9lQSBu0ZkyrbMY+GwAk/CAVh62pqbW/139Z9y+B20BR8fGn+n7fpA9AcCk59iqIEKkTU9bl4LxlkBTM69VHzwidTAgxurSfz3kOfbuvQpbN607AByg7m16jp3qVRziVwgIgaVJQATv0lxXmVUbBOIUnSnTupqBWsqrrZo/vKYmdkXw7rhYc3Z4gfs9x96rjSVddEjStCYIyALse05+haptEbWPfrc3YmSerhH9rvIhjD7oFXLn9SLmpGl9goD3Bb4Zp3muHVyikyYEhIAQiIqACN6oSIqdgScQh+AdHR0/0k/4l4MxUgW0TfOHD6gXuyJ4d3x1RixrP20Gd9eeRH2uuXo5Sl1YAxH9sljIPXfgX+A2JpAyrHcy4cKK4NWe6hUm/tCGmY6GGIaxV5mG7wmO+QBSdKUjmjJYCAiBhQiI4JV3QwhUCUSZh3d09MADZhL++zTmt20HTDd4Ti5IgdXYovS9FBY0mUzuTMMrVZaGoEUteHUjcyyIflgRer3b2ez1WqVMax0DTybgz0XHfkov4kkZ1luYcGmwFIyzi65dKTghTQgIASEQIQERvBHCFFODTSCKHd6kkXkOgV4PwslzadDbPSd38UKEovA92PQX/xAQteCtz/VKhOcXC/Yv+omfns4+Cz7to3FZpeiqNloD+P+cmpr8VRSxjhjZJ2nEf64KzXcVXfuiKOy2akM3rNtAOJCZtwxhZsR13ftatSH9hYAQEALNCIjgbUZIni8bAp2KzpSRfT0Tf7kB2FbN52dOTeXXLQayU99LcZHiZKKbmVsBegJAmxO8bT/XdR/qF4a6Yb0VBPXhaL6/n9lzbC2KWHXT+iSA9wa2ND/jTU5ORmG3FRsjo5lnaBr9Vo1h4Hslxz6+lfHSVwgIASEQloAI3rCkpN+SJ9CJwGq4lKaUyhRA5xad3FVhwHXiO4z9QewTF5M16XQqwYmpKpP/9hz7eWH4GMb4i2dQ/lfJzf9PmP6t9hkdzR5V1vhcAuZU3tvBTkRnbXXTUmek9wNwq+fYi/tsdTIh+yfT1oXEeKfq7hOO31CwvxdyqHQTAkJACLREQARvS7ik81Im0I7ASqWtFzFwWd2ltGkCnR5W6NZ4tuN7Ka+FmltcTKq5kL+ofBDzOUU3f0EzlqtXr145vPIxm4JdV4YD4GYCF3kIf/by+Z81G9/seeOZ5cX703s9J/fpZjYXe64b2RNB/B3Vp5flhJOmtZGAxwN8n/f4fffHzTfPdDIvGSsEhIAQWIiACF55N4RAlUCrAitlZk9l8JXbAS58Ka0Z5FZ9N7O3FJ7HxUQ3sz8E+NhA8BI9q1jI1fL9LoitLlctNx41IOYPFd38RzthrpvWNIBq+WT6vOfkzqq3V6tAVv3Zzz3HfkFn/jLfAOjVVcF7RFy71ovFGHxYZPw46COFJjpZThkrBIRACAIieENAki7Lg0ArAqvxCAOw+KW0ZgRb8d3M1lJ5HheT7eKVt3pOfudmvBpyAtd3rxO//Fcw/tNz86pkdEutwf6PPMcOxPhcwZs9g8BfqP6Mp7c8vMvGjRtns1i05LCye+5XhfsWz7FXtTo+iv66aX0FwCnKlk/0og2F3E1R2BUbQkAICIH5CIjglfdCCFQJhBFY8+fV5Wc0u5TWDHIY381sLLXncTCpv1jIwDtKjv25ZtyS5naxyaCrAN6ZgJcBGA7GEh4E49+qdn6s+dr5U1MTwUWsMK3ePphe67m5bzSOaxTdnWStSGYySSpTNfsDXec5OTWXrreUaU1zZVd7m+fYO3U9AHEoBITAsiIggndZLbdMdjECiwmslGkdzeBTATphu432jzA0xhGHuBv01Y6DiW5mbYAzzFxO8JA+NXX7nc046UbmWhAFopDKPFYs5gvq31PpzDnM+BhAqmCCD2Z1RqKSQYGh+vwPE1xo2vrS5MRP5vOj8jX7ifIvwEhX7K8YKxb/GtiP6x1JmZnjGXRdYL9HOYiDtGvMv66gwvtLjq0yRkgTAkJACMRGQARvbGjF8KARaCz+sNOuW57CzCcQ6FiAU3Pn09kRhrjEzKAxXyzeuvXY7Dn2Lp3OLWVmjmFQcOSAgW+XHLtW6nlR07NHIAgPegV7j/rOI2NjuuaTEr3BediK6eCfOanDfN9/7oapyV/WxqZS2ecigdcw+HXb7dG9npPbd6FgovoAoJvWxwF8MAhW045aSIx3ynvRtU1nPwDmc4M+PUqJFuf8xLYQEAL9R0AEb/+tiUTUIwL1gkKlSmLCfIn4Q+XVbXUKUYmZVv32a/9qudm/V+O72XPstZ3GqpvWXwEcrL5CpzIOLBZtlW1hcbGbzpwGpitUJyIcXSzYN843oFq5Te2aVo45bG+Vc76EB/0yH7NhKv+7VCq1Oyd2+md9JwJmio7dOHaOoajeEd20fg4gSMVGZRopFnN3NOMQ9XPdtP4C4Ikg3OYVbLUm0oSAEBACsRIQwRsrXjE+SATqBcUOcROuJcZXi45duVUecYtKzEQcVs/MJY3MEUQUVBRj4Fslx35VJ8HM3d3lq0pO/vVh7Olm5u8A7cWAXXLsbLMx6qytRvxsgJ9NgDr+sgLAAwDUzvBDBP8dDE0J6CAjQyv5mqN6R2Z3rBkPeu7cHetm84vieSqdPZyZbwnWlnBRqWC/Kwq7YkMICAEhsBgBEbzyfixrAiPp7FEaYxzMaRBOa4AxDaIPJ/xtX3Ndd2OcoKISM3HG2E3bKTPzSgZdHfhk+ozn5t7Tif/Z3V3CA6uGtZGJiYlHmtlLmtnTCXx5IEwX2d1dyM7o6MF7+9rMjwA+tE70znYn4LNFxw6KLoRpUbwjesgd6zDxtNsnaVpfJiD4wCHZGdqlKOOEgBBolYAI3laJSf8lQyCZzCRpuHZbvXFaO+ZCjXPiUYiZOOPrtm09nXk3mILiCsR4V9G15zteEiqsObu7zB8qhcyZq5vWZgArlZN2syI0iN5AvlemFG7HuH6CUbwjSdO6n4A92/EfCnaITrqRmQEFF/0kO0MIXtJFCAiBaAiI4I2Go1gZQAJ1xQR2iL5dgdMuhijETLu++3FcfclZMJ3kublr242ztrvLwD0lx94/rJ2o1qQieqfvrffbzo5xp/HM2bEGn1B08teHZRFVvzkV3iQ7Q1RYxY4QEAIhCIjgDQFJuiw9ArppqfOhR1RmRjcwcGNdYv+2d/TaJdWpmGnXb7+O09PWd8A4MVgd0g4rFib+2E6so2nrOJ8RCDsmvKtUCL9THPWadJp1otN46qq5Peo5drBz3e2mm5Yqw/x85bdM5dE7CoVit2MQf0JACCxPAiJ4l+e6L+tZjxiZd2lEnwkgMC71XAhPky0AACAASURBVPtt6l87FRSdQO2l707ijmusblq/B/BUZV/zh/eZmlpfy9jQksukmXUJPArgTs+x17QyOOo1qdlr99uDTuKZW7iCrvCc3OmtsIii75wYmH/iufmjorArNoSAEBACYQiI4A1DSfosGQIjKetoLQGVi1W9+w95jr17bXKNeXjvvnudOsPZldaJmOlKgF12UleFq+3ddt3MvgfgTwWhE5/uFfJBerGwrd/WpD6ezSuHd713/fpNYecSpppbWFvt9tNN62MA/iMYz9qJnjvx3XZtyTghIASEQKsERPC2Skz6DywBJXYTQ/g+M4bAXPbcfJAaaj7B2+4uXLtw+k1ctTuPqMZ1yqOSx3fonkoVNN7iOflVrcbWaQyt+mvWvy6eOR/Umo1Tz3XDugaEk4K+M37G8yYnw4yLsI+mm5bKdLIfA3eVHHt1hLbFlBAQAkKgKQERvE0RSYelQCBlWG9mwsUA1O3wBzR/2Gr8mryXAqeXvvtxfTvlUX9Bi8FnlJz8F1udZ6cxtOpvsf6dHkmou6AZSdW6VufWkGbuPM/NBZXepAkBISAEukVABG+3SIufnhFIpTIHcYLWVwOYV+wGu2CmpVJGBU12eHu2XIHjTtciZVrrGHgyCBu8gp1sZzadxtCOz4XGdHIkoVOxHMU8Uunsb5j5mequmuZrmampCTcKu2JDCAgBIRCWgAjesKSk38ASqLudPq35w6sXugDVS4HTS9/9uLCd8Eilxw9l9v8QzMvn93pT+SCfb6utkxha9dWsfydHEjoRy83iCvN8dDTzFF+jP6m+xLip6NovCjNO+ggBISAEoiQggjdKmmKr7wikzOzlDA5upBPh+cWC/YuFguylwOml775btLk7vC2n0EqamcsJdDqYt/BMIlUqTfytnTnWr4nv8zM3TOV/146dKMZ0ciRBNzLXguhlwZ+BMo8Vi/lCFDGFtaGbmc8B9PbAP3BMXOW5w8Yj/YSAEFieBETwLs91XxazHhkb0zVfm6poXVxfdOwTFpt4L0VnL33328tQuXA2XEtDdrPn2GvDxphMJh9Lwys9AI8F07Wem6tc1Gqj1a8JCA/6ZT6mF6K30yMJs2KZ8KBXsPdoA0XbQ5LJJz6Whrc+WDXQ8oeXth3LQCEgBIRAAwERvPJKLFkCetpaD8ZBADYneHoP13W3DoLg3brbql3uXte9lGj99gIkjcwRRKQKg6gtwW94Bfu1YWNMmpnTCXS56s/MR5bc/E/Djm3sp5vWIwB2UZccAewB8EPs0+mlKfvb7dpsZ1wnRxL0dOY0MAXp2Nqp7tZOvPVjkmnrpcT4XmUttTO9wsQlndqU8UJACAiBdgiI4G2HmozpewJJw3ozES4Lfs8yn1N08xc0C7qXu6x1vh/xHPsxzWJdys91I/saEH89EK1EF5QKuXPCzrdWRhgMx3PtdNhx8/WrlASe+RHAh4LxIAj/FoTE9Maim7uyE9utjK0vwtFqSjHdzPwdoL0YsEuOnW3FbxR9k6Z1PQHHAfz3VSsSB0xMTGyLwq7YEAJCQAi0SkAEb6vEpP9AENBNa6aagix0ztJeCd5kJpOkMqmv4ZU8v85zcsF5y+XaUkbmbCY6v4KDz/QK+VC7gkkj80Ii+kmV2/s8x64UneigzRG9qi5fpWCJ+p/PFh37nR2YDjU0ZVpXM/DKaud7PcfeN9RAAPWp2Xqxu3uAceDoEJVtAMMEfLLo2O8PG7v0EwJCQAhETUAEb9RExV7PCeim9WcAT1K7ccNU3rtQKNwfJqheCd6UmTmeQddVBB590CvkzgsT71LtoxvZi0EclHsm8MuKTr7CpknTTetmAM8BsI0TPFbK50vNxoR5XhG908Xq8YbtQwjrfNLet2Fy4pdh7LTSJ2UeeAxT+RIwRqrjyp5jzymU0syeblqqUuBK1a/bafaUz6RhXUgE9aGgXKZy+o5CQTGUJgSEgBDoCQERvD3BLk7jIqCb2e8CXLmc5vOZ3lS43UHVvVeCVzetjwMIEvGzph1Vmpyo7VLGhamv7davIZF2WLEw8cdmAafGxg5hX6v0Y/qh5+Ze0mxMq891M/tFgN9UHfcvAJWy1AyV9eB/QHSb5vOdgPqn1mgN4P9zamqyciY5REulMmlOUF0lNLrBc3IvDjF0Tpdevc8qCMMwdqpePNwNwM89x35Bq/FLfyEgBIRAlARE8EZJU2z1lMCadPbwBPMtKggifL9YsI9rJaBeCQTdtH4O4HkqVs0f3mehPMGtzGWQ+9afWd28cnjXe9ev39RsPrphfRWEk1U/n3D8hoJduSgVcdPTmTeBqVa1TR1xUP9oTdz4mr9tv6mpqVrmiUW71x3HUW/y2z0npyoEttx69T6rQFNG9hwmrnxTwXSS5+aubXkCMkAICAEhECEBEbwRwhRTvSVQEwoMbCo59q6tRtMrgaCblrrIMwxgq+fYO7ca91LrX1coJNRX8avHx/cY3ub/o8oh9kt/1UIK/1tds2b4K+d+Q2abqD+OQ2V6frGY++9mDhZ63qv3OZ1OP26aEyr3sSrjLe90uwso44SAEIiUgAjeSHGKsV4R0M3MrQA9AcBMeevQ3nfccVst92fokHohEObmWMWPPcc+JnTAS7Rjq+uQMrKvZuJvVHcTX+u5ucq/x9zU2mnEzwb42QSoYzQrFnBZDsQf06KxpczM1QyqXFBr8TjOfH5b5RgVrvpUaAD+03NsdWRHmhAQAkKgpwRE8PYUvziPgkDStH5MgCpXOu1r/gs3TIY/L1nvvxcCoT4FF4FfXXTyV0fBZJBttLoOqXT2l8y8lsB3FZ386n6Z+5x8wpWg7tu8clhvPKIxOnrgAX6i/GkwqkUy2juz2zjveo5bN63a5e67u5PbWTesP4JwCKk/jwlOR3V5sF/WVeIQAkJgMAmI4B3MdZOoqwT0dPYzYH6X+k+VyqpUyH2gXTh1AmGL59ir2rXTyriUaX2BgTMAxP5VfCtx9bJvK4K3/rIaM19QcvOhc/Z2Y47JdPZ8Yj4bwD9B+FF9EY3R0fG15YT/OuLK2WPVCHikGFEe5lY4RsUilR4/lNn/Q9XejzzHPjYq22JHCAgBIdAJARG8ndCTsT0lMGJZT9Zm8KdqbtRfeI79/HYDSiaTO9Pwyi3V8b/3HPvp7dpqZZyetkqV1FP0B8/JPbWVsUu1bytCLWVa32LgFQA2+UMwN9j2Pf3GpXouV51HfnIttpHRsbWapjWmM4v0vGsrHKNiljIzlzPo9EC8E44uFuwbo7ItdoSAEBACnRAQwdsJPRnbUwK6aanb+2ondpPXxiW1+uBHLGs/bQZ3V3fZvl107Fqy/9jmWL38pAS7kgdXeE4uEAr92lJp63lUpuGpqdxNccYYVqgFRwG0sqvOzjL4ipKT72t+s2I3ZR2tJXBDXRGLKYDOLTq5q6LkGpZjVD5Xr169cnjlYzZWyjBT3nNyVlS2xY4QEAJCoFMCIng7JSjje0JANy2V1/QIdb1H8/GEqSn79k4Cqb88RkQXFFsoZ9uu35RhvYMJFwVyl+k1RTf3zXZtxTHOMLJPmgG/gIBREJ4NYEz52Urlve4OWcyjnbjCCrWUaX2CgfcFPphf6Ln5n7Xjr5tjRlLW0YkhfJ8ZqohEmUCnRS10a/MJyzGq+c8pCQ06o+TkaunbonIhdoSAEBACbRMQwds2OhnYKwK6mfkUQO+pCp3zPDcfFG3opI2OjT/T9/3fBNoJ9OaSk7u8E3thxqYM63tMeGkgeBM8VsznVQGDvmh62joZjK/OE8zDnmOrYgKxtbBCrdaPgIeLMccUxWRTqewaDPFUVew+oPnDVpw5l8NyjGJuyoZuWioH9uEA37dqRWL1xMSESrcnTQgIASHQFwRE8PbFMkgQYQnoY2MHw9fUpZidCPhz0bGfEnbsYv10w3ojCP8ViM8unT3UzcyMkrrKZy9Kvy7EQ09b/wXGG+d/Hv/RizBCLWVaRzPwIxUjM95Zcu3PRvEexGlDN7N/APhQALGL3aoAVTmAgxb3+zU6ah3oa7gt+PMDfLLo2O+Pk6XYFgJCQAi0SkAEb6vEpH9PCSRNax0B6vLP5gQPj7nuenVmsOOmG9mLQfy2wBBpT/QKE3/t2GgTA2GEXdwx1Ns3DGOvMg0rEXlY9efbGLguOHABflXwsya5ZKOINwwX3bR+CkCVq52e3vLw7hs3bqxdOIwihFhsVASvqqY3dEycO7u14MNwjGqiSTNzOVUuq5V9zTc3TE56UdkWO0JACAiBKAiI4I2CotjoCoFU2noRM34c6C7mD5Xc/Eejcrz961iAyjRSLObuiMr2Qna6KUiazSU5mjkFGn2GgD0DvsBnS479TvXvumF9H4SXBJ8FusCmGRfDMFaXafjOyocTfN0r2LNpvZrNczk9b8YxKhbVymp3VQpv8E89J39kVLbFjhAQAkIgKgIieKMiKXZiJ6Cb1kMAHsPAXSXHjrTAgG5afvXWfNfy4XZLkDRbmBFz/FUa/O0X5kg73StMXFEbN1vql/lfnpt/bDN7nT5vxkU3Mp8BUSX3MuPpJdf+fac+l+L4ZhyjmnPKzF7F4NdVPn/wCUUnf31UtsWOEBACQiAqAiJ4oyIpdmIlkDSyZxPx+cqJ5vMhU1P5dVE5nFveN/4zqnVCcvaM5eaVw7s2VuCKan6L2VFZA+pTZJU1//A7Jid/WxuTSmfOYabzgv9m7SWeO/HDuONaTKgdMDa2/7CvbWAEWQ4izVsb97y6bb8bgjeZHN+Xhv1a7uN/eY4d+weibnMUf0JACCwNAiJ4l8Y6LulZjI+P77p5m78hyO/J9FPPzUX6lWnSzJ5B4C9URB291nNz3+gG0DpB8pDn2Lt3w2e9j/oUWQTMbFuh7bNxYuKBWh/DMHYr07A6i7kHGI7n2uluxLiYUNPNzOcBOlPFQdBeXXQmln0p5oXWpBuCtz41HDHeWnTty7rxjogPISAEhECrBETwtkpM+nedQNLMfI5Abw9ETtk/tFicrBZriCYU3bCuAeGkQO9OD1ml0m35aCwvbKVXu8q1iBrywc6bNSBpWBcSITjHS6A3F7uQqk35Wkio6fqB+2CoXAKws0p95Tn5veNep0G2H7fgrVYnVGepHwfA8ZzufCAa5DWR2IWAEOgdARG8vWMvnkMQWD0+vsfwtnIRoN2J+ZdFN//cEMNa6qKbVlmdlACwxXNsVbkt9tarXWU1sdHR8Wf6mq9ypqo///OKXcMwdirT8N8B7EaAW3RsM3YoVQcLCbU5ZWs1ek1xsr8KdXSLT1g/cQte3bQ+BuA/gnjYP8lzJ68NG5v0EwJCQAh0m4AI3m4TF38tERhJZz+gMZ8b/E5lXlty8ze3ZKBJ51TKMjmBasEH/qbn5F8Tpf2FbNXvKncj80EtjmT6oAzxjKpKp/L/btP84QPmS5GlpzNvA9PFwTjiN3mFfJCjuBttPqGWSqV258RO9wEYJuCuYsSXFrsxr277iFPwrjnooH9LPDqjjruoozg3e469ttvzE39CQAgIgVYIiOBthZb07SqB8fHxFZu3+So92D4A/uA59lOjDqC+HCrAb/ecfEXkxdx003pUFc9QbuIuCjA7lSOOGNLvulflLVY8y742bG6YXD9vvlTdtGaqovhRz7FXxoxjjvn5hJpuWFeAcFovBHg35x6lrzgFr25mvwvwCQQ8wjMJw/NuvzfK2MWWEBACQiBqAiJ4oyYq9iIjkDKyZ3M1MwOBjy06+aCyVpRNN6xLQHirskmkHVYsTPwxSvsL2YpTjCzsM3MrQE9QO7tEOK5YsG+cr2+vzxc3sqmeFd1UPXZyh+fYI91Yo0H3Uc8xyiwgqbGxQ7hS7ZCizoc96MwlfiEgBPqXgAje/l2bZR1Z5eyur3aNhkB4wCvYQUGEqJtuWn8B8EQlAj3HDnZcu9G6LXiTpnUdAccHNSWIz/AK+dk8u43z7eX5YhVLIxvdzH4R4DepZwy8o+TYn+vGGg26j7iygKRMa4KBbJSlvQedtcQvBIRA/xMQwdv/a7QsI9QN6w0gfCmYfEypwlavXr1yeOVjNlcB3+I59rO7BbubgjdpWE8jwu8ql9ToYs/JBRkvFmq6mbkWoJep51TmsWIxXz3j3B069Ww0f3gfX5uufV3+sOfYu3UnisH2EtcufV1auGlo/kHe5OTkYJOS6IWAEFguBETwLpeVHrB56mbGBigDwPYcOxtH+COjY2s1TftlIOwInygW7LPj8DOfzW4K3lqFOjD+5rn2fs3mWMtawYwHS669R7P+UT+vZwNAZQL4z8CHr53uTW2vABe136VkL45d+qqIvlVdHATjS55rV85USxMCQkAIDAABEbwDsEjLLUTdGH8xyP9BMG/CKV7B/locDFJG5mwmCqq3+YTjNxTs78Xhp5eCN5W23smMC6ui/uiFzu3WYtTTmdPAFBx3IELT/nHwmit4+e8A7Q3GbZ5rHxyHv6VoM2Va1zNwXLCOZRopFnPq8mdHTTezNsDqQ+jdnmM/viNjMlgICAEh0GUCIni7DFzcNSeQNK3fEfB0EEpewdabj2ivh25mfgjQsWr0tK+ZG6cm3PYstT6qXtRt3bRql7vvXlc7WtG6sUVG6KZ1N4D9mPC7UsF+ZjPjSdO6n4A9GbBLMe2sN4uhYYc36O5r2nM3TE4Eu/HSmhPQTWtbsBNL2OQV7F2bj1i8R8ocP4HhfzfoFeOH0E7jlPFCQAgIgYUIiOCVd6OvCIwYmadrROq8qbpddVbJyX0+rgDrCk5s9hx7l7j8zGe3G0caUob1FiZcGmgUzT+0OLl4hTrdtN4L4JNB/x7t7irf8wjeX3uO/Zxurs8g+0oamSOI6FfBnyHCRaWC/a5O51N3LOYOz5UsGZ3ylPFCQAh0n4AI3u4zF4+LEKjl9wTwCJW3ri4Wi/+KA1hcl3rCxtoNwaunrbvA2D/s7m5tV5CA+4qO3bOyvfVsmLnM0A7d4OZUNg1pIQgkjcyHiehDqqtP9KINhdxNIYYt2CWZzn6YmAN7ms+HTE3l13ViT8YKASEgBHpBQARvL6iLz3kJjFjWftoM7gwKHjCu8Vz7FXGhiuNSTyuxxi149XTm3WD6tIqJgGOKjv3jxeJLGtb3iPDS4PSAn0hOTd2u1mFOS5mZY0DkNzsH3AqH+frOEbygr5Sc3Kmd2lxO43XTygMYA3Cv59j7djL3dDr9uGlOTKkS02D6qefmjuzEnowVAkJACPSKgAjeXpEXvzsQqN+ZgkYneZO5a+PCpBuZ74LohEAQRnSpp5VYYxe8pjUd5DAG7vEce//FYksZY69n0r4c9GE+z3PzH2zsn0yO70vDvjoPTHGmKpu7897FKnStLF4LfUczmaeo7lP57uyKJs3sUwn8+8oHHb6q6ORf30K4O3TVzey1AAcp6mR3txOSMlYICIFeExDB2+sVEP+zBOrO1G7xHHtVnGjqSvt2teBEbU5xCl7dtP4M4Elqt3aYyvsUCoX7F2K51/j4rrtu89Vu7mNBfJtXyM+bCUHfLqCVlPqp50S/01cVu38CMFvKuGtll2N62arvtCofnYjJxRyzSSNzNlUzjzDhlaWC/e12/Y4Y1pM1gqo8qMnubrsUZZwQEAL9QkAEb7+sxDKPo5tnaut9EeOmomu/qNv44xK8umF9G4SXB/Px+UxvKn/JYnNLGtbtRBhXiSrKrD3xDnci19g/ZWYvZ/Dpwc8Zm0FYtdBOcLscq2uihPqcaneDLHh103oEQHAZslvz0E3rFgCHq/XkaW1NqTTxt3bXRDet/wXwNDVednfbpSjjhIAQ6BcCInj7ZSWWeRzdPFObTFsvJUY15y5f7Dn5RSuPxbE0cQjeNens4QlmJXjULuwNnpN78aJi18y8jkBXVXQsvl1y7Fc29h8ZG9M1X1NnOHf8uyKiCngNxxhUOq0VtTjiTNkWx7rW24xjjRddz8qxE5VvV6Uju8UrtF85UJ3XZtANVX8/9hz7mLh5iX0hIASEQJwERPDGSVdshyaQNK3raTZRPtLFou2EHtxix6RhvYMIFwWykPGuomsH/97NFkce3rpjBzOeYw83m49uWjPVC4KbPXf+tGx62loPxkEANoPxPhBmd4xVBoWZnRJ7b5yYeKCZr4WezykyUr1k1W2h2G7szcZ1ex4pI/tqJv5GJS56r+fkgkuL7TTdtP4PwGEAHuUEW6V8vrSQnWTyiY/FTtueo/HMP4qFwm/a8SdjhIAQEAJxExDBGzdhsR+KwGw5W+CBkmPvGWpQm52SaetCYrwzGM50kufGdzluQaFnWlx7FsXX3clMJkll8qpi5zrPyQUXjRZqc3ZVmb7hubnXNvbVjQPfACp/qfJzfq/n5D+tm9kvAvymWt8EJwzXvV3tALfcxsfHV2ze5m+tDpwtId1todhy4CEHdHseSdP6FgFBZhOf8ZQNrq2OiLTc6vP4gvkSz82fOZ8R3cieCI1PBOPE2eeE28B8s0/aTZ2mQ2s5cBkgBISAEFiEgAheeT16TqD+jGg3zgrqaes7tV/SRNphxcKEupjT1Ra1GEqZmeMZdF0wCaIPeoXceYtNqNkREsMw9irT8F3B1+PAnIt9dZcLwcxrS27+5nbg6aa1BcDOzNhcqtthjppNO7FFMabb86jb4e/oImbKyP6SidcycFfJsVfXWCTT1is0n/cHaQcx81Eg7FPPiYhmmFllBqk1l8C3kK9dOzXVWS7gKNZDbAgBIbC8CYjgXd7r3/PZ158RJeD6omMHqcLibLqZ+QNAhwbasAcpyZTfqMWQblofBxCkE2NNO6o0OfGTxRjqpvULAM+tMFgxViz+tVDfv75KG5he7rm579Se1/PzkchucG63W10vPZ05DUxXVPQ5jq7P7Rs1m1Zji6p/HMdWFoptdHTc8DW/dgzoZs+x17Yzj2Q6cw4x1T4sqfPgDzGQJAQXG1to/HeAZouXMPDZhJ+4cL78zi0Yla5CQAgIgbYJiOBtG50MjIJA/RnRBE/v4bpu7SvuKMzPa0M3LT+4hEV4wCvEe3xioUlELep00/opgBcEgnd6J71UunXBM5cjKetoLYEfVWKjez0nt0NxgqRp3V4VOXd6jr2mfh66af0VQJC+rLzz0B533Hbbg60uVtK07idgTwbskmNnG+xHetyj1dii6h/1Gi8W10g6e5TGfGNlSXGZV7Df2so8DMNYXaahswB6T7hxvBFEP2GfvzmEmQlN07hMNOb72kuA4IhDsmpHreX23zOML5FWvqJYKHT9W5Vw85JeQkAILFUCIniX6soOwLz0seyJ8DnYOSTiDxQL+fPjDjtlWkczKmKPQWeUnNwX4/Y5n/2oxVDdMYNHPceezWPb6FuJ3cQQvs8cFKXw58sPO+cM5zypzXTTUqmugq+z2zl/rI9m3gaNLq6s+9zdXfWzqNn0Yn27PQ/dzJ4J8Oer7/VZJScX/HuYlqxk6/gkgL2a9N/GzOrP6M2LHWPZ5+CDd1n16LZTwXRh9UiMMqtKhO9esz+95eFVGzduVEdapAkBISAEukJABG9XMIuTBURfrRrYVs+xd+4Gpbqd0OnpLQ/v3qtfulGKurAX1qo7uz8IMjMAD2j+sDU1tf7vjdz1tPV1MF4D5i3TOyVWN2Zh6DT22tldEP7hFezH7eA/4gt93XivuvGhZrF5pEzrIgbeEfRh7SWeO/HDZvNW7402o13GxEfV9yXgLjDOY2gbGeWH1DNK8D3e5ORkM5uNz1Nm9lQGvwvAgfXP2vmg1Kpv6S8EhIAQaPi7TYAIge4T6GahidrsKl/bDquqYupL1q97Bfvk7s+84jHK851hLqytSVvPSzB+Vv16eUGxm0qldufETiqX624A/9Bz8uor6jmtE8GbMjJnc7USGBE9r1jI/XeU9vWxsRdTWRsFsNUH762B/sXAQwA9xPAfooS2F8/wDDTyEv6QN5/gj+qd6IRTqzGkDOt7THipGhcmQ0PKyH6EKRCiu1Z9qRR1QyBsSvjDGdddv7HVGBbrXxW+V9b6iOCNkq7YEgJCIAwB2eENQ0n6RE6gWZaAyB0CGBm1PqZp+A9lmxlPL7n27+PwE8ZmlGKo2YU101QJd4duD0rEAts0f/iAhYRevTAhptcU3dw3oxKkj89k9lxRJjcoYwz82nPs58zHql02upF9DYi/HoZ/XZ8HAJoC+7cMa/6FhUJBZaaIpLU7j3ac66b1FwBPVGMTPL2367r3zWcnlbJMTtC1AD+h7rn60BFcYGTmD5Xc/EfbiaHZmG7yaBaLPBcCQmD5ERDBu/zWvC9mrBvWNSCcFASjDWe8yfUtf13a6kRmv0oHlz0nX58+qVVTHfeP8pd//YU1zR/ep1HM6qZVVJvK6o6Z5g/vv9iupm5avwPwdACbpldoa+YrKtFu7Lpp/QrAEc2KGbRjP2VaVzOwQ6W4BRZKXaTawioLc6WqW61IR6iCHWEXv34em1cO73rv+vWbwo5ttZ9uZmbUwQM1br7d09HRAw8oa+V3Ue3YQ8VBCey/F6RdU/0wdIfn2COt+g7bv511DWtb+gkBISAEmhEQwduMkDyPhcBsoYmGHKyxOFO7u5a1nzaDu5V9BiZKjj3nTGFcfheyG+Uvf920VGaLoBxvo9hJpTO/ZKYgRVWChw9Y7KtqfWzsYPiaysAABl9fcvLzpohrJ3Y9nXk3mILKX0z+J0qFybOjYDNiWkdrhEvBqAk1JWY3ALgHoCEQ7w4OLkupf5qeE4/yq/Z2OLX7Hi7ka3R0fG054b+OGA3Hd/jiVSsSH9i8zVcfhtRltUjF/nzz6CaPdjnKOCEgBJYuARG8S3dt+3ZmvTi/OzqaeYqv0Z+qgvfGkmMf3UtAUf7yX8iWbmY/B/Dbgzn7eGVpyv72YnNOGpnziagiRBepQFfvTxUKqbfJjBHWWFNnZgHaB0T7gPn5oGrKNGZ1w3/RHLFh2czJJhEEQTd4Tu7FzdZVVXh7ZK1cZAAAG3JJREFU9FHah4em90FZO4yVYK62qATv3He8vWwWzeZR/3w+ZiOjY2s1Tftlg51Nmua/cGpy8re6aakLaY9Rz/0h7L/Btu9pxWerfcOua6t2pb8QEAJCIAwBEbxhKEmfSAn04vzunItdwKc8x35fpJNq0ViUv/zns5VMZ04hpq9UwqKmpYZVr7CVuur9tTZtvs9z8rPFCBYaG4ZNNePEDdtzvNLbPScXpDprtYXx16rNOe94m+nbWvHZOIdGPgRMAXRu0cldVV3r2VzHmqYdPjU58dtW/LXTNw7O7cQhY4SAEFieBETwLs917+ms68/vdqvSWdK03kfAJ9TEGXxqyclXxWBvUET5y7/RVjUbhcq0oP58P+I5drCLt1jT09lngfnXFT54f8mxVV7WeVt7gpc2ek7ugGZxNIqxrZtW7XL33es214+rzyVMhBkq89OmpvLrwtier0+Ua1Gzr5uWEuPH1P47qp3jMGviI3FCgsrXVHMtlwl0Wk3oNvItszZ+hzuRa5ddK+Pi4NyKf+krBITA8iYggnd5r39PZq+b1qMAdlLO4xYCswLEsP4LhDcGgo557WKJ87sBJcpf/vW2Vq3Qdtq8zb+/+lV1eRtPJ+9y3aYppvR09gNgPjeYu+ZnFsu5qpvWIwB2WYyTOidNhBwBRYB+VCzkfhOW62Js1oxmTkto+EL1gtaC6dXC+moUgFG9j3WFQIJQorLbTPAy86NE6hJecIFtDh/dGD8W5M/m551eoe0536XEVti10jfKd74Vv9JXCAgBIaAIiOCV96DrBHrxi68uQ8CSFrwE5BhQpXp9Ar+k6OSrJYQXX+bZtFaE27yCHZQN7lWb7/1QWQZ8Kn8IhDcEcTEe1Hg4E0Ue3ajfR920/gzgSfX84hS8jeeF5+Ojj42/GL6vio6ottV7/D674uabVe7drrWoOXctcHEkBITAkiAggndJLONgTaIXv/iWi+CtvQmt7GKn0tnDmfmWQEcSLioVbFWQoGdt7qU47bllzT+ZgFPqAlo0l3CrgUf5PuqG9W0QXt4YQ5yCt/G8cGOuZcOwnlwm/LGWh3lbgve/K5//R6ucOu0fJedOY5HxQkAILD8CIniX35r3fMa9+MVXL3gXS8zfLThRMmg8U0vEHygW8ueHnctsKeFgW5hetKGQuyns2Dj6NTkjvFXzh9dEsbNbiz2qtdCNzLEgqh4Z4BsAOrbmI07Bq5uWOnv9rKqvHXIt64Z1Dwj7EjAzQ+WxOwoFlYqs6y0qzl0PXBwKASGwJAiI4F0SyzhYk+jFL766DATTnmMHOWt72aJkMFcg0nc9J3di2Lklk+P70rAfpKMi4JFiiAtuYW23228+wduYZaBd2/ONi2otUqY1zao8L2Oz59q7RGV3sblWszHMHltpLDyiG9afQHgKwGUfePkGJ39dlOxasdUNHq3EI32FgBBYXgRE8C6v9e6L2Xb7F5+uH7gPhsp/q07+Zs+xF80D2w1IUTEYMTJP14hUdbSgtbqTmDSyHyLiD6uxDHpzycld3o35L+Zjhx1r1s4uuhNBho04WhRrUf8NguZr5tTUhBuF3cXmm0pl12CIp6rZGHZY/5SZvYrBr6usLb5Qcuy3xMEvrM3Gy5UTExPbwo6VfkJACAiBTgmI4O2UoIxvmUDcQqAxIN3IvABEPw1+TrjMK9hvbTnoiAdEwWBNNjuemObb6i+ftiJ4VQGGzdt8lb5sHxAKXsEei3iabZmbI3iZLvHc3JltGQo5qNO1SI5aryAN3wrc1cXbqd1m4etm9g8AH1rfr7b+ejrzaTC9u/rsD55jP7WZvbifzz2bnVgzNXX7nXH7FPtCQAgIgRoBEbzyLnSdQNxCYAfBa2bfA/CnAr3LeFfRtS/q+qQbHHbKYPX4+B7DlfRjc/4MtyJ4U4b1llqVMQKfUHTy1/eai/LfKZtW59CpP93IzIAo0XgkpFO7zeZREbyByp4VvWr9U4b1TiZcWB3veY6damarG89107oLwP7Bn0PNP7Q4ORlUPpQmBISAEOgGARG83aAsPuYQiFsI7CB4jezFIH5bRRvQSZ6bu7bXS9IpA9208gDUjqxfvX0fTKkVwaubltrdPQCgP3pO7rBeM6mK3Z8BeH4tllbm0278nazFfEcZanF0YreVudT7YeB6Ao6vjt/sOfai+ZJb8dNpXz1t/QmszhMHiveJXmHir53alPFCQAgIgbAERPCGJSX9IiPQLSGwXXhkvwvwCcHvWdIOKxYmVIqmnrZOGCSNzK+I6Ag1gWlasXqYt80WlggrEFOG9WYmXKZs+L7/7xumJn/VUyAAkmPjR5Lvz8kQEXY+ncTe7lqkzAOPYZRVRTV1SPZSz7UrH6qqrV27rc5lvkt+KjXZNJVTGwsFtavaF003LSVwgxzPCZ4+wA1REKUvApcghIAQWBIERPAuiWUcrEl0SwjUCY/fAwjOMDbeYu8VuXYZJM3slQQ+VcVN0F5VdCa+1Y6tWtYKBu4qOfbqXnFoEIibAKyq/1k/C966zB/z7qS2sy6trsOImTleAzVkXuBvJnjmDa7rbm3VXpz9ddNS30ao3zkPeI69Z5y+xLYQEAJCoJGACF55J7pOoBtCoH5Ss+miWvzKP04w7TAwLOvJ5Rmoc48E5m96bv41KsZWbdVVAvOHqbxPoVBQZ4F73ubbqey24OXpLStLpZIqfb1o09OZ08B0RfDBg3B0sWDf2Dig1XVp5rPxuWEY42UannNpkQmvKBXsa1q1FXf/lGkdzUCQPo1BZ5Sc3Bfj9in2hYAQEAL1BETwyvvQdQJxC4FuC492ALbDQDethwA8BkDJc2y95nfu7Xc+ZHs82t4YKjNm6L7az3xNO6d2vAM+n+lN5S9pJ/44xvRe8NJGz8kdEGZuumkppo9jwC45tirlvENrZ43D+K71qf8gV/tZNz4gtBJj3TuqsqS8QJ3Cmd7y8O4bN27c0o4dGSMEhIAQaJeACN52ycm4tgnELQSWouDVjeynQazSTPlURqZYtJ35BG/YRSHC94sF+7iw/bvRrxeCd7FLZ/PNeXT0wAPKiZmvENO/q+c++IQNC2S3iPM919OZm8B0ZGOM/Sh4DcNYXabhSgoywte9gn1yN94n8SEEhIAQqCcgglfeh64TiFMIzDeZbvsLA7SVmFSBAU6wysqwksFXlZz86+t9LHBpabEwHvH6oKLaYh9Mas/iFHBzLsk1yc+cNDLPIdDrQdgu1oge9Aq5PRYC3coah3lnan1SpnUZA2+u6Ed8joGzas9WrdB26reCDkkjez4Rn12N8TmeY6tSyNKEgBAQAl0lIIK3q7jFmSIQlxDotvDoZDVbYaCblhIIzwLhgVXD2sjExMQjDYJX/XfY9FObPMfetZPY4xrb7R3euktn2zzH3mmheaWM7OuZ+MsNzxcdE8d7nhwdO5K0xEfr8u7e7zn2XvXciPmcopu/IK41aseublozKjEDgH96jv1v7diQMUJACAiBTgmI4O2UoIxvmUArYq9l4/MM6La/MDGHjUkfy54En4NLSMz8oZKb/2gY+4PYp5uCd40xnk2QP1HhRFd4Tu70+ZilTOtqBl5Ze0bAFEDnFp3cVc0Yh13jpnbS6ScQa2cz6BV1fWd36XXT2qx2/6vP7lu1QlvdL7u8KTN7OYMDtprPh0xN5dc1m688FwJCQAjEQUAEbxxUxeaiBKISAmExd9tfmLjCxpQ0rY0EPJ6Be0qOHVSpWqptPsG7ddOqXe6+e50SdJG2pJk9g8BfCIwyvdZzc9+odxBkFSBcCsZI9efTBDo9jNCt2Qm7xgtNLJk8KEPD028GSB1fGK7285n5I/UffFJm9lQGX8nAJgJ2IcZni679zkiBtWFsZGxM13xtqpLIAtcXHTvIhS1NCAgBIdALAiJ4e0F9mfvsVAi0iq/b/sLEFyampGmdRcBnq/b+w3Psc8PYHtQ+3dzh1U3rFwCeq1hRecVYsfjXQo1b0sgcQUR1hTjoBs/JvbhVrmHWeD6ba9IHpRI883pmPpuI1FGAWrsZRP/PK+RuaRynm5ZKT3bgrCie3mnPUunWf7Yac5T99bS1HoyDAGxO8PQe/ZYXOMq5ii0hIAT6n4AI3v5foyUXYbtCoF0Q3fYXJs4wMemmVVbfBBMwXXTsFWHsDnKfbgnekZR1tJao5IQF6F7Pye1b41Z9pqqnVf9upLd7Tu7idriGWeN6u6Oj1oFljd5G4NcB2H6mmLAOPn3Sc3PfWSiO2i5vkOZW7aj2+CyvPpY9ET4H8RLxB4qF/PntMJQxQkAICIGoCIjgjYqk2AlNoFUhENrwAh277S9MvM1iSmYySSqTV7X1Y8+xjwljd5D7dEPwpsyxsxj0GSDYOfU9x57dQdWN7EkgvlpdsCJghnx+WidnTputcW2tdCPzAhBOA+h49QGnbg0fJaa3Ft3clWHWVTctldt252rfnp7lrbsQuNVz7FpMYaYhfYSAEBACsRAQwRsLVjG6GIGwQiAqivX+wlbSisr3QnaaMUiZmeO5VjKW6INeIXde3DH12n7cgrea3m1DdZ4PUHnoCH9oZlRjPIfBawF6Qu2Z5g9bU1Pr/94Jk8XWeCRtHaf5OI6JnkHg0QY/dwO4zB/ClRts+56wMcxzlveiomu/K+z4qPqFvRAYlT+xIwSEgBAIQ0AEbxhK0idSAvVCoMza+B3uRC5SBw3G6vzd6Tn2mjh9hbXdTPDqpvVxAB9U9ljTjipNTvwkrO1B7Re74DWtaQaGgnoRgCrcMTYPq22aP3xAp2JX2W1cY31s7GD42gsBPhGgQxt9MzClMS6Fv/XKYrH4r3bWsfEsL2aQ8jy7JvLbMdnymGYXAls2KAOEgBAQAhEQEMEbAUQx0RoB3bS2AqicSZ3nhnxr1hbvrZvWnwE8SXkapvLehULh/ijtt2trjrhjvM1z7UtrtqoZAr4ERnC2lKdZL5XypXZ9Dcq4OAWvblo/A/D8Jiw2af5wKgqx2yh4m/j9GYOu33O3lVeuW7duupP1ajzLC8I3vIL92k5stjo2ZVrrGHhyME4bzniT6ydbtSH9hYAQEAJRExDBGzVRsdeUQMqwbmTCURW9i4tKhXi+dtXT1rfBeHkQkM9nelP5S5oG16UOO4o7/hlYux2ElwKc2h7G3EtVXQqvJ27iEry6kTkXRB9YYFK3MvB7jek323aimzZOTDwQ1eTr5hNcJGuwO83M52k+risW8yrDQmStLi+vuvSYAOi9npP7dGQOFjCUSlnP4wR9EmD1AVO1ez3Hnr0QGLd/sS8EhIAQWIyACF55P7pOoP7rejD+5Ln2Dl/vdhrUmnT28ARzNX1Te2mlOo1hsfG6YW0CYVWlD20GuPrv9aO47Dl59RX8smhxCF7DOHC0TGV1ZGY2ywWBvuIT/wDbdro5ztRdumn9A0BQejg4rgC6hZl/44P+vMHN/SWuRd0xrRqYCe8uFeyL4vA5OnrgATOa/14NfOasfeay5y6fdzcOrmJTCAiBaAmI4I2Wp1gLQWDOhSz1jf2Wh3fZuHGjumEeWau7JT7t9WlKr2Q6ez4xn73DpAnXEuOrRcf+cWRABsBQlII3OTZ+JJXLqhzzGwHae7sQw6Wea7+tWzjUBa4V2Hqf67r3dcun8lP3bs3uLhPxrwA6r1iwVQ7ijpuezj4LPp8GQsORCfq85+TO6tiBGBACQkAIREhABG+EMMVUOAINKbcA0p7qFSb+EG50816DdEu8uhtXuZBG9NGEv+1rrutubD7LpdejoURuMEHPsUP/HTU2NvaYbWXtJBBOAfCseQg97Dn2bkuP3Pwz0s3MZoCqJYfrv0XgizV/6FNTU7ff2Q4LtaPrJ/z3gbnxg8NWZj6y5OZvbseujBECQkAIxEkg9C+TOIMQ28uPQK2oQmXm0Z4xlFvig/k+1V24mp2A5vMh22ej7Y2hMmOGZndLfeIUNFoDn3QiOpHB23dz52Lo253+OFer4VuEuWeJCV8i4HtU5ntrMTBjhIb8LTOJxK31KdH2OfjgXXbZ4q9lKh8FxlvqYyZgCqBzWym7HOecxbYQEAJCYD4CInjlvegJgaRpXU3AK6vOb/Yce21UgehG5loQvSyQ0mUeKxbzs2Vjo/IhduIh0JBWqwMnXK4WlwCB3lx0cpd3YGygh1a/RbipriiFSnm2ewST2kagM0ToRkBSTAgBIRA7ARG8sSMWB/MRmLMLCzBPa/uXShN/i4LW7O4x4UGvYAeXhqQNBoH5dnlbiLwEpusB/6cg+iaAxzFglxw724KNJdu1ylYVojiwk0nKjm4n9GSsEBACvSIggrdX5Je537nnbIP8ZJd4bm77Le82+ejpzGlgukINJ8LRxYJ9Y5umZFiPCMx3eW2RUKYJuIyB6z3H/rXqlzKzV/L/b+/+XeOs4ziAf54L/kIXcegilgtNm6hrMxRx6SZYROkkDv4DopsuLmKho0h1dCkuBUVxEYcitIsiuLRXc01joS4OYhE7xPYeyfX6K0bzaULN8+m9OoXyubv39/XJ8CY89zzRvr728yjaVy4Oz322Q0fp5MdOiu/afZ8n1/emY15uonnLX3TTXgYJEOiQgMLboWVMW5T+3MJqRDxw49y9Xu+55Z/OnN6OQ39uYe36Tn/Z2w7iDr92s6fQ/Ve82bmFT9sbl8o08dvK0uCJHT5O2Y/v95/dFQ/+tasdzTxytbl66dLS0i9lDyM4AQJTL6DwTv2vwM4B9PfMvxhN8+X125SOb8y/5Wt5x98c7137JCIOrp2o18TLy0uDz3fudD55qwJbKbyzexdeaCM+ijZ2Tz53Kr+ktlVzryNAgMD9LqDw3u8b7vj5+nPzxyOaVyPiSsT4QQzvrgwH72Vjz84+fXA0077WxPhWVNf/uXY3y9fJubstvP+87rd7DxrpJLRQBAgQmCIBhXeKlt3Fo+7et6/fGzXDG9+onxTWY71rM0c3u0/o7OzsU+3MQxfXnWt1ZTh4qItnlSkncHvh7Y1Wdy0vL//6b6+84xKG6788b6wMz36Y+yRTBAgQIDAtAgrvtGy6w+ec3Tv/Tts2R267tGGcto34ONreFz+fP/P1RvH7c/NXb916yr1AO7ziu4p2x5fWmji+sjRY9ySviP6efYeimTkW0T45efPV3qg9sLx87oe7+jDDBAgQIDAVAgrvVKy5+4fszy2sPVr44UnSdfcJbS83bXNyFPFtRPvjeKbXvN+0cWD8N7229/aF82eOdv+UEmYE+nsW/oxmfHnLtYiYibY90kZ8M9n7oaZtX4po+rfeyyUMGVczBAgQmGYBhXeat9+xs298n9BbDxDYKG4T8cGF4eDNjh1FnG0ITB6UcDL3Fi5hyDmZIkCAwHQLKLzTvf9Onn5SfNeuw3x0k4C/rwwHj3fyEEJtS2DdI3E3eq8rvVH7vEsYtsXsxQQIEJgaAYV3alZd76D9vc8sRoz2xygWo2kWI9r5205xZWU42KwQ1zu0xDcF+nMLf0TEYzf/o21ONzH6Knq9UxeWzp5CRYAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8SanDhw9/17bt/uS4MQIECBAgQIDAPRVomub7EydOLN7TD7lP3lzhTS5S4U1CGSNAgAABAgT+FwGFN8+s8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQU+Bsshf4JxN0mVgAAAABJRU5ErkJggg==', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0');
INSERT INTO `portal_user_db` (`id_user`, `created_date`, `full_name`, `badge_no`, `username`, `password`, `department`, `project_id`, `email`, `sign_id`, `id_role`, `status_user`, `last_update`, `update_by`, `sign_approval`, `warehouse_permission`) VALUES
(1000075, '0000-00-00 00:00:00', 'Direktur Finance', '-', 'dir.finance', '827ccb0eea8a706c4c34a16891f84e7b', 33, 0, 'dir.finance@gmail.com', 'default.png', '3', 1, '2020-02-23 15:01:41', 1000049, 'iVBORw0KGgoAAAANSUhEUgAAArwAAAD7CAYAAACIeEe+AAAgAElEQVR4XuydCZwjVbX/f6fSPTADgg9kdZhOpavSqTTgBrihMs8NBFRAcAdREVwQd0Hf+7uyuIHKIvIUXFFEcEHB7SlP1OdTR3GgU+lUdSoDA4ggKDAzzHSnzv9zK0lPOtPdqSRVWbrP/XxQ6Lr3nHO/t2b6l5t7zyFIEwJCYKAIjI+Pr9i8zd8CQJsncPYce76fD9QcOw12jTGeTZB/K4DhTm0tOp6x2XPtXWL1scSM66Z1N4D9APj+EFZvsO17WpliKp09HL5/OBMdDkD9s3swnrGZiB9g0OqqvYc9x96tFdvSVwgIgaVLgJbu1GRmQmBpEtBNa9uiQo60p3qFiT8szdmHm5VuWlsBrJin9yYAdwC4G4RHwPwIAY8w4xEGHtGIVjJoJYhXMdNKjXkVE60EWIlaJZ7UP/82K7JAV3hO7vRwUUmvpJH5LRE9Q5GYpvLqjYXCXZ1QWT0+vseKrXwUk38UQEcB2KNqr6z+33PsoU7sy1ghIASWDgERvEtnLWUmy4CAns6cBqYrFp8qvddzcp9eBjjmnaJuWr8G8Kxgz4/xHQA/90F/nhny77grn/9Hp1ySZvYMAn8hsMP0Ws/NfaNTm8thvG5kPw3id1ew0QWlQu6cKOedTqcfNw3tOIA+AsZ+DEyVHNuI0ofYEgJCYHAJiOAd3LWTyJchgaRp3U/AngzYJcfO1hAkTetqAl5Z/e+fe479gmWIB7qRPRHESuSCwK8uOvmro+agG9Y1IJwU+CjTSLGYUzvG0hYhkEynM8QJOxC7wI0lxz46LmC6af0CwHMB3Ow59tq4/IhdISAEBouACN7BWi+JdhkTGEmPv1Vj/xKFQCMcN1Wwv79d8NbtOgI8veXhXTZu3KjO+S6rppvWXwEcDMKDXsGufb0dKYP64xKeY8vfoSHo6qb1r+pxkH96jq2OhMTSUunxQ5n9ynEewslewf56LI7EqBAQAgNHQP6yHrglk4DjIJBKW8/TfNrFdXM/iMN+FDZ105oGoM4k7nAZp3pJa6LmZzkKsfpdRIC/6zn5E6Pg3mhDNy1ezpxbZaqb1o8AqB1dn6E9peRMqMuEsbSUaX2BgTOYuTyzU2LvjRMTD8TiSIwKASEwcARE8A7ckknAURIYHT3wAD/hvw/Mb1O/kLduWvWYu+9etzlKH1HYqj+7S4SjiwX7RhFicwnMOVtLfLpXyDc569zeyojgDc8taY6/kuAHx0oY/MWSkz8j/OjWeu5z8MG7rNoy/TcAuzLwvZJjH9+aBektBITAUiYggncpr67MbUECI6PjazXyX6e+9pzTiXCKV7C/1m/o9LR1P3jHs7v1cS53IVa3kwhofsabnJyMYx2XO+dWmOqm5QFIArjHc+z9Wxnbat+UkT2Hic8LxjGd5Lm5a1u1If2FgBBYugRE8C7dtZWZLUBgZHRsraZpv5zvMTP/suTm1YWXvmm6mX0PwJ9SAS20u6ueLWchlkw+8bE0vFXld10J4FbPsZ8U1wIuZ86tMNVN6z8AfCx4b5neUHRzV7YyvpW+hmHsVaZhlc83AWCr59g7tzJe+goBIbD0CYjgXfprLDOsIzCSso7WErghuNJS+Z8pAH9l4HiAywAloPlP8CYn1/cDuCDVEifUL3J1dneb59g7LRTXchZiSTPzOgJdFWzuMX+k5OY/HNf61XG+33PsveLyM8h2q8cLVPaKPYjoV8VC7t/jnE/SzJ5O4MsDH6Sd6RUmgsud0oSAEBACNQIieOVdWDYE9HTmTWC6LNgFYi4TaacVndxVhmGsLtPwndtB9E8xgblnd/ndxUL+wjCCd/PK4V3vXb9eFVlYFi1lWDcyQRUeUIJ3bcnN3xzHxJNG5ggl4Cq26e2ek7s4Dj+DblM3MzcAdAzA/+LpnZOl0q3/jHNOumn9GcCTAPpngrft67quKjwiTQgIASEwS0AEr7wMy4JAyrTOYuCzFZ2CB7TysDU1tf7vtcnraes7YNRu9ffN5bVaEQUCpv0Ep0v5fCmE4H3Ic+xKudVl0Kq74PdVp/qo59jqWEMsTTesr1bPfW/jBI8tth6xBDAARpPpzCnE9BUVqsZ03pSb+2CcYdenImP2P1tyJ98Zpz+xLQSEwGASEME7mOsmUbdAIGVaV/P2ogxbNX94Tb3YVaZSpnU0Ayp9UtD6Ia1XZed5qBQcswB+5Dn2sQtNe25asv7ZoW5hmdrumjKyr2fiLwcGGKd5rv2lto0tMlAfGxuDr92myjoT6Lqik3tZHH4G2eacI0Mx5kKuZ6SnMzeB6UgAm/whmBtsWx0BkiYEhIAQmENABK+8EEuWwOjo+Fpf89VFGXVLXLVtmj98QKPYrQHotzOweto6GYyvqvgWu6ymni/ncreptPVbZjwDoDs9J7cmrhc6ZVrXM3BcmPWIK4Z+tqvEbmII32duft48qnmMjmae4mv0p8pnHb6i5ORPj8q22BECQmBpERDBu7TWU2ZTJTCaHj/FZz/4WrXS6AbPyb14MUB9J3hnE/az5zn51KKx15e7TawYK+b/WlgOL0MqkzmIyxRcMGSmD5fc3EfimHcqZZmcQF59Sw/gL55jPzkOP4NqM5my3kwJvrj6bcQDmj/3yFBc85rd3WVs9odhyO5uXKTFrhAYfAIieAd/DWUGDQRSRvbDTPyhOrEb6nJRPwneNel0KsGao66ch8k6oJtWORBjzI96bj62M6z99rKlzOzlDD49zBnnTmJPpq0LiPH+wIbkeJ2D0jAyTy8T/a7CBg9qPJxZ6FuUTtagcazs7kZJU2wJgaVPQATv0l/jZTVD3bTUBZmPVybNZc3HU6em8uvCQKgXvFs3rdqllxXXRszMpzTQeyo7l4tnHUhmMkkqk0rwr3ayr/OWydnSVCq1Oyd22gBgdzD9wHNzLw2zzu30qSvrLDleGwDqpjVTzX+76JGhdrgvNqbu7O4jPK2ZpdKEqrImTQgIASEwLwERvPJiLBkCKcN6MxNU2jHVNiV4OOO66zeGnWA/7fDWCaymGRdSZuZ4Bl1X0bv0Qa+Qq1SbWuJNNzNnAvT5yrTnL7ccBYKRtHWcxrg+sMU42XPtr0dhdynY2J4ODOwjMb7Bud3uxrzqd3fBfInn5s/shl/xIQSEwOASEME7uGsnkdcR0I2xk0DaNdUfPew59m6tAuoXwasb2RNB/J2KwNJe4rkTP1xsLrppqR3tIPUTa9pRpcmJn7Q690Hsr5uWOqdsApT3nJwV1xxql9UIuL8ohSZmMafMzHcZdELwA5/P9KbyXSv2UHd2958JTI+4rvtQXOsvdoWAEFgaBETwLo11XNazSBrW04jwv1UIWzzHXtUOkH4RvLNZBxiO59rpZnPRTev/ABwW9JtB0vNs9TX/km7JsexLyOfvByIfdFbJyQU7vVG30dEDD/C1sgtgBRhf81z7lKh9DKK9Nens4QnmW1TsRPh+sWAH2Su60eac3WU6v+TmPtANv+JDCAiBwSYggnew12/ZR7///k9ZtdMum2sVxRYtvdsMVj8I3tTY2CHsa38MYg1RIrWa97SSP5job14ht1+zeS6F57ppqUpqzwHwiOYPj8Z1SSppWmdRrWCJXFabfXXqzu1u8hx7126+U9vP7vLf99htl9Xr1q2b7qZ/8SUEhMBgEhDBO5jrJlED0I3xY0F+7et+33NsVaCh7dYXgte0vsXAK0B48JFhbc19ExOPLDSh6pllVdpWzbvj+bcNrssD9fT4YWBf7WqDwF8pOvlT4wpBN60cAHVc4uHy1qGRO+647cG4fA2KXd20fgHguQDKCZ5Ouq4b+px8p3Occ3ZXw//zJu2PdWpTxgsBIbA8CIjgXR7rvORmqRvWa0H4WnVi//Ic+7GdTrLXgrf+63MGf77k5M9aUOymsms4wZWjC/OUSu6URT+PT5nZqxj8OhWjz/yMDW6+dpwl0rCTRuYIIvpVxSh/03Pyr4nUwQAa0w3rO6CgBDeDcUq3L/Clah8IgW3TVE5tLBTuGkCMErIQEAI9ICCCtwfQxWVnBHTT+k8AH61aedRz7EjyzvZa8CaNzIeJKMgfzL5/VGlqcsHLZ7ppbVMlbtUum+YP7x/XV/qdrVT0o6sfCiYBrATznzw3f2j0XioW9bT1JTDeUNG7zS8PxhVHv9hNGZlLmOit1Q9ZX/YK9hu7GdvjM5k9V5Rxb1DcgvmHnpt/STf9iy8hIAQGm4AI3sFev2UXfcq0zuLamUqg7Dn2UFQQep2Hd7Z4BHiL5+QXvHi3xhjPJsifqAgxfN1z7ZOjYtDvdnQz+x6AP1URXXy6V8hfEVfMupGZAZE6LhLZh6q4Yo3bbjKTOYLK1d1u5nWemz8kbp+N9nUjcy6IggtqPuH4DQX7e92OQfwJASEwuARE8A7u2i27yFNm9lQGX1md+LYya0+6w51QZywjab3c4Z0jYkFXeE7u9IUmlTSzZxD4CxXBS6/13Nw3IgEwAEZSpjXBQBbgLZgZ0j3v9nvjCHsknT1KY76xoqvx1qJr1/I7x+Gu723WfaPQM/Ffl5t62nPsFX0PTQIUAkKgrwiI4O2r5ZBgFiJQf0GNgBl/estjSqXSo1ES66XgbUXEpkxrHQNPDuauDWe8yfXqK/4l3+ZeVqPrijFWlEuZ2SsZfCoDm4Z4Wndd974lD3iBCdYVl/CHqbxPoVC4v9ss6j+AAPxpz8m/t9sxiD8hIAQGm4AI3sFev2URfSqVfS4n+OfB9SzwfZ6T3zuOifdS8OqGdQ0IJwU7imUaKRZzdzTOMTk6fiRp/ocBPLX67F7PsfeNg0U/2kylrUuZ8RYVG2v00tJk7gdxxJlMJnem4ZUq88CeYFzjufYr4vAzCDZ10/oKgEru4S4Xl6jnkzKtLzBwRvDng/D8YsFWmSKkCQEhIARCExDBGxqVdOwFgdXpJzx+mLepbASxn6XsqeA1LbVbvZNi7Dn2Dn8uV68e32N4pf+P2TVgLntuPrLzy71Y21Z91n2l3VG+5WZ+U+b4qxj+N4N+zC/03PzPmo1Zis+TaesVxPhWVWR2tbhEI8+kaW0k4PEAHvAce8+lyFvmJASEQLwERPDGy1esd0hAN62iujCvshHwtLa6VJr4W4cmFxzeY8HLtcDmE7y6aW0BsHOlD33ec3ILpiyLi08v7c6trsUfKbl5tdMdS9NN69cAnoWQle5iCaLHRlePj+8xvM2/J6gwB8x4jq0ygvSkzcm9S/QTr5A7qieBiFMhIAQGmoAI3oFevqUdvG5aamft+UrsQqNXeZO578Q5434VvKm09U5mXBhIXeIPFAv58+Pk0I+29XTmTWD6oopN07TDpyYnfhtHnKOj1oG+htuWM2s195SRfTUTB5chmeP9gNFsHevXvtexNItVngsBIdC/BETw9u/aLOvIdMO6FFQ5r0nAJ4uO/f64gfQyLdliYls3rbsB7MeE35UK9jPj5tCP9pOm9WUCXq9iK+88tMcdt8VT8Uw3sheD+G2B0KOyVSoU8v3II+6Y6ot7MPPakptXpZx70nQz+0WA31QV3z2NpScAxKkQEAKREBDBGwlGMRIlAT2dPgyc+H0lIxRuLjn22ijtL2SrH3d4U4b1FiZcGgh/zT+0ODn5p26w6DcfupmdAjgFxp2ea6+JI77Vq1evHF75mDvVZTUC/rvo2M+Lw88g2NRNSx0d2geMv3muvV8vY9ZN6y8AnqhimO+4Ty9jE99CQAgMDgERvIOzVssmUt20tlbPDm7xHHvBAgxRA+lHwaublsrWcMBy3t1Np9OPm+ZENS0Y/9Bz4qmwNSfPM+EUr2DXSldH/ar1tb36ksoE+krRyZ3aq4BHLGs/bQbqGw4w8PuSYz+9V7GIXyEgBAabgAjewV6/JRf97IUhdVbT18ypqQm3W5PsN8GbMjPHMOiG6vxf4Dm2Ss227Foybb2UGEFVLWI+p+jmL4gDgm5mbgXoCQD9c4VWXjM5OflwHH763WbSGLuASAuOEBG0Vxediat7FXP9nwEmuqBUyJ3Tq1jErxAQAoNNQATvYK/fkoo+OTZ+JPn+TZVftPhc0bHf0c0J9pvgna1uxdjguXaymyz6yVfKtC5iIHgX4jpPqpvWswH8T3XeX/Uc+3X9xKCbsdT9Objfc+y9uum70VfSyHyYiD4U/JzpJM/NXdvLeMS3EBACg0tABO/grt2Si7zuF21Pypf2k+BNmtbvCAi+vtV8PmRqKr9uyS14yAklDWsdUaWynD+E/TfYtkqXFWlLmZnLGVQp57yMc++mzOzlDA449EOBB93M/gTgF1b+HHT3G59IXzAxJgSEQM8JiODt+RJIADUCNcHZq4sp/SJ4QbgGjJcHoqNLGSr6+S3UTaus9A6Ahz3H3i2OWHXTUmeEHwfgTs+J51JcHHFHaXNkbEzXfG2qonVxfdGxT4jSfju2dNN6EMBjGWSXnFy2HRsyRggIASFQ/X0qIISAEFAE+kbwzi4H/8hz8scu59XR09lngVkVgogtH2wqbb2IGT+u7O7SZzw3957lyFw3MzZAGQCbEzy9h+u66vJoz5pujL8Y5Aflo4nwiWLBPrtnwYhjISAEBp6A7PAO/BLKBKIi0E+Cl4G/lBw7+Bp/OTc9nf0AmM9VDMqsjd/hTuSi5qGb1lcAnKLs+szP2ODm/zdqH/1uLzWWfTX7lUITAL/Xc/Kf7nXMSdP6FgGvqAre5xcL9i96HZP4FwJCYHAJiOAd3LWTyCMm0EeCtydnmCPGGYk53cj8BkTPZKBUcmxVYjryljKtaQaGAGz1HLtavjlyN31tUDesTSCoFIA9LSNcD6nuKEtX0xP29UJJcEJACLRNQARv2+hk4FIj0C+CVy7nVN4swzBWl2moBFAirpKyo2Njz/R97TfBLiLw7aJjv3KpvdfN5jM6Om74mu9U+93sdanQy2JxjZrW+3zgE0Efxsmea3+92TzkuRAQAkJgMQIieOX9EAJVAr0qLZwyrEu5WkZZhdKrS3v99iLoaetkML5a0Tz0tJKT+7+oY0ya1vuoJqyIz/QK+Uui9tHv9pJG5oVE9JOK6O9toYkaKz1t3QPGvgAVPSc32u8MJT4hIAT6n4AI3v5fI4mwSwS6vcObNMZfCOLzCfyk+imK4K3QSJnZqxgc5MOdXqHtuXFi4oGoXwXdzPwAoBcruz7Tkze4OVXGdlk13bDeAMKXgg8WzB8pufkP9xJAKm1dwIyg8IUGPnXKyasz1tKEgBAQAh0REMHbET4ZvJQIdFPw1pdvbWQogrdCRDetLQB2BrjsOXl1xjbyppvWPwDsAeYtnpvvWhnryCfSgcH64g4MPrXUQ4G5enx8j+FtvgdgNxDWeQX7kA6mJkOFgBAQArMERPDKyyAEqgS6JXhHUtbRWgKqZHD1zx99HuC31xZCBG9wfne3Mg3/SzFhYKLk2AdG/aIaxoGjZSpXS1fTTz0nd2TUPgbBXv1OelyV7MJySJrW1QS8kggzPmuHlpyJW8OOlX5CQAgIgcUIiOCV90MIdFHw6unMm8B0GYAEmMsMPK/k5m/ultgelMVeY4xnE+RPBPEy/dZzc4dHHbtuZF8D4tplqP/0HPvjUfsYBHt6OnsLmAO+cVWyC8NhzrcejC97rv3GMOOkjxAQAkIgDAERvGEoSZ9lQSBu0ZkyrbMY+GwAk/CAVh62pqbW/139Z9y+B20BR8fGn+n7fpA9AcCk59iqIEKkTU9bl4LxlkBTM69VHzwidTAgxurSfz3kOfbuvQpbN607AByg7m16jp3qVRziVwgIgaVJQATv0lxXmVUbBOIUnSnTupqBWsqrrZo/vKYmdkXw7rhYc3Z4gfs9x96rjSVddEjStCYIyALse05+haptEbWPfrc3YmSerhH9rvIhjD7oFXLn9SLmpGl9goD3Bb4Zp3muHVyikyYEhIAQiIqACN6oSIqdgScQh+AdHR0/0k/4l4MxUgW0TfOHD6gXuyJ4d3x1RixrP20Gd9eeRH2uuXo5Sl1YAxH9sljIPXfgX+A2JpAyrHcy4cKK4NWe6hUm/tCGmY6GGIaxV5mG7wmO+QBSdKUjmjJYCAiBhQiI4JV3QwhUCUSZh3d09MADZhL++zTmt20HTDd4Ti5IgdXYovS9FBY0mUzuTMMrVZaGoEUteHUjcyyIflgRer3b2ez1WqVMax0DTybgz0XHfkov4kkZ1luYcGmwFIyzi65dKTghTQgIASEQIQERvBHCFFODTSCKHd6kkXkOgV4PwslzadDbPSd38UKEovA92PQX/xAQteCtz/VKhOcXC/Yv+omfns4+Cz7to3FZpeiqNloD+P+cmpr8VRSxjhjZJ2nEf64KzXcVXfuiKOy2akM3rNtAOJCZtwxhZsR13ftatSH9hYAQEALNCIjgbUZIni8bAp2KzpSRfT0Tf7kB2FbN52dOTeXXLQayU99LcZHiZKKbmVsBegJAmxO8bT/XdR/qF4a6Yb0VBPXhaL6/n9lzbC2KWHXT+iSA9wa2ND/jTU5ORmG3FRsjo5lnaBr9Vo1h4Hslxz6+lfHSVwgIASEQloAI3rCkpN+SJ9CJwGq4lKaUyhRA5xad3FVhwHXiO4z9QewTF5M16XQqwYmpKpP/9hz7eWH4GMb4i2dQ/lfJzf9PmP6t9hkdzR5V1vhcAuZU3tvBTkRnbXXTUmek9wNwq+fYi/tsdTIh+yfT1oXEeKfq7hOO31CwvxdyqHQTAkJACLREQARvS7ik81Im0I7ASqWtFzFwWd2ltGkCnR5W6NZ4tuN7Ka+FmltcTKq5kL+ofBDzOUU3f0EzlqtXr145vPIxm4JdV4YD4GYCF3kIf/by+Z81G9/seeOZ5cX703s9J/fpZjYXe64b2RNB/B3Vp5flhJOmtZGAxwN8n/f4fffHzTfPdDIvGSsEhIAQWIiACF55N4RAlUCrAitlZk9l8JXbAS58Ka0Z5FZ9N7O3FJ7HxUQ3sz8E+NhA8BI9q1jI1fL9LoitLlctNx41IOYPFd38RzthrpvWNIBq+WT6vOfkzqq3V6tAVv3Zzz3HfkFn/jLfAOjVVcF7RFy71ovFGHxYZPw46COFJjpZThkrBIRACAIieENAki7Lg0ArAqvxCAOw+KW0ZgRb8d3M1lJ5HheT7eKVt3pOfudmvBpyAtd3rxO//Fcw/tNz86pkdEutwf6PPMcOxPhcwZs9g8BfqP6Mp7c8vMvGjRtns1i05LCye+5XhfsWz7FXtTo+iv66aX0FwCnKlk/0og2F3E1R2BUbQkAICIH5CIjglfdCCFQJhBFY8+fV5Wc0u5TWDHIY381sLLXncTCpv1jIwDtKjv25ZtyS5naxyaCrAN6ZgJcBGA7GEh4E49+qdn6s+dr5U1MTwUWsMK3ePphe67m5bzSOaxTdnWStSGYySSpTNfsDXec5OTWXrreUaU1zZVd7m+fYO3U9AHEoBITAsiIggndZLbdMdjECiwmslGkdzeBTATphu432jzA0xhGHuBv01Y6DiW5mbYAzzFxO8JA+NXX7nc046UbmWhAFopDKPFYs5gvq31PpzDnM+BhAqmCCD2Z1RqKSQYGh+vwPE1xo2vrS5MRP5vOj8jX7ifIvwEhX7K8YKxb/GtiP6x1JmZnjGXRdYL9HOYiDtGvMv66gwvtLjq0yRkgTAkJACMRGQARvbGjF8KARaCz+sNOuW57CzCcQ6FiAU3Pn09kRhrjEzKAxXyzeuvXY7Dn2Lp3OLWVmjmFQcOSAgW+XHLtW6nlR07NHIAgPegV7j/rOI2NjuuaTEr3BediK6eCfOanDfN9/7oapyV/WxqZS2ecigdcw+HXb7dG9npPbd6FgovoAoJvWxwF8MAhW045aSIx3ynvRtU1nPwDmc4M+PUqJFuf8xLYQEAL9R0AEb/+tiUTUIwL1gkKlSmLCfIn4Q+XVbXUKUYmZVv32a/9qudm/V+O72XPstZ3GqpvWXwEcrL5CpzIOLBZtlW1hcbGbzpwGpitUJyIcXSzYN843oFq5Te2aVo45bG+Vc76EB/0yH7NhKv+7VCq1Oyd2+md9JwJmio7dOHaOoajeEd20fg4gSMVGZRopFnN3NOMQ9XPdtP4C4Ikg3OYVbLUm0oSAEBACsRIQwRsrXjE+SATqBcUOcROuJcZXi45duVUecYtKzEQcVs/MJY3MEUQUVBRj4Fslx35VJ8HM3d3lq0pO/vVh7Olm5u8A7cWAXXLsbLMx6qytRvxsgJ9NgDr+sgLAAwDUzvBDBP8dDE0J6CAjQyv5mqN6R2Z3rBkPeu7cHetm84vieSqdPZyZbwnWlnBRqWC/Kwq7YkMICAEhsBgBEbzyfixrAiPp7FEaYxzMaRBOa4AxDaIPJ/xtX3Ndd2OcoKISM3HG2E3bKTPzSgZdHfhk+ozn5t7Tif/Z3V3CA6uGtZGJiYlHmtlLmtnTCXx5IEwX2d1dyM7o6MF7+9rMjwA+tE70znYn4LNFxw6KLoRpUbwjesgd6zDxtNsnaVpfJiD4wCHZGdqlKOOEgBBolYAI3laJSf8lQyCZzCRpuHZbvXFaO+ZCjXPiUYiZOOPrtm09nXk3mILiCsR4V9G15zteEiqsObu7zB8qhcyZq5vWZgArlZN2syI0iN5AvlemFG7HuH6CUbwjSdO6n4A92/EfCnaITrqRmQEFF/0kO0MIXtJFCAiBaAiI4I2Go1gZQAJ1xQR2iL5dgdMuhijETLu++3FcfclZMJ3kublr242ztrvLwD0lx94/rJ2o1qQieqfvrffbzo5xp/HM2bEGn1B08teHZRFVvzkV3iQ7Q1RYxY4QEAIhCIjgDQFJuiw9ArppqfOhR1RmRjcwcGNdYv+2d/TaJdWpmGnXb7+O09PWd8A4MVgd0g4rFib+2E6so2nrOJ8RCDsmvKtUCL9THPWadJp1otN46qq5Peo5drBz3e2mm5Yqw/x85bdM5dE7CoVit2MQf0JACCxPAiJ4l+e6L+tZjxiZd2lEnwkgMC71XAhPky0AACAASURBVPtt6l87FRSdQO2l707ijmusblq/B/BUZV/zh/eZmlpfy9jQksukmXUJPArgTs+x17QyOOo1qdlr99uDTuKZW7iCrvCc3OmtsIii75wYmH/iufmjorArNoSAEBACYQiI4A1DSfosGQIjKetoLQGVi1W9+w95jr17bXKNeXjvvnudOsPZldaJmOlKgF12UleFq+3ddt3MvgfgTwWhE5/uFfJBerGwrd/WpD6ezSuHd713/fpNYecSpppbWFvt9tNN62MA/iMYz9qJnjvx3XZtyTghIASEQKsERPC2Skz6DywBJXYTQ/g+M4bAXPbcfJAaaj7B2+4uXLtw+k1ctTuPqMZ1yqOSx3fonkoVNN7iOflVrcbWaQyt+mvWvy6eOR/Umo1Tz3XDugaEk4K+M37G8yYnw4yLsI+mm5bKdLIfA3eVHHt1hLbFlBAQAkKgKQERvE0RSYelQCBlWG9mwsUA1O3wBzR/2Gr8mryXAqeXvvtxfTvlUX9Bi8FnlJz8F1udZ6cxtOpvsf6dHkmou6AZSdW6VufWkGbuPM/NBZXepAkBISAEukVABG+3SIufnhFIpTIHcYLWVwOYV+wGu2CmpVJGBU12eHu2XIHjTtciZVrrGHgyCBu8gp1sZzadxtCOz4XGdHIkoVOxHMU8Uunsb5j5mequmuZrmampCTcKu2JDCAgBIRCWgAjesKSk38ASqLudPq35w6sXugDVS4HTS9/9uLCd8Eilxw9l9v8QzMvn93pT+SCfb6utkxha9dWsfydHEjoRy83iCvN8dDTzFF+jP6m+xLip6NovCjNO+ggBISAEoiQggjdKmmKr7wikzOzlDA5upBPh+cWC/YuFguylwOml775btLk7vC2n0EqamcsJdDqYt/BMIlUqTfytnTnWr4nv8zM3TOV/146dKMZ0ciRBNzLXguhlwZ+BMo8Vi/lCFDGFtaGbmc8B9PbAP3BMXOW5w8Yj/YSAEFieBETwLs91XxazHhkb0zVfm6poXVxfdOwTFpt4L0VnL33328tQuXA2XEtDdrPn2GvDxphMJh9Lwys9AI8F07Wem6tc1Gqj1a8JCA/6ZT6mF6K30yMJs2KZ8KBXsPdoA0XbQ5LJJz6Whrc+WDXQ8oeXth3LQCEgBIRAAwERvPJKLFkCetpaD8ZBADYneHoP13W3DoLg3brbql3uXte9lGj99gIkjcwRRKQKg6gtwW94Bfu1YWNMmpnTCXS56s/MR5bc/E/Djm3sp5vWIwB2UZccAewB8EPs0+mlKfvb7dpsZ1wnRxL0dOY0MAXp2Nqp7tZOvPVjkmnrpcT4XmUttTO9wsQlndqU8UJACAiBdgiI4G2HmozpewJJw3ozES4Lfs8yn1N08xc0C7qXu6x1vh/xHPsxzWJdys91I/saEH89EK1EF5QKuXPCzrdWRhgMx3PtdNhx8/WrlASe+RHAh4LxIAj/FoTE9Maim7uyE9utjK0vwtFqSjHdzPwdoL0YsEuOnW3FbxR9k6Z1PQHHAfz3VSsSB0xMTGyLwq7YEAJCQAi0SkAEb6vEpP9AENBNa6aagix0ztJeCd5kJpOkMqmv4ZU8v85zcsF5y+XaUkbmbCY6v4KDz/QK+VC7gkkj80Ii+kmV2/s8x64UneigzRG9qi5fpWCJ+p/PFh37nR2YDjU0ZVpXM/DKaud7PcfeN9RAAPWp2Xqxu3uAceDoEJVtAMMEfLLo2O8PG7v0EwJCQAhETUAEb9RExV7PCeim9WcAT1K7ccNU3rtQKNwfJqheCd6UmTmeQddVBB590CvkzgsT71LtoxvZi0EclHsm8MuKTr7CpknTTetmAM8BsI0TPFbK50vNxoR5XhG908Xq8YbtQwjrfNLet2Fy4pdh7LTSJ2UeeAxT+RIwRqrjyp5jzymU0syeblqqUuBK1a/bafaUz6RhXUgE9aGgXKZy+o5CQTGUJgSEgBDoCQERvD3BLk7jIqCb2e8CXLmc5vOZ3lS43UHVvVeCVzetjwMIEvGzph1Vmpyo7VLGhamv7davIZF2WLEw8cdmAafGxg5hX6v0Y/qh5+Ze0mxMq891M/tFgN9UHfcvAJWy1AyV9eB/QHSb5vOdgPqn1mgN4P9zamqyciY5REulMmlOUF0lNLrBc3IvDjF0Tpdevc8qCMMwdqpePNwNwM89x35Bq/FLfyEgBIRAlARE8EZJU2z1lMCadPbwBPMtKggifL9YsI9rJaBeCQTdtH4O4HkqVs0f3mehPMGtzGWQ+9afWd28cnjXe9ev39RsPrphfRWEk1U/n3D8hoJduSgVcdPTmTeBqVa1TR1xUP9oTdz4mr9tv6mpqVrmiUW71x3HUW/y2z0npyoEttx69T6rQFNG9hwmrnxTwXSS5+aubXkCMkAICAEhECEBEbwRwhRTvSVQEwoMbCo59q6tRtMrgaCblrrIMwxgq+fYO7ca91LrX1coJNRX8avHx/cY3ub/o8oh9kt/1UIK/1tds2b4K+d+Q2abqD+OQ2V6frGY++9mDhZ63qv3OZ1OP26aEyr3sSrjLe90uwso44SAEIiUgAjeSHGKsV4R0M3MrQA9AcBMeevQ3nfccVst92fokHohEObmWMWPPcc+JnTAS7Rjq+uQMrKvZuJvVHcTX+u5ucq/x9zU2mnEzwb42QSoYzQrFnBZDsQf06KxpczM1QyqXFBr8TjOfH5b5RgVrvpUaAD+03NsdWRHmhAQAkKgpwRE8PYUvziPgkDStH5MgCpXOu1r/gs3TIY/L1nvvxcCoT4FF4FfXXTyV0fBZJBttLoOqXT2l8y8lsB3FZ386n6Z+5x8wpWg7tu8clhvPKIxOnrgAX6i/GkwqkUy2juz2zjveo5bN63a5e67u5PbWTesP4JwCKk/jwlOR3V5sF/WVeIQAkJgMAmI4B3MdZOoqwT0dPYzYH6X+k+VyqpUyH2gXTh1AmGL59ir2rXTyriUaX2BgTMAxP5VfCtx9bJvK4K3/rIaM19QcvOhc/Z2Y47JdPZ8Yj4bwD9B+FF9EY3R0fG15YT/OuLK2WPVCHikGFEe5lY4RsUilR4/lNn/Q9XejzzHPjYq22JHCAgBIdAJARG8ndCTsT0lMGJZT9Zm8KdqbtRfeI79/HYDSiaTO9Pwyi3V8b/3HPvp7dpqZZyetkqV1FP0B8/JPbWVsUu1bytCLWVa32LgFQA2+UMwN9j2Pf3GpXouV51HfnIttpHRsbWapjWmM4v0vGsrHKNiljIzlzPo9EC8E44uFuwbo7ItdoSAEBACnRAQwdsJPRnbUwK6aanb+2ondpPXxiW1+uBHLGs/bQZ3V3fZvl107Fqy/9jmWL38pAS7kgdXeE4uEAr92lJp63lUpuGpqdxNccYYVqgFRwG0sqvOzjL4ipKT72t+s2I3ZR2tJXBDXRGLKYDOLTq5q6LkGpZjVD5Xr169cnjlYzZWyjBT3nNyVlS2xY4QEAJCoFMCIng7JSjje0JANy2V1/QIdb1H8/GEqSn79k4Cqb88RkQXFFsoZ9uu35RhvYMJFwVyl+k1RTf3zXZtxTHOMLJPmgG/gIBREJ4NYEz52Urlve4OWcyjnbjCCrWUaX2CgfcFPphf6Ln5n7Xjr5tjRlLW0YkhfJ8ZqohEmUCnRS10a/MJyzGq+c8pCQ06o+TkaunbonIhdoSAEBACbRMQwds2OhnYKwK6mfkUQO+pCp3zPDcfFG3opI2OjT/T9/3fBNoJ9OaSk7u8E3thxqYM63tMeGkgeBM8VsznVQGDvmh62joZjK/OE8zDnmOrYgKxtbBCrdaPgIeLMccUxWRTqewaDPFUVew+oPnDVpw5l8NyjGJuyoZuWioH9uEA37dqRWL1xMSESrcnTQgIASHQFwRE8PbFMkgQYQnoY2MHw9fUpZidCPhz0bGfEnbsYv10w3ojCP8ViM8unT3UzcyMkrrKZy9Kvy7EQ09b/wXGG+d/Hv/RizBCLWVaRzPwIxUjM95Zcu3PRvEexGlDN7N/APhQALGL3aoAVTmAgxb3+zU6ah3oa7gt+PMDfLLo2O+Pk6XYFgJCQAi0SkAEb6vEpH9PCSRNax0B6vLP5gQPj7nuenVmsOOmG9mLQfy2wBBpT/QKE3/t2GgTA2GEXdwx1Ns3DGOvMg0rEXlY9efbGLguOHABflXwsya5ZKOINwwX3bR+CkCVq52e3vLw7hs3bqxdOIwihFhsVASvqqY3dEycO7u14MNwjGqiSTNzOVUuq5V9zTc3TE56UdkWO0JACAiBKAiI4I2CotjoCoFU2noRM34c6C7mD5Xc/Eejcrz961iAyjRSLObuiMr2Qna6KUiazSU5mjkFGn2GgD0DvsBnS479TvXvumF9H4SXBJ8FusCmGRfDMFaXafjOyocTfN0r2LNpvZrNczk9b8YxKhbVymp3VQpv8E89J39kVLbFjhAQAkIgKgIieKMiKXZiJ6Cb1kMAHsPAXSXHjrTAgG5afvXWfNfy4XZLkDRbmBFz/FUa/O0X5kg73StMXFEbN1vql/lfnpt/bDN7nT5vxkU3Mp8BUSX3MuPpJdf+fac+l+L4ZhyjmnPKzF7F4NdVPn/wCUUnf31UtsWOEBACQiAqAiJ4oyIpdmIlkDSyZxPx+cqJ5vMhU1P5dVE5nFveN/4zqnVCcvaM5eaVw7s2VuCKan6L2VFZA+pTZJU1//A7Jid/WxuTSmfOYabzgv9m7SWeO/HDuONaTKgdMDa2/7CvbWAEWQ4izVsb97y6bb8bgjeZHN+Xhv1a7uN/eY4d+weibnMUf0JACCwNAiJ4l8Y6LulZjI+P77p5m78hyO/J9FPPzUX6lWnSzJ5B4C9URB291nNz3+gG0DpB8pDn2Lt3w2e9j/oUWQTMbFuh7bNxYuKBWh/DMHYr07A6i7kHGI7n2uluxLiYUNPNzOcBOlPFQdBeXXQmln0p5oXWpBuCtz41HDHeWnTty7rxjogPISAEhECrBETwtkpM+nedQNLMfI5Abw9ETtk/tFicrBZriCYU3bCuAeGkQO9OD1ml0m35aCwvbKVXu8q1iBrywc6bNSBpWBcSITjHS6A3F7uQqk35Wkio6fqB+2CoXAKws0p95Tn5veNep0G2H7fgrVYnVGepHwfA8ZzufCAa5DWR2IWAEOgdARG8vWMvnkMQWD0+vsfwtnIRoN2J+ZdFN//cEMNa6qKbVlmdlACwxXNsVbkt9tarXWU1sdHR8Wf6mq9ypqo///OKXcMwdirT8N8B7EaAW3RsM3YoVQcLCbU5ZWs1ek1xsr8KdXSLT1g/cQte3bQ+BuA/gnjYP8lzJ68NG5v0EwJCQAh0m4AI3m4TF38tERhJZz+gMZ8b/E5lXlty8ze3ZKBJ51TKMjmBasEH/qbn5F8Tpf2FbNXvKncj80EtjmT6oAzxjKpKp/L/btP84QPmS5GlpzNvA9PFwTjiN3mFfJCjuBttPqGWSqV258RO9wEYJuCuYsSXFrsxr277iFPwrjnooH9LPDqjjruoozg3e469ttvzE39CQAgIgVYIiOBthZb07SqB8fHxFZu3+So92D4A/uA59lOjDqC+HCrAb/ecfEXkxdx003pUFc9QbuIuCjA7lSOOGNLvulflLVY8y742bG6YXD9vvlTdtGaqovhRz7FXxoxjjvn5hJpuWFeAcFovBHg35x6lrzgFr25mvwvwCQQ8wjMJw/NuvzfK2MWWEBACQiBqAiJ4oyYq9iIjkDKyZ3M1MwOBjy06+aCyVpRNN6xLQHirskmkHVYsTPwxSvsL2YpTjCzsM3MrQE9QO7tEOK5YsG+cr2+vzxc3sqmeFd1UPXZyh+fYI91Yo0H3Uc8xyiwgqbGxQ7hS7ZCizoc96MwlfiEgBPqXgAje/l2bZR1Z5eyur3aNhkB4wCvYQUGEqJtuWn8B8EQlAj3HDnZcu9G6LXiTpnUdAccHNSWIz/AK+dk8u43z7eX5YhVLIxvdzH4R4DepZwy8o+TYn+vGGg26j7iygKRMa4KBbJSlvQedtcQvBIRA/xMQwdv/a7QsI9QN6w0gfCmYfEypwlavXr1yeOVjNlcB3+I59rO7BbubgjdpWE8jwu8ql9ToYs/JBRkvFmq6mbkWoJep51TmsWIxXz3j3B069Ww0f3gfX5uufV3+sOfYu3UnisH2EtcufV1auGlo/kHe5OTkYJOS6IWAEFguBETwLpeVHrB56mbGBigDwPYcOxtH+COjY2s1TftlIOwInygW7LPj8DOfzW4K3lqFOjD+5rn2fs3mWMtawYwHS669R7P+UT+vZwNAZQL4z8CHr53uTW2vABe136VkL45d+qqIvlVdHATjS55rV85USxMCQkAIDAABEbwDsEjLLUTdGH8xyP9BMG/CKV7B/locDFJG5mwmCqq3+YTjNxTs78Xhp5eCN5W23smMC6ui/uiFzu3WYtTTmdPAFBx3IELT/nHwmit4+e8A7Q3GbZ5rHxyHv6VoM2Va1zNwXLCOZRopFnPq8mdHTTezNsDqQ+jdnmM/viNjMlgICAEh0GUCIni7DFzcNSeQNK3fEfB0EEpewdabj2ivh25mfgjQsWr0tK+ZG6cm3PYstT6qXtRt3bRql7vvXlc7WtG6sUVG6KZ1N4D9mPC7UsF+ZjPjSdO6n4A9GbBLMe2sN4uhYYc36O5r2nM3TE4Eu/HSmhPQTWtbsBNL2OQV7F2bj1i8R8ocP4HhfzfoFeOH0E7jlPFCQAgIgYUIiOCVd6OvCIwYmadrROq8qbpddVbJyX0+rgDrCk5s9hx7l7j8zGe3G0caUob1FiZcGmgUzT+0OLl4hTrdtN4L4JNB/x7t7irf8wjeX3uO/Zxurs8g+0oamSOI6FfBnyHCRaWC/a5O51N3LOYOz5UsGZ3ylPFCQAh0n4AI3u4zF4+LEKjl9wTwCJW3ri4Wi/+KA1hcl3rCxtoNwaunrbvA2D/s7m5tV5CA+4qO3bOyvfVsmLnM0A7d4OZUNg1pIQgkjcyHiehDqqtP9KINhdxNIYYt2CWZzn6YmAN7ms+HTE3l13ViT8YKASEgBHpBQARvL6iLz3kJjFjWftoM7gwKHjCu8Vz7FXGhiuNSTyuxxi149XTm3WD6tIqJgGOKjv3jxeJLGtb3iPDS4PSAn0hOTd2u1mFOS5mZY0DkNzsH3AqH+frOEbygr5Sc3Kmd2lxO43XTygMYA3Cv59j7djL3dDr9uGlOTKkS02D6qefmjuzEnowVAkJACPSKgAjeXpEXvzsQqN+ZgkYneZO5a+PCpBuZ74LohEAQRnSpp5VYYxe8pjUd5DAG7vEce//FYksZY69n0r4c9GE+z3PzH2zsn0yO70vDvjoPTHGmKpu7897FKnStLF4LfUczmaeo7lP57uyKJs3sUwn8+8oHHb6q6ORf30K4O3TVzey1AAcp6mR3txOSMlYICIFeExDB2+sVEP+zBOrO1G7xHHtVnGjqSvt2teBEbU5xCl7dtP4M4Elqt3aYyvsUCoX7F2K51/j4rrtu89Vu7mNBfJtXyM+bCUHfLqCVlPqp50S/01cVu38CMFvKuGtll2N62arvtCofnYjJxRyzSSNzNlUzjzDhlaWC/e12/Y4Y1pM1gqo8qMnubrsUZZwQEAL9QkAEb7+sxDKPo5tnaut9EeOmomu/qNv44xK8umF9G4SXB/Px+UxvKn/JYnNLGtbtRBhXiSrKrD3xDnci19g/ZWYvZ/Dpwc8Zm0FYtdBOcLscq2uihPqcaneDLHh103oEQHAZslvz0E3rFgCHq/XkaW1NqTTxt3bXRDet/wXwNDVednfbpSjjhIAQ6BcCInj7ZSWWeRzdPFObTFsvJUY15y5f7Dn5RSuPxbE0cQjeNens4QlmJXjULuwNnpN78aJi18y8jkBXVXQsvl1y7Fc29h8ZG9M1X1NnOHf8uyKiCngNxxhUOq0VtTjiTNkWx7rW24xjjRddz8qxE5VvV6Uju8UrtF85UJ3XZtANVX8/9hz7mLh5iX0hIASEQJwERPDGSVdshyaQNK3raTZRPtLFou2EHtxix6RhvYMIFwWykPGuomsH/97NFkce3rpjBzOeYw83m49uWjPVC4KbPXf+tGx62loPxkEANoPxPhBmd4xVBoWZnRJ7b5yYeKCZr4WezykyUr1k1W2h2G7szcZ1ex4pI/tqJv5GJS56r+fkgkuL7TTdtP4PwGEAHuUEW6V8vrSQnWTyiY/FTtueo/HMP4qFwm/a8SdjhIAQEAJxExDBGzdhsR+KwGw5W+CBkmPvGWpQm52SaetCYrwzGM50kufGdzluQaFnWlx7FsXX3clMJkll8qpi5zrPyQUXjRZqc3ZVmb7hubnXNvbVjQPfACp/qfJzfq/n5D+tm9kvAvymWt8EJwzXvV3tALfcxsfHV2ze5m+tDpwtId1todhy4CEHdHseSdP6FgFBZhOf8ZQNrq2OiLTc6vP4gvkSz82fOZ8R3cieCI1PBOPE2eeE28B8s0/aTZ2mQ2s5cBkgBISAEFiEgAheeT16TqD+jGg3zgrqaes7tV/SRNphxcKEupjT1Ra1GEqZmeMZdF0wCaIPeoXceYtNqNkREsMw9irT8F3B1+PAnIt9dZcLwcxrS27+5nbg6aa1BcDOzNhcqtthjppNO7FFMabb86jb4e/oImbKyP6SidcycFfJsVfXWCTT1is0n/cHaQcx81Eg7FPPiYhmmFllBqk1l8C3kK9dOzXVWS7gKNZDbAgBIbC8CYjgXd7r3/PZ158RJeD6omMHqcLibLqZ+QNAhwbasAcpyZTfqMWQblofBxCkE2NNO6o0OfGTxRjqpvULAM+tMFgxViz+tVDfv75KG5he7rm579Se1/PzkchucG63W10vPZ05DUxXVPQ5jq7P7Rs1m1Zji6p/HMdWFoptdHTc8DW/dgzoZs+x17Yzj2Q6cw4x1T4sqfPgDzGQJAQXG1to/HeAZouXMPDZhJ+4cL78zi0Yla5CQAgIgbYJiOBtG50MjIJA/RnRBE/v4bpu7SvuKMzPa0M3LT+4hEV4wCvEe3xioUlELep00/opgBcEgnd6J71UunXBM5cjKetoLYEfVWKjez0nt0NxgqRp3V4VOXd6jr2mfh66af0VQJC+rLzz0B533Hbbg60uVtK07idgTwbskmNnG+xHetyj1dii6h/1Gi8W10g6e5TGfGNlSXGZV7Df2so8DMNYXaahswB6T7hxvBFEP2GfvzmEmQlN07hMNOb72kuA4IhDsmpHreX23zOML5FWvqJYKHT9W5Vw85JeQkAILFUCIniX6soOwLz0seyJ8DnYOSTiDxQL+fPjDjtlWkczKmKPQWeUnNwX4/Y5n/2oxVDdMYNHPceezWPb6FuJ3cQQvs8cFKXw58sPO+cM5zypzXTTUqmugq+z2zl/rI9m3gaNLq6s+9zdXfWzqNn0Yn27PQ/dzJ4J8Oer7/VZJScX/HuYlqxk6/gkgL2a9N/GzOrP6M2LHWPZ5+CDd1n16LZTwXRh9UiMMqtKhO9esz+95eFVGzduVEdapAkBISAEukJABG9XMIuTBURfrRrYVs+xd+4Gpbqd0OnpLQ/v3qtfulGKurAX1qo7uz8IMjMAD2j+sDU1tf7vjdz1tPV1MF4D5i3TOyVWN2Zh6DT22tldEP7hFezH7eA/4gt93XivuvGhZrF5pEzrIgbeEfRh7SWeO/HDZvNW7402o13GxEfV9yXgLjDOY2gbGeWH1DNK8D3e5ORkM5uNz1Nm9lQGvwvAgfXP2vmg1Kpv6S8EhIAQaPi7TYAIge4T6GahidrsKl/bDquqYupL1q97Bfvk7s+84jHK851hLqytSVvPSzB+Vv16eUGxm0qldufETiqX624A/9Bz8uor6jmtE8GbMjJnc7USGBE9r1jI/XeU9vWxsRdTWRsFsNUH762B/sXAQwA9xPAfooS2F8/wDDTyEv6QN5/gj+qd6IRTqzGkDOt7THipGhcmQ0PKyH6EKRCiu1Z9qRR1QyBsSvjDGdddv7HVGBbrXxW+V9b6iOCNkq7YEgJCIAwB2eENQ0n6RE6gWZaAyB0CGBm1PqZp+A9lmxlPL7n27+PwE8ZmlGKo2YU101QJd4duD0rEAts0f/iAhYRevTAhptcU3dw3oxKkj89k9lxRJjcoYwz82nPs58zHql02upF9DYi/HoZ/XZ8HAJoC+7cMa/6FhUJBZaaIpLU7j3ac66b1FwBPVGMTPL2367r3zWcnlbJMTtC1AD+h7rn60BFcYGTmD5Xc/EfbiaHZmG7yaBaLPBcCQmD5ERDBu/zWvC9mrBvWNSCcFASjDWe8yfUtf13a6kRmv0oHlz0nX58+qVVTHfeP8pd//YU1zR/ep1HM6qZVVJvK6o6Z5g/vv9iupm5avwPwdACbpldoa+YrKtFu7Lpp/QrAEc2KGbRjP2VaVzOwQ6W4BRZKXaTawioLc6WqW61IR6iCHWEXv34em1cO73rv+vWbwo5ttZ9uZmbUwQM1br7d09HRAw8oa+V3Ue3YQ8VBCey/F6RdU/0wdIfn2COt+g7bv511DWtb+gkBISAEmhEQwduMkDyPhcBsoYmGHKyxOFO7u5a1nzaDu5V9BiZKjj3nTGFcfheyG+Uvf920VGaLoBxvo9hJpTO/ZKYgRVWChw9Y7KtqfWzsYPiaysAABl9fcvLzpohrJ3Y9nXk3mILKX0z+J0qFybOjYDNiWkdrhEvBqAk1JWY3ALgHoCEQ7w4OLkupf5qeE4/yq/Z2OLX7Hi7ka3R0fG054b+OGA3Hd/jiVSsSH9i8zVcfhtRltUjF/nzz6CaPdjnKOCEgBJYuARG8S3dt+3ZmvTi/OzqaeYqv0Z+qgvfGkmMf3UtAUf7yX8iWbmY/B/Dbgzn7eGVpyv72YnNOGpnziagiRBepQFfvTxUKqbfJjBHWWFNnZgHaB0T7gPn5oGrKNGZ1w3/RHLFh2czJJhEEQTd4Tu7FzdZVVXh7ZK1cZAAAG3JJREFU9FHah4em90FZO4yVYK62qATv3He8vWwWzeZR/3w+ZiOjY2s1Tftlg51Nmua/cGpy8re6aakLaY9Rz/0h7L/Btu9pxWerfcOua6t2pb8QEAJCIAwBEbxhKEmfSAn04vzunItdwKc8x35fpJNq0ViUv/zns5VMZ04hpq9UwqKmpYZVr7CVuur9tTZtvs9z8rPFCBYaG4ZNNePEDdtzvNLbPScXpDprtYXx16rNOe94m+nbWvHZOIdGPgRMAXRu0cldVV3r2VzHmqYdPjU58dtW/LXTNw7O7cQhY4SAEFieBETwLs917+ms68/vdqvSWdK03kfAJ9TEGXxqyclXxWBvUET5y7/RVjUbhcq0oP58P+I5drCLt1jT09lngfnXFT54f8mxVV7WeVt7gpc2ek7ugGZxNIqxrZtW7XL33es214+rzyVMhBkq89OmpvLrwtier0+Ua1Gzr5uWEuPH1P47qp3jMGviI3FCgsrXVHMtlwl0Wk3oNvItszZ+hzuRa5ddK+Pi4NyKf+krBITA8iYggnd5r39PZq+b1qMAdlLO4xYCswLEsP4LhDcGgo557WKJ87sBJcpf/vW2Vq3Qdtq8zb+/+lV1eRtPJ+9y3aYppvR09gNgPjeYu+ZnFsu5qpvWIwB2WYyTOidNhBwBRYB+VCzkfhOW62Js1oxmTkto+EL1gtaC6dXC+moUgFG9j3WFQIJQorLbTPAy86NE6hJecIFtDh/dGD8W5M/m551eoe0536XEVti10jfKd74Vv9JXCAgBIaAIiOCV96DrBHrxi68uQ8CSFrwE5BhQpXp9Ar+k6OSrJYQXX+bZtFaE27yCHZQN7lWb7/1QWQZ8Kn8IhDcEcTEe1Hg4E0Ue3ajfR920/gzgSfX84hS8jeeF5+Ojj42/GL6vio6ottV7/D674uabVe7drrWoOXctcHEkBITAkiAggndJLONgTaIXv/iWi+CtvQmt7GKn0tnDmfmWQEcSLioVbFWQoGdt7qU47bllzT+ZgFPqAlo0l3CrgUf5PuqG9W0QXt4YQ5yCt/G8cGOuZcOwnlwm/LGWh3lbgve/K5//R6ucOu0fJedOY5HxQkAILD8CIniX35r3fMa9+MVXL3gXS8zfLThRMmg8U0vEHygW8ueHnctsKeFgW5hetKGQuyns2Dj6NTkjvFXzh9dEsbNbiz2qtdCNzLEgqh4Z4BsAOrbmI07Bq5uWOnv9rKqvHXIt64Z1Dwj7EjAzQ+WxOwoFlYqs6y0qzl0PXBwKASGwJAiI4F0SyzhYk+jFL766DATTnmMHOWt72aJkMFcg0nc9J3di2Lklk+P70rAfpKMi4JFiiAtuYW23228+wduYZaBd2/ONi2otUqY1zao8L2Oz59q7RGV3sblWszHMHltpLDyiG9afQHgKwGUfePkGJ39dlOxasdUNHq3EI32FgBBYXgRE8C6v9e6L2Xb7F5+uH7gPhsp/q07+Zs+xF80D2w1IUTEYMTJP14hUdbSgtbqTmDSyHyLiD6uxDHpzycld3o35L+Zjhx1r1s4uuhNBho04WhRrUf8NguZr5tTUhBuF3cXmm0pl12CIp6rZGHZY/5SZvYrBr6usLb5Qcuy3xMEvrM3Gy5UTExPbwo6VfkJACAiBTgmI4O2UoIxvmUDcQqAxIN3IvABEPw1+TrjMK9hvbTnoiAdEwWBNNjuemObb6i+ftiJ4VQGGzdt8lb5sHxAKXsEei3iabZmbI3iZLvHc3JltGQo5qNO1SI5aryAN3wrc1cXbqd1m4etm9g8AH1rfr7b+ejrzaTC9u/rsD55jP7WZvbifzz2bnVgzNXX7nXH7FPtCQAgIgRoBEbzyLnSdQNxCYAfBa2bfA/CnAr3LeFfRtS/q+qQbHHbKYPX4+B7DlfRjc/4MtyJ4U4b1llqVMQKfUHTy1/eai/LfKZtW59CpP93IzIAo0XgkpFO7zeZREbyByp4VvWr9U4b1TiZcWB3veY6damarG89107oLwP7Bn0PNP7Q4ORlUPpQmBISAEOgGARG83aAsPuYQiFsI7CB4jezFIH5bRRvQSZ6bu7bXS9IpA9208gDUjqxfvX0fTKkVwaubltrdPQCgP3pO7rBeM6mK3Z8BeH4tllbm0278nazFfEcZanF0YreVudT7YeB6Ao6vjt/sOfai+ZJb8dNpXz1t/QmszhMHiveJXmHir53alPFCQAgIgbAERPCGJSX9IiPQLSGwXXhkvwvwCcHvWdIOKxYmVIqmnrZOGCSNzK+I6Ag1gWlasXqYt80WlggrEFOG9WYmXKZs+L7/7xumJn/VUyAAkmPjR5Lvz8kQEXY+ncTe7lqkzAOPYZRVRTV1SPZSz7UrH6qqrV27rc5lvkt+KjXZNJVTGwsFtavaF003LSVwgxzPCZ4+wA1REKUvApcghIAQWBIERPAuiWUcrEl0SwjUCY/fAwjOMDbeYu8VuXYZJM3slQQ+VcVN0F5VdCa+1Y6tWtYKBu4qOfbqXnFoEIibAKyq/1k/C966zB/z7qS2sy6trsOImTleAzVkXuBvJnjmDa7rbm3VXpz9ddNS30ao3zkPeI69Z5y+xLYQEAJCoJGACF55J7pOoBtCoH5Ss+miWvzKP04w7TAwLOvJ5Rmoc48E5m96bv41KsZWbdVVAvOHqbxPoVBQZ4F73ubbqey24OXpLStLpZIqfb1o09OZ08B0RfDBg3B0sWDf2Dig1XVp5rPxuWEY42UannNpkQmvKBXsa1q1FXf/lGkdzUCQPo1BZ5Sc3Bfj9in2hYAQEAL1BETwyvvQdQJxC4FuC492ALbDQDethwA8BkDJc2y95nfu7Xc+ZHs82t4YKjNm6L7az3xNO6d2vAM+n+lN5S9pJ/44xvRe8NJGz8kdEGZuumkppo9jwC45tirlvENrZ43D+K71qf8gV/tZNz4gtBJj3TuqsqS8QJ3Cmd7y8O4bN27c0o4dGSMEhIAQaJeACN52ycm4tgnELQSWouDVjeynQazSTPlURqZYtJ35BG/YRSHC94sF+7iw/bvRrxeCd7FLZ/PNeXT0wAPKiZmvENO/q+c++IQNC2S3iPM919OZm8B0ZGOM/Sh4DcNYXabhSgoywte9gn1yN94n8SEEhIAQqCcgglfeh64TiFMIzDeZbvsLA7SVmFSBAU6wysqwksFXlZz86+t9LHBpabEwHvH6oKLaYh9Mas/iFHBzLsk1yc+cNDLPIdDrQdgu1oge9Aq5PRYC3coah3lnan1SpnUZA2+u6Ed8joGzas9WrdB26reCDkkjez4Rn12N8TmeY6tSyNKEgBAQAl0lIIK3q7jFmSIQlxDotvDoZDVbYaCblhIIzwLhgVXD2sjExMQjDYJX/XfY9FObPMfetZPY4xrb7R3euktn2zzH3mmheaWM7OuZ+MsNzxcdE8d7nhwdO5K0xEfr8u7e7zn2XvXciPmcopu/IK41aseublozKjEDgH96jv1v7diQMUJACAiBTgmI4O2UoIxvmUArYq9l4/MM6La/MDGHjUkfy54En4NLSMz8oZKb/2gY+4PYp5uCd40xnk2QP1HhRFd4Tu70+ZilTOtqBl5Ze0bAFEDnFp3cVc0Yh13jpnbS6ScQa2cz6BV1fWd36XXT2qx2/6vP7lu1QlvdL7u8KTN7OYMDtprPh0xN5dc1m688FwJCQAjEQUAEbxxUxeaiBKISAmExd9tfmLjCxpQ0rY0EPJ6Be0qOHVSpWqptPsG7ddOqXe6+e50SdJG2pJk9g8BfCIwyvdZzc9+odxBkFSBcCsZI9efTBDo9jNCt2Qm7xgtNLJk8KEPD028GSB1fGK7285n5I/UffFJm9lQGX8nAJgJ2IcZni679zkiBtWFsZGxM13xtqpLIAtcXHTvIhS1NCAgBIdALAiJ4e0F9mfvsVAi0iq/b/sLEFyampGmdRcBnq/b+w3Psc8PYHtQ+3dzh1U3rFwCeq1hRecVYsfjXQo1b0sgcQUR1hTjoBs/JvbhVrmHWeD6ba9IHpRI883pmPpuI1FGAWrsZRP/PK+RuaRynm5ZKT3bgrCie3mnPUunWf7Yac5T99bS1HoyDAGxO8PQe/ZYXOMq5ii0hIAT6n4AI3v5foyUXYbtCoF0Q3fYXJs4wMemmVVbfBBMwXXTsFWHsDnKfbgnekZR1tJao5IQF6F7Pye1b41Z9pqqnVf9upLd7Tu7idriGWeN6u6Oj1oFljd5G4NcB2H6mmLAOPn3Sc3PfWSiO2i5vkOZW7aj2+CyvPpY9ET4H8RLxB4qF/PntMJQxQkAICIGoCIjgjYqk2AlNoFUhENrwAh277S9MvM1iSmYySSqTV7X1Y8+xjwljd5D7dEPwpsyxsxj0GSDYOfU9x57dQdWN7EkgvlpdsCJghnx+WidnTputcW2tdCPzAhBOA+h49QGnbg0fJaa3Ft3clWHWVTctldt252rfnp7lrbsQuNVz7FpMYaYhfYSAEBACsRAQwRsLVjG6GIGwQiAqivX+wlbSisr3QnaaMUiZmeO5VjKW6INeIXde3DH12n7cgrea3m1DdZ4PUHnoCH9oZlRjPIfBawF6Qu2Z5g9bU1Pr/94Jk8XWeCRtHaf5OI6JnkHg0QY/dwO4zB/ClRts+56wMcxzlveiomu/K+z4qPqFvRAYlT+xIwSEgBAIQ0AEbxhK0idSAvVCoMza+B3uRC5SBw3G6vzd6Tn2mjh9hbXdTPDqpvVxAB9U9ljTjipNTvwkrO1B7Re74DWtaQaGgnoRgCrcMTYPq22aP3xAp2JX2W1cY31s7GD42gsBPhGgQxt9MzClMS6Fv/XKYrH4r3bWsfEsL2aQ8jy7JvLbMdnymGYXAls2KAOEgBAQAhEQEMEbAUQx0RoB3bS2AqicSZ3nhnxr1hbvrZvWnwE8SXkapvLehULh/ijtt2trjrhjvM1z7UtrtqoZAr4ERnC2lKdZL5XypXZ9Dcq4OAWvblo/A/D8Jiw2af5wKgqx2yh4m/j9GYOu33O3lVeuW7duupP1ajzLC8I3vIL92k5stjo2ZVrrGHhyME4bzniT6ydbtSH9hYAQEAJRExDBGzVRsdeUQMqwbmTCURW9i4tKhXi+dtXT1rfBeHkQkM9nelP5S5oG16UOO4o7/hlYux2ElwKc2h7G3EtVXQqvJ27iEry6kTkXRB9YYFK3MvB7jek323aimzZOTDwQ1eTr5hNcJGuwO83M52k+risW8yrDQmStLi+vuvSYAOi9npP7dGQOFjCUSlnP4wR9EmD1AVO1ez3Hnr0QGLd/sS8EhIAQWIyACF55P7pOoP7rejD+5Ln2Dl/vdhrUmnT28ARzNX1Te2mlOo1hsfG6YW0CYVWlD20GuPrv9aO47Dl59RX8smhxCF7DOHC0TGV1ZGY2ywWBvuIT/wDbdro5ztRdumn9A0BQejg4rgC6hZl/44P+vMHN/SWuRd0xrRqYCe8uFeyL4vA5OnrgATOa/14NfOasfeay5y6fdzcOrmJTCAiBaAmI4I2Wp1gLQWDOhSz1jf2Wh3fZuHGjumEeWau7JT7t9WlKr2Q6ez4xn73DpAnXEuOrRcf+cWRABsBQlII3OTZ+JJXLqhzzGwHae7sQw6Wea7+tWzjUBa4V2Hqf67r3dcun8lP3bs3uLhPxrwA6r1iwVQ7ijpuezj4LPp8GQsORCfq85+TO6tiBGBACQkAIREhABG+EMMVUOAINKbcA0p7qFSb+EG50816DdEu8uhtXuZBG9NGEv+1rrutubD7LpdejoURuMEHPsUP/HTU2NvaYbWXtJBBOAfCseQg97Dn2bkuP3Pwz0s3MZoCqJYfrv0XgizV/6FNTU7ff2Q4LtaPrJ/z3gbnxg8NWZj6y5OZvbseujBECQkAIxEkg9C+TOIMQ28uPQK2oQmXm0Z4xlFvig/k+1V24mp2A5vMh22ej7Y2hMmOGZndLfeIUNFoDn3QiOpHB23dz52Lo253+OFer4VuEuWeJCV8i4HtU5ntrMTBjhIb8LTOJxK31KdH2OfjgXXbZ4q9lKh8FxlvqYyZgCqBzWym7HOecxbYQEAJCYD4CInjlvegJgaRpXU3AK6vOb/Yce21UgehG5loQvSyQ0mUeKxbzs2Vjo/IhduIh0JBWqwMnXK4WlwCB3lx0cpd3YGygh1a/RbipriiFSnm2ewST2kagM0ToRkBSTAgBIRA7ARG8sSMWB/MRmLMLCzBPa/uXShN/i4LW7O4x4UGvYAeXhqQNBoH5dnlbiLwEpusB/6cg+iaAxzFglxw724KNJdu1ylYVojiwk0nKjm4n9GSsEBACvSIggrdX5Je537nnbIP8ZJd4bm77Le82+ejpzGlgukINJ8LRxYJ9Y5umZFiPCMx3eW2RUKYJuIyB6z3H/rXqlzKzV/L/b+/+XeOs4ziAf54L/kIXcegilgtNm6hrMxRx6SZYROkkDv4DopsuLmKho0h1dCkuBUVxEYcitIsiuLRXc01joS4OYhE7xPYeyfX6K0bzaULN8+m9OoXyubv39/XJ8CY89zzRvr728yjaVy4Oz322Q0fp5MdOiu/afZ8n1/emY15uonnLX3TTXgYJEOiQgMLboWVMW5T+3MJqRDxw49y9Xu+55Z/OnN6OQ39uYe36Tn/Z2w7iDr92s6fQ/Ve82bmFT9sbl8o08dvK0uCJHT5O2Y/v95/dFQ/+tasdzTxytbl66dLS0i9lDyM4AQJTL6DwTv2vwM4B9PfMvxhN8+X125SOb8y/5Wt5x98c7137JCIOrp2o18TLy0uDz3fudD55qwJbKbyzexdeaCM+ijZ2Tz53Kr+ktlVzryNAgMD9LqDw3u8b7vj5+nPzxyOaVyPiSsT4QQzvrgwH72Vjz84+fXA0077WxPhWVNf/uXY3y9fJubstvP+87rd7DxrpJLRQBAgQmCIBhXeKlt3Fo+7et6/fGzXDG9+onxTWY71rM0c3u0/o7OzsU+3MQxfXnWt1ZTh4qItnlSkncHvh7Y1Wdy0vL//6b6+84xKG6788b6wMz36Y+yRTBAgQIDAtAgrvtGy6w+ec3Tv/Tts2R267tGGcto34ONreFz+fP/P1RvH7c/NXb916yr1AO7ziu4p2x5fWmji+sjRY9ySviP6efYeimTkW0T45efPV3qg9sLx87oe7+jDDBAgQIDAVAgrvVKy5+4fszy2sPVr44UnSdfcJbS83bXNyFPFtRPvjeKbXvN+0cWD8N7229/aF82eOdv+UEmYE+nsW/oxmfHnLtYiYibY90kZ8M9n7oaZtX4po+rfeyyUMGVczBAgQmGYBhXeat9+xs298n9BbDxDYKG4T8cGF4eDNjh1FnG0ITB6UcDL3Fi5hyDmZIkCAwHQLKLzTvf9Onn5SfNeuw3x0k4C/rwwHj3fyEEJtS2DdI3E3eq8rvVH7vEsYtsXsxQQIEJgaAYV3alZd76D9vc8sRoz2xygWo2kWI9r5205xZWU42KwQ1zu0xDcF+nMLf0TEYzf/o21ONzH6Knq9UxeWzp5CRYAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8SanDhw9/17bt/uS4MQIECBAgQIDAPRVomub7EydOLN7TD7lP3lzhTS5S4U1CGSNAgAABAgT+FwGFN8+s8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQU+Bsshf4JxN0mVgAAAABJRU5ErkJggg==', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0');
INSERT INTO `portal_user_db` (`id_user`, `created_date`, `full_name`, `badge_no`, `username`, `password`, `department`, `project_id`, `email`, `sign_id`, `id_role`, `status_user`, `last_update`, `update_by`, `sign_approval`, `warehouse_permission`) VALUES
(1000076, '0000-00-00 00:00:00', 'Direktur Oprational', '-', 'dir.ops', '827ccb0eea8a706c4c34a16891f84e7b', 33, 0, 'dir.ops@gmail.com', 'default.png', '3', 1, '2020-02-23 15:01:42', 1000049, 'iVBORw0KGgoAAAANSUhEUgAAArwAAAD7CAYAAACIeEe+AAAgAElEQVR4XuydCZwjVbX/f6fSPTADgg9kdZhOpavSqTTgBrihMs8NBFRAcAdREVwQd0Hf+7uyuIHKIvIUXFFEcEHB7SlP1OdTR3GgU+lUdSoDA4ggKDAzzHSnzv9zK0lPOtPdqSRVWbrP/XxQ6Lr3nHO/t2b6l5t7zyFIEwJCYKAIjI+Pr9i8zd8CQJsncPYce76fD9QcOw12jTGeTZB/K4DhTm0tOp6x2XPtXWL1scSM66Z1N4D9APj+EFZvsO17WpliKp09HL5/OBMdDkD9s3swnrGZiB9g0OqqvYc9x96tFdvSVwgIgaVLgJbu1GRmQmBpEtBNa9uiQo60p3qFiT8szdmHm5VuWlsBrJin9yYAdwC4G4RHwPwIAY8w4xEGHtGIVjJoJYhXMdNKjXkVE60EWIlaJZ7UP/82K7JAV3hO7vRwUUmvpJH5LRE9Q5GYpvLqjYXCXZ1QWT0+vseKrXwUk38UQEcB2KNqr6z+33PsoU7sy1ghIASWDgERvEtnLWUmy4CAns6cBqYrFp8qvddzcp9eBjjmnaJuWr8G8Kxgz4/xHQA/90F/nhny77grn/9Hp1ySZvYMAn8hsMP0Ws/NfaNTm8thvG5kPw3id1ew0QWlQu6cKOedTqcfNw3tOIA+AsZ+DEyVHNuI0ofYEgJCYHAJiOAd3LWTyJchgaRp3U/AngzYJcfO1hAkTetqAl5Z/e+fe479gmWIB7qRPRHESuSCwK8uOvmro+agG9Y1IJwU+CjTSLGYUzvG0hYhkEynM8QJOxC7wI0lxz46LmC6af0CwHMB3Ow59tq4/IhdISAEBouACN7BWi+JdhkTGEmPv1Vj/xKFQCMcN1Wwv79d8NbtOgI8veXhXTZu3KjO+S6rppvWXwEcDMKDXsGufb0dKYP64xKeY8vfoSHo6qb1r+pxkH96jq2OhMTSUunxQ5n9ynEewslewf56LI7EqBAQAgNHQP6yHrglk4DjIJBKW8/TfNrFdXM/iMN+FDZ105oGoM4k7nAZp3pJa6LmZzkKsfpdRIC/6zn5E6Pg3mhDNy1ezpxbZaqb1o8AqB1dn6E9peRMqMuEsbSUaX2BgTOYuTyzU2LvjRMTD8TiSIwKASEwcARE8A7ckknAURIYHT3wAD/hvw/Mb1O/kLduWvWYu+9etzlKH1HYqj+7S4SjiwX7RhFicwnMOVtLfLpXyDc569zeyojgDc8taY6/kuAHx0oY/MWSkz8j/OjWeu5z8MG7rNoy/TcAuzLwvZJjH9+aBektBITAUiYggncpr67MbUECI6PjazXyX6e+9pzTiXCKV7C/1m/o9LR1P3jHs7v1cS53IVa3kwhofsabnJyMYx2XO+dWmOqm5QFIArjHc+z9Wxnbat+UkT2Hic8LxjGd5Lm5a1u1If2FgBBYugRE8C7dtZWZLUBgZHRsraZpv5zvMTP/suTm1YWXvmm6mX0PwJ9SAS20u6ueLWchlkw+8bE0vFXld10J4FbPsZ8U1wIuZ86tMNVN6z8AfCx4b5neUHRzV7YyvpW+hmHsVaZhlc83AWCr59g7tzJe+goBIbD0CYjgXfprLDOsIzCSso7WErghuNJS+Z8pAH9l4HiAywAloPlP8CYn1/cDuCDVEifUL3J1dneb59g7LRTXchZiSTPzOgJdFWzuMX+k5OY/HNf61XG+33PsveLyM8h2q8cLVPaKPYjoV8VC7t/jnE/SzJ5O4MsDH6Sd6RUmgsud0oSAEBACNQIieOVdWDYE9HTmTWC6LNgFYi4TaacVndxVhmGsLtPwndtB9E8xgblnd/ndxUL+wjCCd/PK4V3vXb9eFVlYFi1lWDcyQRUeUIJ3bcnN3xzHxJNG5ggl4Cq26e2ek7s4Dj+DblM3MzcAdAzA/+LpnZOl0q3/jHNOumn9GcCTAPpngrft67quKjwiTQgIASEwS0AEr7wMy4JAyrTOYuCzFZ2CB7TysDU1tf7vtcnraes7YNRu9ffN5bVaEQUCpv0Ep0v5fCmE4H3Ic+xKudVl0Kq74PdVp/qo59jqWEMsTTesr1bPfW/jBI8tth6xBDAARpPpzCnE9BUVqsZ03pSb+2CcYdenImP2P1tyJ98Zpz+xLQSEwGASEME7mOsmUbdAIGVaV/P2ogxbNX94Tb3YVaZSpnU0Ayp9UtD6Ia1XZed5qBQcswB+5Dn2sQtNe25asv7ZoW5hmdrumjKyr2fiLwcGGKd5rv2lto0tMlAfGxuDr92myjoT6Lqik3tZHH4G2eacI0Mx5kKuZ6SnMzeB6UgAm/whmBtsWx0BkiYEhIAQmENABK+8EEuWwOjo+Fpf89VFGXVLXLVtmj98QKPYrQHotzOweto6GYyvqvgWu6ymni/ncreptPVbZjwDoDs9J7cmrhc6ZVrXM3BcmPWIK4Z+tqvEbmII32duft48qnmMjmae4mv0p8pnHb6i5ORPj8q22BECQmBpERDBu7TWU2ZTJTCaHj/FZz/4WrXS6AbPyb14MUB9J3hnE/az5zn51KKx15e7TawYK+b/WlgOL0MqkzmIyxRcMGSmD5fc3EfimHcqZZmcQF59Sw/gL55jPzkOP4NqM5my3kwJvrj6bcQDmj/3yFBc85rd3WVs9odhyO5uXKTFrhAYfAIieAd/DWUGDQRSRvbDTPyhOrEb6nJRPwneNel0KsGao66ch8k6oJtWORBjzI96bj62M6z99rKlzOzlDD49zBnnTmJPpq0LiPH+wIbkeJ2D0jAyTy8T/a7CBg9qPJxZ6FuUTtagcazs7kZJU2wJgaVPQATv0l/jZTVD3bTUBZmPVybNZc3HU6em8uvCQKgXvFs3rdqllxXXRszMpzTQeyo7l4tnHUhmMkkqk0rwr3ayr/OWydnSVCq1Oyd22gBgdzD9wHNzLw2zzu30qSvrLDleGwDqpjVTzX+76JGhdrgvNqbu7O4jPK2ZpdKEqrImTQgIASEwLwERvPJiLBkCKcN6MxNU2jHVNiV4OOO66zeGnWA/7fDWCaymGRdSZuZ4Bl1X0bv0Qa+Qq1SbWuJNNzNnAvT5yrTnL7ccBYKRtHWcxrg+sMU42XPtr0dhdynY2J4ODOwjMb7Bud3uxrzqd3fBfInn5s/shl/xIQSEwOASEME7uGsnkdcR0I2xk0DaNdUfPew59m6tAuoXwasb2RNB/J2KwNJe4rkTP1xsLrppqR3tIPUTa9pRpcmJn7Q690Hsr5uWOqdsApT3nJwV1xxql9UIuL8ohSZmMafMzHcZdELwA5/P9KbyXSv2UHd2958JTI+4rvtQXOsvdoWAEFgaBETwLo11XNazSBrW04jwv1UIWzzHXtUOkH4RvLNZBxiO59rpZnPRTev/ABwW9JtB0vNs9TX/km7JsexLyOfvByIfdFbJyQU7vVG30dEDD/C1sgtgBRhf81z7lKh9DKK9Nens4QnmW1TsRPh+sWAH2Su60eac3WU6v+TmPtANv+JDCAiBwSYggnew12/ZR7///k9ZtdMum2sVxRYtvdsMVj8I3tTY2CHsa38MYg1RIrWa97SSP5job14ht1+zeS6F57ppqUpqzwHwiOYPj8Z1SSppWmdRrWCJXFabfXXqzu1u8hx7126+U9vP7vLf99htl9Xr1q2b7qZ/8SUEhMBgEhDBO5jrJlED0I3xY0F+7et+33NsVaCh7dYXgte0vsXAK0B48JFhbc19ExOPLDSh6pllVdpWzbvj+bcNrssD9fT4YWBf7WqDwF8pOvlT4wpBN60cAHVc4uHy1qGRO+647cG4fA2KXd20fgHguQDKCZ5Ouq4b+px8p3Occ3ZXw//zJu2PdWpTxgsBIbA8CIjgXR7rvORmqRvWa0H4WnVi//Ic+7GdTrLXgrf+63MGf77k5M9aUOymsms4wZWjC/OUSu6URT+PT5nZqxj8OhWjz/yMDW6+dpwl0rCTRuYIIvpVxSh/03Pyr4nUwQAa0w3rO6CgBDeDcUq3L/Clah8IgW3TVE5tLBTuGkCMErIQEAI9ICCCtwfQxWVnBHTT+k8AH61aedRz7EjyzvZa8CaNzIeJKMgfzL5/VGlqcsHLZ7ppbVMlbtUum+YP7x/XV/qdrVT0o6sfCiYBrATznzw3f2j0XioW9bT1JTDeUNG7zS8PxhVHv9hNGZlLmOit1Q9ZX/YK9hu7GdvjM5k9V5Rxb1DcgvmHnpt/STf9iy8hIAQGm4AI3sFev2UXfcq0zuLamUqg7Dn2UFQQep2Hd7Z4BHiL5+QXvHi3xhjPJsifqAgxfN1z7ZOjYtDvdnQz+x6AP1URXXy6V8hfEVfMupGZAZE6LhLZh6q4Yo3bbjKTOYLK1d1u5nWemz8kbp+N9nUjcy6IggtqPuH4DQX7e92OQfwJASEwuARE8A7u2i27yFNm9lQGX1md+LYya0+6w51QZywjab3c4Z0jYkFXeE7u9IUmlTSzZxD4CxXBS6/13Nw3IgEwAEZSpjXBQBbgLZgZ0j3v9nvjCHsknT1KY76xoqvx1qJr1/I7x+Gu723WfaPQM/Ffl5t62nPsFX0PTQIUAkKgrwiI4O2r5ZBgFiJQf0GNgBl/estjSqXSo1ES66XgbUXEpkxrHQNPDuauDWe8yfXqK/4l3+ZeVqPrijFWlEuZ2SsZfCoDm4Z4Wndd974lD3iBCdYVl/CHqbxPoVC4v9ss6j+AAPxpz8m/t9sxiD8hIAQGm4AI3sFev2URfSqVfS4n+OfB9SzwfZ6T3zuOifdS8OqGdQ0IJwU7imUaKRZzdzTOMTk6fiRp/ocBPLX67F7PsfeNg0U/2kylrUuZ8RYVG2v00tJk7gdxxJlMJnem4ZUq88CeYFzjufYr4vAzCDZ10/oKgEru4S4Xl6jnkzKtLzBwRvDng/D8YsFWmSKkCQEhIARCExDBGxqVdOwFgdXpJzx+mLepbASxn6XsqeA1LbVbvZNi7Dn2Dn8uV68e32N4pf+P2TVgLntuPrLzy71Y21Z91n2l3VG+5WZ+U+b4qxj+N4N+zC/03PzPmo1Zis+TaesVxPhWVWR2tbhEI8+kaW0k4PEAHvAce8+lyFvmJASEQLwERPDGy1esd0hAN62iujCvshHwtLa6VJr4W4cmFxzeY8HLtcDmE7y6aW0BsHOlD33ec3ILpiyLi08v7c6trsUfKbl5tdMdS9NN69cAnoWQle5iCaLHRlePj+8xvM2/J6gwB8x4jq0ygvSkzcm9S/QTr5A7qieBiFMhIAQGmoAI3oFevqUdvG5aamft+UrsQqNXeZO578Q5434VvKm09U5mXBhIXeIPFAv58+Pk0I+29XTmTWD6oopN07TDpyYnfhtHnKOj1oG+htuWM2s195SRfTUTB5chmeP9gNFsHevXvtexNItVngsBIdC/BETw9u/aLOvIdMO6FFQ5r0nAJ4uO/f64gfQyLdliYls3rbsB7MeE35UK9jPj5tCP9pOm9WUCXq9iK+88tMcdt8VT8Uw3sheD+G2B0KOyVSoU8v3II+6Y6ot7MPPakptXpZx70nQz+0WA31QV3z2NpScAxKkQEAKREBDBGwlGMRIlAT2dPgyc+H0lIxRuLjn22ijtL2SrH3d4U4b1FiZcGgh/zT+0ODn5p26w6DcfupmdAjgFxp2ea6+JI77Vq1evHF75mDvVZTUC/rvo2M+Lw88g2NRNSx0d2geMv3muvV8vY9ZN6y8AnqhimO+4Ty9jE99CQAgMDgERvIOzVssmUt20tlbPDm7xHHvBAgxRA+lHwaublsrWcMBy3t1Np9OPm+ZENS0Y/9Bz4qmwNSfPM+EUr2DXSldH/ar1tb36ksoE+krRyZ3aq4BHLGs/bQbqGw4w8PuSYz+9V7GIXyEgBAabgAjewV6/JRf97IUhdVbT18ypqQm3W5PsN8GbMjPHMOiG6vxf4Dm2Ss227Foybb2UGEFVLWI+p+jmL4gDgm5mbgXoCQD9c4VWXjM5OflwHH763WbSGLuASAuOEBG0Vxediat7FXP9nwEmuqBUyJ3Tq1jErxAQAoNNQATvYK/fkoo+OTZ+JPn+TZVftPhc0bHf0c0J9pvgna1uxdjguXaymyz6yVfKtC5iIHgX4jpPqpvWswH8T3XeX/Uc+3X9xKCbsdT9Objfc+y9uum70VfSyHyYiD4U/JzpJM/NXdvLeMS3EBACg0tABO/grt2Si7zuF21Pypf2k+BNmtbvCAi+vtV8PmRqKr9uyS14yAklDWsdUaWynD+E/TfYtkqXFWlLmZnLGVQp57yMc++mzOzlDA449EOBB93M/gTgF1b+HHT3G59IXzAxJgSEQM8JiODt+RJIADUCNcHZq4sp/SJ4QbgGjJcHoqNLGSr6+S3UTaus9A6Ahz3H3i2OWHXTUmeEHwfgTs+J51JcHHFHaXNkbEzXfG2qonVxfdGxT4jSfju2dNN6EMBjGWSXnFy2HRsyRggIASFQ/X0qIISAEFAE+kbwzi4H/8hz8scu59XR09lngVkVgogtH2wqbb2IGT+u7O7SZzw3957lyFw3MzZAGQCbEzy9h+u66vJoz5pujL8Y5Aflo4nwiWLBPrtnwYhjISAEBp6A7PAO/BLKBKIi0E+Cl4G/lBw7+Bp/OTc9nf0AmM9VDMqsjd/hTuSi5qGb1lcAnKLs+szP2ODm/zdqH/1uLzWWfTX7lUITAL/Xc/Kf7nXMSdP6FgGvqAre5xcL9i96HZP4FwJCYHAJiOAd3LWTyCMm0EeCtydnmCPGGYk53cj8BkTPZKBUcmxVYjryljKtaQaGAGz1HLtavjlyN31tUDesTSCoFIA9LSNcD6nuKEtX0xP29UJJcEJACLRNQARv2+hk4FIj0C+CVy7nVN4swzBWl2moBFAirpKyo2Njz/R97TfBLiLw7aJjv3KpvdfN5jM6Om74mu9U+93sdanQy2JxjZrW+3zgE0Efxsmea3+92TzkuRAQAkJgMQIieOX9EAJVAr0qLZwyrEu5WkZZhdKrS3v99iLoaetkML5a0Tz0tJKT+7+oY0ya1vuoJqyIz/QK+Uui9tHv9pJG5oVE9JOK6O9toYkaKz1t3QPGvgAVPSc32u8MJT4hIAT6n4AI3v5fI4mwSwS6vcObNMZfCOLzCfyk+imK4K3QSJnZqxgc5MOdXqHtuXFi4oGoXwXdzPwAoBcruz7Tkze4OVXGdlk13bDeAMKXgg8WzB8pufkP9xJAKm1dwIyg8IUGPnXKyasz1tKEgBAQAh0REMHbET4ZvJQIdFPw1pdvbWQogrdCRDetLQB2BrjsOXl1xjbyppvWPwDsAeYtnpvvWhnryCfSgcH64g4MPrXUQ4G5enx8j+FtvgdgNxDWeQX7kA6mJkOFgBAQArMERPDKyyAEqgS6JXhHUtbRWgKqZHD1zx99HuC31xZCBG9wfne3Mg3/SzFhYKLk2AdG/aIaxoGjZSpXS1fTTz0nd2TUPgbBXv1OelyV7MJySJrW1QS8kggzPmuHlpyJW8OOlX5CQAgIgcUIiOCV90MIdFHw6unMm8B0GYAEmMsMPK/k5m/ultgelMVeY4xnE+RPBPEy/dZzc4dHHbtuZF8D4tplqP/0HPvjUfsYBHt6OnsLmAO+cVWyC8NhzrcejC97rv3GMOOkjxAQAkIgDAERvGEoSZ9lQSBu0ZkyrbMY+GwAk/CAVh62pqbW/139Z9y+B20BR8fGn+n7fpA9AcCk59iqIEKkTU9bl4LxlkBTM69VHzwidTAgxurSfz3kOfbuvQpbN607AByg7m16jp3qVRziVwgIgaVJQATv0lxXmVUbBOIUnSnTupqBWsqrrZo/vKYmdkXw7rhYc3Z4gfs9x96rjSVddEjStCYIyALse05+haptEbWPfrc3YmSerhH9rvIhjD7oFXLn9SLmpGl9goD3Bb4Zp3muHVyikyYEhIAQiIqACN6oSIqdgScQh+AdHR0/0k/4l4MxUgW0TfOHD6gXuyJ4d3x1RixrP20Gd9eeRH2uuXo5Sl1YAxH9sljIPXfgX+A2JpAyrHcy4cKK4NWe6hUm/tCGmY6GGIaxV5mG7wmO+QBSdKUjmjJYCAiBhQiI4JV3QwhUCUSZh3d09MADZhL++zTmt20HTDd4Ti5IgdXYovS9FBY0mUzuTMMrVZaGoEUteHUjcyyIflgRer3b2ez1WqVMax0DTybgz0XHfkov4kkZ1luYcGmwFIyzi65dKTghTQgIASEQIQERvBHCFFODTSCKHd6kkXkOgV4PwslzadDbPSd38UKEovA92PQX/xAQteCtz/VKhOcXC/Yv+omfns4+Cz7to3FZpeiqNloD+P+cmpr8VRSxjhjZJ2nEf64KzXcVXfuiKOy2akM3rNtAOJCZtwxhZsR13ftatSH9hYAQEALNCIjgbUZIni8bAp2KzpSRfT0Tf7kB2FbN52dOTeXXLQayU99LcZHiZKKbmVsBegJAmxO8bT/XdR/qF4a6Yb0VBPXhaL6/n9lzbC2KWHXT+iSA9wa2ND/jTU5ORmG3FRsjo5lnaBr9Vo1h4Hslxz6+lfHSVwgIASEQloAI3rCkpN+SJ9CJwGq4lKaUyhRA5xad3FVhwHXiO4z9QewTF5M16XQqwYmpKpP/9hz7eWH4GMb4i2dQ/lfJzf9PmP6t9hkdzR5V1vhcAuZU3tvBTkRnbXXTUmek9wNwq+fYi/tsdTIh+yfT1oXEeKfq7hOO31CwvxdyqHQTAkJACLREQARvS7ik81Im0I7ASqWtFzFwWd2ltGkCnR5W6NZ4tuN7Ka+FmltcTKq5kL+ofBDzOUU3f0EzlqtXr145vPIxm4JdV4YD4GYCF3kIf/by+Z81G9/seeOZ5cX703s9J/fpZjYXe64b2RNB/B3Vp5flhJOmtZGAxwN8n/f4fffHzTfPdDIvGSsEhIAQWIiACF55N4RAlUCrAitlZk9l8JXbAS58Ka0Z5FZ9N7O3FJ7HxUQ3sz8E+NhA8BI9q1jI1fL9LoitLlctNx41IOYPFd38RzthrpvWNIBq+WT6vOfkzqq3V6tAVv3Zzz3HfkFn/jLfAOjVVcF7RFy71ovFGHxYZPw46COFJjpZThkrBIRACAIieENAki7Lg0ArAqvxCAOw+KW0ZgRb8d3M1lJ5HheT7eKVt3pOfudmvBpyAtd3rxO//Fcw/tNz86pkdEutwf6PPMcOxPhcwZs9g8BfqP6Mp7c8vMvGjRtns1i05LCye+5XhfsWz7FXtTo+iv66aX0FwCnKlk/0og2F3E1R2BUbQkAICIH5CIjglfdCCFQJhBFY8+fV5Wc0u5TWDHIY381sLLXncTCpv1jIwDtKjv25ZtyS5naxyaCrAN6ZgJcBGA7GEh4E49+qdn6s+dr5U1MTwUWsMK3ePphe67m5bzSOaxTdnWStSGYySSpTNfsDXec5OTWXrreUaU1zZVd7m+fYO3U9AHEoBITAsiIggndZLbdMdjECiwmslGkdzeBTATphu432jzA0xhGHuBv01Y6DiW5mbYAzzFxO8JA+NXX7nc046UbmWhAFopDKPFYs5gvq31PpzDnM+BhAqmCCD2Z1RqKSQYGh+vwPE1xo2vrS5MRP5vOj8jX7ifIvwEhX7K8YKxb/GtiP6x1JmZnjGXRdYL9HOYiDtGvMv66gwvtLjq0yRkgTAkJACMRGQARvbGjF8KARaCz+sNOuW57CzCcQ6FiAU3Pn09kRhrjEzKAxXyzeuvXY7Dn2Lp3OLWVmjmFQcOSAgW+XHLtW6nlR07NHIAgPegV7j/rOI2NjuuaTEr3BediK6eCfOanDfN9/7oapyV/WxqZS2ecigdcw+HXb7dG9npPbd6FgovoAoJvWxwF8MAhW045aSIx3ynvRtU1nPwDmc4M+PUqJFuf8xLYQEAL9R0AEb/+tiUTUIwL1gkKlSmLCfIn4Q+XVbXUKUYmZVv32a/9qudm/V+O72XPstZ3GqpvWXwEcrL5CpzIOLBZtlW1hcbGbzpwGpitUJyIcXSzYN843oFq5Te2aVo45bG+Vc76EB/0yH7NhKv+7VCq1Oyd2+md9JwJmio7dOHaOoajeEd20fg4gSMVGZRopFnN3NOMQ9XPdtP4C4Ikg3OYVbLUm0oSAEBACsRIQwRsrXjE+SATqBcUOcROuJcZXi45duVUecYtKzEQcVs/MJY3MEUQUVBRj4Fslx35VJ8HM3d3lq0pO/vVh7Olm5u8A7cWAXXLsbLMx6qytRvxsgJ9NgDr+sgLAAwDUzvBDBP8dDE0J6CAjQyv5mqN6R2Z3rBkPeu7cHetm84vieSqdPZyZbwnWlnBRqWC/Kwq7YkMICAEhsBgBEbzyfixrAiPp7FEaYxzMaRBOa4AxDaIPJ/xtX3Ndd2OcoKISM3HG2E3bKTPzSgZdHfhk+ozn5t7Tif/Z3V3CA6uGtZGJiYlHmtlLmtnTCXx5IEwX2d1dyM7o6MF7+9rMjwA+tE70znYn4LNFxw6KLoRpUbwjesgd6zDxtNsnaVpfJiD4wCHZGdqlKOOEgBBolYAI3laJSf8lQyCZzCRpuHZbvXFaO+ZCjXPiUYiZOOPrtm09nXk3mILiCsR4V9G15zteEiqsObu7zB8qhcyZq5vWZgArlZN2syI0iN5AvlemFG7HuH6CUbwjSdO6n4A92/EfCnaITrqRmQEFF/0kO0MIXtJFCAiBaAiI4I2Go1gZQAJ1xQR2iL5dgdMuhijETLu++3FcfclZMJ3kublr242ztrvLwD0lx94/rJ2o1qQieqfvrffbzo5xp/HM2bEGn1B08teHZRFVvzkV3iQ7Q1RYxY4QEAIhCIjgDQFJuiw9ArppqfOhR1RmRjcwcGNdYv+2d/TaJdWpmGnXb7+O09PWd8A4MVgd0g4rFib+2E6so2nrOJ8RCDsmvKtUCL9THPWadJp1otN46qq5Peo5drBz3e2mm5Yqw/x85bdM5dE7CoVit2MQf0JACCxPAiJ4l+e6L+tZjxiZd2lEnwkgMC71XAhPky0AACAASURBVPtt6l87FRSdQO2l707ijmusblq/B/BUZV/zh/eZmlpfy9jQksukmXUJPArgTs+x17QyOOo1qdlr99uDTuKZW7iCrvCc3OmtsIii75wYmH/iufmjorArNoSAEBACYQiI4A1DSfosGQIjKetoLQGVi1W9+w95jr17bXKNeXjvvnudOsPZldaJmOlKgF12UleFq+3ddt3MvgfgTwWhE5/uFfJBerGwrd/WpD6ezSuHd713/fpNYecSpppbWFvt9tNN62MA/iMYz9qJnjvx3XZtyTghIASEQKsERPC2Skz6DywBJXYTQ/g+M4bAXPbcfJAaaj7B2+4uXLtw+k1ctTuPqMZ1yqOSx3fonkoVNN7iOflVrcbWaQyt+mvWvy6eOR/Umo1Tz3XDugaEk4K+M37G8yYnw4yLsI+mm5bKdLIfA3eVHHt1hLbFlBAQAkKgKQERvE0RSYelQCBlWG9mwsUA1O3wBzR/2Gr8mryXAqeXvvtxfTvlUX9Bi8FnlJz8F1udZ6cxtOpvsf6dHkmou6AZSdW6VufWkGbuPM/NBZXepAkBISAEukVABG+3SIufnhFIpTIHcYLWVwOYV+wGu2CmpVJGBU12eHu2XIHjTtciZVrrGHgyCBu8gp1sZzadxtCOz4XGdHIkoVOxHMU8Uunsb5j5mequmuZrmampCTcKu2JDCAgBIRCWgAjesKSk38ASqLudPq35w6sXugDVS4HTS9/9uLCd8Eilxw9l9v8QzMvn93pT+SCfb6utkxha9dWsfydHEjoRy83iCvN8dDTzFF+jP6m+xLip6NovCjNO+ggBISAEoiQggjdKmmKr7wikzOzlDA5upBPh+cWC/YuFguylwOml775btLk7vC2n0EqamcsJdDqYt/BMIlUqTfytnTnWr4nv8zM3TOV/146dKMZ0ciRBNzLXguhlwZ+BMo8Vi/lCFDGFtaGbmc8B9PbAP3BMXOW5w8Yj/YSAEFieBETwLs91XxazHhkb0zVfm6poXVxfdOwTFpt4L0VnL33328tQuXA2XEtDdrPn2GvDxphMJh9Lwys9AI8F07Wem6tc1Gqj1a8JCA/6ZT6mF6K30yMJs2KZ8KBXsPdoA0XbQ5LJJz6Whrc+WDXQ8oeXth3LQCEgBIRAAwERvPJKLFkCetpaD8ZBADYneHoP13W3DoLg3brbql3uXte9lGj99gIkjcwRRKQKg6gtwW94Bfu1YWNMmpnTCXS56s/MR5bc/E/Djm3sp5vWIwB2UZccAewB8EPs0+mlKfvb7dpsZ1wnRxL0dOY0MAXp2Nqp7tZOvPVjkmnrpcT4XmUttTO9wsQlndqU8UJACAiBdgiI4G2HmozpewJJw3ozES4Lfs8yn1N08xc0C7qXu6x1vh/xHPsxzWJdys91I/saEH89EK1EF5QKuXPCzrdWRhgMx3PtdNhx8/WrlASe+RHAh4LxIAj/FoTE9Maim7uyE9utjK0vwtFqSjHdzPwdoL0YsEuOnW3FbxR9k6Z1PQHHAfz3VSsSB0xMTGyLwq7YEAJCQAi0SkAEb6vEpP9AENBNa6aagix0ztJeCd5kJpOkMqmv4ZU8v85zcsF5y+XaUkbmbCY6v4KDz/QK+VC7gkkj80Ii+kmV2/s8x64UneigzRG9qi5fpWCJ+p/PFh37nR2YDjU0ZVpXM/DKaud7PcfeN9RAAPWp2Xqxu3uAceDoEJVtAMMEfLLo2O8PG7v0EwJCQAhETUAEb9RExV7PCeim9WcAT1K7ccNU3rtQKNwfJqheCd6UmTmeQddVBB590CvkzgsT71LtoxvZi0EclHsm8MuKTr7CpknTTetmAM8BsI0TPFbK50vNxoR5XhG908Xq8YbtQwjrfNLet2Fy4pdh7LTSJ2UeeAxT+RIwRqrjyp5jzymU0syeblqqUuBK1a/bafaUz6RhXUgE9aGgXKZy+o5CQTGUJgSEgBDoCQERvD3BLk7jIqCb2e8CXLmc5vOZ3lS43UHVvVeCVzetjwMIEvGzph1Vmpyo7VLGhamv7davIZF2WLEw8cdmAafGxg5hX6v0Y/qh5+Ze0mxMq891M/tFgN9UHfcvAJWy1AyV9eB/QHSb5vOdgPqn1mgN4P9zamqyciY5REulMmlOUF0lNLrBc3IvDjF0Tpdevc8qCMMwdqpePNwNwM89x35Bq/FLfyEgBIRAlARE8EZJU2z1lMCadPbwBPMtKggifL9YsI9rJaBeCQTdtH4O4HkqVs0f3mehPMGtzGWQ+9afWd28cnjXe9ev39RsPrphfRWEk1U/n3D8hoJduSgVcdPTmTeBqVa1TR1xUP9oTdz4mr9tv6mpqVrmiUW71x3HUW/y2z0npyoEttx69T6rQFNG9hwmrnxTwXSS5+aubXkCMkAICAEhECEBEbwRwhRTvSVQEwoMbCo59q6tRtMrgaCblrrIMwxgq+fYO7ca91LrX1coJNRX8avHx/cY3ub/o8oh9kt/1UIK/1tds2b4K+d+Q2abqD+OQ2V6frGY++9mDhZ63qv3OZ1OP26aEyr3sSrjLe90uwso44SAEIiUgAjeSHGKsV4R0M3MrQA9AcBMeevQ3nfccVst92fokHohEObmWMWPPcc+JnTAS7Rjq+uQMrKvZuJvVHcTX+u5ucq/x9zU2mnEzwb42QSoYzQrFnBZDsQf06KxpczM1QyqXFBr8TjOfH5b5RgVrvpUaAD+03NsdWRHmhAQAkKgpwRE8PYUvziPgkDStH5MgCpXOu1r/gs3TIY/L1nvvxcCoT4FF4FfXXTyV0fBZJBttLoOqXT2l8y8lsB3FZ386n6Z+5x8wpWg7tu8clhvPKIxOnrgAX6i/GkwqkUy2juz2zjveo5bN63a5e67u5PbWTesP4JwCKk/jwlOR3V5sF/WVeIQAkJgMAmI4B3MdZOoqwT0dPYzYH6X+k+VyqpUyH2gXTh1AmGL59ir2rXTyriUaX2BgTMAxP5VfCtx9bJvK4K3/rIaM19QcvOhc/Z2Y47JdPZ8Yj4bwD9B+FF9EY3R0fG15YT/OuLK2WPVCHikGFEe5lY4RsUilR4/lNn/Q9XejzzHPjYq22JHCAgBIdAJARG8ndCTsT0lMGJZT9Zm8KdqbtRfeI79/HYDSiaTO9Pwyi3V8b/3HPvp7dpqZZyetkqV1FP0B8/JPbWVsUu1bytCLWVa32LgFQA2+UMwN9j2Pf3GpXouV51HfnIttpHRsbWapjWmM4v0vGsrHKNiljIzlzPo9EC8E44uFuwbo7ItdoSAEBACnRAQwdsJPRnbUwK6aanb+2ondpPXxiW1+uBHLGs/bQZ3V3fZvl107Fqy/9jmWL38pAS7kgdXeE4uEAr92lJp63lUpuGpqdxNccYYVqgFRwG0sqvOzjL4ipKT72t+s2I3ZR2tJXBDXRGLKYDOLTq5q6LkGpZjVD5Xr169cnjlYzZWyjBT3nNyVlS2xY4QEAJCoFMCIng7JSjje0JANy2V1/QIdb1H8/GEqSn79k4Cqb88RkQXFFsoZ9uu35RhvYMJFwVyl+k1RTf3zXZtxTHOMLJPmgG/gIBREJ4NYEz52Urlve4OWcyjnbjCCrWUaX2CgfcFPphf6Ln5n7Xjr5tjRlLW0YkhfJ8ZqohEmUCnRS10a/MJyzGq+c8pCQ06o+TkaunbonIhdoSAEBACbRMQwds2OhnYKwK6mfkUQO+pCp3zPDcfFG3opI2OjT/T9/3fBNoJ9OaSk7u8E3thxqYM63tMeGkgeBM8VsznVQGDvmh62joZjK/OE8zDnmOrYgKxtbBCrdaPgIeLMccUxWRTqewaDPFUVew+oPnDVpw5l8NyjGJuyoZuWioH9uEA37dqRWL1xMSESrcnTQgIASHQFwRE8PbFMkgQYQnoY2MHw9fUpZidCPhz0bGfEnbsYv10w3ojCP8ViM8unT3UzcyMkrrKZy9Kvy7EQ09b/wXGG+d/Hv/RizBCLWVaRzPwIxUjM95Zcu3PRvEexGlDN7N/APhQALGL3aoAVTmAgxb3+zU6ah3oa7gt+PMDfLLo2O+Pk6XYFgJCQAi0SkAEb6vEpH9PCSRNax0B6vLP5gQPj7nuenVmsOOmG9mLQfy2wBBpT/QKE3/t2GgTA2GEXdwx1Ns3DGOvMg0rEXlY9efbGLguOHABflXwsya5ZKOINwwX3bR+CkCVq52e3vLw7hs3bqxdOIwihFhsVASvqqY3dEycO7u14MNwjGqiSTNzOVUuq5V9zTc3TE56UdkWO0JACAiBKAiI4I2CotjoCoFU2noRM34c6C7mD5Xc/Eejcrz961iAyjRSLObuiMr2Qna6KUiazSU5mjkFGn2GgD0DvsBnS479TvXvumF9H4SXBJ8FusCmGRfDMFaXafjOyocTfN0r2LNpvZrNczk9b8YxKhbVymp3VQpv8E89J39kVLbFjhAQAkIgKgIieKMiKXZiJ6Cb1kMAHsPAXSXHjrTAgG5afvXWfNfy4XZLkDRbmBFz/FUa/O0X5kg73StMXFEbN1vql/lfnpt/bDN7nT5vxkU3Mp8BUSX3MuPpJdf+fac+l+L4ZhyjmnPKzF7F4NdVPn/wCUUnf31UtsWOEBACQiAqAiJ4oyIpdmIlkDSyZxPx+cqJ5vMhU1P5dVE5nFveN/4zqnVCcvaM5eaVw7s2VuCKan6L2VFZA+pTZJU1//A7Jid/WxuTSmfOYabzgv9m7SWeO/HDuONaTKgdMDa2/7CvbWAEWQ4izVsb97y6bb8bgjeZHN+Xhv1a7uN/eY4d+weibnMUf0JACCwNAiJ4l8Y6LulZjI+P77p5m78hyO/J9FPPzUX6lWnSzJ5B4C9URB291nNz3+gG0DpB8pDn2Lt3w2e9j/oUWQTMbFuh7bNxYuKBWh/DMHYr07A6i7kHGI7n2uluxLiYUNPNzOcBOlPFQdBeXXQmln0p5oXWpBuCtz41HDHeWnTty7rxjogPISAEhECrBETwtkpM+nedQNLMfI5Abw9ETtk/tFicrBZriCYU3bCuAeGkQO9OD1ml0m35aCwvbKVXu8q1iBrywc6bNSBpWBcSITjHS6A3F7uQqk35Wkio6fqB+2CoXAKws0p95Tn5veNep0G2H7fgrVYnVGepHwfA8ZzufCAa5DWR2IWAEOgdARG8vWMvnkMQWD0+vsfwtnIRoN2J+ZdFN//cEMNa6qKbVlmdlACwxXNsVbkt9tarXWU1sdHR8Wf6mq9ypqo///OKXcMwdirT8N8B7EaAW3RsM3YoVQcLCbU5ZWs1ek1xsr8KdXSLT1g/cQte3bQ+BuA/gnjYP8lzJ68NG5v0EwJCQAh0m4AI3m4TF38tERhJZz+gMZ8b/E5lXlty8ze3ZKBJ51TKMjmBasEH/qbn5F8Tpf2FbNXvKncj80EtjmT6oAzxjKpKp/L/btP84QPmS5GlpzNvA9PFwTjiN3mFfJCjuBttPqGWSqV258RO9wEYJuCuYsSXFrsxr277iFPwrjnooH9LPDqjjruoozg3e469ttvzE39CQAgIgVYIiOBthZb07SqB8fHxFZu3+So92D4A/uA59lOjDqC+HCrAb/ecfEXkxdx003pUFc9QbuIuCjA7lSOOGNLvulflLVY8y742bG6YXD9vvlTdtGaqovhRz7FXxoxjjvn5hJpuWFeAcFovBHg35x6lrzgFr25mvwvwCQQ8wjMJw/NuvzfK2MWWEBACQiBqAiJ4oyYq9iIjkDKyZ3M1MwOBjy06+aCyVpRNN6xLQHirskmkHVYsTPwxSvsL2YpTjCzsM3MrQE9QO7tEOK5YsG+cr2+vzxc3sqmeFd1UPXZyh+fYI91Yo0H3Uc8xyiwgqbGxQ7hS7ZCizoc96MwlfiEgBPqXgAje/l2bZR1Z5eyur3aNhkB4wCvYQUGEqJtuWn8B8EQlAj3HDnZcu9G6LXiTpnUdAccHNSWIz/AK+dk8u43z7eX5YhVLIxvdzH4R4DepZwy8o+TYn+vGGg26j7iygKRMa4KBbJSlvQedtcQvBIRA/xMQwdv/a7QsI9QN6w0gfCmYfEypwlavXr1yeOVjNlcB3+I59rO7BbubgjdpWE8jwu8ql9ToYs/JBRkvFmq6mbkWoJep51TmsWIxXz3j3B069Ww0f3gfX5uufV3+sOfYu3UnisH2EtcufV1auGlo/kHe5OTkYJOS6IWAEFguBETwLpeVHrB56mbGBigDwPYcOxtH+COjY2s1TftlIOwInygW7LPj8DOfzW4K3lqFOjD+5rn2fs3mWMtawYwHS669R7P+UT+vZwNAZQL4z8CHr53uTW2vABe136VkL45d+qqIvlVdHATjS55rV85USxMCQkAIDAABEbwDsEjLLUTdGH8xyP9BMG/CKV7B/locDFJG5mwmCqq3+YTjNxTs78Xhp5eCN5W23smMC6ui/uiFzu3WYtTTmdPAFBx3IELT/nHwmit4+e8A7Q3GbZ5rHxyHv6VoM2Va1zNwXLCOZRopFnPq8mdHTTezNsDqQ+jdnmM/viNjMlgICAEh0GUCIni7DFzcNSeQNK3fEfB0EEpewdabj2ivh25mfgjQsWr0tK+ZG6cm3PYstT6qXtRt3bRql7vvXlc7WtG6sUVG6KZ1N4D9mPC7UsF+ZjPjSdO6n4A9GbBLMe2sN4uhYYc36O5r2nM3TE4Eu/HSmhPQTWtbsBNL2OQV7F2bj1i8R8ocP4HhfzfoFeOH0E7jlPFCQAgIgYUIiOCVd6OvCIwYmadrROq8qbpddVbJyX0+rgDrCk5s9hx7l7j8zGe3G0caUob1FiZcGmgUzT+0OLl4hTrdtN4L4JNB/x7t7irf8wjeX3uO/Zxurs8g+0oamSOI6FfBnyHCRaWC/a5O51N3LOYOz5UsGZ3ylPFCQAh0n4AI3u4zF4+LEKjl9wTwCJW3ri4Wi/+KA1hcl3rCxtoNwaunrbvA2D/s7m5tV5CA+4qO3bOyvfVsmLnM0A7d4OZUNg1pIQgkjcyHiehDqqtP9KINhdxNIYYt2CWZzn6YmAN7ms+HTE3l13ViT8YKASEgBHpBQARvL6iLz3kJjFjWftoM7gwKHjCu8Vz7FXGhiuNSTyuxxi149XTm3WD6tIqJgGOKjv3jxeJLGtb3iPDS4PSAn0hOTd2u1mFOS5mZY0DkNzsH3AqH+frOEbygr5Sc3Kmd2lxO43XTygMYA3Cv59j7djL3dDr9uGlOTKkS02D6qefmjuzEnowVAkJACPSKgAjeXpEXvzsQqN+ZgkYneZO5a+PCpBuZ74LohEAQRnSpp5VYYxe8pjUd5DAG7vEce//FYksZY69n0r4c9GE+z3PzH2zsn0yO70vDvjoPTHGmKpu7897FKnStLF4LfUczmaeo7lP57uyKJs3sUwn8+8oHHb6q6ORf30K4O3TVzey1AAcp6mR3txOSMlYICIFeExDB2+sVEP+zBOrO1G7xHHtVnGjqSvt2teBEbU5xCl7dtP4M4Elqt3aYyvsUCoX7F2K51/j4rrtu89Vu7mNBfJtXyM+bCUHfLqCVlPqp50S/01cVu38CMFvKuGtll2N62arvtCofnYjJxRyzSSNzNlUzjzDhlaWC/e12/Y4Y1pM1gqo8qMnubrsUZZwQEAL9QkAEb7+sxDKPo5tnaut9EeOmomu/qNv44xK8umF9G4SXB/Px+UxvKn/JYnNLGtbtRBhXiSrKrD3xDnci19g/ZWYvZ/Dpwc8Zm0FYtdBOcLscq2uihPqcaneDLHh103oEQHAZslvz0E3rFgCHq/XkaW1NqTTxt3bXRDet/wXwNDVednfbpSjjhIAQ6BcCInj7ZSWWeRzdPFObTFsvJUY15y5f7Dn5RSuPxbE0cQjeNens4QlmJXjULuwNnpN78aJi18y8jkBXVXQsvl1y7Fc29h8ZG9M1X1NnOHf8uyKiCngNxxhUOq0VtTjiTNkWx7rW24xjjRddz8qxE5VvV6Uju8UrtF85UJ3XZtANVX8/9hz7mLh5iX0hIASEQJwERPDGSVdshyaQNK3raTZRPtLFou2EHtxix6RhvYMIFwWykPGuomsH/97NFkce3rpjBzOeYw83m49uWjPVC4KbPXf+tGx62loPxkEANoPxPhBmd4xVBoWZnRJ7b5yYeKCZr4WezykyUr1k1W2h2G7szcZ1ex4pI/tqJv5GJS56r+fkgkuL7TTdtP4PwGEAHuUEW6V8vrSQnWTyiY/FTtueo/HMP4qFwm/a8SdjhIAQEAJxExDBGzdhsR+KwGw5W+CBkmPvGWpQm52SaetCYrwzGM50kufGdzluQaFnWlx7FsXX3clMJkll8qpi5zrPyQUXjRZqc3ZVmb7hubnXNvbVjQPfACp/qfJzfq/n5D+tm9kvAvymWt8EJwzXvV3tALfcxsfHV2ze5m+tDpwtId1todhy4CEHdHseSdP6FgFBZhOf8ZQNrq2OiLTc6vP4gvkSz82fOZ8R3cieCI1PBOPE2eeE28B8s0/aTZ2mQ2s5cBkgBISAEFiEgAheeT16TqD+jGg3zgrqaes7tV/SRNphxcKEupjT1Ra1GEqZmeMZdF0wCaIPeoXceYtNqNkREsMw9irT8F3B1+PAnIt9dZcLwcxrS27+5nbg6aa1BcDOzNhcqtthjppNO7FFMabb86jb4e/oImbKyP6SidcycFfJsVfXWCTT1is0n/cHaQcx81Eg7FPPiYhmmFllBqk1l8C3kK9dOzXVWS7gKNZDbAgBIbC8CYjgXd7r3/PZ158RJeD6omMHqcLibLqZ+QNAhwbasAcpyZTfqMWQblofBxCkE2NNO6o0OfGTxRjqpvULAM+tMFgxViz+tVDfv75KG5he7rm579Se1/PzkchucG63W10vPZ05DUxXVPQ5jq7P7Rs1m1Zji6p/HMdWFoptdHTc8DW/dgzoZs+x17Yzj2Q6cw4x1T4sqfPgDzGQJAQXG1to/HeAZouXMPDZhJ+4cL78zi0Yla5CQAgIgbYJiOBtG50MjIJA/RnRBE/v4bpu7SvuKMzPa0M3LT+4hEV4wCvEe3xioUlELep00/opgBcEgnd6J71UunXBM5cjKetoLYEfVWKjez0nt0NxgqRp3V4VOXd6jr2mfh66af0VQJC+rLzz0B533Hbbg60uVtK07idgTwbskmNnG+xHetyj1dii6h/1Gi8W10g6e5TGfGNlSXGZV7Df2so8DMNYXaahswB6T7hxvBFEP2GfvzmEmQlN07hMNOb72kuA4IhDsmpHreX23zOML5FWvqJYKHT9W5Vw85JeQkAILFUCIniX6soOwLz0seyJ8DnYOSTiDxQL+fPjDjtlWkczKmKPQWeUnNwX4/Y5n/2oxVDdMYNHPceezWPb6FuJ3cQQvs8cFKXw58sPO+cM5zypzXTTUqmugq+z2zl/rI9m3gaNLq6s+9zdXfWzqNn0Yn27PQ/dzJ4J8Oer7/VZJScX/HuYlqxk6/gkgL2a9N/GzOrP6M2LHWPZ5+CDd1n16LZTwXRh9UiMMqtKhO9esz+95eFVGzduVEdapAkBISAEukJABG9XMIuTBURfrRrYVs+xd+4Gpbqd0OnpLQ/v3qtfulGKurAX1qo7uz8IMjMAD2j+sDU1tf7vjdz1tPV1MF4D5i3TOyVWN2Zh6DT22tldEP7hFezH7eA/4gt93XivuvGhZrF5pEzrIgbeEfRh7SWeO/HDZvNW7402o13GxEfV9yXgLjDOY2gbGeWH1DNK8D3e5ORkM5uNz1Nm9lQGvwvAgfXP2vmg1Kpv6S8EhIAQaPi7TYAIge4T6GahidrsKl/bDquqYupL1q97Bfvk7s+84jHK851hLqytSVvPSzB+Vv16eUGxm0qldufETiqX624A/9Bz8uor6jmtE8GbMjJnc7USGBE9r1jI/XeU9vWxsRdTWRsFsNUH762B/sXAQwA9xPAfooS2F8/wDDTyEv6QN5/gj+qd6IRTqzGkDOt7THipGhcmQ0PKyH6EKRCiu1Z9qRR1QyBsSvjDGdddv7HVGBbrXxW+V9b6iOCNkq7YEgJCIAwB2eENQ0n6RE6gWZaAyB0CGBm1PqZp+A9lmxlPL7n27+PwE8ZmlGKo2YU101QJd4duD0rEAts0f/iAhYRevTAhptcU3dw3oxKkj89k9lxRJjcoYwz82nPs58zHql02upF9DYi/HoZ/XZ8HAJoC+7cMa/6FhUJBZaaIpLU7j3ac66b1FwBPVGMTPL2367r3zWcnlbJMTtC1AD+h7rn60BFcYGTmD5Xc/EfbiaHZmG7yaBaLPBcCQmD5ERDBu/zWvC9mrBvWNSCcFASjDWe8yfUtf13a6kRmv0oHlz0nX58+qVVTHfeP8pd//YU1zR/ep1HM6qZVVJvK6o6Z5g/vv9iupm5avwPwdACbpldoa+YrKtFu7Lpp/QrAEc2KGbRjP2VaVzOwQ6W4BRZKXaTawioLc6WqW61IR6iCHWEXv34em1cO73rv+vWbwo5ttZ9uZmbUwQM1br7d09HRAw8oa+V3Ue3YQ8VBCey/F6RdU/0wdIfn2COt+g7bv511DWtb+gkBISAEmhEQwduMkDyPhcBsoYmGHKyxOFO7u5a1nzaDu5V9BiZKjj3nTGFcfheyG+Uvf920VGaLoBxvo9hJpTO/ZKYgRVWChw9Y7KtqfWzsYPiaysAABl9fcvLzpohrJ3Y9nXk3mILKX0z+J0qFybOjYDNiWkdrhEvBqAk1JWY3ALgHoCEQ7w4OLkupf5qeE4/yq/Z2OLX7Hi7ka3R0fG054b+OGA3Hd/jiVSsSH9i8zVcfhtRltUjF/nzz6CaPdjnKOCEgBJYuARG8S3dt+3ZmvTi/OzqaeYqv0Z+qgvfGkmMf3UtAUf7yX8iWbmY/B/Dbgzn7eGVpyv72YnNOGpnziagiRBepQFfvTxUKqbfJjBHWWFNnZgHaB0T7gPn5oGrKNGZ1w3/RHLFh2czJJhEEQTd4Tu7FzdZVVXh7ZK1cZAAAG3JJREFU9FHah4em90FZO4yVYK62qATv3He8vWwWzeZR/3w+ZiOjY2s1Tftlg51Nmua/cGpy8re6aakLaY9Rz/0h7L/Btu9pxWerfcOua6t2pb8QEAJCIAwBEbxhKEmfSAn04vzunItdwKc8x35fpJNq0ViUv/zns5VMZ04hpq9UwqKmpYZVr7CVuur9tTZtvs9z8rPFCBYaG4ZNNePEDdtzvNLbPScXpDprtYXx16rNOe94m+nbWvHZOIdGPgRMAXRu0cldVV3r2VzHmqYdPjU58dtW/LXTNw7O7cQhY4SAEFieBETwLs917+ms68/vdqvSWdK03kfAJ9TEGXxqyclXxWBvUET5y7/RVjUbhcq0oP58P+I5drCLt1jT09lngfnXFT54f8mxVV7WeVt7gpc2ek7ugGZxNIqxrZtW7XL33es214+rzyVMhBkq89OmpvLrwtier0+Ua1Gzr5uWEuPH1P47qp3jMGviI3FCgsrXVHMtlwl0Wk3oNvItszZ+hzuRa5ddK+Pi4NyKf+krBITA8iYggnd5r39PZq+b1qMAdlLO4xYCswLEsP4LhDcGgo557WKJ87sBJcpf/vW2Vq3Qdtq8zb+/+lV1eRtPJ+9y3aYppvR09gNgPjeYu+ZnFsu5qpvWIwB2WYyTOidNhBwBRYB+VCzkfhOW62Js1oxmTkto+EL1gtaC6dXC+moUgFG9j3WFQIJQorLbTPAy86NE6hJecIFtDh/dGD8W5M/m551eoe0536XEVti10jfKd74Vv9JXCAgBIaAIiOCV96DrBHrxi68uQ8CSFrwE5BhQpXp9Ar+k6OSrJYQXX+bZtFaE27yCHZQN7lWb7/1QWQZ8Kn8IhDcEcTEe1Hg4E0Ue3ajfR920/gzgSfX84hS8jeeF5+Ojj42/GL6vio6ottV7/D674uabVe7drrWoOXctcHEkBITAkiAggndJLONgTaIXv/iWi+CtvQmt7GKn0tnDmfmWQEcSLioVbFWQoGdt7qU47bllzT+ZgFPqAlo0l3CrgUf5PuqG9W0QXt4YQ5yCt/G8cGOuZcOwnlwm/LGWh3lbgve/K5//R6ucOu0fJedOY5HxQkAILD8CIniX35r3fMa9+MVXL3gXS8zfLThRMmg8U0vEHygW8ueHnctsKeFgW5hetKGQuyns2Dj6NTkjvFXzh9dEsbNbiz2qtdCNzLEgqh4Z4BsAOrbmI07Bq5uWOnv9rKqvHXIt64Z1Dwj7EjAzQ+WxOwoFlYqs6y0qzl0PXBwKASGwJAiI4F0SyzhYk+jFL766DATTnmMHOWt72aJkMFcg0nc9J3di2Lklk+P70rAfpKMi4JFiiAtuYW23228+wduYZaBd2/ONi2otUqY1zao8L2Oz59q7RGV3sblWszHMHltpLDyiG9afQHgKwGUfePkGJ39dlOxasdUNHq3EI32FgBBYXgRE8C6v9e6L2Xb7F5+uH7gPhsp/q07+Zs+xF80D2w1IUTEYMTJP14hUdbSgtbqTmDSyHyLiD6uxDHpzycld3o35L+Zjhx1r1s4uuhNBho04WhRrUf8NguZr5tTUhBuF3cXmm0pl12CIp6rZGHZY/5SZvYrBr6usLb5Qcuy3xMEvrM3Gy5UTExPbwo6VfkJACAiBTgmI4O2UoIxvmUDcQqAxIN3IvABEPw1+TrjMK9hvbTnoiAdEwWBNNjuemObb6i+ftiJ4VQGGzdt8lb5sHxAKXsEei3iabZmbI3iZLvHc3JltGQo5qNO1SI5aryAN3wrc1cXbqd1m4etm9g8AH1rfr7b+ejrzaTC9u/rsD55jP7WZvbifzz2bnVgzNXX7nXH7FPtCQAgIgRoBEbzyLnSdQNxCYAfBa2bfA/CnAr3LeFfRtS/q+qQbHHbKYPX4+B7DlfRjc/4MtyJ4U4b1llqVMQKfUHTy1/eai/LfKZtW59CpP93IzIAo0XgkpFO7zeZREbyByp4VvWr9U4b1TiZcWB3veY6damarG89107oLwP7Bn0PNP7Q4ORlUPpQmBISAEOgGARG83aAsPuYQiFsI7CB4jezFIH5bRRvQSZ6bu7bXS9IpA9208gDUjqxfvX0fTKkVwaubltrdPQCgP3pO7rBeM6mK3Z8BeH4tllbm0278nazFfEcZanF0YreVudT7YeB6Ao6vjt/sOfai+ZJb8dNpXz1t/QmszhMHiveJXmHir53alPFCQAgIgbAERPCGJSX9IiPQLSGwXXhkvwvwCcHvWdIOKxYmVIqmnrZOGCSNzK+I6Ag1gWlasXqYt80WlggrEFOG9WYmXKZs+L7/7xumJn/VUyAAkmPjR5Lvz8kQEXY+ncTe7lqkzAOPYZRVRTV1SPZSz7UrH6qqrV27rc5lvkt+KjXZNJVTGwsFtavaF003LSVwgxzPCZ4+wA1REKUvApcghIAQWBIERPAuiWUcrEl0SwjUCY/fAwjOMDbeYu8VuXYZJM3slQQ+VcVN0F5VdCa+1Y6tWtYKBu4qOfbqXnFoEIibAKyq/1k/C966zB/z7qS2sy6trsOImTleAzVkXuBvJnjmDa7rbm3VXpz9ddNS30ao3zkPeI69Z5y+xLYQEAJCoJGACF55J7pOoBtCoH5Ss+miWvzKP04w7TAwLOvJ5Rmoc48E5m96bv41KsZWbdVVAvOHqbxPoVBQZ4F73ubbqey24OXpLStLpZIqfb1o09OZ08B0RfDBg3B0sWDf2Dig1XVp5rPxuWEY42UannNpkQmvKBXsa1q1FXf/lGkdzUCQPo1BZ5Sc3Bfj9in2hYAQEAL1BETwyvvQdQJxC4FuC492ALbDQDethwA8BkDJc2y95nfu7Xc+ZHs82t4YKjNm6L7az3xNO6d2vAM+n+lN5S9pJ/44xvRe8NJGz8kdEGZuumkppo9jwC45tirlvENrZ43D+K71qf8gV/tZNz4gtBJj3TuqsqS8QJ3Cmd7y8O4bN27c0o4dGSMEhIAQaJeACN52ycm4tgnELQSWouDVjeynQazSTPlURqZYtJ35BG/YRSHC94sF+7iw/bvRrxeCd7FLZ/PNeXT0wAPKiZmvENO/q+c++IQNC2S3iPM919OZm8B0ZGOM/Sh4DcNYXabhSgoywte9gn1yN94n8SEEhIAQqCcgglfeh64TiFMIzDeZbvsLA7SVmFSBAU6wysqwksFXlZz86+t9LHBpabEwHvH6oKLaYh9Mas/iFHBzLsk1yc+cNDLPIdDrQdgu1oge9Aq5PRYC3coah3lnan1SpnUZA2+u6Ed8joGzas9WrdB26reCDkkjez4Rn12N8TmeY6tSyNKEgBAQAl0lIIK3q7jFmSIQlxDotvDoZDVbYaCblhIIzwLhgVXD2sjExMQjDYJX/XfY9FObPMfetZPY4xrb7R3euktn2zzH3mmheaWM7OuZ+MsNzxcdE8d7nhwdO5K0xEfr8u7e7zn2XvXciPmcopu/IK41aseublozKjEDgH96jv1v7diQMUJACAiBTgmI4O2UoIxvmUArYq9l4/MM6La/MDGHjUkfy54En4NLSMz8oZKb/2gY+4PYp5uCd40xnk2QP1HhRFd4Tu70+ZilTOtqBl5Ze0bAFEDnFp3cVc0Yh13jpnbS6ScQa2cz6BV1fWd36XXT2qx2/6vP7lu1QlvdL7u8KTN7OYMDtprPh0xN5dc1m688FwJCQAjEQUAEbxxUxeaiBKISAmExd9tfmLjCxpQ0rY0EPJ6Be0qOHVSpWqptPsG7ddOqXe6+e50SdJG2pJk9g8BfCIwyvdZzc9+odxBkFSBcCsZI9efTBDo9jNCt2Qm7xgtNLJk8KEPD028GSB1fGK7285n5I/UffFJm9lQGX8nAJgJ2IcZni679zkiBtWFsZGxM13xtqpLIAtcXHTvIhS1NCAgBIdALAiJ4e0F9mfvsVAi0iq/b/sLEFyampGmdRcBnq/b+w3Psc8PYHtQ+3dzh1U3rFwCeq1hRecVYsfjXQo1b0sgcQUR1hTjoBs/JvbhVrmHWeD6ba9IHpRI883pmPpuI1FGAWrsZRP/PK+RuaRynm5ZKT3bgrCie3mnPUunWf7Yac5T99bS1HoyDAGxO8PQe/ZYXOMq5ii0hIAT6n4AI3v5foyUXYbtCoF0Q3fYXJs4wMemmVVbfBBMwXXTsFWHsDnKfbgnekZR1tJao5IQF6F7Pye1b41Z9pqqnVf9upLd7Tu7idriGWeN6u6Oj1oFljd5G4NcB2H6mmLAOPn3Sc3PfWSiO2i5vkOZW7aj2+CyvPpY9ET4H8RLxB4qF/PntMJQxQkAICIGoCIjgjYqk2AlNoFUhENrwAh277S9MvM1iSmYySSqTV7X1Y8+xjwljd5D7dEPwpsyxsxj0GSDYOfU9x57dQdWN7EkgvlpdsCJghnx+WidnTputcW2tdCPzAhBOA+h49QGnbg0fJaa3Ft3clWHWVTctldt252rfnp7lrbsQuNVz7FpMYaYhfYSAEBACsRAQwRsLVjG6GIGwQiAqivX+wlbSisr3QnaaMUiZmeO5VjKW6INeIXde3DH12n7cgrea3m1DdZ4PUHnoCH9oZlRjPIfBawF6Qu2Z5g9bU1Pr/94Jk8XWeCRtHaf5OI6JnkHg0QY/dwO4zB/ClRts+56wMcxzlveiomu/K+z4qPqFvRAYlT+xIwSEgBAIQ0AEbxhK0idSAvVCoMza+B3uRC5SBw3G6vzd6Tn2mjh9hbXdTPDqpvVxAB9U9ljTjipNTvwkrO1B7Re74DWtaQaGgnoRgCrcMTYPq22aP3xAp2JX2W1cY31s7GD42gsBPhGgQxt9MzClMS6Fv/XKYrH4r3bWsfEsL2aQ8jy7JvLbMdnymGYXAls2KAOEgBAQAhEQEMEbAUQx0RoB3bS2AqicSZ3nhnxr1hbvrZvWnwE8SXkapvLehULh/ijtt2trjrhjvM1z7UtrtqoZAr4ERnC2lKdZL5XypXZ9Dcq4OAWvblo/A/D8Jiw2af5wKgqx2yh4m/j9GYOu33O3lVeuW7duupP1ajzLC8I3vIL92k5stjo2ZVrrGHhyME4bzniT6ydbtSH9hYAQEAJRExDBGzVRsdeUQMqwbmTCURW9i4tKhXi+dtXT1rfBeHkQkM9nelP5S5oG16UOO4o7/hlYux2ElwKc2h7G3EtVXQqvJ27iEry6kTkXRB9YYFK3MvB7jek323aimzZOTDwQ1eTr5hNcJGuwO83M52k+risW8yrDQmStLi+vuvSYAOi9npP7dGQOFjCUSlnP4wR9EmD1AVO1ez3Hnr0QGLd/sS8EhIAQWIyACF55P7pOoP7rejD+5Ln2Dl/vdhrUmnT28ARzNX1Te2mlOo1hsfG6YW0CYVWlD20GuPrv9aO47Dl59RX8smhxCF7DOHC0TGV1ZGY2ywWBvuIT/wDbdro5ztRdumn9A0BQejg4rgC6hZl/44P+vMHN/SWuRd0xrRqYCe8uFeyL4vA5OnrgATOa/14NfOasfeay5y6fdzcOrmJTCAiBaAmI4I2Wp1gLQWDOhSz1jf2Wh3fZuHGjumEeWau7JT7t9WlKr2Q6ez4xn73DpAnXEuOrRcf+cWRABsBQlII3OTZ+JJXLqhzzGwHae7sQw6Wea7+tWzjUBa4V2Hqf67r3dcun8lP3bs3uLhPxrwA6r1iwVQ7ijpuezj4LPp8GQsORCfq85+TO6tiBGBACQkAIREhABG+EMMVUOAINKbcA0p7qFSb+EG50816DdEu8uhtXuZBG9NGEv+1rrutubD7LpdejoURuMEHPsUP/HTU2NvaYbWXtJBBOAfCseQg97Dn2bkuP3Pwz0s3MZoCqJYfrv0XgizV/6FNTU7ff2Q4LtaPrJ/z3gbnxg8NWZj6y5OZvbseujBECQkAIxEkg9C+TOIMQ28uPQK2oQmXm0Z4xlFvig/k+1V24mp2A5vMh22ej7Y2hMmOGZndLfeIUNFoDn3QiOpHB23dz52Lo253+OFer4VuEuWeJCV8i4HtU5ntrMTBjhIb8LTOJxK31KdH2OfjgXXbZ4q9lKh8FxlvqYyZgCqBzWym7HOecxbYQEAJCYD4CInjlvegJgaRpXU3AK6vOb/Yce21UgehG5loQvSyQ0mUeKxbzs2Vjo/IhduIh0JBWqwMnXK4WlwCB3lx0cpd3YGygh1a/RbipriiFSnm2ewST2kagM0ToRkBSTAgBIRA7ARG8sSMWB/MRmLMLCzBPa/uXShN/i4LW7O4x4UGvYAeXhqQNBoH5dnlbiLwEpusB/6cg+iaAxzFglxw724KNJdu1ylYVojiwk0nKjm4n9GSsEBACvSIggrdX5Je537nnbIP8ZJd4bm77Le82+ejpzGlgukINJ8LRxYJ9Y5umZFiPCMx3eW2RUKYJuIyB6z3H/rXqlzKzV/L/b+/+XeOs4ziAf54L/kIXcegilgtNm6hrMxRx6SZYROkkDv4DopsuLmKho0h1dCkuBUVxEYcitIsiuLRXc01joS4OYhE7xPYeyfX6K0bzaULN8+m9OoXyubv39/XJ8CY89zzRvr728yjaVy4Oz322Q0fp5MdOiu/afZ8n1/emY15uonnLX3TTXgYJEOiQgMLboWVMW5T+3MJqRDxw49y9Xu+55Z/OnN6OQ39uYe36Tn/Z2w7iDr92s6fQ/Ve82bmFT9sbl8o08dvK0uCJHT5O2Y/v95/dFQ/+tasdzTxytbl66dLS0i9lDyM4AQJTL6DwTv2vwM4B9PfMvxhN8+X125SOb8y/5Wt5x98c7137JCIOrp2o18TLy0uDz3fudD55qwJbKbyzexdeaCM+ijZ2Tz53Kr+ktlVzryNAgMD9LqDw3u8b7vj5+nPzxyOaVyPiSsT4QQzvrgwH72Vjz84+fXA0077WxPhWVNf/uXY3y9fJubstvP+87rd7DxrpJLRQBAgQmCIBhXeKlt3Fo+7et6/fGzXDG9+onxTWY71rM0c3u0/o7OzsU+3MQxfXnWt1ZTh4qItnlSkncHvh7Y1Wdy0vL//6b6+84xKG6788b6wMz36Y+yRTBAgQIDAtAgrvtGy6w+ec3Tv/Tts2R267tGGcto34ONreFz+fP/P1RvH7c/NXb916yr1AO7ziu4p2x5fWmji+sjRY9ySviP6efYeimTkW0T45efPV3qg9sLx87oe7+jDDBAgQIDAVAgrvVKy5+4fszy2sPVr44UnSdfcJbS83bXNyFPFtRPvjeKbXvN+0cWD8N7229/aF82eOdv+UEmYE+nsW/oxmfHnLtYiYibY90kZ8M9n7oaZtX4po+rfeyyUMGVczBAgQmGYBhXeat9+xs298n9BbDxDYKG4T8cGF4eDNjh1FnG0ITB6UcDL3Fi5hyDmZIkCAwHQLKLzTvf9Onn5SfNeuw3x0k4C/rwwHj3fyEEJtS2DdI3E3eq8rvVH7vEsYtsXsxQQIEJgaAYV3alZd76D9vc8sRoz2xygWo2kWI9r5205xZWU42KwQ1zu0xDcF+nMLf0TEYzf/o21ONzH6Knq9UxeWzp5CRYAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8WSlzBAgQIECAAAECJQUU3pJrE5oAAQIECBAgQCAroPBmpcwRIECAAAECBAiUFFB4S65NaAIECBAgQIAAgayAwpuVMkeAAAECBAgQIFBSQOEtuTahCRAgQIAAAQIEsgIKb1bKHAECBAgQIECAQEkBhbfk2oQmQIAAAQIECBDICii8SanDhw9/17bt/uS4MQIECBAgQIDAPRVomub7EydOLN7TD7lP3lzhTS5S4U1CGSNAgAABAgT+FwGFN8+s8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQUUHgLLk1kAgQIECBAgACBvIDCm7cySYAAAQIECBAgUFBA4S24NJEJECBAgAABAgTyAgpv3sokAQIECBAgQIBAQQGFt+DSRCZAgAABAgQIEMgLKLx5K5MECBAgQIAAAQIFBRTegksTmQABAgQIECBAIC+g8OatTBIgQIAAAQIECBQU+Bsshf4JxN0mVgAAAABJRU5ErkJggg==', '0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;1;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0'),
(1000077, '0000-00-00 00:00:00', 'Boat of Director', '-', 'dir.boat', '827ccb0eea8a706c4c34a16891f84e7b', 2, 0, 'dir.boat@gmail.com', 'default.png', '7', 1, '2020-02-25 13:50:47', 1000049, '', '0;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1');

-- --------------------------------------------------------

--
-- Table structure for table `portal_user_history_log`
--

CREATE TABLE `portal_user_history_log` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `ip_address` varchar(200) NOT NULL,
  `history_log` longtext NOT NULL,
  `method` varchar(200) NOT NULL,
  `user_log` longtext NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portal_user_history_log`
--

INSERT INTO `portal_user_history_log` (`id`, `username`, `ip_address`, `history_log`, `method`, `user_log`, `log_date`) VALUES
(1, 'Habib Syuhada', '::1', 'Update table eproc_material_catalog ', '3', '', '2020-02-11 09:44:22'),
(2, 'Habib Syuhada', '::1', 'Update table eproc_material_catalog H-BEAM', '3', '', '2020-02-11 09:44:27'),
(3, 'Habib Syuhada', '::1', 'Update table eproc_material_catalog FLAT', '3', '', '2020-02-11 09:44:32'),
(4, 'Sulis', '::1', 'Update table eproc_material_catalog H-BEAMM', '3', '', '2020-02-11 22:50:36'),
(5, 'Sulis', '::1', 'Add table eproc_master_vendor ', '2', '', '2020-02-15 12:09:30'),
(6, 'Sulis', '::1', 'Add table eproc_master_vendor ', '2', '', '2020-02-15 12:23:32'),
(7, 'Habib Syuhada', '::1', 'Update table eproc_material_catalog Laptop', '3', '', '2020-02-17 02:27:35'),
(8, 'Habib Syuhada', '::1', 'Update table eproc_material_catalog Welding Machine', '3', '', '2020-02-17 02:27:48'),
(9, 'Sulis', '::1', 'Import table eproc_master_vendor Short Desc :', '2', '', '2020-02-25 02:29:21'),
(10, 'Sulis', '::1', 'Import table eproc_master_vendor Short Desc :', '2', '', '2020-02-25 02:29:31'),
(11, 'Sulis', '::1', 'Import table eproc_master_vendor Short Desc :', '2', '', '2020-02-25 02:29:43'),
(12, 'Sulis', '::1', 'Import table eproc_master_vendor Short Desc :', '2', '', '2020-02-25 02:40:07'),
(13, 'Sulis', '::1', 'Add table eproc_master_vendor ', '2', '', '2020-02-25 03:25:19'),
(14, 'Sulis', '::1', 'Add table eproc_master_vendor ', '2', '', '2020-02-25 06:48:34'),
(15, 'Sulis', '::1', 'Add table eproc_master_vendor ', '2', '', '2020-02-25 09:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `portal_user_login_history`
--

CREATE TABLE `portal_user_login_history` (
  `id` int(200) NOT NULL,
  `ip_address` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `login_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_login` varchar(200) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portal_user_login_history`
--

INSERT INTO `portal_user_login_history` (`id`, `ip_address`, `username`, `full_name`, `login_datetime`, `status_login`) VALUES
(245, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:39:15', '0'),
(246, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:39:35', '0'),
(247, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:41:26', '0'),
(248, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:42:09', '0'),
(249, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:42:26', '0'),
(250, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:42:36', '0'),
(251, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:43:17', '0'),
(252, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:47:25', '0'),
(253, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:57:17', '0'),
(254, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:57:17', '0'),
(255, '::1', 'habib', 'Habib Syuhada', '2020-02-07 02:58:09', '0'),
(256, '10.5.253.8', 'habib', 'Habib Syuhada', '2020-02-07 03:02:10', '0'),
(257, '10.5.253.8', 'habib', 'Habib Syuhada', '2020-02-07 03:45:52', '0'),
(258, '10.5.253.11', 'habib', 'Habib Syuhada', '2020-02-08 03:54:16', '1'),
(259, '::1', 'habib', 'Habib Syuhada', '2020-02-10 01:11:24', '0'),
(260, '10.5.253.152', 'habib', 'Habib Syuhada', '2020-02-10 06:47:47', '1'),
(261, '::1', 'habib', 'Habib Syuhada', '2020-02-10 09:55:43', '0'),
(262, '::1', 'habib', 'Habib Syuhada', '2020-02-10 10:02:19', '0'),
(263, '::1', 'sulis', 'Sulis', '2020-02-11 10:11:31', '0'),
(264, '::1', 'habib', 'Habib Syuhada', '2020-02-11 02:53:30', '0'),
(265, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 03:08:35', '0'),
(266, '::1', 'habib', 'Habib Syuhada', '2020-02-11 04:03:55', '0'),
(267, '::1', 'test', 'Test User', '2020-02-11 03:58:20', '0'),
(268, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 03:15:29', '0'),
(269, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 03:16:03', '0'),
(270, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 03:16:58', '0'),
(271, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 03:17:49', '0'),
(272, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 06:50:02', '0'),
(273, '::1', 'habib', 'Habib Syuhada', '2020-02-11 04:03:55', '0'),
(274, '::1', 'test', 'Test User', '2020-02-11 04:07:56', '0'),
(275, '::1', 'habib', 'Habib Syuhada', '2020-02-11 04:42:19', '0'),
(276, '::1', 'habib', 'Habib Syuhada', '2020-02-11 04:42:45', '0'),
(277, '::1', 'habib', 'Habib Syuhada', '2020-02-11 04:43:23', '0'),
(278, '::1', 'habib', 'Habib Syuhada', '2020-02-11 04:43:59', '0'),
(279, '::1', 'habib', 'Habib Syuhada', '2020-02-11 06:27:47', '0'),
(280, '::1', 'habib', 'Habib Syuhada', '2020-02-11 06:27:55', '0'),
(281, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 11:09:58', '0'),
(282, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-11 11:09:58', '0'),
(283, '::1', 'habib', 'Habib Syuhada', '2020-02-11 09:32:24', '0'),
(284, '::1', 'habib', 'Habib Syuhada', '2020-02-11 09:37:02', '0'),
(285, '::1', 'test', 'Test User', '2020-02-11 09:37:36', '0'),
(286, '::1', 'sulis', 'Sulis', '2020-02-11 10:13:20', '0'),
(287, '::1', 'habib', 'Habib Syuhada', '2020-02-11 10:15:58', '0'),
(288, '::1', 'habib', 'Habib Syuhada', '2020-02-11 11:04:05', '0'),
(289, '::1', 'habib', 'Habib Syuhada', '2020-02-11 11:04:05', '0'),
(290, '::1', 'habib', 'Habib Syuhada', '2020-02-11 14:56:48', '0'),
(291, '::1', 'sulis', 'Sulis', '2020-02-17 03:27:17', '0'),
(292, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-17 01:46:41', '0'),
(293, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-20 03:27:56', '0'),
(294, '::1', 'sulis', 'Sulis', '2020-02-17 03:27:17', '0'),
(295, '::1', 'habib', 'Habib Syuhada', '2020-02-17 04:08:09', '0'),
(296, '::1', 'habib', 'Habib Syuhada', '2020-02-24 03:54:45', '0'),
(297, '::1', 'sulis', 'Sulis', '2020-02-22 07:22:10', '0'),
(298, '10.5.253.17', 'habib', 'Habib Syuhada', '2020-02-17 08:54:08', '0'),
(299, '::1', 'sulis', 'Sulis', '2020-02-22 07:22:10', '0'),
(300, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-20 06:01:30', '0'),
(301, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-20 06:39:23', '0'),
(302, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-20 06:39:54', '0'),
(303, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-20 06:40:57', '0'),
(304, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-20 06:42:01', '0'),
(305, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-20 06:42:18', '0'),
(306, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-20 06:42:58', '0'),
(307, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-20 06:43:13', '0'),
(308, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-20 06:43:43', '0'),
(309, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-20 08:34:04', '0'),
(310, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-20 08:34:12', '0'),
(311, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-22 03:26:41', '0'),
(312, '10.5.253.152', 'mahmud', 'Mahmud Amma Rizki', '2020-02-22 03:07:12', '1'),
(313, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-22 03:26:06', '0'),
(314, '10.5.252.250', 'habib', 'Habib Syuhada', '2020-02-22 03:26:41', '0'),
(315, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-24 09:42:37', '0'),
(316, '::1', 'sulis', 'Sulis', '2020-02-22 07:22:10', '0'),
(317, '::1', 'habib', 'Habib Syuhada', '2020-02-24 03:54:45', '0'),
(318, '::1', 'mahmud', 'Mahmud Amma Rizki', '2020-02-23 01:31:45', '1'),
(319, '::1', 'habib', 'Habib Syuhada', '2020-02-24 03:54:45', '0'),
(320, '::1', 'habib', 'Habib Syuhada', '2020-02-24 03:55:08', '0'),
(321, '::1', 'habib', 'Habib Syuhada', '2020-02-24 11:01:16', '0'),
(322, '10.5.253.148', 'habib', 'Habib Syuhada', '2020-02-24 08:35:26', '1'),
(323, '10.5.253.148', 'habib', 'Habib Syuhada', '2020-02-24 08:51:21', '1'),
(324, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-25 00:45:29', '0'),
(325, '::1', 'sulis', 'Sulis', '2020-02-25 00:58:28', '0'),
(326, '::1', 'habib', 'Habib Syuhada', '2020-02-25 01:24:19', '0'),
(327, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-25 00:45:29', '0'),
(328, '10.5.252.250', 'sulis', 'Sulis', '2020-02-25 06:51:24', '0'),
(329, '::1', 'sulis', 'Sulis', '2020-02-25 02:22:18', '0'),
(330, '::1', 'habib', 'Habib Syuhada', '2020-02-25 02:21:54', '0'),
(331, '::1', 'sulis', 'Sulis', '2020-02-25 02:22:18', '0'),
(332, '::1', 'habib', 'Habib Syuhada', '2020-02-25 02:24:27', '0'),
(333, '::1', 'habib', 'Habib Syuhada', '2020-02-25 06:34:03', '0'),
(334, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-25 02:56:51', '0'),
(335, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-25 02:56:55', '1'),
(336, '10.5.252.250', 'dir.boat', 'Boat of Director', '2020-02-25 06:55:19', '0'),
(337, '10.5.252.250', 'dir.boat', 'Boat of Director', '2020-02-25 06:58:31', '0'),
(338, '10.5.252.250', 'dir.boat', 'Boat of Director', '2020-02-25 06:58:51', '1'),
(339, '10.5.252.250', 'mahmud', 'Mahmud Amma Rizki', '2020-02-25 08:54:07', '1'),
(340, '::1', 'habib', 'Habib Syuhada', '2020-02-25 09:30:40', '0'),
(341, '::1', 'habib', 'Habib Syuhada', '2020-02-25 09:36:34', '0'),
(342, '::1', 'habib', 'Habib Syuhada', '2020-02-25 09:37:17', '0'),
(343, '::1', 'habib', 'Habib Syuhada', '2020-02-25 09:39:00', '0'),
(344, '::1', 'habib', 'Habib Syuhada', '2020-02-25 09:46:23', '0'),
(345, '::1', 'habib', 'Habib Syuhada', '2020-02-25 09:50:50', '1'),
(346, '::1', 'sulis', 'Sulis', '2020-02-25 10:51:15', '0'),
(347, '::1', 'sulis', 'Sulis', '2020-02-25 10:51:23', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `eproc_budgeting`
--
ALTER TABLE `eproc_budgeting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_budgeting_category`
--
ALTER TABLE `eproc_budgeting_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_budgeting_detail`
--
ALTER TABLE `eproc_budgeting_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_budgeting_transfer`
--
ALTER TABLE `eproc_budgeting_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_catalog_category`
--
ALTER TABLE `eproc_catalog_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_currency`
--
ALTER TABLE `eproc_currency`
  ADD PRIMARY KEY (`id_cur`);

--
-- Indexes for table `eproc_logistic`
--
ALTER TABLE `eproc_logistic`
  ADD PRIMARY KEY (`receiving_id`);

--
-- Indexes for table `eproc_logistic_detail`
--
ALTER TABLE `eproc_logistic_detail`
  ADD PRIMARY KEY (`rec_det_id`);

--
-- Indexes for table `eproc_logistic_doc`
--
ALTER TABLE `eproc_logistic_doc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_master_certification`
--
ALTER TABLE `eproc_master_certification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_master_vendor`
--
ALTER TABLE `eproc_master_vendor`
  ADD PRIMARY KEY (`id_vendor`);

--
-- Indexes for table `eproc_master_vendor_verification`
--
ALTER TABLE `eproc_master_vendor_verification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_material_catalog`
--
ALTER TABLE `eproc_material_catalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_mr`
--
ALTER TABLE `eproc_mr`
  ADD PRIMARY KEY (`id_mr`);

--
-- Indexes for table `eproc_mrir`
--
ALTER TABLE `eproc_mrir`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_mrir_material`
--
ALTER TABLE `eproc_mrir_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_mrir_material_document`
--
ALTER TABLE `eproc_mrir_material_document`
  ADD PRIMARY KEY (`doc_id`);

--
-- Indexes for table `eproc_mr_detail`
--
ALTER TABLE `eproc_mr_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_mr_quotation`
--
ALTER TABLE `eproc_mr_quotation`
  ADD PRIMARY KEY (`id_quotation`);

--
-- Indexes for table `eproc_mr_tabulation`
--
ALTER TABLE `eproc_mr_tabulation`
  ADD PRIMARY KEY (`id_tabulation`);

--
-- Indexes for table `eproc_mto`
--
ALTER TABLE `eproc_mto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_mto_cat`
--
ALTER TABLE `eproc_mto_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_mto_detail`
--
ALTER TABLE `eproc_mto_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_mto_rev`
--
ALTER TABLE `eproc_mto_rev`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_osd`
--
ALTER TABLE `eproc_osd`
  ADD PRIMARY KEY (`osd_id`);

--
-- Indexes for table `eproc_osd_detail`
--
ALTER TABLE `eproc_osd_detail`
  ADD PRIMARY KEY (`osd_det_id`);

--
-- Indexes for table `eproc_po`
--
ALTER TABLE `eproc_po`
  ADD PRIMARY KEY (`id_po`);

--
-- Indexes for table `eproc_po_detail_old`
--
ALTER TABLE `eproc_po_detail_old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_po_old`
--
ALTER TABLE `eproc_po_old`
  ADD PRIMARY KEY (`id_po`);

--
-- Indexes for table `eproc_priority`
--
ALTER TABLE `eproc_priority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_receiving`
--
ALTER TABLE `eproc_receiving`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_receiving_detail`
--
ALTER TABLE `eproc_receiving_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `receiving_id` (`receiving_id`,`catalog_id`);

--
-- Indexes for table `eproc_receiving_document`
--
ALTER TABLE `eproc_receiving_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eproc_term`
--
ALTER TABLE `eproc_term`
  ADD PRIMARY KEY (`id_term`);

--
-- Indexes for table `eproc_uom`
--
ALTER TABLE `eproc_uom`
  ADD PRIMARY KEY (`id_uom`);

--
-- Indexes for table `portal_department`
--
ALTER TABLE `portal_department`
  ADD PRIMARY KEY (`id_department`);

--
-- Indexes for table `portal_login_slider`
--
ALTER TABLE `portal_login_slider`
  ADD PRIMARY KEY (`id_slider`);

--
-- Indexes for table `portal_role`
--
ALTER TABLE `portal_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portal_user_db`
--
ALTER TABLE `portal_user_db`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `portal_user_history_log`
--
ALTER TABLE `portal_user_history_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portal_user_login_history`
--
ALTER TABLE `portal_user_login_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `eproc_budgeting`
--
ALTER TABLE `eproc_budgeting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eproc_budgeting_category`
--
ALTER TABLE `eproc_budgeting_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `eproc_budgeting_detail`
--
ALTER TABLE `eproc_budgeting_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `eproc_budgeting_transfer`
--
ALTER TABLE `eproc_budgeting_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `eproc_catalog_category`
--
ALTER TABLE `eproc_catalog_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `eproc_currency`
--
ALTER TABLE `eproc_currency`
  MODIFY `id_cur` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `eproc_logistic`
--
ALTER TABLE `eproc_logistic`
  MODIFY `receiving_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `eproc_logistic_detail`
--
ALTER TABLE `eproc_logistic_detail`
  MODIFY `rec_det_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `eproc_logistic_doc`
--
ALTER TABLE `eproc_logistic_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_master_certification`
--
ALTER TABLE `eproc_master_certification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eproc_master_vendor`
--
ALTER TABLE `eproc_master_vendor`
  MODIFY `id_vendor` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;

--
-- AUTO_INCREMENT for table `eproc_master_vendor_verification`
--
ALTER TABLE `eproc_master_vendor_verification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `eproc_material_catalog`
--
ALTER TABLE `eproc_material_catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `eproc_mr`
--
ALTER TABLE `eproc_mr`
  MODIFY `id_mr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `eproc_mrir`
--
ALTER TABLE `eproc_mrir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_mrir_material`
--
ALTER TABLE `eproc_mrir_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_mrir_material_document`
--
ALTER TABLE `eproc_mrir_material_document`
  MODIFY `doc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_mr_detail`
--
ALTER TABLE `eproc_mr_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `eproc_mr_quotation`
--
ALTER TABLE `eproc_mr_quotation`
  MODIFY `id_quotation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `eproc_mr_tabulation`
--
ALTER TABLE `eproc_mr_tabulation`
  MODIFY `id_tabulation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `eproc_mto`
--
ALTER TABLE `eproc_mto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `eproc_mto_cat`
--
ALTER TABLE `eproc_mto_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `eproc_mto_detail`
--
ALTER TABLE `eproc_mto_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `eproc_mto_rev`
--
ALTER TABLE `eproc_mto_rev`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_osd`
--
ALTER TABLE `eproc_osd`
  MODIFY `osd_id` int(200) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_osd_detail`
--
ALTER TABLE `eproc_osd_detail`
  MODIFY `osd_det_id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `eproc_po`
--
ALTER TABLE `eproc_po`
  MODIFY `id_po` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `eproc_po_detail_old`
--
ALTER TABLE `eproc_po_detail_old`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_po_old`
--
ALTER TABLE `eproc_po_old`
  MODIFY `id_po` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_priority`
--
ALTER TABLE `eproc_priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `eproc_receiving`
--
ALTER TABLE `eproc_receiving`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `eproc_receiving_detail`
--
ALTER TABLE `eproc_receiving_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `eproc_receiving_document`
--
ALTER TABLE `eproc_receiving_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `eproc_term`
--
ALTER TABLE `eproc_term`
  MODIFY `id_term` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `eproc_uom`
--
ALTER TABLE `eproc_uom`
  MODIFY `id_uom` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `portal_department`
--
ALTER TABLE `portal_department`
  MODIFY `id_department` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `portal_login_slider`
--
ALTER TABLE `portal_login_slider`
  MODIFY `id_slider` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `portal_role`
--
ALTER TABLE `portal_role`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `portal_user_db`
--
ALTER TABLE `portal_user_db`
  MODIFY `id_user` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000078;

--
-- AUTO_INCREMENT for table `portal_user_history_log`
--
ALTER TABLE `portal_user_history_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `portal_user_login_history`
--
ALTER TABLE `portal_user_login_history`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=348;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
