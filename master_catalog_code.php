<?php

	$servername = "10.5.253.8";
	$username   = "ptsmoe_iss";
	$password   = "Abcd4321s";
	$database   = "pcms_old";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $database);

	// Check connection
	if ($conn->connect_error) {
    	die("Connection failed: " . $conn->connect_error);
	}


?>
	
	<script type="text/javascript" src="assets/jquery/jquery-3.4.1.min.js"></script>

    <!-- Datatable -->
    <link href="assets/datatables/jquery.dataTables.min.css" rel="stylesheet">
    <script type="text/javascript" src="assets/datatables/jquery.dataTables.min.js"></script>

    <center>

  	<h1>Catalog Code</h1>

    <div style="width:700px;">

		<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
      <thead>
        <tr>
          <th width="5px">No</th>
          <th>Code</th>
          <th>Description</th>
        </tr>
      </thead>
      <tbody>
        <?php
            $no          = 1;
            $query       = "SELECT * FROM pcms_wm_catalog_category WHERE status_delete = '1' ORDER BY id desc";
            $query_view  = mysqli_query($conn, $query);
            $total       = mysqli_num_rows($query_view);
         
            while($view  = mysqli_fetch_array($query_view)){ 
        ?>
        <tr>
          <td>
            <?php echo $no; ?>                       
          </td>
          <td>
            <center>
              <input type="text" value="<?php echo $view["catalog_category"]; ?>" id="myInput<?php echo $no; ?>" readonly>
              <button style='background-color: #ffffa3;' onclick="myFunction(<?php echo $no; ?>)">Copy Code</button>
            </center> 
          </td>
          <td><?= $view['catalog_category'] ?></td>
                      
        </tr>
        <?php $no++; ?>
        <?php } ?>

      </tbody>
    	</table>

	</div>

	</center>

<script>

function myFunction(f){

  <?php for($i=1;$i<=$total;$i++){ ?>
    var no = "<?php echo $i; ?>";
    if(no == f){
      var copyText<?php echo $i; ?> = document.getElementById("myInput<?php echo $i; ?>");
      copyText<?php echo $i; ?>.select();
      copyText<?php echo $i; ?>.setSelectionRange(0, 99999)
      document.execCommand("copy");
      alert("Copied Code : " + copyText<?php echo $i; ?>.value +"\nPaste to excel material template file to import.");
    }
  <?php } ?>

}

 $('#dataTable').DataTable( {
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
</script>