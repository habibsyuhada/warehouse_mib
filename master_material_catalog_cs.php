<?php

	$servername = "10.5.253.8";
	$username   = "ptsmoe_iss";
	$password   = "Abcd4321s";
	$database   = "pcms_old";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $database);

	// Check connection
	if ($conn->connect_error) {
    	die("Connection failed: " . $conn->connect_error);
	}


?>
	
	<script type="text/javascript" src="assets/jquery/jquery-3.4.1.min.js"></script>

    <!-- Datatable -->
    <link href="assets/datatables/jquery.dataTables.min.css" rel="stylesheet">
    <script type="text/javascript" src="assets/datatables/jquery.dataTables.min.js"></script>

    <center>

    	<h1>Material Catalog (CS) Code</h1>

    <div style="width:700px;">

		<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th rowspan="2" width="5px">No</th>
                    <th rowspan="2" width="1000">Catalog Code</th>
                    <th rowspan="2" >Catalog Category</th>
                    <th rowspan="2" >Description</th>
                    <th rowspan="2" >Steel Type</th>
                    <th rowspan="2" >Grade</th>
                    <th colspan="4" >Plate / Profile Size</th>
                  </tr>
                  <tr>
                    <th>Thk (mm)</th>
                    <th>Width (M)</th>
                    <th>Length (M)</th>
                    <th>Weight</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                   $no=1;	
                   $query = "SELECT * FROM pcms_wm_material_catalog mc INNER JOIN pcms_wm_catalog_category cc ON cc.id = mc.catalog_category_id INNER JOIN pcms_wm_mto_cat mto_c ON mto_c.id = mc.material_category WHERE mc.status_delete = '0' AND mc.material_category = '2' ORDER BY mc.id DESC";
                   $query_view = mysqli_query($conn, $query);
                   $total_all = mysqli_num_rows($query_view);
                   while($view = mysqli_fetch_array($query_view)){ 

                  ?>
                  <tr>
                    <td>
                      <?php echo $no; ?>                       
                    </td>
                    <td width="1000">
                        <input type="text" value="<?php echo $view["code_material"]; ?>" id="myInput<?php echo $no; ?>" readonly style="width: 85px;">
                        <button style='background-color: #ffffa3;' onclick="myFunction(<?php echo $no; ?>)">Copy Code</button>
                    </td>
                    <td>
                      <center><?php echo $view["catalog_category"]; ?></center>
                    </td>
                    <td>
                      <center><?php echo $view["material"]; ?></center>
                    </td>
                    <td>
                      <center><?= $view['steel_type'] ?></center>
                    </td>
                    <td>
                      <center><?= $view['grade'] ?></center>
                    </td>
                    <td>
                      <center><?= $view['thk_mm'] ?></center>
                    </td>
                    <td>
                      <center><?= $view['width_m'] ?></center>
                    </td>
                    <td>
                      <center><?= $view['length_m'] ?></center>
                    </td>
                    <td>
                      <center><?= $view['weight'] ?></center>
                    </td>
                  </tr>
                  <?php $no++; ?>
                  <?php } ?>

                </tbody>
    	</table>

	</div>

	</center>

<script>

function myFunction(f){

  <?php for($i=1;$i<=$total_all;$i++){ ?>
    var no = "<?php echo $i; ?>";
    if(no == f){
      var copyText<?php echo $i; ?> = document.getElementById("myInput<?php echo $i; ?>");
      copyText<?php echo $i; ?>.select();
      copyText<?php echo $i; ?>.setSelectionRange(0, 99999)
      document.execCommand("copy");
      alert("Copied Code : " + copyText<?php echo $i; ?>.value +"\nPaste to excel material template file to import.");
    }
  <?php } ?>

}

 $('#dataTable').DataTable( {
      "paging": false,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
</script>